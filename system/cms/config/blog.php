<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Empty-Organization
|--------------------------------------------------------------------------
|
| Allow a post does not have organization
|
|--------------------------------------------------------------------------*/
$config['allow_empty_organization']	= FALSE;