<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Menu Order
|--------------------------------------------------------------------------
|
| Order of menu for main navigation
|
|--------------------------------------------------------------------------*/
$config['menu_order']	= array(
	'lang:cp:nav_Laporan', 
	'lang:cp:nav_content', 
	'lang:cp:nav_Gallery',
	'lang:cp:nav_structure', 
	'lang:cp:nav_data', 
	'lang:cp:nav_Pendamping',
	'lang:cp:nav_users', 
	'lang:cp:nav_Location', 
	'lang:cp:nav_organization', 
	'lang:cp:nav_Dialog',
	'lang:cp:nav_Human_Resource',
	'lang:cp:nav_profile',
	'lang:cp:nav_SMS Gateway API',
	'lang:cp:nav_addons',
	'lang:cp:nav_settings', 
);

/*
|--------------------------------------------------------------------------
| Module Order
|--------------------------------------------------------------------------
|
| Order of module (by slug)
|
|--------------------------------------------------------------------------*/
$config['module_order'] = array(
	// content
	'blog',
	'comments',
	'pages',
	'files',

	// users
	'users',
	'groups',
	'permissions',
	'organization',

	// structure
	'workflow',
	'navigation',
	'redirects',
	'taxonomy',
	'templates',
	'widgets',
	'wysiwyg',
	'streams_core',

	// data
	'keywords',
	'maintenance',
	'variables',
	'contact',
	'search',
	'sitemap',

	// settings
	'settings',

	// addons
	'addons',
	'module_scaffold',
);