<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Organization
*
* Module API
*
*/
class Api extends API2_Controller
{
	public $metod = 'get';

	/**
	 * Every time this controller is called should:
	 * - load the blog and blog_categories models.
	 * - load the keywords library.
	 * - load the blog language file.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('blog_m');
		$this->load->model('comments/comment_m');
		$this->load->model('blog_categories_m');
		$this->load->library(array('keywords/keywords'));
		$this->lang->load('blog');

		// We are going to get all the categories so we can
		// easily access them later when processing posts.
		$cates = $this->db->get('blog_categories')->result_array();
		$this->categories = array();
	
		foreach ($cates as $cate)
		{
			$this->categories[$cate['id']] = $cate;
		}

		// Get blog stream. We use this to set the template
		// stream throughout the blog module.
		// $this->stream = $this->streams_m->get_stream('blog', true, 'blogs');
		$this->load->helper('array_helper');

		$this->authorize_api_access('blog');
	}

	public function get_all()
	{
		// Get our comment count whil we're at it.
		$this->row_m->sql['select'][] = "(SELECT COUNT(id) FROM ".
				$this->db->protect_identifiers('comments', true)." WHERE module='blog'
				AND is_active='1' AND entry_key='blog:post' AND entry_plural='blog:posts'
				AND entry_id=".$this->db->protect_identifiers('blog.id', true).") as `comment_count`";

		// Get the latest blog posts
		// $params = array(
		// 	'stream'		=> 'blog',
		// 	'namespace'		=> 'blogs',
		// 	'limit'			=> Settings::get('records_per_page'),
		// 	'where'			=> "`status` = 'live'",
		// 	'paginate'		=> 'yes',
		// 	'pag_base'		=> site_url('blog/page'),
		// 	'pag_segment'   => 3
		// );
		// $posts = $this->streams->entries->get_entries($params);
		// dump($posts);

		// -------------------------------------
		// Pagination
		// -------------------------------------
		$params = array(
			'status'=>'live',
			'custom' => "(default_blog_translation.lang_code is null or default_blog_translation.lang_code='".$_SESSION['lang_code']."')"
			);

		$pagination_config['base_url'] = base_url(). $_SESSION['lang_code'] . '/blog/page';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = count($this->blog_m->get_many_by($params));
		$pagination_config['per_page'] = 2;
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		if(strpos(current_url(), "page")) {
			$limit = $pagination_config['per_page'];
			$page = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 1;
			$params['limit'] = array($limit, ($page!=1) ? ($page-1) * $limit : 0);
		}
		$blog = $this->blog_m->get_many_by($params);
		$posts['entries'] = object_to_array($blog);

		$posts['total'] = $pagination_config['total_rows'];
		$posts['pagination'] = $this->pagination->create_links();

		// Process posts
		foreach ($posts['entries'] as &$post)
		{
			$post['commented'] = NULL;
			$this->_process_post($post);

		}

		$data = array(
			'pagination' => $posts['pagination'],
			'posts' => $posts['entries']
		);

		$status = "200";
		$result = $data['posts'];
		if(empty($data['posts'])) {
			$status = "400";
			$result = array("status"=>"error","message"=>"Post data not found");
		}
		_output($result,$status);
	}

	public function get_by_slug($slug)
	{
		// We need a slug to make this work.
		if ( ! $slug)
		{
			$status = "400";
			$result = array("status"=>"error","message"=>"Parameter slug is required");
		}

		// $params = array(
		// 	'stream'		=> 'blog',
		// 	'namespace'		=> 'blogs',
		// 	'limit'			=> 1,
		// 	'where'			=> "`slug` = '{$slug}'"
		// );
		// $data = $this->streams->entries->get_entries($params);
		$data = (Array)$this->blog_m->get_by("(default_blog.slug like '%".$slug."%' OR default_blog_translation.translation like '%\"slug_%\":\"".$slug."\"%')");
		$post = (isset($data)) ? $data : null;
		
		if ( ! $post or ($post['status'] !== 'live' and ! $this->ion_auth->is_admin()))
		{
			$status = "400";
			$result = array("status"=>"error","message"=>"There is no live post");
		}

		// Translation
		// get the translation of that post
		$translation = $this->blog_m->get_post_translation($post['id'],$_SESSION['lang_code']);
		$post['translation'] = NULL;
		if($translation) {
			$post['translation'] = $translation[0];
			$tmp_translation = json_decode($translation[0]->translation,true);
			if($slug!=$tmp_translation['slug_'.$_SESSION['lang_code']]) {
				$redirect = $_SESSION['lang_code'].'/blog/'.date('Y/m', $post['created_on']).'/'.$tmp_translation['slug_'.$_SESSION['lang_code']];
				redirect($redirect);
			}
		}

		$this->_process_post($post);
		$post['comments'] = $this->comment_m->get_by_entry('blog','blog:post',$post['id']);

		$status = "200";
		$result = $post;
		if(empty($post)) {
			$status = "400";
			$result = array("status"=>"error","message"=>"Post data not found");
		}
		_output($result,$status);
	}

	/**
	 * Process Post
	 *
	 * Process data that was not part of the 
	 * initial streams call.
	 *
	 * @return 	void
	 */
	private function _process_post(&$post)
	{
		$this->load->helper('text');

		// Keywords array
		$keywords = Keywords::get($post['keywords']);
		$formatted_keywords = array();
		$keywords_arr = array();

		foreach ($keywords as $key)
		{
			$formatted_keywords[] 	= array('keyword' => $key->name);
			$keywords_arr[] 		= $key->name;

		}
		$post['keywords'] = $formatted_keywords;
		$post['keywords_arr'] = $keywords_arr;

		// Full URL for convenience.
		$post['url'] = site_url('blog/'.date('Y/m', $post['created_on']).'/'.$post['slug']);
	
		// What is the preview? If there is a field called intro,
		// we will use that, otherwise we will cut down the blog post itself.
		$post['preview'] = (isset($post['intro'])) ? $post['intro'] : $post['body'];

		// Category
		if ($post['category_id'] > 0 and isset($this->categories[$post['category_id']]))
		{
			$post['category'] = $this->categories[$post['category_id']];
		}

		// Translation
		// get the translation of that post
		$translation = $this->blog_m->get_post_translation($post['id'],$_SESSION['lang_code']);
		$post['translation'] = NULL;
		if($translation) {
			$post['translation'] = $translation[0];
			$tmp_translation = json_decode($translation[0]->translation,true);
			$post['title'] = $tmp_translation['title_'.$_SESSION['lang_code']];
			$post['slug'] = $tmp_translation['slug_'.$_SESSION['lang_code']];
			$post['body'] = $tmp_translation['body_'.$_SESSION['lang_code']];
			$post['intro'] = $tmp_translation['intro_'.$_SESSION['lang_code']];
			$post['preview'] = (isset($post['intro'])) ? $post['intro'] : $post['body'];
		}
		$post['url'] = site_url($_SESSION['lang_code'].'/blog/'.date('Y/m', $post['created_on']).'/'.$post['slug']);
		$post['commented'] = $this->count_comment_string($post['id']);

		// clean the unused fields
		$fields = array("id", "created", "updated", "created_by", "title", "slug", "category_id", "body", "author_id", "status", "intro", "image", "username", "display_name", "id_unit", "unit_name", "unit_slug", "category", "commented");

		foreach ($post as $key => $value) {
			if(in_array($key, $fields)) {
				$reformat[$key] = $value;
			}
		}
		$post = NULL;
		$post = $reformat;
	}

	public function count_comment_string($entry_id)
	{
		$this->lang->load('comments/comments');
		$total = $this->db->where(array(
			'module'	=> 'blog',
			'entry_key'	=> 'blog:post',
			'entry_id'	=> $entry_id,
			'is_active'	=> true,
		))->count_all_results('comments');
		switch ($total)
		{
			case 0:
				$line = 'none';
				break;
			case 1:
				$line = 'singular';
				break;
			default:
				$line = 'plural';
		}
		return sprintf(lang('comments:counter_'.$line.'_label'), $total);
	}
}