<div class="page-header">
	<h1><?php echo lang('blog:posts_title') ?></h1>
	<?php if(group_has_role('blog','create_post') OR group_has_role('blog','create_own_unit_post') ) : ?>
	<div class="btn-group content-toolbar">
		<a class="btn btn-yellow btn-sm" href="<?php echo base_url(); ?>admin/blog/create">
			<i class="icon-plus"></i>
			<span class="no-text-shadow"><?php echo lang('blog:create_title'); ?></span>
		</a>
	</div>
	<?php endif; ?>
	<?php file_partial('shortcuts'); ?>
</div>

<?php echo $this->load->view('admin/partials/filters') ?>
<hr />
<?php if ($blog) : ?>	

	<?php echo form_open('admin/blog/action',array('id'=>'form_action_post')) ?>
		<div id="filter-stage">
			<?php echo $this->load->view('admin/tables/posts') ?>
		</div>
	<?php echo form_close() ?>
<?php else : ?>
	<div class="well"><?php echo lang('blog:currently_no_posts') ?></div>
<?php endif ?>