<?php if ($this->method == 'edit'): ?>
	<div class="page-header">
    	<h1><?php echo sprintf(lang('groups:edit_title'), $group->name) ?></h1>
	</div>
<?php else: ?>
	<div class="page-header">
    	<h1><?php echo lang('groups:add_title') ?></h1>
	</div>
<?php endif ?>

<?php echo form_open(uri_string(), 'class="form-horizontal"') ?>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('groups:name');?> <span>*</span></label>
	<div class="col-sm-10"><?php echo form_input('description', $group->description);?></div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('groups:short_name');?> <span>*</span></label>
	

	<?php if ( ! in_array($group->name, array('admin'))): ?>
	<div class="col-sm-10">
		<?php echo form_input('name', $group->name);?>
	</div>

	<?php else: ?>
	<label class="col-sm-10">
		<?php echo $group->name ?>
	</label>
	<?php endif ?>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('groups:force_fill_profile');?></label>
	<div class="col-sm-10">
		<?php $value = ($this->input->post('force_fill_profile')) ? $this->input->post('force_fill_profile') : $group->force_fill_profile; ?>
		<label>
			<input name="force_fill_profile" class="ace ace-switch ace-switch-7" value="1" <?php echo ($value=='1') ? 'checked' : ''; ?> type="checkbox">
			<span class="lbl"></span>
		</label>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('groups:enable_personel_link');?></label>
	<div class="col-sm-10">
		<?php $value = ($this->input->post('enable_personel_link')) ? $this->input->post('enable_personel_link') : $group->enable_personel_link; ?>
		<label>
			<input name="enable_personel_link" class="ace ace-switch ace-switch-7" value="1" <?php echo ($value=='1') ? 'checked' : ''; ?> type="checkbox">
			<span class="lbl"></span>
		</label>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('groups:force_create_personel_link');?></label>
	<div class="col-sm-10">
		<?php $value = ($this->input->post('force_create_personel_link')) ? $this->input->post('force_create_personel_link') : $group->force_create_personel_link; ?>
		<label>
			<input name="force_create_personel_link" class="ace ace-switch ace-switch-7" value="1" <?php echo ($value=='1') ? 'checked' : ''; ?> type="checkbox">
			<span class="lbl"></span>
		</label>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-9">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
	</div>
</div>
	
<?php echo form_close();?>