<?php

function get_groups_by_id($id) {
	ci()->load->model('groups/group_m');

	$group_list = '';

	if ($id) {
		foreach ($id as $key => $value) {
			$res = ci()->group_m->get_group_by_id($value);
			if(isset($res)) {
				$group_list .= '<a href="'.base_url().'admin/users/index?f_group='.$res->id.'">'.$res->description.'</a>';

				if ($key != count($id)-1) {
					$group_list .= '<strong> , </strong>';
				}
			}
		}
	}

	return $group_list;
}

function get_setting_by_group($id) {
	ci()->load->model('groups/group_m');

	return ci()->group_m->get_group_by_id($id);
}

?>