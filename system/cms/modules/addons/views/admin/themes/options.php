<div class="page-header">
	<h1><?php echo lang('addons:themes:theme_label').' '.lang('addons:themes:options') ?></h1>
</div>

<?php if ($options_array): ?>

	<div class="padding-top">
		<?php echo form_open('admin/addons/themes/options/'.$slug, 'class="form-horizontal options-form"');?>
		
			<?php echo form_hidden('slug', $slug) ?>
		
			<?php foreach($options_array as $option): ?>
				<div class="form-group">
					<label class="col-sm-2 control-label no-padding-right">
						<?php echo $option->title ?>
					</label>
					<div class="col-sm-10 <?php echo 'type-'.$option->type ?>">
						<?php echo $controller->form_control($option) ?><br />
						<small><?php echo $option->description ?></small>
					</div>
				</div>
			<?php endforeach ?>
			
			
			<div class="clearfix form-actions">
				<div class="col-md-offset-2 col-md-9">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('re-index') )) ?>
				</div>
			</div>
			
		<?php echo form_close() ?>
	</div>

<?php endif ?>

<script type="text/javascript">
	(function($) {
		$(function() {
			$('.colour-picker').miniColors({
				letterCase: 'uppercase',
			});
		});
	})(jQuery);
</script>