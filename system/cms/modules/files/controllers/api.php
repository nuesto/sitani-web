<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Dialog
*
* Module API
*
*/
class Api extends API2_Controller
{
	public $metod = 'get';

	public function __construct()
	{
		parent::__construct();

		$this->config->load('files');

		$this->load->model(array(
			'file_m',
			'file_folders_m'
		));

		$this->_path = FCPATH.rtrim($this->config->item('files:path'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;

		// $this->authorize_api_access('files');
	}

	public function listing($folder_id = '')
	{
		if(empty($folder_id)) {
			$status = "400";
			$result = array("status"=>"error","message"=>"Parameter folder id not found");
		} else {
			$tags      = false;
			$limit     = null;
			$offset    = '';
			$type      = '';
			$fetch     = NULL;
			$order_by  = 'sort';
			$order_dir = 'asc';

			if ( ! empty($folder_id) && (empty($type) || in_array($type, array('a','v','d','i','o'))))
			{
				if (is_numeric($folder_id))
				{
					$folder = $this->file_folders_m->get($folder_id);
				}
				elseif (is_string($folder_id))
				{
					$folder = $this->file_folders_m->get_by_path($folder_id);
				}
			}

			if(empty($folder)) {
				$status = "400";
				$result = array("status"=>"error","message"=>"Folder data not found");
			} else {
				if (isset($folder) and $folder)
				{
					// we're getting the files for an entire tree
					if (in_array($fetch, array('root', 'subfolder')))
					{
						$fetch_id = ($fetch === 'root' ? $folder->root_id : $folder->id);

						$subfolders = $this->file_folders_m->folder_tree($fetch_id);

						if ($subfolders)
						{
							$ids = array_merge(array((int) $folder->id), array_keys($subfolders));
							$this->db->select('files.*, files.id as file_id, file_folders.location, file_folders.name folder_name, file_folders.slug folder_slug')
							->join('file_folders', 'file_folders.id = files.folder_id')
							->where_in('folder_id', $ids);
						}
					}
					// just the files for one folder
					else
					{
						$this->db->select('files.*, files.id as file_id, file_folders.location, file_folders.name folder_name, file_folders.slug folder_slug')
						->join('file_folders', 'file_folders.id = files.folder_id')
						->where('folder_id', $folder->id);
					}
				}
				// no restrictions by folder so we'll just be getting files by their tags. Set up the join
				elseif ( ! isset($folder))
				{
					$this->db->select('files.*, files.id as file_id, file_folders.location, file_folders.name folder_name, file_folders.slug folder_slug')
					->join('file_folders', 'file_folders.id = files.folder_id');
				}
				else
				{
					return array();
				}

				$type      and $this->db->where('type', $type);
				$limit     and $this->db->limit($limit);
				$offset    and $this->db->offset($offset);
				$order_by  and $this->db->order_by($order_by, $order_dir);

				$files = array();
				if ($tags)
				{
					$files = $this->file_m->get_tagged($tags);
				}
				else
				{
					$files = $this->file_m->get_all();
				}

				$status = "200";
				$result = $files;
				if(empty($result)) {
					$status = "400";
					$result = array("status"=>"error","message"=>"Files data not found");
				}
			}
		}
		_output($result,$status);
	}
}
