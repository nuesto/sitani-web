<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Permissions Events Class
 *
 * @category    events
 * @author      Aditya Satrya
 */
class Events_Permissions
{
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();

		Events::register('module_disabled', array($this, 'delete_all_permissions'));
     }

    function delete_all_permissions($module_slug)
	{
		// commented to make development easier
        //$this->ci->db->where('module', $module_slug);
		//$this->ci->db->delete('permissions'); 
	}

}
/* End of file events.php */