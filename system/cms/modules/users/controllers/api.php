<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Users
*
* Module API
*
*/
class Api extends API2_Controller
{
	public $metod = 'get';

    /**
	 * Constructor method
	 */
	public function __construct()
	{
		parent::__construct();

		// Load the required classes
		$this->load->helper('user');
	}

    /**
	 * Log in
	 */
	public function login()
	{
        header('Access-Control-Allow-Headers: Content-Type');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        $params = json_decode(file_get_contents('php://input'),true);
        $_POST = $params;
		// Set the validation rules

		if($this->input->post('is_pendamping')){
			$this->validation_rules = array(
				array(
					'field' => 'no_hp',
					'label' => 'No Handphone',
					'rules' => 'required|callback__check_login'
				),
			);
			$email = $this->input->post('no_hp');
		}else{
			$this->validation_rules = array(
				array(
					'field' => 'username',
					'label' => lang('global:username'),
					'rules' => 'required|callback__check_login'
				),
				array(
					'field' => 'password',
					'label' => lang('global:password'),
					'rules' => 'required'
				)
			);
			$email = $this->input->post('username');
		}

		// Call validation and set rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validation_rules);

		// If the validation worked, or the user is already logged in
		if ($this->form_validation->run() or $this->ion_auth->logged_in())
		{
			// if they were trying to go someplace besides the
			// dashboard we'll have stored it in the session
            $user = $this->ion_auth->get_user_by_identity($email);
            if($user->group_id == '9'){
            	$this->ion_auth->logout();
            	$result = array('status'=>'error','messages'=>'Log In Gagal');
            }else{
            	$result = array('status'=>'success','messages'=>lang('user:logged_in'),'data'=>(array)$user);
            }
		} else {
            $result = array('status'=>'error','messages'=>validation_errors());
        }

        _output($result);
	}

	public function get_user_units($id_user)
	{
		$this->load->library('organization/organization');
		$user = $this->ion_auth->get_user($id_user);
		dump($user);

		$user_unit = $this->organization->get_membership_by_user($id_user);
		dump($user_unit);
	}

	/**
	 * Logout
	 */
	public function logout()
	{
		$this->load->language('users/user');
		$this->ion_auth->logout();
		echo json_encode(array('status'=>'success','messages'=>lang('user:logged_out')));
	}

	/**
	 * Callback From: login()
	 *
	 * @param string $email The Email address to validate
	 *
	 * @return bool
	 */
	public function _check_login($email)
	{
		if ($this->ion_auth->login($email, $this->input->post('password'), (bool)$this->input->post('remember')))
		{
			Events::trigger('post_admin_login');

			return true;
		}

		Events::trigger('login_failed', $email);
		error_log('Login failed for user '.$email);

		$this->form_validation->set_message('_check_login', $this->ion_auth->errors());
		return false;
	}
}
