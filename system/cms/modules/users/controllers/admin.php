<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin controller for the users module
 *
 * @author		 PyroCMS Dev Team
 * @package	 PyroCMS\Core\Modules\Users\Controllers
 */
class Admin extends Admin_Controller
{

	protected $section = 'users';

	/**
	 * Validation for basic profile
	 * data. The rest of the validation is
	 * built by streams.
	 *
	 * @var array
	 */
	private $validation_rules = array(
    'email' => array(
	    'field' => 'email',
	    'label' => 'lang:global:email',
	    'rules' => 'max_length[60]'
    ),

		'password' => array(
			'field' => 'password',
			'label' => 'lang:global:password',
			'rules' => 'min_length[6]|max_length[20]'
		),
		'username' => array(
			'field' => 'username',
			'label' => 'lang:user_username',
			'rules' => 'required|alpha_dot_dash|min_length[3]|max_length[20]'
		),
		array(
			'field' => 'group_id',
			'label' => 'lang:user_group_label',
			'rules' => 'required|callback__group_check'
		),
		array(
			'field' => 'active',
			'label' => 'lang:user_active_label',
			'rules' => ''
		),
		array(
			'field' => 'display_name',
			'label' => 'lang:profile_display_name',
			'rules' => 'required|max_length[50]'
		)
	);

	/**
	 * Constructor method
	 */
	public function __construct()
	{
		parent::__construct();

		// Load the required classes
		$this->load->model('user_m');
		$this->load->model('groups/group_m');
		$this->load->helper('user');
		$this->load->library('form_validation');
		$this->lang->load('user');

		// Load classes from module Organization
		$this->load->model('organization/units_m');
		$this->load->model('organization/memberships_m');
		$this->load->model('human_resource/personel_m');
		$this->lang->load('organization/organization');
		$this->load->library('organization/Organization');
		$this->config->load('organization');

		// Load location
		$this->lang->load('location/location');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');

		if ($this->current_user->group != 'admin')
		{

			if (!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
				$memberships = $this->organization->get_membership_by_user($this->current_user->id);
				$data['membership_units'] = $memberships['entries'];

				$unit_lists = array();

				foreach ($memberships['entries'] as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				if(count($unit_lists)>0) {
					$res_available_groups = $this->group_m->get_available_groups_by_units($unit_lists);
					$available_groups = array();
					foreach ($res_available_groups as $value) {
						$available_groups = array_merge($available_groups, json_decode($value['available_groups']));
					}
					$available_groups = implode(",", $available_groups);

					$this->template->groups = $this->group_m->where_not_in('name', 'admin')->get_all(array('available_groups' => $available_groups));
				} else {
					$this->template->groups = $this->group_m->where_not_in('name', 'admin')->get_all();
				}
			} else {
				$this->template->groups = $this->group_m->where_not_in('name', 'admin')->get_all();
			}
		}
		else
		{
			$this->template->groups = $this->group_m->get_all();
		}

		$this->template->groups_select = array_for_select($this->template->groups, 'id', 'description');
	}

	/**
	 * List all users
	 */
	public function index()
	{
		if(! group_has_role('users', 'manage_users') AND ! group_has_role('users', 'manage_own_unit_users')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$base_where = array('active' => 0);

		// ---------------------------
		// User Filters
		// ---------------------------

		// Determine active param
		$base_where['active'] = $this->input->get('f_module') ? (int)$this->input->get('f_active') : $base_where['active'];

		// Determine organization unit param
		$data['membership_units'] = array();
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			$memberships = $this->organization->get_membership_by_user($this->current_user->id);
			$data['membership_units'] = $memberships['entries'];

			foreach ($memberships['entries'] as $key => $value) {
				$data['unit_lists'][] = $value['membership_unit']['id'];
			}

			if (!isset($_GET['f_unit']) AND isset($data['unit_lists'])) {
				$_GET['f_unit'] = implode(',', $data['unit_lists']);
			}
		}

		$base_where = $this->input->get('f_unit') ? $base_where + array('unit_id' => $this->input->get('f_unit')) : $base_where;

		// Determine group param
		$base_where = $this->input->get('f_group') ? $base_where + array('group_id' => (int)$this->input->get('f_group')) : $base_where;

		// Keyphrase param
		$base_where = $this->input->get('f_keywords') ? $base_where + array('name' => $this->input->get('f_keywords')) : $base_where;

		// Create pagination links
		$pagination = create_pagination('admin/users/index', $this->user_m->count_by($base_where));

		//Skip admin
		$skip_admin = ( $this->current_user->group != 'admin' ) ? 'admin' : '';

		// Using this data, get the relevant results
		$this->db->order_by('active', 'desc')
			->limit($pagination['limit'], $pagination['offset']);

		if($skip_admin!='') {
			$this->db->where_not_in('groups.name', $skip_admin);
		}

		$users = $this->user_m->get_many_by($base_where);
		$user2 = array();
		$id_user = array();

		foreach ($users as $value) {
			$user2[$value->id] = (array)$value;

			$id_user[] = $value->id;
		}

		$memberships_list = $this->memberships_m->get_memberships_by_multi_user($id_user);
		$unit_list = array();

		foreach ($memberships_list as $value) {
			$unit_list[$value['membership_user']][] = $value;
		}

		foreach ($user2 as $key => $value) {
			if(array_key_exists($key, $unit_list)) {
				$user2[$key]['units'] = $unit_list[$key];
			} else {
				$user2[$key]['units'] = array();
			}

			$user2[$key] = (object)$user2[$key];
		}

		// Unset the layout if we have an ajax request
		if ($this->input->is_ajax_request())
		{
			$this->template->set_layout(false);
		}

		// set session page that user view
		$this->session->set_userdata('page',$pagination['current_page']);

		// Render the view
		$this->template
			->title($this->module_details['name'])
			->set('pagination', $pagination)
			->set('users', $user2)
			->set_partial('filters', 'admin/partials/filters')
			->set_partial('tables/users', 'admin/tables/users');

		$this->input->is_ajax_request() ? $this->template->build('admin/tables/users', $data) : $this->template->build('admin/index', $data);
	}

	/**
	 * View a user profile based on the username
	 *
	 * @param string $username The Username or ID of the user
	 */
	public function view($id = NULL)
	{
		// If $id is NULL then make it current user id
		if($id == NULL){
			$user = $this->current_user;
			$id = $user->id;
		}else{
			$user = $this->ion_auth->get_user($id);
		}

		// No user? Show a 404 error
		$user or show_404();

		// check permission
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);
			if(count($memberships_current_user['entries'])!=0) {

				if (count($memberships_current_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_current_user = $memberships_current_user['entries'];

				foreach ($unit_current_user as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				$memberships_user = $this->organization->get_membership_by_user($id);

				if (count($memberships_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_user = $memberships_user['entries'];

				foreach ($unit_user as $key => $value) {
					$unit_user_lists[] = $value['membership_unit']['id'];
				}

				if (in_array($unit_user_lists, $unit_lists)) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}


		if($id == NULL OR $this->current_user->id == $id){
			if(! (group_has_role('users', 'view_own_account') OR group_has_role('users', 'view_own_profile')
				OR group_has_role('users', 'view_all_account') OR group_has_role('users', 'view_all_profile'))){

				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}else{
			if(! (group_has_role('users', 'view_all_account') OR group_has_role('users', 'view_all_profile'))){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}


		// We need the profile ID to pass to get_stream_fields.
		// This theoretically could be different from the actual user id.
		if ($id)
		{
			$profile_id = $this->db->limit(1)->select('id')->where('user_id', $id)->get('profiles')->row()->id;
		}
		else
		{
			$profile_id = null;
		}

		$stream_fields = $this->streams_m->get_stream_fields($this->streams_m->get_stream_id_from_slug('profiles', 'users'));

		$profile = $this->db->limit(1)->where('user_id', $id)->get('profiles')->row();

		// Set Values
		$values = $this->fields->set_values($stream_fields, $profile, 'edit');
		$has_personel = $this->personel_m->get_personel_by_account($id);

		$this->template
			->append_css('module::users.css')
			->build('admin/profile_view', array(
				'user' => $user,
				'profile_fields' => $this->streams->fields->get_stream_fields('profiles', 'users', $values, $profile_id),
				'has_personel' => $has_personel,
			));
	}

	/**
	 * Method for handling different form actions
	 */
	public function action()
	{
		// Pyro demo version restrction
		if (PYRO_DEMO)
		{
			$this->session->set_flashdata('notice', lang('global:demo_restrictions'));
			redirect('admin/users');
		}

		// Determine the type of action
		switch ($this->input->post('btnAction'))
		{
			case 'activate':
				$this->activate();
				break;
			case 'delete':
				$this->delete();
				break;
			default:
				redirect('admin/users');
				break;
		}
	}

	/**
	 * Create a new user
	 */
	public function create()
	{
		// check permission
		role_or_die('users', 'create_users');

		// Extra validation for basic data
		if($this->input->post('group_id') != 7 && $this->input->post('group_id') != 9 && $this->input->post('group_id') != 10){
			$this->validation_rules['email']['rules'] .= '|required|valid_email|callback__email_check';
		}
		$this->validation_rules['password']['rules'] .= '|required';
		$this->validation_rules['username']['rules'] .= '|callback__username_check';

		// Location
		$data['mode'] = 'new';
		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;

		// if(!group_has_role('users','create_users')){
		// 	$data['id_provinsi'] = user_provinsi($this->current_user->id);
		// 	$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
		// }else{
			$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
		// }

		$filter_kota = NULL;
  	if($data['id_provinsi'] != NULL) {
  		$filter_kota['id_provinsi'] = $data['id_provinsi'];
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);
  	}else{
  		if($this->input->post('provinsi') != ''){
  			$data['id_provinsi'] = $this->input->post('provinsi');
	  		$filter_kota['id_provinsi'] = $data['id_provinsi'];
	  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);
  		}
  	}

  	$data['contents_units'] = '-';
  	$id_kota = NULL;
		if($this->input->post('kota')){
			$id_kota = $this->input->post('kota');
		}else if($data['id_kota'] != NULL){
			$id_kota = $data['id_kota'];
		}

		// Add custom validation rules here
		// ....
		//
		$this->validation_rules['nip'] = array(
			'field' => 'nip',
			'label' => 'lang:profile_nip',
			'rules' => 'required|is_natural|max_length[20]'
		);

		$this->validation_rules['telp'] = array(
			'field' => 'telp',
			'label' => 'lang:profile_telp',
			'rules' => 'required|is_natural|max_length[12]|callback__telp_check'
		);

		if($this->input->post('group_id') == 8 || $this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10){
			$this->validation_rules['provinsi'] = array(
				'field' => 'provinsi',
				'label' => 'lang:location:provinsi:singular',
				'rules' => 'required'
			);
			if($this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10){
				$this->validation_rules['kota'] = array(
					'field' => 'kota',
					'label' => 'lang:location:kota:singular',
					'rules' => 'required'
				);
			}
		}
		if($this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10){
			$validate_units = true;
			if($this->input->post('organization_membership')){
				if(count($this->input->post('organization_membership')) == 0){
					$validate_units= false;
				}
			}else{
				$validate_units = false;
			}

			if(!$validate_units){
				// $this->validation_rules['organization_membership'] = array(
				// 	'field' => 'organization_membership',
				// 	'label' => 'lang:organization:units:plural',
				// 	'rules' => 'required'
				// );
			}
		}

		if($this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10){
			unset($this->validation_rules['username']);
			unset($this->validation_rules['password']);
			unset($this->validation_rules['nip']);
		}

		// Set the validation rules
		$this->form_validation->set_rules($this->validation_rules);

		$email = strtolower($this->input->post('email'));
		$group_id = $this->input->post('group_id');
		$password = ($group_id != 7 && $group_id != 9 && $group_id != 10) ? $this->input->post('password') : ($this->input->post('telp'));
		$username = ($group_id != 7 && $group_id != 9 && $group_id != 10) ? $this->input->post('username') : ($this->input->post('telp'));
		$activate = $this->input->post('active');

		// keep non-admins from creating admin accounts. If they aren't an admin then force new one as a "user" account
		$group_id = ($this->current_user->group !== 'admin' and $group_id == 1) ? 2 : $group_id;

		// Get user profile data.
		$profile_data = array();
		$profile_data['display_name'] = $this->input->post('display_name');
		$profile_data['nip'] = ($group_id != 7 && $group_id != 9 && $group_id != 10) ? $this->input->post('nip') : NULL;
		$profile_data['id_provinsi'] = ($group_id == 8) ? $this->input->post('provinsi') : NULL;
		$profile_data['telp'] = $this->input->post('telp');

		// Add custom profile fields here
		// ...
		//
		// $profile_data['field_name'] = $this->input->post('field_name');

		if ($this->form_validation->run() !== false)
		{
			$this->load->library('files/files');
			$id_folder = Files::get_id_by_name('Public');


			if($_FILES['picture_id']['name']!=''){
				$data = Files::upload($id_folder, $_FILES['picture_id']['name'], 'picture_id');
				$profile_data['picture_id'] = $data['data']['id'];
			}

			$profile_data['is_complete'] = 0;


			if ($activate === '2')
			{
				// we're sending an activation email regardless of settings
				Settings::temp('activation_email', true);
			}
			else
			{
				// we're either not activating or we're activating instantly without an email
				Settings::temp('activation_email', false);
			}

			$group = $this->group_m->get($group_id);

			// Register the user (they are activated by default if an activation email isn't requested)
			if ($user_id = $this->ion_auth->register($username, $password, $email, $group_id, $profile_data, NULL, $group->name))
			{
				if ($activate === '0')
				{
					// admin selected Inactive
					$this->ion_auth_model->deactivate($user_id);
				}

				// Fire an event. A new user has been created.
				Events::trigger('user_created', $user_id);

				// insert all membership for this user
				$organization_memberships = $this->input->post('organization_membership');

				if(isset($organization_memberships) AND is_array($organization_memberships)){
					foreach($organization_memberships as $unit_id){
						$this->memberships_m->create_membership($user_id, $unit_id);
					}
				}

				// Set the flashdata message and redirect
				$this->session->set_flashdata('success', $this->ion_auth->messages());

				// Redirect back to the form or main page
				$this->input->post('btnAction') === 'save_exit' ? redirect('admin/users?'.$_SERVER['QUERY_STRING']) : redirect('admin/users/edit/'.$user_id.'?'.$_SERVER['QUERY_STRING']);
			}
			// Error
			else
			{
				$this->template->error_string = $this->ion_auth->errors();
			}
		}
		else
		{
			// Dirty hack that fixes the issue of having to
			// re-add all data upon an error
			if ($_POST)
			{
				$member = (object) $_POST;
			}

		}

		if ( ! isset($member))
		{
			$member = new stdClass();
		}

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			$member->{$rule['field']} = set_value($rule['field']);
		}

		$group_id = null;
		if($member->group_id != NULL AND $member->group_id != ''){
			$group_id = $member->group_id;
		}else{
			$group_id = $this->input->get('f_group');
		}

		if($id_kota != NULL){
  		$data['contents_units'] = $this->ajax_get_units_by_kota($id_kota, $group_id, $is_ajax = false, null, null);
		}
		// Get organizations values
		// $root_units = $this->units_m->get_units_by_level(0);
		// $units = $this->organization->build_serial_tree(array(), $root_units);

		// if (! group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
		// 	foreach ($units as $key => $value) {
		// 		$unit_list[$value['id']] = $value;
		// 	}

		// 	$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);

		// 	foreach ($memberships_current_user['entries'] as $value) {
		// 		$memberships_unit_list[$value['membership_unit']['id']] = $value['membership_unit']['id'];
		// 	}

		// 	if(isset($memberships_unit_list)) {
		// 		$units = array_intersect_key($unit_list, $memberships_unit_list);

		// 		sort($units);
		// 	}
		// }

		$units = array();

		$this->template
			->title($this->module_details['name'], lang('user:add_title'))
			->set('member', $member)
			->set('display_name', set_value('display_name', $this->input->post('display_name')))
			->set('units', $units)
			->build('admin/form',$data);
	}

	/**
	 * Edit an existing user
	 *
	 * @param int $id The id of the user.
	 */
	public function edit($id = 0, $forced = '')
	{
		// check permission
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {

			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);
			if(count($memberships_current_user['entries'])!=0) {
				if (count($memberships_current_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_current_user = $memberships_current_user['entries'];

				foreach ($unit_current_user as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				$memberships_user = $this->organization->get_membership_by_user($id);

				if (count($memberships_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_user = $memberships_user['entries'];

				foreach ($unit_user as $key => $value) {
					$unit_user_lists[] = $value['membership_unit']['id'];
				}

				if (in_array($unit_user_lists, $unit_lists)) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		if($id == NULL OR $this->current_user->id == $id){
			if(! (group_has_role('users', 'edit_own_account') OR group_has_role('users', 'edit_own_profile')
				OR group_has_role('users', 'edit_all_account') OR group_has_role('users', 'edit_all_profile'))){

				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}else{
			if(! (group_has_role('users', 'edit_all_account') OR group_has_role('users', 'edit_all_profile'))){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		$this->load->library('files/files');

		// Get the user's data
		if ( ! ($member = $this->ion_auth->get_user($id)))
		{
			$this->session->set_flashdata('error', lang('user:edit_user_not_found_error'));
			redirect('admin/users');
		}

		// Check to see if we are changing usernames
		if ($member->username != $this->input->post('username'))
		{
			$this->validation_rules['username']['rules'] .= '|callback__username_check';
		}

		// Check to see if we are changing emails
		if ($member->email != $this->input->post('email'))
		{
			if($this->input->post('group_id') != 7 && $this->input->post('group_id') != 9){
				$this->validation_rules['email']['rules'] .= '|callback__email_check';
			}
		}

		// Location
		$this->load->model('organization/memberships_m');
		$data['mode'] = 'edit';
		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
		if($member->group_id == 8){
			$data['fields'] = (array)$this->user_m->get(array('id'=>$id));
		}else{
			$data['fields'] = $this->memberships_m->get_units_by_member($id)[0];
		}

		$data['id_provinsi'] = $data['fields']['id_provinsi'];
		$data['id_kota'] = $data['fields']['id_kota'];

		if($this->input->post('kota')){
			$id_kota = $this->input->post('kota');
		}else if($data['id_kota'] != NULL){
			$id_kota = $data['id_kota'];
		}

		$group_id = null;
		if($member->group_id != NULL AND $member->group_id != ''){
			$group_id = $member->group_id;
		}else{
			$group_id = $this->input->get('f_group');
		}

		$data['contents_units'] = $this->ajax_get_units_by_kota($id_kota, $group_id, $is_ajax = false, $mode = 'edit', $id);


		// if(!group_has_role('enumerator','edit_all_enumerator')){
		// 	$data['id_provinsi'] = user_provinsi($this->current_user->id);
		// 	$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
		// }else{
			$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
		// }

		if(!group_has_role('laporan','edit_all_provinsi')) {
			$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
		}

		$filter_kota = NULL;
  	if($data['id_provinsi'] != NULL) {
  		$filter_kota['id_provinsi'] = $data['id_provinsi'];
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);
  	}

		// Add custom validation rules here
		// ....
		//
		// $this->validation_rules['field_name'] = array(
		//	'field' => 'field_name',
		//	'label' => 'lang:field_name',
		//	'rules' => 'required'
		// );

		if($this->input->post('group_id') != 7 && $this->input->post('group_id') != 9 && $this->input->post('group_id') != 10){
			$this->validation_rules['email']['rules'] .= '|required|valid_email';
		}

		if ($member->telp != $this->input->post('telp'))
		{
			$this->validation_rules['telp'] = array(
				'field' => 'telp',
				'label' => 'lang:profile_telp',
				'rules' => 'required|is_natural|max_length[12]|callback__telp_check'
			);
		}

		if($this->session->userdata('group_id') != 7 && $this->input->post('group_id') != 9 && $this->input->post('group_id') != 10){
			if($this->input->post('group_id') == 7 || $this->input->post('group_id') == 10){
				unset($this->validation_rules['nip']);
			}
		}

		if($this->input->post('group_id') == 8 || $this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10){
			$this->validation_rules['provinsi'] = array(
				'field' => 'provinsi',
				'label' => 'lang:location:provinsi:singular',
				'rules' => 'required'
			);
			if($this->input->post('group_id') == 7){
				$this->validation_rules['kota'] = array(
					'field' => 'kota',
					'label' => 'lang:location:kota:singular',
					'rules' => 'required'
				);
			}
		}
		if($this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10){
			$validate_units = true;
			if($this->input->post('organization_membership')){
				if(count($this->input->post('organization_membership')) == 0){
					$validate_units= false;
				}
			}else{
				$validate_units = false;
			}

			if(!$validate_units){
				$this->validation_rules['organization_membership'] = array(
					'field' => 'organization_membership',
					'label' => 'lang:organization:units:plural',
					'rules' => 'required'
				);
			}
		}

		if($this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10){
			unset($this->validation_rules['username']);
			unset($this->validation_rules['password']);
			unset($this->validation_rules['nip']);
		}

		// Set the validation rules
		$this->form_validation->set_rules($this->validation_rules);

		if ($this->form_validation->run() === true)
		{
			$email = strtolower($this->input->post('email'));
			$group_id = $this->input->post('group_id');
			$_POST['password'] = ($group_id != 7 && $group_id != 9 && $group_id != 10) ? $this->input->post('password') : ($this->input->post('telp'));
			$_POST['username'] = ($group_id != 7 && $group_id != 9 && $group_id != 10) ? $this->input->post('username') : ($this->input->post('telp'));

			if (PYRO_DEMO)
			{
				$this->session->set_flashdata('notice', lang('global:demo_restrictions'));
				redirect('admin/users');
			}

			// Get the POST data
			$update_data['email'] = $this->input->post('email');
			$update_data['active'] = $this->input->post('active');
			$update_data['username'] = ($this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10) ? $this->input->post('telp') : $this->input->post('username');

			// allow them to update their one group but keep users with user editing privileges from escalating their accounts to admin
			$update_data['group_id'] = ($this->current_user->group !== 'admin' and $this->input->post('group_id') == 1) ? $member->group_id : $this->input->post('group_id');

			if ($update_data['active'] === '2')
			{
				$this->ion_auth->activation_email($id);
				unset($update_data['active']);
			}
			else
			{
				$update_data['active'] = (bool) $update_data['active'];
			}

			// Password provided, hash it for storage
			if($this->input->post('group_id') == 7 || $this->input->post('group_id') == 9 || $this->input->post('group_id') == 10){
				$update_data['password'] = $this->input->post('telp');
			}else{
				if ($this->input->post('password'))
				{
					$update_data['password'] = $this->input->post('password');
				}
			}

			// Grab profile data
			$profile_data = array();
			$profile_data['display_name'] = $this->input->post('display_name');
			$profile_data['nip'] = ($update_data['group_id'] != 7 && $update_data['group_id'] != 9 && $update_data['group_id'] != 10) ? $this->input->post('nip') : NULL;
			$profile_data['telp'] = $this->input->post('telp');
			$profile_data['is_complete'] = 1;

			// Add custom profile fields here
			// ...
			//
			// $profile_data['field_name'] = $this->input->post('field_name');

			$id_folder = Files::get_id_by_name('Public');


			if($_FILES['picture_id']['name']!=''){
				$data = Files::upload($id_folder, $_FILES['picture_id']['name'], 'picture_id');
				$profile_data['picture_id'] = $data['data']['id'];
			}

			if($this->input->post('delete_pic')==1) {
				$profile_data['picture_id'] = NULL;
			}

			if ($this->ion_auth->update_user($id, $update_data, $profile_data))
			{
				// Update organization membership
				$organization_memberships = $this->input->post('organization_membership');

				// delete all membership for this user
				$this->memberships_m->delete_membership_by_user($id);

				if($update_data['group_id'] == 8){
					// Update location
					$values['id_provinsi'] = $this->input->post('provinsi');
					$this->db->where('user_id', $id);
					$this->db->update('default_profiles', $values);
				}

				// insert all membership for this user
				if(isset($organization_memberships) AND is_array($organization_memberships)){
					foreach($organization_memberships as $unit_id){
						$this->memberships_m->create_membership($id, $unit_id);
					}
				}

				// Fire an event. A user has been updated.
				Events::trigger('user_updated', $id);

				$this->session->set_flashdata('success', $this->ion_auth->messages());
			}
			else
			{
				$this->session->set_flashdata('error', $this->ion_auth->errors());
			}

			// Redirect back to the form or main page
			if ($this->session->userdata('page')) {
				$page = $this->session->userdata('page');
				$this->session->unset_userdata('page');
			} else {
				$page = '';
			}
			$this->input->post('btnAction') === 'save_exit' ? redirect('admin/users/index/'.$page.'?'.$_SERVER['QUERY_STRING']) : redirect('admin/users/edit/'.$id.'?'.$_SERVER['QUERY_STRING']);
		}
		else
		{
			// Dirty hack that fixes the issue of having to re-add all data upon an error
			if ($_POST)
			{
				$member = (object) $_POST;
			}
		}

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			if ($this->input->post($rule['field']) !== null)
			{
				$member->{$rule['field']} = set_value($rule['field']);
			}
		}

		// Get organizations values
		$root_units = $this->units_m->get_units_by_level(0);
		$units = $this->organization->build_serial_tree(array(), $root_units);

		$memberships = $this->organization->get_membership_by_user($id);

		if (! group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			foreach ($units as $key => $value) {
				$unit_list[$value['id']] = $value;
			}

			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);

			foreach ($memberships_current_user['entries'] as $value) {
				$memberships_unit_list[$value['membership_unit']['id']] = $value['membership_unit']['id'];
			}

			if (isset($memberships_unit_list)) {
				$units = array_intersect_key($unit_list, $memberships_unit_list);

				sort($units);
			}
		}

		$title = sprintf(lang('user:edit_title'), $member->username);
		if($forced=='forced') {
			$this->template->set('forced','forced');
			$title = 'Lengkapi data profil pengguna';
		}

		$this->template
			->title($this->module_details['name'], $title)
			->set('display_name', $member->display_name)
			->set('member', $member)
			->set('units', $units)
			->set('memberships', $memberships)
			->build('admin/form', $data);
	}

	/**
	 * Show a user preview
	 *
	 * @param	int $id The ID of the user.
	 */
	public function preview($id = 0)
	{
		$user = $this->ion_auth->get_user($id);

		$this->template
			->set_layout('modal', 'admin')
			->set('user', $user)
			->build('admin/preview');
	}

	/**
	 * Activate users
	 *
	 * Grabs the ids from the POST data (key: action_to).
	 */
	public function activate()
	{
		// Activate multiple
		if ( ! ($ids = $this->input->post('action_to')))
		{
			$this->session->set_flashdata('error', lang('user:activate_error'));
			redirect('admin/users');
		}

		$activated = 0;
		$to_activate = 0;
		foreach ($ids as $id)
		{
			if ($this->ion_auth->activate($id))
			{
				$activated++;
			}
			$to_activate++;
		}
		$this->session->set_flashdata('success', sprintf(lang('user:activate_success'), $activated, $to_activate));

		if ($this->session->userdata('page')) {
			$page = $this->session->userdata('page');
			$this->session->unset_userdata('page');
		} else {
			$page = '';
		}
		redirect('admin/users/index/'.$page.'?'.$_SERVER['QUERY_STRING']);
	}

	/**
	 * Delete an existing user
	 *
	 * @param int $id The ID of the user to delete
	 */
	public function delete($id = 0)
	{
		// check permission
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {

			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);
			if (count($memberships_current_user['entries']) < 1) {
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}

			$unit_current_user = $memberships_current_user['entries'];

			foreach ($unit_current_user as $key => $value) {
				$unit_lists[] = $value['membership_unit']['id'];
			}

			$memberships_user = $this->organization->get_membership_by_user($id);

			if (count($memberships_user['entries']) < 1) {
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}

			$unit_user = $memberships_user['entries'];

			foreach ($unit_user as $key => $value) {
				$unit_user_lists[] = $value['membership_unit']['id'];
			}

			if (in_array($unit_user_lists, $unit_lists)) {
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		role_or_die('users', 'delete_users');

		if (PYRO_DEMO)
		{
			$this->session->set_flashdata('notice', lang('global:demo_restrictions'));
			redirect('admin/users');
		}

		$ids = ($id > 0) ? array($id) : $this->input->post('action_to');

		if ( ! empty($ids))
		{
			$deleted = 0;
			$to_delete = 0;
			$deleted_ids = array();
			foreach ($ids as $id)
			{
				// Make sure the admin is not trying to delete themself
				if ($this->ion_auth->get_user()->id == $id)
				{
					$this->session->set_flashdata('notice', lang('user:delete_self_error'));
					continue;
				}

				// check laporan datab
				$this->db->where('id_user',$id);
				$laporan_data = $this->db->get('laporan_data');
				if(count($laporan_data)>0) {
					$this->session->set_flashdata('notice', 'User memiliki laporan data');
					continue;
				}

				$this->db->where('created_by',$id);
				$laporan_data = $this->db->get('laporan_sp2d');
				if(count($laporan_data)>0) {
					$this->session->set_flashdata('notice', 'User memiliki dokumen sp2d');
					continue;
				}

				if ($this->ion_auth->delete_user($id))
				{
					$deleted_ids[] = $id;
					$deleted++;
				}
				$to_delete++;
			}

			if ($to_delete > 0)
			{
				// Fire an event. One or more users have been deleted.
				Events::trigger('user_deleted', $deleted_ids);

				$this->session->set_flashdata('success', sprintf(lang('user:mass_delete_success'), $deleted, $to_delete));
			}
		}
		// The array of id's to delete is empty
		else
		{
			$this->session->set_flashdata('error', lang('user:mass_delete_error'));
		}

		if ($this->session->userdata('page')) {
			$page = $this->session->userdata('page');
			$this->session->unset_userdata('page');
		} else {
			$page = '';
		}
		redirect('admin/users/index/'.$page.'?'.$_SERVER['QUERY_STRING']);
	}

	/**
	 * Username check
	 *
	 * @author Ben Edmunds
	 *
	 * @param string $username The username.
	 *
	 * @return bool
	 */
	public function _username_check()
	{
		if ($this->ion_auth->username_check($this->input->post('username')))
		{
			$this->form_validation->set_message('_username_check', lang('user:error_username'));
			return false;
		}
		return true;
	}

	/**
	 * Email check
	 *
	 * @author Ben Edmunds
	 *
	 * @param string $email The email.
	 *
	 * @return bool
	 */
	public function _email_check()
	{
		if ($this->ion_auth->email_check($this->input->post('email')))
		{
			$this->form_validation->set_message('_email_check', lang('user:error_email'));
			return false;
		}

		return true;
	}

	/**
	 * Check that a proper group has been selected
	 *
	 * @author Stephen Cozart
	 *
	 * @param int $group
	 *
	 * @return bool
	 */
	public function _group_check($group)
	{
		if ( ! $this->group_m->get($group))
		{
			$this->form_validation->set_message('_group_check', lang('regex_match'));
			return false;
		}
		return true;
	}

	public function _telp_check()
	{
		if ($this->ion_auth->telp_check($this->input->post('telp')))
		{
			$this->form_validation->set_message('_telp_check', lang('user:error_telp'));
			return false;
		}

		return true;
	}

	function import_user()
	{
		// Check Permission to create and import user
		if (!group_has_role('users', 'create_users') OR !group_has_role('users', 'import_users')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/users');
		}


		// Get Groups
		if (!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			$memberships = $this->organization->get_membership_by_user($this->current_user->id);
			$data['membership_units'] = $memberships['entries'];

			if(count($memberships['entries'])>0) {
				foreach ($memberships['entries'] as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				$units = implode(',', $unit_lists);

				$res_available_groups = $this->group_m->get_available_groups_by_units($unit_lists);
				$available_groups = array();
				foreach ($res_available_groups as $value) {
					$available_groups = array_merge($available_groups, json_decode($value['available_groups']));
				}
				$available_groups = implode(",", $available_groups);

				$group = $this->group_m->where_not_in('name', 'admin')->get_all(array('available_groups' => $available_groups));
			} else {
				$group = $this->group_m->where_not_in('name', 'admin')->get_all();
			}
		} else {
			$group = $this->group_m->where_not_in('name', 'admin')->get_all();
		}

		$row_skipped = 0;
		$row_inserted = 0;
		$error_index = 0;
		$insert = 0;
		$row = 1;
		$err_message = '';
		if($this->input->post()!=null){
			$name = $_FILES['file']['name'];

			if($name!=null){
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$skip_status = $this->input->post('header');
				    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

				        // account fields
				        $values['email'] = $data[0];
				        $values['salt'] = $this->ion_auth_model->salt($data[1]);
				        $values['password'] = $this->ion_auth_model->hash_password($data[1],$values['salt']);
				        $values['username'] = $data[2];
				        $values['group_id'] = ($this->input->post('group_id')) ? $this->input->post('group_id') : NULL;
				        $values['active'] = $this->input->post('active');
				        $values['created_on'] = time();
				        $values['last_login'] = 0;

				        // profile fields
				        // @custom add custom profile data here
				        $values['display_name'] = $data[3];

				        if($skip_status == 'yes'){
				        	$skip_status = 'no';
				        }else{
							$email_check = $this->ion_auth_model->email_check($values['email']);
					        $username_check = $this->ion_auth_model->username_check($values['username']);
					        if($email_check == 0 && $username_check == 0){
					        	$temp[$insert] = $values;
					        	$insert++;
					        }elseif($email_check != 0 || $username_check !=0)
					        {
					        	$error[$error_index] = lang('user:row').' '.$row.' '.lang('user:err_message');
				        		$error_index++;
				        	}
				        }
				        $row++;
				    }

	            	if($error_index != 0){
	            		for ($i = 0; $i < sizeof($error); $i++) {
		            		$err_message .= $error[$i]."<br>";
		            	}
		            	$err_message .= lang('user:err_import');
		            	$this->session->set_flashdata('error', $err_message);
				    	fclose($handle);

				    	redirect('admin/users/import_user');
	            	}else{
	            		for ($i=0; $i < sizeof($temp); $i++) {
	            			$this->db->trans_start();

	            			$retval = $this->ion_auth_model->import_insert($temp[$i]);
	            			if($retval == TRUE){
	            				$user_id = $this->db->insert_id();
	            				$this->ion_auth_model->import_profile($user_id, $temp[$i]);

	            				// set memberships of user which imported in the same unit with current user
	            				// if current user only has permission to manage his own units users

		            			if (!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
		            				$membership_unit = $this->organization->get_membership_by_user($this->current_user->id);
		            				if (count($membership_unit['entries'])>1) {
		            					$data_membership = array('membership_unit' => $this->input->post('membership_unit'), 'membership_user'=>$user_id);
		            				} else if (count($membership_unit['entries'])==1){
			            				$data_membership = array('membership_unit' => $membership_unit['entries'][0]['membership_unit']['id'], 'membership_user' => $user_id);
			            			}

			            			if(isset($data_membership)) {
			            				$this->user_m->set_membership_user($data_membership);
			            			}
		            			}

	            				$row_inserted++;
	            			}

	            			$this->db->trans_complete();
	            		}
	            		if($row_inserted > 0){
		            		if($row_inserted == 1){
								$this->session->set_flashdata('success', $row_inserted.' '.lang('user:scc1_import'));
		            		}else{
		            			$this->session->set_flashdata('success', $row_inserted.' '.lang('user:scc2_import'));
		            		}
		            	}
		            	fclose($handle);

		            	redirect('admin/users/index');
	            	}
				}
			}
		}

		if(isset($data['membership_units'])) {
			$data['units'] = $data['membership_units'];
		} else {
			$data['units'] = array();
		}

		$this->template
			->title($this->module_details['name'], lang('user:add_title'))
			->set('group',$group)
			->build('admin/import_user', $data);
	}

	public function download_contoh(){
		role_or_die('users', 'create_users');

		$this->load->helper('download');
		force_download("example.csv", file_get_contents(base_url().'uploads/user_import_example.csv'));
		redirect('admin/user/import_user');
	}

	public function ajax_get_units_by_kota($id_kota, $id_grup = null, $is_ajax = true, $mode = null, $id_user = null){
		$this->load->library('organization/organization');
		$units_temp = $this->organization->get_units_by_kota($id_kota);
		$units = array();
		foreach ($units_temp as $key => $unit_temp) {
			$available_groups = json_decode($unit_temp['available_groups']);
			if(in_array($id_grup, $available_groups)){
				$units[$key] = $unit_temp;
			}
		}
		$contents = '';
		$organization_membership = array();
		if($mode == 'edit'){
			$organization_membership = $this->memberships_m->get_unit_ids_by_member($id_user);
		}else{
			if($this->input->post('organization_membership')) {
				$organization_membership = $this->input->post('organization_membership');
			}
		}
		$selected = '';
		if(count($units) > 0){
			foreach ($units as $key => $unit) {
				if(!$is_ajax){
					$selected = '';
					if(in_array($unit['id'], $organization_membership)) {
						$selected = 'checked';
					}
				}
				
				$contents.= '
				<div class="checkbox">
					<label class="unit_list" data-unit-type="7">
						<input name="organization_membership[]" '.$selected.' value="'.$unit['id'].'" class="ace cb_unit" type="checkbox" id="unit_'.$unit['id'].'" data-id="'.$unit['id'].'" data-unit="'.$unit['id'].'" data-parent="0" data-unit-type="'.$unit['unit_type'].'" onclick="get_unit()">
							<span class="lbl" style="margin-left:0px">
								'.$unit['unit_name'].'
							</span>
					</label>
				</div>';

				if(count($unit['childs']) > 0){
					foreach ($unit['childs'] as $key => $child) {
						if(!$is_ajax){
							$selected = '';
							if(in_array($child['id'], $organization_membership)) {
								$selected = 'checked';
							}
						}
						$contents.= '
						<div class="checkbox">
							<label class="unit_list" data-unit-type="7">
								<input name="organization_membership[]" '.$selected.' value="'.$child['id'].'" class="ace cb_unit" type="checkbox" id="unit_'.$child['id'].'" data-id="'.$unit['id'].'" data-unit="'.$child['id'].'" data-parent="'.$unit['id'].'" data-unit-type="'.$unit['unit_type'].'" onclick="get_unit()">
								<span class="lbl" style="margin-left:20px">
									'.$child['unit_name'].'
								</span>
							</label>
						</div>';
					}
				}
			}
		}else{
			$contents = '-';
		}

		if($is_ajax){
			echo $contents;
		}else{
			return $contents;
		}
	}
}
