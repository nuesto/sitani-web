<style type="text/css">
	#picture {
		position: relative;
		display: inline-block;
	}
	#delete-pic {
		position: absolute;
		top: -5px;
		right: -9px;
		font: 20px bold;
		width: 25px;
		height: 25px;
		background: #E1DADA;
		text-align: center;
		line-height: 26px;
		border-radius: 50%;
		color: #000000;
		cursor: pointer;
	}
</style>

<?php $permission = ($mode == 'edit') ? 'edit_all_account' : 'create_users'; ?>
<div class="page-header">
	<?php if ($this->method === 'create'): ?>
		<h1><?php echo lang('user:add_title') ?></h1>
	<?php else: ?>
		<h1><?php $title = sprintf(lang('user:edit_title'), $member->username);
		if($forced=='forced') {
			$title = 'Lengkapi data profil pengguna';
		}
		echo $title; ?></h1>
	<?php endif ?>
</div>

<?php if ($this->method === 'create'): ?>
	<?php echo form_open_multipart(uri_string().'?'.$_SERVER['QUERY_STRING'], 'class="form-horizontal" autocomplete="off"') ?>
<?php else: ?>
	<?php echo form_open_multipart(uri_string().'?'.$_SERVER['QUERY_STRING'], 'class="form-horizontal"') ?>
	<?php echo form_hidden('row_edit_id', isset($member->row_edit_id) ? $member->row_edit_id : $member->profile_id); ?>
<?php endif ?>

<?php
$is_create_mode = FALSE;
$is_own_user = FALSE;

// check if in edit mode?
if($this->method == 'create'){
	$is_create_mode = TRUE;
}else{
	// check if user being edited is current user?
	$member_id = isset($member->id) ? $member->id : $member->row_edit_id;
	if($this->current_user->id == $member_id){
		$is_own_user = TRUE;
	}
}
?>
	
<!-- Content tab -->
<?php 
if($is_create_mode
	OR group_has_role('users', 'edit_all_account') 
	OR (group_has_role('users', 'edit_own_account') AND $is_own_user)){ 
?>

<fieldset>
	<div class="form-group">
	<h3 class="header smaller lighter blue col-sm-12"><?php echo lang('user_account_data_label'); ?></h3>
	</div>
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_group') 
		OR (group_has_role('users', 'edit_own_group') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:group_label') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php 
			if($member->group_id != NULL AND $member->group_id != ''){
				$group_id = $member->group_id;
			}else{
				$group_id = $this->input->get('f_group');
			}
			echo form_dropdown('group_id', array('' => lang('global:select-pick')) + $groups_select, $group_id, 'id="group_id"') 
			?>
		</div>
	</div>
	<?php }else{ 
		echo '<input type="hidden" name="group_id" id="group_id" value="'.$member->group_id.'">';
	} ?>

	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_email') 
		OR (group_has_role('users', 'edit_own_email') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:email') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('email', $member->email, 'id="email"') ?>
		</div>
	</div>
	<?php }else{ 
		echo form_hidden('email', $member->email);
	} ?>
	
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_username') 
		OR (group_has_role('users', 'edit_own_username') AND $is_own_user)){ 
	?>
	<div class="form-group" id="username" <?php echo ($group_id == 7 || $group_id == 9 || $group_id == 10) ? 'style="display:none;"' : '' ?>>
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:username') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('username', $member->username, 'id="username"') ?>
		</div>
	</div>
	<?php }else{ 
		echo '<input type="hidden" name="username" id="username" value="'.$member->username.'">';
	} ?>
	
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_password') 
		OR (group_has_role('users', 'edit_own_password') AND $is_own_user)){ 
	?>
	<div class="form-group" id="password" <?php echo ($group_id == 7 || $group_id == 9 || $group_id == 10) ? 'style="display:none;"' : '' ?>>
		<label class="col-sm-2 control-label no-padding-right">
			<?php echo lang('global:password') ?>
			<?php if ($this->method == 'create'): ?> <span>*</span><?php endif ?>
		</label>
		<div class="col-sm-10">
			<?php echo form_password('password', '', 'id="password" autocomplete="off"') ?>
		</div>
	</div>
	<?php }else{ 
		echo '<input type="hidden" name="password" id="password" value="">';
	} ?>

	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_status') 
		OR (group_has_role('users', 'edit_own_status') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:activate_label') ?></label>
		<div class="col-sm-10">
			<?php $options = array(0 => lang('user:do_not_activate'), 1 => lang('user:active'), 2 => lang('user:send_activation_email')) ?>
			<?php echo form_dropdown('active', $options, $member->active, 'id="active"') ?>
		</div>
	</div>
	<?php }else{ 
		echo '<input type="hidden" name="active" id="active" value="'.$member->active.'">';
	} ?>

</fieldset>	

<?php }else{ 

echo '<input type="hidden" name="email" id="email" value="'.$member->email.'">';
echo '<input type="hidden" name="username" id="username" value="'.$member->username.'">';
if($member->group_id != NULL AND $member->group_id != ''){
	$group_id = $member->group_id;
}else{
	$group_id = $this->input->get('f_group');
}
echo '<input type="hidden" name="group_id" id="group_id" value="'.$group_id.'">';
echo '<input type="hidden" name="active" id="active" value="'.$member->active.'">';
echo form_hidden('password', '');

} //if ?>

<?php 
if($is_create_mode
	OR group_has_role('users', 'edit_all_profile') 
	OR (group_has_role('users', 'edit_own_profile') AND $is_own_user)){ 
?>

<fieldset>
	<div class="form-group">
	<h3 class="header smaller lighter blue col-sm-12"><?php echo lang('profile_user_details_label'); ?></h3>
	</div>
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_displayname') 
		OR (group_has_role('users', 'edit_own_displayname') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('profile_display_name') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('display_name', $display_name, 'id="display_name"') ?>
		</div>
	</div>
	<?php }else{ 
		echo form_hidden('display_name',  $member->display_name);
	} ?>

	<div class="form-group" id="nip" <?php echo ($group_id == 7 || $group_id == 9 || $group_id == 10) ? 'style="display:none;"' : '' ?>>
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('profile_nip') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('nip', $member->nip, 'id="nip"') ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('profile_telp') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('telp', $member->telp, 'id="telp"') ?>
		</div>
	</div>


	<?php if($this->session->userdata('group_id') != 7) { ?>

		<div id="location" <?php echo ($group_id != 8 && $group_id !=7 && $group_id !=9 && $group_id !=10) ? 'style="display:none;"' : '' ?>>

			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?> <span>*</span></label>

				<div class="col-sm-6">
					<?php
					if(!group_has_role('users', $permission)){ ?>
		        <input type="text" value="<?php echo $nama_provinsi; ?>" readonly>
		        <input type="hidden" value="<?php echo $id_provinsi; ?>" name="provinsi" readonly>
		      <?php }else{ ?>
						<?php 
							$val_prov = $id_provinsi;
							if($this->input->post('provinsi') != NULL){
								$val_prov = $this->input->post('provinsi');
							}elseif($mode == 'edit'){
								$val_prov = $fields['id_provinsi'];
							}
						?>
						<select name="provinsi" id="provinsi" class="col-xs-10 col-sm-5">
			    		<option value=""><?php echo lang('global:select-pick') ?></option>
			    		<?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
			    			<option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
			    		<?php } ?>
			    	</select>
			    <?php } ?>
				</div>
			</div>

			<div class="form-group" id="content_kota" <?php echo ($group_id !=7 && $group_id !=9 && $group_id !=10) ? 'style="display:none;"' : '' ?>>
				<label class="col-sm-2 control-label no-padding-right" for="kota"><?php echo lang('location:kota:singular'); ?> <span>*</span></label>

				<div class="col-sm-6">
					<?php
						$value = $id_kota;
						if($this->input->post('kota') != NULL){
							$value = $this->input->post('kota');
						}elseif($mode == 'edit'){
							$value = $fields['id_kota'];
						}
					?>
					<select name="kota" id="kota" class="col-xs-10 col-sm-5">
		    		<?php 
		    			if($val_prov != NULL || !group_has_role('users', $permission)) {
		    		?>
		    			<option value=""><?php echo lang('global:select-pick') ?></option>
		    		<?php 
		      			foreach ($kota['entries'] as $kota_entry){ ?>
		      				<option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
		      	<?php 
		      			} 
		      		}else{
		      	?>
		      			<option value=""><?php echo lang('global:select-none') ?></option>
		      	<?php
		      		}
		      	?>
		    	</select>
		    	<span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>

		    	<script type="text/javascript">
		    		<?php if($val_prov == NULL) { ?>
                $(document).ready(function(){
                    $("#provinsi").change();
                })
            <?php } ?>
		        $('#provinsi').change(function() {
              $("#id_unit").html('<option value=""><?php echo  lang("global:select-none") ?></option>');
              var id_provinsi = $(this).val();
              $("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
              $('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
              $.ajax({
                url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                dataType: 'json',
                success: function(data){
                  if(data.length > 0){
                    $('#kota').html('<option value="">-- Pilih --</option>');
                  }else{
                    $('#kota').html('<option value="">-- Tidak ada --</option>');
                  }
                  $.each(data, function(i, object){
                    $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                  });

                  $("#kota").change();
                	$('.loading-kota').html('');
                }
              });
            });

            $('#kota').change(function() {
              load_units();
            });

		    	</script>
				</div>
			</div>
		</div>
	<?php  } ?>
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_organization_unit') 
		OR (group_has_role('users', 'edit_own_organization_unit') AND $is_own_user)){ 
	?>
	<div class="form-group" id="units" <?php echo ($group_id != 7 && $group_id !=9 && $group_id !=10) ? 'style="display:none;"' : '' ?>>
		<label class="col-sm-2 control-label no-padding-right">
			<?php echo lang('organization:units:plural'); ?>
		</label>
		<div class="col-sm-10" id="load_units">
			<?php echo $contents_units ?>
		</div>
	</div>
	<div class="form-group" id="empty_units" <?php echo ($mode !='edit' || ($group_id != 7 && $group_id !=9 && $group_id !=10)) ? 'style="display:none;"' : ''; ?>>
		
		<div class="col-sm-10 col-sm-offset-2">
			<span id="btnClearOrganization" class="btn btn-xs btn-yellow"><?php echo lang('user:clear_organization'); ?></span>
		</div>
	</div>
	<?php }else{
		die();
		foreach($memberships['entries'] as $membership){
			echo form_hidden('organization_membership[]', $membership['membership_unit']['id']);
		}
	} ?>

</fieldset>

<?php }else{



} //if ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-sm btn-info" value="save_exit" name="btnAction" type="submit">
			<span><?php echo lang('buttons:save_exit'); ?></span>
		</button>
		
		<?php if ($this->method == 'create'){ ?>
		
		<a class="btn btn-sm btn-info cancel" href="<?php echo base_url(); ?>admin/users?<?php echo $_SERVER['QUERY_STRING']; ?>"><?php echo lang('buttons:cancel'); ?></a>
		
		<?php }else{ ?>
		
		<a class="btn btn-sm btn-info cancel" href="<?php echo base_url(); ?>admin/users/index?<?php echo $_SERVER['QUERY_STRING']; ?>"><?php echo lang('buttons:cancel'); ?></a>
		
		<?php } ?>
	</div>
</div>

<?php echo form_close() ?>

<script type="text/javascript">
	jQuery(function($) {
		$(".chosen-select").chosen();
		$('#chosen-multiple-style').on('click', function(e){
			var target = $(e.target).find('input[type=radio]');
			var which = parseInt(target.val());
			if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
			 else $('#form-field-select-4').removeClass('tag-input-style');
		});
	});

	// check_available_group($('#group_id').val());
	function check_available_group(group_id) {
		$('.unit_list').each(function(key,value) {
			var unitType = $(this).data('unitType').toString().split(',');
			// alert(group_id);
			if(group_id!='' && $.inArray(group_id, unitType) < 0 && group_id != 1) {
				$(this).find('input').attr('disabled',true);
			} else {
				$(this).find('input').removeAttr('disabled');
			}
		});
	}

	$(document).ready(function() {
		$('#group_id').change(function() {
			// check_available_group($(this).val());
			load_units();

			var group_id = $(this).val();
			if(group_id == 7 || group_id == 9 || group_id == 10){
				$('#username, #password, #nip').hide();
				$('#username input, #password input, #nip input').val('');
				$('#units').show();
			}else{
				$('#username, #password, #nip').show();
				$('#units, #empty_units').hide();
			}

			if(group_id == 8 || group_id == 7 || group_id == 9 || group_id == 10){
				$('#location').show();
			}else{
				$('#location').hide();
			}

			if(group_id == 8){
				$('#content_kota').hide();
			}else{
				$('#content_kota').show();
			}
		});

		$('#btnClearOrganization').click(function() {
			clear_organization();
		});

		get_unit();
	});

	function clear_organization() {
		$('.unit_list').each(function(key,value) {
			$(this).find('input').removeAttr('checked');
			$(this).find('input').removeAttr('disabled');
		});
	}

	$(document).ready(function() {
		$('#delete-pic').click(function() {
			$('#picture').after('<input type="hidden" name="delete_pic" value="1">');
			$('#picture').remove();
		});

		
	});

	function get_unit(){
		var jml_check = 0;
		$(".cb_unit").each(function(){
			if($(this).is(":checked")){
				var id_unit = $(this).attr('data-unit');
				var id_parent = $(this).attr('data-parent');
				cek_unit(id_unit, id_parent);
				jml_check++;
			}
		});

		if(jml_check == 0){
			clear_organization();
		}
	}

	function cek_unit(id_checked, parent_id){
		var parent_id2 = (parent_id == 0) ? id_checked : parent_id;

		if($("#unit_"+id_checked).is(":checked")){
			$(".cb_unit").each(function(){
				$(this).checked = false;
				var dataid = $(this).attr('data-id');
				var unittype = $(this).attr('data-unit-type');
				id = dataid;
				if(id != parent_id2 && unittype != 5 && unittype != 7){
					$("input[data-id="+dataid+"]").each(function(){
						// $(this).attr('disabled','disabled');
					})
				}
			});
		}else{
			$(".cb_unit").each(function(){
				var dataid = $(this).attr('data-id');
				id = dataid;
				if(id != parent_id2){
					$("input[data-id="+dataid+"]").each(function(){
						if(parent_id == 0){
							$(this).removeAttr('disabled');
						}
						// $(this).checked = false;
					})
				}
			});
		}
	}

	function load_units(){
  	var kota = ($("#kota").val() == '') ? 0 : $('#kota').val();
    var group_id = ($("#group_id").val() == '') ? 0 : $('#group_id').val();
		$("#load_units").html('<i class="icon-spinner icon-spin orange bigger-150"></i>');
    $("#load_units").load("<?php echo site_url('admin/users/ajax_get_units_by_kota') ?>" + '/' + kota + '/' +group_id, function(data) {
    	if(data == '-' || data == ''){
    		$("#empty_units").hide();
    	}else{
    		$("#empty_units").show();
    	}

    });
	}
</script>