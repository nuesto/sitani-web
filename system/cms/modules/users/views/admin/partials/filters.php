<?php echo form_open('',  array('method' => 'get')) ?>
	<?php echo form_hidden('f_module', $module_details['slug']) ?>	


	<?php 
	$root_units = $this->units_m->get_units_by_level(0);
	$units = $this->organization->build_serial_tree(array(), $root_units);
	?>

	<?php if(group_has_role('users', 'manage_users')) { ?>
	<div class="form-group">
		<div class="col-md-2">
		<label><?php echo lang('organization:units:singular', 'f_unit') ?></label>
		</div>
		<div class="col-md-3">
		<select name="f_unit" class="form-control">
			<option value=""><?php echo lang('organization:units:select'); ?></option>

			<?php foreach($units as $unit){ ?>
			<option <?php if($unit['id'] == $this->input->get('f_unit')){echo 'selected';} ?> value="<?php echo $unit['id']; ?>">
				<?php 
				if(isset($unit['parent_id'])) {
					for($n = 1; $n<=$unit['type_level']; $n++) {
						echo '&nbsp;&nbsp;&nbsp;'; 
					}
					echo '>>&nbsp;';
				}

				echo $unit['unit_name'];
				?>
			</option>

			<?php } ?>
		</select>
		</div>
	</div>
	<?php } elseif (!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) { ?>
	<?php if (isset($unit_lists) AND count($unit_lists)>1) { ?>
	<div class="form-group">
		<div class="col-md-3">
		<label><?php echo lang('organization:units:singular', 'f_unit') ?></label>
		</div>
		<div class="col-md-3">
		<select name="f_unit" class="form-control">
			<option value=""><?php echo lang('organization:units:select'); ?></option>

			<?php foreach($units as $unit){ 
				if(in_array($unit['id'], $unit_lists)) {
					if ($unit['id'] == $this->input->get('f_unit')) {
						echo '<option value="'.$unit['id'].'" selected="selected">'.$unit['unit_name'].'</option>';
					} else {
						echo '<option value="'.$unit['id'].'">'.$unit['unit_name'].'</option>';
					}
				}
			} ?>
		</select>
		</div>
	</div>
	<?php } ?>
	<?php } ?>
	<div class="form-group">
		<div class="col-md-1"><label><?php echo lang('user:active', 'f_active') ?></label></div>
		<div class="col-md-2"><?php echo form_dropdown('f_active', array(0 => lang('global:select-all'), 1 => lang('global:yes'), 2 => lang('global:no') ), $this->input->get('f_active'), 'class="form-control"') ?>
		</div>
	</div>

	<?php
	$active_group_id = $this->input->get('f_group');
	echo form_hidden('f_group', $active_group_id);
	?>
	
	<div class="form-group">
		<div class="col-md-2"><label for="f_keywords"><?php echo lang('global:keywords') ?></label></div>
		<div class="col-md-2"><?php echo form_input(array('class'=> 'form-control', 'name'=>'f_keywords'), $this->input->get('f_keywords')) ?></div>
	</div>

	<div class="form-group col-md-12" >
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
			<i class="icon-filter"></i>
			<?php echo lang('global:filters'); ?>
		</button>
		
		<a href="<?php echo site_url('admin/users').'?f_group='.$active_group_id; ?>" class="btn btn-xs">
			<i class="icon-remove"></i>
			<?php echo lang('buttons:clear'); ?>
		</a>
	</div>
<?php echo form_close() ?>