<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Phil Sturgeon
 */
// ------------------------------------------------------------------------

/**
 * Checks to see if a user is logged in or not.
 * 
 * @access public
 * @return bool
 */
function is_logged_in()
{
    return (isset(get_instance()->current_user->id)) ? true : false; 
}

// ------------------------------------------------------------------------

/**
 * Checks if a group has access to module or role
 *
 * @access public
 * @param string $module sameple: pages
 * @param string $role sample: put_live
 * @return bool
 */
function group_has_role($module, $role, $group_id = NULL)
{
	if(is_null($group_id)){
		if (empty(ci()->current_user))
		{
			return false;
		}

		if (ci()->current_user->group == 'admin')
		{
			return true;
		}
		$group_id = ci()->current_user->group_id;
	}

	if ($group_id==1)
	{
		return true;
	}

	$permissions = ci()->permission_m->get_group($group_id);

	if (empty($permissions[$module]) or empty($permissions[$module][$role]))
	{
		return false;
	}

	return true;
}

// ------------------------------------------------------------------------

/**
 * Checks if a group has access to module or role
 *
 * @access public
 * @param string $module sameple: pages
 * @param string $role sample: put_live
 * @return bool
 */
function group_has_access($module, $action, $stream_slug, $entry_object)
{
	if(group_has_role($module, $action . '_all_' . $stream_slug)){
		return TRUE;
	}elseif(group_has_role($module, $action . '_own_' . $stream_slug)){
		if ($entry_object->created_by_user_id == ci()->current_user->id){
			return TRUE;
		}
	}

	return FALSE;
}

// ------------------------------------------------------------------------

/**
 * Checks if role has access to module or returns error 
 * 
 * @access public
 * @param string $module sample: pages
 * @param string $role sample: edit_live
 * @param string $redirect_to (default: 'admin') Url to redirect to if no access
 * @param string $message (default: '') Message to display if no access
 * @return mixed
 */
function role_or_die($module, $role, $redirect_to = 'admin', $message = '')
{
	ci()->lang->load('admin');

	if (ci()->input->is_ajax_request() and ! group_has_role($module, $role))
	{
		echo json_encode(array('error' => ($message ? $message : lang('cp:access_denied')) ));
		return false;
	}
	elseif ( ! group_has_role($module, $role))
	{
		ci()->session->set_flashdata('error', ($message ? $message : lang('cp:access_denied')) );
		redirect($redirect_to);
	}
	return true;
}

// ------------------------------------------------------------------------

/**
 * Return a users display name based on settings
 *
 * @param int $user the users id
 * @param string $linked if true a link to the profile page is returned, 
 *                       if false it returns just the display name.
 * @return  string
 */
function user_displayname($user, $linked = true, $admin_link = false)
{
    ci()->lang->load('users/user');

    // User is numeric and user hasn't been pulled yet isn't set.
    if (is_numeric($user))
    {
        $user = ci()->ion_auth->get_user($user);
    }

    if(!isset($user)) {
    	return lang('user:not_found');
    }

    $user = (array) $user;
    $name = empty($user['display_name']) ? $user['username'] : $user['display_name'];

    // Static var used for cache
    if ( ! isset($_users))
    {
        static $_users = array();
    }

    // check if it exists
    if (isset($_users[$user['id']]))
    {
        if( ! empty( $_users[$user['id']]['profile_link'] ) and $linked)
        {
        	return $_users[$user['id']]['profile_link'];
        }
        else
        {
            return $name;
        }
    }

    // Set cached variable.
    if (ci()->settings->enable_profiles and $linked)
    {
    	if($admin_link){
			$_users[$user['id']]['profile_link'] = '<a href="'.site_url('admin/users/view/'.$user['id']).'">'.$name.'</a>';
		}else{
			$_users[$user['id']]['profile_link'] = anchor('user/'.$user['id'], $name);
		}
        return $_users[$user['id']]['profile_link'];
    }

    // Not cached, Not linked. get_user caches the result so no need to cache non linked
    return $name;
}

function user_groups($user_id)
{
	ci()->load->model('users/user_m');

	$user = ci()->user_m->get(array('id' => $user_id));
	echo $user->group_description;
}

function user_has_group($group_short_name)
{
	ci()->load->model('users/user_m');

	$user = ci()->user_m->get(array('id' => ci()->current_user->id));

	if($user->group_name == $group_short_name){
		return TRUE;
	}else{
		return FALSE;
	}
}

function user_units($user_id)
{
	ci()->load->model('organization/memberships_m');

	$unit = ci()->memberships_m->get_one_unit_by_member($user_id);
	return $unit['id'];
}

function user_units2($user_id)
{
	ci()->load->model('organization/memberships_m');

	$units = ci()->memberships_m->get_units_by_member($user_id);

	$arr_unit = array();
	foreach ($units as $key => $unit) {
		$arr_unit[] = $unit['id'];
	}
	
	$result = implode(',',$arr_unit);
	return $result;
}

function user_cek_entries($user_id, $id_organization_unit)
{
	ci()->load->model('organization/units_m');

	$unit_user = user_units($user_id);
	$result = ci()->units_m->get_child($unit_user);
	$units_id = $unit_user.$result;
	$arr_units = explode(",",$units_id);
	foreach ($arr_units as $value) {
		if($value == $id_organization_unit){
			return TRUE;
		}
	}
}

function user_provinsi($user_id){
	ci()->load->model('users/user_m');

	$user = ci()->user_m->get_provinsi_by_id($user_id);
	return $user->id_provinsi;
}

                        /**
 * Checks if a group has access to module or role
 *
 * @access public
 * @param array $api sample: array('user'=>'username', 'key'=>'api_key')
 * @param string $module sample: pages
 * @param string $role sample: put_live
 * @return bool
 */
function api_has_role($api, $module, $role)
{
	ci()->load->model('users/user_m');
	$user = ci()->user_m->get_user_by_api($api['user'], $api['key']);

	if(is_array($user)){
		$auth = group_has_role($module, $role, $user['group_id']);
		if($auth){
			return true;
		}
	}
	return false;
}

/* End of file users/helpers/user_helper.php */