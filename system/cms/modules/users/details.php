<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Users Module
 *
 * @author PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Users
 */
class Module_Users extends Module {


	public $version = '1.4.4';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Users',
				'ar' => 'المستخدمون',
				'br' => 'Usuários',
				'pt' => 'Utilizadores',
				'cs' => 'Uživatelé',
				'da' => 'Brugere',
				'de' => 'Benutzer',
				'el' => 'Χρήστες',
				'es' => 'Usuarios',
				'fa' => 'کاربران',
				'fi' => 'Käyttäjät',
				'fr' => 'Utilisateurs',
				'he' => 'משתמשים',
				'id' => 'Pengguna',
				'it' => 'Utenti',
				'lt' => 'Vartotojai',
				'nl' => 'Gebruikers',
				'pl' => 'Użytkownicy',
				'ru' => 'Пользователи',
				'sl' => 'Uporabniki',
				'tw' => '用戶',
				'cn' => '用户',
				'hu' => 'Felhasználók',
				'th' => 'ผู้ใช้งาน',
				'se' => 'Användare'
				),
			'description' => array(
				'en' => 'Let users register and log in to the site, and manage them via the control panel.',
				'ar' => 'تمكين المستخدمين من التسجيل والدخول إلى الموقع، وإدارتهم من لوحة التحكم.',
				'br' => 'Permite com que usuários se registrem e entrem no site e também que eles sejam gerenciáveis apartir do painel de controle.',
				'pt' => 'Permite com que os utilizadores se registem e entrem no site e também que eles sejam geriveis apartir do painel de controlo.',
				'cs' => 'Umožňuje uživatelům se registrovat a přihlašovat a zároveň jejich správu v Kontrolním panelu.',
				'da' => 'Lader brugere registrere sig og logge ind på sitet, og håndtér dem via kontrolpanelet.',
				'de' => 'Erlaube Benutzern das Registrieren und Einloggen auf der Seite und verwalte sie über die Admin-Oberfläche.',
				'el' => 'Παρέχει λειτουργίες εγγραφής και σύνδεσης στους επισκέπτες. Επίσης από εδώ γίνεται η διαχείριση των λογαριασμών.',
				'es' => 'Permite el registro de nuevos usuarios quienes podrán loguearse en el sitio. Estos podrán controlarse desde el panel de administración.',
				'fa' => 'به کاربر ها امکان ثبت نام و لاگین در سایت را بدهید و آنها را در پنل مدیریت نظارت کنید',
				'fi' => 'Antaa käyttäjien rekisteröityä ja kirjautua sisään sivustolle sekä mahdollistaa niiden muokkaamisen hallintapaneelista.',
				'fr' => 'Permet aux utilisateurs de s\'enregistrer et de se connecter au site et de les gérer via le panneau de contrôle',
				'he' => 'ניהול משתמשים: רישום, הפעלה ומחיקה',
				'id' => 'Memungkinkan pengguna untuk mendaftar dan masuk ke dalam situs, dan mengaturnya melalui control panel.',
				'it' => 'Fai iscrivere de entrare nel sito gli utenti, e gestiscili attraverso il pannello di controllo.',
				'lt' => 'Leidžia vartotojams registruotis ir prisijungti prie puslapio, ir valdyti juos per administravimo panele.',
				'nl' => 'Laat gebruikers registreren en inloggen op de site, en beheer ze via het controlepaneel.',
				'pl' => 'Pozwól użytkownikom na logowanie się na stronie i zarządzaj nimi za pomocą panelu.',
				'ru' => 'Управление зарегистрированными пользователями, активирование новых пользователей.',
				'sl' => 'Dovoli uporabnikom za registracijo in prijavo na strani, urejanje le teh preko nadzorne plošče',
				'tw' => '讓用戶可以註冊並登入網站，並且管理者可在控制台內進行管理。',
				'cn' => '让用户可以注册并登入网站，并且管理者可在控制台内进行管理。',
				'th' => 'ให้ผู้ใช้ลงทะเบียนและเข้าสู่เว็บไซต์และจัดการกับพวกเขาผ่านทางแผงควบคุม',
				'hu' => 'Hogy a felhasználók tudjanak az oldalra regisztrálni és belépni, valamint lehessen őket kezelni a vezérlőpulton.',
				'se' => 'Låt dina besökare registrera sig och logga in på webbplatsen. Hantera sedan användarna via kontrollpanelen.'
				),
			'frontend' 	=> false,
			'backend'  	=> true,
			'menu'	  	=> 'users',
			'roles'		=> array(
				'manage_users',
				'manage_own_unit_users',
				'manage_user_profile_fields',
				'create_users',
				'import_users',
				'delete_users',

				// accounts
				'view_all_account',
				'view_own_account',
				'edit_all_account',
				'edit_own_account',
				'edit_all_email',
				'edit_all_username',
				'edit_all_group',
				'edit_all_status',
				'edit_all_password',
				'edit_own_email',
				'edit_own_username',
				'edit_own_group',
				'edit_own_status',
				'edit_own_password',

				// profile
				'view_all_profile',
				'view_own_profile',
				'edit_all_profile',
				'edit_own_profile',
				'edit_all_displayname',
				'edit_own_displayname',
				'edit_all_picture',
				'edit_own_picture',

				// organization
				'edit_all_organization_unit',
				'edit_own_organization_unit',
				)
			);

if (function_exists('group_has_role'))
{
	if(group_has_role('users', 'manage_users') OR group_has_role('users', 'manage_own_unit_users'))
	{
		$info['sections']['users'] = array(
			'name' 	=> 'user:list_title',
			'uri' 	=> 'admin/users/index',
			);

		if(group_has_role('users', 'create_users')){
			$info['sections']['users']['shortcuts'] = array(
				'create' => array(
					'name' 	=> 'user:add_title',
					'uri' 	=> 'admin/users/create',
					'class' => 'add',
					)
				);
		}
	}

	if(group_has_role('users', 'manage_user_profile_fields'))
	{
		$info['sections']['fields'] = array(
			'name' 	=> 'user:profile_fields_label',
			'uri' 	=> 'admin/users/fields',
			'shortcuts' => array(
				'create' => array(
					'name' 	=> 'user:add_field',
					'uri' 	=> 'admin/users/fields/create',
					'class' => 'add'
					)
				)
			);
	}
}

return $info;
}

public function admin_menu(&$menu,&$order)
{

	$users_label = lang('user:list_title');
	$profiles_label = lang('user:profile_fields_label');
	$menu['lang:cp:nav_users'][$users_label] = array();

		// // unset($menu["lang:cp:nav_users"][$users_label]);

	if(group_has_role('users', 'manage_users') OR group_has_role('users', 'manage_own_unit_users')) {
		$menu["lang:cp:nav_users"][$users_label][$users_label]['urls'] = array(
			'admin/users/index','admin/users/import_user','admin/users/view','admin/users/view%1','admin/users/create','admin/users/edit%1');
	}

	if(group_has_role('users', 'manage_user_profile_fields')) {
		$menu["lang:cp:nav_users"][$users_label][$profiles_label]['urls'] = array(
			'admin/users/fields','admin/users/fields/view%1','admin/users/fields/create','admin/users/fields/edit%1');
	}

		// if(! group_has_role('users', 'manage_users') AND ! group_has_role('users', 'manage_own_unit_users')){

		// }
}

	/**
	 * Installation logic
	 *
	 * This is handled by the installer only so that a default user can be created.
	 *
	 * @return boolean
	 */
	public function install()
	{
		// Load up the streams driver and convert the profiles table
		// into a stream.
		$this->load->driver('Streams');

		if ( ! $this->streams->utilities->convert_table_to_stream('profiles', 'users', null, 'lang:user_profile_fields_label', 'Profiles for users module', 'display_name', array('display_name')))
		{
			return false;
		}

		// Go ahead and convert our standard user fields:
		$columns = array();

		// Run through each column and add the field
		// metadata to it.
		foreach ($columns as $field_slug => $column)
		{
			// We only want fields that actually exist in the
			// DB. The user could have deleted some of them.
			if ($this->db->field_exists($field_slug, 'profiles'))
			{
				$extra = array();
				$assign = array();

				if (isset($column['extra']))
				{
					$extra = $column['extra'];
				}

				if (isset($column['assign']))
				{
					$assign = $column['assign'];
				}

				$this->streams->utilities->convert_column_to_field('profiles', 'users', $column['field_name'], $field_slug, $column['field_type'], $extra, $assign);

				unset($extra);
				unset($assign);
			}
		}

		// Install the settings
		$settings = array(
			array(
				'slug' => 'auto_username',
				'title' => 'Auto Username',
				'description' => 'Create the username automatically, meaning users can skip making one on registration.',
				'type' => 'radio',
				'default' => true,
				'value' => '',
				'options' => '1=Enabled|0=Disabled',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'users',
				'order' => 964,
				),
			array(
				'slug' => 'enable_profiles',
				'title' => 'Enable profiles',
				'description' => 'Allow users to add and edit profiles.',
				'type' => 'radio',
				'default' => true,
				'value' => '',
				'options' => '1=Enabled|0=Disabled',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'users',
				'order' => 963,
				),
			array(
				'slug' => 'activation_email',
				'title' => 'Activation Email',
				'description' => 'Send out an e-mail with an activation link when a user signs up. Disable this so that admins must manually activate each account.',
				'type' => 'select',
				'default' => true,
				'value' => '',
				'options' => '0=activate_by_admin|1=activate_by_email|2=no_activation',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'users',
				'order' => 961,
				),
			array(
				'slug' => 'registered_email',
				'title' => 'User Registered Email',
				'description' => 'Send a notification email to the contact e-mail when someone registers.',
				'type' => 'radio',
				'default' => true,
				'value' => '',
				'options' => '1=Enabled|0=Disabled',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'users',
				'order' => 962,
				),
			array(
				'slug' => 'enable_registration',
				'title' => 'Enable user registration',
				'description' => 'Allow users to register in your site.',
				'type' => 'radio',
				'default' => true,
				'value' => '',
				'options' => '1=Enabled|0=Disabled',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'users',
				'order' => 961,
				),
			array(
				'slug' => 'profile_visibility',
				'title' => 'Profile Visibility',
				'description' => 'Specify who can view user profiles on the public site',
				'type' => 'select',
				'default' => 'public',
				'value' => '',
				'options' => 'public=profile_public|owner=profile_owner|hidden=profile_hidden|member=profile_member',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'users',
				'order' => 960,
				),
			);

		foreach ($settings as $setting)
		{
			if ( ! $this->db->insert('settings', $setting))
			{
				return false;
			}
		}

		return true;
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		return false;
	}

	public function upgrade($old_version)
	{
		switch ($old_version) {
			case '1.1.0':
				// add fields

			$fields = array(
				'picture_id' => array(
					'type' => 'CHAR',
					'constraint' => 15,
					'null' => true,
					),
				);

			$this->dbforge->add_column('profiles', $fields);

			case '1.1.1':
				// add fields

			$fields = array(
				'is_complete' => array(
					'type' => 'TINYINT',
					'default' => 0,
					),
				);

			$this->dbforge->add_column('profiles', $fields);

			case '1.1.2':
			$this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." ADD INDEX(`membership_user`);");

	        	// stop at this version
				// to continue please upgrade again
			$this->version = '1.1.3';
			break;

			case '1.1.3':
				// convert all tables to InnoDB engine
			$this->db->query("ALTER TABLE `core_users` ENGINE=InnoDB");
			$this->db->query("ALTER TABLE ".$this->db->dbprefix('users')." ENGINE=InnoDB");

				// stop at this version
				// to continue please upgrade again
			$this->version = '1.2.0';
			break;

			case '1.2.0':
				// convert all tables to InnoDB engine
			$this->db->query("ALTER TABLE ".$this->db->dbprefix('profiles')." ENGINE=InnoDB");

				// stop at this version
				// to continue please upgrade again
			$this->version = '1.2.1';
			break;

			case '1.2.1':
				// convert ci_session table to InnoDB engine
			$this->db->query("ALTER TABLE ".$this->db->dbprefix('ci_sessions')." ENGINE=InnoDB");

				// stop at this version
				// to continue please upgrade again
			$this->version = '1.2.2';
			break;

			case '1.2.2':

			$fields = array(
				'telp' => array(
					'type' => 'VARCHAR',
					'constraint' => 12,
					'null' => TRUE,
					),
				'nip' => array(
					'type' => 'VARCHAR',
					'constraint' => 20,
					'null' => TRUE,
					),
				);

			$this->dbforge->add_column('profiles', $fields, 'display_name');

        		// stop at this version
				// to continue please upgrade again
			$this->version = '1.2.3';
			break;

			case '1.2.3';
				// change column id data type
			$this->db->query("ALTER TABLE ".$this->db->dbprefix('users')." CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL");

				// stop at this version
				// to continue please upgrade again
			$this->version = '1.3.0';
			break;

			case '1.3.0':
				// change column id data type
			$this->db->query("SET foreign_key_checks = 0"); 
			$this->db->query("ALTER TABLE ".$this->db->dbprefix('users')." CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT");
			$this->db->query("SET foreign_key_checks = 1"); 

				// stop at this version
				// to continue please upgrade again
			$this->version = '1.4.0';
			break;

			case '1.4.0':

			$fields = array(	
				'id_provinsi' => array(
					'type' => 'INT',
					'constraint' => 11,
					'null' => TRUE,
					),
				);

			$this->dbforge->add_column('profiles', $fields);
			break;

			case '1.4.1':

			$fields = array(	
				'alamat' => array(
					'type' => 'TEXT',
					'null' => TRUE,
					),
				);

			$this->dbforge->add_column('profiles', $fields);
			break;

			case '1.4.2':

			$this->db->query("ALTER TABLE `default_profiles` CHANGE `id_provinsi` `id_provinsi` INT(11) UNSIGNED NULL DEFAULT NULL");
			$this->db->query('DELETE FROM default_profiles WHERE user_id NOT IN (SELECT id FROM default_users)');
			$this->db->query("ALTER TABLE `default_profiles` ADD CONSTRAINT `fk_prof_user` FOREIGN KEY (`user_id`) REFERENCES `default_users`(`id`)");
			$this->db->query("ALTER TABLE `default_profiles` ADD CONSTRAINT `fk_prof_prov` FOREIGN KEY (`id_provinsi`) REFERENCES `default_location_provinsi`(`id`)");
			break;

			case '1.4.3':

			$fields = array(	
				'api_key' => array(
					'type' => 'VARCHAR',
					'constraint' => 50,
					'null' => true,
					),
				);

			$this->dbforge->add_column('users', $fields);

			$this->version = '1.4.4';
			break;

		}



		return true;
	}

}
