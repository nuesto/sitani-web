<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['settings:role_all'] = 'Semua Tab';
$lang['settings:role_general'] = 'Umum';
$lang['settings:role_integration'] = 'Integrasi';
$lang['settings:role_files'] = 'File';
$lang['settings:role_wysiwyg'] = 'Wysiwyg';
$lang['settings:role_email'] = 'Email';
$lang['settings:role_comments'] = 'Komentar';
$lang['settings:role_users'] = 'Pengguna';