<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['settings:role_all'] = 'All Tab';
$lang['settings:role_general'] = 'General';
$lang['settings:role_integration'] = 'Integration';
$lang['settings:role_files'] = 'Files';
$lang['settings:role_wysiwyg'] = 'Wysiwyg';
$lang['settings:role_email'] = 'Email';
$lang['settings:role_comments'] = 'Comments';
$lang['settings:role_users'] = 'Users';