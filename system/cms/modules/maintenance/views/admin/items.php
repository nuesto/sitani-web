<div class="page-header">
	<h1><?php echo lang('maintenance:export_data') ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<?php if (! empty($tables)): ?>
	<table border="0" class="table table-striped table-bordered table-hover" cellspacing="0">
		<thead>
			<tr>
				<th><?php echo lang('maintenance:table_label') ?></th>
				<th class="align-center"><?php echo lang('maintenance:record_label') ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($tables as $table): ?>
			<tr>
				<td><?php echo $table['name'] ?></td>
				<td class="align-center"><?php echo $table['count'] ?></td>
				<td class="">
					<?php if ($table['count'] > 0):
						echo anchor('admin/maintenance/export/'.$table['name'].'/xml', lang('maintenance:export_xml'), array('class'=>'btn btn-xs btn-info')).' ';
						echo anchor('admin/maintenance/export/'.$table['name'].'/csv', lang('maintenance:export_csv'), array('class'=>'btn btn-xs btn-info')).' ';
						echo anchor('admin/maintenance/export/'.$table['name'].'/json', lang('maintenance:export_json'), array('class'=>'btn btn-xs btn-info')).' ';
					endif ?>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php else: ?>
	<div class="well"><?php echo lang('maintenance:no_data') ?></div>
<?php endif;?>

<div class="page-header">
	<h1><?php echo lang('maintenance:list_label') ?></h1>
</div>
	
<?php if (! empty($folders)): ?>
	<table border="0" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th><?php echo lang('name_label') ?></th>
				<th class="align-center"><?php echo lang('maintenance:count_label') ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($folders as $folder): ?>
			<tr>
				<td><?php echo $folder['name'] ?></td>
				<td class="align-center"><?php echo $folder['count'] ?></td>
				<td class="">
					<?php if ($folder['count'] > 0) echo anchor('admin/maintenance/cleanup/'.$folder['name'], lang('global:empty'), array('class'=>'btn btn-xs btn-danger confirm empty')) ?>
					<?php if ( ! $folder['cannot_remove']) echo anchor('admin/maintenance/cleanup/'.$folder['name'].'/1', lang('global:remove'), array('class'=>'btn btn-xs btn-danger confirm remove')) ?>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php else: ?>
	<div class="well"><?php echo lang('maintenance:no_items') ?></div>
<?php endif;?>