<?php defined('BASEPATH') or exit('No direct script access allowed');

// Page Permissions
$lang['pages:role_put_live']    = 'Publikasikan halaman';
$lang['pages:role_create_live']    = 'Tambah halaman terpublikasi';
$lang['pages:role_edit_live']   = 'Edit halaman terpublikasi';
$lang['pages:role_delete_live'] = 'Hapus halaman terpublikasi';
$lang['pages:role_administrator_pages'] = 'Administrator halaman';

// Page Type Permissions
$lang['pages:role_create_types'] = 'Tambah Types'; #translate
$lang['pages:role_edit_types']   = 'Ubah Types'; #translate
$lang['pages:role_delete_types'] = 'Hapus Types'; #translate
$lang['pages:role_administrator_types'] = 'Administrator Types';