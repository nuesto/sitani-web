(function($) {
	$(function() {

		// set values for pyro.sort_tree (we'll use them below also)
		$item_list	= $('ul.sortable');
		$url		= 'admin/pages/order';
		$cookie		= 'open_pages';

		// store some common elements
		$details 	= $('div#page-details');
		$details_id	= $('div#page-details #page-id');
		
		// Used by Pages and Navigation and is available for third-party add-ons.
		// Module must load jquery/jquery.ui.nestedSortable.js and jquery/jquery.cooki.js
		pyro.sort_tree = function($item_list, $url, $cookie, data_callback, post_sort_callback, sortable_opts)
		{
			// set options or create a empty object to merge with defaults
			sortable_opts = sortable_opts || {};
			
			// collapse all ordered lists but the top level
			$item_list.find('ul').children().hide();

			// this gets ran again after drop
			var refresh_tree = function() {

				// add the minus icon to all parent items that now have visible children
				$item_list.find('li:has(li:visible)').removeClass().addClass('minus');

				// add the plus icon to all parent items with hidden children
				$item_list.find('li:has(li:hidden)').removeClass().addClass('plus');
				
				// Remove any empty ul elements
				$('.plus, .minus').find('ul').not(':has(li)').remove();
				
				// remove the class if the child was removed
				$item_list.find("li:not(:has(ul li))").removeClass();

				// call the post sort callback
				post_sort_callback && post_sort_callback();
			}
			refresh_tree();

			// set the icons properly on parents restored from cookie
			$($.cookie($cookie)).has('ul').toggleClass('minus plus');

			// show the parents that were open on last visit
			$($.cookie($cookie)).children('ul').children().show();

			// show/hide the children when clicking on an <li>
			$item_list.find('li').on('click', null, function()
			{
				$(this).children('ul').children().slideToggle('fast');

				$(this).has('ul').toggleClass('minus plus');

				var items = [];

				// get all of the open parents
				$item_list.find('li.minus:visible').each(function(){ items.push('#' + this.id) });

				// save open parents in the cookie
				$.cookie($cookie, items.join(', '), { expires: 1 });

				 return false;
			});
			
			// Defaults for nestedSortable
			var default_opts = {
				delay: 100,
				disableNesting: 'no-nest',
				forcePlaceholderSize: true,
				handle: 'div',
				helper:	'clone',
				items: 'li',
				opacity: .4,
				placeholder: 'placeholder',
				tabSize: 25,
				listType: 'ul',
				tolerance: 'pointer',
				toleranceElement: '> div',
				update: function(event, ui) {

					post = {};
					// create the array using the toHierarchy method
					post.order = $item_list.nestedSortable('toHierarchy');

					// pass to third-party devs and let them return data to send along
					if (data_callback) {
						post.data = data_callback(event, ui);
					}

					// Refresh UI (no more timeout needed)
					refresh_tree();

					$.post(SITE_URL + $url, post );
				}
			};

			// init nestedSortable with options
			$item_list.nestedSortable($.extend({}, default_opts, sortable_opts));
		}

		// show the page details pane
		$item_list.find('li a').on('click', null, function(e) {
			e.preventDefault();

			$a = $(this);

			page_id 	= $a.attr('rel');
			page_title 	= $a.text();
			$('#page-list a').removeClass('selected');
			$a.addClass('selected');

			// Load the details box in
			$details.load(SITE_URL + 'admin/pages/ajax_page_details/' + page_id, function(){
				refresh_sticky_page_details(true);
			});

			$details.parent().prev('section.title').html( $('<h4 />').text(page_title) );

			// return false to keep the list from toggling when link is clicked
			return false;
		});

		// retrieve the ids of root pages so we can POST them along
		data_callback = function(even, ui) {
			// In the pages module we get a list of root pages
			root_pages = [];
			// grab an array of root page ids
			$('ul.sortable').children('li').each(function(){
				root_pages.push($(this).attr('id').replace('page_', ''));
			});
			return { 'root_pages' : root_pages };
		}

		// the "post sort" callback
		post_sort_callback = function() {

			$details 	= $('div#page-details');
			$details_id	= $('div#page-details #page-id');

			// refresh pane if it exists
			if($details_id.val() > 0)
			{
				$details.load(SITE_URL + 'admin/pages/ajax_page_details/' + $details_id.val());
			}
		}

		// And off we go
		pyro.sort_tree($item_list, $url, $cookie, data_callback, post_sort_callback);

		function refresh_sticky_page_details(reset) {
			 var els = $('.scroll-follow');
			if ( reset === true ) {
				els.stickyScroll('reset');
			}
			els.stickyScroll({ topBoundary: 170, bottomBoundary: 110, minimumWidth: 770});
		}
		refresh_sticky_page_details();
	});
})(jQuery);