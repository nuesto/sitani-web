<?php if ($this->method == 'create'): ?>
	<div class="hidden" id="title-value-<?php echo $link->navigation_group_id ?>">
		<h4 class="green smaller lighter"><?php echo lang('nav:link_create_title');?></h4>
	</div>
	<h4 class="green smaller lighter"><?php echo lang('nav:link_create_title');?></h4>
<?php else: ?>
	<div class="hidden" id="title-value-<?php echo $link->navigation_group_id ?>">
		<h4 class="green smaller lighter"><?php echo sprintf(lang('nav:link_edit_title'), $link->title);?></h4>
	</div>
	<h4 class="green smaller lighter"><?php echo sprintf(lang('nav:link_edit_title'), $link->title);?></h4>
<?php endif ?>

<div id="details-container">
	
	<?php echo form_open(uri_string(), 'id="nav-' . $this->method . '" class="form-horizontal"') ?>
	
		<?php if ($this->method == 'edit'): ?>
		<?php echo form_hidden('link_id', $link->id) ?>
		<?php endif ?>

		<?php echo form_hidden('current_group_id', $link->navigation_group_id) ?>
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:title');?> <span>*</span></label>
			<div class="col-sm-10">
				<?php echo form_input('title', $link->title, 'maxlength="50" class="text"') ?>
			</div>
		</div>
		
		<?php if ($this->method == 'edit'): ?>
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:group_label');?></label>
			<div class="col-sm-10">
				<?php echo form_dropdown('navigation_group_id', $groups_select, $link->navigation_group_id) ?>
			</div>
		</div>
		<?php else: ?>
			<?php echo form_hidden('navigation_group_id', $link->navigation_group_id) ?>
		<?php endif ?>

		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:type_label');?></label>
			<div class="col-sm-10">
				<?php echo form_radio('link_type', 'url', $link->link_type == 'url') ?> <?php echo lang('nav:url_label');?>
				<?php echo form_radio('link_type', 'uri', $link->link_type == 'uri') ?> <?php echo lang('nav:uri_label');?>
				<?php echo form_radio('link_type', 'module', $link->link_type == 'module') ?> <?php echo lang('nav:module_label');?>
				<?php echo form_radio('link_type', 'page', $link->link_type == 'page') ?> <?php echo lang('nav:page_label');?>
				<br />
				<small><?php echo lang('nav:link_type_desc') ?></small>
			</div>
		</div>
		
		<div id="link-type-fields">

		<div class="form-group" id="navigation-url" style="<?php echo @$link->link_type == 'url' ? '' : 'display:none' ?>">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:url_label');?></label>
			<div class="col-sm-10">
				<input type="text" id="url" name="url" value="<?php echo empty($link->url) ? 'http://' : $link->url ?>" />
			</div>
		</div>

		<div class="form-group" id="navigation-module" style="<?php echo @$link->link_type == 'module' ? '' : 'display:none' ?>">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:module_label');?></label>
			<div class="col-sm-10">
				<?php echo form_dropdown('module_name', array(lang('nav:link_module_select_default'))+$modules_select, $link->module_name) ?>
			</div>
		</div>

		<div class="form-group" id="navigation-uri" style="<?php echo @$link->link_type == 'uri' ? '' : 'display:none' ?>">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:uri_label');?></label>
			<div class="col-sm-10">
				<input type="text" id="uri" name="uri" value="<?php echo $link->uri ?>" />
			</div>
		</div>

		<div class="form-group" id="navigation-page" style="<?php echo @$link->link_type == 'page' ? '' : 'display:none' ?>">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:page_label');?></label>
			<div class="col-sm-10">
				<select name="page_id">
					<option value=""><?php echo lang('global:select-pick');?></option>
					<?php echo $tree_select ?>
				</select>
			</div>
		</div>
		
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:target_label') ?></label>
			<div class="col-sm-10">
				<?php echo form_dropdown('target', array(''=> lang('nav:link_target_self'), '_blank' => lang('nav:link_target_blank')), $link->target) ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:restricted_to');?></label>
			<div class="col-sm-10">
				<?php echo form_multiselect('restricted_to[]', array(0 => lang('global:select-any')) + $group_options, $link->restricted_to, 'size="'.(($count = count($group_options)) > 1 ? $count : 2).'"') ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nav:class_label') ?></label>
			<div class="col-sm-10">
				<?php echo form_input('class', $link->class) ?>
			</div>
		</div>
	
		<div class="clearfix form-actions">
			<div class="col-md-offset-2 col-md-9">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
			</div>
		</div>
	
	<?php echo form_close() ?>
</div>

<script>

// Pick a rule type, show the correct field
$('input[name="link_type"]').on('change', null, function(){
	$(this).closest('#details-container').find('#navigation-' + $(this).val())

	// Show only the selected type
	.show().siblings().hide()

	// Reset values when switched
	.find('input:not([value="http://"]), select').val('');

// Trigger default checked
}).filter(':checked').change();

// submit create form via ajax
$('#nav-create button:submit').on('click', null, function(e){
	e.preventDefault();
	$.post(SITE_URL + 'admin/navigation/create', $('#nav-create').serialize(), function(message){

		// if message is simply "success" then it's a go. Refresh!
		if (message == 'success') {
			window.location.href = window.location
		}
		else {
			$('.alert').remove();
			$('div#details-container').prepend(message);
			// Fade in the notifications
			$(".alert").fadeIn("slow");
		}
	});
});

// submit edit form via ajax
$('#nav-edit button:submit').on('click', null, function(e){
	e.preventDefault();
	$.post(SITE_URL + 'admin/navigation/edit/' + $('input[name="link_id"]').val(), $('#nav-edit').serialize(), function(message){

		// if message is simply "success" then it's a go. Refresh!
		if (message == 'success') {
			window.location.href = window.location
		}
		else {
			$('.alert').remove();
			$('div#details-container').prepend(message);
			// Fade in the notifications
			$(".alert").fadeIn("slow");
		}
	});

});

</script>