<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="lighter"><?php echo lang('global:filters') ?></h5>
		
		<div class="widget-toolbar">
			<a data-action="collapse" href="#">
				<i class="icon-chevron-up"></i>
			</a>
		</div>
	</div>

	<div class="widget-body">
		<div class="widget-main">
			
			<?php echo form_open('', array('class' => 'form-online', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
				
				<?php echo form_hidden('f_module', $module_details['slug']) ?>
				
				<?php if (Settings::get('moderate_comments')): ?>				
					<label><?php echo lang('comments:status_label', 'f_active') ?></label>
					<?php echo form_dropdown('f_active', array(0 =>lang('comments:inactive_title'), 1 => lang('comments:active_title')), (int) $comments_active) ?>
				<?php endif ?>
				
				<label><?php echo lang('comments:module_label', 'module_slug') ?></label>
				<?php echo form_dropdown('module_slug', array(0 => lang('global:select-all')) + $module_list, $this->input->get('module_slug')) ?>
				
				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
					<i class="icon-ok"></i>
					<?php echo lang('buttons:submit'); ?>
				</button>
				
				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-xs" type="reset">
					<i class="icon-remove"></i>
					<?php echo lang('buttons:clear'); ?>
				</button>
				
			<?php echo form_close() ?>
			
		</div>
	</div>
</div>