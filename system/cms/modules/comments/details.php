<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Comments module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Comments
 */
class Module_Comments extends Module
{

	public $version = '1.3.0';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Comments',
				'ar' => 'التعليقات',
				'br' => 'Comentários',
				'pt' => 'Comentários',
				'cs' => 'Komentáře',
				'da' => 'Kommentarer',
				'de' => 'Kommentare',
				'el' => 'Σχόλια',
				'es' => 'Comentarios',
				'fi' => 'Kommentit',
				'fr' => 'Commentaires',
                            'fa' => 'نظرات',
				'he' => 'תגובות',
				'id' => 'Komentar',
				'it' => 'Commenti',
				'lt' => 'Komentarai',
				'nl' => 'Reacties',
				'pl' => 'Komentarze',
				'ru' => 'Комментарии',
				'sl' => 'Komentarji',
				'tw' => '回應',
				'cn' => '回应',
				'hu' => 'Hozzászólások',
				'th' => 'ความคิดเห็น',
				'se' => 'Kommentarer',
			),
			'description' => array(
				'en' => 'Users and guests can write comments for content like comments, pages and photos.',
				'ar' => 'يستطيع الأعضاء والزوّار كتابة التعليقات على المُحتوى كالأخبار، والصفحات والصّوَر.',
				'br' => 'Usuários e convidados podem escrever comentários para quase tudo com suporte nativo ao captcha.',
				'pt' => 'Utilizadores e convidados podem escrever comentários para quase tudo com suporte nativo ao captcha.',
				'cs' => 'Uživatelé a hosté mohou psát komentáře k obsahu, např. neovinkám, stránkám a fotografiím.',
				'da' => 'Brugere og besøgende kan skrive kommentarer til indhold som comments, sider og fotoer.',
				'de' => 'Benutzer und Gäste können für fast alles Kommentare schreiben.',
				'el' => 'Οι χρήστες και οι επισκέπτες μπορούν να αφήνουν σχόλια για περιεχόμενο όπως το ιστολόγιο, τις σελίδες και τις φωτογραφίες.',
				'es' => 'Los usuarios y visitantes pueden escribir comentarios en casi todo el contenido con el soporte de un sistema de captcha incluído.',
                            'fa' => 'کاربران و مهمان ها می توانند نظرات خود را بر روی محتوای سایت در بلاگ و دیگر قسمت ها ارائه دهند',
                                'fi' => 'Käyttäjät ja vieraat voivat kirjoittaa kommentteja eri sisältöihin kuten uutisiin, sivuihin ja kuviin.',
				'fr' => 'Les utilisateurs et les invités peuvent écrire des commentaires pour quasiment tout grâce au générateur de captcha intégré.',
				'he' => 'משתמשי האתר יכולים לרשום תגובות למאמרים, תמונות וכו',
				'id' => 'Pengguna dan pengunjung dapat menuliskan komentaruntuk setiap konten seperti comments, halaman dan foto.',
				'it' => 'Utenti e visitatori possono scrivere commenti ai contenuti quali comments, pagine e foto.',
				'lt' => 'Vartotojai ir svečiai gali komentuoti jūsų naujienas, puslapius ar foto.',
				'nl' => 'Gebruikers en gasten kunnen reageren op bijna alles.',
				'pl' => 'Użytkownicy i goście mogą dodawać komentarze z wbudowanym systemem zabezpieczeń captcha.',
				'ru' => 'Пользователи и гости могут добавлять комментарии к новостям, информационным страницам и фотографиям.',
				'sl' => 'Uporabniki in obiskovalci lahko vnesejo komentarje na vsebino kot je blok, stra ali slike',
				'tw' => '用戶和訪客可以針對新聞、頁面與照片等內容發表回應。',
				'cn' => '用户和访客可以针对新闻、页面与照片等内容发表回应。',
				'hu' => 'A felhasználók és a vendégek hozzászólásokat írhatnak a tartalomhoz (bejegyzésekhez, oldalakhoz, fotókhoz).',
				'th' => 'ผู้ใช้งานและผู้เยี่ยมชมสามารถเขียนความคิดเห็นในเนื้อหาของหน้าเว็บบล็อกและภาพถ่าย',
				'se' => 'Användare och besökare kan skriva kommentarer till innehåll som commentsginlägg, sidor och bilder.',
			),
			'frontend' => false,
			'backend' => true,
			'menu' => 'content'
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('comments');
		$this->dbforge->drop_table('comment_blacklists');

		$tables = array(
			'comments' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'is_active' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
				'user_id' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'user_name' => array('type' => 'VARCHAR', 'constraint' => 40, 'default' => ''),
				'user_email' => array('type' => 'VARCHAR', 'constraint' => 40, 'default' => ''), // @todo Shouldn't this be 255?
				'user_website' => array('type' => 'VARCHAR', 'constraint' => 255),
				'comment' => array('type' => 'TEXT'),
				'parsed' => array('type' => 'TEXT'),
				'module' => array('type' => 'VARCHAR', 'constraint' => 40),
				'entry_id' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => 0),
				'entry_title' => array('type' => 'char', 'constraint' => 255, 'null' => false),
				'entry_key' => array('type' => 'varchar', 'constraint' => 100, 'null' => false),
				'entry_plural' => array('type' => 'varchar', 'constraint' => 100, 'null' => false),
				'uri' => array('type' => 'varchar', 'constraint' => 255, 'null' => true),
				'cp_uri' => array('type' => 'varchar', 'constraint' => 255, 'null' => true),
				'created_on' => array('type' => 'INT', 'constraint' => 11, 'default' => '0'),
				'ip_address' => array('type' => 'VARCHAR', 'constraint' => 45, 'default' => ''),
			),
			'comment_blacklists' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'website' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => ''),
				'email' => array('type' => 'VARCHAR', 'constraint' => 150, 'default' => ''),
			),
		);

		if ( ! $this->install_tables($tables))
		{
			return false;
		}

		// Install the setting
		$settings = array(
			array(
				'slug' => 'akismet_api_key',
				'title' => 'Akismet API Key',
				'description' => 'Akismet is a spam-blocker from the WordPress team. It keeps spam under control without forcing users to get past human-checking CAPTCHA forms.',
				'type' => 'text',
				'default' => '',
				'value' => '',
				'options' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => 'integration',
				'order' => 981,
			),
			array(
				'slug' => 'enable_comments',
				'title' => 'Enable Comments',
				'description' => 'Enable comments.',
				'type' => 'radio',
				'default' => true,
				'value' => true,
				'options' => '1=Enabled|0=Disabled',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'comments',
				'order' => 968,
			),
			array(
				'slug' => 'moderate_comments',
				'title' => 'Moderate Comments',
				'description' => 'Force comments to be approved before they appear on the site.',
				'type' => 'radio',
				'default' => true,
				'value' => true,
				'options' => '1=Enabled|0=Disabled',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'comments',
				'order' => 967,
			),
			array(
				'slug' => 'comment_order',
				'title' => 'Comment Order',
				'description' => 'Sort order in which to display comments.',
				'type' => 'select',
				'default' => 'ASC',
				'value' => 'ASC',
				'options' => 'ASC=Oldest First|DESC=Newest First',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'comments',
				'order' => 966,
			),
			array(
				'slug' => 'comment_markdown',
				'title' => 'Allow Markdown',
				'description' => 'Do you want to allow visitors to post comments using Markdown?',
				'type' => 'select',
				'default' => '0',
				'value' => '0',
				'options' => '0=Text Only|1=Allow Markdown',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'comments',
				'order' => 965,
			),
		);

		foreach ($settings as $setting)
		{
			if ( ! $this->db->insert('settings', $setting))
			{
				return false;
			}
		}

		return true;
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		return false;
	}

	public function upgrade($old_version)
	{
		switch ($old_version) {
			case '1.1.0':
                // convert all tables to InnoDB engine
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('comments')." ENGINE=InnoDB");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('comment_blacklists')." ENGINE=InnoDB");

                // force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '1.2.0';
				break;
            case '1.2.0':
				// ------------------------------------------
				// CREATE FK FOR `comments`.`user_id`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('comments')." WHERE KEY_NAME = 'comments_user_id_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `comments_user_id_idx` ON ".$this->db->dbprefix('comments')."(`user_id`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_user_ids = $this->db->get('users')->result();

				$existing_user_ids_arr = array();
				foreach ($existing_user_ids as $existing_user_id) {
					$existing_user_ids_arr[] = (int)$existing_user_id->id;
				}

				if(count($existing_user_ids_arr) > 0){
					$this->db->where_not_in('user_id', $existing_user_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('comments');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('comments')." MODIFY `user_id` INT(11) UNSIGNED NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('comments')." ADD FOREIGN KEY(`user_id`) REFERENCES ".$this->db->dbprefix('users')."(`id`)");

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '1.3.0';
				break;
		}

		return true;
	}

}
