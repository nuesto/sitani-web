<?php if (isset($widget->options['has_container'])): ?>
<div class="widget <?php if (isset($widget->options['color_scheme'])) echo 'widget-'.$widget->options['color_scheme'] ?> widget-<?php echo $widget->slug ?>" data-source="<?php echo site_url($widget->options['view_uri']) ?>">
	<?php if ($widget->options['show_title']): ?>
	<div class="widget-head clearfix">
	  <h4><?php echo $widget->instance_title ?></h4>
	  <div class="widget-tools pull-right">
			<a href="#" class="widget-refresh"><i class="fa fa-refresh"></i></a>
			<a href="#" class="widget-minimize"><i class="fa fa-chevron-up"></i></a>
	  </div>
	</div>
	<?php
		endif;
		echo $widget->body;
	?>
</div>
<?php
	else:
		echo $widget->body;
	endif;
?>
