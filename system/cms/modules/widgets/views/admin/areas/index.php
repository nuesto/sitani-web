<?php if ($this->controller === 'admin_areas' and ! $this->input->is_ajax_request()): ?>
<div class="page-header">
	<h1><?php echo lang('widgets:areas') ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>	
<?php endif ?>

<section class="item">
	<div class="content">
		<?php if ($widget_areas): ?>
		<!-- Available Widget Areas -->
		<div id="widget-areas-list">

		<?php foreach ($widget_areas as $widget_area): ?>
		<section class="widget-area-box" id="area-<?php echo $widget_area->slug ?>" data-id="<?php echo $widget_area->id ?>">
			<header>
				<h3><?php echo $widget_area->title ?></h3>
			</header>
			<div class="widget-area-content accordion-content">
				<div class="area-buttons buttons buttons-small">
							
					<?php echo anchor('admin/'.$this->module_details['slug'].'/areas/edit/'.$widget_area->slug, lang('global:edit'), 'class="btn btn-xs btn-info edit"') ?>
					<?php echo anchor('admin/'.$this->module_details['slug'].'/areas/delete/'.$widget_area->id, lang('global:delete'), 'class="confirm btn btn-xs btn-danger delete"') ?>

				</div>

				<!-- Widget Area Tag -->
				<input type="text" class="widget-section-code widget-code" value='<?php echo sprintf('{{ widgets:area slug="%s" }}', $widget_area->slug) ?>' />

				<!-- Widget Area Instances -->
				<div class="widget-list">
					<?php $this->load->view('admin/instances/index', array('widgets' => $widget_area->widgets)) ?>
					<div style="clear:both"></div>
				</div>
			</div>
		</section>
		<?php endforeach; ?>
		
		</div>
		<?php endif; ?>
	</div>
</section>