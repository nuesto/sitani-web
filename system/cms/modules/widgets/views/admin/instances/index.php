<ol>
<?php if ($widgets): ?>
	<?php foreach($widgets as $widget): ?>
	<li id="instance-<?php echo $widget->id ?>" class="widget-instance">
		
		<h4><span><?php echo $widget->title ?>:</span> <?php echo $widget->instance_title ?></h4>
		
		<div class="widget-actions buttons buttons-small">	
		
			<a class="btn btn-xs btn-info edit" href="<?php echo base_url().'admin/widgets/instances/edit/'.$widget->id; ?>"><i class="icon-edit bigger-110 icon-only"></i></a>						
			
			<a class="btn btn-xs btn-danger delete confirm" href="<?php echo base_url().'admin/widgets/instances/delete/'.$widget->id; ?>"><i class="icon-trash bigger-110 icon-only"></i></a>
			
			<button class="btn btn-xs instance-code" id="instance-code-<?php echo $widget->id ?>"><i class="icon-code bigger-110 icon-only"></i></button>
			
		</div>
		
		<div id="instance-code-<?php echo $widget->id ?>-wrap" style="display: none;">
			<input type="text" class="widget-code" value='{{ widgets:instance id="<?php echo $widget->id ?>"}}' />
		</div>
		<div style="clear:both"></div>
	</li>
	<?php endforeach ?>
<?php endif ?>
	<li class="empty-drop-item no-sortable"></li>
</ol>