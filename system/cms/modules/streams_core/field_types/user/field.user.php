<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * PyroStreams User Field Type
 *
 * @package		PyroCMS\Core\Modules\Streams Core\Field Types
 * @author		Parse19
 * @copyright	Copyright (c) 2011 - 2012, Parse19
 * @license		http://parse19.com/pyrostreams/docs/license
 * @link		http://parse19.com/pyrostreams
 */
class Field_user
{
	public $field_type_slug			= 'user';
	
	public $db_col_type				= 'int';

	public $custom_parameters		= array('restrict_group');

	public $version					= '1.0.0';

	public $author					= array('name'=>'Parse19', 'url'=>'http://parse19.com');

	// --------------------------------------------------------------------------

	// Run-time cache of users.
	private $cache 					= array();
	
	// --------------------------------------------------------------------------

	/**
	 * Output form input
	 *
	 * @param	array
	 * @param	array
	 * @return	string
	 */
	public function form_output($params, $entry_id, $field)
	{
		$this->CI->load->model('users/user_m');
		$this->CI->template
			->append_js('chosen.jquery.min.js')
			->append_css('chosen.css');
		
		$base_where = array('active' => 0);
		
		if (isset($params['custom']['restrict_group']) and is_numeric($params['custom']['restrict_group']))
		{
			$base_where['group_id'] = $params['custom']['restrict_group'];
		}
		
		//Skip admin
		$skip_admin = ( $this->CI->current_user->group != 'admin' ) ? 'admin' : '';
		
		// Using this data, get the relevant results
		$this->CI->db->order_by('display_name', 'asc')
			->order_by('active', 'desc')
			->join('groups', 'groups.id = users.group_id')
			->where_not_in('groups.name', $skip_admin);

		$users_raw = $this->CI->user_m->get_many_by($base_where);
		
		$users = array();

		// If this is not required, then
		// let's allow a null option
		if ($field->is_required == 'no')
		{
			$users[null] = $this->CI->config->item('dropdown_choose_null');
		}
		
		// Get user choices
		foreach ($users_raw as $user)
		{
			$users[$user->id] = $user->display_name.' - '.$user->username;
		}
	
		$output = form_dropdown($params['form_slug'], $users, $params['value'], 'id="'.$params['form_slug'].'" class="form-control"');
		$output .= "<script>
				$(document).ready(function(){
					$('#membership_user').addClass('chosen-select').chosen({
						width: '300px',
					});
				});
			</script>";
			
		return $output;
	}

	// --------------------------------------------------------------------------

	/**
	 * User Field Type Query Build Hook
	 *
	 * This joins our user fields.
	 *
	 * @access 	public
	 * @param 	array 	&$sql 	The sql array to add to.
	 * @param 	obj 	$field 	The field obj
	 * @param 	obj 	$stream The stream object
	 * @return 	void
	 */
	public function query_build_hook(&$sql, $field, $stream)
	{
		// Create a special alias for the users table.
		$alias = 'users_'.$field->field_slug;

		$sql['select'][] = '`'.$alias.'`.`id` as `'.$field->field_slug.'||user_id`';
		$sql['select'][] = '`'.$alias.'`.`email` as `'.$field->field_slug.'||email`';
		$sql['select'][] = '`'.$alias.'`.`username` as `'.$field->field_slug.'||username`';

		$sql['join'][] = 'LEFT JOIN '.$this->CI->db->protect_identifiers('users', true).' as `'.$alias.'` ON `'.$alias.'`.`id`='.$this->CI->db->protect_identifiers($stream->stream_prefix.$stream->stream_slug.'.'.$field->field_slug, true);
	}

	// --------------------------------------------------------------------------

	/**
	 * Restrict to Group
	 */
	public function param_restrict_group($value = null)
	{
		$this->CI->db->order_by('name', 'asc');
		
		$db_obj = $this->CI->db->get('groups');
		
		$groups = array('no' => lang('streams:user.dont_restrict_groups'));
		
		$groups_raw = $db_obj->result();
		
		foreach ($groups_raw as $group)
		{
			$groups[$group->id] = $group->name;
		}
	
		return form_dropdown('restrict_group', $groups, $value);
	}

}