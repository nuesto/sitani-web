<div class="row">
<div class="col-sm-12">

<div class="widget-box ?php echo $widget->slug ?>">
	<?php if ($widget->options['show_title']){ ?>
	<div class="widget-header">
		<h4 class="lighter smaller">
			<?php echo $widget->instance_title ?>
		</h4>
	</div>
	<?php } ?>

	<div class="widget-body">
		<div class="widget-main">
			<?php echo $widget->body ?>
		</div><!-- /widget-main -->
	</div><!-- /widget-body -->
</div>

</div>
</div>

<div class="hr hr8 hr-none"></div>