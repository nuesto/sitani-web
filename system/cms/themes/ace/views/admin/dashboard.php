<!-- css module -->
<?php Asset::css('dashboard.css', false, 'dashboard'); ?>
<?php Asset::css('morris.css', false, 'dashboard'); ?>
<?php echo Asset::render_css('dashboard') ?>

<div class="main-header clearfix">
	<div class="page-title">
		<h3 class="no-margin">Dashboard</h3>
		<span><i class="icon-double-angle-right"></i> overview &amp; stats</span>
	</div><!-- /page-title -->

	<ul class="page-stats">
  	<li>
  		<div class="value">
  			<span>New visits</span>
  			<h4 id="currentVisitor">4068</h4>
  		</div>
			<span class="sparkinline" sparkHeight="44px" sparkBarColor="#fc8675">1,2,3,4,5,4,3,2,1</span>
  	</li>
  	<li>
  		<div class="value">
  			<span>My balance</span>
  			<h4>$<strong id="currentBalance">30726</strong></h4>
  		</div>
  		<span class="sparkinline" sparkHeight="44px" sparkBarColor="#65cea7">1,2,3,4,5,4,3,12,1</span>
  	</li>
  </ul><!-- /page-stats -->
</div>

<div class="grey-container shortcut-wrapper">
	<a href="#" class="shortcut-link">
		<span class="shortcut-icon">
			<i class="fa fa-bar-chart-o"></i>
		</span>
		<span class="text">Statistic</span>
	</a>
	<a href="#" class="shortcut-link">
		<span class="shortcut-icon">
			<i class="fa fa-envelope-o"></i>
			<span class="shortcut-alert">
				5
			</span>
		</span>
		<span class="text">Messages</span>
	</a>
	<a href="#" class="shortcut-link">
		<span class="shortcut-icon">
			<i class="fa fa-user"></i>
		</span>
		<span class="text">New Users</span>
	</a>
	<a href="#" class="shortcut-link">
		<span class="shortcut-icon">
			<i class="fa fa-globe"></i>
			<span class="shortcut-alert">
				7
			</span>
		</span>
		<span class="text">Notification</span>
	</a>
	<a href="#" class="shortcut-link">
		<span class="shortcut-icon">
			<i class="fa fa-list"></i>
		</span>
		<span class="text">Activity</span>
	</a>
	<a href="#" class="shortcut-link">
		<span class="shortcut-icon">
			<i class="fa fa-cog"></i></span>
		<span class="text">Setting</span>
	</a>
</div> <!-- /grey-container -->

<div class="row statsbox-wrapper">
	{{ widgets:area slug="dashboard-stats" }}
</div><!-- /panel-stats -->

<div class="row">

	<div class="col-md-8">

		{{ widgets:area slug="dashboard-left" }}

	  <div class="row">

	  	<div class="col-md-6">
	  		{{ widgets:area slug="dashboard-middle-left" }}
	  	</div> <!-- /.col -->

	  	<div class="col-md-6">
	  		{{ widgets:area slug="dashboard-middle-right" }}
	  	</div> <!-- /.col -->

	  </div>

	</div> <!-- /.col -->

	<div class="col-md-4">
		{{ widgets:area slug="dashboard-right" }}
	</div><!-- /.col -->

</div> <!-- /.row -->

<div class="row">

	<div class="col-md-6">
		{{ widgets:area slug="dashboard-bottom-left" }}
	</div><!-- /.col -->

	<div class="col-md-6">
		{{ widgets:area slug="dashboard-bottom-right" }}
	</div><!-- /.col -->

</div> <!-- /.row -->

<!-- inline scripts related to this page -->
<?php Asset::js('jquery.slimscroll.js', false, 'dashboard'); ?>
<?php Asset::js('jquery.easy-pie-chart.min.js', false, 'dashboard'); ?>
<?php Asset::js('jquery.sparkline.min.js', false, 'dashboard'); ?>
<?php Asset::js('flot/jquery.flot.min.js', false, 'dashboard'); ?>
<?php Asset::js('flot/jquery.flot.pie.min.js', false, 'dashboard'); ?>
<?php Asset::js('flot/jquery.flot.resize.min.js', false, 'dashboard'); ?>
<?php Asset::js('raphael-min.js', false, 'dashboard'); ?>
<?php Asset::js('morris.min.js', false, 'dashboard'); ?>
<?php echo Asset::render_js('dashboard') ?>

<script type="text/javascript">

	/**
	 * Number.prototype.format(n, x, s, c)
	 *
	 * @param integer n: length of decimal
	 * @param integer x: length of whole part
	 * @param mixed   s: sections delimiter
	 * @param mixed   c: decimal delimiter
	 */
	Number.prototype.formatNum = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	};

	// function to refresh statbox widget
	function statsRefresh ($statsbox) {
		var $graphic = null,
				$spinner = $('<i class="loading-icon fa fa-lg fa-3x fa-spinner fa-pulse"></i>'),
				$overlay = $('<div class="loading-overlay"></div>').append($spinner);

		// create overlay spinner
		$spinner.css({ top: ($statsbox.innerHeight()/2) - ($spinner.outerHeight()/2) - 10 });
		$statsbox.append($overlay);

		// request ajax data
		// return ajax should be formated like this
		// {"items":[1,8,5,15,12,5,12,8,15,10,14,5],"total":89536}
		$.ajax({
			url: $statsbox.data('source'),
			dataType: 'json',
			success: function (data) {
				// update total counter
				var $total = $statsbox.find('.datas-text > span');
				$total.text(data.total);
				$({numberValue: 0}).animate({numberValue: data.total}, {
					duration: 2500,
					easing: 'linear',
					step: function() {
						$total.text((Math.ceil(this.numberValue).formatNum(0, 3, '.')));
					}
				});

				// update data graphic
				$graphic = $statsbox.find('.sparkinline');
				if ($graphic.length > 0 ) {
					$graphic.html(data.items.join(','));
					$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
				}
			},
			complete: function () {
				$overlay.remove();
			}
		});
	}

	// function to refresh normal widget
	function widgetRefresh ($widget,param) {
		param = param || '';
		var $head = null,
				$html = null,
				$scripts = null,
				$content = $widget.find('.widget-content'),
				$spinner = $('<i class="loading-icon fa fa-lg fa-3x fa-spinner fa-pulse"></i>'),
				$overlay = $('<div class="loading-overlay"></div>').append($spinner);

		// create overlay spinner
		if ($content.length > 0) {
			$spinner.css({ top: ($content.innerHeight()/2) - ($spinner.outerHeight()/2) - 10 });
			$content.append($overlay);
		}
		var url = $widget.data('source');
		if($widget.data('source').lastIndexOf('/') != $widget.data('source').length) {
			url += '/';
		}

		// request ajax view
		$.ajax({
			url: url+param,
			success: function (data) {
				$head = $widget.find('.widget-head');
				$head.nextAll().remove();

				// create object
				$html = $('<div class="widget-content" />').html(data);

				// find inline script
				$scripts = $html.find('script');

				// append html
				$html.find('script').remove();
				$head.after($html);

				// append scripts
				// backup if script not executed
				// -------------------------------
				if ($scripts.length > 0) {
					$scripts.each(function (i) {
						window.eval.call(window, '(function ($widget) {'+$(this).text()+'})')($widget);
					});
				}
			},
			error: function () {
				$head = $widget.find('.widget-head');
				$head.nextAll().remove();

				// create object
				$html = $('<div class="widget-content" />').html('<div class="widget-not-loaded">Widget not loaded correctly.. <br> <a class="btn btn-link widget-refresh">refresh here</a></div>');

				$head.after($html);
			},
			complete: function () {
				$overlay.remove();
			}
		});
	}

	jQuery(function($) {

		$('.sparkinline').sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });

		$('.statsbox')

		.each(function (e) {
			var $this = $(this);
			statsRefresh($this);
		})

		.on('click', '.stats-refresh', function (e) {
			e.preventDefault();
			var $this = $(this);
			statsRefresh($this.closest('.statsbox'));
		});

		// ---------------------------------------------

		$('.widget')

		.each(function (e) {
			var $this = $(this);
			widgetRefresh($this);
		})

		.on('click', '.widget-refresh', function (e) {
			e.preventDefault();
			var $this = $(this);
			widgetRefresh($this.closest('.widget'));
		})

		.on('click', '.widget-minimize', function (e) {
			e.preventDefault();
			var $widget = $(this).closest('.widget');
			$widget.find('.widget-content').slideToggle();
			$widget.find('.widget-tools > .widget-minimize > i')
							.toggleClass('fa-chevron-down', 'fa-chevron-up');
		});

		// ---------------------------------------------

		// $('.easy-pie-chart.percentage').each(function(){
		// 	var $box = $(this).closest('.infobox');
		// 	var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
		// 	var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
		// 	var size = parseInt($(this).data('size')) || 50;
		// 	$(this).easyPieChart({
		// 		barColor: barColor,
		// 		trackColor: trackColor,
		// 		scaleColor: false,
		// 		lineCap: 'butt',
		// 		lineWidth: parseInt(size/10),
		// 		animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
		// 		size: size
		// 	});
		// })

		// $('.sparkline').each(function(){
		// 	var $box = $(this).closest('.infobox');
		// 	var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
		// 	$(this).sparkline('html', {tagValuesAttribute:'data-values', type: 'bar', height: 'auto', barColor: barColor , chartRangeMin:$(this).data('min') || 0} );
		// });

		// $('.sparkinline').sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });




	 //  var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
	 //  var data = [
		// { label: "social networks",  data: 38.7, color: "#68BC31"},
		// { label: "search engines",  data: 24.5, color: "#2091CF"},
		// { label: "ad campaigns",  data: 8.2, color: "#AF4E96"},
		// { label: "direct traffic",  data: 18.6, color: "#DA5430"},
		// { label: "other",  data: 10, color: "#FEE074"}
	 //  ]
	 //  function drawPieChart(placeholder, data, position) {
		//   $.plot(placeholder, data, {
		// 	series: {
		// 		pie: {
		// 			show: true,
		// 			tilt:0.8,
		// 			highlight: {
		// 				opacity: 0.25
		// 			},
		// 			stroke: {
		// 				color: '#fff',
		// 				width: 2
		// 			},
		// 			startAngle: 2
		// 		}
		// 	},
		// 	legend: {
		// 		show: true,
		// 		position: position || "ne",
		// 		labelBoxBorderColor: null,
		// 		margin:[-30,15]
		// 	}
		// 	,
		// 	grid: {
		// 		hoverable: true,
		// 		clickable: true
		// 	}
		//  })
	 // }
	 // drawPieChart(placeholder, data);

	 // *
	 // we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
	 // so that's not needed actually.
	 // *

	 // placeholder.data('chart', data);
	 // placeholder.data('draw', drawPieChart);



	 //  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
	 //  var previousPoint = null;

	 //  placeholder.on('plothover', function (event, pos, item) {
		// if(item) {
		// 	if (previousPoint != item.seriesIndex) {
		// 		previousPoint = item.seriesIndex;
		// 		var tip = item.series['label'] + " : " + item.series['percent']+'%';
		// 		$tooltip.show().children(0).text(tip);
		// 	}
		// 	$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
		// } else {
		// 	$tooltip.hide();
		// 	previousPoint = null;
		// }

	 // });






		// var d1 = [];
		// for (var i = 0; i < Math.PI * 2; i += 0.5) {
		// 	d1.push([i, Math.sin(i)]);
		// }

		// var d2 = [];
		// for (var i = 0; i < Math.PI * 2; i += 0.5) {
		// 	d2.push([i, Math.cos(i)]);
		// }

		// var d3 = [];
		// for (var i = 0; i < Math.PI * 2; i += 0.2) {
		// 	d3.push([i, Math.tan(i)]);
		// }


		// var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
		// $.plot("#sales-charts", [
		// 	{ label: "Domains", data: d1 },
		// 	{ label: "Hosting", data: d2 },
		// 	{ label: "Services", data: d3 }
		// ], {
		// 	hoverable: true,
		// 	shadowSize: 0,
		// 	series: {
		// 		lines: { show: true },
		// 		points: { show: true }
		// 	},
		// 	xaxis: {
		// 		tickLength: 0
		// 	},
		// 	yaxis: {
		// 		ticks: 10,
		// 		min: -2,
		// 		max: 2,
		// 		tickDecimals: 3
		// 	},
		// 	grid: {
		// 		backgroundColor: { colors: [ "#fff", "#fff" ] },
		// 		borderWidth: 1,
		// 		borderColor:'#555'
		// 	}
		// });


		// $('#recent-box [data-rel="tooltip"]').tooltip({placement: tooltip_placement});
		// function tooltip_placement(context, source) {
		// 	var $source = $(source);
		// 	var $parent = $source.closest('.tab-content')
		// 	var off1 = $parent.offset();
		// 	var w1 = $parent.width();

		// 	var off2 = $source.offset();
		// 	var w2 = $source.width();

		// 	if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
		// 	return 'left';
		// }


		// $('.dialogs,.comments').slimScroll({
		// 	height: '300px'
		// });


		//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
		//so disable dragging when clicking on label
		// var agent = navigator.userAgent.toLowerCase();
		// if("ontouchstart" in document && /applewebkit/.test(agent) && /android/.test(agent))
		//   $('#tasks').on('touchstart', function(e){
		// 	var li = $(e.target).closest('#tasks li');
		// 	if(li.length == 0)return;
		// 	var label = li.find('label.inline').get(0);
		// 	if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
		// });

		// $('#tasks input:checkbox').removeAttr('checked').on('click', function(){
		// 	if(this.checked) $(this).closest('li').addClass('selected');
		// 	else $(this).closest('li').removeClass('selected');
		// });


	})
</script>