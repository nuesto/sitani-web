<div class="form-group">

	<label for="<?php echo $field['field_slug'];?>" class="col-sm-2 control-label no-padding-right"><?php echo $field['field_name'];?> <?php if ($field['required']): ?><span>*</span><?php endif; ?></label>
	
	<div class="col-sm-10">
	
		<?php echo $field['input']; ?>
		
		<?php if( $field['instructions'] != '' ): ?>
		<span class="help-inline col-xs-12 col-sm-7">
			<span class="middle"><?php echo $field['instructions']; ?></span>
		</span>
		<?php endif; ?>
		
	</div>
	
</div>