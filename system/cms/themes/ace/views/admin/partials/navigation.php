<ul class="nav nav-list">
	
	<li class="<?php if($this->uri->uri_string() == 'admin'){ ?>active<?php } ?>">
		<a href="<?php echo base_url(); ?>admin/">
			<i class="icon-dashboard"></i>
			<span class="menu-text"> <?php echo lang('global:dashboard'); ?> </span>
		</a>
	</li>
	
	<?php foreach ($menu_items as $key => $menu_item){ ?>
		
		<?php
		// skip these menu, won't displayed
		if($key == 'lang:cp:nav_profile'){
			continue;
		}
		
		// set default icon for core modules menu
		$icon_name = 'chevron-sign-right';
		if($key == 'lang:cp:nav_content'){
			$icon_name = 'edit';
		}elseif($key == 'lang:cp:nav_Gallery'){
			$icon_name = 'picture';
		}elseif($key == 'lang:cp:nav_structure'){
			$icon_name = 'list-alt';
		}elseif($key == 'lang:cp:nav_data'){
			$icon_name = 'list';
		}elseif($key == 'lang:cp:nav_users'){
			$icon_name = 'user';
		}elseif($key == 'lang:cp:nav_organization'){
			$icon_name = 'sitemap';
		}elseif($key == 'lang:cp:nav_Human_Resource'){
			$icon_name = 'group';
		}elseif($key == 'lang:cp:nav_SMS Gateway API'){
			$icon_name = 'mobile-phone';
		}elseif($key == 'lang:cp:nav_addons'){
			$icon_name = 'plus';
		}elseif($key == 'lang:cp:nav_settings'){
			$icon_name = 'cog';
		}

		// set icon for addons modules menu here
		elseif($key == 'lang:cp:nav_Laporan'){
			$icon_name = 'bar-chart';
		}elseif($key == 'lang:cp:nav_Pendamping'){
			$icon_name = 'group';
		}elseif($key == 'lang:cp:nav_Dialog'){
			$icon_name = 'comments';
		}elseif($key == 'lang:cp:nav_Location'){
			$icon_name = 'map-marker';
		}elseif($key == 'lang:cp:nav_Sp2d'){
			$icon_name = 'file-text';
		}
		
		?>

		<?php
		$urls = array();
		if(is_array($menu_item) AND array_key_exists('urls', $menu_item)) {
			$label = find_parent($menu_items, 'urls');
			$urls = $menu_item['urls'];
			$menu_item = array($label => $menu_item['urls'][0]);
			unset($menu_item['urls']);
		}
		?>
		
		<?php if(is_array($menu_item) AND count($menu_item) == 1){ ?>
			<?php foreach($menu_item as $key_2 => $value_2){ ?>
				<?php
				if(is_array($value_2) AND count($value_2)==1 AND array_key_exists('urls', $value_2)) {
					$value_2[$key_2] = $value_2;
					unset($value_2['urls']);
				}
				?>
				<?php if(is_array($value_2) and count($value_2)>1 and $key_2!='urls'){ ?>
				<?php $is_submenu_active = is_active_menu($value_2, $active_section); ?>

				<li class="<?php if($is_submenu_active === TRUE){ ?>active open<?php } ?>">
					<a href="<?php echo current_url(); ?>#" class="dropdown-toggle">
						<i class="icon-<?php echo $icon_name; ?>"></i>
						<span class="menu-text"> <?php echo lang_label($key_2); ?> </span>

						<b class="arrow icon-angle-down"></b>
					</a>
					
					<ul class="submenu">
					<?php foreach($value_2 as $key_3 => $value_3){ ?>
						<?php 
						if(is_array($value_3) AND array_key_exists('urls', $value_3)) {
							$label = find_parent($value_2, 'urls');
							$urls = $value_3['urls'];
							$value_3 = $value_3['urls'][0];
						}
						?>
						<li class="<?php if(strpos($this->uri->uri_string(), $value_3) === 0 OR check_urls($urls, $this->uri->uri_string(),$value_3)){ ?>active open<?php } ?>">
							<a href="<?php echo base_url() . $value_3; ?>">
								<span class="menu-text"><?php echo lang_label($key_3); ?></span>
							</a>
						</li>

					<?php } ?>
					</ul>
				</li>		
				<?php }elseif(is_array($value_2) and $key_2!='urls'){ ?>

					<?php foreach($value_2 as $key_3 => $value_3){ ?>
						<?php 
						if(is_array($value_3) AND array_key_exists('urls', $value_3)) {
							$label = find_parent($value_2, 'urls');
							$urls = $value_3['urls'];
							$value_3 = $value_3['urls'][0];
						}
						?>
						<li class="<?php if(strpos($this->uri->uri_string(), $value_3) === 0 OR check_urls($urls, $this->uri->uri_string(),$value_3)){ ?>active open<?php } ?>">
							<a href="<?php echo base_url() . $value_3; ?>">
								<i class="icon-<?php echo $icon_name; ?>"></i>
								<span class="menu-text"><?php echo lang_label($key_3); ?></span>
							</a>
						</li>

					<?php } ?>
				<?php }else{ ?>
					<li class="<?php if(strpos($this->uri->uri_string(), $value_2) === 0 OR check_urls($urls, $this->uri->uri_string(),$value_2)){ ?>active open<?php } ?>">
						<a href="<?php echo base_url() . $value_2; ?>">
							<i class="icon-<?php echo $icon_name; ?>"></i>
							<span class="menu-text"><?php echo lang_label($key_2); ?></span>
						</a>
					</li>

				<?php } ?>

			<?php } ?>

		<?php }elseif (is_array($menu_item) AND count($menu_item) > 1) { ?>
			
			<?php $is_submenu_active = is_active_menu($menu_item, $active_section); ?>
			
			<li class="<?php if($is_submenu_active === TRUE){ ?>active open<?php } ?>">
				<a href="<?php echo current_url(); ?>#" class="dropdown-toggle">
					<i class="icon-<?php echo $icon_name; ?>"></i>
					<span class="menu-text"> <?php echo lang_label($key); ?> </span>

					<b class="arrow icon-angle-down"></b>
				</a>
				
				<ul class="submenu">
				
				<?php foreach ($menu_item as $lang_key => $uri) { ?>
					<?php
					if(is_array($uri) AND count($uri)==1 AND !array_key_exists('urls', $uri)) {
						$uri = reset($uri);
					}

					$urls2 = array();
					if(is_array($uri) AND array_key_exists('urls', $uri)) {
						$label = find_parent($menu_items, 'urls');
						$urls2 = $uri['urls'];
						$uri = $uri['urls'][0];
					}
					?>

					<?php 
					/***************************
					 *	Don't have submenu
					 ***************************/
					if(! is_array($uri)){
					?>
					<li class="<?php if(strpos($this->uri->uri_string(), $uri) === 0 OR check_urls($urls2, $this->uri->uri_string(),$uri) ){ ?>active open<?php } ?>">
					
						<a href="<?php echo site_url($uri); ?>">
							<i class="icon-double-angle-right"></i>
							<?php echo lang_label($lang_key); ?>
						</a>
						
					</li>
					
					<?php 
					/***************************
					 *	Have submenu
					 ***************************/
					}else{ // if 
					?>
					
					<?php $is_section_active = is_active_menu($uri, $active_section); ?>
					
					<li class="<?php if($is_section_active){ ?>active open<?php } ?>">
					
					<a href="#" class="dropdown-toggle">
						<i class="icon-double-angle-right"></i>
						<?php echo lang_label($lang_key); ?>
						<b class="arrow icon-angle-down"></b>
					</a>
					
					<ul class="submenu">
					
					<?php foreach($uri as $section_name => $section_uri){ ?>
						<?php
						if(is_array($section_uri) AND array_key_exists('urls', $section_uri)) {
							$section_uri = $section_uri['urls'];
							$link = $section_uri[0];
						} else {
							$link = $section_uri;
						}
						?>
						<?php $is_section_active = is_active_menu($section_uri, $active_section); ?>
						<li class="<?php if($is_section_active){ ?>active<?php } ?>">
							<a href="<?php echo site_url($link); ?>">
								<i class="icon-chevron-sign-right"></i>
								
								<?php echo lang_label($section_name); ?>
							</a>
						</li>
					
					<?php } // foreach ?>
					
					</ul>
					
					</li>
					
					<?php } // if ?>
					<?php unset($urls); ?>
				<?php } ?>

				</ul>
			</li>
			
		<?php }else{ ?>
			<li class="<?php if(strpos($this->uri->uri_string(), $menu_item) === 0){ ?>active open<?php } ?>">
				<a href="<?php echo base_url() . $menu_item; ?>">
					<i class="icon-<?php echo $icon_name; ?>"></i>
					<span class="menu-text"><?php echo lang_label($key); ?></span>
				</a>
			</li>
			
		<?php } ?>
	<?php } ?>
	
</ul><!-- /.nav-list -->