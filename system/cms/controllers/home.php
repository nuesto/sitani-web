<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Home controller
 *
 * Description
 *
 */
class Home extends Public_Controller
{
    public function __construct()
    {
      parent::__construct();
      $this->load->model('laporan/komoditas_m');
    }
	
	public function index()
	{
		// Pendamping
		$this->db->select('COUNT(id) as jml, group_id');
		$this->db->where_in('group_id',array(7,9,10));
		$this->db->where('active', 1);
		$this->db->group_by('group_id');
		$result = $this->db->get('default_users')->result_array();
		$arr_total_pendamping = array(7=>'total_pendamping', 9=>'total_pendamping_2017', 10=>'total_pendamping_2018');		
		foreach ($result as $key => $value) {
			$data[$arr_total_pendamping[$value['group_id']]] = $value['jml'];
		}

		// Units
		$arr_unit_type = array(1,2,4,5,6,7);
		$this->db->select('COUNT(id) as jml, unit_type');
		$this->db->where_in('unit_type',$arr_unit_type);
		$this->db->group_by('unit_type');
		$result = $this->db->get('default_organization_units')->result_array();
		$arr_total_type = array(1=>'total_gapoktan', 2=>'total_tti', 4=>'total_gapoktan_2017',5=>'total_tti_2017',6=>'total_gapoktan_2018',7=>'total_tti_2018');
		foreach ($result as $key => $value) {
			$data[$arr_total_type[$value['unit_type']]] = $value['jml'];
		}

		// Komoditas
		$data['komoditas_first'] = $this->komoditas_m->get_first_komoditas();
		$data['komoditas'] = $this->komoditas_m->get_komoditas_related();
		$data['komoditas_gap'] = $this->komoditas_m->get_komoditas_related_by_tipe(1);
		$data['komoditas_tti'] = $this->komoditas_m->get_komoditas_related_by_tipe(2);
		$data['komoditas_ttic'] = $this->komoditas_m->get_komoditas_related_by_tipe(1);

		$this->template->title('Dashboard')
			->set_breadcrumb('Home', '/')
			->set_layout('home.html')
			->build('home', $data);
	}
}