<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Code here is run before frontend controllers
 *
 * @author      qSmart Team
 * @copyright   Copyright (c) 2015, PT. Nuesto Technology Indonesia
 * @package			PyroCMS\Core\Controllers
 */
class API2_Controller extends Public_Controller
{
	/**
	* Loads the gazillion of stuff, in Flash Gordon speed.
	* @todo Document properly please.
	*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('users/user_m', 'permissions/permission_m'));
		$this->load->helper(array('api_auth','users/user'));
		header('Access-Control-Allow-Origin: *');
	}

	public function authorize_api_access($module, $role_name = NULL){
		if($this->input->get('metod')){
			$method = $this->input->get('metod');
		}else{
			$method = $this->metod;
		}
		$data = $this->input->$method();
		
		if(empty($role_name)) {
			$role_name = "api_".$module;
		}

		$has_access = true;
		if(isset($data['api_user']) && isset($data['api_key'])){
			//pemeriksaan hak akses berdasarkan api key dan username
			$api = array('user'=>$data['api_user'], 'key'=>$data['api_key']);
			$has_access = api_has_role($api, $module, $role_name);
		}elseif(isset($data['token']) && isset($data['uuid']) && isset($data['phone'])){
			//pemeriksaan berdasarkan token mobile
			$has_access = $this->cek_token($data);
		}else{
			//pemeriksaan berdasarkan role yang ada didalam database
			if(! group_has_role('api', $role_name)){
				$has_access = false;
			}
		}

		if($has_access==false){
			header('Content-Type: application/json');
			header("HTTP Error 401 Unauthorized");
			die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
		}
	}

	public function cek_token($data){
		if($data['token'] == substr(md5($data['uuid'].$data['phone']),0,15)){
			if($this->customer_m->cek_token($data)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
