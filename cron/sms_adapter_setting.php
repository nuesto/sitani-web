<?php
	$apiKeyAccess = 'nU3sT0S1T0ni';
	$api_url = 'http://tti.pertanian.go.id/';
	// $api_url = 'http://localhost/sitoni_pyro/';
	$url_get = $api_url.'sms_gateway_api/action/get_inbox?apiKeyAccess='.$apiKeyAccess;
	$url_process = $api_url.'laporan/sms/process?apiKeyAccess='.$apiKeyAccess;
	$url_send = $api_url.'sms_gateway_api/action/send_sms?apiKeyAccess='.$apiKeyAccess;
	
	if(!function_exists('request_curl')){
		function request_curl($url, $apiKeyAccess){
			$field = 'apiKeyAccess='.$apiKeyAccess;
			
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $url);
			curl_setopt($curlHandle, CURLOPT_POST, 1);
			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $field);
			curl_setopt($curlHandle, CURLOPT_HEADER, 0);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);

			$results = json_decode(curl_exec($curlHandle), true);

			curl_close($curlHandle);
			return $results;
		}
	}

	if(!function_exists('httpGet')){
		function httpGet($url)
		{
	    $ch = curl_init();  
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	    $output = json_decode(curl_exec($ch), true);

			curl_close($ch);
	    return $output;
		}
	}
?>