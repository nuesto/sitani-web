<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Location
*
* Module API
*
*/
class Api extends API2_Controller
{
	public $metod = "get";

	public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('location');

		$this->load->model('kelurahan_m');
		$this->load->model('kota_m');
		$this->load->model('provinsi_m');

		// $this->authorize_api_access('location');
    }

    public function get_provinsi()
	{
		$result = $this->clean_fields($this->provinsi_m->get_provinsi());

		$status = "200";

		if(empty($result)) {
			$result = array('status'=>'error', 'message'=>'Provinsi not found');
	    	$status = '200';
	    }

	    _output($result,$status);
	}

    public function get_kota_by_provinsi($id_provinsi='')
	{
		$result = $this->clean_fields($this->kota_m->get_kota_by_provinsi($id_provinsi));

		$status = "200";

		if(empty($result)) {
			$result = array('status'=>'error', 'message'=>'Kota not found');
	    	$status = '200';
	    }

	    _output($result,$status);
	}

	public function get_kecamatan_by_kota($id_kota='')
	{
		$result = $this->clean_fields($this->kecamatan_m->get_kecamatan_by_kota($id_kota));

		$status = "200";

		if(empty($result)) {
			$result = array('status'=>'error', 'message'=>'Kecamatan not found');
	    	$status = '200';
	    }

	    _output($result,$status);
	}

	public function get_kelurahan_by_kecamatan($id_kecamatan='')
	{
		$result = $this->clean_fields($this->kelurahan_m->get_kelurahan_by_kecamatan($id_kecamatan));

		$status = "200";

		if(empty($result)) {
			$result = array('status'=>'error', 'message'=>'Kelurahan not found');
	    	$status = '200';
	    }

	    _output($result,$status);
	}

	private function clean_fields($data) {
		$clean_data = NULL;
		$unused_fields = array("created_on","created_by","updated_on","updated_by");
		foreach ($data as $key => $value) {
			if(is_array($value)) {
				foreach ($value as $key2 => $value2) {
					if(!in_array($key2, $unused_fields)) {
						$clean_data[$key][$key2] = $value2;
					}
				}
			}
		}
		return $clean_data;
	}
}
