<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['sms_gateway_api/admin/inbox(:any)'] = 'admin_inbox$1';
$route['sms_gateway_api/admin/outbox(:any)'] = 'admin_outbox$1';
$route['sms_gateway_api/inbox(:any)'] = 'sms_gateway_api_inbox$1';
$route['sms_gateway_api/outbox(:any)'] = 'sms_gateway_api_outbox$1';
