<?php
	function httpGet($url)
	{
    $ch = curl_init();  
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    $output=curl_exec($ch);
    curl_close($ch);
    return $output;
	}
	function curl($url, $postfields){
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postfields);
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		$results = curl_exec($curlHandle);
		return $results;
	}
	function sendsms($destination, $message){
		include "apiconfig.php";
		$url = $urlsend;
		$postfields = 'appkey='.$appkey.'&user='.$user.'&to='.$destination.'&send_text='.urlencode($message).'&service=pupm&smsc=gsm0';
		$result = curl($url, $postfields);
		return $result;
	}

	function readinbox(){
		include "apiconfig.php";
		$url = $urlinbox.'?appkey='.$appkey.'&user='.$user;
		$json = httpGet($url);
		$data = json_decode($json,TRUE);
		if($data['status']['code'] == 400){
			$results['status'] = 'error';
			$results['status_code'] = 400;
			$results['status_message'] = $data['text'];
		}else{
			$results['result'] = $data['result'];
			$results['status'] = 'success';
			$results['status_code'] = 200;
			$results['status_message'] = 'Berhasil mengambil data';
		}
		return $results;	
	}

?>