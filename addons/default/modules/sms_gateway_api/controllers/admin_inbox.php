<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SMS Gateway API Module
 *
 * Module untuk mengelola SMS Gateway API
 *
 */
class Admin_inbox extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'inbox';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'access_inbox_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$this->load->config('apiconfig', true);
    $this->apiKeyAccess = $this->config->item('apiKeyAccess', 'apiconfig');
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('sms_gateway_api');		
		$this->load->model('inbox_m');
  }

  /**
	 * List all inbox
   *
   * @return	void
   */
  public function index()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'view_all_inbox') AND ! group_has_role('sms_gateway_api', 'view_own_inbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$surffix = '';
    if($_SERVER['QUERY_STRING']){
      $surffix = '?'.$_SERVER['QUERY_STRING'];
    }

		$get_inbox = $this->inbox_m->get_inbox(NULL);
		$pagination_config['base_url'] = base_url(). 'admin/sms_gateway_api/inbox/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($get_inbox);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
    // -------------------------------------
		// Get entries
		// -------------------------------------
		
    $data['inbox']['entries'] = $this->inbox_m->get_inbox($pagination_config);
		$data['inbox']['total'] = count($get_inbox);
		$data['inbox']['pagination'] = $this->pagination->create_links();

		$data['apiKeyAccess'] = $this->apiKeyAccess;

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api:inbox:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api:inbox:plural'))
			->build('admin/inbox_index', $data);
  }
	
	/**
   * Display one inbox
   *
   * @return  void
   */
  public function view($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'view_all_inbox') AND ! group_has_role('sms_gateway_api', 'view_own_inbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['inbox'] = $this->inbox_m->get_inbox_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'view_all_inbox')){
			if($data['inbox']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api:inbox:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api:inbox:plural'), '/admin/sms_gateway_api/inbox/index')
			->set_breadcrumb(lang('sms_gateway_api:inbox:view'))
			->build('admin/inbox_entry', $data);
  }
	
	/**
   * Create a new inbox entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
  public function create()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'create_inbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_inbox('new')){	
				$this->session->set_flashdata('success', lang('sms_gateway_api:inbox:submit_success'));				
				redirect('admin/sms_gateway_api/inbox/index');
			}else{
				$data['messages']['error'] = lang('sms_gateway_api:inbox:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/sms_gateway_api/inbox/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api:inbox:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api:inbox:plural'), '/admin/sms_gateway_api/inbox/index')
			->set_breadcrumb(lang('sms_gateway_api:inbox:new'))
			->build('admin/inbox_form', $data);
  }
	
	/**
   * Edit a inbox entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the inbox to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'edit_all_inbox') AND ! group_has_role('sms_gateway_api', 'edit_own_inbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('sms_gateway_api', 'edit_all_inbox')){
			$entry = $this->inbox_m->get_inbox_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_inbox('edit', $id)){	
				$this->session->set_flashdata('success', lang('sms_gateway_api:inbox:submit_success'));				
				redirect('admin/sms_gateway_api/inbox/index');
			}else{
				$data['messages']['error'] = lang('sms_gateway_api:inbox:submit_failure');
			}
		}
		
		$data['fields'] = $this->inbox_m->get_inbox_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/sms_gateway_api/inbox/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api:inbox:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api:inbox:plural'), '/admin/sms_gateway_api/inbox/index')
			->set_breadcrumb(lang('sms_gateway_api:inbox:view'), '/admin/sms_gateway_api/inbox/view/'.$id)
			->set_breadcrumb(lang('sms_gateway_api:inbox:edit'))
			->build('admin/inbox_form', $data);
  }
	
	/**
   * Delete a inbox entry
   * 
   * @param   int [$id] The id of inbox to be deleted
   * @return  void
   */
  public function delete($id = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'delete_all_inbox') AND ! group_has_role('sms_gateway_api', 'delete_own_inbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('sms_gateway_api', 'delete_all_inbox')){
			$entry = $this->inbox_m->get_inbox_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
    $this->inbox_m->delete_inbox_by_id($id);
    $this->session->set_flashdata('error', lang('sms_gateway_api:inbox:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
    redirect('admin/sms_gateway_api/inbox/index');
  }
	
	/**
   * Insert or update inbox entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_inbox($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('sms_gateway_api:inbox:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->inbox_m->insert_inbox($values);
				
			}
			else
			{
				$result = $this->inbox_m->update_inbox($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}