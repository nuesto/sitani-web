<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SMS Gateway API Module
 *
 * Module untuk mengelola SMS Gateway API
 *
 */
class Action extends Public_Controller
{
  // -------------------------------------
  // This will set the active section tab
  // -------------------------------------
  
  protected $section = '';

  public function __construct()
  {
    parent::__construct();
    
    // -------------------------------------
    // Load everything we need
    // -------------------------------------

    $this->lang->load('sms_gateway_api');
    $this->load->config('apiconfig', true);

    $this->apiKeyAccess = $this->config->item('apiKeyAccess', 'apiconfig');
    $this->provider = $this->config->item('provider', 'apiconfig');
        
    $this->load->helper($this->provider.'/'.$this->provider);
    $this->load->model('inbox_m');  
    $this->load->model('outbox_m');  

    date_default_timezone_set('Asia/Jakarta');
  }
  
  public function get_inbox_local(){
    $data_inbox = array();
    if(isset($_GET['sender']) && isset($_GET['sms'])){
      $row['id'] = rand();
      $row['sms'] = $_GET['sms'];
      $tri_num = substr($_GET['sender'], 0, 3);
      $nohp = $_GET['sender'];
      if($tri_num == '628'){
        $nohp = '+'.$_GET['sender'];
      }
      $row['sender'] = str_replace("+62","0", $nohp);
      $row['waktu'] = date('Y-m-d H:i:s');
      $data_inbox[] = $row;
      $this->inbox_m->insert_inbox($row);
      $results['inbox'] = $data_inbox;
    }

    $json_encode = json_encode($results);
    echo $json_encode;
  }
  public function get_inbox(){
    // echo 'SMS - Get Inbox '.$this->provider.':<br><br>';
    $apiKeyAccess = $this->input->get('apiKeyAccess');
    if($apiKeyAccess){
      $getApiKeyAccess = $this->input->get('apiKeyAccess');
      if($getApiKeyAccess == $this->apiKeyAccess){
        $http_response= $this->get_http_response();
        if($http_response['available']){
          $inbox = readInbox();
          $results = $inbox;
        }
        $results['http_response'] = $http_response['msg']; 
      }else{
        $results['status_code'] = 400;
        $results['status_message'] = "Key is not registered";
      }
    }else{
      $results['status_code'] = 400;
      $results['status_message'] = "Forbidden Access";
    }

    $data_inbox = array();
    if(isset($results['result']) && is_array($results['result'])){
      foreach ($results['result'] as $key => $pesan) {
        if($this->inbox_m->check_id($pesan['id'])){
          $row['id'] = $pesan['id'];
          $row['sms'] = $pesan['message'];
          $tri_num = substr($pesan['src'], 0, 3);
          $nohp = $pesan['src'];
          if($tri_num == '628'){
            $nohp = '+'.$pesan['src'];
          }
          $row['sender'] = str_replace("+62","0", $nohp);
          $row['waktu'] = $pesan['created_at'];
          $data_inbox[] = $row;
          $this->inbox_m->insert_inbox($row);
        }
      }
      unset($results['result']);
      $results['inbox'] = $data_inbox;
    }

    $json_encode = json_encode($results);
    //if(count($data_inbox) > 0){
      echo $json_encode;
    //}
  }
  
  public function send_sms(){
    // echo 'SMS - Send sms to '.$this->provider.':<br><br>';
    $apiKeyAccess = $this->input->get('apiKeyAccess');
    if($apiKeyAccess){
      $getApiKeyAccess = $this->input->get('apiKeyAccess');

      if($getApiKeyAccess == $this->apiKeyAccess){

        $http_response= $this->get_http_response();
        $get_outbox = $this->outbox_m->get_outbox_to_send();
        if(count($get_outbox) > 0){
          $message = array();
          $sendsms = true;
          foreach ($get_outbox as $key => $outbox) {
            $row_id = $outbox['id'];
            $destination = $outbox['destination'];
            $message = $outbox['sms'];
            $result['status_code'] = 400;
            $result['status_message'] = "Error";
            if($http_response['available']){
              $result['message'] = $message;
              $result['destination'] = $destination;
              
              if(sendsms($destination, $message)){
                $result['status_code'] = 200;
                $result['status_message'] = "Success";
              }
            }
            $result['http_response'] = $http_response['msg'];
            $json_encode_result = json_encode($result);

            $values['UpdatedInDB'] = date('Y-m-d H:i:s');
            $values['status'] = $result['status_message'];
            $values['results'] = $json_encode_result;
            $this->outbox_m->update_outbox($values, $row_id);
            $results[] = $result;
          }
          
        }else{
          $results['status_code'] = 200;
          $results['status_message'] = "Tidak ada pesan yang akan dikirim";
        }
      }else{
        $results['status_code'] = 400;
        $results['status_message'] = "Key is not registered";
      }
    }else{
      $results['status_code'] = 400;
      $results['status_message'] = "Forbidden Access";
    }
    $json_encode = json_encode($results);
    echo $json_encode;
  }

  public function get_http_response(){
    $apiUrl = $this->config->item('apiUrl', 'apiconfig');
    $content = @file_get_contents($apiUrl);
    if($content === false){
      $result['msg'] = 'failed to open stream: HTTP request failed!';
      $result['available'] = false;
    }else{
      $result['msg'] = $http_response_header[0];
      $result['available'] = true;
    }
    return $result;
  }

  // --------------------------------------------------------------------------

}