<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Inbox model
 *
 * @author Aditya Satrya
 */
class Inbox_m extends MY_Model {
	
	public function get_inbox($pagination_config = NULL)
	{
		$this->db->select('*');
		
		if($this->input->get('f-sms')){
			$this->db->like('sms', $this->input->get('f-sms'));
		}
		if($this->input->get('f-sender')){
			$this->db->like('sender', $this->input->get('f-sender'));
		}
		if($this->input->get('f-waktu')){
			$this->db->like('waktu', $this->input->get('f-waktu'));
		}

		$this->db->order_by('waktu', 'DESC');

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_sms_gateway_api_inbox');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_inbox_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_sms_gateway_api_inbox');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_inbox_for_parsing(){
		$this->db->select('*');
		$this->db->where("DATE_FORMAT(waktu, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d')");
		$this->db->where('status',NULL);
		$this->db->where('ExecutedTime',NULL);
		$query = $this->db->get('default_sms_gateway_api_inbox');
		$result = $query->result_array();
		return $result;
	}

	public function check_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get('default_sms_gateway_api_inbox');
		$result = $query->num_rows();
		if($result > 0){
			return false;
		}
		return true;
	}
	
	public function count_all_inbox()
	{
		return $this->db->count_all('sms_gateway_api_inbox');
	}
	
	public function delete_inbox_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_sms_gateway_api_inbox');
	}
	
	public function insert_inbox($values)
	{	
		return $this->db->insert('default_sms_gateway_api_inbox', $values);
	}
	
	public function update_inbox($values, $row_id)
	{
		$this->db->where('id', $row_id);
		return $this->db->update('default_sms_gateway_api_inbox', $values); 
	}
	
}