<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Sumber Data
*
* Module API
*
*/
class Api_sumberdata extends API2_Controller
{
	public $metod = 'get';

	public function __construct()
	{
		parent::__construct();

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('laporan');
		$this->lang->load('location/location');

		$this->load->model('organization/units_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
		$this->load->model('tipe_m');
		$this->load->model('organization/types_m');
		$this->lang->load('organization/organization');
	}

	public function get_all() {
		if($this->uri->segment('2')=='gapoktan') {
			$_GET['f-tipe_laporan'] = 1;
		} else if($this->uri->segment('2')=='tti') {
			$_GET['f-tipe_laporan'] = 2;
		}

		if($this->input->get('id_provinsi')) {
			$_GET['f-provinsi'] = $this->input->get('id_provinsi');
		}

		if($this->input->get('id_kota')) {
			$_GET['f-kota'] = $this->input->get('id_kota');
		}

		$result = $this->units_m->get_sumberdata();

		$status = "200";

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>$this->uri->segment('2')." data not found");
			$status = "200";
		}
		_output($result,$status);
	}

	public function jumlah()
	{
		if($this->uri->segment('2')=='gapoktan') {
			$_GET['f-tipe_laporan'] = 1;
		} else if($this->uri->segment('2')=='tti') {
			$_GET['f-tipe_laporan'] = 2;
		}

		if($this->input->get('id_provinsi')) {
			$_GET['f-provinsi'] = $this->input->get('id_provinsi');
		}

		if($this->input->get('id_kota')) {
			$_GET['f-kota'] = $this->input->get('id_kota');
		}

		$result = count($this->units_m->get_sumberdata());

		$status = "200";

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>$this->uri->segment('2')." data not found");
			$status = "200";
		}
		_output($result,$status);
	}

	public function get_by_id($id='')
	{
		if($this->uri->segment('2')=='gapoktan') {
			$supposed_type = 1;
		} else if($this->uri->segment('2')=='tti') {
			$supposed_type = 2;
		}

		$data['units'] =  $this->units_m->get_units_by_id($id);
		if($data['units']->unit_type == $supposed_type) {
			$data['units']->unit_type = (object) $this->types_m->get_type_by_id($data['units']->unit_type);


			// Get multiple relationship

			$unit_parents = $this->units_m->get_parent_units_by_id($data['units']->id);
			foreach ($unit_parents as $key => $value) {
				$unit_parents[$key]['unit_type'] = (object) $this->types_m->get_type_by_id($value['unit_type']);
			}

			$data['units']->unit_parents = $unit_parents;

			$this->load->model('organization/memberships_m');
			$pendampings = $this->memberships_m->get_membership_by_unit($id);
			$data['pendampings'] = $pendampings;
			$nos = '';
			$namas = '';
			if(count($pendampings) > 0){
				foreach ($pendampings as $key => $pendamping) {
					$nohp = substr($pendamping['user_telp'], 0, -4).'xxxx';
					$nama_pendamping[] = $pendamping['user_display_name'] .' ('.$nohp.')';
					// $nama_pendamping[] = $pendamping['user_display_name'];
					$no_pendamping[] = $nohp;
				}
				$nos = implode(', ',$no_pendamping);
				$namas = implode(', ', $nama_pendamping);
			}

			$data['units']->nama_pendamping = $namas;
			$data['units']->no_pendamping = $nos;
		} else {
			$data['units'] = NULL;
		}

		$result = $data['units'];

		$status = "200";

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>$this->uri->segment('2')." data not found");
			$status = "200";
		}
		_output($result,$status);
	}
}
