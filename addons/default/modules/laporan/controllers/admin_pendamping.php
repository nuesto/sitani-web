<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * user
 *
 * Modul yang me-manage user
 *
 */
class Admin_pendamping extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'pendamping';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('users', 'manage_users') AND ! group_has_role('users', 'manage_own_unit_users')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    // Load the required classes
		$this->load->model('organization/memberships_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
		$this->load->model('tipe_m');
		$this->load->model('pendamping_m');
  }

  /**
	 * List all user
   *
   * @return	void
   */
  public function index()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(!group_has_role('users', 'manage_users') AND !group_has_role('users', 'manage_own_unit_users')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		if($this->input->get('f-groups') == "7"){
			$data['id_groups'] = $this->input->get('f-groups');
		} else if($this->input->get('f-groups') == "9") {
			$data['id_groups'] = $this->input->get('f-groups');
		} else if($this->input->get('f-groups') == "10") {
			$data['id_groups'] = $this->input->get('f-groups');
		} else {
			$data['id_groups'] = '0';
		}
		
		$surffix = '';
	  if($_SERVER['QUERY_STRING']){
	    $surffix = '?'.$_SERVER['QUERY_STRING'];
	  }

	  if($this->input->get('f-groups')){
	   	// -------------------------------------
			// Pagination
			// -------------------------------------
		  // $count_pendamping = $this->memberships_m->get_membership();
		  $count_pendamping = $this->pendamping_m->get_pendamping();
			$pagination_config['base_url'] = base_url(). 'admin/laporan/pendamping/index/';
			$pagination_config['uri_segment'] = 5;
			$pagination_config['suffix'] = $surffix;
			$pagination_config['total_rows'] = count($count_pendamping);
			$pagination_config['per_page'] = 10;
			// $pagination_config['per_page'] = Settings::get('records_per_page');
			$this->pagination->initialize($pagination_config);
			$data['pagination_config'] = $pagination_config;
			
	    // -------------------------------------
			// Get entries
			// -------------------------------------
			
	  	// $data['pendamping']['entries'] = $this->memberships_m->get_membership($pagination_config);
	  	$data['pendamping']['entries'] = $this->pendamping_m->get_pendamping($pagination_config);
			$data['pendamping']['total'] = count($count_pendamping);
			$data['pendamping']['pagination'] = $this->pagination->create_links(null, 1);

		}

		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
		
		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		// Set Location
		$filter_kota = null;
  	if($this->input->get('f-provinsi') != '') {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
  	}

  	$data['tipes'] = $this->tipe_m->get_tipe();
  	$data['uri'] = $this->get_query_string(5);
		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:pendamping:singular'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:pendamping:singular'))
			->build('admin/pendamping_index2', $data);
  }

  public function download($print = 0){

		if(!group_has_role('users', 'manage_users') AND !group_has_role('users', 'manage_own_unit_users')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;

		$pendamping['entries'] = $this->pendamping_m->get_pendamping(NULL, NULL, NULL, TRUE);

		$laporan = "";
		if($this->input->get('f-tipe_laporan')){
			$laporan = $this->tipe_m->get_tipe_by_id($this->input->get('f-tipe_laporan'))['nama_laporan'];
		}
		$colspan = 10;

		$judul = "Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM) <br> melalui kegiatan Toko Tani Indonesia (TTI)";
		$nama_file = "Data Pendamping";

  	if($print == 0){
	  	$this->load->library('excel');
  		$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle('Daftar Pendamping');


			$align_center = array(
	      'alignment' => array(
	          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	      )
			);

			$border = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000')
          )
        ),
	    );


			$this->excel->getActiveSheet()->setCellValue('A1', 'Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM)');
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:J1');

			$this->excel->getActiveSheet()->setCellValue('A2', 'melalui kegiatan Toko Tani Indonesia (TTI)');
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A2:J2');

			$this->excel->getActiveSheet()->getStyle('A1:J2')->applyFromArray($align_center);

			$row = 3;
			if($this->input->get('f-provinsi') != ''){
	    	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	    	$kota = "";
	    	if($this->input->get('f-kota') != ''){
	    		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	    	}

	    	$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(13);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->mergeCells('A'.$row.':J'.$row);

				$this->excel->getActiveSheet()->getStyle('A'.$row.':J'.$row)->applyFromArray($align_center);
				$nama_file .= '_'.$provinsi.$kota;
			}


    	
			$row++;
			foreach ($pendamping['entries'] as $pendamping_entry){
        $row++;
        $frow_bold = $row;
        $rows = ($pendamping_entry['total_gap'] + $pendamping_entry['total_tti']) - 1;

        $this->excel->getActiveSheet()->mergeCells('A'.$row.':J'.$row);
        $this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$pendamping_entry['provinsi'].', '.$pendamping_entry['kota']);
        $this->excel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);
        $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				
				$row++;
				$row_border = $row;
				$this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Pendamping');

				$this->excel->getActiveSheet()->mergeCells('C'.$row.':F'.$row);
				$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Gapoktan');

				$this->excel->getActiveSheet()->mergeCells('G'.$row.':J'.$row);
				$this->excel->getActiveSheet()->setCellValue('G'.$row, 'TTI');

				foreach (range('A','J') as $columnID) {
					// $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
					$this->excel->getActiveSheet()->getColumnDimension($columnID)->setWidth('18');
				}

				$row++;
				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Nama');
				$this->excel->getActiveSheet()->setCellValue('B'.$row, 'No HP');
				$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Nama');
				$this->excel->getActiveSheet()->setCellValue('D'.$row, 'Ketua');
				$this->excel->getActiveSheet()->setCellValue('E'.$row, 'Alamat');
				$this->excel->getActiveSheet()->setCellValue('F'.$row, 'No HP');
				$this->excel->getActiveSheet()->setCellValue('G'.$row, 'Nama');
				$this->excel->getActiveSheet()->setCellValue('H'.$row, 'Pemilik');
				$this->excel->getActiveSheet()->setCellValue('I'.$row, 'Alamat');
				$this->excel->getActiveSheet()->setCellValue('J'.$row, 'No HP');

				$this->excel->getActiveSheet()->getStyle('A'.$frow_bold.':J'.$row)->getFont()->setBold(true);

        foreach ($pendamping_entry['gapoktan'] as $key2 => $gap) {
        	$row++; 
          $rows2 = (count($gap['tti']) > 1 ? count($gap['tti']) - 1 : count($gap['tti']));
          $this->excel->getActiveSheet()->mergeCells('A'.$row.':A'.($row+$rows2));
          $this->excel->getActiveSheet()->setCellValue('A'.$row, $gap['display_name']);
          $this->excel->getActiveSheet()->mergeCells('B'.$row.':B'.($row+$rows2));
          $this->excel->getActiveSheet()->setCellValue('B'.$row, $gap['telp']);
          $this->excel->getActiveSheet()->mergeCells('C'.$row.':C'.($row+$rows2));
          $this->excel->getActiveSheet()->setCellValue('C'.$row, $gap['unit_name']);
          $this->excel->getActiveSheet()->mergeCells('D'.$row.':D'.($row+$rows2));
          $this->excel->getActiveSheet()->setCellValue('D'.$row, $gap['nama_ketua']);
          $this->excel->getActiveSheet()->mergeCells('E'.$row.':E'.($row+$rows2));
          $this->excel->getActiveSheet()->setCellValue('E'.$row, $gap['unit_description']);
          $this->excel->getActiveSheet()->mergeCells('F'.$row.':F'.($row+$rows2));
          $this->excel->getActiveSheet()->setCellValue('F'.$row, $gap['hp_ketua']);
          if(count($gap['tti']) > 0){
            foreach ($gap['tti'] as $key3 => $tti) {
            	$this->excel->getActiveSheet()->setCellValue('G'.$row, $tti['unit_name']);
	          	$this->excel->getActiveSheet()->setCellValue('H'.$row, $tti['nama_ketua']);
	          	$this->excel->getActiveSheet()->setCellValue('I'.$row, $tti['unit_description']);
	          	$this->excel->getActiveSheet()->setCellValue('J'.$row, $tti['hp_ketua']);
              if($key3 < $rows2){
              	$row++;
              }
            }
          }else{
          	$this->excel->getActiveSheet()->setCellValue('G'.$row, '');
          	$this->excel->getActiveSheet()->setCellValue('H'.$row, '');
          	$this->excel->getActiveSheet()->setCellValue('I'.$row, '');
          	$this->excel->getActiveSheet()->setCellValue('J'.$row, '');
          	// $row++;
          } 
        }
        $this->excel->getActiveSheet()->getStyle('A'.$row_border.':J'.$row)->applyFromArray($border);
        $this->excel->getActiveSheet()->getStyle('A'.$row_border.':J'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
        $row++;
      }

  		header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
	  }else{
	  	$html ="
	    <table border=\"0\" cellpadding=\"5\">
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	      <tr>
	          <td colspan=\"".$colspan."\" align=\"center\">
	              <span style=\"font: bold 20px Open Sans; display: block;\">".$judul."</span>
	          </td>
	      </tr>";


	      if($id_provinsi != NULL){
			  	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
			  	$kota = "";
			  	if($this->input->get('f-kota') != ''){
			  		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
			  	}
					$html .= '
						<tr>
							<td colspan="'.$colspan.'" align="center">
								<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
							</td>
						</tr>
					';
					$nama_file .= '_'.$provinsi.$kota;
				}

	      $html .= "
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	    </table>";

	  	foreach ($pendamping['entries'] as $pendamping_entry){
        $rows = ($pendamping_entry['total_gap'] + $pendamping_entry['total_tti']) - 1;

        $html .= '
        <table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th colspan="10">
                <center>
                  Provinsi '.$pendamping_entry['provinsi'].'<br>
                  '.$pendamping_entry['kota'].'
                </center>
              </th>
            </tr>
            <tr>
              <th colspan="2" align="left">Pendamping</th>
              <th colspan="4" align="left">Gapoktan</th>
              <th colspan="4" align="left">TTI</th>
            </tr>
            <tr>
              <th align="left">Nama</th>
              <th align="left">No HP</th>
              <th align="left">Nama</th>
              <th align="left">Ketua</th>
              <th align="left">Alamat</th>
              <th align="left">No HP</th>
              <th align="left">Nama</th>
              <th align="left">Pemilik</th>
              <th align="left">Alamat</th>
              <th align="left">No HP</th>
            </tr>
          </thead>
          <tbody>
            <tr>';
              foreach ($pendamping_entry['gapoktan'] as $key2 => $gap) { 
                $rows2 = count($gap['tti']);
                $html .= '
                <td rowspan="'.$rows2.'">'.$gap['display_name'].'</td>
                <td rowspan="'.$rows2.'">'.$gap['telp'].'</td>
                <td rowspan="'.$rows2.'">'.$gap['unit_name'].'</td>
                <td rowspan="'.$rows2.'">'.$gap['nama_ketua'].'</td>
                <td rowspan="'.$rows2.'">'.$gap['unit_description'].'</td>
                <td rowspan="'.$rows2.'">'.$gap['hp_ketua'].'</td>';
                if(count($gap['tti']) > 0){
                  foreach ($gap['tti'] as $key3 => $tti) { 
                    if($key3 > 0){ 
                      $html .='<tr>';
                    }
                    $html .='
                    <td>'.$tti['unit_name'].'</td>
                    <td>'.$tti['nama_ketua'].'</td>
                    <td>'.$tti['unit_description'].'</td>
                    <td>'.$tti['hp_ketua'].'</td>';
                    if($key3 > 0){
                      $html .='</tr>';
                    }
                  }
                }else{
                	$html .='
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  </tr>';
                }
                if($key2 > 0){
                  $html .='</tr>';
                } 
              }
              $html .= '
            </tr>
          </tbody>
        </table><br>';
      }
	  	$data['html'] = $html;
	  	$this->load->view('laporan/admin/page_print', $data);
	  }
	}

	public function download_harian($print=0){
		if(!group_has_role('users', 'manage_users') AND !group_has_role('users', 'manage_own_unit_users')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;

		$pendamping['entries'] = $this->pendamping_m->get_pendamping(NULL, NULL, NULL, TRUE);

		$laporan = "";
		if($this->input->get('f-tipe_laporan')){
			$laporan = $this->tipe_m->get_tipe_by_id($this->input->get('f-tipe_laporan'))['nama_laporan'];
		}
		$colspan = 10;

		$judul = "Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM) <br> melalui kegiatan Toko Tani Indonesia (TTI)";
		$nama_file = "Data Pendamping";

  	if($print == 0){
	  	$this->load->library('excel');
  		$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle('Daftar Pendamping');


			$align_center = array(
	      'alignment' => array(
	          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	      )
			);

			$border = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000')
          )
        ),
	    );


			$this->excel->getActiveSheet()->setCellValue('A1', 'Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM)');
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:J1');

			$this->excel->getActiveSheet()->setCellValue('A2', 'melalui kegiatan Toko Tani Indonesia (TTI)');
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A2:J2');

			$this->excel->getActiveSheet()->getStyle('A1:J2')->applyFromArray($align_center);

			$row = 3;
			if($this->input->get('f-provinsi') != ''){
	    	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	    	$kota = "";
	    	if($this->input->get('f-kota') != ''){
	    		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	    	}

	    	$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(13);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->mergeCells('A'.$row.':J'.$row);

				$this->excel->getActiveSheet()->getStyle('A'.$row.':J'.$row)->applyFromArray($align_center);
				$nama_file .= '_'.$provinsi.$kota;
			}

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
			$this->excel->getActiveSheet()->setCellValue('B'.$row, 'Tipe Unit');
			$this->excel->getActiveSheet()->setCellValue('C'.$row, lang("location:provinsi:singular"));
			$this->excel->getActiveSheet()->setCellValue('D'.$row, lang("location:kota:singular"));
			$this->excel->getActiveSheet()->setCellValue('E'.$row, 'Nama Pendamping');
			$this->excel->getActiveSheet()->setCellValue('F'.$row, 'No HP Pendamping');
			$this->excel->getActiveSheet()->setCellValue('G'.$row, 'Nama Unit');
			$this->excel->getActiveSheet()->setCellValue('H'.$row, 'Ketua');
			$this->excel->getActiveSheet()->setCellValue('I'.$row, 'Alamat');
			$this->excel->getActiveSheet()->setCellValue('J'.$row, 'No HP Ketua');

			
			$this->excel->getActiveSheet()->getStyle('A'.$row.':J'.$row)->applyFromArray(
		    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'CCCCCC')
	        ),
		    )
			);

			$total =  $row + count($pendamping['entries']);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':J'.$total)->applyFromArray(
		    array(
	        'borders' => array(
	          'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	          )
	        ),
		    )
			);

			$no = 1;

			foreach ($pendamping['entries'] as $pendamping_entry){
  			$row++;
  			$this->excel->getActiveSheet()->setCellValue('A'.$row, $no++);
				$this->excel->getActiveSheet()->setCellValue('B'.$row, $pendamping_entry['type_name']);
				$this->excel->getActiveSheet()->setCellValue('C'.$row, $pendamping_entry['provinsi']);
				$this->excel->getActiveSheet()->setCellValue('D'.$row, $pendamping_entry['kota']);
				$this->excel->getActiveSheet()->setCellValue('E'.$row, $pendamping_entry['display_name']);
				$this->excel->getActiveSheet()->setCellValue('F'.$row, $pendamping_entry['telp']);
				$this->excel->getActiveSheet()->setCellValue('G'.$row, $pendamping_entry['unit_name']);
				$this->excel->getActiveSheet()->setCellValue('H'.$row, $pendamping_entry['nama_ketua']);
				$this->excel->getActiveSheet()->setCellValue('I'.$row, $pendamping_entry['unit_description']);
				$this->excel->getActiveSheet()->setCellValue('J'.$row, $pendamping_entry['hp_ketua']);
  		}

  		header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
	  }else{
	  	$html ="
	    <table border=\"0\" cellpadding=\"5\">
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	      <tr>
	          <td colspan=\"".$colspan."\" align=\"center\">
	              <span style=\"font: bold 20px Open Sans; display: block;\">".$judul."</span>
	          </td>
	      </tr>";


	      if($id_provinsi != NULL){
			  	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
			  	$kota = "";
			  	if($this->input->get('f-kota') != ''){
			  		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
			  	}
					$html .= '
						<tr>
							<td colspan="'.$colspan.'" align="center">
								<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
							</td>
						</tr>
					';
					$nama_file .= '_'.$provinsi.$kota;
				}

	      $html .= "
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	    </table>";

	  	$html .= '
		  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
		  		$html .='
		      <thead style="background-color:#ccc;">
		        <tr>
		        	<th>No</th>
		        	<th>Tipe Unit</th>
		        	<th>'.lang("location:provinsi:singular").'</th>
		        	<th>'.lang("location:kota:singular").'</th>
					   	<th>Nama Pendamping</th>
					   	<th>No HP Pendamping</th>
					   	<th>Nama Unit</th>
					   	<th>Ketua</th>
					   	<th>Alamat</th>
					   	<th>No HP Ketua</th>';
					   	$html .= '
		  			</tr>
		  		</thead>
		  		<tbody>';
		  		$no = 1;
		  		foreach ($pendamping['entries'] as $pendamping_entry){
		  			$html .='
		  				<tr>
		  					<td>'.$no++.'</td>
		  					<td>'.$pendamping_entry['type_name'].'</td>
		  					<td>'.$pendamping_entry['provinsi'].'</td>
		  					<td>'.$pendamping_entry['kota'].'</td>
		  					<td>'.$pendamping_entry['display_name'].'</td>
						  	<td>'.$pendamping_entry['telp'].'</td>
						  	<td>'.$pendamping_entry['unit_name'].'</td>
						  	<td>'.$pendamping_entry['nama_ketua'].'</td>
						  	<td>'.$pendamping_entry['unit_description'].'</td>
						  	<td>'.$pendamping_entry['hp_ketua'].'</td>';
						   	$html .= '
		  				</tr>
		  			';
		  		}
		  		$html .='
		  		</tbody>
		  	</table>';
	  	$data['html'] = $html;
	  	$this->load->view('laporan/admin/page_print', $data);
	  }
	}

	public function get_query_string($page){
		// -------------------------------------
		// Get query string
		// -------------------------------------

		$page = $this->uri->segment($page);
		$uri = '';
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }
    
    return $uri;
	}

	// --------------------------------------------------------------------------
	// Import
	// --------------------------------------------------------------------------

	public function import(){
		$uri = $this->get_query_string(5);
		if($_POST){
			if($this->_import_pendamping()){	
				$this->session->set_flashdata('success', lang('laporan:pendamping:submit_success'));
				redirect('admin/laporan/pendamping/index/'.$uri);
			}else{
				$data['messages']['error'] = lang('laporan:pendamping:submit_failure');
			}
		}

		$data['uri'] = $uri;
		$data['return'] = 'admin/laporan/pendamping/index/'. $uri;

		$this->template->title(lang('laporan:pendamping:import'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:pendamping:plural'), '/admin/laporan/pendamping/index')
			->set_breadcrumb(lang('laporan:pendamping:import'))
			->build('admin/pendamping_import', $data);

	}

	private function _import_pendamping()
	{
		global $input_user_enum;
		$this->load->library('form_validation');

		$this->form_validation->set_rules('upload', 'pendamping', 'callback__upload_csv_pendamping');

    $result = false;

		if ($this->form_validation->run() === true)
		{
			$f_group = $this->input->get('f-groups');
			foreach ($input_user_enum as $data) {

    		$this->db->select('p.user_id');
    		$this->db->from('default_profiles p');
    		$this->db->join('default_users u','p.user_id = u.id');
    		$this->db->where('p.display_name', $data['display_name']);
    		$this->db->where('p.telp', $data['telp']);
    		$this->db->where('u.group_id', $f_group);
    		$query = $this->db->get();
    		if($query->num_rows() > 0){
    			$user = $query->row_array();
    			$user_id = $user['user_id'];
    		}else{
					$password = $data['telp'];
					$values['email'] = '-';
	        $values['salt'] = $this->ion_auth_model->salt($password);
	        $values['password'] = $this->ion_auth_model->hash_password($password,$values['salt']);
	        $values['username'] = $data['telp'];
	        $values['group_id'] = $f_group;
	        $values['active'] = 1;
	        $values['created_on'] = time();
	        $values['last_login'] = 0;
	        $values['display_name'] = $data['display_name'];
	        $values['telp'] = $data['telp'];
	        $values['alamat'] = $data['alamat'];

	        $retval = $this->ion_auth_model->import_insert($values);
	  			if($retval == TRUE){
	  				$user_id = $this->db->insert_id();
	  				$this->ion_auth_model->import_profile($user_id, $values);
	  			}
	  		}

	  		$this->memberships_m->create_membership($user_id, $data['id_unit']);
	  		$result = true;
			}
		}
		
		return $result;
	}

	public function _upload_csv_pendamping()
	{
		global $input_user_enum;
		$config['upload_path'] = 'uploads/temp/';
    $config['allowed_types'] = 'csv';
    $config['file_name'] = 'data.csv';
    $config['overwrite'] = true;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('file')) 
    {
	    $filename = $config['upload_path'] . $config['file_name'];
	    $withHeader = true;
	    $csv = fopen($filename, FOPEN_READ);
	    $i = 0;
	    $errors = array();

	    while (($row = fgetcsv($csv,0,';'))) 
	    {
        $i++;
        if ($withHeader) {
            $withHeader = false;
            continue;
        }
        
        if($row[0] == "" && $row[1]=="" && $row[2]=="" && $row[3]=="" && $row[4]=="" && $row[5]==""){
        	continue;
        }

        // Cek Provinsi
        $id_provinsi = NULL;
      	if($row[0] == "")
      	{
          $errors[] = "Baris {$i}: <b>Provinsi</b> belum diisi.";
        }else{
	        $this->db->where('nama', $row[0]);
	        $query = $this->db->get('default_location_provinsi');
	        if($query->num_rows() > 0){
	        	$provinsi = $query->row_array();
	        	$id_provinsi = $provinsi['id'];
	        }else{
	        	$errors[] = "Baris {$i}: Provinsi <b>".$row[0]."</b> tidak terdaftar";
	        }
	      }
        
        // Cek Kota
        $id_kota = NULL;
        if($row[1] == "")
      	{
          $errors[] = "Baris {$i}: <b>Kota/Kabupaten</b> belum diisi.";
        }else{
	        $this->db->where('nama', $row[1]);
	        $this->db->where('id_provinsi', $id_provinsi);
	        $query = $this->db->get('default_location_kota');
	        if($query->num_rows() > 0){
	        	$kota = $query->row_array();
	        	$id_kota = $kota['id'];
	        }else{
	        	$errors[] = "Baris {$i}: Kota <b>".$row[1]."</b> pada provinsi <b>".$row[0]."</b> tidak terdaftar";
	        }
	      }

	      $id_unit = NULL;
	      if($row[2] == ""){
      		$errors[] = "Baris {$i}: <b>Unit</b> tidak boleh kosong";
      	}else{
      		$this->db->where('unit_name', $row[2]);
        	$this->db->where('id_kota', $id_kota);
        	// if($row[6] != ''){
        	// 	$this->db->where('nama_ketua', $row[6]);
        	// }
	        $query = $this->db->get('default_organization_units');
	        if($query->num_rows() > 0){
	        	$unit = $query->row_array();
	        	$id_unit = $unit['id'];
	        }else{
	        	$errors[] = "Baris {$i}: Unit <b>'".$row[2]."'</b> tidak terdaftar di kota <b>".$row[1]."</b>";
	        }
      	}

      	// $id_user = NULL;
      	if($row[3] == ""){
      		$errors[] = "Baris {$i}: <b>Nama Pendamping</b> tidak boleh kosong";
      	}


	      // Cek No Telp

	      if($row[4] == ""){
	      	$errors[] = "Baris {$i}: <b>No HP Pendamping</b> tidak boleh kosong";
	      }else{
	        $row[4] = str_replace(" ", "", $row[4]);
	        $row[4] = str_replace("+62","0", $row[4]);
	        if(substr($row[4], 0,1) == 8){
	        	$row[4] = '0'.$row[4];
	        }
	        if(strlen($row[4]) < 8 || strlen($row[4]) > 15){
	      		$errors[] = "Baris {$i}: No HP tidak boleh kurang dari 8 digit dan tidak boleh lebih dari 15 digit";
	      	}
	      }

      	$input_user_enum[] = array(
        	'id_provinsi' => $id_provinsi,
        	'id_kota' => $id_kota,
        	'id_unit' => $id_unit,
        	'display_name' => $row[3],
        	'telp' => $row[4],
        	'alamat' => $row[5],
        );
        
       
      }

      fclose($csv);
      unlink($filename);

      if (count($errors)) {
          $this->form_validation->set_message('_upload_csv_pendamping', 'Terjadi kesalahan data pada berkas.<br />' . implode('<br />', $errors));
          return false;
      } else {
          return true;
      }
	  }
	  else
	  {
		  $this->form_validation->set_message('_upload_csv_pendamping', $this->upload->display_errors('', ''));
	    return false;
	  }
	}

	public function csv_example()
	{
		$this->load->helper('download');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=contoh_data_pendamping.csv");
		header("Pragma: no-cache");
		header("Expires: 0");

    $data = 'Provinsi,Kabupaten/Kota,Unit,Nama Pendamping,No HP Pendamping,Alamat
Sulawesi Barat,Kab. Polewali Mandar,Gap Sakina,Sakinah M.r,86543678765,Desa Sulaiman Komp Asri Raya
Sulawesi Barat,Kab. Polewali Mandar,Toko Indah Mulia,Indah Lestari,86425674653,Jl. Ambon Seram Wahai';
    $name = 'contoh_data_pendamping.csv';

    force_download($name, $data);
	}
	// --------------------------------------------------------------------------

}