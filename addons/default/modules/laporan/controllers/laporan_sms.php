<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Laporan_sms extends Public_Controller 
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------

  public function __construct()
  {
    parent::__construct();

    date_default_timezone_set('Asia/Jakarta');
    header ("Content-Type: text/plain");
    // -------------------------------------
		// Load everything we need
		// -------------------------------------
		$this->load->model('metadata_m');
		$this->load->model('komoditas_m');
		$this->load->model('pendamping_m');
		$this->load->model('tipe_m');
		$this->load->model('absen_m');
		$this->load->model('data_m');
		$this->load->model('sms_m');
		$this->load->model('dialog_m');
		$this->load->model('location/kota_m');
		$this->load->model('sms_gateway_api/inbox_m');
		$this->load->model('sms_gateway_api/outbox_m');
		$this->load->helper('laporan');

		$this->load->config('../../sms_gateway_api/config/apiconfig', true); 
    $this->apiKeyAccess = $this->config->item('apiKeyAccess', 'apiconfig');
  }

  public function process()
  {
  	// echo '<pre>';
  	$apiKeyAccess = $this->input->get('apiKeyAccess');
  	if($apiKeyAccess){
  		$getApiKeyAccess = $this->input->get('apiKeyAccess');
  		if($getApiKeyAccess == $this->apiKeyAccess){
  			$get_inbox = $this->inbox_m->get_inbox_for_parsing();
				// $get_inbox = $this->readinbox_demo();
				if(count($get_inbox) > 0){
					$types = $this->tipe_m->get_tipe();
					foreach ($types as $key => $tipe) { 
						$code_types[$tipe['id']] = $tipe['kode_sms'];
					}

					foreach ($get_inbox as $key => $inbox) {
						$status = 'error';
						if($this->sms_m->check_kode($inbox['id'])){ 
							$no_hp = $inbox['sender'];
							$sms = $inbox['sms'];

							$pemisah = '#';
							if(isset($_GET['testing'])){
								$pemisah = '-';
							}
							
							if($this->pendamping_m->cek_no_hp($no_hp)){
								// continue;
								$new_id_sms = $this->sms_m->get_new_id();
								$insert_sms['id'] = $new_id_sms;
								$insert_sms['kode'] = $inbox['id'];
								$insert_sms['no_hp'] = $no_hp;
								$insert_sms['isi'] = $sms;
								$insert_sms['tanggal'] = date('Y-m-d H:i:s');
								$insert_sms['status_error'] = '';
								$insert_sms['keterangan_parsing'] = '';
								$this->sms_m->insert_sms($insert_sms); 

								$explode_spasi = explode(' ',trim($sms));
								if(count($explode_spasi) > 3){
									$spasi1 = $explode_spasi[0];
									unset($explode_spasi[0]);
									$replace_spasi = implode(' ',$explode_spasi);
									$replace_spasi = str_replace(' ', '', $replace_spasi);
									$sms = $spasi1.' '.$replace_spasi;
									$explode_spasi = explode(' ',$sms);
								}

								// cek jumlah kata
								// if(count($explode_spasi) == 2){
									$explode_spasi[0] = explode('-',$explode_spasi[0]);
									$type_code = strtoupper($explode_spasi[0][0]);
									// cek kode panel
									if(in_array($type_code, $code_types)){
										$metadatas = $explode_spasi[2];
										$id_tipe = array_keys($code_types, $type_code)[0];

										$tipe_laporan = $id_tipe;
										if($id_tipe == 3 || $id_tipe == 4){
											$tipe_laporan = 1;
										}
										if($id_tipe == 5){
											$tipe_laporan = 2;
										}

										$kode_tti = NULL;
										if(($id_tipe == 2 || $id_tipe == 3) && isset($explode_spasi[0][1])){
											$kode_tti = $explode_spasi[0][1];
										}

										$nama_laporan = $this->tipe_m->get_tipe_by_id($id_tipe)['nama_laporan'];

										$pendamping = $this->pendamping_m->get_pendamping_by_phone($id_tipe, $no_hp);

										// cek pendamping
										if(count($pendamping) > 0){
											$true_kode_tti = false;
											if(count($pendamping) == 1){
												$id_user = $pendamping[0]['user_id'];
												$id_unit = $pendamping[0]['id_unit'];
												$id_grup = $pendamping[0]['id_grup'];
												$true_kode_tti = true;
											}else{
												$abjad = array('A','B','C','D','E','F','G','H','I');
												if(in_array($kode_tti, $abjad)){
													$key_arr = array_search($kode_tti, $abjad);
													if(isset($pendamping[$key_arr])){
														$id_user = $pendamping[$key_arr]['user_id'];
														$id_unit = $pendamping[$key_arr]['id_unit'];
														$id_grup = $pendamping[$key_arr]['id_grup'];
														$true_kode_tti = true;
													}
												}
											}
										
											if($true_kode_tti){
												$commodities = $this->komoditas_m->get_komoditas();
												foreach ($commodities as $key => $commodity) { 
													$code_comodities[$commodity['id']] = $commodity['kode_komoditas'];
												}

												$comodity_code = strtoupper($explode_spasi[1]);
												// cek kode komoditas
												if(in_array($comodity_code, $code_comodities)){
													$id_komoditas = array_keys($code_comodities, $comodity_code)[0];

													$explode_metadatas = explode($pemisah,$metadatas);
													if(count($explode_metadatas)>1){
														$tanggal = $explode_metadatas[0];
														$validate_tgl = true;
														if(strlen($tanggal) != 6 or !is_numeric($tanggal)){
															$validate_tgl = false;
														}else{
															$tgl = substr($tanggal,0,2);
															$bln = substr($tanggal,2,2);
															$default_thn = substr(date('Y'),0,2);
															$thn = $default_thn."".substr($tanggal,4,2);
															$tanggal = $thn."-".$bln."-".$tgl;
															$today = date('Y-m-d H:i:s');
															if(!checkdate($bln, $tgl, $thn) or $bln != date('m') or $thn != date('Y')){
																$validate_tgl = false;
															}
														}

														if($validate_tgl){
															$absen = $this->absen_m->cek_tgl_input($today, $id_tipe);
															if($id_tipe > 2){
																$absen_tgl = date('Y-m-d');
															}else{
																$absen_tgl = $absen['tanggal'];
															}
															if(count($absen) > 0 || $id_tipe > 2){
																if($absen_tgl == $tanggal){
																	unset($explode_metadatas[0]);
																	$values = $explode_metadatas;
																	$metadata_by_tipe = $this->metadata_m->get_metadata_by_tipe_laporan($tipe_laporan, $id_komoditas,'default_laporan_metadata.*',NULL,'urutan');
																	if(count($metadata_by_tipe) == count($values)){
																		$true_value = true;
																		foreach ($values as $key => $value) {
																			if(!is_numeric($value) || $value < 0 || strlen($value) > 10){
																				$true_value =  false;
																				break;
																			}else{
																				$arr_values[$key] = $value;
																			}
																		}

																		if($true_value){
																			$error = array();
																			//$ex_id_prov = array(33,34);
																			//$id_provinsi = $this->kota_m->get_kota_by_id($id_kota)['id_provinsi'];
																			foreach ($metadata_by_tipe as $key => $metadata) {
																				$val_hrg = $arr_values[$key+1];

																				// if(!in_array($id_provinsi, $ex_id_prov)){
																					$nilai_min = ($metadata['nilai_minimal'] != "") ? $metadata['nilai_minimal'] : 0;
																					$nilai_max = ($metadata['nilai_maksimal'] != "") ? $metadata['nilai_maksimal'] : 1000000000;
																				// }else{
																				// 	$nilai_min = 0;
																				// 	$nilai_max = 1000000000;
																				// }

																				if($val_hrg < $nilai_min || $val_hrg > $nilai_max){
																					$error[] = "Field ".$metadata['field']." tidak boleh kurang dari ".$nilai_min." dan tidak boleh lebih dari ".$nilai_max;
																				}
																				if(count($error) > 0){
																					$true_value = false;
																				}
																			}
																			if(!$true_value){
																				$ket_parsing = implode(", ", $error);
																			}
																		}else{
																			$ket_parsing = "Format nilai harus diisi dengan angka dan tidak boleh lebih dari 10 digit.";
																		}

																		// cek jika value itu angka atau bukan
																		if($true_value){
																			foreach ($metadata_by_tipe as $key => $metadata) {
																				$arr_metadata[$key]['id'] = $metadata['id'];
																				$arr_metadata[$key]['value'] = $arr_values[$key+1];
																			}

																			$minggu_ke = NULL;
																			if($id_tipe > 2){
																				$weeks = getWeeks(date('Y-m-d'), 'monday');
																				$minggu_ke = $weeks['week'];
																			}

																			$is_ttic = ($id_tipe == 3) ? 1 : 0;
																			$wheres['id_unit'] = $id_unit;
																			$wheres['id_user'] = $id_user;
																			$wheres['is_ttic'] = $is_ttic;
																			if($id_tipe < 3){
																				$wheres['id_absen'] = $absen['id'];
																			}else{
																				$wheres['date(created_on)'] = date('Y-m-d');
																			}

																			$cek_input = $this->data_m->get_data_input_by_user($tipe_laporan, $id_komoditas, $wheres);
																			if(count($cek_input) > 0){
																				$this->data_m->delete_data_input_by_user($tipe_laporan, $id_komoditas, $wheres);
																			}
																			$data['id_sms'] = $new_id_sms;
																			foreach ($arr_metadata as $key => $value) {
																				$data['value'] = $value['value'];
																				$data['channel'] = 'sms';
																				$data['id_metadata'] = $value['id'];
																				if($id_tipe < 3){
																					$data['id_absen'] = $absen['id'];
																				}else{
																					$data['minggu_ke'] = $minggu_ke;
																				}
																				$data['id_user'] = $id_user;
																				$data['id_unit'] = $id_unit;
																				$data['id_group'] = $id_grup;
																				$data['is_ttic'] = $is_ttic;
																				$this->data_m->insert_data($data);
																			}
																			// $ket_parsing = "Terimakasih telah mengirim SMS PUPM.";
																			$ket_parsing = "Berhasil mengirim SMS PUPM.";
																			$status = 'success';
																		}
																	}else{
																		// aksi jika format harga tidak sesuai
																		$ket_parsing = "Format SMS yang anda kirim salah, mohon segera diperbaiki dan dikirim kembali. Terimakasih.";
																	}
																}else{
																	// aksi jika absen salah
																	$ket_parsing = "Anda mengirimkan data SMS bukan untuk hari ini.";
																}
															}else{
																// aksi jika tanggal yang diinput tidak sesuai dengan tanggal sms
																$ket_parsing = "SMS anda ditolak, mohon dikirim pada waktu yang sudah ditentukan.";
															}
														}else{
															// aksi jika format tanggal tidak sesuai
															$ket_parsing = "Format tanggal tidak sesuai, mohon segera diperbaiki dan dikirim kembali. Terimakasih.";
														}
													}else{
														// aksi jika format tanggal tidak sesuai
														$ket_parsing = "Format SMS yang anda kirim salah, mohon segera diperbaiki dan dikirim kembali. Terimakasih.";
													}
												}else{
													$ket_parsing = "Kode komoditas yang anda kirim tidak terdaftar.";
												}
											}else{
												$ket_parsing = "Anda belum menambahkan kode ".($id_tipe == 3 ? 'Gapoktan' : 'TTI').".";
											}

										}else{
											// aksi jika enumerator tidak terdaftar
											$ket_parsing = "No anda tidak terdaftar sebagai pendamping '".$nama_laporan."'";
										}

									}elseif($type_code == "DIALOG"){
										$user = $this->pendamping_m->get_user_by_telp($no_hp);
										$status = 'success';
										// $ket_parsing = "Terimakasih telah mengirim SMS Dialog.";
										$ket_parsing = "Berhasil mengirim SMS Dialog.";

									}else{
										// aksi yang dilakukan ketika kode panel harga salah
										$ket_parsing = "Anda mengirimkan data SMS dengan kode yang tidak terdaftar disistem, mohon segera diperbaiki dan dikirim kembali. Terimakasih.";
									}

									$val_sms['status_error'] = $status;
									$val_sms['keterangan_parsing'] = $ket_parsing;
									$this->sms_m->update_sms($val_sms, $new_id_sms);

									if($type_code == "DIALOG"){
										$id_sms = $this->db->insert_id();
										$dialog['nama'] = $user['display_name'];
										$dialog['email'] = $user['email'];
										$dialog['kontak'] = $no_hp;
										$dialog['isi'] = substr($sms, 6);
										$dialog['id_sms'] = $id_sms;
										$dialog['status'] = 0;
										$dialog['created_on'] = date('Y-m-d H:i:s');
										// $this->dialog_m->insert_dialog($dialog);
									}

								// }else{
									// aksi yang dilakukan ketika format salah (!= 2 kata)
									// $ket_parsing = "Format SMS yang anda kirim salah, pastikan <spasi> tidak boleh kurang atau lebih dari satu. Terimakasih.";
								// }
									
								// if($status != 'success'){
									// $outbox['sms'] = $ket_parsing;
									// $outbox['destination'] = $no_hp;
									// $outbox['InsertIntoDB'] = date('Y-m-d H:i:s');
									// // $outbox['status'] = $status;
									// $this->outbox_m->insert_outbox($outbox);

									// $update_inbox[t'status'] = 'executed';
									// $update_inbox['ExecutedTime'] = date('Y-m-d H:i:s');
									// $this->inbox_m->update_inbox($update_inbox, $inbox['id']);
									
									// echo $ket_parsing;
								// }
							}else{
								// aksi yang dilakukan ketika no hp tidak terdaftar
								$ket_parsing = "Anda tidak terdaftar dalam SMS PUMP.";
								// echo $ket_parsing;
							}

							$outbox['sms'] = $ket_parsing;
							$outbox['destination'] = $no_hp;
							$outbox['InsertIntoDB'] = date('Y-m-d H:i:s');
							// $outbox['status'] = $status;
							$this->outbox_m->insert_outbox($outbox);

							$update_inbox['status'] = 'executed';
							$update_inbox['ExecutedTime'] = date('Y-m-d H:i:s');
							$this->inbox_m->update_inbox($update_inbox, $inbox['id']);

							echo json_encode($outbox);
						}
					}
				}
			}
		}
	} 

	public function readinbox_demo(){
		// $data[] = array('id'=> 1234567, 'sms'=>'PANELPRD 180216#10#2000#3000#4000#5000#6000#7000#8000#9000#10000#110#120', 'sender'=>'082367055040', 'waktu'=>date('Y-m-d H:i:g'));
		// $data[] = array('id'=> 1234568, 'sms'=>'PANELPPG 180216#1000#10#2000#20#3000#30#4000#40#5000#50#6000#60#7000#70#8000#80#9000#90#10000#100#11000#110#12000', 'sender'=>'086374845945', 'waktu'=>date('Y-m-d H:i:g'));
		$data[] = array('id'=> rand(), 'sms'=>'GAP17 BRS 030417#31#2700#39#3400', 'sender'=>'08234827342', 'waktu'=>date('Y-m-d H:i:s'));
		// $data[] = array('id'=> rand(), 'sms'=>'GAP BWG 030417#31#2700', 'sender'=>'082277718971', 'waktu'=>date('Y-m-d H:i:s'));
		// $data[] = array('id'=> rand(), 'sms'=>'TTIC-B BWG 030417#31#2700', 'sender'=>'0823478349', 'waktu'=>date('Y-m-d H:i:s'));
		// $data[] = array('id'=> rand(), 'sms'=>'DIALOG Kapan ada absen lagi ya?', 'sender'=>'085759167701', 'waktu'=>date('Y-m-d H:i:s'));
		return $data;
	}
	// --------------------------------------------------------------------------

}