<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Laporan_data extends Public_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
	protected $section = 'data';
	
	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set('Asia/Jakarta');
		ini_set('max_execution_time', '3600');

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('laporan');		
    // $this->lang->load('enumerator/enumerator');		
		$this->load->model('data_m');
		$this->load->model('tipe_m');
		$this->load->model('metadata_m');
		$this->load->model('pendamping_m');
		$this->load->model('komoditas_m');
		// $this->load->model('enumerator/enumerator_m');
		$this->load->model('absen_m');

		$this->lang->load('location/location');	
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');

		// Organization
		
		$this->load->library('organization/organization');
		$this->load->model('organization/types_m');
		$this->load->model('organization/units_m');
		$this->load->model('organization/memberships_m');

		$this->config->load('organization/organization');
		$this->node_type_slug = $this->config->item('node_type');
		$this->get_type = $this->types_m->get_type_by_slug($this->node_type_slug);
		$this->node_type_level = $this->get_type->type_level;
		$this->node_type_slug = $this->get_type->type_slug;

		$this->unit = $this->memberships_m->get_one_unit_by_member(1);

	}

	public function harga($id, $download = null)
	{
		$this->load->helper('laporan');


		$data['id'] = $id;
		$id_tipe_laporan = $id;
		$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;
		$wheres['d.is_ttic'] = $is_ttic;
		$wheres_akumulasi['d.is_ttic'] = $is_ttic;
		$gap_type_ids = array(3, 4, 6);
		if(in_array($id_tipe_laporan, $gap_type_ids)){
			$id_tipe_laporan = 1;
		}
		$tti_type_ids = array(5, 7);
		if(in_array($id_tipe_laporan, $tti_type_ids)){
			$id_tipe_laporan = 2;
		}
		$data['id_laporan_tipe'] = $id_tipe_laporan;
		$wheres['m.id_laporan_tipe'] = $id_tipe_laporan;
		$wheres_akumulasi['m.id_laporan_tipe'] = $id_tipe_laporan;

		// Set hari
		$hari = ($this->input->get('f-hari')) ? $this->input->get('f-hari') : date('Y-m-d');
    //--------------------------------------------------------------------------------------------------------

		// Set default periode
		$weeks = getWeeks($hari, 'monday');
		// dump($weeks);
		// die();
    //--------------------------------------------------------------------------------------------------------

		// Set Tahun
		$tahun = NULL;
		if($this->input->get('f-tahun')){
			$tahun = $this->input->get('f-tahun');
		}else{
			$tahun = $weeks['tahun'];
		}

		$data['tahun'] = $tahun;

		$data['min_year'] = $this->absen_m->get_min_year(); 
		$data['max_year'] = $this->absen_m->get_max_year();

		//--------------------------------------------------------------------------------------------------------

		// Set Bulan
		if($this->input->get('f-bulan')){
			$bulan = $this->input->get('f-bulan');
		}else{
			$bulan = $weeks['bulan'];
		}

		// $bulan = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
		$bulan = (strlen($bulan) == 1) ? '0'.$bulan : $bulan;
		$thn_bln = $tahun."-".$bulan;

		if(isset($_GET['f-bulan'])){
			if($_GET['f-bulan'] != ""){
				$bulan = $this->input->get('f-bulan');
			}else{
				$bulan = '';
			}
		}
			
		if($bulan != ''){
			$bulan = $bulan + 0;
		}

		$data['bulan'] = $bulan;

		$data['tahun_bulan'] = date_idr($tahun.'-'.$bulan, 'F Y', null);
    //--------------------------------------------------------------------------------------------------------

		// Set Jumlah Minggu
		$first_day_of_month = $thn_bln.'-01';

		$count_of_week = get_count_of_week($first_day_of_month);
		$data['count_of_week'] = $count_of_week;
    //--------------------------------------------------------------------------------------------------------

		// Minggu ke
		if($this->input->get('f-minggu_ke')){
			$minggu_ke = $this->input->get('f-minggu_ke');
		}else{
			$minggu_ke = (string) $weeks['week'];
		}

		if(isset($_GET['f-minggu_ke'])){
			if($_GET['f-minggu_ke'] != ""){
				$minggu_ke = $this->input->get('f-minggu_ke');
			}else{
				$minggu_ke = '';
			}
		}

		$data['minggu_ke'] = $minggu_ke;
    //--------------------------------------------------------------------------------------------------------


		$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke);
		$weeks = getWeeks($firstDayOfWeek, 'monday');

    // Data Komoditas
		$data['komoditas_related'] = $this->komoditas_m->get_komoditas_related_by_tipe($id_tipe_laporan);
		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
		$wheres['m.id_laporan_komoditas'] = $id_komoditas;
		$wheres_akumulasi['m.id_laporan_komoditas'] = $id_komoditas;
		$metadatas = $this->metadata_m->get_metadata_by_tipe_laporan($id_tipe_laporan, $id_komoditas, 'default_laporan_metadata.*', NULL, 'urutan');
		$data['metadata'] = $metadatas;
		$metadatas2 = $this->metadata_m->get_metadata_by_tipe_laporan2($id_tipe_laporan, $id_komoditas, 'default_laporan_metadata.*', NULL, 'urutan');
		$data['metadata2'] = $metadatas2;

		if($this->input->get('f-metadata')){
			$wheres['m.id'] = $this->input->get('f-metadata');
			$wheres_harian['m.id'] = $this->input->get('f-metadata');
			$wheres_akumulasi['m.id'] = $this->input->get('f-metadata');
		}
    //--------------------------------------------------------------------------------------------------------
		
		// Set Location
		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			
		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		// Set Location
		$filter_kota = null;
  	if($this->input->get('f-provinsi')) {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);

			$wheres['k.id_provinsi'] = $this->input->get('f-provinsi');
			$wheres_harian['k.id_provinsi'] = $this->input->get('f-provinsi');
			$wheres_akumulasi['k.id_provinsi'] = $this->input->get('f-provinsi');
  	}

  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		if($this->input->get('f-kota')){
			$wheres['k.id'] = $this->input->get('f-kota');
			$wheres_harian['k.id'] = $this->input->get('f-kota');
			$wheres_akumulasi['k.id'] = $this->input->get('f-kota');
		}

	 	if($this->input->get('id_organization_unit') > 0){
    	$wheres['d.id_unit'] = $this->input->get('id_organization_unit');
    	$wheres_harian['d.id_unit'] = $this->input->get('id_organization_unit');
    	$wheres_akumulasi['d.id_unit'] = $this->input->get('id_organization_unit');
		}

		// -----------------------------------------------------------------------------------

		// -------------------------------------
    // Get Organization by Member
    // -------------------------------------

		if($download == NULL){
			$data['types'] = $this->types_m->get_all_types_front(NULL, $id);
			$unit_id = $this->unit['id'];

			$types = $data['types'];
			$organization = array();
			$addedLevel = array();
			$n2 = 0;
			$n = count($types);

			foreach($types as $i => $type){
				if (in_array($type['level'], $addedLevel)) {
					continue;
				}
				$addedLevel[] = $type['level'];
				$units = $this->units_m->get_unit_by_child($unit_id);

				if(isset($units['id'])){
					if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
						$organization['organization_name_'.$units['type_level']] = $units['unit_name'];
						$organization['id_organization_unit_'.$units['type_level']] = $units['id'];
						$n2++;
					} else if ($units['type_slug'] == $this->node_type_slug) {
						$organization['organization_name_'.$units['type_level']] = $units['unit_name'];
						$organization['id_organization_unit'] = $units['id'];
					}
					$unit_id = $units['id'];
				}
			}

			if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
				if($this->unit['type_slug'] == $this->node_type_slug){
					$organization['id_organization_unit'] = $this->unit['id'];
				}else{
					$organization['id_organization_unit_'.$n2] = $this->unit['id'];
				}
				$organization['organization_name_'.$n2] = $this->unit['name'];
			}

	      // khusus sitoni
			$data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
	      //---------------------------------
			$data['user_unit_level'] = $this->unit['type_level'];
			$data['node_type_level'] = $this->node_type_level;
			$data['node_type_slug'] = $this->node_type_slug;
			$data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

			$get_units = $this->memberships_m->get_units_by_member(1);
			if(count($get_units) > 1 && $id == 2){
				unset($organization['id_organization_unit']);
				unset($organization['organization_name_1']);
			}

			if(($id == 5 || $id == 7) && count($get_units) > 1){
	    	$is_tti17 = true;
	    	unset($organization['id_organization_unit']);
	    	unset($organization['organization_name_0']);
	    }

      $data['organization'] = $organization;
			if(count($get_units) > 1 && $id == 2){
      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
      }else{
        if(($id == 5 || $id == 7) && count($get_units) > 1){
          $data['child_organization'] = $get_units;
        }
    	}
		}

		// if($id > 2){
		// 	$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
		// 	$week_of_today = getWeeks(date('Y-m-d'), 'monday');
		// 	if($week_of_today['week'] == $minggu_ke && $week_of_today['bulan'] == $bulan && $week_of_today['tahun'] == $tahun){
		// 		$day_looper = date('Y-m-d');
		// 	}else{
		// 		$weeks = getWeeks($firstDayOfWeek, 'monday');
		// 		$day_looper = $weeks['last_day_of_week'];
		// 	}
		// 	for ($h=0; $h <=6 ; $h++) {
		// 		$add_day2 = date('Y-m-d', strtotime('-'.$h.' days', strtotime($day_looper))); 
		// 		$data['days'][] = $add_day2;
		// 		$data['str_days'][] =  ($add_day2 == date('Y-m-d') ? 'Hari ini' : date_idr($add_day2, 'd F', null));
		// 	}
		// 	$data['days'] = array_reverse($data['days']);
		// 	$data['str_days'] = array_reverse($data['str_days']);
		// }else{
		// 	$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
		// }

		// $firstDayOfWeek = $weeks['first_day_of_week'];

		$data['firstDayOfWeek'] = $firstDayOfWeek;

		$nama_laporan = $this->tipe_m->get_tipe_by_id($id)['nama_laporan'];
		$data['nama_laporan'] = $nama_laporan;
		$data['weeks'] = $weeks;
		if($firstDayOfWeek != NULL){
			$weeks = getWeeks($firstDayOfWeek, 'monday');
			$data['weeks'] = $weeks;
			// Set interval waktu
			$start = 0;
			$end = 35;
			$intervals = get_interval($firstDayOfWeek, $start, $end);
			$revese_intervals = array();
			$interval_month = array();
			$interval_week = array();
			$interval_year = array();
			foreach ($intervals as $key => $value) {
				$interval_month[$value['bulan']] = $value['bulan'];
				$interval_week[$value['week']] = $value['week'];
				$interval_year[$value['tahun']] = $value['tahun'];
				$data['str_interval'][$key] = ($key == 0) ? 'harga_minggu_ini' : 'minggu'.$key;
			}
			// if($id<=2){
				$type_laporan = 'mingguan';
				$wheres['a.tahun'] = $interval_year;
				$wheres['a.bulan'] = $interval_month;
				$wheres['a.minggu_ke'] = $interval_week;
			// }else{
			// 	$type_laporan = 'harian';
			// 	$wheres['YEAR(d.created_on)'] = $interval_year;
			// 	$wheres['MONTH(d.created_on)'] = $interval_month;
			// 	$wheres['d.minggu_ke'] = $interval_week;
			// }

			foreach (array_reverse($intervals) as $key => $value) {
				$index = $value['week'].'|'.$value['bulan'].'|'.$value['tahun'];
				$revese_intervals[$index] = $value;
			}

			$lastDayOfLastWeek = $intervals[7]['last_day_of_week'];
			$get_last_week =  getWeeks($lastDayOfLastWeek, 'monday');
			$last_week = $get_last_week['week'];
			$last_bulan = $get_last_week['bulan'];
			$last_tahun = $get_last_week['tahun'];

			// if($type_laporan == 'mingguan'){
				$wheres_akumulasi['a.tanggal <='] = $lastDayOfLastWeek;
				$wheres_akumulasi['a.tanggal >='] = date('Y-01-01');
			// }else{
				// $wheres_akumulasi['DATE(d.created_on) <='] = $lastDayOfLastWeek;
			// }

			if($is_ttic == 0){
				$wheres_akumulasi['u.unit_type'] = $id;
				$wheres['u.unit_type'] = $id;
			}
			$akumlatedLastWeek = $this->data_m->laporan_harga_optimize2($type_laporan, $wheres_akumulasi, 1);
			$lastDayOfThisWeek = $intervals[0]['last_day_of_week'];
			// if($type_laporan == 'mingguan'){
				$wheres_akumulasi['a.tanggal <='] = $lastDayOfThisWeek;
			// }else{
			// 	$wheres_akumulasi['DATE(d.created_on) <='] = $lastDayOfThisWeek;
			// }
			$akumlatedThisWeek = $this->data_m->laporan_harga_optimize2($type_laporan, $wheres_akumulasi, 1);
			// Get entries laporan
			$laporan = $this->data_m->laporan_harga_optimize2($type_laporan, $wheres);

			// dump($this->db->last_query());
			// die();

			$data['laporan'] = $laporan;
			
			$temp_array = array();
			$temp_array2 = array();
			$temp_field = array();
			$temp_field2 = array();
			$series = array();
			$series2 = array();
			$data_harga = array();

			foreach ($metadatas2 as $key => $metadata) {
				$tipe_field = $metadata['id_laporan_tipe_field'];
				$field = $metadata['field'];

				$temp_arr[$field]['name'] = $metadata['nama'];
				$temp_arr[$field]['field'] = $field;
				$temp_arr[$field]['type'] = 'spline';
				$temp_arr[$field]['tooltip']['valueSuffix'] = ' Rp';
				$temp_arr[$field]['yAxis'] = 0;

				if($metadata['satuan']=='Kg') {
					$temp_arr[$field]['yAxis'] = 1;
					$temp_arr[$field]['type'] = 'column';
					$temp_arr[$field]['tooltip']['valueSuffix'] = 'Kg';
				}

				if($type_laporan == 'harian'){
					$temp_arr2[$field]['name'] = $metadata['nama'];
					$temp_arr2[$field]['field'] = $field;
					$temp_arr2[$field]['type'] = 'spline';
					$temp_arr2[$field]['tooltip']['valueSuffix'] = ' Rp';
					$temp_arr2[$field]['yAxis'] = 0;

					if($metadata['satuan']=='Kg') {
						$temp_arr2[$field]['yAxis'] = 1;
						$temp_arr2[$field]['type'] = 'column';
						$temp_arr2[$field]['tooltip']['valueSuffix'] = 'Kg';
					}
					foreach ($data['days'] as $key_day => $day) {
						$temp_field2[$field][$day] = 0;
					}
				}

				foreach ($revese_intervals as $key_range => $val) {
					$temp_field[$field][$key_range] = 0;
				}

				$data_harga[$field]['satuan'] = $metadata['satuan'];
				$data_harga[$field]['komoditas'] = $metadata['nama'];
				$data_harga[$field]['fluktuasi'] = 0;

				if(count($akumlatedLastWeek) > 0){
					foreach ($akumlatedLastWeek as $key => $lap) {
						$tipe_field = $lap['id_laporan_tipe_field'];
						$field = $lap['field'];
						$agregasi = ($tipe_field == 1) ? 'avg' : 'sum';
						$value = intval($lap[$agregasi]);
						$data_harga[$field]['akumulasi_minggu_lalu'] = $value;
					}
				}else{
					$data_harga[$field]['akumulasi_minggu_lalu'] = 0;
				}

				if(count($akumlatedThisWeek) > 0){
					foreach ($akumlatedThisWeek as $key => $lap) {
						$tipe_field = $lap['id_laporan_tipe_field'];
						$field = $lap['field'];
						$agregasi = ($tipe_field == 1) ? 'avg' : 'sum';
						$value = intval($lap[$agregasi]);
						$data_harga[$field]['akumulasi_minggu_ini'] = $value;
					}
				}else{
					$data_harga[$field]['akumulasi_minggu_ini'] = 0;
				}

				$data_harga[$metadata['field']]['harga_minggu_ini'] = 0;
				$data_harga[$metadata['field']]['harga_minggu_lalu'] = 0;
			}

			if(count($laporan) > 0){
				foreach ($laporan as $key => $lap) {
					$tipe_field = $lap['id_laporan_tipe_field'];
					$field = $lap['field'];
					$agregasi = ($tipe_field == 1) ? 'avg' : 'sum';
					$value = intval($lap[$agregasi]);
					$interval = $lap['minggu_ke'].'|'.$lap['bulan'].'|'.$lap['tahun'];
					$temp_field[$field][$interval] = $value;	
					if($lap['minggu_ke'] == $minggu_ke && $lap['bulan'] == $bulan && $lap['tahun'] == $tahun){
						$data_harga[$field]['harga_minggu_ini'] = $value;
					}

					if($lap['minggu_ke'] == $last_week && $lap['bulan'] == $last_bulan && $lap['tahun'] == $last_tahun){
						$data_harga[$field]['harga_minggu_lalu'] = $value;
					}
				}
			}

			// if($id > 2){
			// 	$wheres_harian['DATE(d.created_on)'] = $data['days'];
			// 	$wheres_harian['m.id_laporan_tipe'] = $id_tipe_laporan;
			// 	$wheres_harian['d.is_ttic'] = $is_ttic;
			// 	$wheres_harian['d.id_group'] = 9;
			// 	$wheres_harian['m.id_laporan_komoditas'] = $id_komoditas;
			// 	$laporan_harian = $this->data_m->laporan_harga_harian($wheres_harian);
			// 	if(count($laporan_harian) > 0){
			// 		foreach ($laporan_harian as $key => $lap) {
			// 			$tipe_field = $lap['id_laporan_tipe_field'];
			// 			$field = $lap['field'];
			// 			$agregasi = ($tipe_field == 1) ? 'avg' : 'sum';
			// 			$value = intval($lap[$agregasi]);

			// 			$lap_day = $lap['tanggal'];
			// 			$temp_field2[$field][$lap_day] = $value;
			// 		}
			// 	}
			// }
			
			$data['data']['entries'] = $data_harga;

			if($download == NULL){
				$temp_strtotime = array();
				foreach ($temp_field as $key => $field) {
					foreach ($revese_intervals as $key_range => $day) {
						if(!isset($temp_field[$key][$key_range])){
							$val_exist = 0;
						}else{
							$val_exist = $temp_field[$key][$key_range]; 
						}
						$temp_strtotime[$key][] = $val_exist;
					}
				}

				foreach ($temp_arr as $key => $ta) {
					$temp_arr[$key]['data'] = $temp_strtotime[$key];
				}
				foreach ($temp_arr as $key => $ta) {
					$series[] = $ta;
				}
				
				$data['categories'] = "'Interval 35 hari', 'Interval 28 hari', 'Interval 21 hari', 'Interval 14 hari', 'Minggu Sebelumnya', 'Minggu ke : ".$minggu_ke." (".$thn_bln.")'";
				$data['json_series'] = json_encode($series);

				// if($id > 2){
				// 	$temp_strtotime2 = array();
				// 	foreach ($temp_field2 as $key => $field) {
				// 		foreach ($data['days'] as $key_day => $day) {
				// 			if(!isset($temp_field2[$key][$day])){
				// 				$val_exist = 0;
				// 			}else{
				// 				$val_exist = $temp_field2[$key][$day]; 
				// 			}
				// 			$temp_strtotime2[$key][strtotime($day)] = $val_exist;
				// 		}
				// 		ksort($temp_strtotime2[$key]);
				// 	}

				// 	foreach ($temp_strtotime2 as $key => $dates) {
				// 		$new_key = 0;
				// 		foreach ($dates as $key_dates => $val_dates) {
				// 			unset($temp_strtotime2[$key][$key_dates]);
				// 			$temp_strtotime2[$key][$new_key] = $val_dates;
				// 			$new_key++;
				// 		}
				// 	}


				// 	foreach ($temp_arr2 as $key => $ta) {
				// 		$temp_arr2[$key]['data'] = $temp_strtotime2[$key];
				// 	}
				// 	foreach ($temp_arr2 as $key => $ta) {
				// 		$series2[] = $ta;
				// 	}
				// 	$data['categories2'] = "'".implode("','",$data['str_days'])."'";
				// 	$data['json_series2'] = json_encode($series2);
				// }
			}
		}

		if($download ==  NULL){
			$this->template->title('Laporan '.$nama_laporan)
			->set_breadcrumb('Home', '/')
			->set_breadcrumb('Laporan', '/laporan/data/harga/'.$id)
			->set_breadcrumb($nama_laporan)
			->build('data_harga', $data);
		}else{
			$this->download_harga($data, $download);
		}
	}

	public function harga_old($id, $download = null)
	{
		$this->load->helper('laporan');

		$data['id'] = $id;
		$id_tipe_laporan = $id;
		$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;
		if($id_tipe_laporan == 3 || $id_tipe_laporan == 4){
			$id_tipe_laporan = 1;
		}
		if($id_tipe_laporan == 5){
			$id_tipe_laporan = 2;
		}

		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
		
		$filter_kota = null;
		if($this->input->get('f-provinsi') != '') {
			$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
			$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		}

		$weeks = getWeeks(date('Y-m-d'), 'monday');

		$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
		$thn_bln = $weeks['tahun'].'-'.$bln;
		if($this->input->get('f-bln') && $this->input->get('f-thn')){
			$thn_bln = $this->input->get('f-thn')."-".$this->input->get('f-bln');
		}

		$date = date($thn_bln.'-01');
		if($this->input->get('f-thn') != '' && $this->input->get('f-bln') != ''){
			$date = $this->input->get('f-thn').'-'.$this->input->get('f-bln').'-01';
		}

		$end_week = get_count_of_week($date);
		$data['end_week'] = $end_week;

		$minggu_ke = (string) $weeks['week'];
		if($this->input->get('f-minggu_ke')){
			$minggu_ke = $this->input->get('f-minggu_ke');
		}
		$data['minggu_ke'] = $minggu_ke;

		$ex_thn_bln = explode('-',$thn_bln);
		$data['thn'] = $ex_thn_bln[0];
		$data['bln'] = $ex_thn_bln[1];

		$firstDayOfWeek = $this->absen_m->get_first_day_of_week($data['thn'], $data['bln'], $minggu_ke, $id);
		$data['firstDayOfWeek'] = $firstDayOfWeek;
		$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');

		$start = 7;
		$end = 35;
		$intervals = get_interval($firstDayOfWeek, $start, $end);
		
		// if(empty($firstDayOfWeek)){
		// 	$firstDayOfWeek = get_date_by_wmy($minggu_ke, $thn_bln); 
		// }

		$data['thn_bln'] = date_idr($thn_bln, 'F Y', null);

		$nama_laporan = $this->tipe_m->get_tipe_by_id($id)['nama_laporan'];

		$today = date('Y-m-d H:i:s');
    // $today = '2016-09-09';
		$id_absen = $this->absen_m->get_absen_by_date($today, $id)['id'];
		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
		$data['data']['entries'] = $this->data_m->laporan_harga($id, $minggu_ke, $thn_bln, $intervals, $id_absen, null, $id_komoditas);
		$data['data']['total'] = count($data['data']['entries']);
		$data['nama_laporan'] = $nama_laporan;

		$data['metadata'] = $this->metadata_m->get_metadata_by_tipe_laporan($id, $id_komoditas, 'default_laporan_metadata.*');
		$data['komoditas_related'] = $this->komoditas_m->get_komoditas_related();
		$data['min_year'] = $this->absen_m->get_min_year(); 
		$data['max_year'] = $this->absen_m->get_max_year();

		// if($id == 2){
			// -------------------------------------
      // Get Organization by Member
      // -------------------------------------

		$data['types'] = $this->types_m->get_all_types(NULL, $id);

		$unit_id = $this->unit['id'];

		$types = $data['types'];
		$organization = array();
		$addedLevel = array();
		$n2 = 0;
		$n = count($types);

		foreach($types as $i => $type){
			if (in_array($type['level'], $addedLevel)) {
				continue;
			}
			$addedLevel[] = $type['level'];
			$units = $this->units_m->get_unit_by_child($unit_id);

			if(isset($units['id'])){
				if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
					$organization['organization_name_'.$units['type_level']] = $units['unit_name'];
					$organization['id_organization_unit_'.$units['type_level']] = $units['id'];
					$n2++;
				} else if ($units['type_slug'] == $this->node_type_slug) {
					$organization['organization_name_'.$units['type_level']] = $units['unit_name'];
					$organization['id_organization_unit'] = $units['id'];
				}
				$unit_id = $units['id'];
			}
		}

		if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
			if($this->unit['type_slug'] == $this->node_type_slug){
				$organization['id_organization_unit'] = $this->unit['id'];
			}else{
				$organization['id_organization_unit_'.$n2] = $this->unit['id'];
			}
			$organization['organization_name_'.$n2] = $this->unit['name'];
		}

      // khusus sitoni
		$data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
      //---------------------------------
		$data['user_unit_level'] = $this->unit['type_level'];
		$data['node_type_level'] = $this->node_type_level;
		$data['node_type_slug'] = $this->node_type_slug;
		$data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

		$get_units = $this->memberships_m->get_units_by_member(1);
		if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
			unset($organization['id_organization_unit']);
			unset($organization['organization_name_1']);
		}

		$data['organization'] = $organization;
		if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
			$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
		}
		// }

		if($download == NULL){
			$series = array();
			$temp_arr = array();
			foreach ($data['data']['entries'] as $key => $value) {
				$temp_arr['name'] = $value['komoditas'];
				$temp_arr['type'] = 'spline';
				$temp_arr['tooltip']['valueSuffix'] = ' Rp';
				$temp_arr['yAxis'] = 0;

				if($value['satuan']=='Kg') {
					$temp_arr['yAxis'] = 1;
					$temp_arr['type'] = 'column';
					$temp_arr['tooltip']['valueSuffix'] = 'Kg';
				}
				$temp_arr['data'] = array();
				array_push($temp_arr['data'], ($value['minggu35'] != NULL) ? intval($value['minggu35']) : 0);
				array_push($temp_arr['data'], ($value['minggu28'] != NULL) ? intval($value['minggu28']) : 0);
				array_push($temp_arr['data'], ($value['minggu21'] != NULL) ? intval($value['minggu21']) : 0);
				array_push($temp_arr['data'], ($value['minggu14'] != NULL) ? intval($value['minggu14']) : 0);
				array_push($temp_arr['data'], ($value['minggu7'] != NULL) ? intval($value['minggu7']) : 0);
				array_push($temp_arr['data'], ($value['harga_minggu_ini'] != NULL) ? intval($value['harga_minggu_ini']) : 0);
				$series[$key] = $temp_arr;
			}

			$data['json_series'] = json_encode($series);
			

			$this->template->title('Laporan '.$nama_laporan)
			->set_breadcrumb('Home', '/')
			->set_breadcrumb('Laporan', '/laporan/data/harga/'.$id)
			->set_breadcrumb($nama_laporan)
			->build('data_harga_old', $data);
		}else{
			$this->download_harga($data, $download);
		}
	}

	public function download_harga($data, $download){

		$judul = "Perkembangan PUPM Melalui Kegiatan TTI";
		$nama_file = $judul.' - '.ucfirst($data['nama_laporan']);

		if($download == 0){
			$this->load->library('excel');
			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle('Laporan '.ucfirst($data['nama_laporan']));

			foreach (range('A','F') as $columnID) {
				$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			}

			// $this->excel->getActiveSheet()->setCellValue('A1', $judul.ucfirst($data['nama_laporan']));
			$this->excel->getActiveSheet()->setCellValue('A1', $judul);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:G1');

			$row = 2;
			if($this->input->get('f-provinsi') != ''){
				$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
				$kota = "";
				if($this->input->get('f-kota') != ''){
					$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
				}

				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
				$row++;
				
				$nama_file .= '_'.$provinsi.$kota;
			}

			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bulan') != '' || $this->input->get('f-tahun') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bulan') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan')];
					}
					if($this->input->get('f-tahun') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-tahun');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$data['minggu_ke'];
				$periode[] = 'Bulan '.$arr_month[date('m')];
				$periode[] = 'Tahun '.date('Y');
			}

			if(count($periode) > 0){
				$implode_periode = implode(', ',$periode);
				$nama_file .= "_".implode('_',$periode);

				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Periode: '.$implode_periode);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
				$row++;
			}

			$row++;

			$style0 = array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'CCCCCC')
					),
				);

			$this->excel->getActiveSheet()->getStyle('A'.$row.':G'.($row+1))->applyFromArray($style0);

			$style = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);

			$this->excel->getActiveSheet()->getStyle('A1:G'.($row+1))->applyFromArray($style);

			$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Komoditas');
			$this->excel->getActiveSheet()->mergeCells('A'.$row.':A'.($row+1));
			$this->excel->getActiveSheet()->setCellValue('B'.$row, 'Satuan');
			$this->excel->getActiveSheet()->mergeCells('B'.$row.':B'.($row+1));
			$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Nilai');
			$this->excel->getActiveSheet()->mergeCells('C'.$row.':E'.$row);
			$this->excel->getActiveSheet()->setCellValue('F'.$row, 'Persentase Perubahan');
			$this->excel->getActiveSheet()->setCellValue('G'.$row, 'Fluktuasi');
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$this->excel->getActiveSheet()->mergeCells('G'.$row.':G'.($row+1));

			$this->excel->getActiveSheet()->getStyle('A'.$row.':G'.($row+1))->getAlignment()->setWrapText(true);

			$total =  $row + count($data['data']['entries']) + 1;
			$this->excel->getActiveSheet()->getStyle('A'.$row.':G'.$total)->applyFromArray(
				array(
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('rgb' => '000000')
							)
						),
					)
				);

			$row++;
			$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Akumulasi (awal tahun - minggu lalu)');
			$this->excel->getActiveSheet()->setCellValue('D'.$row, 'Minggu ke : '.$data['minggu_ke'].'('.$data["tahun_bulan"].')');
			$this->excel->getActiveSheet()->setCellValue('E'.$row, 'Total/Rata2 (awal tahun - minggu ini)');
			$this->excel->getActiveSheet()->setCellValue('F'.$row, 'Minggu Lalu - Minggu ke : '.$data["minggu_ke"].' ('.$data["tahun_bulan"].')');


			foreach ($data['data']['entries'] as $key => $entries) { 
				$mingguini = ($entries['harga_minggu_ini'] != "") ? $entries['harga_minggu_ini'] : 0;
        $minggulalu = ($entries['harga_minggu_lalu']) ? $entries['harga_minggu_lalu'] : 0;

        if($mingguini > $minggulalu){
          $color2 = 'red';
          $text2 = 'Naik';
          $silit2 = $mingguini-$minggulalu;
          $silit2 = ($silit2 == 0) ? 0 : ($silit2/$mingguini)*100;
          $silit2 = round($silit2,2);
        }else{
          $color2 = '#E4A000';
          $text2 = 'Turun';
          $silit2 = $minggulalu-$mingguini;
          $silit2 = ($silit2 == 0) ? 0 : ($silit2/$minggulalu)*100;
          $silit2 = round($silit2,2);
        }

        if($mingguini == $minggulalu){
          $color2 = 'red';
          $text2 = 'Stabil';
        }

        if($entries['fluktuasi'] != 0 && $mingguini != 0){
          $fluktuasi = round($entries['fluktuasi'] / $mingguini, 2).'%';
        }else{
          $fluktuasi = '0%';
        }
				// $fluktuasi = number_format($entries['fluktuasi'],0,',','');
				$row++;
				$this->excel->getActiveSheet()->setCellValue('A'.$row, $entries["komoditas"]);
				$this->excel->getActiveSheet()->setCellValue('B'.$row, $entries["satuan"]);
				$this->excel->getActiveSheet()->setCellValue('C'.$row, number_format($entries["akumulasi_minggu_lalu"],0,',',''));
				$this->excel->getActiveSheet()->setCellValue('D'.$row, number_format($entries["harga_minggu_ini"],0,',',''));
				$this->excel->getActiveSheet()->setCellValue('E'.$row, number_format($entries["akumulasi_minggu_ini"],0,',',''));
				$this->excel->getActiveSheet()->setCellValue('F'.$row, $text2.' '.$silit2.'%');

				$styleArray = array(
					'font'  => array(
						'color' => array('rgb' => $color2),
						));
				$this->excel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($styleArray);
				$this->excel->getActiveSheet()->setCellValue('G'.$row, $fluktuasi);
			}

			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache

			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');

		}else{
			$html ="
			<table border=\"0\" cellpadding=\"5\">
				<tr>
					<td colspan=\"8\"></td>
				</tr>
				<tr>
					<td colspan=\"8\" align=\"center\">
						<span style=\"font: bold 20px Open Sans; display: block;\">".$judul."</span>
					</td>
				</tr>";

				if($this->input->get('f-provinsi') != ''){
					$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
					$kota = "";
					if($this->input->get('f-kota') != ''){
						$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
					}
					$html .= '
					<tr>
						<td colspan="8" align="center">
							<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
						</td>
					</tr>
					';
					$nama_file .= '_'.$provinsi.$kota;
				}

				$periode = array();


				$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');

				if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bulan') != '' || $this->input->get('f-tahun') != ''){
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bulan') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan')];
					}
					if($this->input->get('f-tahun') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-tahun');
					}
				}else{
					$periode[] = ' Minggu Ke '.$data['minggu_ke'];
					$periode[] = 'Bulan '.$arr_month[date('m')];
					$periode[] = 'Tahun '.date('Y');
				}

				if(count($periode) > 0){
					$implode_periode = implode(', ',$periode);
					$nama_file .= "_".implode('_',$periode);
					$html .= '
					<tr>
						<td colspan="8" align="center">
							<span style="font: bold 20px Open Sans; display: block;">Periode: '.$implode_periode.'</span>
						</td>
					</tr>
					';
				}

				if($this->input->get('f-komoditas') != ''){
					$nama_komoditas = $this->metadata_m->get_metadata_by_id($this->input->get('f-komoditas'))['nama'];
					$nama_file .= "_".$nama_komoditas;
					$html .= '
					<tr>
						<td colspan="8" align="center">
							<span style="font: bold 20px Open Sans; display: block;">'.$nama_komoditas.'</span>
						</td>
					</tr>
					';
				}

				$html .="
				<tr>
					<td colspan=\"8\"></td>
				</tr>
			</table>";

			$html .= '
			<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
				$html .='
				<thead style="background-color:#ccc;">
					<tr>
						<th rowspan="2">Komoditas</th>
						<th rowspan="2">Satuan</th>
						<th colspan="3">Nilai</th>
						<th>Persentase Perubahan</th>
						<th rowspan="2">Fluktuasi</th>
					</tr>
					<tr>
						<th>Akumulasi<br>(awal tahun - minggu lalu)</th>
						<th>Minggu ke : '.$data['minggu_ke'].'('.$data["tahun_bulan"].')</th>
						<th>Total/Rata2 <br>(awal tahun - minggu ini)</th>
						<th>Minggu Lalu - Minggu ke : '.$data["minggu_ke"].' ('.$data["tahun_bulan"].')</th>
					</tr>
				</thead>
				<tbody>';
					foreach ($data['data']['entries'] as $key => $entries) { 
						$mingguini = ($entries['harga_minggu_ini'] != "") ? $entries['harga_minggu_ini'] : 0;
            $minggulalu = ($entries['harga_minggu_lalu']) ? $entries['harga_minggu_lalu'] : 0;

            if($mingguini > $minggulalu){
              $color2 = 'red';
              $text2 = 'Naik';
              $silit2 = $mingguini-$minggulalu;
              $silit2 = ($silit2 == 0) ? 0 : ($silit2/$mingguini)*100;
              $silit2 = round($silit2,2);
            }else{
              $color2 = '#E4A000';
              $text2 = 'Turun';
              $silit2 = $minggulalu-$mingguini;
              $silit2 = ($silit2 == 0) ? 0 : ($silit2/$minggulalu)*100;
              $silit2 = round($silit2,2);
            }

            if($mingguini == $minggulalu){
              $color2 = 'red';
              $text2 = 'Stabil';
            }

            if($entries['fluktuasi'] != 0 && $mingguini != 0){
              $fluktuasi = round($entries['fluktuasi'] / $mingguini, 2).'%';
            }else{
              $fluktuasi = '0%';
            }
						// $fluktuasi = number_format($entries['fluktuasi'],0,',','');

						$html .='
						<tr>
							<td>'.$entries["komoditas"].'</td>
							<td>'.$entries["satuan"].'</td>
							<td style="text-align:right;">'.number_format($entries["akumulasi_minggu_lalu"],0,',','').'</td>
							<td style="text-align:right;">'.number_format($entries["harga_minggu_ini"],0,',','').'</td>
							<td style="text-align:right;">'.number_format($entries["akumulasi_minggu_ini"],0,',','').'</td>
							<td style="text-align:right; color:'.$color2 .'">'.$text2. " ".$silit2."%" .'</td>
							<td style="text-align:right;">'.$fluktuasi.'</td>
						</tr>
						';
					}
					$html .='
				</tbody>
			</table>';
			$data['html'] = $html;
			$this->load->view('admin/page_print', $data);
		}
	}

	public function kinerja_pendamping(){

		$this->load->helper('laporan');

		$surffix = '';
		if($_SERVER['QUERY_STRING']){
			$surffix = '?'.$_SERVER['QUERY_STRING'];
		}



		// Set hari
		$hari = ($this->input->get('f-hari')) ? $this->input->get('f-hari') : date('Y-m-d');
    //--------------------------------------------------------------------------------------------------------
		
		// Set default periode
		$weeks = getWeeks($hari, 'monday');
    //--------------------------------------------------------------------------------------------------------

		// Set Tahun
		$tahun = NULL;
		if($this->input->get('f-tahun')){
			$tahun = $this->input->get('f-tahun');
		}else{
			$tahun = $weeks['tahun'];
		}

		$data['tahun'] = $tahun;

		$data['min_year'] = $this->absen_m->get_min_year(); 
		$data['max_year'] = $this->absen_m->get_max_year();

		//--------------------------------------------------------------------------------------------------------

		// Set Bulan
		if($this->input->get('f-bulan')){
			$bulan = $this->input->get('f-bulan');
		}else{
			$bulan = $weeks['bulan'];
		}

		$bulan = (strlen($bulan) == 1) ? '0'.$bulan : $bulan;
		$thn_bln = $tahun."-".$bulan;

		if(isset($_GET['f-bulan'])){
			if($_GET['f-bulan'] != ""){
				$bulan = $this->input->get('f-bulan');
			}else{
				$bulan = '';
			}
		}
			
		if($bulan != ''){
			$bulan = $bulan + 0;
		}

		$data['bulan'] = $bulan;

		$data['tahun_bulan'] = date_idr($tahun.'-'.$bulan, 'F Y', null);
		$data['thn_bln'] = $thn_bln;
    //--------------------------------------------------------------------------------------------------------

		// Set Jumlah Minggu
		$first_day_of_month = $thn_bln.'-01';

		$count_of_week = get_count_of_week($first_day_of_month);
		$data['count_of_week'] = $count_of_week;
    //--------------------------------------------------------------------------------------------------------

		// Minggu ke
		if($this->input->get('f-minggu_ke')){
			$minggu_ke = $this->input->get('f-minggu_ke');
		}else{
			$minggu_ke = (string) $weeks['week'];
		}

		if(isset($_GET['f-minggu_ke'])){
			if($_GET['f-minggu_ke'] != ""){
				$minggu_ke = $this->input->get('f-minggu_ke');
			}else{
				$minggu_ke = '';
			}
		}

		$data['minggu_ke'] = $minggu_ke;
    //--------------------------------------------------------------------------------------------------------

		$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke);
		$data['firstDayOfWeek'] = $firstDayOfWeek;
		$data['weeks'] = $weeks;
		$data['pendamping']['entries'] = null;
		$data['pendamping']['total'] = 0;
		$data['pendamping']['pagination'] = null;

	  if($this->input->get('f-groups')){
	   	// -------------------------------------
			// Pagination
			// -------------------------------------
			$count_pendamping = $this->pendamping_m->get_pendamping();

			$pagination_config['base_url'] = base_url(). 'laporan/data/kinerja_pendamping/';
			$pagination_config['uri_segment'] = 4;
			$pagination_config['suffix'] = $surffix;
			$pagination_config['total_rows'] = count($count_pendamping);
			$pagination_config['per_page'] = 10;
			$this->pagination->initialize($pagination_config);
			$data['pagination_config'] = $pagination_config;

	    // -------------------------------------
			// Get entries
			// -------------------------------------
			
			$data['pendamping']['entries'] = $this->pendamping_m->get_pendamping($pagination_config, $minggu_ke, $thn_bln);
			$data['pendamping']['total'] = count($count_pendamping);
			$data['pendamping']['pagination'] = $this->pagination->create_links();
		}

		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
		
		$filter_kota = null;
		if($this->input->get('f-provinsi') != '') {
			$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
			$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		}

		// Set Location
		$filter_kota = null;
		if($this->input->get('f-provinsi') != '') {
			$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
			$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		}

		$data['get_location'] = '';
		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;
		if($id_provinsi != NULL){
			$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
			$kota = "";
			if($this->input->get('f-kota') != ''){
				$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
			}
			$data['get_location'] .= '<br>Provinsi '.$provinsi.$kota;
		}

		$data['tipes'] = $this->tipe_m->get_tipe();
		$data['metadata_gap'] = $this->metadata_m->get_metadata_by_tipe_laporan(1, 1,'default_laporan_metadata.*', NULL, 'urutan');
		$data['metadata_tti'] = $this->metadata_m->get_metadata_by_tipe_laporan(2, 1,'default_laporan_metadata.*', NULL, 'urutan');

		$uri = '';
		$page = $this->uri->segment(4);
		if($page){
			$uri = '/'.$page;
		}
		if($_SERVER['QUERY_STRING']){
			$uri = '?'.$_SERVER['QUERY_STRING'];
		}
		if($_SERVER['QUERY_STRING'] && $page){
			$uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
		}
		$data['uri'] = $uri;

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
		$this->template->title('Laporan '.lang('laporan:kinerja_pendamping'))
		->set_breadcrumb('Home', '/')
		->set_breadcrumb('Laporan', '/laporan/data/kinerja_pendamping')
		->set_breadcrumb(lang('laporan:kinerja_pendamping'))
		->build('data_kinerja_pendamping', $data);
	}

	public function ajax_unit_dropdown($parent_id = 0, $select_all = 0) {
		$units = $parent_id == 0 ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($parent_id);
		if ($select_all) {
			echo '<option value="">' . lang('global:select-all') . '</option>';
		} else {
			if (count($units)) {
				echo '<option value="-1">' . lang('global:select-pick') . '</option>';
			} else {
				echo '<option value="-1">' . lang('global:select-none') . '</option>';
			}
		}

		foreach ($units as $unit) {
			echo '<option value="' . $unit['id'] . '"> ' . $unit['unit_name'] . '</option>';
		}
	}

	public function ajax_get_minggu_ke($year = null, $month = null){
		$this->load->helper('laporan');
		if($year != NULL && $month != NULL){
			$date = $year.'-'.$month.'-01';
			$end_week = get_count_of_week($date);
			$romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
			echo '<option value="">-- '.lang('laporan:minggu_ke').' --</option>';
			for ($i=1;$i<=$end_week;$i++) {
				echo '<option value="'.$i.'">'.$romawi[$i].'</option>';
			}
		}else{
			echo '<option value="">'.lang('global:select-none').'</option>';
		}
	}

	public function ajax_get_minggu_ke2($year = null, $month = null){
		$this->load->helper('laporan');
		if($year != NULL && $month != NULL){
			$date = $year.'-'.$month.'-01';
			$end_week = get_count_of_week($date);
			$romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
			// echo '<option value="">-- '.lang('laporan:minggu_ke').' --</option>';
			for ($i=1;$i<=$end_week;$i++) {
				echo '<option value="'.$i.'">'.$romawi[$i].'</option>';
			}
		}else{
			echo '<option value="">'.lang('global:select-none').'</option>';
		}
	}

	public function ajax_get_hari($year = null, $month = null, $minggu_ke = null, $tipe_laporan = null){
		$this->load->helper('laporan');
		if($year != NULL && $month != NULL && $minggu_ke != NULL){
			if($tipe_laporan <=2){
				$days = $this->absen_m->get_hari($year, $month, $minggu_ke, $tipe_laporan);
			}else{
				$day = $this->data_m->get_hari($year, $month, $minggu_ke, $tipe_laporan);
				$days = array();
				if($day != ''){
					$weeks = getWeeks($day, 'monday');
					$firstDay = $weeks['first_day_of_week'];
					$lastDay = $weeks['last_day_of_week'];
					for ($h=0; $h <=6 ; $h++) { 
						$days[]['tanggal'] = date('Y-m-d', strtotime('+'.$h.' days', strtotime($firstDay)));
					}
				}
			}

			if($year > 2017){
				$gap_type_ids = array(3, 4, 6);
				if(in_array($tipe_laporan, $gap_type_ids)){
					$tipe_laporan = 1;
				}
				$tti_type_ids = array(5, 7);
				if(in_array($tipe_laporan, $tti_type_ids)){
					$tipe_laporan = 2;
				}
				$days = $this->absen_m->get_hari($year, $month, $minggu_ke, $tipe_laporan);
				// dump($this->db->last_query());
			}

			if(count($days) > 0){
				echo '<option value="">- Hari -</option>';
				foreach ($days as $day) {
					echo '<option value="'.$day['tanggal'].'">'.date_idr($day['tanggal'], 'l, d F Y', null).'</option>';
				}
			}else{
				echo '<option value="">'.lang('global:select-none').'</option>';
			}
		}else{
			echo '<option value="">'.lang('global:select-none').'</option>';
		}
	}

	public function ajax_get_kode_tti_by_telp($telp){
		$this->load->model('pendamping_m');
		$data_tti = $this->pendamping_m->get_kode_tti_by_telp($telp);
		$abjad = array('A','B','C','D','E','F','G','H','I');
		foreach ($data_tti as $key => $data) {
			$data_tti[$key]['kode_tti'] = $abjad[$key];
		}

		echo json_encode($data_tti);
	}

	public function ajax_get_kode_ttic_by_telp($telp){
		$this->load->model('pendamping_m');
		$data_ttic = $this->pendamping_m->get_kode_ttic_by_telp($telp);
		$abjad = array('A','B','C','D','E','F','G','H','I');
		foreach ($data_ttic as $key => $data) {
			$data_ttic[$key]['kode_ttic'] = $abjad[$key];
		}

		echo json_encode($data_ttic);
	}

	public function ajax_get_units_by_kota($id_kota){
		$this->load->model('organization/units_m');
		$data_tti = $this->units_m->get_units_by_kota($id_kota, 2);
		echo json_encode($data_tti);
	}

	public function ajax_get_unit_by_kota($kota, $unit_type){
		$content = $this->organization->get_units_by_kota($kota, $unit_type);
		echo $content;
	}

	// --------------------------------------------------------------------------
	// Dashboard
	// --------------------------------------------------------------------------

	public function grafik_laporan($id, $id_komoditas = NULL, $id_unit = NULL, $minggu_ke = NULL, $thn_bln = NULL)
	{
		$this->load->helper('laporan');
		$data['id'] = $id;
		$data['id_komoditas'] = $id_komoditas;
		$id_tipe_laporan = $id;
		$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;
		$wheres['d.is_ttic'] = $is_ttic;
		$wheres_akumulasi['d.is_ttic'] = $is_ttic;
		$gap_type_ids = array(3, 4, 6);
		if(in_array($id_tipe_laporan, $gap_type_ids)){
			$id_tipe_laporan = 1;
		}
		$tti_type_ids = array(5, 7);
		if(in_array($id_tipe_laporan, $tti_type_ids)){
			$id_tipe_laporan = 2;
		}
		$wheres['m.id_laporan_tipe'] = $id_tipe_laporan;
		$wheres_akumulasi['m.id_laporan_tipe'] = $id_tipe_laporan;

		$nama_laporan = $this->tipe_m->get_tipe_by_id($id_tipe_laporan)['nama_laporan'];
		$data['nama_laporan'] = $nama_laporan;

		$weeks = getWeeks(date('Y-m-d'), 'monday');
		$minggu_ke = ($minggu_ke == NULL) ? (string) $weeks['week'] : $minggu_ke;
		$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
		$thn_bln = ($thn_bln == NULL) ? $weeks['tahun'].'-'.$bln : $thn_bln;
		$ex_thn_bln = explode('-',$thn_bln);
		$bulan = $ex_thn_bln[1]+0;
		$tahun = $ex_thn_bln[0];

		// Data Komoditas
		if($id_komoditas != NULL){
			$wheres['m.id_laporan_komoditas'] = $id_komoditas;
			$wheres_akumulasi['m.id_laporan_komoditas'] = $id_komoditas;
		}

		if($id_unit != NULL){
			$wheres['d.id_unit'] = $id_unit;
		}
		$metadatas2 = $this->metadata_m->get_metadata_by_tipe_laporan3($id_tipe_laporan, $id_komoditas, 'default_laporan_metadata.*', NULL, 'urutan');
		$data['metadata2'] = $metadatas2;

		// if($id > 2){
		// 	$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
		// 	$week_of_today = getWeeks(date('Y-m-d'), 'monday');
		// 	if($week_of_today['week'] == $minggu_ke && $week_of_today['bulan'] == $bulan && $week_of_today['tahun'] == $tahun){
		// 		$day_looper = date('Y-m-d');
		// 	}else{
		// 		$weeks = getWeeks($firstDayOfWeek, 'monday');
		// 		$day_looper = $weeks['last_day_of_week'];
		// 	}
		// 	for ($h=0; $h <=6 ; $h++) {
		// 		$add_day2 = date('Y-m-d', strtotime('-'.$h.' days', strtotime($day_looper))); 
		// 		$data['days'][] = $add_day2;
		// 		$data['str_days'][] =  ($add_day2 == date('Y-m-d') ? 'Hari ini' : date_idr($add_day2, 'd F', null));
		// 	}
		// 	$data['days'] = array_reverse($data['days']);
		// 	$data['str_days'] = array_reverse($data['str_days']);
		// }else{
			// $firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
			$firstDayOfWeek = $weeks['first_day_of_week'];
		// }

		$data['firstDayOfWeek'] = $firstDayOfWeek;

		$nama_laporan = $this->tipe_m->get_tipe_by_id($id_tipe_laporan)['nama_laporan'];
		$data['nama_laporan'] = $nama_laporan;
		$data['weeks'] = $weeks;
		if($firstDayOfWeek != NULL){
			$weeks = getWeeks($firstDayOfWeek, 'monday');
			$data['weeks'] = $weeks;
			// Set interval waktu
			$start = 0;
			$end = 35;
			$intervals = get_interval($firstDayOfWeek, $start, $end);
			$revese_intervals = array();
			$interval_month = array();
			$interval_week = array();
			$interval_year = array();
			foreach ($intervals as $key => $value) {
				$interval_month[$value['bulan']] = $value['bulan'];
				$interval_week[$value['week']] = $value['week'];
				$interval_year[$value['tahun']] = $value['tahun'];
				$data['str_interval'][$key] = ($key == 0) ? 'harga_minggu_ini' : 'minggu'.$key;
			}
			// if($id<=2){
				$type_laporan = 'mingguan';
				$wheres['a.tahun'] = $interval_year;
				$wheres['a.bulan'] = $interval_month;
				$wheres['a.minggu_ke'] = $interval_week;
			// }else{
			// 	$type_laporan = 'harian';
			// 	$wheres['YEAR(d.created_on)'] = $interval_year;
			// 	$wheres['MONTH(d.created_on)'] = $interval_month;
			// 	$wheres['d.minggu_ke'] = $interval_week;
			// }

			foreach (array_reverse($intervals) as $key => $value) {
				$index = $value['week'].'|'.$value['bulan'].'|'.$value['tahun'];
				$revese_intervals[$index] = $value;
			}

			$lastDayOfLastWeek = $intervals[7]['last_day_of_week'];
			$get_last_week = getWeeks($lastDayOfLastWeek, 'monday');
			$last_week = $get_last_week['week'];
			$last_bulan = $get_last_week['bulan'];
			$last_tahun = $get_last_week['tahun'];

			if($type_laporan == 'mingguan'){
				$wheres_akumulasi['a.tanggal <='] = $lastDayOfLastWeek;
			}else{
				$wheres_akumulasi['DATE(d.created_on) <='] = $lastDayOfLastWeek;
			}

			$akumlatedLastWeek = $this->data_m->laporan_harga_optimize2($type_laporan, $wheres_akumulasi, 1);

			$lastDayOfThisWeek = $intervals[0]['last_day_of_week'];
			if($type_laporan == 'mingguan'){
				$wheres_akumulasi['a.tanggal <='] = $lastDayOfThisWeek;
			}else{
				$wheres_akumulasi['DATE(d.created_on) <='] = $lastDayOfThisWeek;
			}

			if($is_ttic == 0){
				$wheres_akumulasi['u.unit_type'] = $id;
				$wheres['u.unit_type'] = $id;
			}
			$akumlatedThisWeek = $this->data_m->laporan_harga_optimize2($type_laporan, $wheres_akumulasi, 1);
			// Get entries laporan
			$laporan = $this->data_m->laporan_harga_optimize2($type_laporan, $wheres);

			$data['laporan'] = $laporan;
			
			$temp_array = array();
			$temp_array2 = array();
			$temp_field = array();
			$temp_field2 = array();
			$series = array();
			$series2 = array();
			$data_harga = array();
			
			foreach ($metadatas2 as $key => $metadata) {
				$tipe_field = $metadata['id_laporan_tipe_field'];
				$field = $metadata['field'];

				$temp_arr[$field]['name'] = $metadata['nama'];
				$temp_arr[$field]['field'] = $field;
				$temp_arr[$field]['type'] = 'spline';
				$temp_arr[$field]['tooltip']['valueSuffix'] = ' Rp';
				$temp_arr[$field]['yAxis'] = 0;

				if($metadata['satuan']=='Kg') {
					$temp_arr[$field]['yAxis'] = 1;
					$temp_arr[$field]['type'] = 'column';
					$temp_arr[$field]['tooltip']['valueSuffix'] = 'Kg';
				}

				// if($type_laporan == 'harian'){
				// 	$temp_arr2[$field]['name'] = $metadata['nama'];
				// 	$temp_arr2[$field]['field'] = $field;
				// 	$temp_arr2[$field]['type'] = 'spline';
				// 	$temp_arr2[$field]['tooltip']['valueSuffix'] = ' Rp';
				// 	$temp_arr2[$field]['yAxis'] = 0;

				// 	if($metadata['satuan']=='Kg') {
				// 		$temp_arr2[$field]['yAxis'] = 1;
				// 		$temp_arr2[$field]['type'] = 'column';
				// 		$temp_arr2[$field]['tooltip']['valueSuffix'] = 'Kg';
				// 	}
				// 	foreach ($data['days'] as $key_day => $day) {
				// 		$temp_field2[$field][$day] = 0;
				// 	}
				// }

				foreach ($revese_intervals as $key_range => $val) {
					$temp_field[$field][$key_range] = 0;
				}

				$data_harga[$field]['satuan'] = $metadata['satuan'];
				$data_harga[$field]['komoditas'] = $metadata['nama'];
				$data_harga[$field]['fluktuasi'] = 0;

				if(count($akumlatedLastWeek) > 0){
					foreach ($akumlatedLastWeek as $key => $lap) {
						$tipe_field = $lap['id_laporan_tipe_field'];
						$field = $lap['field'];
						$agregasi = ($tipe_field == 1) ? 'avg' : 'sum';
						$value = intval($lap[$agregasi]);
						$data_harga[$field]['akumulasi_minggu_lalu'] = $value;
					}
				}else{
					$data_harga[$field]['akumulasi_minggu_lalu'] = 0;
				}

				if(count($akumlatedThisWeek) > 0){
					foreach ($akumlatedThisWeek as $key => $lap) {
						$tipe_field = $lap['id_laporan_tipe_field'];
						$field = $lap['field'];
						$agregasi = ($tipe_field == 1) ? 'avg' : 'sum';
						$value = intval($lap[$agregasi]);
						$data_harga[$field]['akumulasi_minggu_ini'] = $value;
					}
				}else{
					$data_harga[$field]['akumulasi_minggu_ini'] = 0;
				}

				$data_harga[$metadata['field']]['harga_minggu_ini'] = 0;
				$data_harga[$metadata['field']]['harga_minggu_lalu'] = 0;
			}

			if(count($laporan) > 0){
				foreach ($laporan as $key => $lap) {
					$tipe_field = $lap['id_laporan_tipe_field'];
					$field = $lap['field'];
					$agregasi = ($tipe_field == 1) ? 'avg' : 'sum';
					$value = intval($lap[$agregasi]);
					$interval = $lap['minggu_ke'].'|'.$lap['bulan'].'|'.$lap['tahun'];
					$temp_field[$field][$interval] = $value;	
					if($lap['minggu_ke'] == $minggu_ke && $lap['bulan'] == $bulan && $lap['tahun'] == $tahun){
						$data_harga[$field]['harga_minggu_ini'] = $value;
					}

					if($lap['minggu_ke'] == $last_week && $lap['bulan'] == $last_bulan && $lap['tahun'] == $last_tahun){
						$data_harga[$field]['harga_minggu_lalu'] = $value;
					}
				}
			}

			// if($id > 2){
			// 	die();
			// 	$wheres_harian['DATE(d.created_on)'] = $data['days'];
			// 	$wheres_harian['m.id_laporan_tipe'] = $id_tipe_laporan;
			// 	$wheres_harian['d.is_ttic'] = $is_ttic;
			// 	$wheres_harian['d.id_group'] = 9;
			// 	if($id_komoditas != NULL){
			// 		$wheres_harian['m.id_laporan_komoditas'] = $id_komoditas;
			// 	}
			// 	$laporan_harian = $this->data_m->laporan_harga_harian($wheres_harian);
			// 	if(count($laporan_harian) > 0){
			// 		foreach ($laporan_harian as $key => $lap) {
			// 			$tipe_field = $lap['id_laporan_tipe_field'];
			// 			$field = $lap['field'];
			// 			$agregasi = ($tipe_field == 1) ? 'avg' : 'sum';
			// 			$value = intval($lap[$agregasi]);

			// 			$lap_day = $lap['tanggal'];
			// 			$temp_field2[$field][$lap_day] = $value;
			// 		}
			// 	}
			// }
			
			$data['data']['entries'] = $data_harga;

			// if($download == NULL){
				$temp_strtotime = array();
				foreach ($temp_field as $key => $field) {
					foreach ($revese_intervals as $key_range => $day) {
						if(!isset($temp_field[$key][$key_range])){
							$val_exist = 0;
						}else{
							$val_exist = $temp_field[$key][$key_range]; 
						}
						$temp_strtotime[$key][] = $val_exist;
					}
				}

				foreach ($temp_arr as $key => $ta) {
					$temp_arr[$key]['data'] = $temp_strtotime[$key];
				}
				foreach ($temp_arr as $key => $ta) {
					$series[] = $ta;
				}
				
				$data['categories'] = "'Interval 35 hari', 'Interval 28 hari', 'Interval 21 hari', 'Interval 14 hari', 'Minggu Sebelumnya', 'Minggu ke : ".$minggu_ke." (".$thn_bln.")'";
				$data['json_series'] = json_encode($series);

				// if($id > 2){
				// 	$temp_strtotime2 = array();
				// 	foreach ($temp_field2 as $key => $field) {
				// 		foreach ($data['days'] as $key_day => $day) {
				// 			if(!isset($temp_field2[$key][$day])){
				// 				$val_exist = 0;
				// 			}else{
				// 				$val_exist = $temp_field2[$key][$day]; 
				// 			}
				// 			$temp_strtotime2[$key][strtotime($day)] = $val_exist;
				// 		}
				// 		ksort($temp_strtotime2[$key]);
				// 	}

				// 	foreach ($temp_strtotime2 as $key => $dates) {
				// 		$new_key = 0;
				// 		foreach ($dates as $key_dates => $val_dates) {
				// 			unset($temp_strtotime2[$key][$key_dates]);
				// 			$temp_strtotime2[$key][$new_key] = $val_dates;
				// 			$new_key++;
				// 		}
				// 	}


				// 	foreach ($temp_arr2 as $key => $ta) {
				// 		$temp_arr2[$key]['data'] = $temp_strtotime2[$key];
				// 	}
				// 	foreach ($temp_arr2 as $key => $ta) {
				// 		$series2[] = $ta;
				// 	}
				// 	$data['categories2'] = "'".implode("','",$data['str_days'])."'";
				// 	$data['json_series2'] = json_encode($series2);
				// }
			}
		// }

		if($id_unit == null){
			$this->load->view('dashboard/grafik_laporan', $data);
		}else{
			$this->db->where('id', $id_unit);
			$query = $this->db->get('default_organization_units');
			$result = $query->row_array();
			$data['unit_name'] = $result['unit_name'];
			$this->load->view('grafik_laporan', $data);
		}
	}

	public function harga_gap_tti($id_group, $id_komoditas = 'null')
	{
		$this->load->helper('laporan');
		$weeks = getWeeks(date('Y-m-d'), 'monday');
		$minggu_ke = $weeks['week'];
		$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
		$thn = $weeks['tahun'];
		
		$wheres['d.id_group'] = $id_group;
		$wheres['d.is_ttic'] = 0;
		$wheres['m.id_laporan_tipe_field'] = 1;
		$wheres['d.value >'] = 0;
		if($id_komoditas != 'null'){
			$wheres['m.id_laporan_komoditas'] = $id_komoditas;
		}
		$data['data'] = array();
		$data['tipe_laporan'] = array(1=>'Gapoktan',2=>'TTI');
		// if($tipe_laporan == 'harian'){
		// 	$wheres['d.minggu_ke'] = $minggu_ke;
		// 	$wheres['MONTH(d.created_on)'] = $bln;
		// 	$wheres['YEAR(d.created_on)'] = $thn;
		// 	// $wheres['DATE(d.created_on)'] = date('Y-m-d');
		// }else{
			$wheres['a.minggu_ke'] = $minggu_ke;
			$wheres['a.tahun'] = $thn;
			$wheres['a.bulan'] = $bln;
		// }
		$get_data = $this->data_m->laporan_harga_home('mingguan', $wheres);
		foreach ($get_data as $key => $value) {
			$data['data'][$value['id_laporan_tipe']][$value['id_metadata']] = $value;
		}
		$this->load->view('dashboard/harga_gap_tti', $data);
	}

	public function harga_ttic($id_komoditas = 'null')
	{
		$this->load->helper('laporan');
		$weeks = getWeeks(date('Y-m-d'), 'monday');
		$minggu_ke = $weeks['week'];
		$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
		$thn = $weeks['tahun'];
		
		$wheres['d.id_group'] = 9;
		$wheres['d.is_ttic'] = 1;
		$wheres['m.id_laporan_tipe_field'] = 1;
		$wheres['d.value >'] = 0;
		if($id_komoditas != 'null'){
			$wheres['m.id_laporan_komoditas'] = $id_komoditas;
		}
		$data['data'] = array();
		$wheres['d.minggu_ke'] = $minggu_ke;
		$wheres['MONTH(d.created_on)'] = $bln;
		$wheres['YEAR(d.created_on)'] = $thn;
		$data['data'] = $this->data_m->laporan_harga_home('harian', $wheres);
		$this->load->view('dashboard/harga_ttic', $data);
	}

	public function dialog_interaktif()
	{
		$this->load->helper('laporan');
		$this->load->model('dialog_m');
		$data['dialog'] = $this->dialog_m->get_dialog_dashboard();
		$this->load->view('dashboard/dialog_interaktif', $data);
	}

	public function ten_last_report()
	{
		$this->load->helper('laporan');
		$this->load->model('data_m');
		$data['report'] = $this->data_m->get_ten_last_report();
		$this->load->view('dashboard/ten_last_report', $data);
	}

	public function get_metadata_by_komoditas($tipe_laporan, $id_komoditas='1')
	{
		$metadata = $this->metadata_m->get_metadata_by_komoditas($tipe_laporan, $id_komoditas);

		echo json_encode($metadata);

	}

}