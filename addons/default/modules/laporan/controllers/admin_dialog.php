<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_dialog extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'dialog';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'access_dialog_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('laporan');		
		$this->load->model('dialog_m');
  }

  /**
	 * List all dialog
   *
   * @return	void
   */
  public function index()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_dialog')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		if($_POST){
			$id = $_POST['id'];
			$values['status'] = $_POST['status'];
			$this->dialog_m->update_dialog($values, $id);
			$this->session->set_flashdata('success', 'Status berhasil diubah');	
			redirect('admin/laporan/dialog/index?'.$_SERVER['QUERY_STRING']);
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/laporan/dialog/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $this->dialog_m->count_all_dialog();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
    // -------------------------------------
		// Get entries
		// -------------------------------------
		
    $data['dialog']['entries'] = $this->dialog_m->get_dialog($pagination_config);
		$data['dialog']['total'] = count($data['dialog']['entries']);
		$data['dialog']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:dialog:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:dialog:plural'))
			->build('admin/dialog_index', $data);
  }
	
	/**
   * Display one dialog
   *
   * @return  void
   */
  public function view($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_dialog') AND ! group_has_role('laporan', 'view_own_dialog')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
    $data['dialog'] = $this->dialog_m->get_dialog_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_dialog')){
			if($data['dialog']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:dialog:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:dialog:plural'), '/admin/laporan/dialog/index')
			->set_breadcrumb(lang('laporan:dialog:view'))
			->build('admin/dialog_entry', $data);
  }
	
	/**
   * Create a new dialog entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'create_dialog')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_dialog('new')){	
				$this->session->set_flashdata('success', lang('laporan:dialog:submit_success'));				
				redirect('admin/laporan/dialog/index');
			}else{
				$data['messages']['error'] = lang('laporan:dialog:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/laporan/dialog/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('laporan:dialog:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:dialog:plural'), '/admin/laporan/dialog/index')
			->set_breadcrumb(lang('laporan:dialog:new'))
			->build('admin/dialog_form', $data);
  }
	
	/**
   * Edit a dialog entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the dialog to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'edit_all_dialog') AND ! group_has_role('laporan', 'edit_own_dialog')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('laporan', 'edit_all_dialog')){
			$entry = $this->dialog_m->get_dialog_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_dialog('edit', $id)){	
				$this->session->set_flashdata('success', lang('laporan:dialog:submit_success'));				
				redirect('admin/laporan/dialog/index');
			}else{
				$data['messages']['error'] = lang('laporan:dialog:submit_failure');
			}
		}
		
		$data['fields'] = $this->dialog_m->get_dialog_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/laporan/dialog/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:dialog:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:dialog:plural'), '/admin/laporan/dialog/index')
			->set_breadcrumb(lang('laporan:dialog:view'), '/admin/laporan/dialog/view/'.$id)
			->set_breadcrumb(lang('laporan:dialog:edit'))
			->build('admin/dialog_form', $data);
  }
	
	/**
   * Delete a dialog entry
   * 
   * @param   int [$id] The id of dialog to be deleted
   * @return  void
   */
  public function delete($id = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'delete_all_dialog') AND ! group_has_role('laporan', 'delete_own_dialog')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('laporan', 'delete_all_dialog')){
			$entry = $this->dialog_m->get_dialog_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
    $this->dialog_m->delete_dialog_by_id($id);
    $this->session->set_flashdata('error', lang('laporan:dialog:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
    redirect('admin/laporan/dialog/index');
  }
	
	/**
   * Insert or update dialog entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_dialog($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('laporan:dialog:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->dialog_m->insert_dialog($values);
				
			}
			else
			{
				$result = $this->dialog_m->update_dialog($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}