<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_metadata extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'metadata';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'access_metadata_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('laporan');		
		$this->load->model('metadata_m');
		$this->load->model('tipe_m');
		$this->load->model('tipe_field_m');
		$this->load->model('komoditas_m');
  }

  /**
	 * List all metadata
   *
   * @return	void
   */
  public function index()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_metadata')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$id_tipes = array(1,2, 8,9);
		$filters[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filters);
		$data['komoditas'] = $this->komoditas_m->get_komoditas();
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$surffix = '';
    if($_SERVER['QUERY_STRING']){
      $surffix = '?'.$_SERVER['QUERY_STRING'];
    }

    $metadata =$this->metadata_m->get_metadata();

		$pagination_config['base_url'] = base_url(). 'admin/laporan/metadata/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($metadata);
		// $pagination_config['per_page'] = 2;
		$pagination_config['per_page'] = ($this->input->get('f-tipe_laporan') ? count($metadata) : Settings::get('records_per_page'));
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
    // -------------------------------------
		// Get entries
		// -------------------------------------
		
    $data['metadata']['entries'] = $this->metadata_m->get_metadata($pagination_config);
		$data['metadata']['total'] = count($metadata);
		$data['metadata']['pagination'] = $this->pagination->create_links();

		$data['uri'] = $this->get_query_string(5);

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
		$this->template
      ->append_css('module::laporan.css')
      ->append_js('module::jquery.tablednd.js');

    $this->template->title(lang('laporan:metadata:plural'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:metadata:plural'))
			->build('admin/metadata_index', $data);
  }
	
	/**
   * Create a new metadata entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
  public function create()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_metadata')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_metadata('new')){	
				$this->session->set_flashdata('success', lang('laporan:metadata:submit_success'));				
				redirect('admin/laporan/metadata/index'.$this->get_query_string(5));
			}else{
				$data['messages']['error'] = lang('laporan:metadata:submit_failure');
			}
		}
		

		$data['komoditas'] = $this->komoditas_m->get_komoditas();
		$id_tipes = array(1,2, 8,9);
		$filters[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filters);
		$data['tipe_field'] = $this->tipe_field_m->get_tipe_field();

		$data['mode'] = 'new';
		$data['uri'] = $this->get_query_string(5);
		$data['return'] = 'admin/laporan/metadata/index'.$data['uri'];

		$metadata =$this->metadata_m->get_metadata();
		$data['total'] = count($metadata) + 1;

		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:metadata:new'))
    	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:metadata:plural'), '/admin/laporan/metadata/index')
			->set_breadcrumb(lang('laporan:data:new'))
			->build('admin/metadata_form', $data);
  }
	
	/**
   * Edit a metadata entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the metadata to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_metadata')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_metadata('edit', $id)){	
				$this->session->set_flashdata('success', lang('laporan:metadata:submit_success'));				
				redirect('admin/laporan/metadata/index'.$this->get_query_string(6));
			}else{
				$data['messages']['error'] = lang('laporan:metadata:submit_failure');
			}
		}
		
		$data['komoditas'] = $this->komoditas_m->get_komoditas();
		$id_tipes = array(1,2, 8,9);
		$filters[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filters);
		$data['fields'] = $this->metadata_m->get_metadata_by_id($id);
		$data['tipe_field'] = $this->tipe_field_m->get_tipe_field();

		$data['mode'] = 'edit';
		$data['uri'] = $this->get_query_string(6);
		$data['return'] = 'admin/laporan/metadata/index'.$data['uri'];
		$data['entry_id'] = $id;
		
		$metadata = $this->metadata_m->get_metadata();
		$data['total'] = count($metadata);


		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:metadata:edit'))
    	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:metadata:plural'), '/admin/laporan/metadata/index')
			->set_breadcrumb(lang('laporan:metadata:edit'))
			->build('admin/metadata_form', $data);
  }
	
	/**
   * Delete a metadata entry
   * 
   * @param   int [$id] The id of metadata to be deleted
   * @return  void
   */
  public function delete($id = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_metadata')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// Cek data

		if($this->metadata_m->is_used($id)){
			// -------------------------------------
			// Delete entry
			// -------------------------------------
			
	    $this->metadata_m->delete_metadata_by_id($id);
	    $this->session->set_flashdata('error', lang('laporan:metadata:deleted'));
		}else{
	    $this->session->set_flashdata('error', lang('laporan:metadata:is_used'));
		}
	
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
		$pagination_config['uri_segment'] = 6;
		$pagination_config['per_page'] = 25;
		$metadata =$this->metadata_m->get_metadata($pagination_config);
		$total = count($metadata);

		$page = $this->uri->segment(6);
		$cek_page = $total%25;
		if($total > 0){
			if($cek_page == 0){
				$page = $page-25;
			}
		}
		$uri = '';
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }

    redirect('admin/laporan/metadata/index'.$uri);
  }
	
	/**
   * Insert or update metadata entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_metadata($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		if($values['id_laporan_tipe'] != 8 && $values['id_laporan_tipe'] != 9){
			$this->form_validation->set_rules('id_laporan_komoditas', lang('laporan:komoditas'), 'required');
		}else{
			$values['id_laporan_komoditas'] = null;
		}
		$this->form_validation->set_rules('id_laporan_tipe', lang('laporan:tipe_laporan'), 'required');
		$this->form_validation->set_rules('id_laporan_tipe_field', lang('laporan:tipe_field'), 'required');
		$this->form_validation->set_rules('field', lang('laporan:field'), 'required|max_length[45]|alpha_numeric');
		$this->form_validation->set_rules('nama', lang('laporan:nama'), 'required|max_length[255]');
		$this->form_validation->set_rules('slug', lang('laporan:slug'), 'required|max_length[45]|alpha_dash');
		$this->form_validation->set_rules('satuan', lang('laporan:satuan'), 'required|max_length[45]');
		$this->form_validation->set_rules('nilai_minimal_home', lang('laporan:nilai_minimal_home'),'required|integer|max_length[11]|callback_cek_nilai_home');
		$this->form_validation->set_rules('nilai_maksimal_home', lang('laporan:nilai_maksimal_home'),'required|integer|max_length[11]');
		
	
		if($values['nilai_maksimal'] != ''){
			$this->form_validation->set_rules('nilai_minimal', lang('laporan:nilai_minimal'),'required|integer|max_length[11]|callback_cek_nilai');
		}

		if($values['nilai_minimal'] != ''){
			$this->form_validation->set_rules('nilai_maksimal', lang('laporan:nilai_maksimal'), 'required|integer|max_length[11]');
		}
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if($this->input->post('nilai_minimal') == ""){
				$values['nilai_minimal'] = NULL;
			}
			if($this->input->post('nilai_maksimal') == ""){
				$values['nilai_maksimal'] = NULL;
			}
				
			if ($method == 'new')
			{
				$result = $this->metadata_m->insert_metadata($values);
			}
			else
			{
				$result = $this->metadata_m->update_metadata($values, $row_id);
			}
		}
		
		return $result;
	}

	public function get_query_string($page){
		// -------------------------------------
		// Get query string
		// -------------------------------------

		$page = $this->uri->segment($page);
		$uri = '';
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }
    
    return $uri;
	}

	public function cek_nilai_home(){
		$nilai_minimal_home = $this->input->post('nilai_minimal_home');
		$nilai_maksimal_home = $this->input->post('nilai_maksimal_home');
		if($nilai_minimal_home != '' && $nilai_maksimal_home != ''){
			if($nilai_minimal_home > $nilai_maksimal_home){
				$this->form_validation->set_message('cek_nilai_home', '<b>Nilai Minimal Beranda</b> harus lebih kecil dari Nilai Maksimal Beranda');
				return false;
			}
		}

		return true;
	}

	public function cek_nilai(){
		$nilai_minimal = $this->input->post('nilai_minimal');
		$nilai_maksimal = $this->input->post('nilai_maksimal');
		if($nilai_minimal != '' && $nilai_maksimal != ''){
			if($nilai_minimal > $nilai_maksimal){
				$this->form_validation->set_message('cek_nilai', '<b>Nilai Minimal Input</b> harus lebih kecil dari Nilai Maksimal Input');
				return false;
			}
		}

		return true;
	}

	public function update_urutan(){
    $result = $_REQUEST["table-1"];
    $id_laporan_komoditas = $_REQUEST['id_laporan_komoditas'];
    $id_laporan_tipe = $_REQUEST['id_laporan_tipe'];
    $i = 0;
    foreach($result as $id) {
    	if($id != ""){
	      $this->metadata_m->update_urutan($id,$id_laporan_tipe,$id_laporan_komoditas,$i);
	      $i++;
	    }
    }   
  }
	// --------------------------------------------------------------------------

}