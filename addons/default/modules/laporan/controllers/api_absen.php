<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Absen
*
* Module API
*
*/
class Api_absen extends API2_Controller
{
	public $metod = 'get';

	// This controller simply redirect to main section controller
	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set('Asia/Jakarta');

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper('laporan/laporan');

		$this->lang->load('laporan');
		$this->lang->load('location/location');

		$this->load->model('absen_m');
		$this->load->model('tipe_m');
		$this->load->model('pendamping_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');

		// $this->authorize_api_access('laporan','api_absen');
	}

	public function get_all($id = NULL)
	{
		if($this->input->get('id_provinsi')) {
			$_GET['f-provinsi'] = $this->input->get('id_provinsi');
			unset($_GET['id_provinsi']);
		}

		if($this->input->get('id_kota')) {
			$_GET['f-kota'] = $this->input->get('id_kota');
			unset($_GET['id_kota']);
		}

		$_GET['f-thn'] = date('Y');
		$_GET['f-bln'] = date('m');

		if(empty($id)) {
			$result = array("status"=>"error","messages"=>"Parameter id laporan not found");
			$status = "200";
			_output($result,$status);
		} else {

			$data['id'] = $id;

			$filter_kota = null;
			if($this->input->get('f-provinsi') != '') {
				$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
				$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
			}

			$surffix = '';
			if($_SERVER['QUERY_STRING']){
				$surffix = '?'.$_SERVER['QUERY_STRING'];
			}

			// -------------------------------------
			// Pagination
			// -------------------------------------

			$nama_laporan = $this->tipe_m->get_tipe_by_id($id)['nama_laporan'];
			if(empty($nama_laporan)) {
				$result = array("status"=>"error","messages"=>"Laporan not found");
				$status = "200";
				_output($result,$status);
			} else {
				$count_data = $this->absen_m->get_absen_per_laporan($id, date('Y-m'), NULL, 1);

				$pagination_config['uri_segment'] = 4;
				$pagination_config['per_page'] = 25;
				// $pagination_config['per_page'] = Settings::get('records_per_page');
				$data['pagination_config'] = $pagination_config;

				// Periode
				$weeks = getWeeks(date('Y-m-d'), 'monday');
				$minggu_ke = (string) $weeks['week'];
				if($this->input->get('f-minggu_ke')){
					$minggu_ke = $this->input->get('f-minggu_ke');
				}
				$data['minggu_ke'] = $minggu_ke;

				$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
				$thn_bln = $weeks['tahun'].'-'.$bln;
				if($this->input->get('f-bln') && $this->input->get('f-thn')){
					$thn_bln = $this->input->get('f-thn')."-".$this->input->get('f-bln');
				}

				$ex_thn_bln = explode('-',$thn_bln);
				$data['thn'] = $ex_thn_bln[0];
				$data['bln'] = $ex_thn_bln[1];

				$firstDayOfWeek = $this->absen_m->get_first_day_of_week($minggu_ke, $thn_bln, $id);
				$data['firstDayOfWeek'] = $firstDayOfWeek;
				$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
				$data['min_year'] = $this->absen_m->get_min_year();
				$data['max_year'] = $this->absen_m->get_max_year();


				// -------------------------------------
				// Get entries
				// -------------------------------------

				$data['nama_laporan'] = $nama_laporan;
				$data['jadwal'] = array();
				$data['absen_per_enum'] = array();
				$data['total'] = 0;

				if($firstDayOfWeek != NULL){
					$data['jadwal'] = $this->absen_m->get_absen_by_year_month($thn_bln, $id);
					$data['absen_per_enum'] = $this->absen_m->get_absen_per_laporan($id, $thn_bln, $pagination_config);
					$data['total'] = $count_data;

					$minggu = array();
					foreach ($data['jadwal'] as $key => $jadwal) {
						array_push($minggu, $jadwal['minggu_ke']);
					}
					$data['minggu'] = $minggu;
				}

				// header table
				$jml_hari = 0;
				$header = array();
				foreach ($data['jadwal'] as $key => $absen) {
	                $header[$key]['week_num'] = 'M'.$absen['minggu_ke'];
	                $header[$key]['date'] = '('.date_idr($absen['tanggal_awal'],'d/m',null).' - '.date_idr($absen['tanggal_akhir'],'d/m',null).')';

	                $i = 1;
	                foreach ($absen['count_day'] as $key2 => $tanggal) {
		        		$header[$key]['day_of_week'][$key2]['day_num'] = 'H'.$i;
		        		$header[$key]['day_of_week'][$key2]['date'] = '('.date_idr($tanggal, 'd/m', null).')';
		        		$i++;
		        		$jml_hari++;
		        	}
		        }

		        // body table
		        $body_data = NULL;
		        foreach ($data['absen_per_enum'] as $key => $enum) {
		        	$row_data['provinsi'] = $enum['provinsi'];
	                $row_data['kota'] = $enum['kota'];
	                $row_data['display_name'] = $enum['display_name'];

	                $jml_hadir = 0;
	                $present_list = array();
	                foreach ($minggu as $week) {
			            if(count($enum['days_on_week'.$week]) > 0){
		            		foreach ($enum['days_on_week'.$week] as $key2 => $val) {
		            			if($val == 1) {
		            				$jml_hadir++;
		            				$present_list['M'.$week]['H'.$key2] = true;
		            			}else{
		            				$present_list['M'.$week]['H'.$key2] = false;
		            			}
			            	}
			            }else{
			            	$present_list['M'.$week]['H'.$key2] = false;
			            }
			        }

			        $row_data['present_list'] = $present_list;
			        $row_data['percent'] = round(($jml_hadir/$jml_hari)*100);

			        $body_data[] = $row_data;
		        }

		        $result = array('header'=>$header,'body'=>$body_data);
		        $status = "200";

		        if(empty($body_data)) {
		        	$result = array("status"=>"error","messages"=>"Absensi data not found");
					$status = "200";
		        }
		        _output($result,$status);
		    }
		}
	}

	public function get_grafik_data($value='')
	{
		$weeks = getWeeks(date('Y-m-d'), 'monday');

		$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
		$thn_bln = $weeks['tahun'].'-'.$bln;
		if($this->input->get('f-bln') && $this->input->get('f-thn')){
			$thn_bln = $this->input->get('f-thn')."-".$this->input->get('f-bln');
		}

		$date = date($thn_bln.'-01');
		if($this->input->get('f-thn') != '' && $this->input->get('f-bln') != ''){
			$date = $this->input->get('f-thn').'-'.$this->input->get('f-bln').'-01';
		}

		$end_week = get_count_of_week($date);
		$data['end_week'] = $end_week;

		$minggu_ke = (string) $weeks['week'];
		if($this->input->get('f-minggu_ke')){
			$minggu_ke = $this->input->get('f-minggu_ke');
		}
		$data['minggu_ke'] = $minggu_ke;

		$ex_thn_bln = explode('-',$thn_bln);
		$data['thn'] = $ex_thn_bln[0];
		$data['bln'] = $ex_thn_bln[1];

		$firstDayOfWeek = $this->absen_m->get_first_day_of_week($minggu_ke, $thn_bln);
		$data['firstDayOfWeek'] = $firstDayOfWeek;
		$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
		$data['min_year'] = $this->absen_m->get_min_year();
		$data['max_year'] = $this->absen_m->get_max_year();

		// ---------------------------------------------------------------------

		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		$filter_kota = null;
		if($this->input->get('f-provinsi') != '') {
			$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
			$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		}

		// Set Location
		$filter_kota = null;
		if($this->input->get('f-provinsi') != '') {
			$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
			$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		}

		$data['get_location'] = '';
		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;
		if($id_provinsi != NULL){
			$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
			$kota = "";
			if($this->input->get('f-kota') != ''){
				$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
			}
			$data['get_location'] .= '<br>Provinsi '.$provinsi.$kota;
		}

		$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');

		if($this->input->get('f-bln') != '' || $this->input->get('f-thn') != '' || $this->input->get('f-minggu_ke') != ''){
			if($this->input->get('f-minggu_ke') != ''){
				$periode[] = 'Minggu Ke '.$this->input->get('f-minggu_ke');
			}
			if($this->input->get('f-bln') != ''){
				$periode[] = $arr_month[$this->input->get('f-bln')];
			}
			if($this->input->get('f-thn') != ''){
				$periode[] = $this->input->get('f-thn');
			}
		}else{
			$periode[] = 'Minggu Ke '.$minggu_ke;
			$periode[] = $arr_month[date('m')];
			$periode[] = date('Y');
		}

		$data['get_periode'] = '';
		if(count($periode) > 0){
			$implode_periode = implode(' ',$periode);
			$data['get_periode'] = '<br>'.$implode_periode;
		}

		$data['tipes'] = $this->tipe_m->get_tipe();

		$pendamping = $this->absen_m->grafik_absensi($thn_bln, $minggu_ke);

		$result = $pendamping;
        $status = "200";

        if(empty($pendamping)) {
        	$result = array("status"=>"error","messages"=>"Absensi data not found");
			$status = "200";
        }
        _output($result,$status);
	}
}
