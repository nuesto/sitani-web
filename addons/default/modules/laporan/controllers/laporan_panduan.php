<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Laporan_panduan extends Public_Controller
{
	public function __construct()
	{
  	parent::__construct();
  	$this->lang->load('laporan');	
		$this->load->model('tipe_m');
		$this->load->model('metadata_m');
    $this->load->model('komoditas_m');
	}
	public function sms()
  {

  	$this->load->config('../../sms_gateway_api/config/apiconfig', true);
    $this->load->helper('laporan');
    $data['nohp'] = $this->config->item('nohp', 'apiconfig');

    $id_tipes = array();
    foreach (range(1, 7) as $number) {
        $id_tipes[] = $number;
    }
    $filter_tipes[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
    $tipes = $this->tipe_m->get_all_tipe($filter_tipes);
    
    if ($this->input->get('f-komoditas')){
      $id_komoditas = $this->input->get('f-komoditas');
    } else {
      $id_komoditas = 1;
    }

    foreach ($tipes as $key => $tipe) {
    	if ($tipe['id'] == 3 || $tipe['id'] == 4){
        $tipe_id = 1;
      } else if ($tipe['id'] == 5){
        $tipe_id = 2;
      } else {
        $tipe_id = $tipe['id'];
      }

      $id = $tipe_id;
    	$kode_sms = $tipe['kode_sms'];
    	$data['metadatas'][$kode_sms] = $this->metadata_m->get_metadata_by_tipe_laporan($id, $id_komoditas, '*', NULL, 'urutan');
    	$data['laporan'][$kode_sms] = $tipe['nama_laporan'];
    }

    $data['data_komoditas']= $this->komoditas_m->get_komoditas();
    $data['kode_komoditas']= $this->komoditas_m->get_komoditas_by_id($id_komoditas);

	
    $this->template->title(lang('laporan:input_sms'))
		->set_breadcrumb('Home', '/')
		->set_breadcrumb(lang('laporan:cara_pelaporan'),'/laporan/panduan/sms')
		->set_breadcrumb(lang('laporan:input_sms'))
		->build('panduan_sms',$data);
  }
	public function web()
  {
	
    $this->template->title(lang('laporan:input_web'))
		->set_breadcrumb('Home', '/')
		->set_breadcrumb(lang('laporan:cara_pelaporan'),'/laporan/panduan/web')
		->set_breadcrumb(lang('laporan:input_web'))
		->build('panduan_web');
  }

  public function android()
  {
	
    $this->template->title(lang('laporan:input_android'))
		->set_breadcrumb('Home', '/')
		->set_breadcrumb(lang('laporan:cara_pelaporan'),'/laporan/panduan/android')
		->set_breadcrumb(lang('laporan:input_android'))
		->build('panduan_android');
  }
}