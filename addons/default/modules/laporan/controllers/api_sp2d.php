<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API DOC SP2D
*
* Module API
*
*/
class Api_sp2d extends API2_Controller
{
	public $metod = 'get';

	public function __construct()
	{
		parent::__construct();

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		// Load the required classes

		$this->lang->load('laporan');
		$this->lang->load('location/location');

		$this->load->model('organization/memberships_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
		$this->load->model('sp2d_m');

		// $this->authorize_api_access('laporan','api_sp2d');
	}

	public function index(){
		$surffix = '';
		if($_SERVER['QUERY_STRING']){
			$surffix = '?'.$_SERVER['QUERY_STRING'];
		}

		// -------------------------------------
		// Pagination
		// -------------------------------------
		// $count_sp2d = $this->memberships_m->get_membership();
		$count_sp2d = $this->sp2d_m->get_sp2d(NULL, NULL, TRUE);
		$pagination_config['base_url'] = base_url(). 'laporan/sp2d/index/';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($count_sp2d);
		$pagination_config['per_page'] = 10;
		// $pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

		// -------------------------------------
		// Get entries
		// -------------------------------------

		// $data['sp2d']['entries'] = $this->memberships_m->get_membership($pagination_config);
		$data['sp2d']['entries'] = $this->sp2d_m->get_sp2d($pagination_config, NULL, TRUE);
		$data['sp2d']['total'] = count($count_sp2d);
		$data['sp2d']['pagination'] = $this->pagination->create_links();

		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();


		$uri = '';
		$page = $this->uri->segment(4);
		if($page){
			$uri = '/'.$page;
		}
		if($_SERVER['QUERY_STRING']){
			$uri = '?'.$_SERVER['QUERY_STRING'];
		}
		if($_SERVER['QUERY_STRING'] && $page){
			$uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
		}
		$data['uri'] = $uri;

		$result = $data['sp2d']['entries'];
        $status = "200";

        if(empty($result)) {
        	$result = array("status"=>"error","messages"=>"SP2D Documents not found");
			$status = "200";
        } else {
        	$fields = array('id','id_provinsi','provinsi','display_name','created_by','created_on','nama_sp2d','deskripsi','file');
        	foreach ($result as $key => $value) {
        		foreach ($value as $key2 => $value2) {
        			if(!in_array($key2, $fields)) {
        				unset($result[$key][$key2]);
	        		}
        		}
        	}
        }
        _output($result,$status);
	}
}
