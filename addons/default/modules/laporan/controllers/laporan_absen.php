<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Laporan_absen extends Public_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------

  protected $section = 'absen';

  public function __construct()
  {
    parent::__construct();

    date_default_timezone_set('Asia/Jakarta');
    // -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->load->helper('laporan/laporan');

    $this->lang->load('laporan');
    $this->lang->load('location/location');

		$this->load->model('absen_m');
		$this->load->model('tipe_m');
		$this->load->model('pendamping_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');

  }

  /**
	 * List all absen
   *
   * @return	void
   */

  public function index($id)
  {

		$data['id'] = $id;
		$id_tipe_laporan = $id;
		$gap_type_ids = array(3, 4, 6);
		if(in_array($id_tipe_laporan, $gap_type_ids)){
			$id_tipe_laporan = 1;
		}
		$tti_type_ids = array(5, 7);
		if(in_array($id_tipe_laporan, $tti_type_ids)){
			$id_tipe_laporan = 2;
		}
  	$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		$filter_kota = null;
  	if($this->input->get('f-provinsi') != '') {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
  	}

  	//--------------------------------------------------------------------------------------------------------

		// Set default periode
		$weeks = getWeeks(date('Y-m-d'), 'monday');
    //--------------------------------------------------------------------------------------------------------

		// Set Periode 1
    //--------------------------------------------------------------------------------------------------------

		// Set Tahun
		$tahun = NULL;
		if($this->input->get('f-tahun')){
			$tahun = $this->input->get('f-tahun');
		}else{
			$tahun = $weeks['tahun'];
		}
		$data['tahun'] = $tahun;

		// if($id <= 2){
			$wheres[] = array('column'=>'a.tahun','function'=>'where','value'=>$tahun);
		// }else{
		// 	$wheres[] = array('column'=>'YEAR(d.created_on)','function'=>'where','value'=>$tahun);
		// }

		$data['min_year'] = $this->absen_m->get_min_year(); 
		$data['max_year'] = $this->absen_m->get_max_year();

    //--------------------------------------------------------------------------------------------------------

		// Set Bulan
		if($this->input->get('f-bulan')){
			$bulan = $this->input->get('f-bulan');
		}else{
			$bulan = $weeks['bulan'];
		}

		$bulan = (strlen($bulan) == 1) ? '0'.$bulan : $bulan;
		$thn_bln = $tahun."-".$bulan;

		$data['bulan'] = $bulan;
		// if($id <= 2){
			$wheres[] = array('column'=>'a.bulan','function'=>'where','value'=>$bulan);
		// }else{
			// $wheres[] = array('column'=>'MONTH(d.created_on)','function'=>'where','value'=>$bulan);
		// }
    //--------------------------------------------------------------------------------------------------------

		// Set Jumlah Minggu
		$first_day_of_month = $thn_bln.'-01';

		$count_of_week = get_count_of_week($first_day_of_month);
		$data['count_of_week'] = $count_of_week;
    //--------------------------------------------------------------------------------------------------------

		// Minggu ke
		if($this->input->get('f-minggu_ke')){
			$minggu_ke = $this->input->get('f-minggu_ke');
		}else{
			$minggu_ke = (string) $weeks['week'];
		}
		// if($id > 2){
			// $wheres[] = array('column'=>'d.minggu_ke','function'=>'where','value'=>$minggu_ke);
		// }

		$data['minggu_ke'] = $minggu_ke;

    //--------------------------------------------------------------------------------------------------------

    $nama_laporan = $this->tipe_m->get_tipe_by_id($id)['nama_laporan'];

		$firstDayOfWeek = $this->absen_m->get_first_day_of_week($data['tahun'], $data['bulan'], $minggu_ke, $id_tipe_laporan);
		$weeks = getWeeks($firstDayOfWeek, 'monday');
		$data['firstDayOfWeek'] = $firstDayOfWeek;
		$data['weeks'] = $weeks;

		// Set Header Text Report Periode 1
		$periode = array();
		$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
		
		if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bulan') != '' || $this->input->get('f-tahun') != ''){

			if($this->input->get('f-minggu_ke') != ''){
				$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
			}
			if($this->input->get('f-bulan') != ''){
				$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan')];
			}
			if($this->input->get('f-tahun') != ''){
				$periode[] = 'Tahun '.$this->input->get('f-tahun');
			}
		}else{
			$periode[] = ' Minggu Ke '.$minggu_ke;
			$periode[] = 'Bulan '.$arr_month[$bulan];
			$periode[] = 'Tahun '.$tahun;
		}

		if(count($periode) > 0){
			$data['text_periode'] = 'Periode: '.implode(', ',$periode);
		}

  	// -------------------------------------
		// Get entries
		// -------------------------------------

	  $data['nama_laporan'] = $nama_laporan;


    if($firstDayOfWeek != NULL){

    	$surffix = '';
	    if($_SERVER['QUERY_STRING']){
	      $surffix = '?'.$_SERVER['QUERY_STRING'];
	    }
    	$count_data = count($this->pendamping_m->get_pendamping_by_type($id, NULL));
    	// dump($this->db->last_query());
    	// die();
	    if(!$this->input->get('page')){
				$pagination_config['base_url'] = base_url(). 'laporan/absen/index/'.$id;
				$pagination_config['uri_segment'] = 5;
				$pagination_config['suffix'] = $surffix;
				$pagination_config['total_rows'] = $count_data;
				// $pagination_config['per_page'] = 2;
				$pagination_config['per_page'] = Settings::get('records_per_page');;
				$this->pagination->initialize($pagination_config);
				$data['pagination_config'] = $pagination_config;

				$data['total'] = $count_data;
				$data['pagination'] = $this->pagination->create_links();
			}else{
				$pagination_config = NULL;
			}

			// if($id <= 2){
				$data['jadwal'] = $this->absen_m->get_absen_by_year_month($thn_bln, $id_tipe_laporan);
				// dump($data['jadwal']);
			// }else{
			// 	$data['jadwal'][0]['minggu_ke'] = $weeks['week'];
			// 	$data['jadwal'][0]['tanggal_awal'] = $weeks['first_day_of_week'];
			// 	$data['jadwal'][0]['tanggal_akhir'] = $weeks['last_day_of_week'];
			// 	$day_looper = $weeks['first_day_of_week'];
			// 	for ($h=0; $h <=6 ; $h++) {
			// 		$add_day2 = date('Y-m-d', strtotime('+'.$h.' days', strtotime($day_looper))); 
			// 		$data['jadwal'][0]['count_day'][] = $add_day2;
			// 	}
			// }

			$minggu = array();
			$data['col_hari'] = 0; 
			foreach ($data['jadwal'] as $key => $jadwal) {
				$minggu[] = $jadwal['minggu_ke'];
				foreach ($jadwal['count_day'] as $key_day => $day) {
					$data['col_hari'] += 1; 
				}
			}

			if($count_data > 0){

				$data_pendamping = $this->pendamping_m->get_pendamping_by_type($id, $pagination_config);
				$users = array();
				$units = array();

				foreach ($data_pendamping as $key => $pendamping) {
					$users[$pendamping['user_id']] = $pendamping['user_id'];
					$units[$pendamping['id_unit']] = $pendamping['id_unit'];
				}

				if($id != 3){
					$wheres[] = array('column'=>'d.id_user','function'=>'where_in','value'=>$users);
					$wheres[] = array('column'=>'d.id_unit','function'=>'where_in','value'=>$units);
					$wheres[] = array('column'=>'d.is_ttic','function'=>'where','value'=>0);
				}else{
					$wheres[] = array('column'=>'d.is_ttic','function'=>'where','value'=>1);
				}

				$data_entries = $this->absen_m->get_absen_user_by_type($id, $wheres);
				// dump($this->db->last_query(), $data_entries);

				foreach ($data_pendamping as $key_pendamping => $pendamping) {
					foreach ($data['jadwal'] as $key => $jadwal) {
						$jadwal_minggu_ke = $jadwal['minggu_ke'];
						foreach ($jadwal['count_day'] as $jk => $jc) {
							$data_pendamping[$key_pendamping]['days_on_week'.$jadwal_minggu_ke][$jc] = 0; 
						
							foreach ($data_entries as $key_data => $entry) {
								if($pendamping['user_id'] == $entry['id_user'] && $pendamping['id_unit'] == $entry['id_unit'] && $jadwal['minggu_ke'] == $entry['minggu_ke'] && $jc == $entry['tanggal']){
									$data_pendamping[$key_pendamping]['days_on_week'.$jadwal_minggu_ke][$jc] = 1; 
								}
							}
						}
					}
				}
				$data['absensi'] = $data_pendamping;
			}else{
				$data['absensi'] = array();
			}
			$data['minggu'] = $minggu;
		}

		if(!$this->input->get('page')){
	    $this->template->title('Absensi Pelaporan')
				->set_breadcrumb('Home', '/')
				->set_breadcrumb('Absensi', '/laporan/absen/index/'.$id)
				->set_breadcrumb($nama_laporan)
				->build('absen_index', $data);
		}else{
			$this->download($data, $this->input->get('page'));
		}
  }

  public function grafik_absensi(){

  	$weeks = getWeeks(date('Y-m-d'), 'monday');

		$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
		$thn_bln = $weeks['tahun'].'-'.$bln;
		if($this->input->get('f-bln') && $this->input->get('f-thn')){
			$thn_bln = $this->input->get('f-thn')."-".$this->input->get('f-bln');
		}

  	$date = date($thn_bln.'-01');
		if($this->input->get('f-thn') != '' && $this->input->get('f-bln') != ''){
			$date = $this->input->get('f-thn').'-'.$this->input->get('f-bln').'-01';
		}

		$end_week = get_count_of_week($date);
		$data['end_week'] = $end_week;

  	$minggu_ke = (string) $weeks['week'];
		if($this->input->get('f-minggu_ke')){
			$minggu_ke = $this->input->get('f-minggu_ke');
		}
		$data['minggu_ke'] = $minggu_ke;

		$ex_thn_bln = explode('-',$thn_bln);
		$data['thn'] = $ex_thn_bln[0];
		$data['bln'] = $ex_thn_bln[1];

		$firstDayOfWeek = $this->absen_m->get_first_day_of_week($ex_thn_bln[0], $ex_thn_bln[1], $minggu_ke);
		$data['firstDayOfWeek'] = $firstDayOfWeek;
		$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
		$data['min_year'] = $this->absen_m->get_min_year();
		$data['max_year'] = $this->absen_m->get_max_year();

		// ---------------------------------------------------------------------

		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		$filter_kota = null;
  	if($this->input->get('f-provinsi') != '') {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
  	}

		// Set Location
		$filter_kota = null;
  	if($this->input->get('f-provinsi') != '') {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
  	}

  	$data['get_location'] = '';
  	$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;
  	if($id_provinsi != NULL){
	  	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
	  	$kota = "";
	  	if($this->input->get('f-kota') != ''){
	  		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	  	}
			$data['get_location'] .= '<br>Provinsi '.$provinsi.$kota;
		}

		$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');

		if($this->input->get('f-bln') != '' || $this->input->get('f-thn') != '' || $this->input->get('f-minggu_ke') != ''){
			if($this->input->get('f-minggu_ke') != ''){
				$periode[] = 'Minggu Ke '.$this->input->get('f-minggu_ke');
			}
			if($this->input->get('f-bln') != ''){
				$periode[] = $arr_month[$this->input->get('f-bln')];
			}
			if($this->input->get('f-thn') != ''){
				$periode[] = $this->input->get('f-thn');
			}
		}else{
			$periode[] = 'Minggu Ke '.$minggu_ke;
			$periode[] = $arr_month[date('m')];
			$periode[] = date('Y');
 		}

		$data['get_periode'] = '';
		if(count($periode) > 0){
			$implode_periode = implode(' ',$periode);
			$data['get_periode'] = '<br>'.$implode_periode;
		}

  	$data['tipes'] = $this->tipe_m->get_tipe();

  	$pendamping = $this->absen_m->grafik_absensi($thn_bln, $minggu_ke);
  	$data['pendamping'] = $pendamping;
  	$melapor = array();
  	$belum_lapor = array();
  	$type_names = array();
  	foreach ($pendamping as $key => $type) {
  		array_push($melapor,$type['melapor']);
  		array_push($belum_lapor,$type['belum_lapor']);
  		array_push($type_names,$type['type_name']);
  	}

  	$data['melapor'] = implode(', ',$melapor);
  	$data['belum_lapor'] = implode(', ',$belum_lapor);
  	$data['type_names'] = "'".implode("','",$type_names)."'";

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------

    $this->template->title(lang('laporan:pendamping:singular'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('laporan:grafik_absensi'))
			->build('grafik_absensi', $data);
  }

	public function download($data, $page){

		$nama_laporan = $data['nama_laporan'];
		$colspan = 5 + $data['col_hari'];
		$judul = "Absen Pelaporan - ".$nama_laporan;
		$nama_file = $judul;

		$html = "
    <table border=\"0\" cellpadding=\"5\">
      <tr>
          <td colspan=\"".$colspan."\"></td>
      </tr>
      <tr>
          <td colspan=\"".$colspan."\" align=\"center\">
              <span style=\"font: bold 20px Open Sans; display: block;\">".$judul."</span>
          </td>
      </tr>";

      if($this->input->get('f-provinsi') != ''){
      	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
      	$kota = "";
      	if($this->input->get('f-kota') != ''){
      		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
      	}
				$html .= '
					<tr>
						<td colspan="'.$colspan.'" align="center">
							<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
						</td>
					</tr>
				';
				$nama_file .= '_'.$provinsi.$kota;
			}

			$html .= '
				<tr>
					<td colspan="'.$colspan.'" align="center">
						<span style="font: bold 20px Open Sans; display: block;">Periode: '.$data['text_periode'].'</span>
					</td>
				</tr>
			';

      $html .= "
      <tr>
          <td colspan=\"".$colspan."\"></td>
      </tr>
    </table>";

  	$html .= '
  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
  		$html .='
      <thead style="background-color:#ccc;">
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Provinsi</th>
					<th rowspan="2">Kota/Kabupaten</th>
					<th rowspan="2">Nama</th>';
					foreach ($data['jadwal'] as $key => $absen) {
						if(count($absen['count_day']) > 1){
							$html .= '<th colspan="'.count($absen['count_day']).'">M'.$absen['minggu_ke'].'<br>('.date_idr($absen['tanggal_awal'],'d/m',null).' - '.date_idr($absen['tanggal_akhir'],'d/m',null).')</th>';
						}else{
							$html .= '<th>M'.$absen['minggu_ke'].'<br>('.date_idr($absen['tanggal_awal'],'d/m',null).' - '.date_idr($absen['tanggal_akhir'],'d/m',null).')</th>';
						}
					}
					$html .='
					<th rowspan="2">Presentase</th>
				</tr>
				<tr>';
					$jml_hari = 0;
					foreach ($data['jadwal'] as $absen) {
						$i=1;
						foreach ($absen['count_day'] as $key => $tanggal) {
							$jml_hari++;
							$html .='
						 	<th>H'.$i.'<br>('.date_idr($tanggal, 'd/m', null).')</th>';
							$i++;
						}
					}
				$html .= '
				</tr>
  		</thead>
  		<tbody>';
  			$no = 1;
				foreach ($data['absensi'] as $key => $absen) {
					$html .='
					<tr>
						<td>'.$no++.'</td>
						<td>'.$absen['provinsi'].'</td>
						<td>'.$absen['kota'].'</td>
						<td>'.$absen['display_name'].'</td>';
							$jml_hadir = 0;
							foreach ($data['minggu'] as $week) {
								if(count($absen['days_on_week'.$week]) > 0){
									foreach ($absen['days_on_week'.$week] as $key => $val) {
										if($val == 1) {
											$jml_hadir++;
											$html .= '<td>1</td>';
										}else{
											$html .= '<td>0</td>';
										}
									}
								}else{
									 $html .= '<td>0</td>';
								}

							}
						$html .= '
						<td>'.round(($jml_hadir/$jml_hari)*100).'%</td>
					</tr>';
				}
  		$html .='
  		</tbody>
  	</table>';

  	if($page == 'download'){
	  	$save = '';
	    $save .= "
	    <html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
	    xmlns=\"http://www.w3.org/TR/REC-html40\">

	    <head>
	        <meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
	        <meta name=ProgId content=Excel.Sheet>
	        <meta name=Generator content=\"Microsoft Excel 11\">
	        <title>Data Absen</title>
	    </head>
	    <body>";

	    $save .= $html;
	    $save .= "
	        </body>
	        </html>";

	    header("Content-Disposition: attachment; filename=\"".$nama_file.".xls\"");
	    header("Content-Type: application/vnd.ms-excel");
	    header('Cache-Control: max-age=0');

	    echo $save;
	  }else{
	  	$data['html'] = $html;
	  	$this->load->view('admin/page_print', $data);
	  }
	}

	// --------------------------------------------------------------------------

}
