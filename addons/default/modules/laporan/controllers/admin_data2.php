<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_data extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'data';

  public function __construct()
  {
    parent::__construct();
    
    date_default_timezone_set('Asia/Jakarta');
		// -------------------------------------
		// Check permission
		// -------------------------------------
	
		if(! group_has_role('laporan', 'access_laporan_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper('laporan');
		$this->load->library('organization/organization');
		
    $this->lang->load('laporan');		
		$this->load->model('data_m');
		$this->load->model('tipe_m');
		$this->load->model('metadata_m');
		$this->load->model('absen_m');
		$this->load->model('komoditas_m');

		$this->lang->load('location/location');	
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');

		// Organization
		$this->load->model('organization/types_m');
    $this->load->model('organization/units_m');
    $this->load->model('organization/memberships_m');

		$this->config->load('organization/organization');
    $this->node_type_slug = $this->config->item('node_type');
    $this->get_type = $this->types_m->get_type_by_slug($this->node_type_slug);
    $this->node_type_level = $this->get_type->type_level;
    $this->node_type_slug = $this->get_type->type_slug;

    $this->unit = $this->memberships_m->get_one_unit_by_member($this->current_user->id);
  }

  /**
	 * List all data
   *
   * @return	void
   */


  public function index($id=0)
  {
  	ini_set('max_execution_time', '3600');
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan') AND ! group_has_role('laporan', 'view_own_unit_laporan') AND ! group_has_role('laporan','view_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		$data['tipes'] = $this->tipe_m->get_tipe();

		$data['unit_types'] = $this->types_m->get_all_types();
		$memberships_user = $this->memberships_m->get_memberships_by_user($this->current_user->id);
		$type_ids=array();
		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}
		$data['type_ids'] = $type_ids;
		$data['skip_group'] = array('admin','site_admin');

		if($this->input->get('f-tipe_laporan')){

			// Set Tipe Laporan
			$f_tipe_laporan = $this->input->get('f-tipe_laporan');

			$id_tipe_laporan = $this->input->get('f-tipe_laporan');
			// untuk yang harian (TTIC, Gapoktan 2017, TTI 2017)
			$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;
			if($id_tipe_laporan == 3 || $id_tipe_laporan == 4){
				$id_tipe_laporan = 1;
			}
			if($id_tipe_laporan == 5){
				$id_tipe_laporan = 2;
			}

			$hari = ($this->input->get('f-hari')) ? $this->input->get('f-hari') : date('Y-m-d');
			$weeks = getWeeks($hari, 'monday');

			if($this->input->get('f-bln') && $this->input->get('f-thn')){
				$thn_bln = $this->input->get('f-thn')."-".$this->input->get('f-bln');
			}else{
				$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
				$thn_bln = $weeks['tahun'].'-'.$bln;
			}

			// Get Periode
			$date = date($thn_bln.'-01');
			if($this->input->get('f-thn') != '' && $this->input->get('f-bln') != ''){
				$date = $this->input->get('f-thn').'-'.$this->input->get('f-bln').'-01';
			}

			$end_week = get_count_of_week($date);
			$data['end_week'] = $end_week;

	  	$minggu_ke = (string) $weeks['week'];
			if($this->input->get('f-minggu_ke')){
				$minggu_ke = $this->input->get('f-minggu_ke');
			}
			$data['minggu_ke'] = $minggu_ke;

			$ex_thn_bln = explode('-',$thn_bln);
			$data['thn'] = $ex_thn_bln[0];
			$data['bln'] = $ex_thn_bln[1];

			if($f_tipe_laporan > 2){
				$data['weeks'] = $weeks;
				$firstDayOfWeek = $this->data_m->get_hari($data['thn'], $data['bln'], $minggu_ke);
				$data['count_day'] = array();
				if($firstDayOfWeek){
					$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
					$firstDay = $data['weeks']['first_day_of_week'];
					for ($h=0; $h <=6 ; $h++) { 
						$data['count_day'][] = date('Y-m-d', strtotime('+'.$h.' days', strtotime($firstDay)));
					}
				}
				$data['days'] = $data['count_day'];
			}else{
				$firstDayOfWeek = $this->absen_m->get_first_day_of_week($minggu_ke, $thn_bln);
				$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
				$data['count_day'] = $this->absen_m->get_count_day_by_week($minggu_ke, $thn_bln);
				$data['days'] = $this->absen_m->get_days_by_week($minggu_ke, $thn_bln);
			}

			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bln') != '' || $this->input->get('f-thn') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bln') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bln')];
					}
					if($this->input->get('f-thn') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-thn');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$data['minggu_ke'];
				$periode[] = 'Bulan '.$arr_month[$data['bln']];
				$periode[] = 'Tahun '.$data['thn'];
			}

			if(count($periode) > 0){
				$data['text_periode'] = 'Periode: '.implode(', ',$periode);
			}

			// Data Komoditas
			$data['komoditas_related'] = $this->komoditas_m->get_komoditas_related();

			// ---------------------------------------------------------------------------------

			// sandingkan periode

			$hari2 = ($this->input->get('f-hari2')) ? $this->input->get('f-hari2') : date('Y-m-d');
			$weeks2 = getWeeks($hari2, 'monday');
			
			if($this->input->get('f-bln2') && $this->input->get('f-thn2')){
				$thn_bln2 = $this->input->get('f-thn2')."-".$this->input->get('f-bln2');
			}else{
				$bln2 = (strlen($weeks2['bulan']) == 1) ? '0'.$weeks2['bulan'] : $weeks2['bulan'];
				$thn_bln2 = $weeks2['tahun'].'-'.$bln2;
			}

			// Get Periode
			$date = date($thn_bln2.'-01');
			if($this->input->get('f-thn2') != '' && $this->input->get('f-bln2') != ''){
				$date = $this->input->get('f-thn2').'-'.$this->input->get('f-bln2').'-01';
			}

			$end_week2 = get_count_of_week($date);
			$data['end_week2'] = $end_week2;

	  	$minggu_ke2 = (string) $weeks2['week'];
			if($this->input->get('f-minggu_ke2')){
				$minggu_ke2 = $this->input->get('f-minggu_ke2');
			}
			$data['minggu_ke2'] = $minggu_ke2;

			$ex_thn_bln2 = explode('-',$thn_bln2);
			$data['thn2'] = $ex_thn_bln2[0];
			$data['bln2'] = $ex_thn_bln2[1];

			$firstDayOfWeek2 = $this->absen_m->get_first_day_of_week($minggu_ke2, $thn_bln2);

			if($f_tipe_laporan > 2){
				$data['weeks2'] = $weeks2;
				$firstDay2 = $data['weeks2']['first_day_of_week'];
				$lastDay2 = $data['weeks2']['last_day_of_week'];
				for ($h=0; $h <=6 ; $h++) { 
					$data['count_day2'][] = date('Y-m-d', strtotime('+'.$h.' days', strtotime($firstDay2)));
				}
				$data['days2'] = $data['count_day2'];
			}else{
				$firstDayOfWeek2 = $this->absen_m->get_first_day_of_week($minggu_ke2, $thn_bln2);
				$data['weeks2'] = getWeeks($firstDayOfWeek2, 'monday');
				$data['count_day2'] = $this->absen_m->get_count_day_by_week($minggu_ke2, $thn_bln2);
				$data['days2'] = $this->absen_m->get_days_by_week($minggu_ke2, $thn_bln2);
			}

			// $data['weeks2'] = getWeeks($firstDayOfWeek2, 'monday');
			// $data['count_day2'] = $this->absen_m->get_count_day_by_week($minggu_ke2, $thn_bln2);
			// $data['days2'] = $this->absen_m->get_days_by_week($minggu_ke2, $thn_bln2);


			$periode = array();
			
			if($this->input->get('f-minggu_ke2') != '' || $this->input->get('f-bln2') != '' || $this->input->get('f-thn2') != '' || $this->input->get('f-hari2')){

				if($this->input->get('f-hari2') != ''){
					$periode[] = date_idr($this->input->get('f-hari2'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke2') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke2');
					}
					if($this->input->get('f-bln2') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bln2')];
					}
					if($this->input->get('f-thn2') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-thn2');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$data['minggu_ke2'];
				$periode[] = 'Bulan '.$arr_month[$data['bln2']];
				$periode[] = 'Tahun '.$data['thn2'];
			}

			if(count($periode) > 0){
				$data['text_periode2'] = 'Periode 2: '.implode(', ',$periode);
			}

			// ---------------------------------------------------------------------------------

			$data['f_tipe_laporan'] = $f_tipe_laporan;
			$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($id_tipe_laporan)['nama_laporan'];

			// -- open sitoni --
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			if(group_has_role('laporan','view_all_laporan')) {
				
				$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

				// Set Location
				$filter_kota = null;
		  	if($this->input->get('f-provinsi') != '') {
		  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
		  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		  	}

	  	}elseif(group_has_role('laporan','view_own_prov_laporan')){
	  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
	  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
	  	}

	  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
	  	// -- close sitoni --

  	
  		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
			$data['komoditas'] = $this->metadata_m->get_metadata_by_tipe_laporan($id_tipe_laporan, $id_komoditas, 'default_laporan_metadata.*', NULL, 'urutan');

			$metadata_harga = array();
			$metadata_volume = array();
			foreach ($data['komoditas'] as $i => $metadata) {
				if($metadata['id_laporan_tipe_field'] == 1){
					$metadata_harga[$metadata['id']] = $metadata['nama'].'_'.$metadata['field'];
				}
				if($metadata['id_laporan_tipe_field'] == 2){
					$metadata_volume[$metadata['id']] = $metadata['nama'].'_'.$metadata['field'];
				}
			}
			$data['metadata_harga'] = $metadata_harga;
			$data['metadata_volume'] = $metadata_volume;

			$data['min_year'] = $this->absen_m->get_min_year(); 
			$data['max_year'] = $this->absen_m->get_max_year();


			$units_id = NULL;
		  if(! group_has_role('laporan', 'view_all_laporan') && !group_has_role('laporan','view_own_prov_laporan')){
        $units_id = user_units2($this->current_user->id);
        $result = $this->units_m->get_child($units_id);
        $units_id = $units_id.$result;
      }

	    if(!$this->input->get('page')){
				$surffix = '';
			  if($_SERVER['QUERY_STRING']){
			    $surffix = '?'.$_SERVER['QUERY_STRING'];
			  }

				// -------------------------------------
				// Pagination
				// -------------------------------------

		    $total_rows = $this->data_m->count_of_rekapitulasi_data(NULL, $data['komoditas'], $minggu_ke, $thn_bln, $units_id, $data['id_provinsi']);
				$pagination_config['base_url'] = base_url(). 'admin/laporan/data/index';
				$pagination_config['uri_segment'] = 5;
				$pagination_config['suffix'] = '/'.($this->uri->segment(6) ? $this->uri->segment(6) : 0) . $surffix;
				$pagination_config['total_rows'] = $total_rows;
				// $pagination_config['per_page'] = 2;
				$pagination_config['per_page'] = Settings::get('records_per_page');;
				$this->pagination->initialize($pagination_config);
				$data['pagination_config'] = $pagination_config;

				$data['uri'] = $this->get_query_string(5);
				
		    // -------------------------------------
				// Get entries
				// -------------------------------------

				$data['data']['entries'] = $this->data_m->rekapitulasi_data($pagination_config, $data['komoditas'], $minggu_ke, $thn_bln, $units_id, 0, $metadata_harga, $metadata_volume, $data['id_provinsi']);
			  $data['data']['total'] = $total_rows;
				$data['data']['pagination'] = $this->pagination->create_links();


				// sandingan entries -------------------------------------

				if($this->input->get('f-is_sandingan')){
					$total_rows2 = $this->data_m->count_of_rekapitulasi_data(NULL, $data['komoditas'], $minggu_ke2, $thn_bln2, $units_id, $data['id_provinsi'], 1);
					$pagination_config2['base_url'] = base_url(). 'admin/laporan/data/index/'.($this->uri->segment(5) ? $this->uri->segment(5) : 0);
					$pagination_config2['uri_segment'] = 6;
					$pagination_config2['suffix'] = $surffix;
					$pagination_config2['total_rows'] = $total_rows2;
					// $pagination_config['per_page'] = 2;
					$pagination_config2['per_page'] = Settings::get('records_per_page');;
					$this->pagination->initialize($pagination_config2);
					$data['pagination_config2'] = $pagination_config2;

					// $data['uri'] = $this->get_query_string(5);
					
			    // -------------------------------------
					// Get entries
					// -------------------------------------

					$data['data2']['entries'] = $this->data_m->rekapitulasi_data($pagination_config, $data['komoditas'], $minggu_ke2, $thn_bln2, $units_id, 0, $metadata_harga, $metadata_volume, $data['id_provinsi'], 1);
				  $data['data2']['total'] = $total_rows2;
					$data['data2']['pagination'] = $this->pagination->create_links();
				}


				// -------------------------------------
        // Get Organization by Member
        // -------------------------------------

				$data['types'] = $this->types_m->get_all_types();

        $unit_id = $this->unit['id'];
        
        $types = $data['types'];
        $organization = array();
        $addedLevel = array();
        $n2 = 0;
        $n = count($types);

        foreach($types as $i => $type){
          if (in_array($type['level'], $addedLevel)) {
            continue;
          }
          $addedLevel[] = $type['level'];
          $units = $this->units_m->get_unit_by_child($unit_id);

          if(isset($units['id'])){
            if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit_'.$units['type_level']] = $units['id'];
              $n2++;
            } else if ($units['type_slug'] == $this->node_type_slug) {
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit'] = $units['id'];
            }
            $unit_id = $units['id'];
          }
        }

        if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
          if($this->unit['type_slug'] == $this->node_type_slug){
            $organization['id_organization_unit'] = $this->unit['id'];
          }else{
            $organization['id_organization_unit_'.$n2] = $this->unit['id'];
          }
          $organization['organization_name_'.$n2] = $this->unit['name'];
        }

        // khusus sitoni
        $data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
        //---------------------------------
        $data['user_unit_level'] = $this->unit['type_level'];
        $data['node_type_level'] = $this->node_type_level;
        $data['node_type_slug'] = $this->node_type_slug;
        $data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

        $get_units = $this->memberships_m->get_units_by_member($this->current_user->id);
	      if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
		      unset($organization['id_organization_unit']);
		      unset($organization['organization_name_1']);
		    }

        $data['organization'] = $organization;
				if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
	      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
	      }

				// -------------------------------------
		    // Build the page. See views/admin/index.php
		    // for the view code.
				// -------------------------------------
				
		  	$this->template->title(lang('laporan:data:plural'))
					->set_breadcrumb('Dasbor', '/admin')
					->set_breadcrumb(lang('laporan:data:plural'))
					->build('admin/data_index', $data);
			
			}else{
				$data['entries'] = $this->data_m->rekapitulasi_data(NULL, $data['komoditas'], $minggu_ke, $thn_bln, $units_id, 1, $metadata_harga, $metadata_volume, $data['id_provinsi']);

				if($this->input->get('f-is_sandingan')){
					$data['entries2'] = $this->data_m->rekapitulasi_data(NULL, $data['komoditas'], $minggu_ke2, $thn_bln2, $units_id, 1, $metadata_harga, $metadata_volume, $data['id_provinsi'], 1);
				}
				$this->download_laporan_harga($data, $this->input->get('page'));
			}

		}else{

			// -------------------------------------
	    // Build the page. See views/admin/index.php
	    // for the view code.
			// -------------------------------------
			
	    $this->template->title(lang('laporan:data:plural'))
				->set_breadcrumb('Dasbor', '/admin')
				->set_breadcrumb(lang('laporan:data:plural'))
				->build('admin/data_index', $data);
		}
  }
	
	/**
   * Display one data
   *
   * @return  void
   */
  public function view($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_data') AND ! group_has_role('laporan', 'view_own_data')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
    $data['data'] = $this->data_m->get_data_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_data')){
			if($data['data']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:data:view'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:data:plural'), '/admin/laporan/data/index')
			->set_breadcrumb(lang('laporan:data:view'))
			->build('admin/data_entry', $data);
  }
	
	/**
   * Create a new data entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
  public function create()
  {

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'create_laporan') && ! group_has_role('laporan', 'create_own_laporan') && ! group_has_role('laporan', 'create_own_unit_laporan') && ! group_has_role('laporan', 'create_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		$data['uri'] = $this->get_query_string(5);

		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_data('new')){	
				$this->session->set_flashdata('success', lang('laporan:data:submit_success'));
				if(group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_laporan') || group_has_role('laporan','view_own_prov_laporan')){
					$redirect = 'admin/laporan/data/index'.$data['uri'];			
				}else{		
					$redirect = 'admin/laporan/data/create'.$data['uri'];	
				}
				redirect($redirect);
			}else{
				$data['messages']['error'] = lang('laporan:data:submit_failure');
			}
		}


		$data['tipes'] = $this->tipe_m->get_tipe();

		$data['unit_types'] = $this->types_m->get_all_types();
		$memberships_user = $this->memberships_m->get_memberships_by_user($this->current_user->id);
		$type_ids=array();

		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}
		$data['type_ids'] = $type_ids;

		
		if($this->input->get('f-tipe_laporan')){
			
			// Cek Tanggal Pelaporan
			$today = date('Y-m-d H:i:s');
			$data['absen'] = $this->absen_m->cek_tgl_input($today, $this->input->get('f-tipe_laporan'));

			$dates = $this->absen_m->get_dates();
			$data['addDates'] = (count($dates) > 0) ? implode(',', $dates) : '';

			// Data Komoditas
			$data['komoditas'] = $this->komoditas_m->get_komoditas_related();

			// Tampung data GET
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			$data['id_unit'] = ($this->input->get('f-id_unit')) ? $this->input->get('f-id_unit') : NULL;

			if(count($data['absen']) > 0 || $this->input->get('f-tipe_laporan') > 2) {
				$tipe_laporan = $this->input->get('f-tipe_laporan');
				// untuk yang harian (TTIC, Gapoktan 2017, TTI 2017)
				if($tipe_laporan == 3 || $tipe_laporan == 4){
					$tipe_laporan = 1;
				}
				if($tipe_laporan == 5){
					$tipe_laporan = 2;
				}
				$data['tipe_laporan'] = $tipe_laporan;
				$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($tipe_laporan)['nama_laporan'];

				if(group_has_role('laporan','create_laporan')) {
					
					// Load location
					$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
					
					$filter_kota = NULL;
			  	if($this->input->post('provinsi') != ''){
			  		$data['id_provinsi'] = $this->input->post('provinsi');
			  	}

		  		if($this->input->post('kota') != ''){
			  		$data['id_kota'] = $this->input->post('kota');
			  	}

			  }elseif(group_has_role('laporan','create_own_prov_laporan')){
		  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
		  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
		  	}

		  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
	  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);

	  		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
	  		if($this->input->post('id_laporan_komoditas')){
	  			$id_komoditas = $this->input->post('id_laporan_komoditas');
	  		}
			  $data['metadata'] = $this->metadata_m->get_metadata_by_tipe_laporan($tipe_laporan, $id_komoditas, 'default_laporan_metadata.*',NULL,'urutan');

				$data['mode'] = 'new';
				$data['return'] = 'admin/laporan/data/index'.$data['uri'];
			}

			// -------------------------------------
      // Get Organization by Member
      // -------------------------------------

      $data['types'] = $this->types_m->get_all_types();

      $unit_id = $this->unit['id'];
      
      $types = $data['types'];
      $organization = array();
      $addedLevel = array();
      $n2 = 0;
      $n = count($types);

      foreach($types as $i => $type){
        if (in_array($type['level'], $addedLevel)) {
          continue;
        }
        $addedLevel[] = $type['level'];
        $units = $this->units_m->get_unit_by_child($unit_id);

        if(isset($units['id'])){
          if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
            $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
            $organization['id_organization_unit_'.$units['type_level']] = $units['id'];
            $n2++;
          } else if ($units['type_slug'] == $this->node_type_slug) {
            $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
            $organization['id_organization_unit'] = $units['id'];
          }
          $unit_id = $units['id'];
        }
      }

      if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){    
        if($this->unit['type_slug'] == $this->node_type_slug){
        	$organization['id_organization_unit'] = $this->unit['id'];
        }else{
          $organization['id_organization_unit_'.$n2] = $this->unit['id'];
        }
        $organization['organization_name_'.$n2] = $this->unit['name'];
      }

      $get_units = $this->memberships_m->get_units_by_member($this->current_user->id);
      if(count($get_units) > 1 && ($this->input->get('f-tipe_laporan') == 2 || $this->input->get('f-tipe_laporan') == 3)){
	      unset($organization['id_organization_unit']);
	      unset($organization['organization_name_1']);
	    }

      // khusus sitoni
      $data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
      //---------------------------------

      $data['node_type_slug'] = $this->node_type_slug;
      $data['organization'] = $organization;
      
      $group_id = $this->session->userdata('group_id');
      $data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0, $data['id_provinsi'], $data['id_kota']) : $this->units_m->get_units_by_parent($this->unit['id'], 1, $group_id);

      if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1, $group_id);
      }
		}

		$data['title'] = lang('laporan:data:new');
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title($data['title'])
			->append_js('module::jquery-ui.multidatespicker.js')
    	->append_css('module::mdp.css')
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb($data['title'])
			->build('admin/data_form', $data);
  }
	
	/**
   * Edit a data entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the data to the be deleted.
   * @return	void
   */
  public function edit($id_unit = 0, $id_user = 0, $id_absen = 0, $id_laporan_tipe)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'edit_all_laporan') AND ! group_has_role('laporan', 'edit_own_laporan') AND ! group_has_role('laporan','edit_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		$data['uri'] = $this->get_query_string(9);

		if($_POST){
			if($this->_update_data('edit')){	
				$this->session->set_flashdata('success', lang('laporan:data:submit_success'));				
				redirect('admin/laporan/data/daftar_laporan'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('laporan:data:submit_failure');
			}
		}
		
		$data['metadata'] = $this->data_m->get_input_laporan_by_id($id_unit, $id_user, $id_absen, $id_laporan_tipe);
		// dump($data['metadata']);
		// die();
		$data['mode'] = 'edit';
		$data['return'] = 'admin/laporan/data/daftar_laporan'.$data['uri'];

		$data['id_unit'] = $id_unit;
		$data['id_user'] = $id_user;
		$data['id_absen'] = $id_absen;
		$data['id_laporan_tipe'] = $id_laporan_tipe;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:daftar_laporan'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:daftar_laporan'), '/admin/laporan/data/daftar_laporan')
			->set_breadcrumb(lang('laporan:data:edit'))
			->build('admin/edit_laporan', $data);
  }
	
	/**
   * Delete a data entry
   * 
   * @param   int [$id] The id of data to be deleted
   * @return  void
   */
  public function delete($id_unit, $id_user, $id_absen, $id_laporan_tipe)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		// if(! group_has_role('laporan', 'delete_all_data') AND ! group_has_role('laporan', 'delete_own_data')){
		// 	$this->session->set_flashdata('error', lang('cp:access_denied'));
		// 	redirect('admin');
		// }
		// Check view all/own permission
		// if(! group_has_role('laporan', 'delete_all_data')){
		// 	$entry = $this->data_m->get_data_by_id($id);
		// 	$created_by_user_id = $entry['created_by'];
		// 	if($created_by_user_id != $this->current_user->id){
		// 		$this->session->set_flashdata('error', lang('cp:access_denied'));
		// 		redirect('admin');
		// 	}
		// }
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		$data['uri'] = $this->get_query_string(9);
    $this->data_m->delete_data_input_by_user($id_user, $id_unit, $id_absen, $id_laporan_tipe);
    $this->session->set_flashdata('error', lang('laporan:data:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
    redirect('admin/laporan/data/daftar_laporan'.$data['uri']);
  }
	
	/**
   * Insert or update data entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_data($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));

		if($method == 'new'){
			
			$values = $this->input->post();
			if($values['tipe_laporan'] == 3){
				$nama_laporan = $this->tipe_m->get_tipe_by_id(4)['nama_laporan'];
			}else{
				$nama_laporan = $this->tipe_m->get_tipe_by_id($values['tipe_laporan'])['nama_laporan'];
			}
			$val_tipe_laporan = $values['tipe_laporan'];
			$tipe_laporan = $this->input->post('tipe_laporan');

			$minggu_ke = NULL;
			if($tipe_laporan > 2){
				$weeks = getWeeks(date('Y-m-d'), 'monday');
				$minggu_ke = $weeks['week'];
			}
			// untuk yang harian (TTIC, Gapoktan 2017, TTI 2017)
			$is_ttic = ($tipe_laporan == 3) ? 1 : 0;
			if($tipe_laporan == 3 || $tipe_laporan == 4){
				$tipe_laporan = 1;
			}
			if($tipe_laporan == 5){
				$tipe_laporan = 2;
			}
			$metadata =$this->metadata_m->get_metadata_by_tipe_laporan($tipe_laporan, $values['id_laporan_komoditas'], 'default_laporan_metadata.*',NULL,'urutan');

			if(group_has_role('laporan','create_laporan')){
				$this->form_validation->set_rules('provinsi', lang('location:provinsi:plural'), 'required');
				$this->form_validation->set_rules('kota', lang('location:kota:plural'), 'required');
			}
			if($values['id_organization_unit'] < 0){
				$this->form_validation->set_rules('id_organization_unit2', $nama_laporan, 'required');
			}

		}else{

			$id_absen = $this->input->post('id_absen');
			$id_unit = $this->input->post('id_unit');
			$id_user = $this->input->post('id_user');
			$id_laporan_tipe = $this->input->post('id_laporan_tipe');
			$metadata = $this->data_m->get_input_laporan_by_id($id_unit, $id_user, $id_absen, $id_laporan_tipe);
		}


		foreach ($metadata as $key=> $data){
			$nilai_min = ($data['nilai_minimal'] != "") ? $data['nilai_minimal'] : 0;
			$nilai_max = ($data['nilai_maksimal'] != "") ? $data['nilai_maksimal'] : 1000000000;
			$this->form_validation->set_rules($data['field'], $data['nama'], 'required|is_natural|callback_check_equal_greater['.$nilai_min.']|callback_check_equal_less['.$nilai_max.']');
		}
	
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$cek_input = $this->data_m->get_data_input_by_user($this->current_user->id, $values['id_organization_unit'], $values['id_absen'], $tipe_laporan, $values['id_laporan_komoditas'], $val_tipe_laporan, $is_ttic);
				if(count($cek_input) > 0){
					$this->data_m->delete_data_input_by_user($this->current_user->id, $values['id_organization_unit'], $values['id_absen'], $tipe_laporan, $values['id_laporan_komoditas'], $val_tipe_laporan, $is_ttic);
				}

				foreach ($metadata as $key => $data) {
					$data_insert['value'] = $this->input->post($data['field']);
					$data_insert['channel'] = 'web';
					$data_insert['id_metadata'] = $data['id'];
					if($val_tipe_laporan < 3){
						$data_insert['id_absen'] = $values['id_absen'];
					}else{
						$data_insert['minggu_ke'] = $minggu_ke;
					}
					$data_insert['id_user'] = $values['id_user'];
					$data_insert['id_unit'] = $values['id_organization_unit'];
					$data_insert['id_group'] = ($val_tipe_laporan < 3) ? 7 : 9;
					$data_insert['is_ttic'] = $is_ttic;

					$this->data_m->insert_data($data_insert);
				}
				$result = true;
			}
			else
			{
				$where['id_absen'] = $id_absen;
				$where['id_user'] = $id_user;
				$where['id_unit'] = $id_unit;
				foreach ($metadata as $key => $data) {
					if($this->input->post($data['field']) != $data['value']){
						$where['id_metadata'] = $data['id'];
						$values['value'] = $this->input->post($data['field']);
						$this->data_m->update_data($values, $where);
					}
				}
				$result = true;
			}
		}
		
		return $result;
	}

	public function ajax_get_minggu_ke($year = null, $month = null){
    if($year != NULL && $month != NULL){
			$date = $year.'-'.$month.'-01';
			$end_week = get_count_of_week($date);
			$romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
			// echo '<option value="">-- '.lang('laporan:minggu_ke').' --</option>';
			for ($i=1;$i<=$end_week;$i++) {
				echo '<option value="'.$i.'">'.$romawi[$i].'</option>';
			}
		}else{
			echo '<option value="">'.lang('global:select-none').'</option>';
		}
	}

	public function ajax_get_unit_by_kota($kota, $unit_type){
		$content = $this->organization->get_units_by_kota($kota, $unit_type);
		echo $content;
	}

	public function ajax_get_tti_by($kota, $unit_type){
		$content = $this->organization->get_units_by_kota($kota, $unit_type);
		echo $content;
	}

	public function check_equal_less($value,$max_value)
  {
    if ($value > $max_value)
    {
      $this->form_validation->set_message('check_equal_less', 'Nilai maksimal <b>%s</b> adalah '.$max_value);
      return false;       
    }
    else
    {
      return true;
    }
  }
	
	public function check_equal_greater($value,$min_value)
  {
    if ($value < $min_value)
    {
      $this->form_validation->set_message('check_equal_greater', 'Nilai minimal <b>%s</b> adalah '.$min_value);
      return false;       
    }
    else
    {
      return true;
    }
  }

	public function get_query_string($page){
		// -------------------------------------
		// Get query string
		// -------------------------------------

		$page = $this->uri->segment($page);
		$uri = '';
		$other_post = '';
		if($this->input->post('id_laporan_komoditas')){
			$other_post .= '&f-komoditas='.$this->input->post('id_laporan_komoditas');
		}
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'] . $other_post;
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'] . $other_post;
    }
    
    return $uri;
	}

	public function insert_data_dummy(){
		$id_user = 115;
		$nama_enum = "Pedagang Eceran";
		$query = $this->db->query("select * from default_laporan_absen where date_format(tanggal, '%Y') = '2015'");
		$absens = $query->result_array();
		$metadata =$this->metadata_m->get_metadata_by_enum($nama_enum);
		foreach($absens as $absen){
			$id_absen = $absen['id'];
			$tgl = $absen['tanggal']." 00:00:00";
			$value = intval(rand(1,9)."000");

			$cek_absen = $this->data_m->get_data_by_absen($id_user, $id_absen, $nama_enum);
			if(count($cek_absen) > 0){
				$this->data_m->delete_data_by_absen($id_user, $id_absen, $nama_enum);
			}
			foreach ($metadata as $key => $data) {
				$id_metadata = $data['id'];
				$query_insert = "INSERT INTO `default_laporan_data` (`value`, `channel`, `id_metadata`, `id_absen`, `id_user`, `created_on`) VALUES ('$value', 'web', '$id_metadata', '$id_absen', '$id_user', '$tgl')";

				$this->db->query($query_insert);
			}
		}
	}

	public function daftar_laporan()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan') AND ! group_has_role('laporan','view_own_unit_laporan') AND ! group_has_role('laporan','view_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['tipes'] = $this->tipe_m->get_tipe();
		$data['komoditas_related'] = $this->komoditas_m->get_komoditas_related();

		$data['unit_types'] = $this->types_m->get_all_types();
		$memberships_user = $this->memberships_m->get_memberships_by_user($this->current_user->id);
		$type_ids=array();
		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}
		$data['type_ids'] = $type_ids;
		$data['skip_group'] = array('admin','site_admin');

		if($this->input->get('f-tipe_laporan')){


			// Set Tipe Laporan
			$tipe_laporan = $this->input->get('f-tipe_laporan');
			$data['tipe_laporan'] = $tipe_laporan;
			$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($tipe_laporan)['nama_laporan'];
			
			// Get Periode
			$weeks = getWeeks(date('Y-m-d'), 'monday');

			if($this->input->get('f-bln') && $this->input->get('f-thn')){
				$thn_bln = $this->input->get('f-thn')."-".$this->input->get('f-bln');
			}else{
				$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
				$thn_bln = $weeks['tahun'].'-'.$bln;
			}

			$date = date($thn_bln.'-01');
			if($this->input->get('f-thn') != '' && $this->input->get('f-bln') != ''){
				$date = $this->input->get('f-thn').'-'.$this->input->get('f-bln').'-01';
			}

			$end_week = get_count_of_week($date);
			$data['end_week'] = $end_week;

	  	$minggu_ke = (string) $weeks['week'];
			if($this->input->get('f-minggu_ke')){
				$minggu_ke = $this->input->get('f-minggu_ke');
			}
			$data['minggu_ke'] = $minggu_ke;

			$ex_thn_bln = explode('-',$thn_bln);
			$data['thn'] = $ex_thn_bln[0];
			$data['bln'] = $ex_thn_bln[1];

			$firstDayOfWeek = $this->absen_m->get_first_day_of_week($minggu_ke, $thn_bln);
			$data['firstDayOfWeek'] = $firstDayOfWeek;
			$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
			$data['min_year'] = $this->absen_m->get_min_year(); 
			$data['max_year'] = $this->absen_m->get_max_year();

			$data['count_day'] = $this->absen_m->get_count_day_by_week($minggu_ke, $thn_bln);
			$data['days'] = $this->absen_m->get_days_by_week($minggu_ke, $thn_bln);
			
			if(!$this->input->get('f-hari')){
				$absen = $this->absen_m->get_days_by_week($minggu_ke, $thn_bln);
				foreach ($absen as $key => $absen) {
					$tanggal_awal = $absen['tanggal_awal'];
					$tanggal_akhir = $absen['tanggal_akhir'];
				}
			}else{
				$absen = $this->absen_m->get_absen_by_date($this->input->get('f-hari'), $tipe_laporan);
				$tanggal_awal = date("Y-m-d", strtotime($absen['waktu_buka']));
				$tanggal_akhir = date("Y-m-d", strtotime($absen['waktu_tutup']));
			}
			// ---------------------------------------------------------------------------------

			$today = date('Y-m-d H:i:s');
			$data['id_absen'] = NULL;
			$cek_absen = $this->absen_m->get_absen_by_date($today, $tipe_laporan);
			if(count($absen) > 1){
				$data['id_absen'] = $cek_absen['id'];
			}

			// -- open sitoni --
			
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			if(group_has_role('laporan','view_all_laporan')) {
				
				$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

				// Set Location
				$filter_kota = null;
		  	if($this->input->get('f-provinsi') != '') {
		  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
		  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		  	}

	  	}elseif(group_has_role('laporan','view_own_prov_laporan')){
	  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
	  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
	  	}

	  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);

  		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));

	  	// -- close sitoni --


			$units_id = NULL;
		  if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('Laporan','view_own_prov_laporan')){
        $units_id = user_units2($this->current_user->id);
        $result = $this->units_m->get_child($units_id);
        $units_id = $units_id.$result;
      }

	    if(!$this->input->get('page')){

		    if($firstDayOfWeek != NULL) {


		    	$surffix = '';
			    if($_SERVER['QUERY_STRING']){
			      $surffix = '?'.$_SERVER['QUERY_STRING'];
			    }

					// -------------------------------------
					// Pagination
					// -------------------------------------

			  	$count_input = $this->data_m->get_input_laporan(NULL, 1, $tanggal_awal, $tanggal_akhir, $units_id, $data['id_provinsi'], $id_komoditas);
					$pagination_config['base_url'] = base_url(). 'admin/laporan/data/daftar_laporan';
					$pagination_config['uri_segment'] = 5;
					$pagination_config['suffix'] = $surffix;
					$pagination_config['total_rows'] = $count_input;
					// $pagination_config['per_page'] = 2;
					$pagination_config['per_page'] = Settings::get('records_per_page');
					$this->pagination->initialize($pagination_config);
					$data['pagination_config'] = $pagination_config;
					
			    // -------------------------------------
					// Get entries
					// -------------------------------------
					// 
					
					
			    $data['laporan_data']['entries'] = $this->data_m->get_input_laporan($pagination_config, 0, $tanggal_awal, $tanggal_akhir, $units_id, $data['id_provinsi'], $id_komoditas);
					$data['laporan_data']['total'] = $count_input;
					$data['laporan_data']['pagination'] = $this->pagination->create_links();

					$data['uri'] = $this->get_query_string(5);
				}

				// -------------------------------------
        // Get Organization by Member
        // -------------------------------------

				$data['types'] = $this->types_m->get_all_types();

        $unit_id = $this->unit['id'];
        
        $types = $data['types'];
        $organization = array();
        $addedLevel = array();
        $n2 = 0;
        $n = count($types);

        foreach($types as $i => $type){
          if (in_array($type['level'], $addedLevel)) {
            continue;
          }
          $addedLevel[] = $type['level'];
          $units = $this->units_m->get_unit_by_child($unit_id);

          if(isset($units['id'])){
            if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit_'.$units['type_level']] = $units['id'];
              $n2++;
            } else if ($units['type_slug'] == $this->node_type_slug) {
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit'] = $units['id'];
            }
            $unit_id = $units['id'];
          }
        }

        if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
          if($this->unit['type_slug'] == $this->node_type_slug){
            $organization['id_organization_unit'] = $this->unit['id'];
          }else{
            $organization['id_organization_unit_'.$n2] = $this->unit['id'];
          }
          $organization['organization_name_'.$n2] = $this->unit['name'];
        }

        // khusus sitoni
        $data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
        //---------------------------------
        $data['user_unit_level'] = $this->unit['type_level'];
        $data['node_type_level'] = $this->node_type_level;
        $data['node_type_slug'] = $this->node_type_slug;
        $data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

        $get_units = $this->memberships_m->get_units_by_member($this->current_user->id);
	      if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
		      unset($organization['id_organization_unit']);
		      unset($organization['organization_name_1']);
		    }

        $data['organization'] = $organization;
				if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
	      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
	      }

				// -------------------------------------
		    // Build the page. See views/admin/index.php
		    // for the view code.
				// -------------------------------------
			
		    $this->template->title(lang('laporan:daftar_laporan'))
					->set_breadcrumb('Dasbor', '/admin')
					->set_breadcrumb(lang('laporan:daftar_laporan'))
					->build('admin/laporan_input', $data);
			}else{
				$data['laporan_data']['entries'] = $this->data_m->get_input_laporan(NULL, 0, $tanggal_awal, $tanggal_akhir, $units_id);
				$this->download_input_laporan($data, $this->input->get('page'));
			}
		}else{

			// -------------------------------------
	    // Build the page. See views/admin/index.php
	    // for the view code.
			// -------------------------------------
			
	    $this->template->title(lang('laporan:daftar_laporan'))
				->set_breadcrumb('Dasbor', '/admin')
				->set_breadcrumb(lang('laporan:daftar_laporan'))
				->build('admin/laporan_input', $data);
		}
  }

  public function download_laporan_harga($data, $page){
  	$this->load->library('excel');
  	$this->load->helper('laporan');

  	if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$jml_komoditas = count($data['komoditas']);

		$minggu = array();

		$col = 4;
		if(!$this->input->get('f-hari')){
			foreach ($data['komoditas'] as $key => $komoditas) {
				$col += count($data['count_day']);
			}
		}else{
			$col += $jml_komoditas;
		}
		$col += count($data['metadata_harga']);
		$col += count($data['metadata_volume']);

		$colspan = $col;
		

  	if($page == 'download'){
  		$rowspan = (!$this->input->get('f-hari') && $this->input->get('f-minggu_ke') ? 'rowspan="3"' : 'rowspan="2"');
			$rowspan2 = (!$this->input->get('f-hari') && $this->input->get('f-minggu_ke') ? 'rowspan="2"' : '');
			$colspan_metadata = (!$this->input->get('f-hari') && $this->input->get('f-minggu_ke')) ? 'colspan="'.count($data['count_day']).'"' : '';

			$arr_cols = range('A','Z');

			$master_hrf = $arr_cols;

			$hitung = count($arr_cols);
			foreach ($arr_cols as $key => $value) {
					
			}

			if(count($arr_cols) < $colspan){
				foreach ($arr_cols as $key => $hrf) {
					foreach ($master_hrf as $key2 => $hrf2) {
						if(count($arr_cols) < $colspan){
							$arr_cols[] = $hrf.''.$hrf2;
						}else{
							break;
						}
					}
				}
			}else{
				$arr_cols = array();
				foreach ($master_hrf as $key2 => $hrf2) {
					if(count($arr_cols) < $colspan){
						$arr_cols[] = $hrf2;
					}else{
						break;
					}
				}
			}

			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			foreach (range('A','D') as $columnID) {
				$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
				// $this->excel->getActiveSheet()->getColumnDimension($columnID)->setWidth(15);
			}
			
			$total_cell = count($arr_cols);

	  	$nama_laporan = $data['nama_laporan'];

	  	if($this->input->get('id_organization_unit') > 0){
				$id_unit = $this->input->get('id_organization_unit');
				$nama_unit = $this->units_m->get_units_by_id($id_unit)->unit_name;
				$nama_laporan .= ' "'.$nama_unit.'"';
			}

			$nama_file = 'Data_Harga_'.$nama_laporan;

			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle('Laporan Panel Harga');

			$this->excel->getActiveSheet()->setCellValue('A1', 'Data Harga '.$nama_laporan);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$cols_header = $arr_cols[0].'1:'.$arr_cols[$total_cell-1].'1';
			$this->excel->getActiveSheet()->mergeCells($cols_header);
			$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$row = 2;

			if($this->input->get('f-provinsi') != ''){
	    	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	    	$kota = "";
	    	if($this->input->get('f-kota') != ''){
	    		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	    	}

	    	$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].$row;
				$this->excel->getActiveSheet()->mergeCells($cols_header);
				$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$row++;
				
				$nama_file .= '_'.$provinsi.$kota;
			}

			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bln') != '' || $this->input->get('f-thn') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bln') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bln')];
					}
					if($this->input->get('f-thn') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-thn');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$data['minggu_ke'];
				$periode[] = 'Bulan '.$arr_month[$data['bln']];
				$periode[] = 'Tahun '.$data['thn'];
			}

			if(count($periode) > 0){
				$implode_periode = implode(', ',$periode);
				$nama_file .= "_".implode('_',$periode);

				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Periode: '.$implode_periode);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].$row;
				$this->excel->getActiveSheet()->mergeCells($cols_header);
				$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$row++;
			}

			$row++;

			$merge_row = (!$this->input->get('f-hari') && $this->input->get('f-minggu_ke') ? '2' : '1');
			$merge_row2 = (!$this->input->get('f-hari') && $this->input->get('f-minggu_ke') ? '1' : '0');

			$this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
			$cols_header = 'A'.$row.':A'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$this->excel->getActiveSheet()->setCellValue('B'.$row, 'Provinsi');
			$cols_header = 'B'.$row.':B'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Kota');
			$cols_header = 'C'.$row.':C'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$this->excel->getActiveSheet()->setCellValue('D'.$row, 'Petugas');
			$cols_header = 'D'.$row.':D'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].($row+$merge_row);
			$this->excel->getActiveSheet()->getStyle($cols_header)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle($cols_header)->applyFromArray(
		    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'CCCCCC')
	        ),
	        'borders' => array(
	          'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	          )
	        ),
		    )
			);

			$next_key = array_search('E',$arr_cols);
			foreach ($data['komoditas'] as $key => $metadata) { 
	      $chr = $arr_cols[$next_key];
	      if(!$this->input->get('f-hari') && $this->input->get('f-minggu_ke')){
	      	$cols = $next_key + (count($data['count_day'])-1);
	      	$chr2 = $arr_cols[$cols];
					$this->excel->getActiveSheet()->mergeCells($chr.$row.':'.$chr2.($row+1));

					$next_key = $cols;
	      }else{
	      	$cols_header = $chr.$row.':'.$chr.($row+1);
					$this->excel->getActiveSheet()->mergeCells($cols_header);
	      }
				$next_key++;
	      $this->excel->getActiveSheet()->setCellValue($chr.$row, $metadata['nama']);
	    }

	    $mas_next_key = $next_key;
	    $chr = $arr_cols[$next_key];
	    $next_key = $next_key + (count($data['metadata_harga'])-1);
	  	$chr2 = $arr_cols[$next_key];
	    $cols_header = $chr.$row.':'.$chr2.$row;
	    $this->excel->getActiveSheet()->mergeCells($cols_header);
	    $this->excel->getActiveSheet()->setCellValue($chr.$row, 'Rata-Rata Harga Satu Tahun Per Enum di Kota/Kab');

	    foreach ($data['metadata_harga'] as $field) { 
	    	$chr = $arr_cols[$mas_next_key];
				if(!$this->input->get('f-hari') && $this->input->get('f-minggu_ke')){
					$this->excel->getActiveSheet()->mergeCells($chr.($row+1).':'.$chr.($row+2));
	      }
	      $explode = explode('_',$field);
	      $nama = $explode[0];
	      $width = strlen('Rata - Rata Harga '.$nama);
				$this->excel->getActiveSheet()->getColumnDimension($chr)->setWidth($width);
	      $this->excel->getActiveSheet()->setCellValue($chr.($row+1), 'Rata - Rata Harga '.$nama);
	      $mas_next_key++;
	    }

	    if(count($data['metadata_volume']) > 0) {
	    	$next_key = $next_key+1;
	    	$chr = $arr_cols[$next_key];
		    $next_key = $next_key + (count($data['metadata_volume'])-1);
		  	$chr2 = $arr_cols[$next_key];
		    $cols_header = $chr.$row.':'.$chr2.$row;
		    $this->excel->getActiveSheet()->mergeCells($cols_header);
		    $this->excel->getActiveSheet()->setCellValue($chr.$row, 'Total Pasokan Satu Tahun Per Enum di Kota/Kab');
	    }

	    foreach ($data['metadata_volume'] as $field) { 
	    	$chr = $arr_cols[$mas_next_key];
				if(!$this->input->get('f-hari') && $this->input->get('f-minggu_ke')){
					$this->excel->getActiveSheet()->mergeCells($chr.($row+1).':'.$chr.($row+2));
	      }
	      $explode = explode('_',$field);
	      $nama = $explode[0];
	      $width = strlen('Total '.$nama);
				$this->excel->getActiveSheet()->getColumnDimension($chr)->setWidth($width);
	      $this->excel->getActiveSheet()->setCellValue($chr.($row+1), 'Total '.$nama);
	      $mas_next_key++;
	    }


	    $next_keydays = array_search('E',$arr_cols);
	    if(!$this->input->get('f-hari') && $this->input->get('f-minggu_ke')){
	    	$row = $row+2;
				foreach ($data['komoditas'] as $key => $metadata) {
					$strlen = strlen($metadata['nama']);
					$width = round($strlen/count($data['count_day']));
					$width = $width+1;
	  			for($d=1;$d<=count($data['count_day']);$d++){
	  				$chr3 = $arr_cols[$next_keydays];
	  				$this->excel->getActiveSheet()->setCellValue($chr3.($row), 'H'.$d);
	    			$this->excel->getActiveSheet()->getColumnDimension($chr3)->setWidth($width);
	  				$next_keydays++;
	  			}
	  		}
			}else{
				$row = $row+1;
			}

			$endchr = $arr_cols[count($arr_cols)-1];
			// $this->excel->getActiveSheet()->getStyle('A1:'.$endchr.$row)->getAlignment()->setWrapText(true); 

			$rowberfore = $row+1;
	  	$no = 1;
	  	// echo $row;
	  	// die();
			foreach ($data['entries'] as $data_entry){
	      $row++;
				$this->excel->getActiveSheet()->setCellValue('A'.$row, $no++);
				$this->excel->getActiveSheet()->setCellValue('B'.$row, $data_entry['provinsi']);
				$this->excel->getActiveSheet()->setCellValue('C'.$row, $data_entry['kota']);
				$this->excel->getActiveSheet()->setCellValue('D'.$row, $data_entry['display_name']);
				$next_keysval = array_search('E',$arr_cols);
		  	foreach ($data['komoditas'] as $key => $metadata) {
		  		$val_metadatas = $data_entry[$metadata['field']];
		  		if(!$this->input->get('f-hari') && $this->input->get('f-minggu_ke')){
			  		foreach($val_metadatas as $val){
							$chr = $arr_cols[$next_keysval];
			  			$this->excel->getActiveSheet()->setCellValue($chr.$row, number_format($val,0,',',''));
			  			$next_keysval++;
	          }
	        }else{
	        	$chr = $arr_cols[$next_keysval];
	        	$this->excel->getActiveSheet()->setCellValue($chr.$row, number_format($val_metadatas,0,',',''));
	        	$next_keysval++;
	        }
	    	}

	      foreach ($data['metadata_harga'] as $field) { 
	      	$chr = $arr_cols[$next_keysval];
	        $rata2tahunan = number_format($data_entry['avgthnan_'.$field],0,',','');
	        $this->excel->getActiveSheet()->setCellValue($chr.$row, ($rata2tahunan > 0 ? $rata2tahunan : '-'));
	        $next_keysval++;
	      }

	      foreach ($data['metadata_volume'] as $field) {
	      	$chr = $arr_cols[$next_keysval];
	        $totaltahunan = number_format($data_entry['sumthnan_'.$field],0,',','');
	        $this->excel->getActiveSheet()->setCellValue($chr.$row, ($totaltahunan > 0 ? $totaltahunan : '-'));
	        $next_keysval++;
	      }

			}

			$cols_header = 'A'.$rowberfore.':'.$endchr.$row;
			$this->excel->getActiveSheet()->getStyle($cols_header)->applyFromArray(
		    array(
	        'borders' => array(
	          'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	          )
	        ),
		    )
			);


			// -------------------------------------Sandingan-------------------------------------

			if($this->input->get('f-is_sandingan')){
				$row++;

				$rowspan = (!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2') ? 'rowspan="3"' : 'rowspan="2"');
				$rowspan2 = (!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2') ? 'rowspan="2"' : '');
				$colspan_metadata = (!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')) ? 'colspan="'.count($data['count_day']).'"' : '';

				$arr_cols = range('A','Z');

				$master_hrf = $arr_cols;

				$hitung = count($arr_cols);
				foreach ($arr_cols as $key => $value) {
						
				}

				if(count($arr_cols) < $colspan){
					foreach ($arr_cols as $key => $hrf) {
						foreach ($master_hrf as $key2 => $hrf2) {
							if(count($arr_cols) < $colspan){
								$arr_cols[] = $hrf.''.$hrf2;
							}else{
								break;
							}
						}
					}
				}else{
					$arr_cols = array();
					foreach ($master_hrf as $key2 => $hrf2) {
						if(count($arr_cols) < $colspan){
							$arr_cols[] = $hrf2;
						}else{
							break;
						}
					}
				}


				$row++;
				$periode = array();
				$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
				
				if($this->input->get('f-minggu_ke2') != '' || $this->input->get('f-bln2') != '' || $this->input->get('f-thn2') != '' || $this->input->get('f-hari2')){

					if($this->input->get('f-hari') != ''){
						$periode[] = date_idr($this->input->get('f-hari2'), 'l, d F Y', null);
					}else{
						if($this->input->get('f-minggu_ke2') != ''){
							$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke2');
						}
						if($this->input->get('f-bln2') != ''){
							$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bln2')];
						}
						if($this->input->get('f-thn2') != ''){
							$periode[] = 'Tahun '.$this->input->get('f-thn2');
						}
					}
				}else{
					$periode[] = ' Minggu Ke '.$data['minggu_ke2'];
					$periode[] = 'Bulan '.$arr_month[$data['bln2']];
					$periode[] = 'Tahun '.$data['thn2'];
				}

				if(count($periode) > 0){
					$implode_periode = implode(', ',$periode);
					$nama_file .= "_".implode('_',$periode);

					$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Periode: '.$implode_periode);
					$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
					$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
					$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].$row;
					$this->excel->getActiveSheet()->mergeCells($cols_header);
					$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$row++;
				}

				$row++;

				$merge_row = (!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2') ? '2' : '1');
				$merge_row2 = (!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2') ? '1' : '0');

				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
				$cols_header = 'A'.$row.':A'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$this->excel->getActiveSheet()->setCellValue('B'.$row, 'Provinsi');
				$cols_header = 'B'.$row.':B'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Kota');
				$cols_header = 'C'.$row.':C'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$this->excel->getActiveSheet()->setCellValue('D'.$row, 'Petugas');
				$cols_header = 'D'.$row.':D'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].($row+$merge_row);
				$this->excel->getActiveSheet()->getStyle($cols_header)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle($cols_header)->applyFromArray(
			    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'CCCCCC')
		        ),
		        'borders' => array(
		          'allborders' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array('rgb' => '000000')
		          )
		        ),
			    )
				);

				$next_key = array_search('E',$arr_cols);
				foreach ($data['komoditas'] as $key => $metadata) { 
		      $chr = $arr_cols[$next_key];
		      if(!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')){
		      	$cols = $next_key + (count($data['count_day2'])-1);
		      	$chr2 = $arr_cols[$cols];
						$this->excel->getActiveSheet()->mergeCells($chr.$row.':'.$chr2.($row+1));

						$next_key = $cols;
		      }else{
		      	$cols_header = $chr.$row.':'.$chr.($row+1);
						$this->excel->getActiveSheet()->mergeCells($cols_header);
		      }
					$next_key++;
		      $this->excel->getActiveSheet()->setCellValue($chr.$row, $metadata['nama']);
		    }

		    $mas_next_key = $next_key;
		    $chr = $arr_cols[$next_key];
		    $next_key = $next_key + (count($data['metadata_harga'])-1);
		  	$chr2 = $arr_cols[$next_key];
		    $cols_header = $chr.$row.':'.$chr2.$row;
		    $this->excel->getActiveSheet()->mergeCells($cols_header);
		    $this->excel->getActiveSheet()->setCellValue($chr.$row, 'Rata-Rata Harga Satu Tahun Per Enum di Kota/Kab');

		    foreach ($data['metadata_harga'] as $field) { 
		    	$chr = $arr_cols[$mas_next_key];
					if(!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')){
						$this->excel->getActiveSheet()->mergeCells($chr.($row+1).':'.$chr.($row+2));
		      }
		      $explode = explode('_',$field);
		      $nama = $explode[0];
		      $width = strlen('Rata - Rata Harga '.$nama);
					$this->excel->getActiveSheet()->getColumnDimension($chr)->setWidth($width);
		      $this->excel->getActiveSheet()->setCellValue($chr.($row+1), 'Rata - Rata Harga '.$nama);
		      $mas_next_key++;
		    }

		    if(count($data['metadata_volume']) > 0) {
		    	$next_key = $next_key+1;
		    	$chr = $arr_cols[$next_key];
			    $next_key = $next_key + (count($data['metadata_volume'])-1);
			  	$chr2 = $arr_cols[$next_key];
			    $cols_header = $chr.$row.':'.$chr2.$row;
			    $this->excel->getActiveSheet()->mergeCells($cols_header);
			    $this->excel->getActiveSheet()->setCellValue($chr.$row, 'Total Pasokan Satu Tahun Per Enum di Kota/Kab');
		    }

		    foreach ($data['metadata_volume'] as $field) { 
		    	$chr = $arr_cols[$mas_next_key];
					if(!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')){
						$this->excel->getActiveSheet()->mergeCells($chr.($row+1).':'.$chr.($row+2));
		      }
		      $explode = explode('_',$field);
		      $nama = $explode[0];
		      $width = strlen('Total '.$nama);
					$this->excel->getActiveSheet()->getColumnDimension($chr)->setWidth($width);
		      $this->excel->getActiveSheet()->setCellValue($chr.($row+1), 'Total '.$nama);
		      $mas_next_key++;
		    }


		    $next_keydays = array_search('E',$arr_cols);
		    if(!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')){
		    	$row = $row+2;
					foreach ($data['komoditas'] as $key => $metadata) {
						$strlen = strlen($metadata['nama']);
						$width = round($strlen/count($data['count_day2']));
						$width = $width+1;
		  			for($d=1;$d<=count($data['count_day2']);$d++){
		  				$chr3 = $arr_cols[$next_keydays];
		  				$this->excel->getActiveSheet()->setCellValue($chr3.($row), 'H'.$d);
		    			$this->excel->getActiveSheet()->getColumnDimension($chr3)->setWidth($width);
		  				$next_keydays++;
		  			}
		  		}
				}else{
					$row = $row+1;
				}

				$endchr = $arr_cols[count($arr_cols)-1];
				// $this->excel->getActiveSheet()->getStyle('A1:'.$endchr.$row)->getAlignment()->setWrapText(true); 

				$rowberfore = $row+1;
		  	$no = 1;
		  	// echo $row;
		  	// die();
				foreach ($data['entries2'] as $data_entry){
		      $row++;
					$this->excel->getActiveSheet()->setCellValue('A'.$row, $no++);
					$this->excel->getActiveSheet()->setCellValue('B'.$row, $data_entry['provinsi']);
					$this->excel->getActiveSheet()->setCellValue('C'.$row, $data_entry['kota']);
					$this->excel->getActiveSheet()->setCellValue('D'.$row, $data_entry['display_name']);
					$next_keysval = array_search('E',$arr_cols);
			  	foreach ($data['komoditas'] as $key => $metadata) {
			  		$val_metadatas = $data_entry[$metadata['field']];
			  		if(!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')){
				  		foreach($val_metadatas as $val){
								$chr = $arr_cols[$next_keysval];
				  			$this->excel->getActiveSheet()->setCellValue($chr.$row, number_format($val,0,',',''));
				  			$next_keysval++;
		          }
		        }else{
		        	$chr = $arr_cols[$next_keysval];
		        	$this->excel->getActiveSheet()->setCellValue($chr.$row, number_format($val_metadatas,0,',',''));
		        	$next_keysval++;
		        }
		    	}

		      foreach ($data['metadata_harga'] as $field) { 
		      	$chr = $arr_cols[$next_keysval];
		        $rata2tahunan = number_format($data_entry['avgthnan_'.$field],0,',','');
		        $this->excel->getActiveSheet()->setCellValue($chr.$row, ($rata2tahunan > 0 ? $rata2tahunan : '-'));
		        $next_keysval++;
		      }

		      foreach ($data['metadata_volume'] as $field) {
		      	$chr = $arr_cols[$next_keysval];
		        $totaltahunan = number_format($data_entry['sumthnan_'.$field],0,',','');
		        $this->excel->getActiveSheet()->setCellValue($chr.$row, ($totaltahunan > 0 ? $totaltahunan : '-'));
		        $next_keysval++;
		      }

				}

				$cols_header = 'A'.$rowberfore.':'.$endchr.$row;
				$this->excel->getActiveSheet()->getStyle($cols_header)->applyFromArray(
			    array(
		        'borders' => array(
		          'allborders' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array('rgb' => '000000')
		          )
		        ),
			    )
				);
			}



			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
	  }else{
	  	$rowspan = (!$this->input->get('f-hari') && $this->input->get('f-minggu_ke') ? 'rowspan="3"' : 'rowspan="2"');
			$rowspan2 = (!$this->input->get('f-hari') && $this->input->get('f-minggu_ke') ? 'rowspan="2"' : '');
			$colspan_metadata = (!$this->input->get('f-hari') && $this->input->get('f-minggu_ke')) ? 'colspan="'.count($data['count_day']).'"' : '';

	  	$nama_laporan = $data['nama_laporan'];

			$nama_file = 'Data_Harga_'.$nama_laporan;

			$html ="
	    <table border=\"0\" cellpadding=\"5\"  align=\"center\" style=\"text-align:center;\">
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	      <tr>
	          <td colspan=\"".$colspan."\">
	              <span style=\"font: bold 20px Open Sans; display: block;\">Data Harga ".$nama_laporan."</span>
	          </td>
	      </tr>";

	      if($this->input->get('f-provinsi') != ''){
	      	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	      	$kota = "";
	      	if($this->input->get('f-kota') != ''){
	      		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	      	}
					$html .= '
						<tr>
							<td colspan="'.$colspan.'">
								<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
							</td>
						</tr>
					';
					$nama_file .= '_'.$provinsi.$kota;
				}

				$periode = array();
				$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
				
				if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bln') != '' || $this->input->get('f-thn') != '' || $this->input->get('f-hari')){

					if($this->input->get('f-hari') != ''){
						$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
					}else{
						if($this->input->get('f-minggu_ke') != ''){
							$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
						}
						if($this->input->get('f-bln') != ''){
							$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bln')];
						}
						if($this->input->get('f-thn') != ''){
							$periode[] = 'Tahun '.$this->input->get('f-thn');
						}
					}
				}else{
					$periode[] = ' Minggu Ke '.$data['minggu_ke'];
					$periode[] = 'Bulan '.$arr_month[$data['bln']];
					$periode[] = 'Tahun '.$data['thn'];
				}

				if(count($periode) > 0){
					$implode_periode = implode(', ',$periode);
					$nama_file .= "_".implode('_',$periode);
					$html .= '
						<tr>
							<td colspan="'.$colspan.'">
								<span style="font: bold 20px Open Sans; display: block;">Periode: '.$implode_periode.'</span>
							</td>
						</tr>
					';
				}

	      $html .="
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	    </table>";

	  	$html .= '
	  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
	  		$html .='
	      <thead>
	        <tr style="background-color:#ccc;">
	        	<th '.$rowspan.'>No</th>
	        	<th '.$rowspan.'>Provinsi</th>
	        	<th '.$rowspan.'>Kota</th>
	        	<th '.$rowspan.'>Nama Unit</th>
	  				<th '.$rowspan.'>Petugas</th>';
	  				foreach ($data['komoditas'] as $key => $metadata) { 
	              $html.='<th '.$colspan_metadata.' rowspan="2">'.$metadata['nama'].'</th>';
	          }

	          $html .= '
	          	<th colspan="'.count($data['metadata_harga']).'" style="text-align:center;">Rata-Rata Harga Satu Tahun Per Enum di Kota/Kab</th>';
	          if(count($data['metadata_volume']) > 0) {
	            $html .= '<th colspan="'.count($data['metadata_volume']).'" style="text-align:center;">Total Pasokan Satu Tahun Per Enum di Kota/Kab</th>';
	          }
	        	$html .= '
	        </tr>
	        <tr style="background-color:#ccc;">';
	          foreach ($data['metadata_harga'] as $field) { 
	            $explode = explode('_',$field);
	            $nama = $explode[0];
	            
	            $html .= '<th '.$rowspan2.' width="100">Rata - Rata Harga '.$nama.'</th>';
	          }

	          foreach ($data['metadata_volume'] as $field) { 
	            $explode = explode('_',$field);
	            $nama = $explode[0];
	            
	            $html.='<th '.$rowspan2.'>Total '.$nama.'</th>';
	          }
	        $html .= '</tr>';

	          if(!$this->input->get('f-hari') && $this->input->get('f-minggu_ke')){
	          	$html .= '<tr style="background-color:#ccc;">';
				  			foreach ($data['komoditas'] as $key => $metadata) {
					  			for($d=1;$d<=count($data['count_day']);$d++){
					  				$html.='<th>H'.$d.'</th>';
					  			}
					  		}
				  		$html .= '</tr>';
				  	}

			  	$html .= '
	  		</thead>
	  		<tbody>';
	  		$no = 1;
	  		foreach ($data['entries'] as $data_entry){
	  			$html .='
	  				<tr>
	  					<td>'.$no++.'</td>
	  					<td>'.$data_entry['provinsi'].'</td>
	  					<td>'.$data_entry['kota'].'</td>
	  					<td>'.$data_entry['unit_name'].'</td>
					  	<td>'.$data_entry['display_name'].'</td>';
					  	foreach ($data['komoditas'] as $key => $metadata) {
					  		$h2 = 1;
					  		$val_metadatas = $data_entry[$metadata['field']];
					  		if(!$this->input->get('f-hari') && $this->input->get('f-minggu_ke') != ""){
						  		foreach($val_metadatas as $val){
		              	$html.='<td>'.number_format($val,0,',','').'</td>';
		              	$h2++;
		              }
		            }else{
		            	$html.='<td>'.number_format($val_metadatas,0,',','').'</td>';
		            }
	          	}

	            foreach ($data['metadata_harga'] as $field) { 
	              $rata2tahunan = number_format($data_entry['avgthnan_'.$field],0,',','');
	              $html .= '<td style="text-align:right;">'.($rata2tahunan > 0 ? $rata2tahunan : '-').'</td>';
	            }

	            foreach ($data['metadata_volume'] as $field) {
	              $totaltahunan = number_format($data_entry['sumthnan_'.$field],0,',','');
	              $html .= '<td style="text-align:right;">'.($totaltahunan > 0 ? $totaltahunan : '-').'</td>';
	            } 

	        $html .= '</tr>';
	  		}
	  		$html .='
	  		</tbody>
	  	</table>';


	  	// ------- Sandingan periode ----------------------------

	  	if($this->input->get('f-is_sandingan')){
		  	$rowspan = (!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2') ? 'rowspan="3"' : 'rowspan="2"');
				$rowspan2 = (!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2') ? 'rowspan="2"' : '');
				$colspan_metadata = (!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')) ? 'colspan="'.count($data['count_day2']).'"' : '';

				$html .="<br>
		    <table border=\"0\" cellpadding=\"5\"  align=\"center\" style=\"text-align:center;\">";

					$periode = array();
					$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
					
					if($this->input->get('f-minggu_ke2') != '' || $this->input->get('f-bln2') != '' || $this->input->get('f-thn2') != '' || $this->input->get('f-hari2')){

						if($this->input->get('f-hari2') != ''){
							$periode[] = date_idr($this->input->get('f-hari2'), 'l, d F Y', null);
						}else{
							if($this->input->get('f-minggu_ke2') != ''){
								$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke2');
							}
							if($this->input->get('f-bln2') != ''){
								$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bln2')];
							}
							if($this->input->get('f-thn2') != ''){
								$periode[] = 'Tahun '.$this->input->get('f-thn2');
							}
						}
					}else{
						$periode[] = ' Minggu Ke '.$data['minggu_ke2'];
						$periode[] = 'Bulan '.$arr_month[$data['bln']];
						$periode[] = 'Tahun '.$data['thn'];
					}

					if(count($periode) > 0){
						$implode_periode = implode(', ',$periode);
						$html .= '
							<tr>
								<td colspan="'.$colspan.'">
									<span style="font: bold 20px Open Sans; display: block;">Periode: '.$implode_periode.'</span>
								</td>
							</tr>
						';
					}

		      $html .="
		      <tr>
		          <td colspan=\"".$colspan."\"></td>
		      </tr>
		    </table>";

		  	$html .= '
		  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
		  		$html .='
		      <thead>
		        <tr style="background-color:#ccc;">
		        	<th '.$rowspan.'>No</th>
		        	<th '.$rowspan.'>Provinsi</th>
		        	<th '.$rowspan.'>Kota</th>
		        	<th '.$rowspan.'>Nama Unit</th>
		  				<th '.$rowspan.'>Petugas</th>';
		  				foreach ($data['komoditas'] as $key => $metadata) { 
		              $html.='<th '.$colspan_metadata.' rowspan="2">'.$metadata['nama'].'</th>';
		          }

		          $html .= '
		          	<th colspan="'.count($data['metadata_harga']).'" style="text-align:center;">Rata-Rata Harga Satu Tahun Per Enum di Kota/Kab</th>';
		          if(count($data['metadata_volume']) > 0) {
		            $html .= '<th colspan="'.count($data['metadata_volume']).'" style="text-align:center;">Total Pasokan Satu Tahun Per Enum di Kota/Kab</th>';
		          }
		        	$html .= '
		        </tr>
		        <tr style="background-color:#ccc;">';
		          foreach ($data['metadata_harga'] as $field) { 
		            $explode = explode('_',$field);
		            $nama = $explode[0];
		            
		            $html .= '<th '.$rowspan2.' width="100">Rata - Rata Harga '.$nama.'</th>';
		          }

		          foreach ($data['metadata_volume'] as $field) { 
		            $explode = explode('_',$field);
		            $nama = $explode[0];
		            
		            $html.='<th '.$rowspan2.'>Total '.$nama.'</th>';
		          }
		        $html .= '</tr>';

		          if(!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')){
		          	$html .= '<tr style="background-color:#ccc;">';
					  			foreach ($data['komoditas'] as $key => $metadata) {
						  			for($d=1;$d<=count($data['count_day2']);$d++){
						  				$html.='<th>H'.$d.'</th>';
						  			}
						  		}
					  		$html .= '</tr>';
					  	}

				  	$html .= '
		  		</thead>
		  		<tbody>';
		  		$no = 1;
		  		foreach ($data['entries2'] as $data_entry){
		  			$html .='
		  				<tr>
		  					<td>'.$no++.'</td>
		  					<td>'.$data_entry['provinsi'].'</td>
		  					<td>'.$data_entry['kota'].'</td>
		  					<td>'.$data_entry['unit_name'].'</td>
						  	<td>'.$data_entry['display_name'].'</td>';
						  	foreach ($data['komoditas'] as $key => $metadata) {
						  		$h2 = 1;
						  		$val_metadatas = $data_entry[$metadata['field']];
						  		if(!$this->input->get('f-hari2') && $this->input->get('f-minggu_ke2')){
							  		foreach($val_metadatas as $val){
			              	$html.='<td>'.number_format($val,0,',','').'</td>';
			              	$h2++;
			              }
			            }else{
			            	$html.='<td>'.number_format($val_metadatas,0,',','').'</td>';
			            }
		          	}

		            foreach ($data['metadata_harga'] as $field) { 
		              $rata2tahunan = number_format($data_entry['avgthnan_'.$field],0,',','');
		              $html .= '<td style="text-align:right;">'.($rata2tahunan > 0 ? $rata2tahunan : '-').'</td>';
		            }

		            foreach ($data['metadata_volume'] as $field) {
		              $totaltahunan = number_format($data_entry['sumthnan_'.$field],0,',','');
		              $html .= '<td style="text-align:right;">'.($totaltahunan > 0 ? $totaltahunan : '-').'</td>';
		            } 

		        $html .= '</tr>';
		  		}
		  		$html .='
		  		</tbody>
		  	</table>';
		  }

	  	
	  	$data['html'] = $html;
	  	$this->load->view('admin/page_print2', $data);
	  }
	}

	public function download_input_laporan($data, $page){

		if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan') AND ! group_has_role('laporan','view_own_unit_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;

		$nama_laporan = $data['nama_laporan'];
		$nama_file = 'Data Laporan '.$nama_laporan;

		$laporan_data['entries'] = $data['laporan_data']['entries'];

		$html ="
    <table border=\"0\" cellpadding=\"7\">
      <tr>
          <td colspan=\"7\"></td>
      </tr>
      <tr>
          <td colspan=\"7\" align=\"center\">
              <span style=\"font: bold 20px Open Sans; display: block;\">Daftar Laporan ".$nama_laporan."</span>
          </td>
      </tr>";

      if($id_provinsi != NULL){
      	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
      	$kota = "";
      	if($this->input->get('f-kota') != ''){
      		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
      	}
				$html .= '
					<tr>
						<td colspan="7" align="center">
							<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
						</td>
					</tr>
				';
				$nama_file .= '_'.$provinsi.$kota;
			}

			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bln') != '' || $this->input->get('f-thn') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bln') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bln')];
					}
					if($this->input->get('f-thn') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-thn');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$data['minggu_ke'];
				$periode[] = 'Bulan '.$arr_month[date('m')];
				$periode[] = 'Tahun '.date('Y');
			}

			if(count($periode) > 0){
				$implode_periode = implode(', ',$periode);
				$nama_file .= "_".implode('_',$periode);
				$html .= '
					<tr>
						<td colspan="7" align="center">
							<span style="font: bold 20px Open Sans; display: block;">Periode: '.$implode_periode.'</span>
						</td>
					</tr>
				';
			}

      $html .="
      <tr>
          <td colspan=\"7\"></td>
      </tr>
    </table>";

  	$html .= '
  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
  		$html .='
      <thead style="background-color:#ccc;">
        <tr>
        	<th>No</th>
        	<th>'.lang("laporan:pengirim").'</th>
        	<th>'.lang("location:provinsi:singular").'</th>
  				<th>'.lang("location:kota:singular").'</th>
  				<th>Nama Unit</th>
  				<th>'.lang("laporan:channel").'</th>
  				<th>'.lang("laporan:minggu_ke").'</th>
  				<th>'.lang("laporan:tanggal").'</th>
  			</tr>
  		</thead>
  		<tbody>';
  		$no = 1;
  		foreach ($laporan_data['entries'] as $laporan_data_entry){
  			$html .='
  				<tr>
  					<td>'.$no++.'</td>
  					<td>'.$laporan_data_entry['display_name'].'</td>
  					<td>'.$laporan_data_entry['provinsi'].'</td>
				  	<td>'.$laporan_data_entry['kota'].'</td>
				  	<td>'.$laporan_data_entry['unit_name'].'</td>
				  	<td>'.$laporan_data_entry['channel'].'</td>
				  	<td>'.$laporan_data_entry['minggu_ke'].'</td>
				  	<td>'.date_idr($laporan_data_entry['created_on'], 'd F Y', null).'</td>
  				</tr>
  			';
  		}
  		$html .='
  		</tbody>
  	</table>';

  	if($page == 'download') {
	  	$save = '';
	    $save .= "
	    <html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
	    xmlns=\"http://www.w3.org/TR/REC-html40\">
	     
	    <head>
	        <meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
	        <meta name=ProgId content=Excel.Sheet>
	        <meta name=Generator content=\"Microsoft Excel 11\">
	        <title>Data Input Web</title>
	    </head>
	    <body>";

	    $save .= $html;
	    $save .= "
	        </body>
	        </html>";

	    header("Content-Disposition: attachment; filename=\"".$nama_file.".xls\"");
	    header("Content-Type: application/vnd.ms-excel");
	    header('Cache-Control: max-age=0');

	    echo $save;
	  }else{
	  	$data['html'] = $html;
	  	$this->load->view('admin/page_print', $data);
	  }
	}

	public function ajax_unit_dropdown($parent_id = 0, $select_all = 0) {
    $units = $parent_id == 0 ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($parent_id);
    if ($select_all) {
        echo '<option value="">' . lang('global:select-all') . '</option>';
    } else {
        if (count($units)) {
            echo '<option value="-1">' . lang('global:select-pick') . '</option>';
        } else {
            echo '<option value="-1">' . lang('global:select-none') . '</option>';
        }
    }
    
    foreach ($units as $unit) {
        echo '<option value="' . $unit['id'] . '"> ' . $unit['unit_name'] . '</option>';
    }
	}

	public function ajax_get_metadata_by_komoditas($tipe_laporan, $id_komoditas){
		$data['metadata'] = $this->metadata_m->get_metadata_by_tipe_laporan($tipe_laporan, $id_komoditas, 'default_laporan_metadata.*',NULL,'urutan');
		$data['mode'] = 'new';

		$this->load->view('admin/data_metadata_form', $data);
	}
	// --------------------------------------------------------------------------

}