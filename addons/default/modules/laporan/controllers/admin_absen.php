<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_absen extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'absen';

  public function __construct()
  {
      parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'access_absen_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper('laporan');
    $this->lang->load('laporan');		
		$this->load->model('absen_m');
		$this->load->model('tipe_m');
  }

  /**
	 * List all absen
   *
   * @return	void
   */
  public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_absen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// $tgl = '2018-07-30';
		// $weeks = getWeeks($tgl, 'monday');
		// dump($weeks);
		// die();

    // -------------------------------------
		// Get entries
		// -------------------------------------

		$id_tipes = array(1,2, 8,9);
		$filters[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filters);

		if($this->input->get('f-tipe_laporan')){
			$data['absen']['entries'] = $this->absen_m->get_absen_admin();
			$data['absen']['total'] = count($data['absen']['entries']);

			$dates = $this->absen_m->get_dates();
			$data['addDates'] = (count($dates) > 0) ? implode(',', $dates) : '';

			$data['min_year'] = $this->absen_m->get_min_year(); 
			$data['max_year'] = $this->absen_m->get_max_year();


			if($_POST){

				$tgl_buka = explode('|',$this->input->post('tgl_buka'));
				$tgl_buka = $tgl_buka[0];
				$jam_buka = $this->input->post('jam_buka');
				$tgl_tutup = explode('|',$this->input->post('tgl_tutup'));
				$tgl_tutup = $tgl_tutup[0];
				$jam_tutup = $this->input->post('jam_tutup');

				$oldDates = array();
				$arr_tgl = explode(', ', $this->input->post('tanggal'));
				foreach ($dates as $key => $oldDate) {
					$oldDate = str_replace("'","",$oldDate);
					$oldDates[] = $oldDate;
					if(!in_array($oldDate, $arr_tgl)){
						$oldDate = date('Y-m-d', strtotime($oldDate));
						$this->absen_m->delete_absen_by_tgl($oldDate);
					}
				}

				foreach ($arr_tgl as $key => $tgl) 
				{
					if(!in_array($tgl, $oldDates))
					{
						$tgl = date('Y-m-d', strtotime($tgl));	
						$weeks = getWeeks($tgl, 'monday');

						$waktu_buka = $tgl;
						if($tgl_buka > 0){
							$waktu_buka = date('Y-m-d', strtotime("+".$tgl_buka." day", strtotime($tgl)));
						}
						
						if($tgl_buka == -1 ){
							$waktu_buka = date( "Y-m-d", (strtotime($tgl) - 86400));
						}
						
						$waktu_buka = $waktu_buka ." ".$jam_buka;

						$waktu_tutup = $tgl;
						if($tgl_tutup > 0){
							$waktu_tutup = date('Y-m-d', strtotime("+".$tgl_tutup." day", strtotime($tgl)));
						}
						$waktu_tutup = $waktu_tutup ." ".$jam_tutup;
						$values['id_laporan_tipe'] = $this->input->get('f-tipe_laporan');
						$values['tanggal'] = $tgl;
						$values['bulan'] = $weeks['bulan'];
						$values['tahun'] = $weeks['tahun'];
						$values['minggu_ke'] = (string) $weeks['week'];
						$values['tanggal_awal'] = $weeks['first_day_of_week'];
						$values['tanggal_akhir'] = $weeks['last_day_of_week'];
						$values['waktu_buka'] = $waktu_buka;
						$values['waktu_tutup'] = $waktu_tutup;

						$this->absen_m->insert_absen($values);
					}
				}
				$this->session->set_flashdata('success', lang('laporan:absen:submit_success'));
				redirect('admin/laporan/absen/index?'.$_SERVER['QUERY_STRING']);
			}
		}

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:absen:plural'))
    	->append_js('module::jquery-ui.multidatespicker.js')
    	->append_css('module::mdp.css')
    	->append_js('date-time/bootstrap-timepicker.js')
    	->append_css('bootstrap-timepicker.css')
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:absen:plural'))
			->build('admin/absen_index', $data);
  }

	
	/**
   * Edit a absen entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the absen to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_absen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		if($_POST){
			$tgl_buka = $_POST['tgl_buka'];
			$jam_buka = $_POST['jam_buka'];
			$waktu_buka = $tgl_buka ." ".$jam_buka;

			$tgl_tutup = $_POST["tgl_tutup"];
			$jam_tutup = $_POST["jam_tutup"];
			$waktu_tutup = $tgl_tutup ." ".$jam_tutup;
			$values['waktu_buka'] = $waktu_buka;
			$values['waktu_tutup'] = $waktu_tutup;		
			$this->absen_m->update_absen($values, $id);
			die(lang('laporan:absen:submit_success'));
		}

		$data['fields'] = $this->absen_m->get_absen_by_id($id);
		$data['entry_id'] = $id;
		$data['mode'] = 'edit';

		$waktu_buka = explode(' ',$data['fields']['waktu_buka']);
		$data['fields']['tgl_buka'] = $waktu_buka[0];
		$data['fields']['jam_buka'] = $waktu_buka[1];
		$waktu_tutup = explode(' ',$data['fields']['waktu_tutup']);
		$data['fields']['tgl_tutup'] = $waktu_tutup[0];
		$data['fields']['jam_tutup'] = $waktu_tutup[1];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->load->view('admin/absen_form',$data);
  }
	
	/**
   * Delete a absen entry
   * 
   * @param   int [$id] The id of absen to be deleted
   * @return  void
   */
  public function delete($id = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_absen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
    $this->absen_m->delete_absen_by_id($id);
    $this->session->set_flashdata('error', lang('laporan:absen:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
    redirect('admin/laporan/absen/index?'.$_SERVER["QUERY_STRING"]);
  }
	
	/**
   * Insert or update absen entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_absen($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('laporan:absen:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->absen_m->insert_absen($values);
				
			}
			else
			{
				$result = $this->absen_m->update_absen($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}