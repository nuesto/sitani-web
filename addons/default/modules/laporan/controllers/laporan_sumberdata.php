<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Laporan_sumberdata extends Public_Controller
{

	public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    // Load the required classes

    $this->lang->load('laporan');
    $this->lang->load('location/location');	

		$this->load->model('organization/units_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
		$this->load->model('tipe_m');
		$this->load->model('organization/types_m');
		$this->lang->load('organization/organization');
  }

  public function index(){
  	$surffix = '';
	  if($_SERVER['QUERY_STRING']){
	    $surffix = '?'.$_SERVER['QUERY_STRING'];
	  }

   	// -------------------------------------
		// Pagination
		// -------------------------------------
	  $count_sumberdata = $this->units_m->get_sumberdata();
		$pagination_config['base_url'] = base_url(). 'laporan/sumberdata/index/';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($count_sumberdata);
		// $pagination_config['per_page'] = 2;
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
    // -------------------------------------
		// Get entries
		// -------------------------------------
		
  	$data['sumberdata']['entries'] = $this->units_m->get_sumberdata($pagination_config);
		$data['sumberdata']['total'] = count($count_sumberdata);
		$data['sumberdata']['pagination'] = $this->pagination->create_links();


		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
		
		$filter_kota = null;
  	if($this->input->get('f-provinsi') != '') {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
  	}

		// Set Location
		$filter_kota = null;
  	if($this->input->get('f-provinsi') != '') {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
  	}

  	$id_tipes = array();
		foreach (range(1, 7) as $number) {
		    $id_tipes[] = $number;
		}
		$filter_tipes[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filter_tipes);
  	

  	$uri = '';
    $page = $this->uri->segment(4);
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }
    $data['uri'] = $uri;

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:sumberdata'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('laporan:sumberdata'))
			->build('sumberdata_index', $data);
  }

  public function view($id){
		// Get our entry. We are simply specifying
    $data['units'] =  $this->units_m->get_units_by_id($id);
    if($data['units'] == NULL){
    	redirect('laporan/sumberdata/index');
    }
		$data['units']->unit_type = (object) $this->types_m->get_type_by_id($data['units']->unit_type);
		

		// Get multiple relationship

		$unit_parents = $this->units_m->get_parent_units_by_id($data['units']->id);
		foreach ($unit_parents as $key => $value) {
			$unit_parents[$key]['unit_type'] = (object) $this->types_m->get_type_by_id($value['unit_type']);
		}

		$data['units']->unit_parents = $unit_parents;

		$this->load->model('organization/memberships_m');
		$pendampings = $this->memberships_m->get_membership_by_unit($id);
		$data['pendampings'] = $pendampings;
		$nos = '';
		$namas = '';
		if(count($pendampings) > 0){
			foreach ($pendampings as $key => $pendamping) {
				$nohp = substr($pendamping['user_telp'], 0, -4).'xxxx';
				$nama_pendamping[] = $pendamping['user_display_name'] .' ('.$nohp.')';
				// $nama_pendamping[] = $pendamping['user_display_name'];
				$no_pendamping[] = $nohp;
			}
			$nos = implode(', ',$no_pendamping);
			$namas = implode(', ', $nama_pendamping);
		}
		
		$data['units']->nama_pendamping = $namas;
		$data['units']->no_pendamping = $nos;

  	// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:sumberdata'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('laporan:sumberdata'),'/laporan/sumberdata/index')
			->set_breadcrumb(lang('laporan:sumberdata:view'))
			->build('sumberdata_entry', $data);
  }
  public function download($print = 0){
		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;

		$sumberdata['entries'] = $this->units_m->get_sumberdata();


		$laporan = "";
		if($this->input->get('f-tipe_laporan')){
			$laporan = $this->tipe_m->get_tipe_by_id($this->input->get('f-tipe_laporan'))['nama_laporan'];
		}
		$rowspan = 6;

		$judul = "Daftar Sumber Data ".$laporan;
		$nama_file = $judul;


  	if($print == 0){
	  	$this->load->library('excel');
  		$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle($judul);

			foreach (range('A','E') as $columnID) {
				$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			}

			$this->excel->getActiveSheet()->setCellValue('A1', $judul);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:E1');

			$row = 2;
			if($this->input->get('f-provinsi') != ''){
	    	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	    	$kota = "";
	    	if($this->input->get('f-kota') != ''){
	    		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	    	}

	    	$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->mergeCells('A1:E1');
				$row++;
				
				$nama_file .= '_'.$provinsi.$kota;
			}

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
			$this->excel->getActiveSheet()->setCellValue('B'.$row, lang("location:provinsi:singular"));
			$this->excel->getActiveSheet()->setCellValue('C'.$row, lang("location:kota:singular"));
			$this->excel->getActiveSheet()->setCellValue('D'.$row, lang("laporan:tipe"));
			$this->excel->getActiveSheet()->setCellValue('E'.$row, lang("laporan:gap_or_tti"));

			
			$this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray(
		    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'CCCCCC')
	        ),
		    )
			);

			$total =  $row + count($sumberdata['entries']);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$total)->applyFromArray(
		    array(
	        'borders' => array(
	          'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	          )
	        ),
		    )
			);

			$no = 1;

			foreach ($sumberdata['entries'] as $sumberdata_entry){
  			$row++;
  			$this->excel->getActiveSheet()->setCellValue('A'.$row, $no++);
				$this->excel->getActiveSheet()->setCellValue('B'.$row, $sumberdata_entry['provinsi']);
				$this->excel->getActiveSheet()->setCellValue('C'.$row, $sumberdata_entry['kota']);
				$this->excel->getActiveSheet()->setCellValue('D'.$row, $sumberdata_entry['type_name']);
				$this->excel->getActiveSheet()->setCellValue('E'.$row, $sumberdata_entry['unit_name']);
  		}

  		header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');


	  }else{
	  	$html ="
    <table border=\"0\" cellpadding=\"5\">
      <tr>
          <td colspan=\"".$rowspan."\"></td>
      </tr>
      <tr>
          <td colspan=\"".$rowspan."\" align=\"center\">
              <span style=\"font: bold 20px Open Sans; display: block;\">".$judul."</span>
          </td>
      </tr>";


      if($id_provinsi != NULL){
		  	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
		  	$kota = "";
		  	if($this->input->get('f-kota') != ''){
		  		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
		  	}
				$html .= '
					<tr>
						<td colspan="'.$rowspan.'" align="center">
							<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
						</td>
					</tr>
				';
				$nama_file .= '_'.$provinsi.$kota;
			}

      $html .= "
      <tr>
          <td colspan=\"".$rowspan."\"></td>
      </tr>
    </table>";


  	$html .= '
  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
  		$html .='
      <thead style="background-color:#ccc;">
        <tr>
        	<th>No</th>
        	<th>'.lang("location:provinsi:singular").'</th>
        	<th>'.lang("location:kota:singular").'</th>
			   	<th>'.lang("laporan:tipe").'</th>
			   	<th>'.lang("laporan:gap_or_tti").'</th>';
			   	$html .= '
  			</tr>
  		</thead>
  		<tbody>';
  		$no = 1;
  		foreach ($sumberdata['entries'] as $sumberdata_entry){
  			$html .='
  				<tr>
  					<td>'.$no++.'</td>
  					<td>'.$sumberdata_entry['provinsi'].'</td>
  					<td>'.$sumberdata_entry['kota'].'</td>
  					<td>'.$sumberdata_entry['type_name'].'</td>
				  	<td>'.$sumberdata_entry['unit_name'].'</td>';
				   	$html .= '
  				</tr>
  			';
  		}
  		$html .='
  		</tbody>
  	</table>';

	  	$data['html'] = $html;
	  	$this->load->view('laporan/admin/page_print', $data);
	  }
	}
}