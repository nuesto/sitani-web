<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_sms extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'sms';

  public function __construct()
  {
    parent::__construct();

    date_default_timezone_set('Asia/Jakarta');
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'access_sms_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper('laporan');
    $this->lang->load('laporan');		
    $this->lang->load('location/location');		
		$this->load->model('sms_m');
		$this->load->model('absen_m');
		$this->load->model('tipe_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
		$this->load->model('pendamping_m');
		$this->load->model('data_m');

		// Organization
		$this->load->model('organization/types_m');
    $this->load->model('organization/units_m');
    $this->load->model('organization/memberships_m');

		$this->config->load('organization/organization');
    $this->node_type_slug = $this->config->item('node_type');
    $this->get_type = $this->types_m->get_type_by_slug($this->node_type_slug);
    $this->node_type_level = $this->get_type->type_level;
    $this->node_type_slug = $this->get_type->type_slug;

    $this->unit = $this->memberships_m->get_one_unit_by_member($this->current_user->id);
  }

  /**
	 * List all sms
   *
   * @return	void
   */
  public function index($id=0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------

		$arr_status = array(1=>'success',2=>'error');
		
		if(! group_has_role('laporan', 'view_all_sms') AND ! group_has_role('laporan', 'view_own_sms') AND ! group_has_role('laporan','view_own_prov_sms')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$no_hp = NULL;
		if(!group_has_role('laporan','view_all_sms') AND !group_has_role('laporan','view_own_prov_sms')){
			$data_pendamping = $this->pendamping_m->get_pendamping_by_id($this->current_user->id);
			$no_hp = $data_pendamping['telp'];
			$filter_periode['s.no_hp'] = $no_hp;
		}

		$data['id'] = $id;
		
		$data['tipes'] = $this->tipe_m->get_tipe();

		$data['unit_types'] = $this->types_m->get_all_types();
		$memberships_user = $this->memberships_m->get_memberships_by_user($this->current_user->id);
		$type_ids=array();
		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}
		$data['type_ids'] = $type_ids;
		$data['skip_group'] = array('admin','site_admin');

		$title = lang('laporan:daftar_sms');
				

		if($this->input->get('f-tipe_laporan')){
			// Set Tipe Laporan
			$f_tipe_laporan = $this->input->get('f-tipe_laporan');

			$id_tipe_laporan = $this->input->get('f-tipe_laporan');
			// untuk yang harian (TTIC, Gapoktan 2017, TTI 2017)
			$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;
			if($id_tipe_laporan == 3 || $id_tipe_laporan == 4){
				$id_tipe_laporan = 1;
			}
			if($id_tipe_laporan == 5){
				$id_tipe_laporan = 2;
			}

			// Set hari
			$hari = ($this->input->get('f-hari')) ? $this->input->get('f-hari') : date('Y-m-d');

      //--------------------------------------------------------------------------------------------------------

			// Set default periode
			$weeks = getWeeks($hari, 'monday');
      //--------------------------------------------------------------------------------------------------------

			// Set Periode 1
      //--------------------------------------------------------------------------------------------------------

			// Set Tahun
			$tahun = NULL;
			if($this->input->get('f-tahun')){
				$tahun = $this->input->get('f-tahun');
			}else{
				$tahun = $weeks['tahun'];
			}
			$data['tahun'] = $tahun;


			$data['min_year'] = $this->absen_m->get_min_year(); 
			$data['max_year'] = $this->absen_m->get_max_year();
      //--------------------------------------------------------------------------------------------------------

			// Set Bulan
			if($this->input->get('f-bulan')){
				$bulan = $this->input->get('f-bulan');
			}else{
				$bulan = $weeks['bulan'];
			}

			// $bulan = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
			$bulan = (strlen($bulan) == 1) ? '0'.$bulan : $bulan;
			$thn_bln = $tahun."-".$bulan;

			if(isset($_GET['f-bulan'])){
				if($_GET['f-bulan'] != ""){
					$bulan = $this->input->get('f-bulan');
				}else{
					$bulan = '';
				}
			}
				
			if($bulan != ''){
				$bulan = $bulan + 0;
			}

			$data['bulan'] = $bulan;
      //--------------------------------------------------------------------------------------------------------

			// Set Jumlah Minggu
			$first_day_of_month = $thn_bln.'-01';

			$count_of_week = get_count_of_week($first_day_of_month);
			$data['count_of_week'] = $count_of_week;
      //--------------------------------------------------------------------------------------------------------

			// Minggu ke
			if($this->input->get('f-minggu_ke')){
				$minggu_ke = $this->input->get('f-minggu_ke');
			}else{
				$minggu_ke = (string) $weeks['week'];
			}

			if(isset($_GET['f-minggu_ke'])){
				if($_GET['f-minggu_ke'] != ""){
					$minggu_ke = $this->input->get('f-minggu_ke');
				}else{
					$minggu_ke = '';
				}
			}

			$data['minggu_ke'] = $minggu_ke;
      //--------------------------------------------------------------------------------------------------------

			// Set Hari
			$data['days'] = array();
			if($this->input->get('f-tipe_laporan') > 2){
				$data['weeks'] = $weeks;
				$firstDayOfWeek = $this->data_m->get_hari($tahun, $bulan, $minggu_ke);
			}else{
				$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke);
				// dump($this->db->last_query());
				$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
			}

			if($firstDayOfWeek != NULL) {
	      $kode_sms = NULL;
		    if($this->input->get('f-tipe_laporan')){
		    	$get_tipe = $this->tipe_m->get_tipe_by_id($this->input->get('f-tipe_laporan'));
		    	$kode_sms = $get_tipe['kode_sms'];
		    	$filter_periode['s.isi'] = $kode_sms;
		    }

		    $data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
				$firstDay = $data['weeks']['first_day_of_week'];
				$lastDay = $data['weeks']['last_day_of_week'];
				if($f_tipe_laporan > 2){
					if(!$this->input->get('f-hari')){
						$filter_periode["DATE(s.tanggal)>="] = $firstDay;
						$filter_periode["DATE(s.tanggal)<="] = $lastDay;
					}
					for ($h=0; $h <=6 ; $h++) { 
						$data['days'][] = date('Y-m-d', strtotime('+'.$h.' days', strtotime($firstDay)));
					}
				}else{
					$get_days = $this->absen_m->get_days_by_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
					foreach ($get_days as $key => $day) {
						$data['days'][] = $day['tanggal'];
					}
					if(!$this->input->get('f-hari')){
						$filter_periode["DATE(s.tanggal)>="] = $get_days[0]['tanggal_awal'];
						$filter_periode["DATE(s.tanggal)<="] = $get_days[0]['tanggal_akhir'];
					}
				}

				if($this->input->get('f-hari')){
					$filter_periode["DATE(s.tanggal)"] = $this->input->get('f-hari');	
				}
			}

			$data['firstDayOfWeek'] = $firstDayOfWeek;
      //--------------------------------------------------------------------------------------------------------

			// Set Header Text Report Periode 1
			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bulan') != '' || $this->input->get('f-tahun') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bulan') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan')];
					}
					if($this->input->get('f-tahun') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-tahun');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$minggu_ke;
				$periode[] = 'Bulan '.$arr_month[($bulan < 10 ? '0'.$bulan : $bulan)];
				$periode[] = 'Tahun '.$tahun;
			}

			if(count($periode) > 0){
				$data['text_periode'] = 'Periode: '.implode(', ',$periode);
			}
      //--------------------------------------------------------------------------------------------------------

			if($this->input->get('f-tipe_laporan') <= 2){
	      $today = date('Y-m-d H:i:s');
				$data['tglOrIdAbsen'] = NULL;
				$cek_absen = $this->absen_m->get_absen_by_date($today, $f_tipe_laporan);
				if(count($cek_absen) > 1){
					$data['tglOrIdAbsen'] = $cek_absen['id'];
				}
			}else{
				$data['tglOrIdAbsen'] = date('Y-m-d');
			}

			// ---------------------------------------------------------------------------------

			// Nama Laporan
			$data['f_tipe_laporan'] = $f_tipe_laporan;if($f_tipe_laporan == 3){
				$data['nama_laporan'] = 'Gapoktan tingkat TTIC';
			}else{
				$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($f_tipe_laporan)['nama_laporan'];
			}

			// Set Location
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			if(group_has_role('laporan','view_all_laporan')) {
				
				$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

				// Set Location
				$filter_kota = null;
		  	if($this->input->get('f-provinsi') != '') {
		  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
		  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		  	}

	  	}elseif(group_has_role('laporan','view_own_prov_laporan')){
	  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
	  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
	  	}

	  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);

  		if($data['id_provinsi'] != NULL){
  			$filter_periode['lp.id'] = $data['id_provinsi'];
  		}
  		if($data['id_kota'] != NULL){
  			$filter_periode['k.id'] = $data['id_kota'];
  		}

      //--------------------------------------------------------------------------------------------------------


				// Set Units to find data from some units.
			$units_id = NULL;
		  if(! group_has_role('laporan', 'view_all_laporan') && !group_has_role('laporan','view_own_prov_laporan')){
        $units_id = user_units2($this->current_user->id);
        $result = $this->units_m->get_child($units_id);
        $units_id = $units_id.$result;
      }
      //--------------------------------------------------------------------------------------------------------


			if($this->input->get('f-status_error')){
				$filter_periode['s.status_error'] = $this->input->get('f-status_error');
			}
			
	    if(!$this->input->get('page')){

		    // -------------------------------------
        // Get Organization by Member
        // -------------------------------------

				$data['types'] = $this->types_m->get_all_types();

        $unit_id = $this->unit['id'];
        
        $types = $data['types'];
        $organization = array();
        $addedLevel = array();
        $n2 = 0;
        $n = count($types);

        foreach($types as $i => $type){
          if (in_array($type['level'], $addedLevel)) {
            continue;
          }
          $addedLevel[] = $type['level'];
          $units = $this->units_m->get_unit_by_child($unit_id);

          if(isset($units['id'])){
            if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit_'.$units['type_level']] = $units['id'];
              $n2++;
            } else if ($units['type_slug'] == $this->node_type_slug) {
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit'] = $units['id'];
            }
            $unit_id = $units['id'];
          }
        }

        if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
          if($this->unit['type_slug'] == $this->node_type_slug){
            $organization['id_organization_unit'] = $this->unit['id'];
          }else{
            $organization['id_organization_unit_'.$n2] = $this->unit['id'];
          }
          $organization['organization_name_'.$n2] = $this->unit['name'];
        }

        $data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
        $data['user_unit_level'] = $this->unit['type_level'];
        $data['node_type_level'] = $this->node_type_level;
        $data['node_type_slug'] = $this->node_type_slug;
        $data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

        $get_units = $this->memberships_m->get_units_by_member($this->current_user->id);
	      if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
		      unset($organization['id_organization_unit']);
		      unset($organization['organization_name_1']);
		    }

		    $is_tti17 = false;
		    if($this->input->get('f-tipe_laporan') == 5 && count($get_units) > 1){
		    	$is_tti17 = true;
		    	unset($organization['id_organization_unit']);
		    	unset($organization['organization_name_0']);
		    }

        $data['organization'] = $organization;
				if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
	      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
        }else{
          if($this->input->get('f-tipe_laporan') == 5 && count($get_units) > 1){
            $data['child_organization'] = $get_units;
          }
    		}
    		
      	$data['is_tti17'] = $is_tti17;


	      if($this->input->get('id_organization_unit') > 0){
	      	$filter_periode['m.membership_unit'] = $this->input->get('id_organization_unit');
				}
			  // -------------------------------------------------------------------------------------------------------------

				if($firstDayOfWeek != NULL) {

					$data['uri'] = $this->get_query_string(6);
					$data['status'] = '';

					$surffix = '';
			    if($_SERVER['QUERY_STRING']){
			      $surffix = '?'.$_SERVER['QUERY_STRING'];
			    }

					// -------------------------------------
					// Pagination
					// -------------------------------------
			    // $count_sms = $this->sms_m->get_sms(NULL, 1, $tanggal_awal, $tanggal_akhir, $no_hp, $kode_sms, $data['id_provinsi']);
			    $count_sms = $this->sms_m->get_sms(NULL, 1, $filter_periode);
					$pagination_config['base_url'] = base_url(). 'admin/laporan/sms/index/'.$id;
					$pagination_config['uri_segment'] = 6;
					$pagination_config['suffix'] = $surffix;
					$pagination_config['total_rows'] = $count_sms;
					// $pagination_config['per_page'] = 2;
					$pagination_config['per_page'] = Settings::get('records_per_page');
					$this->pagination->initialize($pagination_config);
					$data['pagination_config'] = $pagination_config;
					
			    // -------------------------------------
					// Get entries
					// -------------------------------------
					
			    $data['sms']['entries'] = $this->sms_m->get_sms($pagination_config, 0, $filter_periode);
					$data['sms']['total'] = $count_sms;
					$data['sms']['pagination'] = $this->pagination->create_links();
				}

				

				// -------------------------------------
		    // Build the page. See views/admin/index.php
		    // for the view code.
				// -------------------------------------

				if($this->input->get('f-tipe_laporan')){
					$title .= ' '.$data['nama_laporan'];
				}

				if($this->input->get('f-tipe_laporan') == 2){
					if($this->input->get('id_organization_unit_0') > 0){
						$org = $this->units_m->get_units_by_id($this->input->get('id_organization_unit_0'));
						$title .= ' Di "'.$org->unit_name.'"';
					}
				}

				$data['title'] = $title;
				
		    $this->template->title(lang('laporan:daftar_sms'))
					->set_breadcrumb('Dasbor', '/admin')
					->set_breadcrumb(lang('laporan:daftar_sms'))
					->build('admin/sms_index', $data);
			}else{
				$data['sms']['entries'] = $this->sms_m->get_sms(NULL,0,$filter_periode);
				$this->download($id, $data, $this->input->get('page'));
			}
		}else{
			$data['title'] = $title;
			$this->template->title(lang('laporan:daftar_sms'))
					->set_breadcrumb('Dasbor', '/admin')
					->set_breadcrumb(lang('laporan:daftar_sms'))
					->build('admin/sms_index', $data);
		}
  }
	
	/**
   * Display one sms
   *
   * @return  void
   */
  public function view($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_sms') AND ! group_has_role('laporan', 'view_own_sms')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
    $data['sms'] = $this->sms_m->get_sms_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_sms')){
			if($data['sms']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:sms:view'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:sms:plural'), '/admin/laporan/sms/index')
			->set_breadcrumb(lang('laporan:sms:view'))
			->build('admin/sms_entry', $data);
  }
	
	/**
   * Create a new sms entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
  public function create()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'create_sms')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_sms('new')){	
				$this->session->set_flashdata('success', lang('laporan:sms:submit_success'));				
				redirect('admin/laporan/sms/index');
			}else{
				$data['messages']['error'] = lang('laporan:sms:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/laporan/sms/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:sms:new'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:sms:plural'), '/admin/laporan/sms/index')
			->set_breadcrumb(lang('laporan:sms:new'))
			->build('admin/sms_form', $data);
  }
	
	/**
   * Edit a sms entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the sms to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'edit_all_sms') AND ! group_has_role('laporan', 'edit_own_sms')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('laporan', 'edit_all_sms')){
			$entry = $this->sms_m->get_sms_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_sms('edit', $id)){	
				$this->session->set_flashdata('success', lang('laporan:sms:submit_success'));				
				redirect('admin/laporan/sms/index');
			}else{
				$data['messages']['error'] = lang('laporan:sms:submit_failure');
			}
		}
		
		$data['fields'] = $this->sms_m->get_sms_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/laporan/sms/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:sms:edit'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:sms:plural'), '/admin/laporan/sms/index')
			->set_breadcrumb(lang('laporan:sms:view'), '/admin/laporan/sms/view/'.$id)
			->set_breadcrumb(lang('laporan:sms:edit'))
			->build('admin/sms_form', $data);
  }
	
	/**
   * Delete a sms entry
   * 
   * @param   int [$id] The id of sms to be deleted
   * @return  void
   */
  public function delete($id, $id_sms = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'delete_all_sms') AND ! group_has_role('laporan', 'delete_group_sms')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		if(! group_has_role('laporan','delete_all_sms')){
			$entry = $this->sms_m->get_sms_by_id($id_sms);
			if($entry['id_provinsi'] != user_provinsi($this->current_user->id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
    $this->sms_m->delete_sms_by_id($id_sms);
    $this->session->set_flashdata('error', lang('laporan:sms:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------

		$uri = $this->get_query_string(7);
		
    redirect('admin/laporan/sms/index/'.$id.$uri);
  }
	
	/**
		* Insert or update sms entry in database
		*
		* @param   string [$method] The method of database update ('new' or 'edit').
	 	* @param   int [$row_id] The entry id (if in edit mode).
   	* @return	boolean
  */
	private function _update_sms($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('laporan:sms:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->sms_m->insert_sms($values);
				
			}
			else
			{
				$result = $this->sms_m->update_sms($values, $row_id);
			}
		}
		
		return $result;
	}

	public function get_query_string($page){
		// -------------------------------------
		// Get query string
		// -------------------------------------

		$page = $this->uri->segment($page);
		$uri = '';
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }
    
    return $uri;
	}

	public function download($id, $data, $page){

		$this->load->helper('laporan');

		if(! group_has_role('laporan', 'view_all_sms')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

  	$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;

  	$sms['entries'] = $data['sms']['entries'];
		$nama_laporan = lang('laporan:daftar_sms');
		$nama_laporan .= " ".$data['nama_laporan'];

		if($this->input->get('id_organization_unit') > 0){
			$id_unit = $this->input->get('id_organization_unit');
			$nama_unit = $this->units_m->get_units_by_id($id_unit)->unit_name;
			$nama_laporan .= ' "'.$nama_unit.'"';
		}

		if($this->input->get('f-tipe_laporan') == 2){
			if($this->input->get('id_organization_unit_0') > 0){
				$nama_unit = $this->units_m->get_units_by_id($this->input->get('id_organization_unit_0'))->unit_name;
				$nama_laporan .= ' Di "'.$nama_unit.'"';
			}
		}

		$nama_file = $nama_laporan;

		

  	if($page == 'download'){
	  	$this->load->library('excel');
  		$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle($nama_laporan);

			foreach (range('A','G') as $columnID) {
				$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			}

			$this->excel->getActiveSheet()->setCellValue('A1', $nama_laporan);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:G1');
			$this->excel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$row = 2;
			if($this->input->get('f-provinsi') != ''){
	    	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	    	$kota = "";
	    	if($this->input->get('f-kota') != ''){
	    		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	    	}

	    	$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
				$this->excel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$row++;
				
				$nama_file .= '_'.$provinsi.$kota;
			}

			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bulan') != '' || $this->input->get('f-tahun') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bulan') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan')];
					}
					if($this->input->get('f-tahun') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-tahun');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$data['minggu_ke'];
				$periode[] = 'Bulan '.$arr_month[date('m')];
				$periode[] = 'Tahun '.date('Y');
			}

			if(count($periode) > 0){
				$implode_periode = implode(', ',$periode);
				$nama_file .= "_".implode('_',$periode);

				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Periode: '.$implode_periode);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
				$this->excel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$row++;
			}

			$row++;

			$this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
			$this->excel->getActiveSheet()->setCellValue('B'.$row, lang("laporan:pengirim"));
			$this->excel->getActiveSheet()->setCellValue('C'.$row, lang("location:provinsi:singular"));
			$this->excel->getActiveSheet()->setCellValue('D'.$row, lang("location:kota:singular"));
			$this->excel->getActiveSheet()->setCellValue('E'.$row, lang("laporan:isi"));
			$this->excel->getActiveSheet()->setCellValue('F'.$row, lang("laporan:tanggal"));
			$this->excel->getActiveSheet()->setCellValue('G'.$row, lang("laporan:keterangan_parsing"));

			
			$this->excel->getActiveSheet()->getStyle('A'.$row.':G'.$row)->applyFromArray(
		    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'CCCCCC')
	        ),
		    )
			);

			$total =  $row + count($sms['entries']);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':G'.$total)->applyFromArray(
		    array(
	        'borders' => array(
	          'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	          )
	        ),
		    )
			);

			$no = 1;
			foreach ($sms['entries'] as $sms_entry){
				$row++;
				$this->excel->getActiveSheet()->setCellValue('A'.$row, $no++);
				$this->excel->getActiveSheet()->setCellValue('B'.$row, $sms_entry['display_name']);
				$this->excel->getActiveSheet()->setCellValue('C'.$row, $sms_entry['provinsi']);
				$this->excel->getActiveSheet()->setCellValue('D'.$row, $sms_entry['kota']);
				$this->excel->getActiveSheet()->setCellValue('E'.$row, $sms_entry['isi']);
				$this->excel->getActiveSheet()->setCellValue('F'.$row, date_idr($sms_entry['tanggal'], 'd F Y', null));
				$this->excel->getActiveSheet()->setCellValue('G'.$row, $sms_entry['keterangan_parsing']);
			}

			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
			
	  }else{
	  	$html ="
		    <table border=\"0\" cellpadding=\"5\">
		      <tr>
		          <td colspan=\"7\"></td>
		      </tr>
		      <tr>
		          <td colspan=\"7\" align=\"center\">
		              <span style=\"font: bold 20px Open Sans; display: block;\">".$nama_laporan."</span>
		          </td>
		      </tr>";

		      if($id_provinsi != NULL){
		      	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
		      	$kota = "";
		      	if($this->input->get('f-kota') != ''){
		      		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
		      	}
						$html .= '
							<tr>
								<td colspan="7" align="center">
									<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
								</td>
							</tr>
						';
						$nama_file .= '_'.$provinsi.$kota;
					}

					$periode = array();
					$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
					
					if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bulan') != '' || $this->input->get('f-tahun') != '' || $this->input->get('f-hari')){

						if($this->input->get('f-hari') != ''){
							$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
						}else{
							if($this->input->get('f-minggu_ke') != ''){
								$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
							}
							if($this->input->get('f-bulan') != ''){
								$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan')];
							}
							if($this->input->get('f-tahun') != ''){
								$periode[] = 'Tahun '.$this->input->get('f-tahun');
							}
						}
					}else{
						$periode[] = ' Minggu Ke '.$data['minggu_ke'];
						$periode[] = 'Bulan '.$arr_month[date('m')];
						$periode[] = 'Tahun '.date('Y');
					}

					if(count($periode) > 0){
						$implode_periode = implode(', ',$periode);
						$nama_file .= "_".implode('_',$periode);
						$html .= '
							<tr>
								<td colspan="7" align="center">
									<span style="font: bold 20px Open Sans; display: block;">Periode: '.$implode_periode.'</span>
								</td>
							</tr>
						';
					}

					if($this->input->get('f-status_error')){
						$html .= '
							<tr>
								<td colspan="7" align="center">
									<span style="font: bold 20px Open Sans; display: block;">Kategori "'.lang('laporan:sms:'.$this->input->get('f-status_error')).'"</span>
								</td>
							</tr>
						';
					}

		      $html .="
		      <tr>
		          <td colspan=\"7\"></td>
		      </tr>
		    </table>";

		  	$html .= '
		  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
		  		$html .='
		      <thead style="background-color:#ccc;">
		        <tr>
		        	<th>No</th>
		        	<th>'.lang("laporan:pengirim").'</th>
		        	<th>'.lang("location:provinsi:singular").'</th>
		  				<th>'.lang("location:kota:singular").'</th>
		  				<th>'.lang("laporan:isi").'</th>
		  				<th>'.lang("laporan:tanggal").'</th>
		  				<th>'.lang("laporan:keterangan_parsing").'</th>
		  			</tr>
		  		</thead>
		  		<tbody>';
		  		$no = 1;
		  		foreach ($sms['entries'] as $sms_entry){
		  			$html .='
		  				<tr>
		  					<td>'.$no++.'</td>
		  					<td>'.$sms_entry['display_name'].'</td>
		  					<td>'.$sms_entry['provinsi'].'</td>
						  	<td>'.$sms_entry['kota'].'</td>
						  	<td>'.$sms_entry['isi'].'</td>
						  	<td>'.date_idr($sms_entry['tanggal'], 'd F Y', null).'</td>
						  	<td>'.$sms_entry['keterangan_parsing'].'</td>
		  				</tr>
		  			';
		  		}
		  		$html .='
		  		</tbody>
		  	</table>';
	  	$data['html'] = $html;
	  	$this->load->view('admin/page_print2', $data);
	  }
	}

	// --------------------------------------------------------------------------

}