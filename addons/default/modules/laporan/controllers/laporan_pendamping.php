<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Laporan_pendamping extends Public_Controller
{

	public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    // Load the required classes

    $this->lang->load('laporan');
    $this->lang->load('location/location');	

		$this->load->model('organization/memberships_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
		$this->load->model('tipe_m');
    $this->load->model('pendamping_m');
		$this->load->model('groups/group_m');
  }

  public function index($id_group){

 
  	$surffix = '';
	  if($_SERVER['QUERY_STRING']){
	    $surffix = '?'.$_SERVER['QUERY_STRING'];
	  }

    $data['id_group'] = $id_group;
    if($id_group == "7"){
      $data['id_groups'] = $id_group;
    } else if($id_group == "9") {
      $data['id_groups'] = $id_group;
    } else if($id_group == "10") {
      $data['id_groups'] = $id_group;
    } else {
      $data['id_groups'] = '0';
    }
   	// -------------------------------------
		// Pagination
		// -------------------------------------
	  // $count_pendamping = $this->memberships_m->get_membership();
	  $count_pendamping = $this->pendamping_m->get_pendamping(null, null, null, false, $id_group);
		$pagination_config['base_url'] = base_url(). 'laporan/pendamping/index/'.$id_group;
		$pagination_config['uri_segment'] = 5;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($count_pendamping);
		$pagination_config['per_page'] = 10;
		// $pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

		
    // -------------------------------------
		// Get entries
		// -------------------------------------
		
  	// $data['pendamping']['entries'] = $this->memberships_m->get_membership($pagination_config);
  	$data['pendamping']['entries'] = $this->pendamping_m->get_pendamping($pagination_config, null, null, false, $id_group);
		$data['pendamping']['total'] = count($count_pendamping);
		$data['pendamping']['pagination'] = $this->pagination->create_links(null, 1);

		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
		
		$filter_kota = null;
  	if($this->input->get('f-provinsi') != '') {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
  	}

		// Set Location
		$filter_kota = null;
  	if($this->input->get('f-provinsi') != '') {
  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
  	}

  	$data['get_location'] = '';
  	$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;
  	if($id_provinsi != NULL){
	  	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
	  	$kota = "";
	  	if($this->input->get('f-kota') != ''){
	  		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	  	}
			$data['get_location'] .= '<br>Provinsi '.$provinsi.$kota;
		}

  	$data['tipes'] = $this->tipe_m->get_tipe();
    $data['nama_pendamping'] = $this->group_m->get_group_by_id($id_group)->description;

  	$uri = '';
    $page = $this->uri->segment(4);
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }
    $data['uri'] = $uri;

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:pendamping:singular'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('laporan:pendamping:singular'))
			->build('pendamping_index2', $data);
  }

  public function pendamping_harian(){
    $surffix = '';
    if($_SERVER['QUERY_STRING']){
      $surffix = '?'.$_SERVER['QUERY_STRING'];
    }

    // -------------------------------------
    // Pagination
    // -------------------------------------
    $count_pendamping = $this->pendamping_m->get_pendamping(null, null, null, false, '9');
    $pagination_config['base_url'] = base_url(). 'laporan/pendamping/pendamping_harian/';
    $pagination_config['uri_segment'] = 4;
    $pagination_config['suffix'] = $surffix;
    $pagination_config['total_rows'] = count($count_pendamping);
    $pagination_config['per_page'] = 10;
    // $pagination_config['per_page'] = Settings::get('records_per_page');
    $this->pagination->initialize($pagination_config);
    $data['pagination_config'] = $pagination_config;

    
    // -------------------------------------
    // Get entries
    // -------------------------------------
    
    $data['pendamping']['entries'] = $this->pendamping_m->get_pendamping($pagination_config, null, null, false, '9');
    $data['pendamping']['total'] = count($count_pendamping);
    $data['pendamping']['pagination'] = $this->pagination->create_links(null, 1);

    $data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
    
    // Set Location
    $filter_kota = null;
    if($this->input->get('f-provinsi') != '') {
      $filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
      $data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
    }

    $data['get_location'] = '';
    $id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;
    if($id_provinsi != NULL){
      $provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
      $kota = "";
      if($this->input->get('f-kota') != ''){
        $kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
      }
      $data['get_location'] .= '<br>Provinsi '.$provinsi.$kota;
    }

    $data['tipes'] = $this->tipe_m->get_tipe();
    

    $uri = '';
    $page = $this->uri->segment(4);
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }
    $data['uri'] = $uri;

    // -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
    // -------------------------------------
    
    $this->template->title(lang('laporan:pendamping:singular'))
      ->set_breadcrumb('Home', '/')
      ->set_breadcrumb(lang('laporan:pendamping:singular'))
      ->build('pendamping_harian', $data);
  }

  public function download($print = 0, $id_group){
		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;

		$pendamping['entries'] = $this->pendamping_m->get_pendamping(NULL, NULL, NULL, TRUE, $id_group);


		$laporan = "";
		if($this->input->get('f-tipe_laporan')){
			$laporan = $this->tipe_m->get_tipe_by_id($this->input->get('f-tipe_laporan'))['nama_laporan'];
		}
		$colspan = 10;

		// $judul = "Data Pendamping ".$laporan;
		$judul = "Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM) <br> melalui kegiatan Toko Tani Indonesia (TTI)";
		$nama_file = "Data Pendamping";

  	if($print == 0){

  		$this->load->library('excel');
  		$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle('Daftar Pendamping');


			$align_center = array(
	      'alignment' => array(
	          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	      )
			);

			$border = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000')
          )
        ),
	    );


			$this->excel->getActiveSheet()->setCellValue('A1', 'Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM)');
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:J1');

			$this->excel->getActiveSheet()->setCellValue('A2', 'melalui kegiatan Toko Tani Indonesia (TTI)');
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A2:J2');

			$this->excel->getActiveSheet()->getStyle('A1:J2')->applyFromArray($align_center);

			$row = 3;
			if($this->input->get('f-provinsi') != ''){
	    	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	    	$kota = "";
	    	if($this->input->get('f-kota') != ''){
	    		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	    	}

	    	$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(13);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->mergeCells('A'.$row.':J'.$row);

				$this->excel->getActiveSheet()->getStyle('A'.$row.':J'.$row)->applyFromArray($align_center);
				$nama_file .= '_'.$provinsi.$kota;
			}


    	
			$row++;
      if($id_group != 9) {
  			foreach ($pendamping['entries'] as $pendamping_entry){
          $row++;
          $frow_bold = $row;
          $rows = ($pendamping_entry['total_gap'] + $pendamping_entry['total_tti']) - 1;

          $this->excel->getActiveSheet()->mergeCells('A'.$row.':J'.$row);
          $this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$pendamping_entry['provinsi'].', '.$pendamping_entry['kota']);
          $this->excel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);
          $this->excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
  				
  				$row++;
  				$row_border = $row;
  				$this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
  				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Pendamping');

  				$this->excel->getActiveSheet()->mergeCells('C'.$row.':F'.$row);
  				$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Gapoktan');

  				$this->excel->getActiveSheet()->mergeCells('G'.$row.':J'.$row);
  				$this->excel->getActiveSheet()->setCellValue('G'.$row, 'TTI');

  				foreach (range('A','J') as $columnID) {
  					// $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
  					$this->excel->getActiveSheet()->getColumnDimension($columnID)->setWidth('18');
  				}

  				$row++;
  				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Nama');
  				$this->excel->getActiveSheet()->setCellValue('B'.$row, 'No HP');
  				$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Nama');
  				$this->excel->getActiveSheet()->setCellValue('D'.$row, 'Ketua');
  				$this->excel->getActiveSheet()->setCellValue('E'.$row, 'Alamat');
  				$this->excel->getActiveSheet()->setCellValue('F'.$row, 'No HP');
  				$this->excel->getActiveSheet()->setCellValue('G'.$row, 'Nama');
  				$this->excel->getActiveSheet()->setCellValue('H'.$row, 'Pemilik');
  				$this->excel->getActiveSheet()->setCellValue('I'.$row, 'Alamat');
  				$this->excel->getActiveSheet()->setCellValue('J'.$row, 'No HP');

  				$this->excel->getActiveSheet()->getStyle('A'.$frow_bold.':J'.$row)->getFont()->setBold(true);

          foreach ($pendamping_entry['gapoktan'] as $key2 => $gap) {
          	$row++; 
            $rows2 = (count($gap['tti']) > 1 ? count($gap['tti']) - 1 : count($gap['tti']));
            $telp = substr($gap['telp'], 0, -4);
            $hp_ketua = substr($gap['hp_ketua'], 0, -4);
            $this->excel->getActiveSheet()->mergeCells('A'.$row.':A'.($row+$rows2));
            $this->excel->getActiveSheet()->setCellValue('A'.$row, $gap['display_name']);
            $this->excel->getActiveSheet()->mergeCells('B'.$row.':B'.($row+$rows2));
            $this->excel->getActiveSheet()->setCellValue('B'.$row, $telp.'xxxx');
            $this->excel->getActiveSheet()->mergeCells('C'.$row.':C'.($row+$rows2));
            $this->excel->getActiveSheet()->setCellValue('C'.$row, $gap['unit_name']);
            $this->excel->getActiveSheet()->mergeCells('D'.$row.':D'.($row+$rows2));
            $this->excel->getActiveSheet()->setCellValue('D'.$row, $gap['nama_ketua']);
            $this->excel->getActiveSheet()->mergeCells('E'.$row.':E'.($row+$rows2));
            $this->excel->getActiveSheet()->setCellValue('E'.$row, $gap['unit_description']);
            $this->excel->getActiveSheet()->mergeCells('F'.$row.':F'.($row+$rows2));
            $this->excel->getActiveSheet()->setCellValue('F'.$row, $hp_ketua.'xxxx');
            if(count($gap['tti']) > 0){
              foreach ($gap['tti'] as $key3 => $tti) {
                // $hp_ketua = substr($tti['hp_ketua'], 0, -4);
                $hp_ketua = $tti['hp_ketua'];
              	$this->excel->getActiveSheet()->setCellValue('G'.$row, $tti['unit_name']);
  	          	$this->excel->getActiveSheet()->setCellValue('H'.$row, $tti['nama_ketua']);
  	          	$this->excel->getActiveSheet()->setCellValue('I'.$row, $tti['unit_description']);
                // $this->excel->getActiveSheet()->setCellValue('J'.$row, $hp_ketua.'xxxx');
  	          	$this->excel->getActiveSheet()->setCellValue('J'.$row, $hp_ketua);
                if($key3 < $rows2){
                	$row++;
                }
              }
            }else{
            	$this->excel->getActiveSheet()->setCellValue('G'.$row, '');
            	$this->excel->getActiveSheet()->setCellValue('H'.$row, '');
            	$this->excel->getActiveSheet()->setCellValue('I'.$row, '');
            	$this->excel->getActiveSheet()->setCellValue('J'.$row, '');
            	// $row++;
            } 
          }
          $this->excel->getActiveSheet()->getStyle('A'.$row_border.':J'.$row)->applyFromArray($border);
          $this->excel->getActiveSheet()->getStyle('A'.$row_border.':J'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
          $row++;
        }
      }else{
        $row++;
        $row_border = $row;
        $this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
        $this->excel->getActiveSheet()->setCellValue('B'.$row, 'Tipe');
        $this->excel->getActiveSheet()->setCellValue('C'.$row, 'Provinsi');
        $this->excel->getActiveSheet()->setCellValue('D'.$row, 'Kota');
        $this->excel->getActiveSheet()->setCellValue('E'.$row, 'Nama Pendamping');
        $this->excel->getActiveSheet()->setCellValue('F'.$row, 'No HP Pendamping');
        $this->excel->getActiveSheet()->setCellValue('G'.$row, 'Nama Unit');
        $this->excel->getActiveSheet()->setCellValue('H'.$row, 'Ketua');
        $this->excel->getActiveSheet()->setCellValue('I'.$row, 'Alamat');
        $this->excel->getActiveSheet()->setCellValue('J'.$row, 'No HP Ketua');

        $no = 1;
        foreach ($pendamping['entries'] as $pendamping_entry) {
          $row++; 
          $telp = substr($pendamping_entry['telp'], 0, -4);
          $hp_ketua = substr($pendamping_entry['hp_ketua'], 0, -4);
          $this->excel->getActiveSheet()->setCellValue('A'.$row, $no);
          $this->excel->getActiveSheet()->setCellValue('B'.$row, $pendamping_entry['type_name']);
          $this->excel->getActiveSheet()->setCellValue('C'.$row, $pendamping_entry['provinsi']);
          $this->excel->getActiveSheet()->setCellValue('D'.$row, $pendamping_entry['kota']);
          $this->excel->getActiveSheet()->setCellValue('E'.$row, $pendamping_entry['display_name']);
          $this->excel->getActiveSheet()->setCellValue('F'.$row, $telp.'xxxx');
          $this->excel->getActiveSheet()->setCellValue('G'.$row, $pendamping_entry['unit_name']);
          $this->excel->getActiveSheet()->setCellValue('H'.$row, $pendamping_entry['nama_ketua']);
          $this->excel->getActiveSheet()->setCellValue('I'.$row, $pendamping_entry['unit_description']);
          $this->excel->getActiveSheet()->setCellValue('J'.$row, $pendamping_entry['hp_ketua']);
          $no++;
        }

        $this->excel->getActiveSheet()->getStyle('A'.$row_border.':J'.$row)->applyFromArray($border);
        $this->excel->getActiveSheet()->getStyle('A'.$row_border.':J'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);


      }

  		header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');

	  }else{
	  	$html ="
	    <table border=\"0\" cellpadding=\"5\">
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	      <tr>
	          <td colspan=\"".$colspan."\" align=\"center\">
	              <span style=\"font: bold 20px Open Sans; display: block;\">".$judul."</span>
	          </td>
	      </tr>";


	      if($id_provinsi != NULL){
			  	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
			  	$kota = "";
			  	if($this->input->get('f-kota') != ''){
			  		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
			  	}
					$html .= '
						<tr>
							<td colspan="'.$colspan.'" align="center">
								<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
							</td>
						</tr>
					';
					$nama_file .= '_'.$provinsi.$kota;
				}

	      $html .= "
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	    </table>";

      if($id_group != 9) {
  	  	foreach ($pendamping['entries'] as $pendamping_entry){
          $rows = ($pendamping_entry['total_gap'] + $pendamping_entry['total_tti']) - 1;

          $html .= '
          <table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th colspan="10">
                  <center>
                    Provinsi '.$pendamping_entry['provinsi'].'<br>
                    '.$pendamping_entry['kota'].'
                  </center>
                </th>
              </tr>
              <tr>
                <th colspan="2" align="left">Pendamping</th>
                <th colspan="4" align="left">Gapoktan</th>
                <th colspan="4" align="left">TTI</th>
              </tr>
              <tr>
                <th align="left">Nama</th>
                <th align="left">No HP</th>
                <th align="left">Nama</th>
                <th align="left">Ketua</th>
                <th align="left">Alamat</th>
                <th align="left">No HP</th>
                <th align="left">Nama</th>
                <th align="left">Pemilik</th>
                <th align="left">Alamat</th>
                <th align="left">No HP</th>
              </tr>
            </thead>
            <tbody>
              <tr>';
                foreach ($pendamping_entry['gapoktan'] as $key2 => $gap) { 
                  $rows2 = count($gap['tti']);
                  $telp = substr($gap['telp'], 0, -4);
                  $hp_ketua = substr($gap['hp_ketua'], 0, -4);
                  $html .= '
                  <td '.($rows2 > 1 ? 'rowspan="'.$rows2.'"' : '').'>'.$gap['display_name'].'</td>
                  <td '.($rows2 > 1 ? 'rowspan="'.$rows2.'"' : '').'>'.$telp.'xxxx</td>
                  <td '.($rows2 > 1 ? 'rowspan="'.$rows2.'"' : '').'>'.$gap['unit_name'].'</td>
                  <td '.($rows2 > 1 ? 'rowspan="'.$rows2.'"' : '').'>'.$gap['nama_ketua'].'</td>
                  <td '.($rows2 > 1 ? 'rowspan="'.$rows2.'"' : '').'>'.$gap['unit_description'].'</td>
                  <td '.($rows2 > 1 ? 'rowspan="'.$rows2.'"' : '').'>'.$hp_ketua.'xxxx</td>';
                  if(count($gap['tti']) > 0){
                    foreach ($gap['tti'] as $key3 => $tti) { 
                      if($key3 > 0){ 
                        $html .='<tr>';
                      }
                      // $hp_ketua = substr($tti['hp_ketua'], 0, -4);
                      $hp_ketua = $tti['hp_ketua'];
                      $html .='
                      <td>'.$tti['unit_name'].'</td>
                      <td>'.$tti['nama_ketua'].'</td>
                      <td>'.$tti['unit_description'].'</td>'.
                      // '<td>'.$hp_ketua.'xxxx</td>';
                      '<td>'.$hp_ketua.'</td>';
                      if($key3 > 0){
                        $html .='</tr>';
                      }
                    }
                  }else{
                  	$html .='
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>';
                  }
                  if($key2 > 0){
                    $html .='</tr>';
                  } 
                }
                $html .= '
              </tr>
            </tbody>
          </table><br>';
        }
      }else{
        $html .= '
          <table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Tipe</th>
                <th>Provinsi</th>
                <th>Kota</th>
                <th>Nama Pendamping</th>
                <th>No HP Pendamping</th>
                <th>Nama Unit</th>
                <th>Ketua</th>
                <th>Alamat</th>
                <th>No HP Ketua</th>
              </tr>
            </thead>
            <tbody>
        ';
        $no = 1;
        foreach ($pendamping['entries'] as $pendamping_entry) {
          $telp = substr($pendamping_entry['telp'], 0, -4);
          $hp_ketua = substr($pendamping_entry['hp_ketua'], 0, -4);
          $html .= '
          <tr>
            <td>'.$no.'</td>
            <td>'.$pendamping_entry['type_name'].'</td>
            <td>'.$pendamping_entry['provinsi'].'</td>
            <td>'.$pendamping_entry['kota'].'</td>
            <td>'.$pendamping_entry['display_name'].'</td>
            <td>'.$telp.'xxxx</td>
            <td>'.$pendamping_entry['unit_name'].'</td>
            <td>'.$pendamping_entry['nama_ketua'].'</td>
            <td>'.$pendamping_entry['unit_description'].'</td>
            <td>'.$pendamping_entry['hp_ketua'].'</td>
          </tr>';
          $no++;
        }
      }

	  	$data['html'] = $html;
	  	$this->load->view('laporan/admin/page_print', $data);
	  }
	}

  public function download_harian($print=0){
    if(!group_has_role('users', 'manage_users') AND !group_has_role('users', 'manage_own_unit_users')){
      $this->session->set_flashdata('error', lang('cp:access_denied'));
      redirect('admin');
    }
    
    $id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;

    $pendamping['entries'] = $this->pendamping_m->get_pendamping(NULL, NULL, NULL, TRUE, 9);
    // dump($pendamping['entries']);
    // die();

    $laporan = "";
    if($this->input->get('f-tipe_laporan')){
      $laporan = $this->tipe_m->get_tipe_by_id($this->input->get('f-tipe_laporan'))['nama_laporan'];
    }
    $colspan = 10;

    $judul = "Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM) <br> melalui kegiatan Toko Tani Indonesia (TTI)";
    $nama_file = "Data Pendamping";

    if($print == 0){
      $this->load->library('excel');
      $this->excel->setActiveSheetIndex(0);
      $this->excel->getActiveSheet()->setShowGridlines(false);
      $this->excel->getActiveSheet()->setTitle('Daftar Pendamping');


      $align_center = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
      );

      $border = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '000000')
          )
        ),
      );


      $this->excel->getActiveSheet()->setCellValue('A1', 'Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM)');
      $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);
      $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
      $this->excel->getActiveSheet()->mergeCells('A1:J1');

      $this->excel->getActiveSheet()->setCellValue('A2', 'melalui kegiatan Toko Tani Indonesia (TTI)');
      $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
      $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
      $this->excel->getActiveSheet()->mergeCells('A2:J2');

      $this->excel->getActiveSheet()->getStyle('A1:J2')->applyFromArray($align_center);

      $row = 3;
      if($this->input->get('f-provinsi') != ''){
        $provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
        $kota = "";
        if($this->input->get('f-kota') != ''){
          $kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
        }

        $this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
        $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(13);
        $this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A'.$row.':J'.$row);

        $this->excel->getActiveSheet()->getStyle('A'.$row.':J'.$row)->applyFromArray($align_center);
        $nama_file .= '_'.$provinsi.$kota;
      }

      $row++;
      $this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
      $this->excel->getActiveSheet()->setCellValue('B'.$row, 'Tipe Unit');
      $this->excel->getActiveSheet()->setCellValue('C'.$row, lang("location:provinsi:singular"));
      $this->excel->getActiveSheet()->setCellValue('D'.$row, lang("location:kota:singular"));
      $this->excel->getActiveSheet()->setCellValue('E'.$row, 'Nama Pendamping');
      $this->excel->getActiveSheet()->setCellValue('F'.$row, 'No HP Pendamping');
      $this->excel->getActiveSheet()->setCellValue('G'.$row, 'Nama Unit');
      $this->excel->getActiveSheet()->setCellValue('H'.$row, 'Ketua');
      $this->excel->getActiveSheet()->setCellValue('I'.$row, 'Alamat');
      $this->excel->getActiveSheet()->setCellValue('J'.$row, 'No HP Ketua');

      
      $this->excel->getActiveSheet()->getStyle('A'.$row.':J'.$row)->applyFromArray(
        array(
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => 'CCCCCC')
          ),
        )
      );

      $total =  $row + count($pendamping['entries']);
      $this->excel->getActiveSheet()->getStyle('A'.$row.':J'.$total)->applyFromArray(
        array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN,
              'color' => array('rgb' => '000000')
            )
          ),
        )
      );

      $no = 1;

      foreach ($pendamping['entries'] as $pendamping_entry){
        $telp = substr($pendamping_entry['telp'], 0, -4);
        $hp_ketua = (strlen($pendamping_entry['hp_ketua']) > 5) ? substr($pendamping_entry['hp_ketua'], 0, -4) : '-';
        $row++;
        $this->excel->getActiveSheet()->setCellValue('A'.$row, $no++);
        $this->excel->getActiveSheet()->setCellValue('B'.$row, $pendamping_entry['type_name']);
        $this->excel->getActiveSheet()->setCellValue('C'.$row, $pendamping_entry['provinsi']);
        $this->excel->getActiveSheet()->setCellValue('D'.$row, $pendamping_entry['kota']);
        $this->excel->getActiveSheet()->setCellValue('E'.$row, $pendamping_entry['display_name']);
        $this->excel->getActiveSheet()->setCellValue('F'.$row, $telp.'xxxx');
        $this->excel->getActiveSheet()->setCellValue('G'.$row, $pendamping_entry['unit_name']);
        $this->excel->getActiveSheet()->setCellValue('H'.$row, $pendamping_entry['nama_ketua']);
        $this->excel->getActiveSheet()->setCellValue('I'.$row, $pendamping_entry['unit_description']);
        $this->excel->getActiveSheet()->setCellValue('J'.$row, $hp_ketua);
      }

      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache
                  
      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
    }else{
      $html ="
      <table border=\"0\" cellpadding=\"5\">
        <tr>
            <td colspan=\"".$colspan."\"></td>
        </tr>
        <tr>
            <td colspan=\"".$colspan."\" align=\"center\">
                <span style=\"font: bold 20px Open Sans; display: block;\">".$judul."</span>
            </td>
        </tr>";


        if($id_provinsi != NULL){
          $provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
          $kota = "";
          if($this->input->get('f-kota') != ''){
            $kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
          }
          $html .= '
            <tr>
              <td colspan="'.$colspan.'" align="center">
                <span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
              </td>
            </tr>
          ';
          $nama_file .= '_'.$provinsi.$kota;
        }

        $html .= "
        <tr>
            <td colspan=\"".$colspan."\"></td>
        </tr>
      </table>";

      $html .= '
        <table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
          $html .='
          <thead style="background-color:#ccc;">
            <tr>
              <th>No</th>
              <th>Tipe Unit</th>
              <th>'.lang("location:provinsi:singular").'</th>
              <th>'.lang("location:kota:singular").'</th>
              <th>Nama Pendamping</th>
              <th>No HP Pendamping</th>
              <th>Nama Unit</th>
              <th>Ketua</th>
              <th>Alamat</th>
              <th>No HP Ketua</th>';
              $html .= '
            </tr>
          </thead>
          <tbody>';
          $no = 1;
          foreach ($pendamping['entries'] as $pendamping_entry){
            $telp = substr($pendamping_entry['telp'], 0, -4);
            $hp_ketua = substr($pendamping_entry['hp_ketua'], 0, -4);
            $html .='
              <tr>
                <td>'.$no++.'</td>
                <td>'.$pendamping_entry['type_name'].'</td>
                <td>'.$pendamping_entry['provinsi'].'</td>
                <td>'.$pendamping_entry['kota'].'</td>
                <td>'.$pendamping_entry['display_name'].'</td>
                <td>'.$telp.'xxxx</td>
                <td>'.$pendamping_entry['unit_name'].'</td>
                <td>'.$pendamping_entry['nama_ketua'].'</td>
                <td>'.$pendamping_entry['unit_description'].'</td>
                <td>'.$hp_ketua.'</td>';
                $html .= '
              </tr>
            ';
          }
          $html .='
          </tbody>
        </table>';
      $data['html'] = $html;
      $this->load->view('laporan/admin/page_print', $data);
    }
  }
}