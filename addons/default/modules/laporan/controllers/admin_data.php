<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_data extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'data';

  public function __construct()
  {
    parent::__construct();
    
    date_default_timezone_set('Asia/Jakarta');
		// -------------------------------------
		// Check permission
		// -------------------------------------
	
		if(! group_has_role('laporan', 'access_laporan_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper('laporan');
		$this->load->library('organization/organization');
		
    $this->lang->load('laporan');		
		$this->load->model('data_m');
		$this->load->model('tipe_m');
		$this->load->model('metadata_m');
		$this->load->model('absen_m');
		$this->load->model('komoditas_m');

		$this->lang->load('location/location');	
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');

		// Organization
		$this->load->model('organization/types_m');
    $this->load->model('organization/units_m');
    $this->load->model('organization/memberships_m');

		$this->config->load('organization/organization');
    $this->node_type_slug = $this->config->item('node_type');
    $this->get_type = $this->types_m->get_type_by_slug($this->node_type_slug);
    $this->node_type_level = $this->get_type->type_level;
    $this->node_type_slug = $this->get_type->type_slug;

    $this->unit = $this->memberships_m->get_one_unit_by_member($this->current_user->id);
  }

  /**
	 * List all data
   *
   * @return	void
   */


  public function cek(){
  	$array = array('081353590262','081353412168','081339890379','085205399351','082347225994','085253714127','085237918144','082339834801','081935988244','081907722009','081373270123','085226220043','085384908282','082307625610','082179520612','081368437996','081367696280','085367751688','081225900267','085268345594','082306928383','085215798456','085357194178','081271885807','085382332211','085368477870','081367197461','085267332696','085377254687','081368021439','081218895125','081382916658','082111710983','081387440977','085692223339','085215449138','081213946103','08812368626','081367343673','08229375758','081382191117','081316800614','081517405688','085313856669','08561149996','081219506708','087771938990','081382634094','085814532062','082112260946','085285855349','085694177918','082111601972','08528970491','081315474538','083873412158','08561411322','085313856669','081298387495','081617619348','087880575939','085695052159','081314605612','087773176884','087773827111','083813235186','085882015731','087772034899','085313251027','085284380277','085811137599','085281530921','081910895748','085921860615','087772977919','087773711214','087773343936','087878733221','085282830009','087772728485','087773699495','085959976424','087772032204','087773699495','087741071600','0877727778499','087772032204','087871115880','087772113864','0859299811010','081807805285','085715748900','08521977895','08128852670','087878232338','085888810089','087772878451','087771065321','087809296832','085920171556','081906197566','081906291926','085715403721','081809097336','085946356220','085920041411','087783084856','081808226137','08975555601','087871350234','087871317272','08561166644','081317952004','081906194124','087771051071','087771093315','085945005084','08817447204','081906055777','081830879735','081328403755','08179808254','085777392914','087772220333','085945372627','085714846502','087884554558','081327178011','081327068821','085869469415','081548034367','085878902606','085229778114','085741356550','08156682274','081902100068','081548942627','087749935688','081327016036','081548819343','081327082194','081325911166','085229351920','081548855626','089622251558','081325660387','085225762510','081548554362','081393338864','081802074981','081328750244','087700124070','08132590819','085866833255','081393457701','08132892566','085229446813','081519489096','082136977100','081225031779','085602047443','081903987752','081227233145','085868253931','081326556016','085729327227','085225726075','081326251405','081542039759','08770857453','085226487518','081327132222','085227188707','082133974311','08157733185','082138501421','085225580376','081802788619','085229551466','085725368584','085235080689','085290955576','081804477773','08156731116','081542131660','085786302746','085225025592','081575413206','085292656744','081329510724','08122585993','08131523218','085226679770','082225405646','081391176876','081553996251','082141190506','085203300761','087856317272','085706020790','085233823336','085230069164','081216854930','085648485791','081398980642','081216269544','081335952238','081335078551','085231571170','082132287808','081510677800','085259318534','087706715085','085735901980','081252220522','082331008987','085651061453','085649276554','085735442428','081334166903','085235570009','085233969069','081259385049','081357650566','08124964036','081231114755','081335707609','081331087054','081230193030','081217724624','081615269534','082330073359','085231731661','08123125352','081553400233','085755470682','081235287444','082257968947','082231147383','081331842533','081233183338','082331847588','081331560075','085730444588','082143082677','082186163403','085380168953','082184289092','082372639324','081329763134','085382527768','085789943222','081271221812','081271467398','085379807279','085269098916','081367598126','085266801520','085377524211','081279501886','082379194866','085279857914','085279535834','082376048888','081215069003','085279607839','081369762309','082281848199','082185598637','081379735398','081379095720','085381194556','082175566345','082185552769','085267442539','081310171975','081279277066','082178904871','082180779538','081368354266');
  	$this->db->select('username');
  	$this->db->from('default_users');
  	$this->db->where_in('username', $array);
  	$this->db->where('group_id', '9');
  	$query = $this->db->get();
  	$not_entries_excel  = 126;
  	$not_in_database  = 15;
  	$result = $query->result_array();
  	count($result);
  	$existing = array();
  	foreach ($result as $key => $value) {
  		$existing[] = $value['username'];
  	}
  	// dump($this->db->last_query());
// die();
  	// $existing = array('081328403755','087884554558','082138501421','081553996251','081218895125','081382916658','082111710983','081387440977','085692223339','085215449138','081213946103','08812368626','081367343673','08229375758','081382191117','081316800614','081517405688','085313856669','08561149996','087771938990','081382634094','085814532062','082112260946','085285855349','085694177918','082111601972','08528970491','081315474538','083873412158','08561411322','085313856669','081298387495','081617619348','087880575939','085695052159','081314605612','087773176884','087773827111','083813235186','085882015731','087772034899','085313251027','085284380277','085811137599','085281530921','085921860615','087772977919','087773711214','087773343936','087878733221','085282830009','087772728485','087773699495','085959976424','087772032204','087773699495','087741071600','087772032204','087871115880','087772113864','081807805285','085715748900','08521977895','08128852670','087878232338','087772878451','087771065321','087809296832','085920171556','081906197566','081906291926','085715403721','081809097336','085946356220','085920041411','08975555601','087871350234','087871317272','08561166644','081317952004','081906194124','087771051071','087771093315','08817447204','081906055777','081328403755','085777392914','087772220333','085945372627','085714846502','087884554558','081353590262','081353412168','081339890379','085205399351','082347225994','085253714127','085237918144','082339834801','081935988244','081907722009','081327178011','081327068821','085869469415','081548034367','085878902606','085229778114','085741356550','08156682274','081902100068','081548942627','087749935688','081327016036','081548819343','081325911166','085229351920','089622251558','081325660387','085225762510','081548554362','081393338864','081802074981','081328750244','087700124070','085866833255','081393457701','08132892566','085229446813','081519489096','082136977100','081225031779','085602047443','081903987752','081227233145','085868253931','081326556016','085729327227','081542039759','085226487518','081327132222','085227188707','082133974311','082138501421','085229551466','085725368584','085290955576','081804477773','08156731116','081542131660','085786302746','085225025592','081575413206','085292656744','081329510724','08122585993','08131523218','085226679770','082225405646','081391176876','081373270123','085226220043','082307625610','082179520612','081368437996','081367696280','085367751688','081225900267','085268345594','082306928383','085215798456','085357194178','081271885807','085382332211','085368477870','081367197461','085267332696','085377254687','081368021439');

  	$not_in = array_diff($array, $existing);

  	dump($not_in_database, $not_entries_excel, count($array), count($existing), count($not_in)); 
  	dump($not_in);
  }
  public function sandingkan_laporan(){

  	ini_set('max_execution_time', '3600');
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan') AND ! group_has_role('laporan', 'view_own_unit_laporan') AND ! group_has_role('laporan','view_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
    //--------------------------------------------------------------------------------------------------------
		
		// Set Tipe Data
		$id_tipes = array();
		foreach (range(1, 7) as $number) {
		    $id_tipes[] = $number;
		}
		$filter_tipes[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filter_tipes);
		$data['unit_types'] = $this->types_m->get_all_types();
		$memberships_user = $this->memberships_m->get_memberships_by_user($this->current_user->id);
		$type_ids=array();
		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}

		$data['type_ids'] = $type_ids;
		$data['skip_group'] = array('admin','site_admin');
    //--------------------------------------------------------------------------------------------------------


    if($this->input->get('f-tipe_laporan')){

			// Set Tipe Laporan
			$f_tipe_laporan = $this->input->get('f-tipe_laporan');

			$id_tipe_laporan = $this->input->get('f-tipe_laporan');
			$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;
			$gap_type_ids = array(3, 4, 6);
			if(in_array($id_tipe_laporan, $gap_type_ids)){
				$id_tipe_laporan = 1;
			}
			$tti_type_ids = array(5, 7);
			if(in_array($id_tipe_laporan, $tti_type_ids)){
				$id_tipe_laporan = 2;
			}


			$filters['m.id_laporan_tipe'] = $id_tipe_laporan;

			if($this->input->get('f-tipe_laporan') == 3){
				$filters['d.is_ttic'] = 1;
			}else{
				$filters['d.is_ttic'] = '0';
			}

			// if($this->input->get('f-tipe_laporan') > 2){
			// 	$filters['d.id_group'] = 9;
			// }else{
			// 	$filters['d.id_group'] = 7;
			// }

			$val_tipe_laporan = $this->input->get('f-tipe_laporan');

			if($val_tipe_laporan == 1 || $val_tipe_laporan == 2){
				$id_group = 7;
			}
			if($val_tipe_laporan >=3 && $val_tipe_laporan <=5){
				$id_group = 9;
			}
			if($val_tipe_laporan == 6 || $val_tipe_laporan == 7){
				$id_group = 10;
			}
			$filters['d.id_group'] = $id_group;


			// Nama Laporan
			$data['f_tipe_laporan'] = $f_tipe_laporan;
			if($f_tipe_laporan == 3){
				$data['nama_laporan'] = 'Gapoktan tingkat TTIC';
			}else{
				$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($f_tipe_laporan)['nama_laporan'];
			}

			// Set Location
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			if(group_has_role('laporan','view_all_laporan')) {
				
				$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi_entries();

				// Set Location
				$filter_kota = null;
		  	if($this->input->get('f-provinsi') != '') {
		  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
		  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		  	}

	  	}elseif(group_has_role('laporan','view_own_prov_laporan')){
	  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
	  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
	  	}

	  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);

  		if($data['id_provinsi'] != NULL){
  			$filters['lp.id'] = $data['id_provinsi'];
  		}
  		if($data['id_kota'] != NULL){
  			$filters['k.id'] = $data['id_kota'];
  		}

      //--------------------------------------------------------------------------------------------------------

  		// Set Komoditas & Metadata
  		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
			$data['komoditas'] = $this->metadata_m->get_metadata_by_tipe_laporan($id_tipe_laporan, $id_komoditas, 'default_laporan_metadata.*', NULL, 'urutan');

			$filters['m.id_laporan_komoditas'] = $id_komoditas;


			// Set Units to find data from some units.
			$units_id = NULL;
		  if(! group_has_role('laporan', 'view_all_laporan') && !group_has_role('laporan','view_own_prov_laporan')){
        $units_id = user_units2($this->current_user->id);
        $result = $this->units_m->get_child($units_id);
        $units_id = $units_id.$result;
      }

      if($this->input->get('id_organization_unit') > 0){
      	$filters['d.id_unit'] = $this->input->get('id_organization_unit');
			}
      //--------------------------------------------------------------------------------------------------------

	    if(!$this->input->get('page')){
	    	// -------------------------------------
        // Get Organization by Member
        // -------------------------------------

				$data['types'] = $this->types_m->get_all_types();

        $unit_id = $this->unit['id'];
        
        $types = $data['types'];
        $organization = array();
        $addedLevel = array();
        $n2 = 0;
        $n = count($types);

        foreach($types as $i => $type){
          if (in_array($type['level'], $addedLevel)) {
            continue;
          }
          $addedLevel[] = $type['level'];
          $units = $this->units_m->get_unit_by_child($unit_id);

          if(isset($units['id'])){
            if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit_'.$units['type_level']] = $units['id'];
              $n2++;
            } else if ($units['type_slug'] == $this->node_type_slug) {
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit'] = $units['id'];
            }
            $unit_id = $units['id'];
          }
        }

        if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
          if($this->unit['type_slug'] == $this->node_type_slug){
            $organization['id_organization_unit'] = $this->unit['id'];
          }else{
            $organization['id_organization_unit_'.$n2] = $this->unit['id'];
          }
          $organization['organization_name_'.$n2] = $this->unit['name'];
        }

        $data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
        $data['user_unit_level'] = $this->unit['type_level'];
        $data['node_type_level'] = $this->node_type_level;
        $data['node_type_slug'] = $this->node_type_slug;
        $data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

        $get_units = $this->memberships_m->get_units_by_member($this->current_user->id);
	      if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
		      unset($organization['id_organization_unit']);
		      unset($organization['organization_name_1']);
		    }

		    $is_tti17 = false;
		    if(($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 7) && count($get_units) > 1){
		    	$is_tti17 = true;
		    	unset($organization['id_organization_unit']);
		    	unset($organization['organization_name_0']);
		    }

        $data['organization'] = $organization;
				if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
	      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
	      }else{
          if(($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 7) && count($get_units) > 1){
            $data['child_organization'] = $get_units;
          }
      	}
      	$data['is_tti17'] = $is_tti17;

				$data['min_year'] = $this->absen_m->get_min_year(); 
				$data['max_year'] = $this->absen_m->get_max_year();

				// Data Komoditas
				$data['komoditas_related'] = $this->komoditas_m->get_komoditas_related();
      }

			$data['jml_periode'] = ($this->input->get('f-jml_periode')) ? $this->input->get('f-jml_periode') : 1;

			$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
  		if($this->input->post('id_laporan_komoditas')){
  			$id_komoditas = $this->input->post('id_laporan_komoditas');
  		}

  		for ($i=1; $i <= $data['jml_periode'] ; $i++) {
  			unset($filters['tahun']);
  			$tahun = '';
  			$tahun_sd = '';
  			$tahun = ($this->input->get('f-tahun_'.$i) ? $this->input->get('f-tahun_'.$i) : date('Y'));
  			$tahun_sd = ($this->input->get('f-tahun_sd_'.$i) ? $this->input->get('f-tahun_sd_'.$i) : $tahun);
				$data['text']['periode'.$i] = ''.$tahun; 
				$data['text_sd']['periode'.$i] = ''.$tahun_sd; 
  			if($this->input->get('f-tipe_laporan') <= 2){
					$filters['tahun'] = 'a.tahun between '.$tahun.' AND '.$tahun_sd;
				}else{
					$filters['tahun'] = 'YEAR(d.created_on) between '.$tahun.' AND '.$tahun_sd;
				}

				unset($filters['bulan']);
				$bulan = '';
				$bulan_sd = '';
				$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
  			$bulan = $this->input->get('f-bulan_'.$i);
  			$bulan_sd = ($this->input->get('f-bulan_sd_'.$i) ? $this->input->get('f-bulan_sd_'.$i) : $bulan);
  			if($bulan != ''){
  				$data['text']['periode'.$i] .= ' '.$arr_month[$bulan];
  				$data['text_sd']['periode'.$i] .= ' '.$arr_month[$bulan_sd];
  			}
				$bulan = $bulan+0;	
				$bulan_sd = $bulan_sd+0;
				if($bulan > 0){
					if($this->input->get('f-tipe_laporan') <= 2){
						$filters['bulan'] = 'a.bulan between '.$bulan.' AND '.$bulan_sd;
					}else{
						$filters['bulan'] = 'MONTH(d.created_on) between '.$bulan.' AND '.$bulan_sd;
					}
				}	

				unset($filters['minggu_ke']);
				if($bulan > 0 && $tahun > 0 && $this->input->get('f-minggu_ke_'.$i)){
					$minggu_ke = $this->input->get('f-minggu_ke_'.$i);
					$minggu_ke_sd = ($this->input->get('f-minggu_ke_sd_'.$i) ? $this->input->get('f-minggu_ke_sd_'.$i) : $minggu_ke);
	  			$data['text']['periode'.$i] .= ' Minggu ke '.$minggu_ke;
	  			$data['text_sd']['periode'.$i] .= ' Minggu ke '.$minggu_ke_sd;
					if($this->input->get('f-tipe_laporan') <= 2){
						$filters['minggu_ke'] = 'a.minggu_ke between '.$minggu_ke.' AND '.$minggu_ke_sd;
					}else{
						$filters['minggu_ke'] = 'd.minggu_ke between '.$minggu_ke.' AND '.$minggu_ke_sd;
					}
				}
				// dump($filters);
				$data['data']['periode'.$i] = $this->data_m->sandingan($filters);
				// dump($data['data']['periode'.$i]);
				// dump($this->db->last_query());
  		}

  		foreach ($data['komoditas'] as $key => $metadata) {
  			for ($i=1; $i <= $data['jml_periode'] ; $i++) {
  				$metadata_col[$metadata['id']][$i] = '-'; 
  				foreach ($data['data']['periode'.$i] as $key => $data_periode) {
  					if($data_periode['id_metadata'] == $metadata['id']){
  						$metadata_col[$metadata['id']][$i] = number_format($data_periode['total'],0,',','.');
  					}
  				}
  			}
  		}
  		$data['metadata_col'] = $metadata_col;
      //--------------------------------------------------------------------------------------------------------
		}

		if($this->input->get('page')){
			$this->download_sandingan($data, $this->input->get('page'));
		}else{
			$this->template->title(lang('laporan:sandingkan_laporan'))
				->set_breadcrumb('Dasbor', '/admin')
				->set_breadcrumb(lang('laporan:data:plural'), '/admin/laporan/data/index?'.$_SERVER['QUERY_STRING'])
				->set_breadcrumb(lang('laporan:sandingkan_laporan'))
				->build('admin/data_sandingan', $data);
		}
  }

  public function download_sandingan($data, $page){
  	$this->load->library('excel');
  	$this->load->helper('laporan');

  	if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$colspan = ($data['jml_periode'] + 1);

		if($page == 'download'){
			$arr_cols = range('A','Z');

			$master_hrf = $arr_cols;

			$hitung = count($arr_cols);
			foreach ($arr_cols as $key => $value) {
					
			}

			if(count($arr_cols) < $colspan){
				foreach ($arr_cols as $key => $hrf) {
					foreach ($master_hrf as $key2 => $hrf2) {
						if(count($arr_cols) < $colspan){
							$arr_cols[] = $hrf.''.$hrf2;
						}else{
							break;
						}
					}
				}
			}else{
				$arr_cols = array();
				foreach ($master_hrf as $key2 => $hrf2) {
					if(count($arr_cols) < $colspan){
						$arr_cols[] = $hrf2;
					}else{
						break;
					}
				}
			}

			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			foreach (range('A','E') as $columnID) {
				$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
				// $this->excel->getActiveSheet()->getColumnDimension($columnID)->setWidth(15);
			}
			
			$total_cell = count($arr_cols);

	  	$nama_laporan = $data['nama_laporan'];

	  	if($this->input->get('id_organization_unit') > 0){
				$id_unit = $this->input->get('id_organization_unit');
				$nama_unit = $this->units_m->get_units_by_id($id_unit)->unit_name;
				$nama_laporan .= ' "'.$nama_unit.'"';
			}

			$nama_file = 'Data_Sandingan_'.$nama_laporan;

			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle('Laporan Sandingan Periode');

			$this->excel->getActiveSheet()->setCellValue('A1', 'Data Sandingan '.$nama_laporan);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$cols_header = $arr_cols[0].'1:'.$arr_cols[$total_cell-1].'1';
			$this->excel->getActiveSheet()->mergeCells($cols_header);
			// $this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$row = 2;

			if($this->input->get('f-provinsi') != ''){
	    	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	    	$kota = "";
	    	if($this->input->get('f-kota') != ''){
	    		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	    	}

	    	$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].$row;
				$this->excel->getActiveSheet()->mergeCells($cols_header);
				// $this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$row++;
				
				$nama_file .= '_'.$provinsi.$kota;
			}

			$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Metadata');
			for ($i=1; $i <= $data['jml_periode'] ; $i++) {
				$col_i = $arr_cols[$i];
				$this->excel->getActiveSheet()->setCellValue($col_i.$row, $data['text']['periode'.$i].' s/d '.$data['text_sd']['periode'.$i]);
    	}

			
			$this->excel->getActiveSheet()->getStyle('A'.$row.':'.$arr_cols[$total_cell-1].$row)->applyFromArray(
		    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'CCCCCC')
	        ),
		    )
			);

			$total =  $row + count($data['komoditas']);
			$this->excel->getActiveSheet()->getStyle('A'.$row.':'.$arr_cols[$total_cell-1].$total)->applyFromArray(
		    array(
	        'borders' => array(
	          'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	          )
	        ),
		    )
			);

			$no = 1;

			foreach ($data['komoditas'] as $metadata){
  			$row++;
  			$this->excel->getActiveSheet()->setCellValue('A'.$row, $metadata['nama']);
  			for ($i=1; $i <= $data['jml_periode']; $i++) { 
					$col_i = $arr_cols[$i];
					$this->excel->getActiveSheet()->setCellValue($col_i.$row, $data['metadata_col'][$metadata['id']][$i]);
      	}
  		}

  		header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');


		}else{
			$nama_laporan = $data['nama_laporan'];

			if($this->input->get('id_organization_unit') > 0){
				$id_unit = $this->input->get('id_organization_unit');
				$nama_unit = $this->units_m->get_units_by_id($id_unit)->unit_name;
				$nama_laporan .= ' "'.$nama_unit.'"';
			}

			$nama_file = 'Data_Sandingan_'.$nama_laporan;

			$html ="
	    <table border=\"0\" cellpadding=\"5\" width=\"100%\">
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	      <tr>
	          <td colspan=\"".$colspan."\" align=\"center\">
	              <span style=\"font: bold 20px Open Sans; display: block;\">Data Sandingan ".$nama_laporan."</span>
	          </td>
	      </tr>";

	      if($this->input->get('f-provinsi') != ''){
	      	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	      	$kota = "";
	      	if($this->input->get('f-kota') != ''){
	      		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	      	}
					$html .= '
						<tr>
							<td colspan="'.$colspan.'" align="center">
								<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
							</td>
						</tr>
					';
					$nama_file .= '_'.$provinsi.$kota;
				}


	      $html .= "
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	    </table>";


	  	$html .= '
	  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
	  		$html .='
	      <thead style="background-color:#ccc;">
	        <tr>
	        	<th>Metadata</th>';
	        	for ($i=1; $i <= $data['jml_periode'] ; $i++) { 
	        		$html .= '<th>'.$data['text']['periode'.$i].' s/d '.$data['text_sd']['periode'.$i].'</th>';
	        	}
				   	$html .= '
	  			</tr>
	  		</thead>
	  		<tbody>';
	  		foreach ($data['komoditas'] as $metadata){
	  			$html .='
	  				<tr>
	  					<td>'.$metadata['nama'].'</td>';
	  					for ($i=1; $i <= $data['jml_periode']; $i++) { 
              	$html .='<td>'.$data['metadata_col'][$metadata['id']][$i].'</td>';
            	}
					   	$html .= '
	  				</tr>
	  			';
	  		}
	  		$html .='
	  		</tbody>
	  	</table>';

	  	$data['html'] = $html;
	  	$this->load->view('laporan/admin/page_print', $data);
		}
  }

  public function index($id=0)
  {
  	ini_set('max_execution_time', '3600');
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan') AND ! group_has_role('laporan', 'view_own_unit_laporan') AND ! group_has_role('laporan','view_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
    //--------------------------------------------------------------------------------------------------------
		
		// Set Tipe Data
		$id_tipes = array();
		foreach (range(1, 7) as $number) {
		    $id_tipes[] = $number;
		}
		$filters[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filters);
		$memberships_user = $this->memberships_m->get_memberships_by_user($this->current_user->id);
		$type_ids=array();
		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}

		$data['type_ids'] = $type_ids;
		$data['skip_group'] = array('admin','site_admin');
    //--------------------------------------------------------------------------------------------------------

		if($this->input->get('f-tipe_laporan')){

			// Set Tipe Laporan
			$f_tipe_laporan = $this->input->get('f-tipe_laporan');

			$id_tipe_laporan = $this->input->get('f-tipe_laporan');
			$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;

			$gap_type_ids = array(3, 4, 6);
			if(in_array($id_tipe_laporan, $gap_type_ids)){
				$id_tipe_laporan = 1;
			}
			$tti_type_ids = array(5, 7);
			if(in_array($id_tipe_laporan, $tti_type_ids)){
				$id_tipe_laporan = 2;
			}

			$filter_periode1['m.id_laporan_tipe'] = $id_tipe_laporan;
			$filter_periode2['m.id_laporan_tipe'] = $id_tipe_laporan;

			if($this->input->get('f-tipe_laporan') == 3){
				$filter_periode1['d.is_ttic'] = 1;
				$filter_periode2['d.is_ttic'] = 1;
			}else{
				$filter_periode1['d.is_ttic'] = 0;
				$filter_periode2['d.is_ttic'] = 0;
			}

			// if($this->input->get('f-tipe_laporan') > 2){
			// 	$filter_periode1['d.id_group'] = 9;
			// 	$filter_periode2['d.id_group'] = 9;
			// }else{
			// 	$filter_periode1['d.id_group'] = 7;
			// 	$filter_periode2['d.id_group'] = 7;
			// }
			$val_tipe_laporan = $this->input->get('f-tipe_laporan');

			if($val_tipe_laporan == 1 || $val_tipe_laporan == 2){
				$id_group = 7;
			}
			if($val_tipe_laporan >=3 && $val_tipe_laporan <=5){
				$id_group = 9;
			}
			if($val_tipe_laporan == 6 || $val_tipe_laporan == 7){
				$id_group = 10;
			}


			$filter_periode1['d.id_group'] = $id_group;
			$filter_periode2['d.id_group'] = $id_group;
      //--------------------------------------------------------------------------------------------------------

			// Set hari
			$hari = ($this->input->get('f-hari')) ? $this->input->get('f-hari') : date('Y-m-d');

			if($this->input->get('f-hari')){
				// if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode1['a.tanggal'] = $this->input->get('f-hari');
				// }
				// else{
				// 	$filter_periode1['DATE(d.created_on)'] = $this->input->get('f-hari');
				// }
			}
      //--------------------------------------------------------------------------------------------------------

			// Set default periode
			$weeks = getWeeks($hari, 'monday');
			// dump($weeks);
      //--------------------------------------------------------------------------------------------------------

			// Set Periode 1
      //--------------------------------------------------------------------------------------------------------

			// Set Tahun
			$tahun = NULL;
			if($this->input->get('f-tahun')){
				$tahun = $this->input->get('f-tahun');
			}else{
				$tahun = $weeks['tahun'];
			}
			if($this->input->get('f-tipe_laporan') <= 2){
				$filter_periode1['a.tahun'] = $tahun;
			}else{
				$filter_periode1['YEAR(d.created_on)'] = $tahun;
			}
			$data['tahun'] = $tahun;


			$data['min_year'] = $this->absen_m->get_min_year(); 
			$data['max_year'] = $this->absen_m->get_max_year();
      //--------------------------------------------------------------------------------------------------------

			// Set Bulan
			if($this->input->get('f-bulan')){
				$bulan = $this->input->get('f-bulan');
			}else{
				$bulan = $weeks['bulan'];
			}

			// $bulan = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
			$bulan = (strlen($bulan) == 1) ? '0'.$bulan : $bulan;
			$thn_bln = $tahun."-".$bulan;

			if(isset($_GET['f-bulan'])){
				if($_GET['f-bulan'] != ""){
					$bulan = $this->input->get('f-bulan');
				}else{
					$bulan = '';
				}
			}
				
			if($bulan != ''){
				$bulan = $bulan + 0;
				// if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode1['a.bulan'] = $bulan;
				// }else{
				// 	$filter_periode1['MONTH(d.created_on)'] = $bulan;
				// }
			}

			$data['bulan'] = $bulan;
      //--------------------------------------------------------------------------------------------------------

			// Set Jumlah Minggu
			$first_day_of_month = $thn_bln.'-01';

			$count_of_week = get_count_of_week($first_day_of_month);
			$data['count_of_week'] = $count_of_week;
      //--------------------------------------------------------------------------------------------------------

			// Minggu ke
			if($this->input->get('f-minggu_ke')){
				$minggu_ke = $this->input->get('f-minggu_ke');
			}else{
				$minggu_ke = (string) $weeks['week'];
			}

			if(isset($_GET['f-minggu_ke'])){
				if($_GET['f-minggu_ke'] != ""){
					$minggu_ke = $this->input->get('f-minggu_ke');
				}else{
					$minggu_ke = '';
				}
			}

			if($minggu_ke != ''){
				// if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode1['a.minggu_ke'] = $minggu_ke;
				// }else{
				// 	$filter_periode1['d.minggu_ke'] = $minggu_ke;
				// }
			}
			// dump($minggu_ke);
			$data['minggu_ke'] = $minggu_ke;
      //--------------------------------------------------------------------------------------------------------

			// Set Hari
			$data['days'] = array();
			if($f_tipe_laporan > 2){
				$data['weeks'] = $weeks;
				$firstDayOfWeek = $this->data_m->get_hari($tahun, $bulan, $minggu_ke);
			}else{
				$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke);
				$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
			}

			$get_days = $this->absen_m->get_days_by_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
			// dump($this->db->last_query());
			if($firstDayOfWeek){
				$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
				$firstDay = $data['weeks']['first_day_of_week'];
				if($f_tipe_laporan > 2){
					for ($h=0; $h <=6 ; $h++) { 
						$data['days'][] = date('Y-m-d', strtotime('+'.$h.' days', strtotime($firstDay)));
					}
				}else{
					foreach ($get_days as $key => $day) {
						$data['days'][] = $day['tanggal'];
					}
				}
			}

			if($tahun > 2017){
				$data['days'] = array();
				foreach ($get_days as $key => $day) {
					$data['days'][] = $day['tanggal'];
				}
			}

      //--------------------------------------------------------------------------------------------------------

			// Set Header Text Report Periode 1
			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bulan') != '' || $this->input->get('f-tahun') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bulan') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan')];
					}
					if($this->input->get('f-tahun') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-tahun');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$minggu_ke;
				$periode[] = 'Bulan '.$arr_month[($bulan < 10 ? '0'.$bulan : $bulan)];
				$periode[] = 'Tahun '.$tahun;
			}

			if(count($periode) > 0){
				$data['text_periode1'] = 'Periode: '.implode(', ',$periode);
			}
      //--------------------------------------------------------------------------------------------------------

			// Data Komoditas
			$data['komoditas_related'] = $this->komoditas_m->get_komoditas_related();
      //--------------------------------------------------------------------------------------------------------

			// Set Periode 2 
			//--------------------------------------------------------------------------------------------------------

			// Set hari
			$hari = ($this->input->get('f-hari2')) ? $this->input->get('f-hari2') : date('Y-m-d');

			if($this->input->get('f-hari2')){
				// if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode2['a.tanggal'] = $this->input->get('f-hari2');
				// }else{
				// 	$filter_periode2['DATE(d.created_on)'] = $this->input->get('f-hari2');
				// }
			}
      //--------------------------------------------------------------------------------------------------------

			// Set default periode
			$weeks = getWeeks($hari, 'monday');
      //--------------------------------------------------------------------------------------------------------

			// Set Periode 1
      //--------------------------------------------------------------------------------------------------------

			// Set Tahun
			$tahun = NULL;
			if($this->input->get('f-tahun2')){
				$tahun = $this->input->get('f-tahun2');
			}else{
				$tahun = $weeks['tahun'];
			}
			// if($this->input->get('f-tipe_laporan') <= 2){
				$filter_periode2['a.tahun'] = $tahun;
			// }else{
			// 	$filter_periode2['YEAR(d.created_on)'] = $tahun;
			// }
			$data['tahun2'] = $tahun;
      //--------------------------------------------------------------------------------------------------------

			// Set Bulan
			if($this->input->get('f-bulan2')){
				$bulan = $this->input->get('f-bulan2');
			}else{
				$bulan = $weeks['bulan'];
			}

			// $bulan = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
			$bulan = (strlen($bulan) == 1) ? '0'.$bulan : $bulan;
			$thn_bln = $tahun."-".$bulan;

			if(isset($_GET['f-bulan2'])){
				if($_GET['f-bulan'] != ""){
					$bulan = $this->input->get('f-bulan2');
				}else{
					$bulan = '';
				}
			}
				
			if($bulan != ''){
				$bulan = $bulan + 0;
				// if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode2['a.bulan'] = $bulan;
				// }else{
				// 	$filter_periode2['MONTH(d.created_on)'] = $bulan;
				// }
			}

			$data['bulan2'] = $bulan;
      //--------------------------------------------------------------------------------------------------------

			// Set Jumlah Minggu
			$first_day_of_month = $thn_bln.'-01';

			$count_of_week = get_count_of_week($first_day_of_month);
			$data['count_of_week2'] = $count_of_week;
      //--------------------------------------------------------------------------------------------------------

			// Minggu ke
			if($this->input->get('f-minggu_ke2')){
				$minggu_ke = $this->input->get('f-minggu_ke2');
			}else{
				$minggu_ke = (string) $weeks['week'];
			}

			if(isset($_GET['f-minggu_ke2'])){
				if($_GET['f-minggu_ke2'] != ""){
					$minggu_ke = $this->input->get('f-minggu_ke2');
				}else{
					$minggu_ke = '';
				}
			}

			if($minggu_ke != ''){
				// if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode2['a.minggu_ke'] = $minggu_ke;
				// }else{
				// 	$filter_periode2['d.minggu_ke'] = $minggu_ke;
				// }
			}
			$data['minggu_ke2'] = $minggu_ke;
      //--------------------------------------------------------------------------------------------------------

			// Set Hari
			$data['days2'] = array();
			if($f_tipe_laporan > 2){
				$data['weeks2'] = $weeks;
				$firstDayOfWeek = $this->data_m->get_hari($tahun, $bulan, $minggu_ke);
			}else{
				$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke);
				$data['weeks2'] = getWeeks($firstDayOfWeek, 'monday');
			}

			if($firstDayOfWeek){
				$data['weeks2'] = getWeeks($firstDayOfWeek, 'monday');
				$firstDay = $data['weeks2']['first_day_of_week'];
				if($f_tipe_laporan > 2){
					for ($h=0; $h <=6 ; $h++) { 
						$data['days2'][] = date('Y-m-d', strtotime('+'.$h.' days', strtotime($firstDay)));
					}
				}else{
					$get_days = $this->absen_m->get_days_by_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
					foreach ($get_days as $key => $day) {
						$data['days2'][] = $day['tanggal'];
					}
				}
			}
      //--------------------------------------------------------------------------------------------------------

			// Set Header Text Report Periode 2
			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke2') != '' || $this->input->get('f-bulan2') != '' || $this->input->get('f-tahun2') != '' || $this->input->get('f-hari2')){

				if($this->input->get('f-hari2') != ''){
					$periode[] = date_idr($this->input->get('f-hari2'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke2') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke2');
					}
					if($this->input->get('f-bulan2') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan2')];
					}
					if($this->input->get('f-tahun2') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-tahun2');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$minggu_ke;
				$periode[] = 'Bulan '.$arr_month[($bulan < 10 ? '0'.$bulan : $bulan)];
				$periode[] = 'Tahun '.$tahun;
			}

			if(count($periode) > 0){
				$data['text_periode2'] = 'Periode: '.implode(', ',$periode);
			}
      //--------------------------------------------------------------------------------------------------------


			// ---------------------------------------------------------------------------------

			// Nama Laporan
			$data['f_tipe_laporan'] = $f_tipe_laporan;
			if($f_tipe_laporan == 3){
				$data['nama_laporan'] = 'Gapoktan tingkat TTIC';
			}else{
				$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($f_tipe_laporan)['nama_laporan'];
			}

			// Set Location
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			if(group_has_role('laporan','view_all_laporan')) {
				
				$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi_entries();

				// Set Location
				$filter_kota = null;
		  	if($this->input->get('f-provinsi') != '') {
		  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
		  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		  	}

	  	}elseif(group_has_role('laporan','view_own_prov_laporan')){
	  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
	  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
	  	}

	  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);

  		if($data['id_provinsi'] != NULL){
  			$filter_periode1['lp.id'] = $data['id_provinsi'];
  			$filter_periode2['lp.id'] = $data['id_provinsi'];
  		}
  		if($data['id_kota'] != NULL){
  			$filter_periode1['k.id'] = $data['id_kota'];
  			$filter_periode2['k.id'] = $data['id_kota'];
  		}

      //--------------------------------------------------------------------------------------------------------

  		// Set Komoditas & Metadata
  		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
			$data['komoditas'] = $this->metadata_m->get_metadata_by_tipe_laporan($id_tipe_laporan, $id_komoditas, 'default_laporan_metadata.*', NULL, 'urutan');
			$filter_periode1['m.id_laporan_komoditas'] = $id_komoditas;
			$filter_periode2['m.id_laporan_komoditas'] = $id_komoditas;

			$metadata_harga = array();
			$metadata_volume = array();
			foreach ($data['komoditas'] as $i => $metadata) {
				if($metadata['id_laporan_tipe_field'] == 1){
					$metadata_harga[$metadata['id']] = $metadata['nama'].'_'.$metadata['field'];
				}
				if($metadata['id_laporan_tipe_field'] == 2){
					$metadata_volume[$metadata['id']] = $metadata['nama'].'_'.$metadata['field'];
				}
			}
			$data['metadata_harga'] = $metadata_harga;
			$data['metadata_volume'] = $metadata_volume;

      //--------------------------------------------------------------------------------------------------------
		
			// Set Units to find data from some units.
			$units_id = NULL;
		  if(! group_has_role('laporan', 'view_all_laporan') && !group_has_role('laporan','view_own_prov_laporan')){
        $units_id = user_units2($this->current_user->id);
        $result = $this->units_m->get_child($units_id);
        $units_id = $units_id.$result;
      }


      if($this->input->get('id_organization_unit') > 0){
      	$filter_periode1['d.id_unit'] = $this->input->get('id_organization_unit');
      	$filter_periode2['d.id_unit'] = $this->input->get('id_organization_unit');
			}
      //--------------------------------------------------------------------------------------------------------

	    if(!$this->input->get('page')){
	    	// -------------------------------------
        // Get Organization by Member
        // -------------------------------------

				$data['types'] = $this->types_m->get_all_types();

        $unit_id = $this->unit['id'];
        
        $types = $data['types'];
        $organization = array();
        $addedLevel = array();
        $n2 = 0;
        $n = count($types);

        foreach($types as $i => $type){
          if (in_array($type['level'], $addedLevel)) {
            continue;
          }
          $addedLevel[] = $type['level'];
          $units = $this->units_m->get_unit_by_child($unit_id);

          if(isset($units['id'])){
            if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit_'.$units['type_level']] = $units['id'];
              $n2++;
            } else if ($units['type_slug'] == $this->node_type_slug) {
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit'] = $units['id'];
            }
            $unit_id = $units['id'];
          }
        }

        if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
          if($this->unit['type_slug'] == $this->node_type_slug){
            $organization['id_organization_unit'] = $this->unit['id'];
          }else{
            $organization['id_organization_unit_'.$n2] = $this->unit['id'];
          }
          $organization['organization_name_'.$n2] = $this->unit['name'];
        }

        $data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
        $data['user_unit_level'] = $this->unit['type_level'];
        $data['node_type_level'] = $this->node_type_level;
        $data['node_type_slug'] = $this->node_type_slug;
        $data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

        $get_units = $this->memberships_m->get_units_by_member($this->current_user->id);
	      if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
		      unset($organization['id_organization_unit']);
		      unset($organization['organization_name_1']);
		    }

		    $is_tti17 = false;
		    if(($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 7) && count($get_units) > 1){
		    	$is_tti17 = true;
		    	unset($organization['id_organization_unit']);
		    	unset($organization['organization_name_0']);
		    }

        $data['organization'] = $organization;
				if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
	      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
	      }else{
          if(($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 7) && count($get_units) > 1){
            $data['child_organization'] = $get_units;
          }
      	}
      	$data['is_tti17'] = $is_tti17;
			  // -------------------------------------------------------------------------------------------------------------


				$surffix = '';
			  if($_SERVER['QUERY_STRING']){
			    $surffix = '?'.$_SERVER['QUERY_STRING'];
			  }

				// -------------------------------------
				// Pagination
				// -------------------------------------
			  // dump($filter_periode1);
			  // die();
			  $total_rows = $this->data_m->count_of_rekapitulasi_data(NULL, $filter_periode1);
		    $total_rows = count($total_rows);
				$pagination_config['base_url'] = base_url(). 'admin/laporan/data/index';
				$pagination_config['uri_segment'] = 5;
				$pagination_config['suffix'] = '/'.($this->uri->segment(6) ? $this->uri->segment(6) : 0) . $surffix;
				$pagination_config['total_rows'] = $total_rows;
				// $pagination_config['per_page'] = 2;
				$pagination_config['per_page'] = Settings::get('records_per_page');;
				$this->pagination->initialize($pagination_config);
				$data['pagination_config'] = $pagination_config;

				$data['uri'] = $this->get_query_string(5);
				
		    // -------------------------------------
				// Get entries
				// -------------------------------------
				$result_total = $this->data_m->count_of_rekapitulasi_data($pagination_config, $filter_periode1);
				// dump($this->db->last_query());
				$users = array();
				$units = array();
				foreach ($result_total as $k => $d) {
					$users[$d['id_user']] = $d['id_user'];
					$units[$d['id_unit']] = $d['id_unit'];
				}
				$data['data']['entries'] = (count($users) > 0 && count($units) > 0) ? $this->data_m->rekapitulasi_data($data['komoditas'], $filter_periode1, $users, $units, $data['days']) : NULL;
			  $data['data']['total'] = $total_rows;
				$data['data']['pagination'] = $this->pagination->create_links(null,1);
				// die();


				// sandingan entries -------------------------------------

				$data2['total'] = 0;
				if($this->input->get('f-is_sandingan')){

					$total_rows2 = $this->data_m->count_of_rekapitulasi_data(NULL, $filter_periode2);
		    	$total_rows2 = count($total_rows2);
					$pagination_config2['base_url'] = base_url(). 'admin/laporan/data/index/'.($this->uri->segment(5) ? $this->uri->segment(5) : 0);
					$pagination_config2['uri_segment'] = 6;
					$pagination_config2['suffix'] = $surffix;
					$pagination_config2['total_rows'] = $total_rows2;
					// $pagination_config['per_page'] = 2;
					$pagination_config2['per_page'] = Settings::get('records_per_page');;
					$this->pagination->initialize($pagination_config2);
					$data['pagination_config2'] = $pagination_config2;

					// $data['uri'] = $this->get_query_string(5);
					
			    // -------------------------------------
					// Get entries
					// -------------------------------------

					$result_total2 = $this->data_m->count_of_rekapitulasi_data($pagination_config2, $filter_periode2);
					$users2 = array();
					$units2 = array();
					foreach ($result_total2 as $k => $d) {
						$users2[$d['id_user']] = $d['id_user'];
						$units2[$d['id_unit']] = $d['id_unit'];
					}
					$data['data2']['entries'] = (count($users2) > 0 && count($units2) > 0) ? $this->data_m->rekapitulasi_data($data['komoditas'], $filter_periode2, $users2, $units2, $data['days2']) : NULL;
				  $data['data2']['total'] = $total_rows2;
					$data['data2']['pagination'] = $this->pagination->create_links(null,1);

				}

				// -------------------------------------
		    // Build the page. See views/admin/index.php
		    // for the view code.
				// -------------------------------------
				
		  	$this->template->title(lang('laporan:data:plural'))
					->set_breadcrumb('Dasbor', '/admin')
					->set_breadcrumb(lang('laporan:data:plural'))
					->build('admin/data_index', $data);
			
			}else{
				$result_total = $this->data_m->count_of_rekapitulasi_data(NULL, $filter_periode1);
				$users = array();
				$units = array();
				foreach ($result_total as $k => $d) {
					$users[$d['id_user']] = $d['id_user'];
					$units[$d['id_unit']] = $d['id_unit'];
				}
				$data['entries'] = (count($users) > 0 && count($units) > 0) ? $this->data_m->rekapitulasi_data($data['komoditas'], $filter_periode1, $users, $units, $data['days']) : NULL;

				if($this->input->get('f-is_sandingan')){
					$result_total2 = $this->data_m->count_of_rekapitulasi_data(NULL, $filter_periode2);
					$users2 = array();
					$units2 = array();
					foreach ($result_total2 as $k => $d) {
						$users2[$d['id_user']] = $d['id_user'];
						$units2[$d['id_unit']] = $d['id_unit'];
					}
					$data['entries2'] = (count($users2) > 0 && count($units2) > 0) ? $this->data_m->rekapitulasi_data($data['komoditas'], $filter_periode2, $users2, $units2, $data['days2']) : NULL;
				}
				$this->download_laporan_harga($data, $this->input->get('page'));
			}

		}else{

			// -------------------------------------
	    // Build the page. See views/admin/index.php
	    // for the view code.
			// -------------------------------------
			
	    $this->template->title(lang('laporan:data:plural'))
				->set_breadcrumb('Dasbor', '/admin')
				->set_breadcrumb(lang('laporan:data:plural'))
				->build('admin/data_index', $data);
		}
  }
	
	/**
   * Display one data
   *
   * @return  void
   */
  public function view($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_data') AND ! group_has_role('laporan', 'view_own_data')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
    $data['data'] = $this->data_m->get_data_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_data')){
			if($data['data']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:data:view'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:data:plural'), '/admin/laporan/data/index')
			->set_breadcrumb(lang('laporan:data:view'))
			->build('admin/data_entry', $data);
  }
	
	/**
   * Create a new data entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
  public function create()
  {
  	$this->config->load('absen');
  	$report_time_picker = $this->config->item('report_time_picker');
  	$start_report_time_picker = $this->config->item('start_report_time_picker');
  	$end_report_time_picker = $this->config->item('end_report_time_picker');
  	$data['report_time_picker'] = $report_time_picker;
  	$data['start_report_time_picker'] = $start_report_time_picker;
  	$data['end_report_time_picker'] = $end_report_time_picker;

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'create_laporan') && ! group_has_role('laporan', 'create_own_laporan') && ! group_has_role('laporan', 'create_own_unit_laporan') && ! group_has_role('laporan', 'create_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		$data['uri'] = $this->get_query_string(5);

		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_data('new')){	
				$this->session->set_flashdata('success', lang('laporan:data:submit_success'));
				if(group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_laporan') || group_has_role('laporan','view_own_prov_laporan')){
					$redirect = 'admin/laporan/data/index'.$data['uri'];			
				}else{		
					$redirect = 'admin/laporan/data/create'.$data['uri'];	
				}
				redirect($redirect);
			}else{
				$data['messages']['error'] = lang('laporan:data:submit_failure');
			}
		}

		$id_tipes = array();
		foreach (range(1, 7) as $number) {
		    $id_tipes[] = $number;
		}
		$filters[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filters);
		// $data['tipes'] = $this->tipe_m->get_tipe();

		$data['unit_types'] = $this->types_m->get_all_types();
		$memberships_user = $this->memberships_m->get_memberships_by_user($this->current_user->id);
		$type_ids=array();

		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}
		$data['type_ids'] = $type_ids;

		
		if($this->input->get('f-tipe_laporan')){
			
			// Cek Tanggal Pelaporan
			$today = date('Y-m-d H:i:s');

			$dates = $this->absen_m->get_dates();
			$data['addDates'] = (count($dates) > 0) ? implode(',', $dates) : '';

			// Data Komoditas
			$data['komoditas'] = $this->komoditas_m->get_komoditas_related();

			// Tampung data GET
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			$data['id_unit'] = ($this->input->get('f-id_unit')) ? $this->input->get('f-id_unit') : NULL;

			$tipe_laporan = $this->input->get('f-tipe_laporan');
			// untuk yang harian (TTIC, Gapoktan 2017, TTI 2017)
			$gap_type_ids = array(3, 4, 6);
			if(in_array($tipe_laporan, $gap_type_ids)){
				$tipe_laporan = 1;
			}
			$tti_type_ids = array(5, 7);
			if(in_array($tipe_laporan, $tti_type_ids)){
				$tipe_laporan = 2;
			}

			$data['absen'] = $this->absen_m->cek_tgl_input($today, $tipe_laporan);

			if($report_time_picker) {
				// $data['list_dates'] = $this->absen_m->get_past($start_report_time_picker, $end_rep);

				// Set Tipe Laporan
				$f_tipe_laporan = $this->input->get('f-tipe_laporan');

				$id_tipe_laporan = $this->input->get('f-tipe_laporan');
				$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;

				$gap_type_ids = array(3, 4, 6);
				if(in_array($id_tipe_laporan, $gap_type_ids)){
					$id_tipe_laporan = 1;
				}
				$tti_type_ids = array(5, 7);
				if(in_array($id_tipe_laporan, $tti_type_ids)){
					$id_tipe_laporan = 2;
				}
				//--------------------------------------------------------------------------------------------------------

				$hari = ($this->input->post('hari')) ? $this->input->post('hari') : date('Y-m-d');

				if($this->input->post('hari')){
					if($this->input->get('f-tipe_laporan') <= 2){
						$filter_periode1['a.tanggal'] = $this->input->post('hari');
					}else{
						$filter_periode1['DATE(d.created_on)'] = $this->input->post('hari');
					}
				}
	      //--------------------------------------------------------------------------------------------------------

				// Set default periode
				$weeks = getWeeks($hari, 'monday');
				// Set Periode 1
	      //--------------------------------------------------------------------------------------------------------

				// Set Tahun
				$tahun = NULL;
				if($this->input->post('tahun')){
					$tahun = $this->input->post('tahun');
				}else{
					$tahun = $weeks['tahun'];
				}
				if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode1['a.tahun'] = $tahun;
				}else{
					$filter_periode1['YEAR(d.created_on)'] = $tahun;
				}
				$data['tahun'] = $tahun;


				$data['min_year'] = 2018; 
				$data['max_year'] = 2018;
	      //--------------------------------------------------------------------------------------------------------

				// Set Bulan
				if($this->input->post('bulan')){
					$bulan = $this->input->post('bulan');
				}else{
					$bulan = $weeks['bulan'];
				}

				// $bulan = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
				$bulan = (strlen($bulan) == 1) ? '0'.$bulan : $bulan;
				$thn_bln = $tahun."-".$bulan;

				if(isset($_GET['bulan'])){
					if($_GET['bulan'] != ""){
						$bulan = $this->input->post('bulan');
					}else{
						$bulan = '';
					}
				}
					
				if($bulan != ''){
					$bulan = $bulan + 0;
					if($this->input->get('f-tipe_laporan') <= 2){
						$filter_periode1['a.bulan'] = $bulan;
					}else{
						$filter_periode1['MONTH(d.created_on)'] = $bulan;
					}
				}

				$data['bulan'] = $bulan;
	      //--------------------------------------------------------------------------------------------------------

				// Set Jumlah Minggu
				$first_day_of_month = $thn_bln.'-01';

				$count_of_week = get_count_of_week($first_day_of_month);
				$data['count_of_week'] = $count_of_week;
	      //--------------------------------------------------------------------------------------------------------

				// Minggu ke
				if($this->input->post('minggu_ke')){
					$minggu_ke = $this->input->post('minggu_ke');
				}else{
					$minggu_ke = (string) $weeks['week'];
				}

				if(isset($_GET['minggu_ke'])){
					if($_GET['minggu_ke'] != ""){
						$minggu_ke = $this->input->post('minggu_ke');
					}else{
						$minggu_ke = '';
					}
				}

				if($minggu_ke != ''){
					if($this->input->get('f-tipe_laporan') <= 2){
						$filter_periode1['a.minggu_ke'] = $minggu_ke;
					}else{
						$filter_periode1['d.minggu_ke'] = $minggu_ke;
					}
				}
				// dump($minggu_ke);
				$data['minggu_ke'] = $minggu_ke;
	      //--------------------------------------------------------------------------------------------------------

				// Set Hari
				$data['days'] = array();
				if($f_tipe_laporan > 2){
					$data['weeks'] = $weeks;
					$firstDayOfWeek = $this->data_m->get_hari($tahun, $bulan, $minggu_ke);
				}else{
					$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke);
					$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
				}

				$get_days = $this->absen_m->get_days_by_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
				// dump($this->db->last_query());
				if($firstDayOfWeek){
					$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
					$firstDay = $data['weeks']['first_day_of_week'];
					if($f_tipe_laporan > 2){
						for ($h=0; $h <=6 ; $h++) { 
							$data['days'][] = date('Y-m-d', strtotime('+'.$h.' days', strtotime($firstDay)));
						}
					}else{
						foreach ($get_days as $key => $day) {
							$data['days'][] = $day['tanggal'];
						}
					}
				}

				if($tahun > 2017){
					$data['days'] = array();
					foreach ($get_days as $key => $day) {
						$data['days'][] = $day['tanggal'];
					}
				}
			}

			// dump($this->db->last_query());
			// $data['absen'] = array();
			if(count($data['absen']) > 0 || $report_time_picker) {
				$data['tipe_laporan'] = $tipe_laporan;
				$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($tipe_laporan)['nama_laporan'];

				if(group_has_role('laporan','create_laporan')) {
					
					// Load location
					$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
					
					$filter_kota = NULL;
			  	if($this->input->post('provinsi') != ''){
			  		$data['id_provinsi'] = $this->input->post('provinsi');
			  	}

		  		if($this->input->post('kota') != ''){
			  		$data['id_kota'] = $this->input->post('kota');
			  	}

			  }elseif(group_has_role('laporan','create_own_prov_laporan')){
		  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
		  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
		  	}

		  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
	  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);

	  		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
	  		if($this->input->post('id_laporan_komoditas')){
	  			$id_komoditas = $this->input->post('id_laporan_komoditas');
	  		}
			  $data['metadata'] = $this->metadata_m->get_metadata_by_tipe_laporan($tipe_laporan, $id_komoditas, 'default_laporan_metadata.*',NULL,'urutan');

				$data['mode'] = 'new';
				$data['return'] = 'admin/laporan/data/index'.$data['uri'];
			}

			// -------------------------------------
      // Get Organization by Member
      // -------------------------------------

      $data['types'] = $this->types_m->get_all_types();

      $unit_id = $this->unit['id'];
      
      $types = $data['types'];
      $organization = array();
      $addedLevel = array();
      $n2 = 0;
      $n = count($types);

      foreach($types as $i => $type){
        if (in_array($type['level'], $addedLevel)) {
          continue;
        }
        $addedLevel[] = $type['level'];
        $units = $this->units_m->get_unit_by_child($unit_id);

        if(isset($units['id'])){
          if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
            $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
            $organization['id_organization_unit_'.$units['type_level']] = $units['id'];
            $n2++;
          } else if ($units['type_slug'] == $this->node_type_slug) {
            $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
            $organization['id_organization_unit'] = $units['id'];
          }
          $unit_id = $units['id'];
        }
      }

      if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){    
        if($this->unit['type_slug'] == $this->node_type_slug){
        	$organization['id_organization_unit'] = $this->unit['id'];
        }else{
          $organization['id_organization_unit_'.$n2] = $this->unit['id'];
        }
        $organization['organization_name_'.$n2] = $this->unit['name'];
      }

      $get_units = $this->memberships_m->get_units_by_member($this->current_user->id);
      if(count($get_units) > 1 && ($this->input->get('f-tipe_laporan') == 2 || $this->input->get('f-tipe_laporan') == 3)){
	      unset($organization['id_organization_unit']);
	      unset($organization['organization_name_1']);
	    }

	    $is_tti17 = false;
	    if(($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 7) && count($get_units) > 1){
	    	$is_tti17 = true;
	    	unset($organization['id_organization_unit']);
	    	unset($organization['organization_name_0']);
	    }

      // khusus sitoni
      $data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
      //---------------------------------

      $data['node_type_slug'] = $this->node_type_slug;
      $data['organization'] = $organization;
      
      $group_id = $this->session->userdata('group_id');
      $data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0, $data['id_provinsi'], $data['id_kota']) : $this->units_m->get_units_by_parent($this->unit['id'], 1, $group_id);

      if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1, $group_id);
      }else{
      	if(($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 7) && count($get_units) > 1){
      		$data['child_organization'] = $get_units;
      	}
      }

      $data['is_tti17'] = $is_tti17;

		}

		$data['title'] = lang('laporan:data:komoditas:new');
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title($data['title'])
			->append_js('module::jquery-ui.multidatespicker.js')
    	->append_css('module::mdp.css')
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb($data['title'])
			->build('admin/data_form', $data);
  }
	
	/**
   * Edit a data entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the data to the be deleted.
   * @return	void
   */
  public function edit($id_unit = 0, $id_user = 0, $tgl_or_id_absen = 0, $id_laporan_tipe, $id_laporan_komoditas = 1)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'edit_all_laporan') AND ! group_has_role('laporan', 'edit_own_laporan') AND ! group_has_role('laporan','edit_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		$data['uri'] = $this->get_query_string(10);

		if($_POST){
			if($this->_update_data('edit')){	
				$this->session->set_flashdata('success', lang('laporan:data:submit_success'));				
				redirect('admin/laporan/data/daftar_laporan'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('laporan:data:submit_failure');
			}
		}
		
		$data['mode'] = 'edit';
		$data['return'] = 'admin/laporan/data/daftar_laporan'.$data['uri'];

		$data['id_unit'] = $id_unit;
		$data['id_user'] = $id_user;
		$data['id_laporan_tipe'] = $id_laporan_tipe;
		$data['id_laporan_komoditas'] = $id_laporan_komoditas;

		if($this->input->get('f-tipe_laporan') <= 2){
			$data['id_absen'] = $tgl_or_id_absen;
			$filters['d.id_absen'] = $tgl_or_id_absen;
		}else{
			$data['tanggal'] = $tgl_or_id_absen;
			$filters['DATE(d.created_on)'] = $tgl_or_id_absen;
		}
		$filters['d.id_unit'] = $id_unit;
		$filters['d.id_user'] = $id_user;
		$filters['m.id_laporan_tipe'] = $id_laporan_tipe;
		// $filters['m.id_laporan_komoditas'] = $id_laporan_komoditas;
		
		$data['metadata'] = $this->data_m->get_input_laporan_by_id($filters);

		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:data:edit'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:daftar_laporan'), '/admin/laporan/data/daftar_laporan')
			->set_breadcrumb(lang('laporan:data:edit'))
			->build('admin/edit_laporan', $data);
  }
	
	/**
   * Delete a data entry
   * 
   * @param   int [$id] The id of data to be deleted
   * @return  void
   */
  public function delete($id_unit, $id_user, $tgl_or_id_absen, $id_laporan_tipe, $id_laporan_komoditas)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		// if(! group_has_role('laporan', 'delete_all_data') AND ! group_has_role('laporan', 'delete_own_data')){
		// 	$this->session->set_flashdata('error', lang('cp:access_denied'));
		// 	redirect('admin');
		// }
		// Check view all/own permission
		// if(! group_has_role('laporan', 'delete_all_data')){
		// 	$entry = $this->data_m->get_data_by_id($id);
		// 	$created_by_user_id = $entry['created_by'];
		// 	if($created_by_user_id != $this->current_user->id){
		// 		$this->session->set_flashdata('error', lang('cp:access_denied'));
		// 		redirect('admin');
		// 	}
		// }
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		$data['uri'] = $this->get_query_string(10);

		if($this->input->get('f-tipe_laporan') < 3){
			$wheres['id_absen'] = $tgl_or_id_absen;
		}else{
			$wheres['date(created_on)'] = $tgl_or_id_absen;
		}

		$is_ttic = ($this->input->get('f-tipe_laporan') == 3) ? 1 : 0;
		$wheres['id_unit'] = $id_unit;
		$wheres['id_user'] = $id_user;
		$wheres['is_ttic'] = $is_ttic;

		$this->data_m->delete_data_input_by_user($id_laporan_tipe, $id_laporan_komoditas, $wheres);
		// dump($this->db->last_query());
		// die();
    $this->session->set_flashdata('error', lang('laporan:data:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
    redirect('admin/laporan/data/daftar_laporan'.$data['uri']);
  }
	
	/**
   * Insert or update data entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_data($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		if($method == 'new'){
			
			$values = $this->input->post();
			if($values['tipe_laporan'] == 3){
				$nama_laporan = $this->tipe_m->get_tipe_by_id(4)['nama_laporan'];
			}else{
				$nama_laporan = $this->tipe_m->get_tipe_by_id($values['tipe_laporan'])['nama_laporan'];
			}
			$val_tipe_laporan = $values['tipe_laporan'];
			$tipe_laporan = $this->input->post('tipe_laporan');

			// $minggu_ke = NULL;
			// if($tipe_laporan > 2){
				$weeks = getWeeks(date('Y-m-d'), 'monday');
				$minggu_ke = $weeks['week'];
			// }
			// untuk yang harian (TTIC, Gapoktan 2017, TTI 2017)
			$is_ttic = ($tipe_laporan == 3) ? 1 : 0;
			
			$gap_type_ids = array(3, 4, 6);
			if(in_array($tipe_laporan, $gap_type_ids)){
				$tipe_laporan = 1;
			}
			$tti_type_ids = array(5, 7);
			if(in_array($tipe_laporan, $tti_type_ids)){
				$tipe_laporan = 2;
			}

			$metadata =$this->metadata_m->get_metadata_by_tipe_laporan($tipe_laporan, $values['id_laporan_komoditas'], 'default_laporan_metadata.*',NULL,'urutan');

			if(group_has_role('laporan','create_laporan')){
				$this->form_validation->set_rules('provinsi', lang('location:provinsi:plural'), 'required');
				$this->form_validation->set_rules('kota', lang('location:kota:plural'), 'required');
			}
			if($values['id_organization_unit'] < 0){
				$this->form_validation->set_rules('id_organization_unit2', $nama_laporan, 'required');
			}


	  	$this->config->load('absen');
	  	$report_time_picker = $this->config->item('report_time_picker');
			if($report_time_picker){
				$this->form_validation->set_rules('tahun','Tahun','required');
				$this->form_validation->set_rules('bulan','Bulan','required');
				$this->form_validation->set_rules('minggu_ke','Minggu ke','required');
				$this->form_validation->set_rules('hari','Hari','required');
			}
		}else{

			$id_unit = $this->input->post('id_unit');
			$id_user = $this->input->post('id_user');
			$id_laporan_tipe = $this->input->post('id_laporan_tipe');
			$id_laporan_komoditas = $this->input->post('id_laporan_komoditas');
			// if($this->input->get('f-tipe_laporan') <= 2){
				$id_absen = $this->input->post('id_absen');
				$filters['d.id_absen'] = $id_absen;
			// }else{
				// $tanggal = $this->input->post('tanggal');
				// $data['tanggal'] = $tanggal;
				// $filters['DATE(d.created_on)'] = $tanggal;
			// }

			$filters['d.id_unit'] = $id_unit;
			$filters['d.id_user'] = $id_user;
			$filters['m.id_laporan_tipe'] = $id_laporan_tipe;
			$filters['m.id_laporan_komoditas'] = $id_laporan_komoditas;
			$metadata = $this->data_m->get_input_laporan_by_id($filters);
		}

		foreach ($metadata as $key=> $data){
			$nilai_min = ($data['nilai_minimal'] != "") ? $data['nilai_minimal'] : 0;
			$nilai_max = ($data['nilai_maksimal'] != "") ? $data['nilai_maksimal'] : 1000000000;
			$this->form_validation->set_rules($data['field'], $data['nama'], 'required|is_natural|callback_check_equal_greater['.$nilai_min.']|callback_check_equal_less['.$nilai_max.']');
		}
	
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				if($report_time_picker){
					$get_id_absen = $this->absen_m->get_id_by_tgl($values['hari'],$tipe_laporan);
					$values['id_absen'] = $get_id_absen['id'];
					$time = date('H:i:s');
					$values['created_on'] = $values['hari'].' '.$time;
					$minggu_ke = $values['minggu_ke'];
					unset($values['tahun'],$values['bulan'],$values['minggu_ke'],$values['hari']);
				}
				// dump($values);
				// die();
				$wheres['id_unit'] = $values['id_organization_unit'];
				$wheres['id_user'] = $values['id_user'];
				$wheres['is_ttic'] = $is_ttic;
				// if($val_tipe_laporan < 3){
					$wheres['id_absen'] = $values['id_absen'];
				// }else{
					// $wheres['date(created_on)'] = date('Y-m-d');
				// }
				$cek_input = $this->data_m->get_data_input_by_user($tipe_laporan, $values['id_laporan_komoditas'], $wheres);
				if(count($cek_input) > 0){
					$this->data_m->delete_data_input_by_user($tipe_laporan, $values['id_laporan_komoditas'], $wheres);
				}

				if($val_tipe_laporan == 1 || $val_tipe_laporan == 2){
					$id_group = 7;
				}
				if($val_tipe_laporan >=3 && $val_tipe_laporan <=5){
					$id_group = 9;
				}
				if($val_tipe_laporan == 6 || $val_tipe_laporan == 7){
					$id_group = 10;
				}

				foreach ($metadata as $key => $data) {
					$data_insert['value'] = $this->input->post($data['field']);
					$data_insert['channel'] = 'web';
					$data_insert['id_metadata'] = $data['id'];
					// if($val_tipe_laporan < 3){
						$data_insert['id_absen'] = $values['id_absen'];
					// }else{
						$data_insert['minggu_ke'] = $minggu_ke;
					// }
					$data_insert['id_user'] = $values['id_user'];
					$data_insert['id_unit'] = $values['id_organization_unit'];
					$data_insert['id_group'] = $id_group;
					$data_insert['is_ttic'] = $is_ttic;
					if($report_time_picker){
						$data_insert['created_on'] = $values['created_on'];
					}
					$this->data_m->insert_data($data_insert);
				}
				$result = true;
			}
			else
			{
				// if($this->input->get('f-tipe_laporan') <= 2){
					$wheres['id_absen'] = $id_absen;
				// }else{
					// $wheres['DATE(created_on)'] = $tanggal;
				// }
				$wheres['id_user'] = $id_user;
				$wheres['id_unit'] = $id_unit;
				foreach ($metadata as $key => $data) {
					if($this->input->post($data['field']) != $data['value']){
						$wheres['id_metadata'] = $data['id'];
						$values['value'] = $this->input->post($data['field']);
						$this->data_m->update_data($values, $wheres);
					}
				}
				$result = true;
			}
		}
		
		return $result;
	}

	public function ajax_get_minggu_ke($year = null, $month = null){
    if($year != NULL && $month != NULL){
			$date = $year.'-'.$month.'-01';
			$end_week = get_count_of_week($date);
			$romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
			// echo '<option value="">-- '.lang('laporan:minggu_ke').' --</option>';
			for ($i=1;$i<=$end_week;$i++) {
				echo '<option value="'.$i.'">'.$romawi[$i].'</option>';
			}
		}else{
			echo '<option value="">'.lang('global:select-none').'</option>';
		}
	}

	public function ajax_get_unit_by_kota($kota, $unit_type){
		$content = $this->organization->get_units_by_kota($kota, $unit_type);
		echo $content;
	}

	public function ajax_get_tti_by($kota, $unit_type){
		$content = $this->organization->get_units_by_kota($kota, $unit_type);
		echo $content;
	}

	public function check_equal_less($value,$max_value)
  {
    if ($value > $max_value)
    {
      $this->form_validation->set_message('check_equal_less', 'Nilai maksimal <b>%s</b> adalah '.$max_value);
      return false;       
    }
    else
    {
      return true;
    }
  }
	
	public function check_equal_greater($value,$min_value)
  {
    if ($value < $min_value)
    {
      $this->form_validation->set_message('check_equal_greater', 'Nilai minimal <b>%s</b> adalah '.$min_value);
      return false;       
    }
    else
    {
      return true;
    }
  }

	public function get_query_string($page){
		// -------------------------------------
		// Get query string
		// -------------------------------------

		$page = $this->uri->segment($page);
		$uri = '';
		$other_post = '';
		if($this->input->post('id_laporan_komoditas')){
			$other_post .= '&f-komoditas='.$this->input->post('id_laporan_komoditas');
		}
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'] . $other_post;
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'] . $other_post;
    }
    
    return $uri;
	}

	public function insert_data_dummy(){
		$id_user = 115;
		$nama_enum = "Pedagang Eceran";
		$query = $this->db->query("select * from default_laporan_absen where date_format(tanggal, '%Y') = '2015'");
		$absens = $query->result_array();
		$metadata =$this->metadata_m->get_metadata_by_enum($nama_enum);
		foreach($absens as $absen){
			$id_absen = $absen['id'];
			$tgl = $absen['tanggal']." 00:00:00";
			$value = intval(rand(1,9)."000");

			$cek_absen = $this->data_m->get_data_by_absen($id_user, $id_absen, $nama_enum);
			if(count($cek_absen) > 0){
				$this->data_m->delete_data_by_absen($id_user, $id_absen, $nama_enum);
			}
			foreach ($metadata as $key => $data) {
				$id_metadata = $data['id'];
				$query_insert = "INSERT INTO `default_laporan_data` (`value`, `channel`, `id_metadata`, `id_absen`, `id_user`, `created_on`) VALUES ('$value', 'web', '$id_metadata', '$id_absen', '$id_user', '$tgl')";

				$this->db->query($query_insert);
			}
		}
	}

	public function daftar_laporan()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan') AND ! group_has_role('laporan','view_own_unit_laporan') AND ! group_has_role('laporan','view_own_prov_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// Set Tipe Data
		$id_tipes = array();
		foreach (range(1, 7) as $number) {
		    $id_tipes[] = $number;
		}
		$filters[] = array('column'=>'id','function'=>'where_in','value'=>$id_tipes);
		$data['tipes'] = $this->tipe_m->get_all_tipe($filters);
		$data['unit_types'] = $this->types_m->get_all_types();
		$memberships_user = $this->memberships_m->get_memberships_by_user($this->current_user->id);
		$type_ids=array();
		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}

		$data['type_ids'] = $type_ids;
		$data['skip_group'] = array('admin','site_admin');
    //--------------------------------------------------------------------------------------------------------

		if($this->input->get('f-tipe_laporan')){


			// Set Tipe Laporan
			$f_tipe_laporan = $this->input->get('f-tipe_laporan');

			$id_tipe_laporan = $this->input->get('f-tipe_laporan');
			$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;
			$gap_type_ids = array(3, 4, 6);
			if(in_array($id_tipe_laporan, $gap_type_ids)){
				$id_tipe_laporan = 1;
			}
			$tti_type_ids = array(5, 7);
			if(in_array($id_tipe_laporan, $tti_type_ids)){
				$id_tipe_laporan = 2;
			}

			$filter_periode['m.id_laporan_tipe'] = $id_tipe_laporan;

			if($this->input->get('f-tipe_laporan') == 3){
				$filter_periode['d.is_ttic'] = 1;
			}else{
				$filter_periode['d.is_ttic'] = 0;
			}

			$val_tipe_laporan = $this->input->get('f-tipe_laporan');

			if($val_tipe_laporan == 1 || $val_tipe_laporan == 2){
				$id_group = 7;
			}
			if($val_tipe_laporan >=3 && $val_tipe_laporan <=5){
				$id_group = 9;
			}
			if($val_tipe_laporan == 6 || $val_tipe_laporan == 7){
				$id_group = 10;
			}


			$filter_periode['d.id_group'] = $id_group;

      //--------------------------------------------------------------------------------------------------------

			// Set hari
			$hari = ($this->input->get('f-hari')) ? $this->input->get('f-hari') : date('Y-m-d');

      //--------------------------------------------------------------------------------------------------------

			// Set default periode
			$weeks = getWeeks($hari, 'monday');
      //--------------------------------------------------------------------------------------------------------

			// Set Periode 1
      //--------------------------------------------------------------------------------------------------------

			// Set Tahun
			$tahun = NULL;
			if($this->input->get('f-tahun')){
				$tahun = $this->input->get('f-tahun');
			}else{
				$tahun = $weeks['tahun'];
			}
			if($this->input->get('f-tipe_laporan') <= 2){
				$filter_periode['a.tahun'] = $tahun;
			}else{
				$filter_periode['YEAR(d.created_on)'] = $tahun;
			}
			$data['tahun'] = $tahun;


			$data['min_year'] = $this->absen_m->get_min_year(); 
			$data['max_year'] = $this->absen_m->get_max_year();
      //--------------------------------------------------------------------------------------------------------

			// Set Bulan
			if($this->input->get('f-bulan')){
				$bulan = $this->input->get('f-bulan');
			}else{
				$bulan = $weeks['bulan'];
			}

			// $bulan = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
			$bulan = (strlen($bulan) == 1) ? '0'.$bulan : $bulan;
			$thn_bln = $tahun."-".$bulan;

			if(isset($_GET['f-bulan'])){
				if($_GET['f-bulan'] != ""){
					$bulan = $this->input->get('f-bulan');
				}else{
					$bulan = '';
				}
			}
				
			if($bulan != ''){
				$bulan = $bulan + 0;
				if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode['a.bulan'] = $bulan;
				}else{
					$filter_periode['MONTH(d.created_on)'] = $bulan;
				}
			}

			$data['bulan'] = $bulan;
      //--------------------------------------------------------------------------------------------------------

			// Set Jumlah Minggu
			$first_day_of_month = $thn_bln.'-01';

			$count_of_week = get_count_of_week($first_day_of_month);
			$data['count_of_week'] = $count_of_week;
      //--------------------------------------------------------------------------------------------------------

			// Minggu ke
			if($this->input->get('f-minggu_ke')){
				$minggu_ke = $this->input->get('f-minggu_ke');
			}else{
				$minggu_ke = (string) $weeks['week'];
			}

			if(isset($_GET['f-minggu_ke'])){
				if($_GET['f-minggu_ke'] != ""){
					$minggu_ke = $this->input->get('f-minggu_ke');
				}else{
					$minggu_ke = '';
				}
			}

			if($minggu_ke != ''){
				if($this->input->get('f-tipe_laporan') <= 2){
					$filter_periode['a.minggu_ke'] = $minggu_ke;
				}else{
					$filter_periode['d.minggu_ke'] = $minggu_ke;
				}
			}
			$data['minggu_ke'] = $minggu_ke;
      //--------------------------------------------------------------------------------------------------------

			// Set Hari
			$data['days'] = array();
			if($f_tipe_laporan > 2){
				$data['weeks'] = $weeks;
				$firstDayOfWeek = $this->data_m->get_hari($tahun, $bulan, $minggu_ke);
			}else{
				$firstDayOfWeek = $this->absen_m->get_first_day_of_week($tahun, $bulan, $minggu_ke);
				$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
			}

			if($firstDayOfWeek){
				$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
				$firstDay = $data['weeks']['first_day_of_week'];
				$lastDay = $data['weeks']['last_day_of_week'];
				if($f_tipe_laporan > 2){
					$filter_periode["DATE(d.created_on)>="] = $firstDay;
					$filter_periode["DATE(d.created_on)<="] = $lastDay;
					for ($h=0; $h <=6 ; $h++) { 
						$data['days'][] = date('Y-m-d', strtotime('+'.$h.' days', strtotime($firstDay)));
					}
				}else{
					$get_days = $this->absen_m->get_days_by_week($tahun, $bulan, $minggu_ke, $id_tipe_laporan);
					foreach ($get_days as $key => $day) {
						$data['days'][] = $day['tanggal'];
					}
					$filter_periode["DATE(d.created_on)>="] = $get_days[0]['tanggal_awal'];
					$filter_periode["DATE(d.created_on)<="] = $get_days[0]['tanggal_akhir'];
				}
			}
			
			$data['firstDayOfWeek'] = $firstDayOfWeek;
      //--------------------------------------------------------------------------------------------------------

			// Set Header Text Report Periode 1
			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bulan') != '' || $this->input->get('f-tahun') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bulan') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bulan')];
					}
					if($this->input->get('f-tahun') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-tahun');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$minggu_ke;
				$periode[] = 'Bulan '.$arr_month[($bulan < 10 ? '0'.$bulan : $bulan)];
				$periode[] = 'Tahun '.$tahun;
			}

			if(count($periode) > 0){
				$data['text_periode'] = 'Periode: '.implode(', ',$periode);
			}
      //--------------------------------------------------------------------------------------------------------

			if($this->input->get('f-tipe_laporan') <= 2){
	      $today = date('Y-m-d H:i:s');
				$data['tglOrIdAbsen'] = NULL;
				$cek_absen = $this->absen_m->get_absen_by_date($today, $f_tipe_laporan);
				if(count($cek_absen) > 1){
					$data['tglOrIdAbsen'] = $cek_absen['id'];
				}
			}else{
				$data['tglOrIdAbsen'] = date('Y-m-d');
			}

			// Data Komoditas
			$data['komoditas_related'] = $this->komoditas_m->get_komoditas_related();
      //--------------------------------------------------------------------------------------------------------


			// ---------------------------------------------------------------------------------

			// Nama Laporan
			$data['f_tipe_laporan'] = $f_tipe_laporan;if($f_tipe_laporan == 3){
				$data['nama_laporan'] = 'Gapoktan tingkat TTIC';
			}else{
				$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($f_tipe_laporan)['nama_laporan'];
			}

			// Set Location
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
			if(group_has_role('laporan','view_all_laporan')) {
				
				$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

				// Set Location
				$filter_kota = null;
		  	if($this->input->get('f-provinsi') != '') {
		  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
		  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		  	}

	  	}elseif(group_has_role('laporan','view_own_prov_laporan')){
	  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
	  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
	  	}

	  	$filter_kota['id_provinsi'] = $data['id_provinsi'];
  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);

  		if($data['id_provinsi'] != NULL){
  			$filter_periode['lp.id'] = $data['id_provinsi'];
  		}
  		if($data['id_kota'] != NULL){
  			$filter_periode['k.id'] = $data['id_kota'];
  		}

      //--------------------------------------------------------------------------------------------------------

  		// Set Komoditas & Metadata
  		$id_komoditas = ($this->input->get('f-komoditas') ? $this->input->get('f-komoditas') : get_first_komoditas('id'));
  		$filter_periode['m.id_laporan_komoditas'] = $id_komoditas;


      //--------------------------------------------------------------------------------------------------------
		
			// Set Units to find data from some units.
			$units_id = NULL;
		  if(! group_has_role('laporan', 'view_all_laporan') && !group_has_role('laporan','view_own_prov_laporan')){
        $units_id = user_units2($this->current_user->id);
        $result = $this->units_m->get_child($units_id);
        $units_id = $units_id.$result;
      }
      //--------------------------------------------------------------------------------------------------------

	    if(!$this->input->get('page')){

		    // -------------------------------------
        // Get Organization by Member
        // -------------------------------------

				$data['types'] = $this->types_m->get_all_types();

        $unit_id = $this->unit['id'];
        
        $types = $data['types'];
        $organization = array();
        $addedLevel = array();
        $n2 = 0;
        $n = count($types);

        foreach($types as $i => $type){
          if (in_array($type['level'], $addedLevel)) {
            continue;
          }
          $addedLevel[] = $type['level'];
          $units = $this->units_m->get_unit_by_child($unit_id);

          if(isset($units['id'])){
            if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit_'.$units['type_level']] = $units['id'];
              $n2++;
            } else if ($units['type_slug'] == $this->node_type_slug) {
              $organization['organization_name_'.$units['type_level']] = $units['unit_name'];
              $organization['id_organization_unit'] = $units['id'];
            }
            $unit_id = $units['id'];
          }
        }

        if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
          if($this->unit['type_slug'] == $this->node_type_slug){
            $organization['id_organization_unit'] = $this->unit['id'];
          }else{
            $organization['id_organization_unit_'.$n2] = $this->unit['id'];
          }
          $organization['organization_name_'.$n2] = $this->unit['name'];
        }

        $data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
        $data['user_unit_level'] = $this->unit['type_level'];
        $data['node_type_level'] = $this->node_type_level;
        $data['node_type_slug'] = $this->node_type_slug;
        $data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

        $get_units = $this->memberships_m->get_units_by_member($this->current_user->id);
	      if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
		      unset($organization['id_organization_unit']);
		      unset($organization['organization_name_1']);
		    }

		    $is_tti17 = false;
		    if(($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 7) && count($get_units) > 1){
		    	$is_tti17 = true;
		    	unset($organization['id_organization_unit']);
		    	unset($organization['organization_name_0']);
		    }

        $data['organization'] = $organization;
				if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
	      	$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
	      }else{
          if(($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 7) && count($get_units) > 1){
            $data['child_organization'] = $get_units;
          }
    		}

	      if($this->input->get('id_organization_unit') > 0){
	      	$filter_periode['d.id_unit'] = $this->input->get('id_organization_unit');
				}
			  // -------------------------------------------------------------------------------------------------------------


				if($firstDayOfWeek != NULL) {

		    	$surffix = '';
			    if($_SERVER['QUERY_STRING']){
			      $surffix = '?'.$_SERVER['QUERY_STRING'];
			    }

					// -------------------------------------
					// Pagination
					// -------------------------------------

			  	$count_input = $this->data_m->get_input_laporan(NULL, 1, $filter_periode);
					$pagination_config['base_url'] = base_url(). 'admin/laporan/data/daftar_laporan';
					$pagination_config['uri_segment'] = 5;
					$pagination_config['suffix'] = $surffix;
					$pagination_config['total_rows'] = $count_input;
					// $pagination_config['per_page'] = 2;
					$pagination_config['per_page'] = Settings::get('records_per_page');
					$this->pagination->initialize($pagination_config);
					$data['pagination_config'] = $pagination_config;
					
			    // -------------------------------------
					// Get entries
					// -------------------------------------
					// 
					
					
			    $data['laporan_data']['entries'] = $this->data_m->get_input_laporan($pagination_config, 0, $filter_periode);
					$data['laporan_data']['total'] = $count_input;
					$data['laporan_data']['pagination'] = $this->pagination->create_links();

					$data['uri'] = $this->get_query_string(5);
				}

				// -------------------------------------
		    // Build the page. See views/admin/index.php
		    // for the view code.
				// -------------------------------------
			
		    $this->template->title(lang('laporan:daftar_laporan'))
					->set_breadcrumb('Dasbor', '/admin')
					->set_breadcrumb(lang('laporan:daftar_laporan'))
					->build('admin/laporan_input', $data);
			}else{
				$data['laporan_data']['entries'] = $this->data_m->get_input_laporan(NULL, 0, $filter_periode);
				$this->download_input_laporan($data, $this->input->get('page'));
			}
		}else{

			// -------------------------------------
	    // Build the page. See views/admin/index.php
	    // for the view code.
			// -------------------------------------
			
	    $this->template->title(lang('laporan:daftar_laporan'))
				->set_breadcrumb('Dasbor', '/admin')
				->set_breadcrumb(lang('laporan:daftar_laporan'))
				->build('admin/laporan_input', $data);
		}
  }

  public function download_laporan_harga($data, $page){
  	$this->load->library('excel');
  	$this->load->helper('laporan');

  	if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$jml_komoditas = count($data['komoditas']);

		$minggu_ke = $data['minggu_ke'];
		$minggu_ke2 = $data['minggu_ke2'];

		
  	if($page == 'download'){

  		$col = 5;
			$col += count($data['metadata_harga']);
			$col += count($data['metadata_volume']);

  		if(!$this->input->get('f-hari') && $minggu_ke != ''){
				foreach ($data['komoditas'] as $key => $komoditas) {
					$col += count($data['days']);
				}
			}else{
				$col += $jml_komoditas;
			}
			$colspan = $col;

  		$rowspan = (!$this->input->get('f-hari') && $minggu_ke != '' ? 'rowspan="3"' : 'rowspan="2"');
			$rowspan2 = (!$this->input->get('f-hari') && $minggu_ke != '' ? 'rowspan="2"' : '');
			$colspan_metadata = (!$this->input->get('f-hari') && $minggu_ke != '') ? 'colspan="'.count($data['days']).'"' : '';

			$arr_cols = range('A','Z');

			$master_hrf = $arr_cols;

			$hitung = count($arr_cols);
			foreach ($arr_cols as $key => $value) {
					
			}

			if(count($arr_cols) < $colspan){
				foreach ($arr_cols as $key => $hrf) {
					foreach ($master_hrf as $key2 => $hrf2) {
						if(count($arr_cols) < $colspan){
							$arr_cols[] = $hrf.''.$hrf2;
						}else{
							break;
						}
					}
				}
			}else{
				$arr_cols = array();
				foreach ($master_hrf as $key2 => $hrf2) {
					if(count($arr_cols) < $colspan){
						$arr_cols[] = $hrf2;
					}else{
						break;
					}
				}
			}

			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			foreach (range('A','E') as $columnID) {
				$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
				// $this->excel->getActiveSheet()->getColumnDimension($columnID)->setWidth(15);
			}
			
			$total_cell = count($arr_cols);

	  	$nama_laporan = $data['nama_laporan'];

	  	if($this->input->get('id_organization_unit') > 0){
				$id_unit = $this->input->get('id_organization_unit');
				$nama_unit = $this->units_m->get_units_by_id($id_unit)->unit_name;
				$nama_laporan .= ' "'.$nama_unit.'"';
			}

			$nama_file = 'Data_Harga_'.$nama_laporan;

			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setShowGridlines(false);
			$this->excel->getActiveSheet()->setTitle('Laporan Panel Harga');

			$this->excel->getActiveSheet()->setCellValue('A1', 'Data Harga '.$nama_laporan);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$cols_header = $arr_cols[0].'1:'.$arr_cols[$total_cell-1].'1';
			$this->excel->getActiveSheet()->mergeCells($cols_header);
			// $this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$row = 2;

			if($this->input->get('f-provinsi') != ''){
	    	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	    	$kota = "";
	    	if($this->input->get('f-kota') != ''){
	    		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	    	}

	    	$this->excel->getActiveSheet()->setCellValue('A'.$row, 'Provinsi '.$provinsi.$kota);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].$row;
				$this->excel->getActiveSheet()->mergeCells($cols_header);
				// $this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$row++;
				
				$nama_file .= '_'.$provinsi.$kota;
			}

			if($data['text_periode1'] != ''){
				$nama_file .= "_".$data['text_periode1'];

				$this->excel->getActiveSheet()->setCellValue('A'.$row, $data['text_periode1']);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
				$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
				$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].$row;
				$this->excel->getActiveSheet()->mergeCells($cols_header);
				// $this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$row++;
			}

			$row++;

			$merge_row = (!$this->input->get('f-hari') && $minggu_ke != '' ? '2' : '1');
			$merge_row2 = (!$this->input->get('f-hari') && $minggu_ke != '' ? '1' : '0');

			$this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
			$cols_header = 'A'.$row.':A'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$this->excel->getActiveSheet()->setCellValue('B'.$row, 'Provinsi');
			$cols_header = 'B'.$row.':B'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Kota');
			$cols_header = 'C'.$row.':C'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$this->excel->getActiveSheet()->setCellValue('D'.$row, 'Nama Unit');
			$cols_header = 'D'.$row.':D'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$this->excel->getActiveSheet()->setCellValue('E'.$row, 'Petugas');
			$cols_header = 'E'.$row.':E'.($row+$merge_row);
			$this->excel->getActiveSheet()->mergeCells($cols_header);

			$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].($row+$merge_row);
			$this->excel->getActiveSheet()->getStyle($cols_header)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->excel->getActiveSheet()->getStyle($cols_header)->applyFromArray(
		    array(
	        'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'CCCCCC')
	        ),
	        'borders' => array(
	          'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	          )
	        ),
		    )
			);

			$next_key = array_search('F',$arr_cols);
			foreach ($data['komoditas'] as $key => $metadata) { 
	      $chr = $arr_cols[$next_key];
	      if(!$this->input->get('f-hari') && $minggu_ke != ''){
	      	$cols = $next_key + (count($data['days'])-1);
	      	$chr2 = $arr_cols[$cols];
					$this->excel->getActiveSheet()->mergeCells($chr.$row.':'.$chr2.($row+1));

					$next_key = $cols;
	      }else{
	      	$cols_header = $chr.$row.':'.$chr.($row+1);
					$this->excel->getActiveSheet()->mergeCells($cols_header);
	      }
				$next_key++;
	      $this->excel->getActiveSheet()->setCellValue($chr.$row, $metadata['nama']);
	    }

	    $mas_next_key = $next_key;
	    $chr = $arr_cols[$next_key];

	    $next_key = $next_key + (count($data['metadata_harga'])-1);
	  	$chr2 = $arr_cols[$next_key];
	    $cols_header = $chr.$row.':'.$chr2.$row;
	    $this->excel->getActiveSheet()->mergeCells($cols_header);
	    $this->excel->getActiveSheet()->setCellValue($chr.$row, 'Rata-Rata Harga Satu Tahun Per Enum di Kota/Kab');

	    foreach ($data['metadata_harga'] as $field) { 
	    	$chr = $arr_cols[$mas_next_key];
				if(!$this->input->get('f-hari') && $minggu_ke != ''){
					$this->excel->getActiveSheet()->mergeCells($chr.($row+1).':'.$chr.($row+2));
	      }
	      $explode = explode('_',$field);
	      $nama = $explode[0];
	      $width = strlen('Rata - Rata Harga '.$nama);
				$this->excel->getActiveSheet()->getColumnDimension($chr)->setWidth($width);
	      $this->excel->getActiveSheet()->setCellValue($chr.($row+1), 'Rata - Rata Harga '.$nama);
	      $mas_next_key++;
	    }

	    if(count($data['metadata_volume']) > 0) {
	    	$next_key = $next_key+1;
	    	$chr = $arr_cols[$next_key];
		    $next_key = $next_key + (count($data['metadata_volume'])-1);
		  	$chr2 = $arr_cols[$next_key];
		    $cols_header = $chr.$row.':'.$chr2.$row;
		    $this->excel->getActiveSheet()->mergeCells($cols_header);
		    $this->excel->getActiveSheet()->setCellValue($chr.$row, 'Total Pasokan Satu Tahun Per Enum di Kota/Kab');
	    }

	    foreach ($data['metadata_volume'] as $field) { 
	    	$chr = $arr_cols[$mas_next_key];
				if(!$this->input->get('f-hari') && $minggu_ke != ''){
					$this->excel->getActiveSheet()->mergeCells($chr.($row+1).':'.$chr.($row+2));
	      }
	      $explode = explode('_',$field);
	      $nama = $explode[0];
	      $width = strlen('Total '.$nama);
				$this->excel->getActiveSheet()->getColumnDimension($chr)->setWidth($width);
	      $this->excel->getActiveSheet()->setCellValue($chr.($row+1), 'Total '.$nama);
	      $mas_next_key++;
	    }


	    $next_keydays = array_search('F',$arr_cols);
	    if(!$this->input->get('f-hari') && $minggu_ke != ''){
	    	$row = $row+2;
				foreach ($data['komoditas'] as $key => $metadata) {
					$strlen = strlen($metadata['nama']);
					$width = round($strlen/count($data['days']));
					$width = $width+1;
	  			for($d=1;$d<=count($data['days']);$d++){
	  				$chr3 = $arr_cols[$next_keydays];
	  				$this->excel->getActiveSheet()->setCellValue($chr3.($row), 'H'.$d);
	    			$this->excel->getActiveSheet()->getColumnDimension($chr3)->setWidth($width);
	  				$next_keydays++;
	  			}
	  		}
			}else{
				$row = $row+1;
			}

			$endchr = $arr_cols[count($arr_cols)-1];
			// $this->excel->getActiveSheet()->getStyle('A1:'.$endchr.$row)->getAlignment()->setWrapText(true); 

			$rowberfore = $row+1;
	  	$no = 1;
			foreach ($data['entries'] as $data_entry){
	      $row++;
				$this->excel->getActiveSheet()->setCellValue('A'.$row, $no++);
				$this->excel->getActiveSheet()->setCellValue('B'.$row, $data_entry['provinsi']);
				$this->excel->getActiveSheet()->setCellValue('C'.$row, $data_entry['kota']);
				$this->excel->getActiveSheet()->setCellValue('D'.$row, $data_entry['unit_name']);
				$this->excel->getActiveSheet()->setCellValue('E'.$row, $data_entry['display_name']);
				$next_keysval = array_search('F',$arr_cols);
		  	foreach ($data['komoditas'] as $key => $metadata) {
		  		$val_metadatas = $data_entry[$metadata['field']];
		  		if(!$this->input->get('f-hari') && $minggu_ke != ''){
			  		foreach($val_metadatas as $val){
							$chr = $arr_cols[$next_keysval];
			  			$this->excel->getActiveSheet()->setCellValue($chr.$row, ($val == 0 ? '' : number_format($val,0,',','')));
			  			$next_keysval++;
	          }
	        }else{
	        	$chr = $arr_cols[$next_keysval];
	        	$this->excel->getActiveSheet()->setCellValue($chr.$row, ($val_metadatas == 0 ? '' : number_format($val_metadatas,0,',','')));
	        	$next_keysval++;
	        }
	    	}

	      foreach ($data['metadata_harga'] as $field) { 
	      	$chr = $arr_cols[$next_keysval];
	        $rata2tahunan = number_format($data_entry['avgthnan_'.$field],0,',','');
	        $this->excel->getActiveSheet()->setCellValue($chr.$row, ($rata2tahunan > 0 ? $rata2tahunan : '-'));
	        $next_keysval++;
	      }

	      foreach ($data['metadata_volume'] as $field) {
	      	$chr = $arr_cols[$next_keysval];
	        $totaltahunan = number_format($data_entry['sumthnan_'.$field],0,',','');
	        $this->excel->getActiveSheet()->setCellValue($chr.$row, ($totaltahunan > 0 ? $totaltahunan : '-'));
	        $next_keysval++;
	      }

			}

			$cols_header = 'A'.$rowberfore.':'.$endchr.$row;
			$this->excel->getActiveSheet()->getStyle($cols_header)->applyFromArray(
		    array(
	        'borders' => array(
	          'allborders' => array(
	            'style' => PHPExcel_Style_Border::BORDER_THIN,
	            'color' => array('rgb' => '000000')
	          )
	        ),
		    )
			);


			// -------------------------------------Sandingan-------------------------------------
			if($this->input->get('f-is_sandingan')){
				$row++;
				$col = 5;
				$col += count($data['metadata_harga']);
				$col += count($data['metadata_volume']);

				if(!$this->input->get('f-hari2') && $minggu_ke2 != ''){
					foreach ($data['komoditas'] as $key => $komoditas) {
						$col += count($data['days2']);
					}
				}else{
					$col += $jml_komoditas;
				}

				$colspan = $col;
				$rowspan = (!$this->input->get('f-hari2') && $minggu_ke2 != '' ? 'rowspan="3"' : 'rowspan="2"');
				$rowspan2 = (!$this->input->get('f-hari2') && $minggu_ke2 != '' ? 'rowspan="2"' : '');
				$colspan_metadata = (!$this->input->get('f-hari2') && $minggu_ke2 != '') ? 'colspan="'.count($data['days2']).'"' : '';

				$arr_cols = range('A','Z');

				$master_hrf = $arr_cols;

				$hitung = count($arr_cols);
				foreach ($arr_cols as $key => $value) {
						
				}

				if(count($arr_cols) < $colspan){
					foreach ($arr_cols as $key => $hrf) {
						foreach ($master_hrf as $key2 => $hrf2) {
							if(count($arr_cols) < $colspan){
								$arr_cols[] = $hrf.''.$hrf2;
							}else{
								break;
							}
						}
					}
				}else{
					$arr_cols = array();
					foreach ($master_hrf as $key2 => $hrf2) {
						if(count($arr_cols) < $colspan){
							$arr_cols[] = $hrf2;
						}else{
							break;
						}
					}
				}

				$total_cell = count($arr_cols);

				$row++;
				if($data['text_periode2'] != ''){
					$nama_file .= "_".$data['text_periode2'];
					$this->excel->getActiveSheet()->setCellValue('A'.$row, $data['text_periode2']);
					$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(15);
					$this->excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true);
					$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].$row;
					$this->excel->getActiveSheet()->mergeCells($cols_header);
					// $this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$row++;
				}

				$row++;

				$merge_row = (!$this->input->get('f-hari2') && $minggu_ke2 != '' ? '2' : '1');
				$merge_row2 = (!$this->input->get('f-hari2') && $minggu_ke2 != '' ? '1' : '0');

				$this->excel->getActiveSheet()->setCellValue('A'.$row, 'No');
				$cols_header = 'A'.$row.':A'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$this->excel->getActiveSheet()->setCellValue('B'.$row, 'Provinsi');
				$cols_header = 'B'.$row.':B'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$this->excel->getActiveSheet()->setCellValue('C'.$row, 'Kota');
				$cols_header = 'C'.$row.':C'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$this->excel->getActiveSheet()->setCellValue('D'.$row, 'Nama Unit');
				$cols_header = 'D'.$row.':D'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$this->excel->getActiveSheet()->setCellValue('E'.$row, 'Petugas');
				$cols_header = 'E'.$row.':E'.($row+$merge_row);
				$this->excel->getActiveSheet()->mergeCells($cols_header);

				$cols_header = $arr_cols[0].$row.':'.$arr_cols[$total_cell-1].($row+$merge_row);
				$this->excel->getActiveSheet()->getStyle($cols_header)->getFont()->setBold(true);
				// $this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$this->excel->getActiveSheet()->getStyle($cols_header)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$this->excel->getActiveSheet()->getStyle($cols_header)->applyFromArray(
			    array(
		        'fill' => array(
		            'type' => PHPExcel_Style_Fill::FILL_SOLID,
		            'color' => array('rgb' => 'CCCCCC')
		        ),
		        'borders' => array(
		          'allborders' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array('rgb' => '000000')
		          )
		        ),
			    )
				);

				$next_key = array_search('F',$arr_cols);
				foreach ($data['komoditas'] as $key => $metadata) { 
		      $chr = $arr_cols[$next_key];
		      if(!$this->input->get('f-hari2') && $minggu_ke2 != ''){
		      	$cols = $next_key + (count($data['days2'])-1);
		      	$chr2 = $arr_cols[$cols];
						$this->excel->getActiveSheet()->mergeCells($chr.$row.':'.$chr2.($row+1));

						$next_key = $cols;
		      }else{
		      	$cols_header = $chr.$row.':'.$chr.($row+1);
						$this->excel->getActiveSheet()->mergeCells($cols_header);
		      }
					$next_key++;
		      $this->excel->getActiveSheet()->setCellValue($chr.$row, $metadata['nama']);
		    }

		    $mas_next_key = $next_key;
		    $chr = $arr_cols[$next_key];
		    $next_key = $next_key + (count($data['metadata_harga'])-1);
		  	$chr2 = $arr_cols[$next_key];
		    $cols_header = $chr.$row.':'.$chr2.$row;
		    $this->excel->getActiveSheet()->mergeCells($cols_header);
		    $this->excel->getActiveSheet()->setCellValue($chr.$row, 'Rata-Rata Harga Satu Tahun Per Enum di Kota/Kab');

		    foreach ($data['metadata_harga'] as $field) { 
		    	$chr = $arr_cols[$mas_next_key];
					if(!$this->input->get('f-hari2') && $minggu_ke2 != ''){
						$this->excel->getActiveSheet()->mergeCells($chr.($row+1).':'.$chr.($row+2));
		      }
		      $explode = explode('_',$field);
		      $nama = $explode[0];
		      $width = strlen('Rata - Rata Harga '.$nama);
					$this->excel->getActiveSheet()->getColumnDimension($chr)->setWidth($width);
		      $this->excel->getActiveSheet()->setCellValue($chr.($row+1), 'Rata - Rata Harga '.$nama);
		      $mas_next_key++;
		    }

		    if(count($data['metadata_volume']) > 0) {
		    	$next_key = $next_key+1;
		    	$chr = $arr_cols[$next_key];
			    $next_key = $next_key + (count($data['metadata_volume'])-1);
			  	$chr2 = $arr_cols[$next_key];
			    $cols_header = $chr.$row.':'.$chr2.$row;
			    $this->excel->getActiveSheet()->mergeCells($cols_header);
			    $this->excel->getActiveSheet()->setCellValue($chr.$row, 'Total Pasokan Satu Tahun Per Enum di Kota/Kab');
		    }

		    foreach ($data['metadata_volume'] as $field) { 
		    	$chr = $arr_cols[$mas_next_key];
					if(!$this->input->get('f-hari2') && $minggu_ke2 != ''){
						$this->excel->getActiveSheet()->mergeCells($chr.($row+1).':'.$chr.($row+2));
		      }
		      $explode = explode('_',$field);
		      $nama = $explode[0];
		      $width = strlen('Total '.$nama);
					$this->excel->getActiveSheet()->getColumnDimension($chr)->setWidth($width);
		      $this->excel->getActiveSheet()->setCellValue($chr.($row+1), 'Total '.$nama);
		      $mas_next_key++;
		    }


		    $next_keydays = array_search('F',$arr_cols);
		    if(!$this->input->get('f-hari2') && $minggu_ke2 != ""){
		    	$row = $row+2;
					foreach ($data['komoditas'] as $key => $metadata) {
						$strlen = strlen($metadata['nama']);
						$width = round($strlen/count($data['days2']));
						$width = $width+1;
		  			for($d=1;$d<=count($data['days2']);$d++){
		  				$chr3 = $arr_cols[$next_keydays];
		  				$this->excel->getActiveSheet()->setCellValue($chr3.($row), 'H'.$d);
		    			$this->excel->getActiveSheet()->getColumnDimension($chr3)->setWidth($width);
		  				$next_keydays++;
		  			}
		  		}
				}else{
					$row = $row+1;
				}

				$endchr = $arr_cols[count($arr_cols)-1];
				// $this->excel->getActiveSheet()->getStyle('A1:'.$endchr.$row)->getAlignment()->setWrapText(true); 

				$rowberfore = $row+1;
		  	$no = 1;

				foreach ($data['entries2'] as $data_entry){
		      $row++;
					$this->excel->getActiveSheet()->setCellValue('A'.$row, $no++);
					$this->excel->getActiveSheet()->setCellValue('B'.$row, $data_entry['provinsi']);
					$this->excel->getActiveSheet()->setCellValue('C'.$row, $data_entry['kota']);
					$this->excel->getActiveSheet()->setCellValue('D'.$row, $data_entry['unit_name']);
					$this->excel->getActiveSheet()->setCellValue('E'.$row, $data_entry['display_name']);
					$next_keysval = array_search('F',$arr_cols);
			  	foreach ($data['komoditas'] as $key => $metadata) {
			  		$val_metadatas = $data_entry[$metadata['field']];
			  		if(!$this->input->get('f-hari2') && $minggu_ke2 != ''){
				  		foreach($val_metadatas as $val){
								$chr = $arr_cols[$next_keysval];
				  			$this->excel->getActiveSheet()->setCellValue($chr.$row, ($val == 0 ? '' : number_format($val,0,',','')));
				  			$next_keysval++;
		          }
		        }else{
		        	$chr = $arr_cols[$next_keysval];
		        	$this->excel->getActiveSheet()->setCellValue($chr.$row, ($val_metadatas == 0 ? '' : number_format($val_metadatas,0,',','')));
		        	$next_keysval++;
		        }
		    	}

		      foreach ($data['metadata_harga'] as $field) { 
		      	$chr = $arr_cols[$next_keysval];
		        $rata2tahunan = number_format($data_entry['avgthnan_'.$field],0,',','');
		        $this->excel->getActiveSheet()->setCellValue($chr.$row, ($rata2tahunan > 0 ? $rata2tahunan : '-'));
		        $next_keysval++;
		      }

		      foreach ($data['metadata_volume'] as $field) {
		      	$chr = $arr_cols[$next_keysval];
		        $totaltahunan = number_format($data_entry['sumthnan_'.$field],0,',','');
		        $this->excel->getActiveSheet()->setCellValue($chr.$row, ($totaltahunan > 0 ? $totaltahunan : '-'));
		        $next_keysval++;
		      }

				}

				$cols_header = 'A'.$rowberfore.':'.$endchr.$row;
				$this->excel->getActiveSheet()->getStyle($cols_header)->applyFromArray(
			    array(
		        'borders' => array(
		          'allborders' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array('rgb' => '000000')
		          )
		        ),
			    )
				);
			}


			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$nama_file.'.xls"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
	  }else{
	  	$col = 5;
			$col += count($data['metadata_harga']);
			$col += count($data['metadata_volume']);

  		if(!$this->input->get('f-hari') && $minggu_ke != ''){
				foreach ($data['komoditas'] as $key => $komoditas) {
					$col += count($data['days']);
				}
			}else{
				$col += $jml_komoditas;
			}
			$colspan = $col;

	  	$rowspan = (!$this->input->get('f-hari') && $minggu_ke != "" ? 'rowspan="3"' : 'rowspan="2"');
			$rowspan2 = (!$this->input->get('f-hari') && $minggu_ke != "" ? 'rowspan="2"' : '');
			$colspan_metadata = (!$this->input->get('f-hari') && $minggu_ke != "") ? 'colspan="'.count($data['days']).'"' : '';

	  	$nama_laporan = $data['nama_laporan'];

			$nama_file = 'Data_Harga_'.$nama_laporan;

			$html ="
	    <table border=\"0\" cellpadding=\"5\"  align=\"center\" style=\"text-align:center;\">
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	      <tr>
	          <td colspan=\"".$colspan."\">
	              <span style=\"font: bold 20px Open Sans; display: block;\">Data Harga ".$nama_laporan."</span>
	          </td>
	      </tr>";

	      if($this->input->get('f-provinsi') != ''){
	      	$provinsi = $this->provinsi_m->get_provinsi_by_id($this->input->get('f-provinsi'))['nama'];
	      	$kota = "";
	      	if($this->input->get('f-kota') != ''){
	      		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
	      	}
					$html .= '
						<tr>
							<td colspan="'.$colspan.'">
								<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
							</td>
						</tr>
					';
					$nama_file .= '_'.$provinsi.$kota;
				}

				if($data['text_periode1'] != ''){
					$html .= '
						<tr>
							<td colspan="'.$colspan.'">
								<span style="font: bold 20px Open Sans; display: block;">'.$data['text_periode1'].'</span>
							</td>
						</tr>
					';
				}

	      $html .="
	      <tr>
	          <td colspan=\"".$colspan."\"></td>
	      </tr>
	    </table>";

	  	$html .= '
	  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
	  		$html .='
	      <thead>
	        <tr style="background-color:#ccc;">
	        	<th '.$rowspan.'>No</th>
	        	<th '.$rowspan.'>Provinsi</th>
	        	<th '.$rowspan.'>Kota</th>
	        	<th '.$rowspan.'>Nama Unit</th>
	  				<th '.$rowspan.'>Petugas</th>';
	  				foreach ($data['komoditas'] as $key => $metadata) { 
	              $html.='<th '.$colspan_metadata.' rowspan="2">'.$metadata['nama'].'</th>';
	          }

	          $html .= '
	          	<th colspan="'.count($data['metadata_harga']).'" style="text-align:center;">Rata-Rata Harga Satu Tahun Per Enum di Kota/Kab</th>';
	          if(count($data['metadata_volume']) > 0) {
	            $html .= '<th colspan="'.count($data['metadata_volume']).'" style="text-align:center;">Total Pasokan Satu Tahun Per Enum di Kota/Kab</th>';
	          }
	        	$html .= '
	        </tr>
	        <tr style="background-color:#ccc;">';
	          foreach ($data['metadata_harga'] as $field) { 
	            $explode = explode('_',$field);
	            $nama = $explode[0];
	            
	            $html .= '<th '.$rowspan2.' width="100">Rata - Rata Harga '.$nama.'</th>';
	          }

	          foreach ($data['metadata_volume'] as $field) { 
	            $explode = explode('_',$field);
	            $nama = $explode[0];
	            
	            $html.='<th '.$rowspan2.'>Total '.$nama.'</th>';
	          }
	        $html .= '</tr>';

	          if(!$this->input->get('f-hari') && $minggu_ke != ""){
	          	$html .= '<tr style="background-color:#ccc;">';
				  			foreach ($data['komoditas'] as $key => $metadata) {
					  			for($d=1;$d<=count($data['days']);$d++){
					  				$html.='<th>H'.$d.'</th>';
					  			}
					  		}
				  		$html .= '</tr>';
				  	}

			  	$html .= '
	  		</thead>
	  		<tbody>';
	  		$no = 1;
	  		foreach ($data['entries'] as $data_entry){
	  			$html .='
	  				<tr>
	  					<td>'.$no++.'</td>
	  					<td>'.$data_entry['provinsi'].'</td>
	  					<td>'.$data_entry['kota'].'</td>
	  					<td>'.$data_entry['unit_name'].'</td>
					  	<td>'.$data_entry['display_name'].'</td>';
					  	foreach ($data['komoditas'] as $key => $metadata) {
					  		$h2 = 1;
					  		$val_metadatas = $data_entry[$metadata['field']];
					  		if(!$this->input->get('f-hari') && $minggu_ke != ""){
						  		foreach($val_metadatas as $val){
		              	$html.='<td>'.($val == 0 ? '' : number_format($val,0,',','')).'</td>';
		              	$h2++;
		              }
		            }else{
		            	$html.='<td>'.($val_metadatas == 0 ? '' : number_format($val_metadatas,0,',','')).'</td>';
		            }
	          	}

	            foreach ($data['metadata_harga'] as $field) { 
	              $rata2tahunan = number_format($data_entry['avgthnan_'.$field],0,',','');
	              $html .= '<td style="text-align:right;">'.($rata2tahunan > 0 ? $rata2tahunan : '-').'</td>';
	            }

	            foreach ($data['metadata_volume'] as $field) {
	              $totaltahunan = number_format($data_entry['sumthnan_'.$field],0,',','');
	              $html .= '<td style="text-align:right;">'.($totaltahunan > 0 ? $totaltahunan : '-').'</td>';
	            } 

	        $html .= '</tr>';
	  		}
	  		$html .='
	  		</tbody>
	  	</table>';


	  	// ------- Sandingan periode ----------------------------

	  	if($this->input->get('f-is_sandingan')){
	  		$col = 5;
				$col += count($data['metadata_harga']);
				$col += count($data['metadata_volume']);

	  		if(!$this->input->get('f-hari') && $minggu_ke != ''){
					foreach ($data['komoditas'] as $key => $komoditas) {
						$col += count($data['days']);
					}
				}else{
					$col += $jml_komoditas;
				}
				$colspan = $col;
				
		  	$rowspan = (!$this->input->get('f-hari2') && $minggu_ke2 != "" ? 'rowspan="3"' : 'rowspan="2"');
				$rowspan2 = (!$this->input->get('f-hari2') && $minggu_ke2 != "" ? 'rowspan="2"' : '');
				$colspan_metadata = (!$this->input->get('f-hari2') && $minggu_ke2 != "") ? 'colspan="'.count($data['days2']).'"' : '';

				$html .="<br>
		    <table border=\"0\" cellpadding=\"5\"  align=\"center\" style=\"text-align:center;\">";


					if($data['text_periode2'] != ''){
						$html .= '
							<tr>
								<td colspan="'.$colspan.'">
									<span style="font: bold 20px Open Sans; display: block;">'.$data['text_periode2'].'</span>
								</td>
							</tr>
						';
					}

		      $html .="
		      <tr>
		          <td colspan=\"".$colspan."\"></td>
		      </tr>
		    </table>";

		  	$html .= '
		  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
		  		$html .='
		      <thead>
		        <tr style="background-color:#ccc;">
		        	<th '.$rowspan.'>No</th>
		        	<th '.$rowspan.'>Provinsi</th>
		        	<th '.$rowspan.'>Kota</th>
		        	<th '.$rowspan.'>Nama Unit</th>
		  				<th '.$rowspan.'>Petugas</th>';
		  				foreach ($data['komoditas'] as $key => $metadata) { 
		              $html.='<th '.$colspan_metadata.' rowspan="2">'.$metadata['nama'].'</th>';
		          }

		          $html .= '
		          	<th colspan="'.count($data['metadata_harga']).'" style="text-align:center;">Rata-Rata Harga Satu Tahun Per Enum di Kota/Kab</th>';
		          if(count($data['metadata_volume']) > 0) {
		            $html .= '<th colspan="'.count($data['metadata_volume']).'" style="text-align:center;">Total Pasokan Satu Tahun Per Enum di Kota/Kab</th>';
		          }
		        	$html .= '
		        </tr>
		        <tr style="background-color:#ccc;">';
		          foreach ($data['metadata_harga'] as $field) { 
		            $explode = explode('_',$field);
		            $nama = $explode[0];
		            
		            $html .= '<th '.$rowspan2.' width="100">Rata - Rata Harga '.$nama.'</th>';
		          }

		          foreach ($data['metadata_volume'] as $field) { 
		            $explode = explode('_',$field);
		            $nama = $explode[0];
		            
		            $html.='<th '.$rowspan2.'>Total '.$nama.'</th>';
		          }
		        $html .= '</tr>';

		          if(!$this->input->get('f-hari2') && $minggu_ke2 != ""){
		          	$html .= '<tr style="background-color:#ccc;">';
					  			foreach ($data['komoditas'] as $key => $metadata) {
						  			for($d=1;$d<=count($data['days2']);$d++){
						  				$html.='<th>H'.$d.'</th>';
						  			}
						  		}
					  		$html .= '</tr>';
					  	}

				  	$html .= '
		  		</thead>
		  		<tbody>';
		  		$no = 1;
		  		foreach ($data['entries2'] as $data_entry){
		  			$html .='
		  				<tr>
		  					<td>'.$no++.'</td>
		  					<td>'.$data_entry['provinsi'].'</td>
		  					<td>'.$data_entry['kota'].'</td>
		  					<td>'.$data_entry['unit_name'].'</td>
						  	<td>'.$data_entry['display_name'].'</td>';
						  	foreach ($data['komoditas'] as $key => $metadata) {
						  		$h2 = 1;
						  		$val_metadatas = $data_entry[$metadata['field']];
						  		if(!$this->input->get('f-hari2') && $minggu_ke2 != ""){
							  		foreach($val_metadatas as $val){
			              	$html.='<td>'.($val == 0 ? '' : number_format($val,0,',','')).'</td>';
			              	$h2++;
			              }
			            }else{
			            	$html.='<td>'.($val_metadatas == 0 ? '' : number_format($val_metadatas,0,',','')).'</td>';
			            }
		          	}

		            foreach ($data['metadata_harga'] as $field) { 
		              $rata2tahunan = number_format($data_entry['avgthnan_'.$field],0,',','');
		              $html .= '<td style="text-align:right;">'.($rata2tahunan > 0 ? $rata2tahunan : '-').'</td>';
		            }

		            foreach ($data['metadata_volume'] as $field) {
		              $totaltahunan = number_format($data_entry['sumthnan_'.$field],0,',','');
		              $html .= '<td style="text-align:right;">'.($totaltahunan > 0 ? $totaltahunan : '-').'</td>';
		            } 

		        $html .= '</tr>';
		  		}
		  		$html .='
		  		</tbody>
		  	</table>';
		  }

	  	
	  	$data['html'] = $html;
	  	$this->load->view('admin/page_print2', $data);
	  }
	}

	public function download_input_laporan($data, $page){

		if(! group_has_role('laporan', 'view_all_laporan') AND ! group_has_role('laporan', 'view_own_laporan') AND ! group_has_role('laporan','view_own_unit_laporan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;

		$nama_laporan = $data['nama_laporan'];
		$nama_file = 'Data Laporan '.$nama_laporan;

		$laporan_data['entries'] = $data['laporan_data']['entries'];

		$html ="
    <table border=\"0\" cellpadding=\"7\">
      <tr>
          <td colspan=\"7\"></td>
      </tr>
      <tr>
          <td colspan=\"7\" align=\"center\">
              <span style=\"font: bold 20px Open Sans; display: block;\">Daftar Laporan ".$nama_laporan."</span>
          </td>
      </tr>";

      if($id_provinsi != NULL){
      	$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
      	$kota = "";
      	if($this->input->get('f-kota') != ''){
      		$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
      	}
				$html .= '
					<tr>
						<td colspan="7" align="center">
							<span style="font: bold 20px Open Sans; display: block;">Provinsi '.$provinsi.$kota.'</span>
						</td>
					</tr>
				';
				$nama_file .= '_'.$provinsi.$kota;
			}

			$periode = array();
			$arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
			
			if($this->input->get('f-minggu_ke') != '' || $this->input->get('f-bln') != '' || $this->input->get('f-thn') != '' || $this->input->get('f-hari')){

				if($this->input->get('f-hari') != ''){
					$periode[] = date_idr($this->input->get('f-hari'), 'l, d F Y', null);
				}else{
					if($this->input->get('f-minggu_ke') != ''){
						$periode[] = ' Minggu Ke '.$this->input->get('f-minggu_ke');
					}
					if($this->input->get('f-bln') != ''){
						$periode[] = 'Bulan '.$arr_month[$this->input->get('f-bln')];
					}
					if($this->input->get('f-thn') != ''){
						$periode[] = 'Tahun '.$this->input->get('f-thn');
					}
				}
			}else{
				$periode[] = ' Minggu Ke '.$data['minggu_ke'];
				$periode[] = 'Bulan '.$arr_month[date('m')];
				$periode[] = 'Tahun '.date('Y');
			}

			if(count($periode) > 0){
				$implode_periode = implode(', ',$periode);
				$nama_file .= "_".implode('_',$periode);
				$html .= '
					<tr>
						<td colspan="7" align="center">
							<span style="font: bold 20px Open Sans; display: block;">Periode: '.$implode_periode.'</span>
						</td>
					</tr>
				';
			}

      $html .="
      <tr>
          <td colspan=\"7\"></td>
      </tr>
    </table>";

  	$html .= '
  	<table class="table table-striped table-bordered table-hover" border="1" cellpadding="0" cellspacing="0">';
  		$html .='
      <thead style="background-color:#ccc;">
        <tr>
        	<th>No</th>
        	<th>'.lang("laporan:pengirim").'</th>
        	<th>'.lang("location:provinsi:singular").'</th>
  				<th>'.lang("location:kota:singular").'</th>
  				<th>'.lang("laporan:nama_unit").'</th>
  				<th>'.lang("laporan:komoditas").'</th>
  				<th>'.lang("laporan:channel").'</th>
  				<th>'.lang("laporan:minggu_ke").'</th>
  				<th>'.lang("laporan:tanggal").'</th>
  			</tr>
  		</thead>
  		<tbody>';
  		$no = 1;
  		foreach ($laporan_data['entries'] as $laporan_data_entry){
  			$html .='
  				<tr>
  					<td>'.$no++.'</td>
  					<td>'.$laporan_data_entry['display_name'].'</td>
  					<td>'.$laporan_data_entry['provinsi'].'</td>
				  	<td>'.$laporan_data_entry['kota'].'</td>
				  	<td>'.$laporan_data_entry['unit_name'].'</td>
				  	<td>'.$laporan_data_entry['nama_komoditas'].'</td>
				  	<td>'.$laporan_data_entry['channel'].'</td>
				  	<td>'.$laporan_data_entry['minggu_ke'].'</td>
				  	<td>'.date_idr($laporan_data_entry['created_on'], 'd F Y', null).'</td>
  				</tr>
  			';
  		}
  		$html .='
  		</tbody>
  	</table>';

  	if($page == 'download') {
	  	$save = '';
	    $save .= "
	    <html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
	    xmlns=\"http://www.w3.org/TR/REC-html40\">
	     
	    <head>
	        <meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
	        <meta name=ProgId content=Excel.Sheet>
	        <meta name=Generator content=\"Microsoft Excel 11\">
	        <title>Data Input Web</title>
	    </head>
	    <body>";

	    $save .= $html;
	    $save .= "
	        </body>
	        </html>";

	    header("Content-Disposition: attachment; filename=\"".$nama_file.".xls\"");
	    header("Content-Type: application/vnd.ms-excel");
	    header('Cache-Control: max-age=0');

	    echo $save;
	  }else{
	  	$data['html'] = $html;
	  	$this->load->view('admin/page_print', $data);
	  }
	}

	public function ajax_unit_dropdown($parent_id = 0, $select_all = 0) {
    $units = $parent_id == 0 ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($parent_id);
    if ($select_all) {
        echo '<option value="">' . lang('global:select-all') . '</option>';
    } else {
        if (count($units)) {
            echo '<option value="-1">' . lang('global:select-pick') . '</option>';
        } else {
            echo '<option value="-1">' . lang('global:select-none') . '</option>';
        }
    }
    
    foreach ($units as $unit) {
        echo '<option value="' . $unit['id'] . '"> ' . $unit['unit_name'] . '</option>';
    }
	}

	public function ajax_get_metadata_by_komoditas($tipe_laporan, $id_komoditas){
		$data['metadata'] = $this->metadata_m->get_metadata_by_tipe_laporan($tipe_laporan, $id_komoditas, 'default_laporan_metadata.*',NULL,'urutan');
		$data['mode'] = 'new';

		$this->load->view('admin/data_metadata_form', $data);
	}
	// --------------------------------------------------------------------------

}