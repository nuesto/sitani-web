<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_komoditas extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'komoditas';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'access_komoditas_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('laporan');		
		$this->load->model('komoditas_m');
		$this->load->model('tipe_m');
		$this->load->model('tipe_field_m');
  }

  /**
	 * List all komoditas
   *
   * @return	void
   */
  public function index()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_komoditas')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
    // -------------------------------------
		// Get entries
		// -------------------------------------
		
    $data['komoditas']['entries'] = $this->komoditas_m->get_komoditas();
		$data['komoditas']['total'] = count($data['komoditas']['entries']);
		$data['komoditas']['pagination'] = '';

		$data['uri'] = $this->get_query_string(5);

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:komoditas:plural'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:komoditas:plural'))
			->build('admin/komoditas_index', $data);
  }
	
	/**
   * Create a new komoditas entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
  public function create()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_komoditas')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_komoditas('new')){	
				$this->session->set_flashdata('success', lang('laporan:komoditas:submit_success'));				
				redirect('admin/laporan/komoditas/index'.$this->get_query_string(5));
			}else{
				$data['messages']['error'] = lang('laporan:komoditas:submit_failure');
			}
		}

		$data['mode'] = 'new';
		$data['uri'] = $this->get_query_string(5);
		$data['return'] = 'admin/laporan/komoditas/index'.$data['uri'];

		$komoditas =$this->komoditas_m->get_komoditas();
		$data['total'] = count($komoditas) + 1;

		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:komoditas:new'))
    	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:komoditas:plural'), '/admin/laporan/komoditas/index')
			->set_breadcrumb(lang('laporan:data:new'))
			->build('admin/komoditas_form', $data);
  }
	
	/**
   * Edit a komoditas entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the komoditas to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_komoditas')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_komoditas('edit', $id)){	
				$this->session->set_flashdata('success', lang('laporan:komoditas:submit_success'));				
				redirect('admin/laporan/komoditas/index'.$this->get_query_string(6));
			}else{
				$data['messages']['error'] = lang('laporan:komoditas:submit_failure');
			}
		}
		
		$data['fields'] = $this->komoditas_m->get_komoditas_by_id($id);

		$data['mode'] = 'edit';
		$data['uri'] = $this->get_query_string(6);
		$data['return'] = 'admin/laporan/komoditas/index'.$data['uri'];
		$data['entry_id'] = $id;

		$komoditas = $this->komoditas_m->get_komoditas();
		$data['total'] = count($komoditas);

		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:komoditas:edit'))
    	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:komoditas:plural'), '/admin/laporan/komoditas/index')
			->set_breadcrumb(lang('laporan:komoditas:edit'))
			->build('admin/komoditas_form', $data);
  }
	
	/**
   * Delete a komoditas entry
   * 
   * @param   int [$id] The id of komoditas to be deleted
   * @return  void
   */
  public function delete($id = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_komoditas')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
		if($this->komoditas_m->is_used($id)){
			// -------------------------------------
			// Delete entry
			// -------------------------------------
	    $this->komoditas_m->delete_komoditas_by_id($id);
    	$this->session->set_flashdata('error', lang('laporan:komoditas:deleted'));
		}else{
	    $this->session->set_flashdata('error', lang('laporan:komoditas:is_used'));
		}
    
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
		$pagination_config['uri_segment'] = 6;
		$pagination_config['per_page'] = 25;
		$komoditas =$this->komoditas_m->get_komoditas($pagination_config);
		$total = count($komoditas);

		$page = $this->uri->segment(6);
		$cek_page = $total%25;
		if($total > 0){
			if($cek_page == 0){
				$page = $page-25;
			}
		}
		$uri = '';
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }

    redirect('admin/laporan/komoditas/index'.$uri);
  }
	
	/**
   * Insert or update komoditas entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_komoditas($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama_komoditas', lang('laporan:nama_komoditas'), 'required|max_length[255]');
		$this->form_validation->set_rules('slug', lang('laporan:slug'), 'required|max_length[45]|alpha_dash');
		$this->form_validation->set_rules('kode_komoditas', lang('laporan:kode_komoditas'),'required|max_length[10]|alpha_numeric');

		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{		
			if ($method == 'new')
			{
				$result = $this->komoditas_m->insert_komoditas($values);
			}
			else
			{
				$result = $this->komoditas_m->update_komoditas($values, $row_id);
			}
		}
		
		return $result;
	}

	public function get_query_string($page){
		// -------------------------------------
		// Get query string
		// -------------------------------------

		$page = $this->uri->segment($page);
		$uri = '';
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }
    
    return $uri;
	}

	public function update_urutan(){
    $result = $_REQUEST["table-1"];
    $i = 0;
    foreach($result as $id) {
    	if($id != ""){
	      $this->komoditas_m->update_urutan($id,$i);
	      $i++;
	    }
    }   
  }
	// --------------------------------------------------------------------------

}