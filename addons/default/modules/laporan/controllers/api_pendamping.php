<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Pendamping
*
* Module API
*
*/
class Api_pendamping extends API2_Controller
{
	public $metod = 'get';

	public function __construct(){
		parent::__construct();

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		// Load the required classes

		$this->lang->load('laporan');
		$this->lang->load('location/location');

		$this->load->model('organization/memberships_m');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
		$this->load->model('tipe_m');
		$this->load->model('pendamping_m');
		// $this->authorize_api_access('laporan','api_pendamping');
	}

	public function get_all()
	{
		if($this->input->get('id_provinsi')) {
			$_GET['f-provinsi'] = $this->input->get('id_provinsi');
			unset($_GET['id_provinsi']);
		}

		if($this->input->get('id_kota')) {
			$_GET['f-kota'] = $this->input->get('id_kota');
			unset($_GET['id_kota']);
		}

		$pendamping = $this->pendamping_m->get_pendamping();

		$result = $pendamping;

		$status = '200';

		if(empty($result)) {
			$result = array('status'=>'error', 'message'=>'Pendamping tidak ditemukan');
	    	$status = '200';
		}

		_output($result,$status);
	}

	public function get_by_id($id='')
	{
		if($id) {
			$_GET['f-user_id'] = $id;
		}
		$pendamping = $this->pendamping_m->get_pendamping();

		$result = $pendamping[0];

		$status = '200';

		if(empty($result)) {
			$result = array('status'=>'error', 'message'=>'Pendamping tidak ditemukan');
	    	$status = '200';
		}

		_output($result,$status);
	}

	public function jumlah()
	{
		if($this->input->get('id_provinsi')) {
			$_GET['f-provinsi'] = $this->input->get('id_provinsi');
			unset($_GET['id_provinsi']);
		}

		if($this->input->get('id_kota')) {
			$_GET['f-kota'] = $this->input->get('id_kota');
			unset($_GET['id_kota']);
		}

		$this->db->where('group_id', 7);
		$this->db->where('active', 1);
		$result = $this->db->count_all_results('default_users');

		$status = '200';

		_output($result,$status);
	}
}
