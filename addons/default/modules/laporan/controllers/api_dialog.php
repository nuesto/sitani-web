<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Dialog
*
* Module API
*
*/
class Api_dialog extends API2_Controller
{
	public $metod = 'get';

	public function __construct()
	{
		parent::__construct();

    	// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('laporan');

		$this->load->model('dialog_m');

		// $this->authorize_api_access('laporan','api_dialog');
	}

	public function index()
	{
		$this->load->model('dialog_m');
		$data['dialog'] = $this->dialog_m->get_dialog_dashboard();

		$result = $data['dialog'];
        $status = "200";

        if(empty($result)) {
        	$result = array("status"=>"error","messages"=>"Dialog data not found");
			$status = "200";
        }
        _output($result,$status);
	}
}
