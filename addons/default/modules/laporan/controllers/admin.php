<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/laporan/data/index/1');
    }

}