<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Organization
*
* Module API
*
*/
class Api_laporan extends API2_Controller
{
	public $metod = 'get';

	// This controller simply redirect to main section controller
	public function __construct()
	{
		parent::__construct();

		date_default_timezone_set('Asia/Jakarta');
		ini_set('max_execution_time', '3600');

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('laporan');
    	// $this->lang->load('enumerator/enumerator');
		$this->load->model('data_m');
		$this->load->model('tipe_m');
		$this->load->model('metadata_m');
		$this->load->model('pendamping_m');
		// $this->load->model('enumerator/enumerator_m');
		$this->load->model('absen_m');

		$this->lang->load('location/location');
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');

		// Organization

		$this->load->library('organization/organization');
		$this->load->model('organization/types_m');
		$this->load->model('organization/units_m');
		$this->load->model('organization/memberships_m');

		$this->config->load('organization/organization');
		$this->node_type_slug = $this->config->item('node_type');
		$this->get_type = $this->types_m->get_type_by_slug($this->node_type_slug);
		$this->node_type_level = $this->get_type->type_level;
		$this->node_type_slug = $this->get_type->type_slug;

		$this->unit = $this->memberships_m->get_one_unit_by_member(1);

		$this->load->helper('laporan');

	}

	public function get_latest($limit = NULL)
	{
		if(empty($limit)) {
			$result = array("status"=>"error","messages"=>"Parameter limit not found");
			$status = "200";
			_output($result,$status);
		} else {
			$report = $this->data_m->get_ten_last_report($limit);

			$result = array();
			foreach ($report as $key => $value) {
				$result[] = array(
					'pendamping' => $value['pendamping'],
					'nama_laporan' => $value['nama_laporan'],
					'unit_name' => $value['unit_name'],
					'time' => humanTiming(strtotime($value['created_on']))
					);
			}

			$status = "200";

			if(empty($result)) {
				$result = array("status"=>"error","messages"=>"Latest report data not found");
				$status = "200";
			}
			_output($result,$status);
		}
	}

	public function get_minggu_ke($year = null, $month = null){
		$this->load->helper('laporan');
		if($year != NULL && $month != NULL){
			$date = $year.'-'.$month.'-01';
			$end_week = get_count_of_week($date);
			$romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
			// echo '<option value="">-- '.lang('laporan:minggu_ke').' --</option>';
			for ($i=1;$i<=$end_week;$i++) {
				$result[$i] = $romawi[$i];
			}
		}else{
			$result = array();
		}

		$status = "200";

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>"Minggu not found");
			$status = "200";
		}
		_output($result,$status);
	}

	public function data_harga($id = NULL, $graph = 0)
	{
		if(empty($id)) {
			$result = array("status"=>"error","messages"=>"Parameter id laporan not found");
			$status = "200";
			_output($result,$status);
		} else {
			$this->load->helper('laporan');

			if($this->input->get('provinsi')) {
				$_GET['f-provinsi'] = $this->input->get('provinsi');
				unset($_GET['provinsi']);
			}

			if($this->input->get('kota')) {
				$_GET['f-kota'] = $this->input->get('kota');
				unset($_GET['kota']);
			}

			if($this->input->get('thn')) {
				$_GET['f-thn'] = $this->input->get('thn');
				unset($_GET['thn']);
			}

			if($this->input->get('bln')) {
				$_GET['f-bln'] = $this->input->get('bln');
				unset($_GET['bln']);
			}

			if($this->input->get('minggu_ke')) {
				$_GET['f-minggu_ke'] = $this->input->get('minggu_ke');
				unset($_GET['minggu_ke']);
			}

			if($this->input->get('komoditas')) {
				$_GET['f-komoditas'] = $this->input->get('komoditas');
				unset($_GET['komoditas']);
			}

			if($this->input->get('gapoktan')) {
				$_GET['id_organization_unit_0'] = $this->input->get('gapoktan');
				unset($_GET['gapoktan']);
			}

			if($this->input->get('tti')) {
				$_GET['id_organization_unit'] = $this->input->get('tti');
				unset($_GET['tti']);
			}

			$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

			$filter_kota = null;
			if($this->input->get('f-provinsi') != '') {
				$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
				$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
			}

			$weeks = getWeeks(date('Y-m-d'), 'monday');

			$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
			$thn_bln = $weeks['tahun'].'-'.$bln;
			if($this->input->get('f-bln') && $this->input->get('f-thn')){
				$thn_bln = $this->input->get('f-thn')."-".$this->input->get('f-bln');
			}

			$date = date($thn_bln.'-01');
			if($this->input->get('f-thn') != '' && $this->input->get('f-bln') != ''){
				$date = $this->input->get('f-thn').'-'.$this->input->get('f-bln').'-01';
			}

			$end_week = get_count_of_week($date);
			$data['end_week'] = $end_week;

			$minggu_ke = (string) $weeks['week'];
			if($this->input->get('f-minggu_ke')){
				$minggu_ke = $this->input->get('f-minggu_ke');
			}
			$data['minggu_ke'] = $minggu_ke;

			$ex_thn_bln = explode('-',$thn_bln);
			$data['thn'] = $ex_thn_bln[0];
			$data['bln'] = $ex_thn_bln[1];

			$firstDayOfWeek = $this->absen_m->get_first_day_of_week($minggu_ke, $thn_bln, $id);
			$data['firstDayOfWeek'] = $firstDayOfWeek;
			$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');

			$start = 7;
			$end = 35;
			$intervals = get_interval($firstDayOfWeek, $start, $end);

			// if(empty($firstDayOfWeek)){
			// 	$firstDayOfWeek = get_date_by_wmy($minggu_ke, $thn_bln);
			// }

			$data['thn_bln'] = date_idr($thn_bln, 'F Y', null);

			$data['id'] = $id;

			$nama_laporan = $this->tipe_m->get_tipe_by_id($id)['nama_laporan'];

			$today = date('Y-m-d H:i:s');
			// $today = '2016-09-09';
			$id_absen = $this->absen_m->get_absen_by_date($today, $id)['id'];
			$data['data']['entries'] = $this->data_m->laporan_harga($id, $minggu_ke, $thn_bln, $intervals, $id_absen);
			$data['data']['total'] = count($data['data']['entries']);
			$data['nama_laporan'] = $nama_laporan;

			$data['komoditas'] = $this->metadata_m->get_metadata_by_tipe_laporan($id, 1, 'default_laporan_metadata.*');
			$data['min_year'] = $this->absen_m->get_min_year();
			$data['max_year'] = $this->absen_m->get_max_year();

			// if($id == 2){
			// -------------------------------------
			// Get Organization by Member
			// -------------------------------------

			$data['types'] = $this->types_m->get_all_types(NULL, $id);

			$unit_id = $this->unit['id'];

			$types = $data['types'];
			$organization = array();
			$addedLevel = array();
			$n2 = 0;
			$n = count($types);

			foreach($types as $i => $type){
				if (in_array($type['level'], $addedLevel)) {
					continue;
				}
				$addedLevel[] = $type['level'];
				$units = $this->units_m->get_unit_by_child($unit_id);

				if(isset($units['id'])){
					if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
						$organization['organization_name_'.$units['type_level']] = $units['unit_name'];
						$organization['id_organization_unit_'.$units['type_level']] = $units['id'];
						$n2++;
					} else if ($units['type_slug'] == $this->node_type_slug) {
						$organization['organization_name_'.$units['type_level']] = $units['unit_name'];
						$organization['id_organization_unit'] = $units['id'];
					}
					$unit_id = $units['id'];
				}
			}

			if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
				if($this->unit['type_slug'] == $this->node_type_slug){
					$organization['id_organization_unit'] = $this->unit['id'];
				}else{
					$organization['id_organization_unit_'.$n2] = $this->unit['id'];
				}
				$organization['organization_name_'.$n2] = $this->unit['name'];
			}

			// khusus sitoni
			$data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
			//---------------------------------
			$data['user_unit_level'] = $this->unit['type_level'];
			$data['node_type_level'] = $this->node_type_level;
			$data['node_type_slug'] = $this->node_type_slug;
			$data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id']);

			$get_units = $this->memberships_m->get_units_by_member(1);
			if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
				unset($organization['id_organization_unit']);
				unset($organization['organization_name_1']);
			}

			$data['organization'] = $organization;
			if(count($get_units) > 1 && $this->input->get('f-tipe_laporan') == 2){
				$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
			}
			$series = array();
			$temp_arr = array();
			foreach ($data['data']['entries'] as $key => $value) {
				$temp_arr['name'] = $value['komoditas'];
				$temp_arr['type'] = 'spline';
				$temp_arr['tooltip']['valueSuffix'] = ' Rp';
				$temp_arr['yAxis'] = 0;

				if($value['satuan']=='Kg') {
					$temp_arr['yAxis'] = 1;
					$temp_arr['type'] = 'column';
					$temp_arr['tooltip']['valueSuffix'] = 'Kg';
				}
				$temp_arr['data'] = array();
				array_push($temp_arr['data'], ($value['minggu35'] != NULL) ? intval($value['minggu35']) : 0);
				array_push($temp_arr['data'], ($value['minggu28'] != NULL) ? intval($value['minggu28']) : 0);
				array_push($temp_arr['data'], ($value['minggu21'] != NULL) ? intval($value['minggu21']) : 0);
				array_push($temp_arr['data'], ($value['minggu14'] != NULL) ? intval($value['minggu14']) : 0);
				array_push($temp_arr['data'], ($value['minggu7'] != NULL) ? intval($value['minggu7']) : 0);
				array_push($temp_arr['data'], ($value['harga_minggu_ini'] != NULL) ? intval($value['harga_minggu_ini']) : 0);
				$series[$key] = $temp_arr;
			}

			$data['json_series'] = json_encode($series);

			unset($data['provinsi']);
			unset($data['org_name']);
			unset($data['user_unit_level']);
			unset($data['node_type_level']);
			unset($data['node_type_slug']);
			unset($data['child_organization']);
			unset($data['organization']);

			if($graph == 0) {
				$result = array();
				foreach ($data['data']['entries'] as $key => $entries) {
					$mingguini = ($entries['harga_minggu_ini'] != "") ? $entries['harga_minggu_ini'] : 0;
					$tigabulanlalu = ($entries['harga3bln']) ? $entries['harga3bln'] : 0;
					$minggulalu = ($entries['minggu7']) ? $entries['minggu7'] : 0;

					if($mingguini > $tigabulanlalu){
						$color1 = 'red';
						$text1 = 'Naik';
						$silit1 = $mingguini-$tigabulanlalu;
						$silit1 = ($silit1 == 0) ? 0 : ($silit1/$mingguini)*100;
						$silit1 = round($silit1,2);
					}else{
						$color1 = '#E4A000';
						$text1 = 'Turun';

						$silit1 = $tigabulanlalu-$mingguini;
						$silit1 = ($silit1 == 0) ? 0 : ($silit1/$tigabulanlalu)*100;
						$silit1 = round($silit1,2);
					}

					if($mingguini > $minggulalu){
						$color2 = 'red';
						$text2 = 'Naik';
						$silit2 = $mingguini-$minggulalu;
						$silit2 = ($silit2 == 0) ? 0 : ($silit2/$mingguini)*100;
						$silit2 = round($silit2,2);
					}else{
						$color2 = '#E4A000';
						$text2 = 'Turun';
						$silit2 = $minggulalu-$mingguini;
						$silit2 = ($silit2 == 0) ? 0 : ($silit2/$minggulalu)*100;
						$silit2 = round($silit2,2);
					}

					if($mingguini == $tigabulanlalu){
						$color1 = 'red';
						$text1 = 'Stabil';
					}

					if($mingguini == $minggulalu){
						$color2 = 'red';
						$text2 = 'Stabil';
					}

					if($entries['fluktuasi'] != 0){
						$fluktuasi = round($entries['fluktuasi'] / $mingguini, 2).'%';
					}else{
						$fluktuasi = '0%';
					}
					$result[] = array(
						'id_komoditas' => $entries['id'],
						'komoditas' => $entries['komoditas'],
						'satuan' => $entries['satuan'],
						"minggu_ke" => $data['minggu_ke'],
						"thn_bln" => $data['thn_bln'],
						"to_last_week" => number_format($entries['akumulasi'],0,',','.'),
						"this_week" => number_format($entries['harga_minggu_ini'],0,',','.'),
						"total_to_this_week" => number_format($entries['total'],0,',','.'),
						"last_week_to_this_week" => $text2. " ".$silit2."%",
						"fluktuasi" => $fluktuasi
						);
				}

				$status = "200";
			} else {
				$result = json_decode($data['json_series']);
				$status = "200";
			}

			if(empty($result))
			{
				$result = array("status"=>"error","messages"=>"Graph data not found");
				$status = "200";
			}

			_output($result,$status);
		}
	}

	public function insert_laporan()
	{
		header('Access-Control-Allow-Headers: Content-Type');
        header('Access-Control-Allow-Methods: POST, OPTIONS');
        $params = json_decode(file_get_contents('php://input'),true);
        $_POST = $params;
		$this->authorize_api_access('laporan','api_laporan');

		$method = 'new';

		if($this->input->post()) {
			$id = ($this->input->post('tipe_laporan')) ? $this->input->post('tipe_laporan') : NULL;
			$_POST['id_absen'] = $this->absen_m->get_absen_by_date($today = date('Y-m-d H:i:s'), $id)['id'];

			$this->form_validation->set_rules('id_user','id_user','required|callback_check_user');
			$this->form_validation->set_rules('id_organization_unit','id_organization_unit','required|callback_check_organization');
			$this->form_validation->set_rules('id_absen','id_absen','required|callback_check_absen');
			$this->form_validation->set_rules('tipe_laporan','tipe_laporan','required|callback_check_tipe_laporan');
			if($method == 'new'){

				$values = $this->input->post();
				$nama_laporan = $this->tipe_m->get_tipe_by_id($values['tipe_laporan'])['nama_laporan'];
				$metadata =$this->metadata_m->get_metadata_by_tipe_laporan($this->input->post('tipe_laporan'), 1, 'default_laporan_metadata.*',NULL,'urutan');

				if($values['id_organization_unit'] < 0){
					$this->form_validation->set_rules('id_organization_unit2', $nama_laporan, 'required');
				}

			}else{

				$id_absen = $this->input->post('id_absen');
				$id_unit = $this->input->post('id_unit');
				$id_user = $this->input->post('id_user');
				$id_laporan_tipe = $this->input->post('id_laporan_tipe');
				$metadata = $this->data_m->get_input_laporan_by_id($id_unit, $id_user, $id_absen, $id_laporan_tipe);
			}

			foreach ($metadata as $key=> $data){
				$nilai_min = ($data['nilai_minimal'] != "") ? $data['nilai_minimal'] : 0;
				$nilai_max = ($data['nilai_maksimal'] != "") ? $data['nilai_maksimal'] : 1000000000;
				$this->form_validation->set_rules($data['field'], $data['nama'], 'required|is_natural|callback_check_equal_greater['.$nilai_min.']|callback_check_equal_less['.$nilai_max.']');
			}

			// Set Error Delimns
			$this->form_validation->set_error_delimiters('<div>', '</div>');

			$result = false;

			if ($this->form_validation->run() === true)
			{
				if ($method == 'new')
				{
					$wheres['id_unit'] = $values['id_organization_unit'];
					$wheres['id_user'] = $values['id_user'];
					$wheres['is_ttic'] = 0;
					$wheres['id_absen'] = $values['id_absen'];
					$cek_input = $this->data_m->get_data_input_by_user($values['tipe_laporan'], 1, $wheres);
					if(count($cek_input) > 0){
						$this->data_m->delete_data_input_by_user($values['tipe_laporan'], 1, $wheres);
					}

					foreach ($metadata as $key => $data) {
						$data_insert['value'] = $this->input->post($data['field']);
						$data_insert['channel'] = 'android';
						$data_insert['id_metadata'] = $data['id'];
						$data_insert['id_absen'] = $values['id_absen'];
						$data_insert['id_user'] = $values['id_user'];
						$data_insert['id_unit'] = $values['id_organization_unit'];
						$data_insert['id_group'] = 7;
						$data_insert['is_ttic'] = 0;
						$this->data_m->insert_data($data_insert);
					}
					$result = true;
				}
				else
				{
					$where['id_absen'] = $id_absen;
					$where['id_user'] = $id_user;
					$where['id_unit'] = $id_unit;
					foreach ($metadata as $key => $data) {
						if($this->input->post($data['field']) != $data['value']){
							$where['id_metadata'] = $data['id'];
							$values['value'] = $this->input->post($data['field']);
							$this->data_m->update_data($values, $where);
						}
					}
					$result = true;
				}
			}

			if($result) {
				$result = array('status'=>'success','messages'=>lang('laporan:data:submit_success'));
				$status = '200';
			} else {
				$validation_errors = explode("<div>", validation_errors());
				foreach ($validation_errors as $key => $value) {
					$value = trim($value);
					if(empty($value)) {
						unset($validation_errors[$key]);
					} else {
						$validation_errors[$key] = $value;
					}
				}
				$result = array('status'=>'error','messages'=>$validation_errors);
				$status = '200';
			}
		} else {
			$result = array('status'=>'error','messages'=>'Form action tidak sesuai');
			$status = '200';
		}

		_output($result,$status);
	}

	public function kinerja_pendamping()
	{
		$this->load->helper('laporan');

		$surffix = '';
		if($_SERVER['QUERY_STRING']){
			$surffix = '?'.$_SERVER['QUERY_STRING'];
		}

		if($this->input->get('provinsi')) {
			$_GET['f-provinsi'] = $this->input->get('provinsi');
			unset($_GET['provinsi']);
		}

		if($this->input->get('kota')) {
			$_GET['f-kota'] = $this->input->get('kota');
			unset($_GET['kota']);
		}

		if($this->input->get('thn')) {
			$_GET['f-thn'] = $this->input->get('thn');
			unset($_GET['thn']);
		}

		if($this->input->get('bln')) {
			$_GET['f-bln'] = $this->input->get('bln');
			unset($_GET['bln']);
		}

		if($this->input->get('minggu_ke')) {
			$_GET['f-minggu_ke'] = $this->input->get('minggu_ke');
			unset($_GET['minggu_ke']);
		}

		$weeks = getWeeks(date('Y-m-d'), 'monday');

		$bln = (strlen($weeks['bulan']) == 1) ? '0'.$weeks['bulan'] : $weeks['bulan'];
		$thn_bln = $weeks['tahun'].'-'.$bln;
		if($this->input->get('f-bln') && $this->input->get('f-thn')){
			$thn_bln = $this->input->get('f-thn')."-".$this->input->get('f-bln');
		}

		$date = date($thn_bln.'-01');
		if($this->input->get('f-thn') != '' && $this->input->get('f-bln') != ''){
			$date = $this->input->get('f-thn').'-'.$this->input->get('f-bln').'-01';
		}

		$end_week = get_count_of_week($date);
		$data['end_week'] = $end_week;

		$minggu_ke = (string) $weeks['week'];
		if($this->input->get('f-minggu_ke')){
			$minggu_ke = $this->input->get('f-minggu_ke');
		}
		$data['minggu_ke'] = $minggu_ke;

		$ex_thn_bln = explode('-',$thn_bln);
		$data['thn'] = $ex_thn_bln[0];
		$data['bln'] = $ex_thn_bln[1];

		$firstDayOfWeek = $this->absen_m->get_first_day_of_week($minggu_ke, $thn_bln);
		$data['firstDayOfWeek'] = $firstDayOfWeek;
		$data['weeks'] = getWeeks($firstDayOfWeek, 'monday');
		$data['min_year'] = $this->absen_m->get_min_year();
		$data['max_year'] = $this->absen_m->get_max_year();
		$data['thn_bln'] = $thn_bln;


		// -------------------------------------
		// Pagination
		// -------------------------------------
		// $count_pendamping = $this->memberships_m->get_membership();
		$count_pendamping = $this->pendamping_m->get_pendamping();
		$pagination_config['base_url'] = base_url(). 'laporan/data/kinerja_pendamping/';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($count_pendamping);
		$pagination_config['per_page'] = 10;
		// $pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

		// -------------------------------------
		// Get entries
		// -------------------------------------

		// $data['pendamping']['entries'] = $this->memberships_m->get_membership($pagination_config);
		$data['pendamping']['entries'] = $this->pendamping_m->get_pendamping($pagination_config, $minggu_ke, $thn_bln);
		$data['pendamping']['total'] = count($count_pendamping);
		$data['pendamping']['pagination'] = $this->pagination->create_links();

		// dump($data['pendamping']['entries']);
		// die();

		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		$filter_kota = null;
		if($this->input->get('f-provinsi') != '') {
			$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
			$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		}

		// Set Location
		$filter_kota = null;
		if($this->input->get('f-provinsi') != '') {
			$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
			$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
		}

		$data['get_location'] = '';
		$id_provinsi = ($this->input->get('f-provinsi') != '') ? $this->input->get('f-provinsi') : NULL;
		if($id_provinsi != NULL){
			$provinsi = $this->provinsi_m->get_provinsi_by_id($id_provinsi)['nama'];
			$kota = "";
			if($this->input->get('f-kota') != ''){
				$kota = ", ".$this->kota_m->get_kota_by_id($this->input->get('f-kota'))['nama'];
			}
			$data['get_location'] .= '<br>Provinsi '.$provinsi.$kota;
		}

		$data['tipes'] = $this->tipe_m->get_tipe();
		$data['metadata_gap'] = $this->metadata_m->get_metadata_by_tipe_laporan(1, 1, 'default_laporan_metadata.*', NULL, 'urutan');
		$data['metadata_tti'] = $this->metadata_m->get_metadata_by_tipe_laporan(2, 1, 'default_laporan_metadata.*', NULL, 'urutan');

		$result  = array();

		foreach ($data['pendamping']['entries'] as $key => $value) {
			foreach ($value['gapoktan'] as $key2 => $value2) {
				$data['pendamping']['entries'][$key]['gapoktan'][$key2]['metadata_gapoktan'] = $data['metadata_gap'];

				$arr_metadata_gap = $value['metadata_gapoktan'];
				foreach ($data['metadata_gap'] as $key_gap => $value_gap) {
					if(isset($arr_metadata_gap[$value_gap['id']])){
	    				$data['pendamping']['entries'][$key]['gapoktan'][$key2]['metadata_gapoktan'][$key_gap]['total'] = number_format($arr_metadata_gap[$value_gap['id']]['total'], 0);
	    			}else{
	    				$data['pendamping']['entries'][$key]['gapoktan'][$key2]['metadata_gapoktan'][$key_gap]['total'] = '-';
			    	}
				}

				unset($data['pendamping']['entries'][$key]['metadata_gapoktan']);

				foreach ($value2['tti'] as $key3 => $value3) {
					$data['pendamping']['entries'][$key]['gapoktan'][$key2]['tti'][$key3]['metadata_tti'] = $data['metadata_tti'];

					$arr_metadata_tti = $value3['metadata_tti'];
                    foreach ($data['metadata_tti'] as $keym => $metadata) {
                      if(isset($arr_metadata_tti[$metadata['id']])){
                        $data['pendamping']['entries'][$key]['gapoktan'][$key2]['tti'][$key3]['metadata_tti'][$keym]['total'] = number_format($arr_metadata_tti[$metadata['id']]['total'], 0);
                      }else{
                      	$data['pendamping']['entries'][$key]['gapoktan'][$key2]['tti'][$key3]['metadata_tti'][$keym]['total'] = '-';
                      }
                    }
				}
			}
		}
		$result = $data['pendamping']['entries'];

		$status = "200";

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>"Performance report data not found");
			$status = "200";
		}

		_output($result,$status);
	}

	public function check_equal_less($value,$max_value)
	{
		if ($value > $max_value)
		{
			$this->form_validation->set_message('check_equal_less', 'Nilai maksimal <b>%s</b> adalah '.$max_value);
			return false;
		}
		else
		{
			return true;
		}
	}

	public function check_equal_greater($value,$min_value)
	{
		if ($value < $min_value)
		{
			$this->form_validation->set_message('check_equal_greater', 'Nilai minimal <b>%s</b> adalah '.$min_value);
			return false;
		}
		else
		{
			return true;
		}
	}

	public function check_organization($id_organization_unit)
	{
		$unit = $this->units_m->get_units_by_id($id_organization_unit);

		if(isset($unit) AND property_exists($unit, 'unit_type')) {
			if($unit->unit_type!=$this->input->post('tipe_laporan')) {
				$this->form_validation->set_message('check_organization', 'Data organisasi tidak ditemukan');
				return false;
			}

			if(!$this->organization->is_member($this->input->post('id_user'),$unit->id)) {
				$this->form_validation->set_message('check_organization', 'User bukan merupakan member dari organisasi yang dimasukkan');
				return false;
			}
		} else {
			$this->form_validation->set_message('check_organization', 'Data organisasi tidak ditemukan');
			return false;
		}
		return true;
	}

	public function check_user($id_user)
	{
		$this->load->model('users/user_m');
		$user = $this->user_m->get(array('id'=>$id_user,'role'=>7));

		if(!$user) {
			$this->form_validation->set_message('check_user', 'Data user tidak ditemukan');
			return false;
		}

		return true;
	}

	public function check_absen($id_absen)
	{
		$absen = $this->absen_m->get_absen_by_id($id_absen);

		if(!$absen) {
			$this->form_validation->set_message('check_absen', 'Data absen tidak ditemukan');
			return false;
		}
		return true;
	}

	public function check_tipe_laporan($tipe_laporan)
	{
		if(!in_array($tipe_laporan, array(1,2))) {
			$this->form_validation->set_message('check_tipe_laporan', 'Tipe laporan tidak ditemukan');
			return false;
		}
		return true;
	}

	public function get_required_data($id_user)
	{
		$data['tipes'] = $this->tipe_m->get_tipe();
		$user = $this->ion_auth->get_user($id_user);

		$data['unit_types'] = $this->types_m->get_all_types();
		$memberships_user = $this->memberships_m->get_memberships_by_user($id_user);
		$type_ids=array();

		foreach ($memberships_user as $key => $member) {
			$type_ids[]=$member['unit_type_id'];
		}
		$data['type_ids'] = $type_ids;
		if($user->group_id == 8){
			$location = (array)$this->user_m->get(array('id'=>$id_user));
		}else{
			$location = $this->memberships_m->get_units_by_member($id_user)[0];
		}

		$data['id_provinsi'] = $location['id_provinsi'];
		$data['id_kota'] = (isset($data['fields']['id_kota'])) ? $data['fields']['id_kota'] : null;

		if($this->input->get('tipe_laporan')){

			// Cek Tanggal Pelaporan
			$today = date('Y-m-d H:i:s');
			$data['absen'] = $this->absen_m->cek_tgl_input($today, $this->input->get('tipe_laporan'));

			$dates = $this->absen_m->get_dates();
			$data['addDates'] = (count($dates) > 0) ? implode(',', $dates) : '';

			// Tampung data GET
			$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : $data['id_provinsi'];
			$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : $data['id_kota'];
			$data['id_unit'] = ($this->input->get('f-id_unit')) ? $this->input->get('f-id_unit') : NULL;

			if(count($data['absen']) > 0) {
				$tipe_laporan = $this->input->get('tipe_laporan');
				$data['tipe_laporan'] = $tipe_laporan;
				$data['nama_laporan'] = $this->tipe_m->get_tipe_by_id($tipe_laporan)['nama_laporan'];
				dump($user->group_id);
				if(group_has_role('laporan','create_laporan', $user->group_id)) {

					// Load location
					$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

					$filter_kota = NULL;
					if($this->input->post('provinsi') != ''){
						$data['id_provinsi'] = $this->input->post('provinsi');
					}

					if($this->input->post('kota') != ''){
						$data['id_kota'] = $this->input->post('kota');
					}

				}elseif(group_has_role('laporan','create_own_prov_laporan', $user->group_id)){
					$data['id_provinsi'] = $user->id_provinsi;
					$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
				}

				$filter_kota['id_provinsi'] = $data['id_provinsi'];
				$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);

				$data['metadata'] = $this->metadata_m->get_metadata_by_tipe_laporan($tipe_laporan, 1, 'default_laporan_metadata.*',NULL,'urutan');
			}

			// -------------------------------------
			// Get Organization by Member
			// -------------------------------------

			$data['types'] = $this->types_m->get_all_types();

			$unit_id = $this->unit['id'];

			$types = $data['types'];
			$organization = array();
			$addedLevel = array();
			$n2 = 0;
			$n = count($types);

			foreach($types as $i => $type){
				if (in_array($type['level'], $addedLevel)) {
					continue;
				}
				$addedLevel[] = $type['level'];
				$units = $this->units_m->get_unit_by_child($unit_id);

				if(isset($units['id'])){
					if($units['type_slug'] != $this->node_type_slug && $units['type_level'] < $this->node_type_level){
						$organization['organization_name_'.$units['type_level']] = $units['unit_name'];
						$organization['id_organization_unit_'.$units['type_level']] = $units['id'];
						$n2++;
					} else if ($units['type_slug'] == $this->node_type_slug) {
						$organization['organization_name_'.$units['type_level']] = $units['unit_name'];
						$organization['id_organization_unit'] = $units['id'];
					}
					$unit_id = $units['id'];
				}
			}

			if($this->unit['type_slug'] == $this->node_type_slug || $this->unit['type_level'] < $this->node_type_level){
				if($this->unit['type_slug'] == $this->node_type_slug){
					$organization['id_organization_unit'] = $this->unit['id'];
				}else{
					$organization['id_organization_unit_'.$n2] = $this->unit['id'];
				}
				$organization['organization_name_'.$n2] = $this->unit['name'];
			}

			$get_units = $this->memberships_m->get_units_by_member($id_user);
			if(count($get_units) > 1 && $this->input->get('tipe_laporan') == 2){
				unset($organization['id_organization_unit']);
				unset($organization['organization_name_1']);
			}

			// khusus sitoni
			$data['org_name'] = ($i > 0) ? 'id_organization_unit_0' : 'id_organization_unit' ;
			//---------------------------------

			$data['node_type_slug'] = $this->node_type_slug;
			$data['organization'] = $organization;
			// dump($this->unit);
			$data['child_organization'] = $this->unit['id'] == '' ? $this->units_m->get_units_by_level(0) : $this->units_m->get_units_by_parent($this->unit['id'], 1);

			if(count($get_units) > 1 && $this->input->get('tipe_laporan') == 2){
				$data['child_organization'] = $this->units_m->get_units_by_parent($organization['id_organization_unit_0'], 1);
			}
		}

		unset($data['tipes']);
		unset($data['unit_types']);
		unset($data['type_ids']);
		unset($data['types']);
		unset($data['org_name']);
		unset($data['node_type_slug']);
		unset($data['organization']);
		unset($data['child_organization']);

		dump($data);
		die("aklnsdkansld");
	}
}
