<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Laporan_dialog extends Public_Controller
{
	// -------------------------------------
	// This will set the active section tab
	// -------------------------------------
	
	protected $section = 'dialog';
	
  public function __construct()
  {
    parent::__construct();

    // -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('laporan');
		
		$this->load->model('dialog_m');
  }
	
	/**
   * Insert or update dialog entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_dialog($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('laporan:nama'), 'required|max_length[25]');
		$this->form_validation->set_rules('email', lang('laporan:email'), 'required|valid_email');
		$this->form_validation->set_rules('kontak', lang('laporan:nomor_kontak'), 'required|is_natural|min_length[8]|max_length[15]');
		$this->form_validation->set_rules('isi', lang('laporan:isi_dialog'), 'required');

		if(strlen($values['isi']) > 200){
			$this->form_validation->set_rules('isi', lang('laporan:isi_dialog'), 'is_natural');
			$this->form_validation->set_message('is_natural', lang('laporan:dialog:to_much_value'));
		}
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$values['created_on'] = date("Y-m-d H:i:s");
				$values['status'] = 0;
				$result = $this->dialog_m->insert_dialog($values);
			}
			else
			{
				$result = $this->dialog_m->update_dialog($values, $row_id);
			}
		} else {
			if($this->input->is_ajax_request()){
				header('Content-Type: application/json',TRUE,406);
				die (validation_errors());
			}

		}
		
		return $result;
	}

	public function ajax_add() {
  	if ($_POST) {
      if ($this->_update_dialog('new')) {
        die('' . lang('laporan:dialog:submit_success'));
      }
  	}  
	}

	// --------------------------------------------------------------------------

}