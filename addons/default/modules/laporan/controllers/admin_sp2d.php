<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_sp2d extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'sp2d';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'access_sp2d_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('laporan');		
    $this->lang->load('location/location');		
		$this->load->model('sp2d_m');
		$this->load->model('location/provinsi_m');
  }

  /**
	 * List all sp2d
   *
   * @return	void
   */
  public function index()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_sp2d') && ! group_has_role('laporan', 'view_own_sp2d') && ! group_has_role('laporan', 'view_own_prov_sp2d')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		if(group_has_role('laporan','view_all_sp2d')) {
			$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
  	}elseif(group_has_role('laporan','view_own_prov_sp2d')){
  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
  	}
		
		$surffix = '';
	  if($_SERVER['QUERY_STRING']){
	    $surffix = '?'.$_SERVER['QUERY_STRING'];
	  }
		// -------------------------------------
		// Pagination
		// -------------------------------------

  	$total_rows = count($this->sp2d_m->get_sp2d(NULL, $data['id_provinsi']));
		$pagination_config['base_url'] = base_url(). 'admin/laporan/sp2d/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $total_rows;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['per_page'] = Settings::get('records_per_page');
		// $pagination_config['per_page'] = 2;
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

		$data['uri'] = $this->get_query_string(5);
		
    // -------------------------------------
		// Get entries
		// -------------------------------------
		
    $data['sp2d']['entries'] = $this->sp2d_m->get_sp2d($pagination_config, $data['id_provinsi']);
		$data['sp2d']['total'] = $total_rows;
		$data['sp2d']['pagination'] = $this->pagination->create_links();


		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:sp2d:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:sp2d:plural'))
			->build('admin/sp2d_index', $data);
  }

  public function get_query_string($page){
		// -------------------------------------
		// Get query string
		// -------------------------------------

		$page = $this->uri->segment($page);
		$uri = '';
    if($page){
        $uri = '/'.$page;
    }
    if($_SERVER['QUERY_STRING']){
        $uri = '?'.$_SERVER['QUERY_STRING'];
    }
    if($_SERVER['QUERY_STRING'] && $page){
        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
    }
    
    return $uri;
	}
	
	/**
   * Display one sp2d
   *
   * @return  void
   */
  public function view($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_sp2d') AND ! group_has_role('laporan', 'view_own_sp2d') AND ! group_has_role('laporan', 'view_own_prov_sp2d')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
    $data['sp2d'] = $this->sp2d_m->get_sp2d_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'view_all_sp2d')){
			if(! group_has_role('laporan', 'view_own_prov_sp2d')){
				if($data['sp2d']['created_by'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}

			if(! group_has_role('laporan', 'view_own_sp2d')){
				if(user_provinsi($this->current_user->id) != $data['sp2d']['id_provinsi']){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		$data['uri'] = $this->get_query_string(6);
		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:sp2d:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:sp2d:plural'), '/admin/laporan/sp2d/index')
			->set_breadcrumb(lang('laporan:sp2d:view'))
			->build('admin/sp2d_entry', $data);
  }
	
	/**
   * Create a new sp2d entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'create_sp2d') && ! group_has_role('laporan', 'create_own_prov_sp2d')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
  	$data['uri'] = $this->get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			$return = $this->_update_sp2d('new');
			if($return == 'success'){	
				$this->session->set_flashdata('success', lang('laporan:sp2d:submit_success'));				
				redirect('admin/laporan/sp2d/index'.$data['uri']);
			}else{
				if($return == FALSE){
					$data['messages']['error'] = lang('laporan:sp2d:submit_failure');
				}else{
					$data['messages']['error'] = $return;
				}
			}
		}
		
		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		if(group_has_role('laporan','view_all_sp2d')) {
			$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
  	}elseif(group_has_role('laporan','view_own_prov_sp2d')){
  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
  	}
  	
		$data['mode'] = 'new';
		$data['return'] = 'admin/laporan/sp2d/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('laporan:sp2d:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:sp2d:plural'), '/admin/laporan/sp2d/index')
			->set_breadcrumb(lang('laporan:sp2d:new'))
			->build('admin/sp2d_form', $data);
  }
	
	/**
   * Edit a sp2d entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the sp2d to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'edit_all_sp2d') AND ! group_has_role('laporan', 'edit_own_sp2d') AND ! group_has_role('laporan', 'edit_own_prov_sp2d')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('laporan', 'edit_all_sp2d')){
			$entry = $this->sp2d_m->get_sp2d_by_id($id);
			if(! group_has_role('laporan', 'edit_own_prov_sp2d')){
				if($entry['created_by'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}

			if(! group_has_role('laporan', 'edit_own_sp2d')){
				if(user_provinsi($this->current_user->id) != $entry['id_provinsi']){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}


		if($entry['status'] > 1){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}


  	$data['uri'] = $this->get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_sp2d('edit', $id)){	
				$this->session->set_flashdata('success', lang('laporan:sp2d:update_success'));				
				redirect('admin/laporan/sp2d/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('laporan:sp2d:update_failure');
			}
		}

		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		if(group_has_role('laporan','view_all_sp2d')) {
			$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();
  	}elseif(group_has_role('laporan','view_own_prov_sp2d')){
  		$data['id_provinsi'] = user_provinsi($this->current_user->id);
  		$data['nama_provinsi'] = $this->provinsi_m->get_provinsi_by_id($data['id_provinsi'])['nama'];
  	}
		
		$data['fields'] = $this->sp2d_m->get_sp2d_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/laporan/sp2d/index'.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:sp2d:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('laporan:sp2d:plural'), '/admin/laporan/sp2d/index')
			->set_breadcrumb(lang('laporan:sp2d:view'), '/admin/laporan/sp2d/view/'.$id)
			->set_breadcrumb(lang('laporan:sp2d:edit'))
			->build('admin/sp2d_form', $data);
  }
	
	/**
   * Delete a sp2d entry
   * 
   * @param   int [$id] The id of sp2d to be deleted
   * @return  void
   */
  public function delete($id = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'delete_all_sp2d') AND ! group_has_role('laporan', 'delete_own_sp2d') AND ! group_has_role('laporan', 'delete_own_prov_sp2d')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('laporan', 'delete_all_sp2d')){
			$entry = $this->sp2d_m->get_sp2d_by_id($id);
			if(! group_has_role('laporan', 'delete_own_prov_sp2d')){
				if($entry['created_by'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}

			if(! group_has_role('laporan', 'delete_own_sp2d')){
				if(user_provinsi($this->current_user->id) != $entry['id_provinsi']){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		if($entry['status'] > 1){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		$uri = $this->get_query_string(6);
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		$this->load->library('files/files');
		Files::delete_file($entry['file']);
    $this->sp2d_m->delete_sp2d_by_id($id);
    $this->session->set_flashdata('error', lang('laporan:sp2d:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
    redirect('admin/laporan/sp2d/index'.$uri);
  }
	
	/**
   * Insert or update sp2d entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_sp2d($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('id_provinsi', lang('location:provinsi:singular'), 'required');
		$this->form_validation->set_rules('nama_sp2d', lang('laporan:nama_sp2d'), 'required|max_length[50]');
		$this->form_validation->set_rules('deskripsi', lang('laporan:deskripsi'), 'required');

		$file_name = $_FILES['file']['name'];
		if($method != 'edit'){
			if(empty($file_name)){
				$this->form_validation->set_rules('file', lang('laporan:file'), 'required');
			}
		}
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$return = false;

		$this->load->library('files/files');
		$folder = Files::get_id_by_name('SP2D');
		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$allowed_type = 'pdf|jpg|jpeg';
				$upload = Files::upload($folder, $_FILES['file']['name'], 'file', FALSE, FALSE, FALSE, $allowed_type);
				$values['file'] = $upload['data']['id'];
				$values['status'] = 1;

				if($upload['status']){
					$result = $this->sp2d_m->insert_sp2d($values);
					$return = 'success';
				}else{
					$return = $upload['message'];
					return $return;
				}
				
			}
			else
			{
				if(!empty($file_name)){
					$upload = Files::upload($folder, $_FILES['file']['name'], 'file', FALSE, FALSE, FALSE, $allowed_type);
					if(!$upload['status']){
						$return = $upload['message'];
						return $return;
					}else{
						$values['file'] = $upload['data']['id'];
						Files::delete_file($values['old_file']);
					}
				}else{
					$values['file'] = $values['old_file'];
				}

				unset($values['old_file']);
				$values['updated_by'] = $this->current_user->id;
				$values['updated_on'] = date("Y-m-d H:i:s");
				$result = $this->sp2d_m->update_sp2d($values, $row_id);
				if($result){
					$return = 'success';
				}
			}
		}
		
		return $return;
	}

	public function ajax_edit($mode, $row_id = null) {

    // -------------------------------------
    // Check permission
    // -------------------------------------

		if($mode == 'edit'){
			if (!group_has_role('laporan', 'change_sp2d_status')) {
        die('' . lang('cp:access_denied'));
      }
	  }

		    // -------------------------------------
		    // Process POST input
		    // -------------------------------------

    if ($_POST) {
      if ($this->_update_state($mode, $row_id)) {
        die('' . lang('laporan:sp2d:change_success'));
      }else{
      	die('' . lang('laporan:sp2d:change_failure'));
      }
    }
	}

	private function _update_state($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();
		
		$values['changed_on'] = date("Y-m-d H:i:s");
		$values['changed_by'] = $this->current_user->id;

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('status', lang('laporan:state'), 'required');
		// $this->form_validation->set_rules('catatan', lang('laporan:catatan'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->state_m->insert_state($values);
				
			}
			else
			{
				$result = $this->sp2d_m->update_sp2d($values, $row_id);
			}
		} else {
			if($this->input->is_ajax_request()){
				header('Content-Type: application/json',TRUE,406);
				die (validation_errors());
			}

		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}