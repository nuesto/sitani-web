<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Laporan Module
 *
 * Modul yang me-manage laporan
 *
 */
class Admin_tipe_field extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'tipe_field';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'access_tipe_field_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('laporan');		
		$this->load->model('tipe_field_m');
  }

  /**
	 * List all tipe_field
   *
   * @return	void
   */
  public function index()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_tipe_field')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		

    // -------------------------------------
		// Get entries
		// -------------------------------------

		$data['tipe_field']['entries'] = $this->tipe_field_m->get_tipe_field();
		$data['tipe_field']['total'] = count($data['tipe_field']['entries']);

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('laporan:tipe_field:plural'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:tipe_field:plural'))
			->build('admin/tipe_field_index', $data);
  }

  /**
   * Create a new tipe_field entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
  public function create()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_tipe_field')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_tipe_field('new')){	
				$this->session->set_flashdata('success', lang('laporan:tipe_field:submit_success'));				
				redirect('admin/laporan/tipe_field/index');
			}else{
				$data['messages']['error'] = lang('laporan:tipe_field:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/laporan/tipe_field/index';

		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:tipe_field:new'))
    	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:tipe_field:plural'), '/admin/laporan/tipe_field/index')
			->set_breadcrumb(lang('laporan:data:new'))
			->build('admin/tipe_field_form', $data);
  }
	
	/**
   * Edit a tipe_field entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the tipe_field to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_tipe_field')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		if($_POST){
			if($this->_update_tipe_field('edit', $id)){	
				$this->session->set_flashdata('success', lang('laporan:tipe_field:submit_success'));				
				redirect('admin/laporan/tipe_field/index');
			}else{
				$data['messages']['error'] = lang('laporan:tipe_field:submit_failure');
			}
		}

		$data['fields'] = $this->tipe_field_m->get_tipe_field_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/laporan/tipe_field/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('laporan:tipe_field:edit'))
			->set_breadcrumb('Dasbor', '/admin')
			->set_breadcrumb(lang('laporan:tipe_field:plural'), '/admin/laporan/tipe_field/index')
			->set_breadcrumb(lang('laporan:tipe_field:edit'))
			->build('admin/tipe_field_form', $data);
  }
	
	/**
   * Delete a tipe_field entry
   * 
   * @param   int [$id] The id of tipe_field to be deleted
   * @return  void
   */
  public function delete($id = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('laporan', 'manage_tipe_field')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
	
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
    $this->tipe_field_m->delete_tipe_field_by_id($id);
    $this->session->set_flashdata('error', lang('laporan:tipe_field:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
    redirect('admin/laporan/tipe_field/index');
  }
	
	/**
   * Insert or update tipe_field entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_tipe_field($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama_tipe_field', lang('laporan:nama_tipe_field'), 'required|max_length[45]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->tipe_field_m->insert_tipe_field($values);
				
			}
			else
			{
				$result = $this->tipe_field_m->update_tipe_field($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}