<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Metadata model
 *
 * @author Aditya Satrya
 */
class Metadata_m extends MY_Model {
	
	public function get_metadata($pagination_config = NULL)
	{
		$this->db->select('m.*, k.nama_komoditas, t.nama_laporan, f.nama_tipe_field');
		$this->db->from('default_laporan_metadata m');
		$this->db->join('default_laporan_komoditas k','k.id = m.id_laporan_komoditas','left');
		$this->db->join('default_laporan_tipe t','t.id = m.id_laporan_tipe');
		$this->db->join('default_laporan_tipe_field f','f.id = m.id_laporan_tipe_field','left');
		$this->db->order_by('k.urutan, t.nama_laporan, m.urutan, m.id', "ASC");

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if($this->input->get('f-komoditas')){
			$this->db->where('m.id_laporan_komoditas', $this->input->get('f-komoditas'));
		}
		if($this->input->get('f-tipe_laporan')){
			$this->db->where('m.id_laporan_tipe', $this->input->get('f-tipe_laporan'));
		}else{
			$this->db->where('k.nama_komoditas IS NOT NULL');
		}
		if($this->input->get('f-field')){
			$this->db->like('field', $this->input->get('f-field'));
		}
		if($this->input->get('f-nama')){
			$this->db->like('nama', $this->input->get('f-nama'));
		}

		$query = $this->db->get();
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_metadata_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_laporan_metadata');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_metadata_by_komoditas($tipe_laporan, $id_komoditas)
	{
		$this->db->select('*');
		$this->db->where('id_laporan_tipe', $tipe_laporan);
		$this->db->where('id_laporan_komoditas', $id_komoditas);
		$query = $this->db->get('default_laporan_metadata');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_metadata_by_enum($enumerator, $select = '*', $field = NULL, $order_by = 'id')
	{
		$this->db->select($select);
		$this->db->where('enumerator', $enumerator);
		if($field!=NULL){
			if($field == 'harga'){
				$this->db->where("(field LIKE '%harga%' OR field LIKE '%hrg%')");
			}else{
				$this->db->like('field', $field);
			}
		}

		$this->db->order_by('enumerator, '.$order_by, "ASC");
		$query = $this->db->get('default_laporan_metadata');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_metadata_by_tipe_laporan2($tipe_laporan, $komoditas = 1, $select = '*', $field = NULL, $order_by = 'id')
	{
		if ($tipe_laporan == 3){
			$tipe_laporan = 1;
		}

		$this->db->select($select);
		$this->db->join('default_laporan_tipe_field','default_laporan_tipe_field.id = default_laporan_metadata.id_laporan_tipe_field');

		if($field!=NULL){
			$this->db->where("id_laporan_tipe_field",$field);
		}

		if($this->input->get('f-metadata')){
			$this->db->where("default_laporan_metadata.id", $this->input->get('f-metadata'));
		}

		$this->db->where('id_laporan_tipe', $tipe_laporan);
		$this->db->where('id_laporan_komoditas', $komoditas);
		$this->db->order_by('id_laporan_tipe, default_laporan_metadata.'.$order_by, "ASC");
		$query = $this->db->get('default_laporan_metadata');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_metadata_by_tipe_laporan3($tipe_laporan, $komoditas = NULL, $select = '*', $field = NULL, $order_by = 'id')
	{
		if ($tipe_laporan == 3){
			$tipe_laporan = 1;
		}

		$this->db->select($select);
		$this->db->join('default_laporan_tipe_field','default_laporan_tipe_field.id = default_laporan_metadata.id_laporan_tipe_field');

		if($field!=NULL){
			$this->db->where("id_laporan_tipe_field",$field);
		}

		if($this->input->get('f-metadata')){
			$this->db->where("default_laporan_metadata.id", $this->input->get('f-metadata'));
		}

		$this->db->where('id_laporan_tipe', $tipe_laporan);
		if($komoditas != NULL){
			$this->db->where('id_laporan_komoditas', $komoditas);
		}
		$this->db->order_by('id_laporan_tipe, default_laporan_metadata.'.$order_by, "ASC");
		$query = $this->db->get('default_laporan_metadata');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_metadata_by_tipe_laporan($tipe_laporan, $komoditas = 1, $select = '*', $field = NULL, $order_by = 'id')
	{
		if ($tipe_laporan == 3){
			$tipe_laporan = 1;
		}

		$this->db->select($select);
		$this->db->join('default_laporan_tipe_field','default_laporan_tipe_field.id = default_laporan_metadata.id_laporan_tipe_field');

		if($field!=NULL){
			$this->db->where("id_laporan_tipe_field",$field);
		}

		$this->db->where('id_laporan_tipe', $tipe_laporan);
		$this->db->where('id_laporan_komoditas', $komoditas);
		$this->db->order_by('id_laporan_tipe, default_laporan_metadata.'.$order_by.', default_laporan_metadata.id', "ASC");
		$query = $this->db->get('default_laporan_metadata');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_metadata_ex_komoditas($tipe_laporan, $select = '*', $order_by = 'id')
	{
		$this->db->select($select);
		$this->db->join('default_laporan_tipe_field','default_laporan_tipe_field.id = default_laporan_metadata.id_laporan_tipe_field');
		$this->db->where('id_laporan_tipe', $tipe_laporan);
		$this->db->order_by('id_laporan_tipe, default_laporan_metadata.'.$order_by.', default_laporan_metadata.id', "ASC");
		$query = $this->db->get('default_laporan_metadata');
		$result = $query->result_array();
		
		return $result;
	}

	public function is_used($id){
		$this->db->where('id_metadata', $id);
		$query = $this->db->get('default_laporan_data');
		if($query->num_rows() > 0){
			return false;
		}
		return true;
	}
	
	public function delete_metadata_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_metadata');
	}
	
	public function insert_metadata($values)
	{
		return $this->db->insert('default_laporan_metadata', $values);
	}
	
	public function update_metadata($values, $row_id)
	{
		$this->db->where('id', $row_id);
		return $this->db->update('default_laporan_metadata', $values); 
	}

	public function update_urutan($id, $id_laporan_tipe, $id_laporan_komoditas, $i){
    return $this->db->query("UPDATE default_laporan_metadata SET urutan = '$i' WHERE id = '$id' AND id_laporan_tipe='{$id_laporan_tipe}' AND id_laporan_komoditas ='{$id_laporan_komoditas}'");
  }
	
}