<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Data model
 *
 * @author Aditya Satrya
 */
class Data_m extends MY_Model {
	public function sandingan($filters){
		$this->db->select("d.id_metadata, m.nama as metadata, if(m.id_laporan_tipe_field = 1, ROUND(AVG(d.value),'0'), ROUND(SUM(d.value),'0')) AS total");
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m', 'm.id = d.id_metadata');
		$this->db->join('default_profiles p','p.user_id = d.id_user');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'k.id_provinsi = lp.id');	
		if($this->input->get('f-tipe_laporan') <=2){
			$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
		}
		
		foreach ($filters as $field => $data) {
			if($field != 'tahun' && $field != 'bulan'&& $field != 'minggu_ke'){
				$this->db->where($field, $data);
			}else{
				$this->db->where($data);
			}
		}
		$this->db->where('d.value >',0);
		$this->db->group_by('d.id_metadata');
		$this->db->order_by('k.kode', 'ASC');

		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	public function count_of_rekapitulasi_data($pagination_config = NULL, $filters){
		$this->db->select('d.id_user, d.id_unit, k.id as id_kota, d.created_on, p.display_name, k.nama AS kota, lp.id as id_provinsi, lp.nama AS provinsi');
		// if($this->input->get('f-tipe_laporan') <=2){
			$this->db->select('a.*, a.id as id_absen, a.tanggal_awal');
		// }
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m', 'm.id = d.id_metadata');
		$this->db->join('default_profiles p','p.user_id = d.id_user');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'k.id_provinsi = lp.id');	
		// if($this->input->get('f-tipe_laporan') <=2){
			$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
		// }
		
		foreach ($filters as $field => $data) {
			$this->db->where($field, $data);
		}
		$this->db->group_by('d.id_user, k.id, u.id');
		$this->db->order_by('k.kode', 'ASC');
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	public function rekapitulasi_data($komoditas, $filters, $users, $units, $days)
	{

		$this->db->select('d.id_user, k.id as id_kota, d.created_on, p.display_name, k.nama AS kota, lp.id as id_provinsi, lp.nama AS provinsi, u.id  as id_unit, u.unit_name, d.id_metadata, d.value, sum(d.value) as sum_val,avg(d.value) as avg_val, count(d.id_metadata) as count_metadata');
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m', 'm.id = d.id_metadata');
		$this->db->join('default_profiles p','p.user_id = d.id_user');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'k.id_provinsi = lp.id');
		
		// if($this->input->get('f-tipe_laporan') <=2){
			$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
		// }

		foreach ($filters as $field => $data) {
			$this->db->where($field, $data);
		}
		$this->db->where_in('d.id_user', $users);
		$this->db->where_in('d.id_unit', $units);

		// $this->db->where("(id_user, id_unit) IN (
		// 	SELECT d.id_user, d.id_unit 
		// 	FROM default_laporan_data d 
		// 	JOIN default_laporan_metadata m ON m.id = d.id_metadata
		// 	{$subquery_join} 
		// 	where d.id_user in ($users) and d.id_unit in ($units)
		// 	{$subquery_and})
		// ");

		$this->db->group_by('d.id_user, d.id_unit, d.id_metadata');
		$this->db->order_by('k.kode', 'ASC');

		$query = $this->db->get();
		$result = $query->result_array();
		// dump($this->last_query());
		// die();
		$temp = array();
		if(count($result) > 0){

			$metadatas = array();
			$tahunan = array();
			$harian = array();
			foreach ($komoditas as $i => $metadata) {
				$metadatas[$metadata['id']] = $metadata;
			}

			// $users = array();
			// $units = array();
			// foreach ($result as $k => $d) {
			// 	$users[$d['id_user']] = $d['id_user'];
			// 	$units[$d['id_unit']] = $d['id_unit'];
			// }

			// $explode_users = implode(',',$users);
			// $explode_units = implode(',',$units);

			$explode_users = $users;
			$explode_units = $units;
			$data_tahunan = $this->get_data_tahunan($explode_users, $explode_units, $filters);
			foreach($data_tahunan as $i2 => $data_tahun){
				$id_metadata = $data_tahun['id_metadata'];
				if(isset($metadatas[$id_metadata])){
					$id_field = $metadatas[$id_metadata]['id_laporan_tipe_field'];
					
					$index = $data_tahun['id_user'].'|'.$data_tahun['id_unit'];
					$agregasi = ($id_field == 1) ? 'avg' : 'sum';
					$field_tahunan = $agregasi.'thnan_'.$metadatas[$id_metadata]['nama'].'_'.$metadatas[$id_metadata]['field'];
					$tahunan[$index][$field_tahunan] = $data_tahun[$agregasi.'_val'];
				}
			}


			if(!$this->input->get('tipe_rekap')){
				if($this->input->get('page') && !isset($filters['a.tanggal']) && !isset($filters['DATE(d.created_on)']) && (isset($filters['a.minggu_ke']) || isset($filters['d.minggu_ke']))){
					$data_harian = $this->get_data_harian($explode_users, $explode_units, $filters);
					foreach ($data_harian as $key => $data_hari) {
						$id_metadata = $data_hari['id_metadata'];
						if(isset($metadatas[$id_metadata])){
							$id_field = $metadatas[$id_metadata]['id_laporan_tipe_field'];
							$field = $metadatas[$id_metadata]['field'];
							$tanggal = $data_hari['tanggal'];
							
							$index = $data_hari['id_user'].'|'.$data_hari['id_unit'].'|'.$field;
							$agregasi = ($id_field == 1) ? 'avg' : 'sum';
							$harian[$index][strtotime($tanggal)] = $data_hari[$agregasi.'_val'];
							foreach ($days as $key => $day) {
								if(!isset($harian[$index][strtotime($day)])){
									$harian[$index][strtotime($day)] = '0';
								}
							}
							ksort($harian[$index]);
						}
					}
				}
			}

			// dump($harian);

			foreach ($result as $key => $data) {
				$id_metadata = $data['id_metadata'];
				if(isset($metadatas[$id_metadata])){
					$field = $metadatas[$id_metadata]['field'];
					$nama = $metadatas[$id_metadata]['nama'];
					$id_field = $metadatas[$id_metadata]['id_laporan_tipe_field'];
					
					$index = $data['id_user'].'|'.$data['id_unit'];
					if(!isset($temp[$data['id_user'].'|'.$data['id_unit']])){
						$temp[$index] = $data;
					}
					$temp[$index][$field] = $data['sum_val'];
					if($id_field == 1){
						$temp[$index][$field] = $data['avg_val'];
					}


					$agregasi = ($id_field == 1) ? 'avg' : 'sum';
					$field_tahunan = $agregasi.'thnan_'.$metadatas[$id_metadata]['nama'].'_'.$metadatas[$id_metadata]['field'];
					if(isset($tahunan[$index][$field_tahunan])){
						$temp[$index][$field_tahunan] = $tahunan[$index][$field_tahunan];
					}

					if(!$this->input->get('tipe_rekap')){
						if($this->input->get('page') && !isset($filters['a.tanggal']) && !isset($filters['DATE(d.created_on)']) && (isset($filters['a.minggu_ke']) || isset($filters['d.minggu_ke']))){
							$index_harian = $data['id_user'].'|'.$data['id_unit'].'|'.$field;
							$temp[$index][$field] = $harian[$index_harian];
						}
					}
				}
			}
		}
    return $temp;
	}

	public function get_data_tahunan($users, $units, $filters){
  	$this->db->select("d.*, m.id_laporan_tipe_field, m.field, ROUND(sum(d.value),'0') AS sum_val");
  	$this->db->select("ROUND(avg(d.value),'0') AS avg_val");
  	$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m', 'm.id = d.id_metadata');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_kota lp', 'lp.id = k.id_provinsi');
  	// $this->db->where('d.value >', 0);

		$this->db->where_in('d.id_user', $users);
		$this->db->where_in('d.id_unit', $units);
  	// $this->db->where("(id_user, id_unit) IN (select id_user, id_unit from default_laporan_data where id_user in ($users) and id_unit in ($units))");

  	if($this->input->get('f-tipe_laporan') <=2){
  		$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
  		unset($filters['YEAR(d.created_on)']);
  	}else{
  		unset($filters['a.tahun']);
  	}
  	unset($filters['a.minggu_ke'], $filters['d.minggu_ke'], $filters['a.bulan'], $filters['MONTH(d.created_on)'], $filters['DATE(d.created_on)'], $filters['a.tanggal']);
  	
  	foreach ($filters as $field => $data) {
			$this->db->where($field, $data);
		}
  	$this->db->group_by('id_user, id_unit, id_metadata');
		$this->db->order_by('k.kode', 'ASC');
  	$query=$this->db->get();
  	$result = $query->result_array();
  	return $result;
	}

	public function get_data_harian($users, $units, $filters){
  	$this->db->select("d.*, m.id_laporan_tipe_field, m.field, ROUND(sum(d.value),'0') AS sum_val");
  	$this->db->select("ROUND(avg(d.value),'0') AS avg_val");
  	$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m', 'm.id = d.id_metadata');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_kota lp', 'lp.id = k.id_provinsi');
  	// $this->db->where('d.value >', 0);


		$this->db->where_in('d.id_user', $users);
		$this->db->where_in('d.id_unit', $units);
  	// $this->db->where("(id_user, id_unit) IN (select id_user, id_unit from default_laporan_data where id_user in ($users) and id_unit in ($units))");

  	$tanggal = 'DATE(d.created_on)';
  	if($this->input->get('f-tipe_laporan') <=2){
  		$tanggal = 'a.tanggal';
  		$this->db->select("a.tanggal");
  		$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
  		unset($filters['YEAR(d.created_on)']);
  	}else{
  		$this->db->select('DATE(d.created_on) as tanggal');
  		unset($filters['a.tahun']);
  	}
  	unset($filters['DATE(d.created_on)'], $filters['a.tanggal']);
  	
  	foreach ($filters as $field => $data) {
			$this->db->where($field, $data);
		}


  	$this->db->group_by('id_user, id_unit, id_metadata, '.$tanggal);
		$this->db->order_by('k.kode, '.$tanggal, 'ASC');
  	$query=$this->db->get();
  	$result = $query->result_array();
  	return $result;
	}

	public function get_input_laporan($pagination_config = NULL, $count = 0, $filters)
	{
		$this->db->select('d.id_user, d.id_unit, d.channel, k.id AS id_kota, d.created_on, d.minggu_ke, p.display_name, k.nama AS kota, lp.id as id_provinsi, lp.nama AS provinsi, t.id as id_laporan,  t.nama_laporan, u.unit_name, m.id_laporan_komoditas, lk.nama_komoditas as nama_komoditas');
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m','m.id = d.id_metadata');
		$this->db->join('default_laporan_tipe t','t.id = m.id_laporan_tipe');
		$this->db->join('default_profiles p','p.user_id = d.id_user');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'k.id_provinsi = lp.id');
		$this->db->join('default_laporan_komoditas lk', 'lk.id = m.id_laporan_komoditas');
		if($this->input->get('f-tipe_laporan') <=2){
			$this->db->select('a.*, a.id as id_absen');
  		$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
  	}


		foreach ($filters as $field => $data) {
			$this->db->where($field, $data);
		}
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);


		$this->db->group_by('d.id_absen, d.id_user, k.id, u.unit_type, u.id');
		$this->db->order_by('d.created_on', 'DESC');
		$query = $this->db->get();

		if($count == 0){
			$result = $query->result_array();
		}else{
			$result = $query->num_rows();
		}
		
    return $result;
	}

	public function get_input_laporan_ex_komoditas($pagination_config = NULL, $count = 0, $filters)
	{
		$this->db->select('d.id_user, d.id_unit, d.channel, k.id AS id_kota, d.created_on, d.minggu_ke, p.display_name, k.nama AS kota, lp.id as id_provinsi, lp.nama AS provinsi, t.id as id_laporan,  t.nama_laporan, u.unit_name, m.id_laporan_komoditas');
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m','m.id = d.id_metadata');
		$this->db->join('default_laporan_tipe t','t.id = m.id_laporan_tipe');
		$this->db->join('default_profiles p','p.user_id = d.id_user');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'k.id_provinsi = lp.id');
		// if($this->input->get('f-tipe_laporan') <=2){
			$this->db->select('a.*, a.id as id_absen');
  		$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
  	// }


		foreach ($filters as $field => $data) {
			$this->db->where($field, $data);
		}
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);


		$this->db->group_by('d.id_absen, d.id_user, k.id, u.unit_type, u.id');
		$this->db->order_by('d.created_on', 'DESC');
		$query = $this->db->get();

		if($count == 0){
			$result = $query->result_array();
		}else{
			$result = $query->num_rows();
		}
		
    return $result;
	}

  public function get_tahunan($thn_bln, $id_unit, $id_user, $id_metadata, $agregasi){
  	$ex_thn_bln = explode('-',$thn_bln);
  	$thn = $ex_thn_bln[0];
  	$select = $agregasi.'(value) AS val';
  	$this->db->select($select);
  	$this->db->from('default_laporan_data');
  	$this->db->where('value >', 0);
  	$this->db->where('id_unit', $id_unit);
  	$this->db->where('id_user', $id_user);
  	$this->db->where('id_metadata', $id_metadata);
  	$this->db->where("DATE_FORMAT(created_on,'%Y')", $thn);
  	if($this->input->get('f-tipe_laporan') == 3){
			$this->db->where('is_ttic', 1);
		}else{
			$this->db->where('is_ttic', 0);
		}

		if($this->input->get('f-tipe_laporan') > 2){
			$this->db->where('id_group', 9);
		}

  	$this->db->group_by('id_user, id_unit, id_metadata');
  	$query = $this->db->get();
  	$result = $query->row_array();

  	$result = ($result['val'] != "") ? $result['val'] : 0;
  	return $result;
  }

	public function get_harga_by_periode($minggu_ke, $thn_bln, $id_unit, $id_kota, $id_user, $id_metadata, $agregasi = 'AVG', $is_sandingan){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];

		$and = '';

		if($this->input->get('f-tipe_laporan') <= 2){
			if($is_sandingan == NULL){
				if(isset($_GET['f-bln'])){
					if($_GET['f-bln'] != ""){
						$and .= "AND a.bulan = '{$bln}'";
					}
				}else{
					$and .= "AND a.bulan = '{$bln}'";
				}

				if(isset($_GET['f-minggu_ke'])){
					if($_GET['f-minggu_ke'] != ""){
						$and .= "AND a.minggu_ke = '{$minggu_ke}'";
					}
				}else{
					$and .= "AND a.minggu_ke = '{$minggu_ke}'";
				}
			}else{
				if(isset($_GET['f-bln2'])){
					if($_GET['f-bln2'] != ""){
						$and .= "AND a.bulan = '{$bln}'";
					}
				}else{
					$and .= "AND a.bulan = '{$bln}'";
				}

				if(isset($_GET['f-minggu_ke2'])){
					if($_GET['f-minggu_ke2'] != ""){
						$and .= "AND a.minggu_ke = '{$minggu_ke}'";
					}
				}else{
					$and .= "AND a.minggu_ke = '{$minggu_ke}'";
				}
			}
		}else{

			if($is_sandingan == NULL){
				if(isset($_GET['f-bln'])){
					if($_GET['f-bln'] != ""){
						$and .= "AND DATE_FORMAT(d.created_on, '%c') = '{$bln}'";
					}
				}else{
					$and .= "AND DATE_FORMAT(d.created_on, '%c') = '{$bln}'";
				}

				if(isset($_GET['f-minggu_ke'])){
					if($_GET['f-minggu_ke'] != ""){
						$and .= "AND d.minggu_ke = '{$minggu_ke}'";
					}
				}else{
					$and .= "AND d.minggu_ke = '{$minggu_ke}'";
				}
			}else{
				if(isset($_GET['f-bln2'])){
					if($_GET['f-bln2'] != ""){
						$and .= "AND DATE_FORMAT(d.created_on, '%c') = '{$bln}'";
					}
				}else{
					$and .= "AND DATE_FORMAT(d.created_on, '%c') = '{$bln}'";
				}

				if(isset($_GET['f-minggu_ke2'])){
					if($_GET['f-minggu_ke2'] != ""){
						$and .= "AND d.minggu_ke = '{$minggu_ke}'";
					}
				}else{
					$and .= "AND d.minggu_ke = '{$minggu_ke}'";
				}
			}
		}

		if($this->input->get('f-tipe_laporan')){
			$id_tipe_laporan = $this->input->get('f-tipe_laporan');
			// untuk yang harian (TTIC, Gapoktan 2017, TTI 2017)
			$is_ttic = ($id_tipe_laporan == 3) ? 1 : 0;
			if($id_tipe_laporan == 3 || $id_tipe_laporan == 4){
				$id_tipe_laporan = 1;
			}
			if($id_tipe_laporan == 5){
				$id_tipe_laporan = 2;
			}
			$and .= "AND a.id_laporan_tipe = '{$id_tipe_laporan}'";
		}

		if($this->input->get('f-tipe_laporan') <= 2){
			$sql = "
				SELECT a.id, a.tanggal, s.harga FROM default_laporan_absen a
				LEFT JOIN(
					SELECT d.id_absen, $agregasi(d.value) harga
					FROM default_laporan_data d
					JOIN default_laporan_absen a ON a.id = d.id_absen
					JOIN default_organization_units u ON u.id = d.id_unit
					JOIN default_location_kota k ON k.id = u.id_kota
					WHERE d.value > 0
					AND u.id_kota = {$id_kota}
					AND u.id = {$id_unit}
					AND d.id_user = {$id_user}
					AND d.id_metadata = {$id_metadata}
					GROUP by d.id_user, u.id_kota, a.tanggal, d.id_metadata, u.id
				) s ON s.id_absen = a.id
				WHERE a.tahun = '{$thn}'
				$and
				ORDER BY a.tanggal ASC
			";
		}else{

		}
		$query = $this->db->query($sql);
		$data_harga = $query->result_array();
		$h = 1;
		foreach ($data_harga as $key => $value) {
			$result['h'.$h] = ($value['harga'] != "") ? $value['harga'] : 0;
			$h++;
		}
		return $result;
	}

	public function get_harga($id_provinsi, $enumerator, $minggu_ke, $thn_bln){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];
		$sql = "
			SELECT d.id_user, d.id_kota, a.tanggal, a.minggu_ke, d.id_metadata, AVG(d.value) harga
			FROM default_laporan_data d
			JOIN default_laporan_metadata m ON m.id = d.id_metadata
			JOIN default_laporan_absen a ON a.id = d.id_absen
			JOIN default_location_kota k ON k.id = d.id_kota
			WHERE a.minggu_ke =  '{$minggu_ke}'
			AND DATE_FORMAT(a.tanggal,'%Y') =  '{$thn}'
			AND a.bulan =  '{$bln}'
			AND m.enumerator = '{$enumerator}'
			AND d.value > 0
		";

		if($id_provinsi != NULL){
			$sql .= "AND k.id_provinsi = ".$id_provinsi;
		}

		if($this->input->get('f-provinsi')){
			$and .= ' AND k.id_provinsi = '.$this->input->get('f-provinsi');
		}
		if($this->input->get('f-kota')){
			$and .= ' AND k.id = '.$this->input->get('f-kota');
		}

		$sql .= "GROUP by d.id_user, d.id_kota, a.tanggal, d.id_metadata";

		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;
	}

	public function get_value_by_tanggal($id_laporan_tipe, $id_metadata, $id_user, $id_unit, $id_kota, $minggu_ke, $thn_bln, $agregasi, $is_sandingan){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];
		$and = '';

		if($this->input->get('f-tipe_laporan') <= 2){
			if($is_sandingan == NULL){
				if(isset($_GET['f-bln'])){
					if($_GET['f-bln'] != ""){
						$and .= "AND a.bulan = '{$bln}'";
					}
				}else{
					$and .= "AND a.bulan = '{$bln}'";
				}

				if(isset($_GET['f-minggu_ke'])){
					if($_GET['f-minggu_ke'] != ""){
						$and .= "AND a.minggu_ke = '{$minggu_ke}'";
					}
				}else{
					$and .= "AND a.minggu_ke = '{$minggu_ke}'";
				}

				if($this->input->get('f-hari') != ''){
					$hari = $this->input->get('f-hari');
					$and .= "AND a.tanggal = '{$hari}'";
				}
			}else{
				if(isset($_GET['f-bln2'])){
					if($_GET['f-bln2'] != ""){
						$and .= "AND a.bulan = '{$bln}'";
					}
				}else{
					$and .= "AND a.bulan = '{$bln}'";
				}

				if(isset($_GET['f-minggu_ke2'])){
					if($_GET['f-minggu_ke2'] != ""){
						$and .= "AND a.minggu_ke = '{$minggu_ke}'";
					}
				}else{
					$and .= "AND a.minggu_ke = '{$minggu_ke}'";
				}

				if($this->input->get('f-hari2') != ''){
					$hari = $this->input->get('f-hari2');
					$and .= "AND a.tanggal = '{$hari}'";
				}
			}
		}else{
			if($is_sandingan == NULL){
				if($this->input->get('f-hari') != ''){
					$hari = $this->input->get('f-hari');
					$and .= "AND DATE_FORMAT(d.created_on, '%Y-%m-%d') = '{$hari}'";
				}
			}else{
				if($this->input->get('f-hari') != ''){
					$hari = $this->input->get('f-hari');
					$and .= "AND DATE_FORMAT(d.created_on, '%Y-%m-%d') = '{$hari}'";
				}
			}
		}

		if($this->input->get('f-tipe_laporan') == 3){
			$and .= "AND d.is_ttic = '1'";
		}else{
			$and .= "AND d.is_ttic = '0'";
		}

		if($this->input->get('f-tipe_laporan') > 2){
			$and .= "AND d.id_group = '9'";
		}

		if($this->input->get('id_organization_unit') > 0){
			$id_unit = $this->input->get('id_organization_unit');
			$and .= "AND d.id_unit = '{$id_unit}'";
		}
		
		$sql = "
			SELECT d.id_user, ROUND($agregasi(d.value),0) AS komoditas FROM default_laporan_data d ";
		if($this->input->get('f-tipe_laporan') <= 2){
			$sql .="
			JOIN default_laporan_absen a
			ON a.id = d.id_absen";
		}
		$sql .= "
			JOIN default_laporan_metadata m
			ON. m.id = d.id_metadata
			JOIN default_profiles p
			ON p.user_id = d.id_user
			JOIN default_organization_units u
			ON u.id = d.id_unit
			JOIN default_location_kota k
			ON k.id = u.id_kota
			WHERE m.id_laporan_tipe = '{$id_laporan_tipe}'
			AND u.id_kota = '{$id_kota}'
			AND u.id = '{$id_unit}'
			AND d.id_user = '{$id_user}'";
		if($this->input->get('f-tipe_laporan') <= 2){
			$sql .="
			AND a.tahun = '{$thn}'";
		}
		$sql .= "
			AND m.id = {$id_metadata}
			AND d.value > 0
			$and
			GROUP BY m.id, d.id_user, u.id
		";
		
		$query = $this->db->query($sql);
		$result = $query->row_array();

		$value = $result['komoditas'];
		return $value;

	}

	public function get_harga_by_periode2($tanggal, $id_user = null, $id_provinsi = null, $id_kota = null, $id_komoditas = null){
		$this->db->select('avg(d.value) harga');
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
		$this->db->join('default_location_kota k', 'k.id = d.id_kota');
		$this->db->where('a.tanggal', $tanggal);
		$this->db->where('d.value >', 0);
		
		if($id_user != NULL){
			$this->db->where('d.id_user', $id_user);
		}
		if($id_provinsi != NULL){
			$this->db->where('k.id_provinsi', $id_provinsi);
		}
		if($id_kota != NULL){
			$this->db->where('k.id', $id_kota);
		}
		
		$this->db->where('d.id_metadata', $id_komoditas);
		$this->db->group_by('a.tanggal');

		$query = $this->db->get();
		$result = $query->row_array();

		return intval($result['harga']);
	}

	public function get_data_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_laporan_data');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function get_data_by_absen($id_user, $id_absen, $enumerator){
		$this->db->where('id_user', $id_user);
		$this->db->where('id_absen', $id_absen);
		$this->db->where("id_metadata IN (SELECT id FROM default_laporan_metadata WHERE enumerator = '{$enumerator}')");
		$query = $this->db->get('default_laporan_data');
		$result = $query->result_array();
		return $result;
	}

	public function get_data_input_by_user($tipe_laporan, $id_komoditas = 1, $wheres){
		foreach ($wheres as $field => $data) {
			$this->db->where($field, $data);
		}
		$this->db->where("id_metadata IN (SELECT id FROM default_laporan_metadata WHERE id_laporan_tipe = '{$tipe_laporan}' AND id_laporan_komoditas = '{$id_komoditas}')");
		$query = $this->db->get('default_laporan_data');
		$result = $query->result_array();
		return $result;
	}

	public function get_data_keuangan_input_by_user($tipe_laporan, $wheres){
		foreach ($wheres as $field => $data) {
			$this->db->where($field, $data);
		}
		$this->db->where("id_metadata IN (SELECT id FROM default_laporan_metadata WHERE id_laporan_tipe = '{$tipe_laporan}')");
		$query = $this->db->get('default_laporan_data');
		$result = $query->result_array();
		return $result;
	}

	public function delete_data_input_by_user($tipe_laporan, $id_komoditas = 1, $wheres)
	{
		foreach ($wheres as $field => $data) {
			$this->db->where($field, $data);
		}
		$this->db->where("id_metadata IN (SELECT id FROM default_laporan_metadata WHERE id_laporan_tipe = '{$tipe_laporan}' AND id_laporan_komoditas = '{$id_komoditas}')");
		$this->db->delete('default_laporan_data');
	}
	public function delete_data_keuangan_input_by_user($tipe_laporan, $wheres)
	{
		foreach ($wheres as $field => $data) {
			$this->db->where($field, $data);
		}
		$this->db->where("id_metadata IN (SELECT id FROM default_laporan_metadata WHERE id_laporan_tipe = '{$tipe_laporan}')");
		$this->db->delete('default_laporan_data');
	}

	public function laporan_harga_harian($wheres){
		$this->db->select('DATE(d.created_on) as tanggal, d.id_metadata, m.nama as komoditas, m.id_laporan_tipe_field, m.satuan, m.field, SUM(d.value) as sum, AVG(d.value) AS avg');
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m','m.id = d.id_metadata');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		foreach ($wheres as $field => $data) {
			if($field == 'DATE(d.created_on)'){
				$this->db->where_in($field, $data);
			}else{
				$this->db->where($field, $data);
			}
		}
		$this->db->order_by('d.created_on, m.urutan, m.id','ASC');
		$this->db->group_by('d.created_on, d.id_metadata');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function laporan_harga_home($tipe_laporan, $wheres){
		$this->db->select('DATE(d.created_on) as tanggal, t.nama_laporan, m.id_laporan_tipe, d.id_metadata, m.nama as nama, AVG(d.value) AS avg_val, MIN(d.value) AS min_val, MAX(d.value) AS max_val');
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m','m.id = d.id_metadata');
		$this->db->join('default_laporan_tipe t','t.id = m.id_laporan_tipe');
		if($tipe_laporan == 'mingguan'){
			$this->db->join('default_laporan_absen a','a.id = d.id_absen');
		}
		foreach ($wheres as $field => $data) {
			$this->db->where($field, $data);
		}
		$this->db->order_by('d.created_on, m.urutan, m.id','ASC');
		$this->db->group_by('d.id_metadata, m.id_laporan_tipe');

		$query = $this->db->get();
		// dump($this->db->last_query());
		return $query->result_array();
	}

	// public function laporan_akumulasi($wheres){
	// 	$this->db->select("m.id as id, m.nama as komoditas, m.id_laporan_tipe_field, m.field, ROUND(AVG(d.value),'0') AS avg, ROUND(SUM(d.value),'0') AS sum, a.minggu_ke, a.bulan, a.tahun");
	// 	$this->db->from('default_laporan_data d');
	// 	$this->db->join('default_laporan_metadata m', 'm.id = d.id_metadata');
	// 	$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
	// }
	public function laporan_harga_optimize2($type_laporan, $wheres, $is_akumulated = null){
		$this->db->select("m.id as id, m.nama as komoditas, m.satuan, m.id_laporan_tipe_field, m.field, ROUND(AVG(d.value),'0') AS avg, ROUND(SUM(d.value),'0') AS sum, ROUND(STDDEV(d.value),'0') AS fluktuasi");
		if($type_laporan == 'mingguan'){
			$this->db->select('a.tanggal, a.minggu_ke, a.bulan, a.tahun');
		}else{
			$this->db->select('DATE(d.created_on) AS tanggal, d.minggu_ke, MONTH(d.created_on) AS bulan, YEAR(d.created_on) AS tahun');
		}
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m', 'm.id = d.id_metadata');
		if($type_laporan == 'mingguan'){
			$this->db->join('default_laporan_absen a', 'a.id = d.id_absen');
		}
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->where('d.value > ',0);
		foreach ($wheres as $field => $data) {
			if(is_array($data)){
				$this->db->where_in($field, $data);
			}else{
				$this->db->where($field, $data);
			}
		}
		if($is_akumulated != NULL){
			$this->db->group_by('m.id');
		}else{
			if($type_laporan == 'mingguan'){
				$this->db->group_by('m.id, a.minggu_ke, a.bulan, a.tahun');
			}else{
				$this->db->group_by('m.id, d.minggu_ke, MONTH(d.created_on), YEAR(d.created_on)');
			}
		}
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function laporan_harga($id_laporan_tipe, $minggu_ke, $thn_bln, $intervals, $id_absen, $id_unit = NULL, $id_komoditas = 1){

		if ($id_laporan_tipe == 3){
			$id_laporan_tipe = 1;
			$is_ttic = 1;
		} else {
			$is_ttic = 0;
		}

		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];

		$interval = $thn_bln.'-01';
		$and = '';
		$group_by = '';
		if($this->input->get('f-provinsi')){
			$and .= ' AND k.id_provinsi = '.$this->input->get('f-provinsi');
		}
		if($this->input->get('f-kota')){
			$and .= ' AND k.id = '.$this->input->get('f-kota');
		}
		if($this->input->get('id_organization_unit') > 0){
			$and .= ' AND d.id_unit = '.$this->input->get('id_organization_unit');
			$group_by .= ', d.id_unit';
		}
		if($id_unit !=  NULL){
			$and .= ' AND d.id_unit = '.$id_unit;
			$group_by .= ', d.id_unit';
		}

		if($is_ttic == 1){
			$and .= ' AND d.is_ttic = 1';
		}



		$and1 = '';
		if($this->input->get('f-metadata')){
			$and1 .= ' AND m.id = '.$this->input->get('f-metadata');
		}

		$where_laporan = "WHERE m.id_laporan_tipe = '{$id_laporan_tipe}'";

		$select_interval = '';
		foreach ($intervals as $key => $value) {
			$select_interval .= 'm'.$key.'.minggu'.$key.',';
		}

		$sql = "
			SELECT m.id, m.id_laporan_tipe_field, m.nama AS komoditas, m.satuan, i3.harga3bln, {$select_interval} ms.harga_minggu_ini, ms.fluktuasi, i31.total, i32.akumulasi
			FROM default_laporan_metadata m
			LEFT JOIN(
				SELECT sq.id, ROUND(AVG(sq.harga),0) AS harga3bln
				FROM (
					SELECT m.id AS id, ROUND(AVG(d.value),0) AS harga FROM default_laporan_data d";

					if ($is_ttic == 0){			
					$sql .="
					JOIN default_laporan_absen a
					ON a.id = d.id_absen ";
					}

		$sql .="			
					JOIN default_laporan_metadata m
					ON m.id = d.id_metadata
					JOIN default_organization_units u
					ON u.id = d.id_unit
					JOIN default_profiles p
					ON p.user_id = d.id_user
					JOIN default_location_kota k
					ON k.id = u.id_kota
					$where_laporan ";
		
					if ($is_ttic == 0){			
					$sql .="
					AND DATE_FORMAT(a.tanggal,'%Y-%m') =  DATE_FORMAT(DATE_SUB('{$interval}', INTERVAL 3 MONTH),'%Y-%m') ";
					} else {
					$sql .="
					AND DATE_FORMAT(d.created_on,'%Y-%m') =  DATE_FORMAT(DATE_SUB('{$interval}', INTERVAL 3 MONTH),'%Y-%m') ";
					}

		$sql .="	
					AND d.value > 0
					$and
					GROUP BY m.id, k.id$group_by
				) sq
				GROUP by sq.id
			) i3 ON i3.id = m.id

			LEFT JOIN(
				SELECT sq2.id, if(sq2.id_laporan_tipe_field = 1, ROUND(AVG(sq2.harga),0), ROUND(SUM(sq2.harga),0)) AS total
				FROM (
					SELECT m.id AS id, m.id_laporan_tipe_field, if(m.id_laporan_tipe_field = 1, ROUND(AVG(d.value),0), ROUND(SUM(d.value),0)) AS harga FROM default_laporan_data d ";
					
					if ($is_ttic == 0){			
					$sql .="
					JOIN default_laporan_absen a
					ON a.id = d.id_absen ";
					}

		$sql .="			
					JOIN default_laporan_metadata m
					ON m.id = d.id_metadata
					JOIN default_organization_units u
					ON u.id = d.id_unit
					JOIN default_profiles p
					ON p.user_id = d.id_user
					JOIN default_location_kota k
					ON k.id = u.id_kota
					$where_laporan
					AND d.value > 0 ";
					
					if ($is_ttic == 0){			
					$sql .="
					AND a.id <= '$id_absen' ";
					}

		$sql .="
					$and
					GROUP BY m.id, k.id$group_by
				) sq2
				GROUP by sq2.id
			) i31 ON i31.id = m.id

			LEFT JOIN(
				SELECT sq3.id, if(sq3.id_laporan_tipe_field = 1, ROUND(AVG(sq3.harga),0), ROUND(SUM(sq3.harga),0)) AS akumulasi
				FROM (
					SELECT m.id AS id, m.id_laporan_tipe_field, if(m.id_laporan_tipe_field = 1, ROUND(AVG(d.value),0), ROUND(SUM(d.value),0)) AS harga FROM default_laporan_data d ";
					
					if ($is_ttic == 0){			
					$sql .="
					JOIN default_laporan_absen a
					ON a.id = d.id_absen ";
					}

		$sql .="			
					JOIN default_laporan_metadata m
					ON m.id = d.id_metadata
					JOIN default_organization_units u
					ON u.id = d.id_unit
					JOIN default_profiles p
					ON p.user_id = d.id_user
					JOIN default_location_kota k
					ON k.id = u.id_kota
					$where_laporan
					AND d.value > 0 ";
					
					if ($is_ttic == 0){			
					$sql .="
					AND a.id <= '$id_absen' ";
					}

		$sql .="
					$and
					GROUP BY m.id, k.id$group_by
				) sq3
				GROUP by sq3.id
			) i32 ON i32.id = m.id
			";

		foreach ($intervals as $i => $data) {
			$past_week = $data['week'];
			$firstday = $data['last_day_of_week'];
			$bulan = $data['bulan'];
			$sql .= "
			LEFT JOIN(
				SELECT sq.id, ROUND(AVG(sq.harga),0) AS minggu$i
				FROM (
					SELECT m.id AS id, ROUND(AVG(d.value),0) AS harga FROM default_laporan_data d ";
		
					if ($is_ttic == 0){			
					$sql .="
					JOIN default_laporan_absen a
					ON a.id = d.id_absen ";
					}
		
		$sql .="			
					JOIN default_laporan_metadata m
					ON. m.id = d.id_metadata
					JOIN default_organization_units u
					ON u.id = d.id_unit
					JOIN default_profiles p
					ON p.user_id = d.id_user
					JOIN default_location_kota k
					ON k.id = u.id_kota
					$where_laporan ";
					
					if ($is_ttic == 0){			
					$sql .="
					AND DATE_FORMAT(a.tanggal, '%Y') = date_format('{$firstday}', '%Y')
					AND a.bulan = '{$bulan}'
					AND a.minggu_ke = {$past_week} ";
					} else {
					$sql .="
					AND DATE_FORMAT(d.created_on, '%Y') = date_format('{$firstday}', '%Y')
					AND MONTH(d.created_on) = '{$bulan}'
					AND d.minggu_ke = {$past_week} ";				
					}

		$sql .="		
					AND d.value > 0
					$and
					GROUP BY m.id, k.id$group_by
				) sq
				GROUP by sq.id
			) m$i ON m$i.id = m.id ";
			$i = $i+6;
		}

		$sql .=" 
			LEFT JOIN(
				SELECT sq.id, ROUND(STDDEV(sq.harga),0) AS fluktuasi, ROUND(AVG(sq.harga),0) AS harga_minggu_ini
				FROM (
					SELECT m.id AS id, ROUND(AVG(d.value),0) AS harga FROM default_laporan_data d ";
					
					if ($is_ttic == 0){			
					$sql .="
					JOIN default_laporan_absen a
					ON a.id = d.id_absen ";
					}

		$sql .="
					JOIN default_laporan_metadata m
					ON. m.id = d.id_metadata
					JOIN default_organization_units u
					ON u.id = d.id_unit
					JOIN default_profiles p
					ON p.user_id = d.id_user
					JOIN default_location_kota k
					ON k.id = u.id_kota
					$where_laporan ";

					if ($is_ttic == 0){			
					$sql .="
					AND a.minggu_ke = {$minggu_ke}
					AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
					AND a.bulan = '{$bln}' ";
					} else {
					$sql .="
					AND d.minggu_ke = {$minggu_ke}
					AND DATE_FORMAT(d.created_on, '%Y') = '{$thn}'
					AND MONTH(d.created_on) = '{$bln}' ";			
					}	

		$sql .="						
					AND d.value > 0
					$and
					GROUP BY m.id, k.id$group_by
				) sq
				GROUP by sq.id
			) ms ON ms.id = m.id 
			$where_laporan
			and m.id_laporan_komoditas = '{$id_komoditas}'
			$and1
			GROUP BY m.id
			ORDER BY m.id ASC
		";

		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;

	}

	public function laporan_stok($minggu_ke, $thn_bln){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];

		$and = '';
		if($this->input->get('f-provinsi')){
			$and .= ' AND k.id_provinsi = '.$this->input->get('f-provinsi');
		}
		if($this->input->get('f-kota')){
			$and .= ' AND k.id = '.$this->input->get('f-kota');
		}

		$sql = "
			SELECT p.nama as provinsi, k.nama as kota, hrg1.hrg_gkp, hrg2.hrg_gkg, bp.hrg_bp, bm.hrg_bm, sg.stok_gkg, sb.stok_beras
			FROM default_location_kota k
			JOIN default_location_provinsi p ON p.id = k.id_provinsi
			JOIN (
				SELECT d.id_kota, ROUND(AVG(d.value),0) as hrg_gkp
				FROM default_laporan_data d
				JOIN default_laporan_metadata m ON d.id_metadata = m.id
				JOIN default_laporan_absen a ON a.id = d.id_absen
				JOIN default_location_kota k ON k.id = d.id_kota
				WHERE m.nama like '%GKP%'
				AND m.field like '%Harga%'
				AND a.minggu_ke = {$minggu_ke}
				AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
				AND a.bulan = '{$bln}'
				AND d.value > 0
				$and
				GROUP BY d.id_kota
			) hrg1 ON hrg1.id_kota = k.id

			JOIN (
				SELECT d.id_kota, ROUND(AVG(d.value),0) as hrg_gkg
				FROM default_laporan_data d
				JOIN default_laporan_metadata m ON d.id_metadata = m.id
				JOIN default_laporan_absen a ON a.id = d.id_absen
				JOIN default_location_kota k ON k.id = d.id_kota
				WHERE m.nama like '%GKG%'
				AND m.field like '%Harga%'
				AND a.minggu_ke = {$minggu_ke}
				AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
				AND a.bulan = '{$bln}'
				AND d.value > 0
				$and
				GROUP BY d.id_kota
			) hrg2 ON hrg2.id_kota = k.id

			JOIN (
				SELECT d.id_kota, ROUND(AVG(d.value),0) as hrg_bp
				FROM default_laporan_data d 
				JOIN default_laporan_metadata m ON d.id_metadata = m.id
				JOIN default_laporan_absen a ON a.id = d.id_absen
				JOIN default_location_kota k ON k.id = d.id_kota
				WHERE m.nama like '%Beras Premium%'
				AND (m.field like '%Harga%' OR m.field like '%Hrg%')
				AND a.minggu_ke = {$minggu_ke}
				AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
				AND a.bulan = '{$bln}'
				AND d.value > 0
				$and
				GROUP BY d.id_kota
			) bp ON bp.id_kota = k.id

			JOIN (
				SELECT d.id_kota, ROUND(AVG(d.value),0) as hrg_bm
				FROM default_laporan_data d
				JOIN default_laporan_metadata m ON d.id_metadata = m.id
				JOIN default_laporan_absen a ON a.id = d.id_absen
				JOIN default_location_kota k ON k.id = d.id_kota
				WHERE m.nama like '%Beras Medium%'
				AND (m.field like '%Harga%' OR m.field like '%Hrg%')
				AND a.minggu_ke = {$minggu_ke}
				AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
				AND a.bulan = '{$bln}'
				AND d.value > 0
				$and
				GROUP BY d.id_kota
			) bm ON bm.id_kota = k.id

			JOIN (
				SELECT d.id_kota, ROUND(AVG(d.value),0) as stok_gkg
				FROM default_laporan_data d
				JOIN default_laporan_metadata m ON d.id_metadata = m.id
				JOIN default_laporan_absen a ON a.id = d.id_absen
				JOIN default_location_kota k ON k.id = d.id_kota
				WHERE m.nama like '%GKG%'
				AND m.field like '%Stok%'
				AND a.minggu_ke = {$minggu_ke}
				AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
				AND a.bulan = '{$bln}'
				AND d.value > 0
				$and
				GROUP BY d.id_kota
			) sg ON sg.id_kota = k.id

			JOIN (
				SELECT d.id_kota, ROUND(AVG(d.value),0) as stok_beras
				FROM default_laporan_data d
				JOIN default_laporan_metadata m ON d.id_metadata = m.id
				JOIN default_laporan_absen a ON a.id = d.id_absen
				JOIN default_location_kota k ON k.id = d.id_kota
				WHERE m.nama like '%Beras%'
				AND m.field like '%Stok%'
				AND a.minggu_ke = {$minggu_ke}
				AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
				AND a.bulan = '{$bln}'
				AND d.value > 0
				$and
				GROUP BY d.id_kota
			) sb ON sb.id_kota = k.id
			WHERE k.id IS NOT NULL
			$and
			GROUP BY k.id
			ORDER BY k.id_provinsi, k.id ASC
		";

		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;

	}

	public function laporan_pasokan($where_enumerator, $minggu_ke, $thn_bln, $intervals){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];

		$interval = $thn_bln.'-01';
		$and = '';
		if($this->input->get('f-provinsi')){
			$and .= ' AND k.id_provinsi = '.$this->input->get('f-provinsi');
		}
		if($this->input->get('f-kota')){
			$and .= ' AND k.id = '.$this->input->get('f-kota');
		}

		$and1 = '';
		if($this->input->get('f-komoditas')){
			$and1 .= ' AND m.id = '.$this->input->get('f-komoditas');
		}

		$select_interval = '';
		foreach ($intervals as $key => $value) {
			$select_interval .= 'm'.$key.'.minggu'.$key.',';
		}

		$sql = "
			SELECT m.nama AS komoditas, m.satuan, i3.harga3bln, {$select_interval} ms.harga_minggu_ini, ms.fluktuasi
			FROM default_laporan_metadata m
			LEFT JOIN(
				SELECT sq.id, ROUND(AVG(sq.harga),0) AS harga3bln
				FROM (
					SELECT m.id AS id, ROUND(AVG(d.value),0) AS harga FROM default_laporan_data d
					JOIN default_laporan_absen a
					ON a.id = d.id_absen
					JOIN default_laporan_metadata m
					ON. m.id = d.id_metadata
					JOIN default_profiles p
					ON p.user_id = d.id_user
					JOIN default_location_kota k
					ON k.id = d.id_kota
					$where_enumerator
					AND DATE_FORMAT(a.tanggal,'%Y-%m') =  DATE_FORMAT(DATE_SUB('{$interval}', INTERVAL 3 MONTH),'%Y-%m')
					AND d.value > 0
					$and
					GROUP BY m.id, k.id
				) sq
				GROUP by sq.id
			) i3 ON i3.id = m.id

			";

		foreach ($intervals as $i => $data) {
			$past_week = $data['week'];
			$firstday = $data['last_day_of_week'];
			$bulan = $data['bulan'];
			$sql .= "
			LEFT JOIN(
				SELECT sq.id, ROUND(AVG(sq.harga),0) AS minggu$i
				FROM (
					SELECT m.id AS id, ROUND(AVG(d.value),0) AS harga FROM default_laporan_data d
					JOIN default_laporan_absen a
					ON a.id = d.id_absen
					JOIN default_laporan_metadata m
					ON. m.id = d.id_metadata
					JOIN default_profiles p
					ON p.user_id = d.id_user
					JOIN default_location_kota k
					ON k.id = d.id_kota
					$where_enumerator
					AND DATE_FORMAT(a.tanggal, '%Y') = date_format('{$firstday}', '%Y')
					AND a.bulan = '{$bulan}'
					AND a.minggu_ke = {$past_week}
					AND d.value > 0
					$and
					GROUP BY m.id, k.id
				) sq
				GROUP by sq.id
			) m$i ON m$i.id = m.id ";
			$i = $i+6;
		}

		$sql .=" 
			LEFT JOIN(
				SELECT sq.id, ROUND(STDDEV(sq.harga),0) AS fluktuasi, ROUND(AVG(sq.harga),0) AS harga_minggu_ini
				FROM (
					SELECT m.id AS id, ROUND(AVG(d.value),0) AS harga FROM default_laporan_data d
					JOIN default_laporan_absen a
					ON a.id = d.id_absen
					JOIN default_laporan_metadata m
					ON. m.id = d.id_metadata
					JOIN default_profiles p
					ON p.user_id = d.id_user
					JOIN default_location_kota k
					ON k.id = d.id_kota
					$where_enumerator
					AND a.minggu_ke = {$minggu_ke}
					AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
					AND a.bulan = '{$bln}'
					AND d.value > 0
					$and
					GROUP BY m.id, k.id
				) sq
				GROUP by sq.id
			) ms ON ms.id = m.id 
			$where_enumerator
			AND m.field like '%psok%'
			$and1
			GROUP BY m.id
			ORDER BY m.id ASC
		";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function laporan_pasar($komoditas, $id_enumerator, $enumerator, $minggu_ke, $thn_bln){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];

		$where_enumerator = "WHERE m.enumerator = '{$enumerator}'";

		$select_komoditas = '';
		foreach ($komoditas as $key => $value) {
			$select_komoditas .= 'k'.++$key.'.komoditas'.$key.',';
		}

		$sql = "
			SELECT p.nama AS provinsi, {$select_komoditas} k.nama AS kota, eu.pasar_utama
			FROM default_profiles pr
			JOIN default_location_kota k ON k.id = pr.id_kota
			JOIN default_location_provinsi p ON p.id = k.id_provinsi
			JOIN default_enumerator_users eu ON eu.id_user = pr.user_id
		";

		foreach ($komoditas as $i => $metadata) {
			$id_metadata = $metadata['id'];
			$i = $i+1;
			$sql .= "
			LEFT JOIN(
				SELECT d.id_user, ROUND(AVG(d.value),0) AS komoditas$i FROM default_laporan_data d
				JOIN default_laporan_absen a
				ON a.id = d.id_absen
				JOIN default_laporan_metadata m
				ON. m.id = d.id_metadata
				JOIN default_profiles p
				ON p.user_id = d.id_user
				JOIN default_location_kota k
				ON k.id = d.id_kota
				$where_enumerator
				AND a.minggu_ke = {$minggu_ke}
				AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
				AND a.bulan = '{$bln}'
				AND m.id = {$id_metadata}
				AND d.value > 0
				GROUP BY m.id, d.id_user
			) k$i ON k$i.id_user = pr.user_id ";
		}

		$sql .= "WHERE eu.keterangan = 1 AND eu.id_enumerator = '{$id_enumerator}' GROUP BY k.id_provinsi ORDER BY k.id_provinsi, k.id ASC";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;

	}

	public function get_input_laporan_by_id($filters){
		$this->db->select('*');
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m', 'm.id = d.id_metadata');
		foreach ($filters as $field => $data) {
			$this->db->where($field, $data);
		}
		$this->db->order_by('m.urutan, m.id', "ASC");

		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	public function get_data_this_week($id_laporan_tipe, $thisweek, $thn_bln, $id_komoditas){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];

		$sql = "
			SELECT sq.pelapor, sq.created_on, sq.kota, sq.id, sq.nama, ROUND(MIN(sq.min_harga),0) AS min_val, ROUND(MAX(sq.max_harga),0) AS max_val, sq.urutan, ROUND(AVG(sq.rata2_harga),0)  AS rata2_val 
			FROM (
				SELECT p.display_name as pelapor, k.nama as kota, m.id, m.nama, d.created_on, ROUND(MIN(d.value),0) AS min_harga, ROUND(MAX(d.value),0) AS max_harga, m.urutan, ROUND(AVG(d.value),0) AS rata2_harga
				FROM default_laporan_metadata m
				LEFT join default_laporan_data d on d.id_metadata = m.id
				LEFT join default_laporan_absen a on a.id = d.id_absen
				LEFT join default_organization_units u on u.id = d.id_unit
				LEFT join default_location_kota k on u.id_kota = k.id
				LEFT join default_profiles p on d.id_user = p.user_id
				WHERE a.minggu_ke = {$thisweek}
				AND DATE_FORMAT(a.tanggal, '%Y') = '{$thn}'
				AND a.bulan = '{$bln}'
				AND m.id_laporan_tipe_field = 1
				AND m.id_laporan_tipe = '{$id_laporan_tipe}'
				AND d.value >= m.nilai_minimal_home
				AND d.value <= m.nilai_maksimal_home
				AND m.id_laporan_komoditas = '{$id_komoditas}'
				GROUP BY m.id, u.id_kota
				ORDER BY m.id_laporan_tipe, m.urutan, m.id ASC
			) sq
			GROUP BY sq.id
			ORDER BY sq.urutan ASC
		";

		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function get_ten_last_report($limit = NULL){
		$this->db->select('p.display_name as pendamping, k.nama as kota, pr.nama as provinsi, u.unit_name, d.created_on, t.type_name as nama_laporan, d.channel');
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m','m.id = d.id_metadata');
		$this->db->join('default_profiles p','p.user_id = d.id_user');
		$this->db->join('default_organization_units u','u.id = d.id_unit');
		$this->db->join('default_organization_types t','t.id = u.unit_type');
		$this->db->join('default_location_kota k','u.id_kota = k.id');
		$this->db->join('default_location_provinsi pr','k.id_provinsi = pr.id');
		$this->db->order_by('d.created_on','DESC');
		$this->db->group_by('d.id_user, d.id_unit, k.id, d.created_on');
		if(empty($limit)) {
			$this->db->limit(10);
		} else {
			$this->db->limit($limit);
		}

		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function count_all_data()
	{
		return $this->db->count_all('laporan_data');
	}
	
	public function delete_data_by_absen($id_user, $id_absen, $enumerator)
	{
		$this->db->where('id_absen', $id_absen);
		$this->db->where('id_user', $id_user);
		$this->db->where("id_metadata IN (SELECT id FROM default_laporan_metadata WHERE enumerator = '{$enumerator}')");
		$this->db->delete('default_laporan_data');
	}

	public function delete_data_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_data');
	}
	
	public function insert_data($values)
	{
		if(!isset($values['created_on'])){
			$values['created_on'] = date("Y-m-d H:i:s");
		}
		return $this->db->insert('default_laporan_data', $values);
	}
	
	public function update_data($values, $where)
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}	
		return $this->db->update('default_laporan_data', $values); 
	}

	public function get_hari($year, $month, $minggu_ke){
		$thn_bln = $year.'-'.$month;
		$month = $month+0;
		$this->db->select("DATE_FORMAT(created_on,'%Y-%m-%d') AS tanggal");
		$this->db->where("DATE_FORMAT(created_on,'%Y') = ", $year);
		$this->db->where("DATE_FORMAT(created_on,'%c') = ", $month);
		$this->db->where('minggu_ke', $minggu_ke);
		$this->db->group_by('created_on');
		$query = $this->db->get('default_laporan_data');
		$result = $query->row_array();
		return $result['tanggal'];
	}
	
}