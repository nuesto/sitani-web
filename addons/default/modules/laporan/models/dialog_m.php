<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Dialog model
 *
 * @author Aditya Satrya
 */
class Dialog_m extends MY_Model {
	
	public function get_dialog($pagination_config = NULL)
	{
		$this->db->select('*');
		$this->db->order_by('id','DESC');
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);


		if($this->input->get('f-nama')){
			$this->db->like('nama', $this->input->get('f-nama'));
		}
		if($this->input->get('f-email')){
			$this->db->like('email', $this->input->get('f-email'));
		}
		if($this->input->get('f-kontak')){
			$this->db->like('kontak', $this->input->get('f-kontak'));
		}
		$query = $this->db->get('default_laporan_dialog');
		$result = $query->result_array();
		
    return $result;
	}

	public function get_dialog_dashboard()
	{
		$this->db->select('*');
		$this->db->where('status',1);
		$this->db->order_by('id','DESC');
		$this->db->limit(15);
		$query = $this->db->get('default_laporan_dialog');
		$result = $query->result_array();
		
    return $result;
	}
	
	public function get_dialog_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_laporan_dialog');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_dialog()
	{
		return $this->db->count_all('laporan_dialog');
	}
	
	public function delete_dialog_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_dialog');
	}
	
	public function insert_dialog($values)
	{	
		return $this->db->insert('default_laporan_dialog', $values);
	}
	
	public function update_dialog($values, $row_id)
	{	
		$this->db->where('id', $row_id);
		return $this->db->update('default_laporan_dialog', $values); 
	}
	
}