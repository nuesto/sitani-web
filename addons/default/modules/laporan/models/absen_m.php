<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Absen model
 *
 * @author Aditya Satrya
 */
class Absen_m extends MY_Model {
	
	public function get_absen($pagination_config = NULL)
	{
		$this->db->select("tanggal as tgl, tahun, DATE_FORMAT(tanggal,'%M') AS bulan, GROUP_CONCAT(DISTINCT DATE_FORMAT(tanggal, '%d') order by tanggal asc SEPARATOR ', ') AS tanggal, waktu_buka, waktu_tutup");
		$this->db->where("tahun = DATE_FORMAT(NOW(),'%Y')");
		$this->db->group_by("DATE_FORMAT(tanggal,'%m-%Y')");
		$this->db->order_by('tgl', 'ASC');

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_laporan_absen');
		$result = $query->result_array();
		
    return $result;
	}

	public function get_absen_admin()
	{
		$this->db->select("*, DATE_FORMAT(tanggal, '%Y') AS tahun");

		if($this->input->get('f-tipe_laporan')){
			$this->db->where('id_laporan_tipe', $this->input->get('f-tipe_laporan'));
		}
		if($this->input->get('f-bln') != ''){
			$bln = $this->input->get('f-bln') + 0;
			$this->db->where("bulan", $bln);
		}else{
			$bln = date('n');
			$this->db->where("bulan",$bln);
		}
		
		if($this->input->get('f-thn') != ''){
			$this->db->where("tahun", $this->input->get('f-thn'));
		}else{
			$this->db->where("tahun = DATE_FORMAT(NOW(),'%Y')");
		}
		$this->db->order_by('tanggal', 'ASC');

		$query = $this->db->get('default_laporan_absen');
		$result = $query->result_array();
		
    return $result;
	}
	
	public function get_dates(){
		$this->db->select("date_format(tanggal, '%m/%d/%Y') as tanggal");

		if($this->input->get('f-tipe_laporan')){
			$this->db->where('id_laporan_tipe', $this->input->get('f-tipe_laporan'));
		}

		$query = $this->db->get('default_laporan_absen');
		$result = $query->result_array();
		$dates = array();
		
		foreach ($result as $key => $tgl) {
			$dates[] = "'".$tgl['tanggal']."'";
		}
		return $dates;
	}
	public function get_absen_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_laporan_absen');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_absen_by_date($date, $tipe_laporan = NULL, $option = NULL){
		if ($tipe_laporan == 3){
			$tipe_laporan = 1;
		}
		
 		$this->db->where('waktu_buka <=',$date);
 		$this->db->where('waktu_tutup >=',$date);
 		if($tipe_laporan != NULL){
 			$this->db->where('id_laporan_tipe', $tipe_laporan);
 		}
 		if($option == 'option'){
 			$this->db->where('tanggal', $date);
 		}
 		$this->db->order_by('tanggal','DESC');
 		$this->db->limit(1);
		$query = $this->db->get('default_laporan_absen');
		$result = $query->row_array();
		return $result;
	}

	public function get_id_by_tgl($tanggal, $tipe_laporan){
		$this->db->where('id_laporan_tipe',$tipe_laporan);
		$this->db->where('tanggal',$tanggal);
		$result = $this->db->get('default_laporan_absen')->row_array();
		return $result;
	}

	public function cek_tgl_input($date, $tipe_laporan){
 		$this->db->where('waktu_buka <=',$date);
 		$this->db->where('waktu_tutup >=',$date);
 		$this->db->where('id_laporan_tipe', $tipe_laporan);
		$query = $this->db->get('default_laporan_absen');
		$result = $query->row_array();
		return $result;
	}


	public function get_min_year(){
		$this->db->select("MIN(tahun) AS min_year");
		$query = $this->db->get('default_laporan_absen');
		$result = $query->row_array();
		$min_year = date('Y');
		if(count($result) > 0){
			$min_year = $result['min_year'];
		}
		return $min_year;
	}

	public function get_max_year(){
		$this->db->select("MAX(tahun) AS max_year");
		$query = $this->db->get('default_laporan_absen');
		$result = $query->row_array();
		$max_year = date('Y');
		if(count($result) > 0){
			$max_year = $result['max_year'];
		}
		return $max_year;
	}

	public function get_first_day_of_week($tahun, $bulan, $minggu_ke, $id_laporan_tipe = NULL){

		if ($id_laporan_tipe == 3){
			$id_laporan_tipe = 1;
		}


		$this->db->where("tahun", $tahun);
		$this->db->where("bulan", $bulan);
		$this->db->where('minggu_ke', $minggu_ke);

		if($this->input->get('f-tipe_laporan')){
			$this->db->where('id_laporan_tipe', $this->input->get('f-tipe_laporan'));
		}

		if($id_laporan_tipe!=NULL){
			$this->db->where('id_laporan_tipe', $id_laporan_tipe);
		}

		// $this->db->group_by("minggu_ke");
		$query = $this->db->get('default_laporan_absen');

		$result = $query->row_array();

		$firstday = $result['tanggal_awal'];
		return $firstday;

	}

	public function get_absen_by_year_month($thn_bln, $id_laporan_tipe, $minggu_ke = null){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];

		$this->db->where("tahun", $thn);
		$this->db->where("bulan", $bln);
		$this->db->where("id_laporan_tipe", $id_laporan_tipe);
		if (!empty($minggu_ke)){
		$this->db->where("minggu_ke", $minggu_ke);
		}
		$this->db->group_by("minggu_ke");
		$query = $this->db->get('default_laporan_absen');
		$result = $query->result_array();
		// dump($this->db->last_query(), $result);
		$result2 = array();
		foreach ($result as $key => $absen) {
			$absen['count_day'] = $this->get_count_day_by_week($thn, $bln, $absen['minggu_ke'], $id_laporan_tipe);
			$result2[$key] = $absen;
		}
		return $result2;
	}

	public function get_count_day_by_week($tahun, $bulan, $minggu_ke, $id_laporan_tipe = NULL){
		$this->db->select('tanggal');
		$this->db->where("tahun", $tahun);
		$this->db->where("bulan", $bulan);
		$this->db->where('minggu_ke', $minggu_ke);

		if($this->input->get('f-tipe_laporan')){
			$this->db->where('id_laporan_tipe', $this->input->get('f-tipe_laporan'));
		}

		if($id_laporan_tipe != NULL){
			$this->db->where('id_laporan_tipe', $id_laporan_tipe);
		}
		$this->db->order_by('tanggal','ASC');
		$query = $this->db->get('default_laporan_absen');
		$result = $query->result_array();
		$result2 = array();
		foreach ($result as $key => $value) {
			$result2[$key] = $value['tanggal'];
		}
		return $result2;
	}

	public function get_days_by_week($tahun, $bulan, $minggu_ke, $id_laporan_tipe = NULL){
		$this->db->where("tahun", $tahun);
		$this->db->where("bulan", $bulan);
		$this->db->where('minggu_ke', $minggu_ke);

		if($id_laporan_tipe != NULL){
			$this->db->where('id_laporan_tipe', $id_laporan_tipe);
		}else{
			if($this->input->get('f-tipe_laporan')){
				$this->db->where('id_laporan_tipe', $this->input->get('f-tipe_laporan'));
			}else{
				$this->db->group_by('tanggal');
			}
			$this->db->group_by('tanggal');
		}

		$query = $this->db->get('default_laporan_absen');
		$result = $query->result_array();
		return $result;
	}

	public function get_absen_user_by_type($id_laporan_tipe, $wheres){
		$this->db->from('default_laporan_data d');
		$this->db->join('default_organization_memberships m', 'd.id_user = m.membership_user');
		$this->db->join('default_organization_units u', 'u.id = m.membership_unit');
		$this->db->join('default_organization_types t', 't.id = u.unit_type');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'lp.id = k.id_provinsi');
		// if($id_laporan_tipe <=2){
			$this->db->select('*');
			$this->db->join('default_laporan_absen a' , 'a.id = d.id_absen');
			$this->db->group_by('d.id_user, t.id, a.tahun, a.bulan, a.tanggal');
		// }else{
		// 	$this->db->select('d.*, YEAR(d.created_on) AS tahun, MONTH(d.created_on) AS bulan, DATE(d.created_on) AS tanggal');
		// 	$this->db->group_by('d.id_user, t.id, YEAR(d.created_on), MONTH(d.created_on), DATE(d.created_on)');
		// }
		if($id_laporan_tipe == 3){
			$this->db->select("u.id as id_unit");
		}
		foreach ($wheres as $key => $where) {
			$this->db->$where['function']($where['column'], $where['value']);
		}
		if($this->input->get('f-provinsi') != ''){
			$this->db->where('lp.id', $this->input->get('f-provinsi'));
		}
		if($this->input->get('f-kota') != ''){
			$this->db->where('k.id', $this->input->get('f-kota'));
		}
		if($id_laporan_tipe != 3){
			$this->db->where('t.id', $id_laporan_tipe);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_absen_per_laporan($id_laporan_tipe, $thn_bln, $pagination_config, $count=0){
		$this->db->select('p.user_id, p.display_name, k.nama as kota, lp.nama as provinsi, u.id AS id_unit');
		$this->db->from('default_profiles p');
		$this->db->join('default_organization_memberships m', 'p.user_id = m.membership_user');
		$this->db->join('default_organization_units u', 'u.id = m.membership_unit');
		$this->db->join('default_organization_types t', 't.id = u.unit_type');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'lp.id = k.id_provinsi');

		$this->db->where('t.id', $id_laporan_tipe);

		if($this->input->get('f-provinsi') != ''){
			$this->db->where('lp.id', $this->input->get('f-provinsi'));
		}
		if($this->input->get('f-kota') != ''){
			$this->db->where('k.id', $this->input->get('f-kota'));
		}

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		$this->db->order_by('k.id_provinsi, k.id','ASC');
		$this->db->group_by('p.user_id, t.id');
		$query = $this->db->get();

		if($count == 0){
			$datas = $query->result_array();
			$result = array();
			foreach ($datas as $key => $data) {
				for($week=1;$week<=5;$week++){
					$data['days_on_week'.$week] = $this->get_days_on_week($id_laporan_tipe, $data['user_id'], $data['id_unit'], $week, $thn_bln);
				}
				$result[$key] = $data;
			}
		}else{

			$result = $query->num_rows();
		}

		return $result;
	}

	public function get_days_on_week($id_laporan_tipe, $id_user, $id_unit, $week, $thn_bln){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];

		$sql = "
			SELECT a.*, s.jml FROM default_laporan_absen a
			LEFT JOIN (
				SELECT d.id_absen, count(d.value) AS jml
				FROM default_laporan_data d
				JOIN default_laporan_metadata m ON m.id = d.id_metadata
				WHERE m.id_laporan_tipe = '{$id_laporan_tipe}'
				#AND d.id_user = '{$id_user}'
				AND d.id_unit = '{$id_unit}'
				GROUP BY d.id_absen
			) s ON s.id_absen = a.id
			WHERE DATE_FORMAT(a.tanggal,'%Y') = '{$thn}'
			AND a.bulan = '{$bln}'
			AND a.minggu_ke = '{$week}'
			AND a.id_laporan_tipe = '{$id_laporan_tipe}'
			ORDER BY a.tanggal ASC
		";
		$query = $this->db->query($sql);
		$absen = $query->result_array();
		$result = array();
		$w = 1;
		foreach ($absen as $key => $value) {
			$result[$w] = (!empty($value['jml'])) ? 1 : 0;
			$w++;
		}

		return $result;
	}

	public function get_max_day(){
		$sql = '
			SELECT MAX(a.jml_hari) max_hari
			FROM (
				SELECT tanggal_awal, minggu_ke, count(tanggal) AS jml_hari FROM default_laporan_absen
				GROUP BY minggu_ke, tanggal_awal
				ORDER BY tanggal_awal ASC 
			) a
		';

		$query = $this->db->query($sql);
		$result = $query->row_array();

		return $result['max_hari'];
	}

	public function get_hari($year, $month, $minggu_ke, $tipe_laporan){
		$thn_bln = $year.'-'.$month;
		$month = $month+0;
		$this->db->where("tahun", $year);
		$this->db->where("bulan", $month);
		$this->db->where('minggu_ke', $minggu_ke);
		if($tipe_laporan != NULL){
			$this->db->where('id_laporan_tipe', $tipe_laporan);
		}else{
			$this->db->group_by('tanggal');
		}
		$query = $this->db->get('default_laporan_absen');
		$result = $query->result_array();
		return $result;
	}

	public function grafik_absensi($thn_bln, $minggu_ke = NULL){
		$ex_thn_bln = explode('-',$thn_bln);
    $bln = $ex_thn_bln[1]+0;
    $thn = $ex_thn_bln[0];

    $and = '';
    if($this->input->get('f-provinsi')){
      $and .= ' AND k.id_provinsi = '.$this->input->get('f-provinsi');
    }
    if($this->input->get('f-kota')){
      $and .= ' AND k.id = '.$this->input->get('f-kota');
    }
    $and_minggu_ke = '';
    if($minggu_ke != NULL){
    	$and_minggu_ke .= ' AND a.minggu_ke = '.$minggu_ke;
    }

    $sql = "
    SELECT 
		t.type_name, 
		COUNT(u.id) as total, 
		IFNULL(q.melapor, 0) AS melapor,
		IFNULL(IF(q.melapor > 0, count(u.id)-q.melapor, count(u.id)), 0) AS belum_lapor 
		FROM default_organization_memberships m 
		JOIN default_organization_units u ON u.id = m.membership_unit
		JOIN default_organization_types t ON t.id = u.unit_type
		JOIN default_location_kota k ON k.id = u.id_kota
		LEFT JOIN (
			SELECT u.unit_type, COUNT(DISTINCT(u.id)) as melapor 
		    FROM default_organization_memberships m 
		    JOIN default_organization_units u ON u.id = m.membership_unit
		    JOIN default_laporan_data d ON d.id_unit = u.id
		    JOIN default_laporan_absen a ON a.id = d.id_absen
		    JOIN default_location_kota k ON k.id = u.id_kota
		    WHERE a.bulan = '{$bln}'
		    AND a.tahun = '{$thn}'
		    $and_minggu_ke
		    $and
		    GROUP BY u.unit_type
		) as q ON q.unit_type = t.id
		WHERE u.id > 0
		and u.unit_type != 3
		$and
		GROUP BY u.unit_type";
		$query = $this->db->query($sql);

		$result = $query->result_array();
		return $result;
	}

	public function count_all_absen()
	{
		return $this->db->count_all('laporan_absen');
	}
	
	public function delete_absen_by_tgl($tanggal)
	{
		$this->db->where('tanggal', $tanggal);
		$this->db->delete('default_laporan_absen');
	}
	
	public function insert_absen($values)
	{	
		return $this->db->insert('default_laporan_absen', $values);
	}
	
	public function update_absen($values, $row_id)
	{	
		$this->db->where('id', $row_id);
		return $this->db->update('default_laporan_absen', $values); 
	}

	public function delete_absen_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_absen');
	}
	
}