<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * sp2d model
 *
 * @author Aditya Satrya
 */
class Sp2d_m extends MY_Model {
	
	public function get_sp2d($pagination_config = NULL, $id_provinsi = NULL, $is_frontend = FALSE)
	{
		$this->db->select('s.*, p.nama as provinsi, pr.display_name');
		$this->db->from('default_laporan_sp2d s');
		$this->db->join('default_location_provinsi p', 'p.id = s.id_provinsi');
		$this->db->join('default_profiles pr', 'pr.user_id = s.created_by');
		$this->db->order_by('id','DESC');
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if($id_provinsi != NULL){
			$this->db->where('p.id', $id_provinsi);
		}

		if($this->input->get('f-provinsi') != ''){
			$this->db->where('p.id', $this->input->get('f-provinsi'));
		}
		if($this->input->get('f-nama_sp2d') != ''){
			$this->db->like('s.nama_sp2d', $this->input->get('f-nama_sp2d'));
		}
		if($this->input->get('f-status') != ''){
			$this->db->where('s.status', $this->input->get('f-status'));
		}

		if($is_frontend){
			$this->db->where('s.status','3');
		}
		$query = $this->db->get();
		$result = $query->result_array();
		
    return $result;
	}
	
	public function get_sp2d_by_id($id)
	{
		$this->db->select('s.*, p.nama as provinsi');
		$this->db->from('default_laporan_sp2d s');
		$this->db->join('default_location_provinsi p', 'p.id = s.id_provinsi');
		$this->db->where('s.id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_sp2d()
	{
		return $this->db->count_all('laporan_sp2d');
	}
	
	public function delete_sp2d_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_sp2d');
	}
	
	public function insert_sp2d($values)
	{	

		$values['created_by'] = $this->current_user->id;
		$values['created_on'] = date("Y-m-d H:i:s");
		return $this->db->insert('default_laporan_sp2d', $values);
	}
	
	public function update_sp2d($values, $row_id)
	{	
		$this->db->where('id', $row_id);
		return $this->db->update('default_laporan_sp2d', $values); 
	}
	
}