<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Sms model
 *
 * @author Aditya Satrya
 */
class Sms_m extends MY_Model {
	
	// public function get_sms($pagination_config = NULL, $count = 0, $tanggal_awal = NULL, $tanggal_akhir = NULL, $no_hp = NULL, $kode_sms = NULL, $id_provinsi = NULL)
	public function get_sms($pagination_config = NULL, $count = 0, $filters)
	{
		$this->db->select('k.id as id_kota, s.*, lp.id as id_provinsi, p.display_name, k.nama AS kota, lp.nama AS provinsi');
		$this->db->from('default_laporan_sms s');
		$this->db->join('default_profiles p','p.telp = s.no_hp');
		$this->db->join('default_organization_memberships m','m.membership_user = p.user_id');
		$this->db->join('default_organization_units u','u.id = m.membership_unit');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'k.id_provinsi = lp.id');
		$this->db->not_like('s.isi','dialog');
		// dump($filters);

		// $this->db->where("DATE_FORMAT(s.tanggal,'%Y-%m-%d') >= ", $tanggal_awal);
		// $this->db->where("DATE_FORMAT(s.tanggal,'%Y-%m-%d') <= ", $tanggal_akhir);

		// if($id_provinsi != NULL){
		// 	$this->db->where('lp.id', $id_provinsi);
		// }
		
		// if($this->input->get('f-provinsi') != ''){
		// 	$this->db->where('lp.id', $this->input->get('f-provinsi'));
		// }
		// if($this->input->get('f-kota') != ''){
		// 	$this->db->where('k.id', $this->input->get('f-kota'));
		// }
		// if($this->input->get('f-status_error') != ''){
		// 	$this->db->like('s.status_error', $this->input->get('f-status_error'));
		// }

		// if($this->input->get('id_organization_unit') > 0){
		// 	$this->db->where('m.membership_unit', $this->input->get('id_organization_unit'));
		// }

		// if($no_hp != NULL){
		// 	$this->db->where('s.no_hp', $no_hp);
		// }

		// if($kode_sms != NULL){
		// 	$this->db->like('s.isi', $kode_sms);
		// }

		// if(!empty($status)){
		// 	$this->db->where('status_error', $status);
		// }

		foreach ($filters as $field => $data) {
			if($field == 's.isi'){
				$this->db->like($field, $data);
			}else{
				$this->db->where($field, $data);
			}
		}
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->order_by('s.tanggal', 'DESC');
		$this->db->group_by('s.id');
		$query = $this->db->get();

		if($count == 0){
			$result = $query->result_array();
		}else{
			$result = $query->num_rows();
		}

		// dump($this->db->last_query());
		// die();
		
    return $result;
	}
	
	public function get_sms_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('default_laporan_sms s');
		$this->db->join('default_profiles p','p.telp = s.no_hp');
		$this->db->join('default_location_kota k', 'k.id = p.id_kota');
		$this->db->where('s.id', $id);
		$query = $this->db->get();
		$result = $query->row_array();
		
		return $result;
	}

	public function get_new_id(){
		$this->db->select_max('id');
		$query = $this->db->get('default_laporan_sms');
		$result = $query->row_array();
		$id = $result['id']+1;
		return $id;
	}

	public function check_kode($kode){
		$this->db->where('kode', $kode);
		$query = $this->db->get('default_laporan_sms');
		$result = $query->num_rows();
		if($result > 0){
			return false;
		}
		return true;
	}

	public function count_all_sms()
	{
		return $this->db->count_all('laporan_sms');
	}
	
	public function delete_sms_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_sms');
	}
	
	public function insert_sms($values)
	{	
		return $this->db->insert('default_laporan_sms', $values);
	}
	
	public function update_sms($values, $row_id)
	{
		$this->db->where('id', $row_id);
		return $this->db->update('default_laporan_sms', $values); 
	}
	
}