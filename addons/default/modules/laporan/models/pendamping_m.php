<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Data model
 *
 * @author Aditya Satrya
 */
class Pendamping_m extends MY_Model {
	public function cek_no_hp($nohp){
		$this->db->select('telp');
		$this->db->where('telp', $nohp);
		$query = $this->db->get('default_profiles');
		$result = $query->num_rows();
		if($result > 0){
			return true;
		}
		return false;
	}

	public function get_pendamping_by_type($id_laporan_tipe, $pagination_config){
		$this->db->select('p.user_id, p.display_name, k.nama as kota, lp.nama as provinsi, u.id AS id_unit');
		$this->db->from('default_profiles p');
		$this->db->join('default_organization_memberships m', 'p.user_id = m.membership_user');
		$this->db->join('default_organization_units u', 'u.id = m.membership_unit');
		$this->db->join('default_organization_types t', 't.id = u.unit_type');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'lp.id = k.id_provinsi');

		$this->db->where('t.id', $id_laporan_tipe);

		if($this->input->get('f-provinsi') != ''){
			$this->db->where('lp.id', $this->input->get('f-provinsi'));
		}
		if($this->input->get('f-kota') != ''){
			$this->db->where('k.id', $this->input->get('f-kota'));
		}

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		$this->db->order_by('k.id_provinsi, k.id, p.user_id','ASC');
		$this->db->group_by('p.user_id, t.id');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_pendamping_by_phone($id_tipe, $nohp, $kode_tti = NULL){
		$this->db->select('user_id, u.id as id_unit, us.group_id as id_grup');
		$this->db->from('default_organization_memberships m');
		$this->db->join('default_organization_units u','u.id = m.membership_unit');
		if($id_tipe == 3){
			$this->db->select('u2.row_id as id_unit');
			$this->db->join('default_organization_units_units u2','u2.units_id = u.id');
		}
		$this->db->join('default_profiles p','p.user_id = m.membership_user');
		$this->db->join('default_users us','p.user_id = us.id');
		$this->db->where('u.unit_type',$id_tipe);
		$this->db->where('p.telp', $nohp);
		$this->db->order_by('u.id','ASC');
		if($kode_tti != NULL){
			$this->db->where('u.kode_tti', $kode_tti);
		}
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function get_user_by_telp($nohp){
		$this->db->select('p.display_name, u.email');
		$this->db->from('default_profiles p');
		$this->db->join('default_users u','u.id = p.user_id');
		$this->db->where('telp', $nohp);
		$query = $this->db->get();
		$result = $query->row_array();
		
		return $result;
	}

	public function get_pendamping_by_id($id_user){
		$this->db->select('*');
		$this->db->from('default_profiles p');
		$this->db->where('user_id',$id_user);
		$query = $this->db->get();

		$result = $query->row_array();
		return $result;
	}

	public function get_kode_tti_by_telp($telp){
		$this->db->select('k.nama as kota, pr.nama as provinsi, u.unit_name');
		$this->db->from('default_organization_memberships m');
		$this->db->join('default_organization_units u','u.id = m.membership_unit');
		$this->db->join('default_profiles p','p.user_id = m.membership_user');
		$this->db->join('default_location_kota k','k.id = u.id_kota');
		$this->db->join('default_location_provinsi pr','pr.id = k.id_provinsi');
		$this->db->where('u.unit_type',2);
		$this->db->where('p.telp', $telp);
		$this->db->order_by('u.id','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function get_kode_ttic_by_telp($telp){
		$this->db->select('k.nama as kota, pr.nama as provinsi, u.unit_name');
		$this->db->from('default_organization_memberships m');
		$this->db->join('default_organization_units u','u.id = m.membership_unit');
		$this->db->join('default_profiles p','p.user_id = m.membership_user');
		$this->db->join('default_location_kota k','k.id = u.id_kota');
		$this->db->join('default_location_provinsi pr','pr.id = k.id_provinsi');
		$this->db->where('p.telp', $telp);
		$this->db->order_by('u.id','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}


	public function get_pendamping($pagination_config = NULL, $minggu_ke = NULL, $thn_bln = NULL, $is_print = FALSE, $group = null){
		if ($this->input->get('f-groups') == '9' || $group == '9'){
			$this->db->select('u.id as id_unit, u.unit_type, u.unit_name, ut.type_name, u.nama_ketua, u.hp_ketua, u.unit_description, p.user_id, p.display_name, p.telp, pr.id as id_provinsi, pr.nama as provinsi, u.id_kota, k.nama as kota');
		} else {
			$this->db->select('u.id as id_unit, p.user_id, p.display_name, p.telp, pr.id as id_provinsi, pr.nama as provinsi, u.id_kota, k.nama as kota');
		
		}
		$this->db->select('u.id as id_unit, p.user_id, p.display_name, p.telp, pr.id as id_provinsi, pr.nama as provinsi, u.id_kota, k.nama as kota');
		$this->db->from('default_location_kota k');
		$this->db->join('default_organization_units u','u.id_kota = k.id');
		$this->db->join('default_organization_types ut','ut.id = u.unit_type');
		$this->db->join('default_organization_memberships m','m.membership_unit = u.id');
		$this->db->join('default_users ur', 'ur.id = m.membership_user');
		$this->db->join('default_profiles p','p.user_id = m.membership_user');
		$this->db->join('default_location_provinsi pr','k.id_provinsi = pr.id');
		if ($this->input->get('f-groups') == '7' || $group == '7'){
			$this->db->where('u.unit_type', 1);
		}
		if ($this->input->get('f-groups') == '9' || $group == '9'){
			$this->db->where('ur.group_id', 9);
		}
		if ($this->input->get('f-groups') == '10' || $group == '10'){
			$this->db->where('u.unit_type', 6);
		}

		if($this->input->get('f-tipe_unit')){
			$this->db->where('ut.id',$this->input->get('f-tipe_unit'));
		}

		if($this->input->get('f-provinsi') != ''){
			$this->db->where('pr.id', $this->input->get('f-provinsi'));
		}
		if($this->input->get('f-kota') != ''){
			$this->db->where('k.id', $this->input->get('f-kota'));
		}
		if($this->input->get('f-user_id') != ''){
			$this->db->where('p.user_id', $this->input->get('f-user_id'));
		}

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		$this->db->order_by('k.id', 'ASC');
		if ($this->input->get('f-groups') == '7' || $this->input->get('f-groups') == 10 || $group == 7 || $group == 10){
			$this->db->group_by('k.id');
		}
		$query = $this->db->get();
		$result = $query->result_array();

		if ($this->input->get('f-groups') == '9' || $group == '9'){
			return $result;
		}

		if(is_array($result) || $is_print){
			if($this->input->get('f-groups')=='7' || $group == 7){
				$root_unit = 1; // id tipe unit gapoktan 2016
				$unit_child = 2; // id_tipe unit tti 2016
			}
			if($this->input->get('f-groups')=='10' || $group == 10){
				$root_unit = 6; // id tipe unit gapoktan 2018
				$unit_child = 7; // id tipe unit tti 2018
			}
			foreach ($result as $key => $value) {
				$result[$key]['gapoktan'] = $this->get_unit_by_kota($root_unit, $value['id_kota']);
				if($minggu_ke != NULL && $thn_bln != NULL){
					$result[$key]['metadata_gapoktan'] = $this->get_kinerja(1, $value['id_unit'], $minggu_ke, $thn_bln);
					// dump($this->db->last_query());
					// die();
				}
				$result[$key]['total_gap'] = count($result[$key]['gapoktan']);
				$total_tti = 0;
				foreach ($result[$key]['gapoktan'] as $key2 => $gap) {
					// $result[$key]['gapoktan'][$key2]['tti'] = $this->get_unit_by_kota($unit_child, $value['id_kota'], $gap['user_id']);
					$result[$key]['gapoktan'][$key2]['tti'] = $this->get_unit_child($gap['id_unit'], $unit_child, $gap['user_id']);
					foreach ($result[$key]['gapoktan'][$key2]['tti'] as $key3 => $tti) {
						if($minggu_ke != NULL && $thn_bln != NULL){
							$result[$key]['gapoktan'][$key2]['tti'][$key3]['metadata_tti'] = $this->get_kinerja(2, $tti['id_unit'], $minggu_ke, $thn_bln);
						}
					}
					
					$total_tti += count($result[$key]['gapoktan'][$key2]['tti']);
				}
				$result[$key]['total_tti'] = $total_tti;
			}
		}
		return $result;
	}


	public function get_unit_child($unit_root, $unit_type, $id_user = null){
		$this->db->select('u.id as id_unit, u.unit_name, u.unit_type, u.unit_description, u.nama_ketua, u.hp_ketua, p.display_name, p.telp, p.user_id');
		$this->db->from('default_organization_units u');
		$this->db->join('default_organization_units_units u2','u2.row_id = u.id');
		$this->db->join('default_organization_memberships m','m.membership_unit = u.id');
		$this->db->join('default_profiles p','p.user_id = m.membership_user');
		$this->db->where('u2.units_id', $unit_root);
		$this->db->where('u.unit_type', $unit_type);
		if($id_user != NULL){
			$this->db->where('m.membership_user', $id_user);
		}
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	public function get_unit_by_kota($unit_type, $id_kota, $id_user = null){
		$this->db->select('u.id as id_unit, u.unit_name, u.unit_type, u.unit_description, u.nama_ketua, u.hp_ketua, p.display_name, p.telp, p.user_id');
		$this->db->from('default_organization_units u');
		$this->db->join('default_organization_memberships m','m.membership_unit = u.id');
		$this->db->join('default_profiles p','p.user_id = m.membership_user');
		$this->db->where('u.id_kota', $id_kota);
		$this->db->where('u.unit_type', $unit_type);
		if($id_user != NULL){
			$this->db->where('m.membership_user', $id_user);
		}
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function get_kinerja($id_laporan_tipe, $id_unit, $minggu_ke, $thn_bln){
		$ex_thn_bln = explode('-',$thn_bln);
		$bln = $ex_thn_bln[1]+0;
		$thn = $ex_thn_bln[0];
		$this->db->select("d.id_metadata, m.nama as nama_metadata, tf.nama_tipe_field, ( CASE WHEN (tf.nama_tipe_field = 'Harga') THEN avg(d.value) ELSE sum(d.value) END ) as total");
		$this->db->from('default_laporan_data d');
		$this->db->join('default_laporan_metadata m','m.id = d.id_metadata');
		$this->db->join('default_laporan_tipe_field tf','tf.id = m.id_laporan_tipe_field');
		$this->db->join('default_laporan_absen a','a.id = d.id_absen');
		$this->db->where('d.id_unit', $id_unit);
		$this->db->where('m.id_laporan_tipe', $id_laporan_tipe);

		$this->db->where('a.tahun', $thn);

		if(isset($_GET['f-bln'])){
			if($_GET['f-bln'] != ""){
				$this->db->where('a.bulan', $bln);
			}
		}else{
			$this->db->where('a.bulan', $bln);
		}

		if(isset($_GET['f-minggu_ke'])){
			if($_GET['f-minggu_ke'] != ""){
				$this->db->where('a.minggu_ke', $minggu_ke);
			}
		}else{
			$this->db->where('a.minggu_ke', $minggu_ke);
		}

		if($this->input->get('f-hari') != ''){
			$this->db->where('a.tanggal', $this->input->get('f-hari'));
		}

		$this->db->group_by('d.id_metadata');
		$query = $this->db->get();
		$result = $query->result_array();
		$return = array();
		foreach ($result as $key => $data) {
			$return[$data['id_metadata']] = $data;
		}
		return $return;
		
	}
}