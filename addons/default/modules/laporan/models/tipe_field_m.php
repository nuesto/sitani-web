<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * tipe_field model
 *
 * @author Aditya Satrya
 */
class Tipe_field_m extends MY_Model {
	
	public function get_tipe_field()
	{
		$query = $this->db->get('default_laporan_tipe_field');
		$result = $query->result_array();
    return $result;
	}
	
	public function get_tipe_field_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_laporan_tipe_field');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function delete_tipe_field_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_tipe_field');
	}
	
	public function insert_tipe_field($values)
	{
		return $this->db->insert('default_laporan_tipe_field', $values);
	}
	
	public function update_tipe_field($values, $row_id)
	{
		$this->db->where('id', $row_id);
		return $this->db->update('default_laporan_tipe_field', $values); 
	}
	
}