<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * tipe model
 *
 * @author Aditya Satrya
 */
class Tipe_m extends MY_Model {
	
	public function get_all_tipe($filters = false)
	{
		if(is_array($filters)){
			foreach ($filters as $key => $filter) {
				$this->db->$filter['function']($filter['column'], $filter['value']);
			}
		}
		$query = $this->db->get('default_laporan_tipe');
		$result = $query->result_array();
		
    return $result;
	}
	public function get_tipe($just_tti_gapoktan = false)
	{

		$this->db->select('default_laporan_tipe.*');
		if ($just_tti_gapoktan == true){
		$this->where("default_laporan_tipe.id < 3");
		}
		$query = $this->db->get('default_laporan_tipe');
		$result = $query->result_array();
		
    return $result;
	}
	
	public function get_tipe_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_laporan_tipe');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_tipe()
	{
		return $this->db->count_all('laporan_tipe');
	}
	
	public function delete_tipe_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_tipe');
	}
	
	public function insert_tipe($values)
	{	
		return $this->db->insert('default_laporan_tipe', $values);
	}
	
	public function update_tipe($values, $row_id)
	{
		$values['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_laporan_tipe', $values); 
	}
	
}