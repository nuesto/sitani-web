<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * komoditas model
 *
 * @author Aditya Satrya
 */
class Komoditas_m extends MY_Model {
	
	public function get_komoditas()
	{
		$this->db->order_by('urutan, nama_komoditas', "ASC");
		$query = $this->db->get('default_laporan_komoditas');
		$result = $query->result_array();
		
    return $result;
	}

	public function get_komoditas_related()
	{
		$this->db->select('k.*');
		$this->db->from('default_laporan_komoditas k');
		$this->db->join('default_laporan_metadata m','m.id_laporan_komoditas = k.id');
		$this->db->order_by('k.urutan, k.nama_komoditas', "ASC");
		$this->db->group_by('k.id');
		$query = $this->db->get();
		$result = $query->result_array();
		
    return $result;
	}

	public function get_komoditas_related_by_tipe($id_laporan_tipe)
	{
		$this->db->select('k.*');
		$this->db->from('default_laporan_komoditas k');
		$this->db->join('default_laporan_metadata m','m.id_laporan_komoditas = k.id');
		$this->db->where('m.id_laporan_tipe', $id_laporan_tipe);
		$this->db->order_by('k.urutan, k.nama_komoditas', "ASC");
		$this->db->group_by('k.id');
		$query = $this->db->get();
		$result = $query->result_array();
		
    return $result;
	}

	public function get_first_komoditas(){
		$this->db->order_by('urutan, nama_komoditas', "ASC");
		$this->db->limit(1);
		$query = $this->db->get('default_laporan_komoditas');
		$result = $query->row_array();
		
    return $result;
	}
	
	public function get_komoditas_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_laporan_komoditas');
		$result = $query->row_array();
		
		return $result;
	}

	public function is_used($id){
		$this->db->where('id_laporan_komoditas', $id);
		$query = $this->db->get('default_laporan_metadata');
		if($query->num_rows() > 0){
			return false;
		}
		return true;
	}

	public function delete_komoditas_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_laporan_komoditas');
	}
	
	public function insert_komoditas($values)
	{
		return $this->db->insert('default_laporan_komoditas', $values);
	}
	
	public function update_komoditas($values, $row_id)
	{
		$this->db->where('id', $row_id);
		return $this->db->update('default_laporan_komoditas', $values); 
	}

	public function update_urutan($id, $i){
    return $this->db->query("UPDATE default_laporan_komoditas SET urutan = '$i' WHERE id = '$id'");
  }
	
}