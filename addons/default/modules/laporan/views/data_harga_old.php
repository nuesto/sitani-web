<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>Laporan <?php echo ucfirst($nama_laporan) ?> <small>( <?php if($firstDayOfWeek == NULL){ echo lang('laporan:absen:no_absen'); }else{ echo date_idr($weeks['first_day_of_week'], 'd F Y', null); ?> - <?php echo date_idr($weeks['last_day_of_week'], 'd F Y', null); }?> )</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
				<div class="portlet-title">
					<div class="caption">
            <i class="fa fa-search font-red"></i>
            <span class="caption-subject font-red bold uppercase"> Filter
            </span>
          </div>
				</div>
				<div class="portlet-body">
					<?php echo form_open(base_url().'laporan/data/harga/'.$id, array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-provinsi"><?php echo lang('location:provinsi:singular'); ?></label>
              <div class="col-sm-4">
                <?php
                  $val_prov = null;
                  if($this->input->get('f-provinsi') != ""){
                    $val_prov = $this->input->get('f-provinsi');
                  }
                ?>
                <select name="f-provinsi" class="form-control" id="provinsi">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                    <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-kota"><?php echo lang('location:kota:singular'); ?></label>
                <div class="col-sm-4">
                  <select name="f-kota" id="kota" class="form-control">
                    <?php
                      if($this->input->get('f-provinsi') != '') {
                        $value = null;
                        if($this->input->get('f-kota') != ""){
                          $value = $this->input->get('f-kota');
                        }
                    ?>
                      <option value=""><?php echo lang('global:select-pick') ?></option>
                    <?php
                        foreach ($kota['entries'] as $kota_entry){ ?>
                          <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                    <?php
                        }
                      }else{
                    ?>
                        <option value=""><?php echo lang('global:select-none') ?></option>
                    <?php
                      }
                    ?>
                  </select>

                  <script type="text/javascript">
                    <?php if($val_prov == NULL) { ?>
                      $(document).ready(function(){
                          $("#provinsi").change();
                      })
                    <?php } ?>
                    $('#provinsi').change(function() {
                      var id_provinsi = $(this).val();
                      $("#kota").html('<option value=""><?php echo  lang("laporan:ajax_load_data") ?></option>');
                      $.ajax({
                        url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                        dataType: 'json',
                        success: function(data){
                          if(data.length > 0){
                            $('#kota').html('<option value="">-- Pilih --</option>');
                          }else{
                            $('#kota').html('<option value="">-- Tidak ada --</option>');
                          }
                          $.each(data, function(i, object){
                            $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                          });

                          $("#kota").change();
                        }
                      });
                    });

                    // khusu sitoni
                    $('#kota').change(function() {
                      var kota = ($(this).val() == '') ? 0 : $(this).val();
                      var tipe_laporan = 1;
                      $("#<?php echo $org_name ?>").html('<option value="-1"><?php echo  lang("global:ajax_load_data") ?></option>');
                      $("#<?php echo $org_name ?>").load("<?php echo site_url('laporan/data/ajax_get_unit_by_kota') ?>" + '/' + kota + '/' + tipe_laporan, function(data) {
                          $("#<?php echo $org_name ?>").change();
                      });
                    });
                  </script>
                </div>
            </div>

            <?php
            if ($this->uri->segment(4) != 3){

              // if($id == 2){
                // Load Organization--------

                $n = 0;
                $addedLevel = array();

                if(isset($organization['id_organization_unit']) && $organization['id_organization_unit'] != -1) { ?>
                  <input type="hidden" name="id_organization_unit" value="<?php echo $organization['id_organization_unit'] ?>">
                  <?php
                }

                foreach ($types as $key => $type) {
                    if (in_array($type['level'], $addedLevel)) {
                        continue;
                    }
                    $addedLevel[] = $type['level'];
                    $n++;
                    $types2[] = $type;
                    if($type['slug'] == $node_type_slug){
                        break;
                    }
                }
                $n2 = $n;

                foreach ($types2 as $i => $type) {

                  $parent_field_name = isset($field_name) ? $field_name : null;
                  $field_name = $i == $n - 1 ? 'id_organization_unit' : ('id_organization_unit_' . $type['level']);
                  ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="<?php echo $field_name; ?>"><?php echo $type['name']; ?></label>
                    <div class="col-sm-4">
                      <?php
                      if(isset($organization[$field_name])){
                        $value = $organization[$field_name];
                      } else {
                        $value = $this->input->get($field_name);
                      }

                      $organization[$field_name] = $value != NULL ? $value : -1;
                      $parent_lvl = $type['level'] - 1;

                      if(isset($organization['organization_name_'.$type['level']])) { ?>
                        <div class="entry-value col-sm-5" id="unit-predefined-text"><?php echo $organization['organization_name_'.$type['level']]; ?></div>
                        <?php
                      } else { ?>

                          <select id="<?php echo $field_name; ?>" name="<?php echo $field_name; ?>" class="form-control">
                            <option value="-1"><?php echo lang('global:select-pick'); ?></option>
                            <?php
                            foreach ($child_organization as $child) {
                              echo '<option value="' . $child['id'] . '"> ' . $child['unit_name'] . '</option>';
                            }
                            ?>
                          </select>

                          <script>
                            $( document ).ready(function() {
                              $("#<?php echo $field_name; ?>").val(<?php echo $organization[$field_name]; ?>).change();
                            });
                          </script>

                          <?php
                      }

                      if ($parent_field_name) { ?>
                        <script type="text/javascript">
                          $('#<?php echo $parent_field_name; ?>').change(function() {
                            var parent_id = $(this).val();
                            $("#<?php echo $field_name; ?>").html('<option><?php echo  lang("global:ajax_load_data") ?></option>');
                            $("#<?php echo $field_name; ?>").load("<?php echo site_url('laporan/data/ajax_unit_dropdown') ?>" + '/' + parent_id, function(data) {
                              if(data != '<option value="-1">-- Tidak ada --</option>'){
                                  $(this).val(<?php echo $organization[$field_name]; ?>).change();
                              }
                            });
                          });
                        </script>
                        <?php
                      }
                      ?>
                    </div>
                  </div>
                  <?php
                }
              // }
            }
            ?>

            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:periode'); ?></label>
              <div class="col-sm-4">
                <div class="row">
                  <div class="col-sm-3" style="padding-right: 0px;">
                    <?php
                      $value = $thn;
                      if($this->input->get('f-thn') != ""){
                        $value = $this->input->get('f-thn');
                      }
                    ?>
                    <select name="f-thn" id="f-thn" class="form-control">
                      <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
                        <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-5" style="padding-right: 0px;">
                    <?php
                      $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
                      $value = $bln;
                      if($this->input->get('f-bln') != ""){
                        $value = $this->input->get('f-bln');
                      }
                    ?>
                    <select name="f-bln" id="f-bln" class="form-control">
                      <?php
                        foreach ($arr_month as $key => $month) { ?>
                          <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
                          <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="col-sm-3" style="padding-right: 0px;">
                    <?php
                      $value = $minggu_ke;
                      if($this->input->get('f-minggu_ke') != ""){
                        $value = $this->input->get('f-minggu_ke');
                      }
                    ?>
                    <select name="f-minggu_ke" id="f-minggu_ke" class="form-control">
                      <?php
                        $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
                        for ($i=1;$i<=$end_week;$i++) { ?>
                          <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
                          <?php
                        }
                      ?>
                    </select>
                    <script type="text/javascript">
                      $('#f-thn, #f-bln').change(function() {
                        var thn = $('#f-thn').val();
                        var bln = $('#f-bln').val();
                        $("#f-minggu_ke").html('<option value="">...</option>');
                        $("#f-minggu_ke").load("<?php echo site_url('laporan/data/ajax_get_minggu_ke2') ?>" + '/' + thn + '/' + bln, function(data) {
                        });
                      });
                    </script>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-komoditas"><?php echo lang('laporan:komoditas'); ?></label>
              <div class="col-sm-4">
                <?php
                  $val_kom = null;
                  if($this->input->get('f-komoditas') != ""){
                    $val_kom = $this->input->get('f-komoditas');
                  }
                ?>
                <select name="f-komoditas" id="komoditas" class="form-control">
                  <?php foreach ($komoditas_related as $key){ ?>
                    <option value="<?php echo $key['id'] ?>" <?php echo ($val_kom == $key['id']) ? 'selected' : ''; ?>><?php echo $key['nama_komoditas'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-metadata"><?php echo lang('laporan:metadata:singular'); ?></label>
              <div class="col-sm-4">
                <?php
                  $value = null;
                  if($this->input->get('f-metadata') != ""){
                    $value = $this->input->get('f-metadata');
                  }
                ?>
                <select name="f-metadata" id="metadata" class="form-control">
                  <option value="">-- Pilih --</option>
                  <?php foreach ($metadata as $metadata_entry){ ?>
                    <option value="<?php echo $metadata_entry['id'] ?>" <?php echo ($value == $metadata_entry['id']) ? 'selected' : ''; ?>><?php echo $metadata_entry['nama'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <script type="text/javascript">
                    <?php if($val_kom == NULL) { ?>
                      $(document).ready(function(){
                          $("#komoditas").change();
                      })
                    <?php } ?>
                    $('#komoditas').change(function() {
                      var id_komoditas = $(this).val();
                      var id_laporan_tipe = '<?php echo $id ?>';
                      $("#metadata").html('<option value=""><?php echo  lang("laporan:ajax_load_data") ?></option>');
                      $.ajax({
                        url: "<?php echo site_url('laporan/data/get_metadata_by_komoditas') ?>" + '/' + id_laporan_tipe + '/' + id_komoditas,
                        dataType: 'json',
                        success: function(data){
                          if(data.length > 0){
                            $('#metadata').html('<option value="">-- Pilih --</option>');
                          }else{
                            $('#metadata').html('<option value="">-- Tidak ada --</option>');
                          }
                          $.each(data, function(i, object){
                            $('#metadata').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                          });

                          $("#metadata").change();
                        }
                      });
                    });

                  </script>

            <div class="form-group">
              <div class="col-sm-2"></div>
              <div class="col-sm-4">
                <button href="<?php echo current_url() . '#'; ?>" class="btn btn-success" type="submit">
                  <i class="icon-ok"></i>
                  Filter
                </button>

                <a href="<?php echo site_url('laporan/data/harga/'.$id); ?>" class="btn btn-default">
                  <i class="icon-remove"></i>
                  Clear
                </a>
              </div>
            </div>
          <?php echo form_close() ?>
				</div>
			</div>
    </div>

    <?php if($firstDayOfWeek != NULL) { ?>
      <div class="page-content-inner">
        <div class="portlet light portlet-fit ">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-line-chart font-red"></i>
              <span class="caption-subject font-red bold uppercase"> Statistik Laporan
              </span>
            </div>
          </div>
          <div class="portlet-body">
            <div id="statistik" style="overflow-x: auto; height: 400px; margin: 0 auto">
            </div>
            <script src="<?php echo base_url() ?>addons/default/modules/laporan/js/highcharts.js" type="text/javascript"></script>
            <script src="<?php echo base_url() ?>addons/default/modules/laporan/js/exporting.js" type="text/javascript"></script>
            <script type="text/javascript">

              $(function () {
                $('#statistik').highcharts({
                  title: {
                    text: 'Perkembangan PUPM Melalui Kegiatan TTI',
                    x: -20 //center
                  },
                  xAxis: {
                    categories: ['Interval 35 hari', 'Interval 28 hari', 'Interval 21 hari', 'Interval 14 hari', 'Minggu Sebelumnya', 'Minggu ke : <?php echo $minggu_ke ?> (<?php echo $thn_bln ?>)'],
                    crosshair: true
                  },
                  yAxis: [{
                    min : 0,
                    title: {
                      text: 'Harga',
                      style: {
                        color: Highcharts.getOptions().colors[0]
                      }
                    },
                    labels: {
                      format: '{value} Rp',
                      style: {
                        color: Highcharts.getOptions().colors[0]
                      }
                    }
                  },{
                    min : 0,
                    title: {
                      text: 'Volume',
                      style: {
                        color: Highcharts.getOptions().colors[0]
                      }
                    },
                    labels: {
                      format: '{value} Kg',
                      style: {
                        color: Highcharts.getOptions().colors[0]
                      }
                    },
                    opposite: true
                  }],
                  tooltip: {
                    shared: true
                  },
                  series: <?php echo $json_series ?>
                });
              });
            </script>
          </div>
        </div>
      </div>

      <div class="page-content-inner">
        <div class="portlet light portlet-fit ">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-table font-red"></i>
              <span class="caption-subject font-red bold uppercase"> Tabel Perkembangan PUPM Melalui Kegiatan TTI
              </span>
            </div>
            <div class="actions">
              <?php if (count($data['entries']) > 0){ ?>
                <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url() ?>laporan/data/harga/<?php echo $id ?>/0?<?php echo $_SERVER['QUERY_STRING'] ?>">
                  <i class="fa fa-download"></i>
                </a>

                <a class="btn btn-circle btn-icon-only btn-default" target="blank" href="<?php echo base_url() ?>laporan/data/harga/<?php echo $id ?>/1?<?php echo $_SERVER['QUERY_STRING'] ?>">
                  <i class="fa fa-print"></i>
                </a>
              <?php } ?>
            </div>
          </div>
          <div class="portlet-body" style="overflow-y: auto">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th rowspan="2">Komoditas</th>
                  <th rowspan="2">Satuan</th>
                  <th colspan="3">Nilai</th>
                  <th>Persentase Perubahan</th>
                  <th rowspan="2">Fluktuasi</th>
                </tr>
                <tr>
                  <!--<th>3 Bulan yang Lalu</th>-->
                  <th>Akumulasi<br>(awal waktu - minggu lalu)</th>
                  <th>Minggu ke : <?php echo $minggu_ke ?> (<?php echo $thn_bln ?>)</th>
                  <th>Akumulasi <br>(awal waktu - minggu ini)</th>
                  <th>Minggu Lalu - Minggu ke : <?php echo $minggu_ke ?> (<?php echo $thn_bln ?>)</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($data['entries'] as $key => $entries) {
                    $mingguini = ($entries['harga_minggu_ini'] != "") ? $entries['harga_minggu_ini'] : 0;
                    $tigabulanlalu = ($entries['harga3bln']) ? $entries['harga3bln'] : 0;
                    $minggulalu = ($entries['minggu7']) ? $entries['minggu7'] : 0;

                    if($mingguini > $tigabulanlalu){
                      $color1 = 'red';
                      $text1 = 'Naik';
                      $silit1 = $mingguini-$tigabulanlalu;
                      $silit1 = ($silit1 == 0) ? 0 : ($silit1/$mingguini)*100;
                      $silit1 = round($silit1,2);
                    }else{
                      $color1 = '#E4A000';
                      $text1 = 'Turun';

                      $silit1 = $tigabulanlalu-$mingguini;
                      $silit1 = ($silit1 == 0) ? 0 : ($silit1/$tigabulanlalu)*100;
                      $silit1 = round($silit1,2);
                    }

                    if($mingguini > $minggulalu){
                      $color2 = 'red';
                      $text2 = 'Naik';
                      $silit2 = $mingguini-$minggulalu;
                      $silit2 = ($silit2 == 0) ? 0 : ($silit2/$mingguini)*100;
                      $silit2 = round($silit2,2);
                    }else{
                      $color2 = '#E4A000';
                      $text2 = 'Turun';
                      $silit2 = $minggulalu-$mingguini;
                      $silit2 = ($silit2 == 0) ? 0 : ($silit2/$minggulalu)*100;
                      $silit2 = round($silit2,2);
                    }

                    if($mingguini == $tigabulanlalu){
                      $color1 = 'red';
                      $text1 = 'Stabil';
                    }

                    if($mingguini == $minggulalu){
                      $color2 = 'red';
                      $text2 = 'Stabil';
                    }

                    if($entries['fluktuasi'] != 0){
                      $fluktuasi = round($entries['fluktuasi'] / $mingguini, 2).'%';
                    }else{
                      $fluktuasi = '0%';
                    }
                    // $fluktuasi = number_format($entries['fluktuasi'],0,',','.');

                ?>
                  <tr>
                    <td><?php echo $entries['komoditas'] ?></td>
                    <td><?php echo $entries['satuan'] ?></td>
                    <td style="text-align:right;"><?php echo number_format($entries['akumulasi'],0,',','.'); ?></td>
                    <td style="text-align:right;"><?php echo number_format($entries['harga_minggu_ini'],0,',','.'); ?></td>
                    <td style="text-align:right;"><?php echo number_format($entries['total'],0,',','.'); ?></td>
                    <td style="text-align:right; color:<?php echo $color2 ?>"><?php echo $text2. " ".$silit2."%" ?></td>
                    <td style="text-align:right;"><?php echo $fluktuasi; ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <?php }else{ ?>
      <div class="portlet light portlet-fit ">
        <div class="portlet-body font-red">
          <?php echo lang('laporan:absen:no_absen'); ?>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
