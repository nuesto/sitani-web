<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo lang('laporan:pendamping:singular') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-search font-red"></i>
            <span class="caption-subject font-red bold uppercase"> Filter
            </span>
          </div>
        </div>
        <div class="portlet-body">
          <?php echo form_open(base_url().'laporan/pendamping/index/'.$id_group, array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-provinsi"><?php echo lang('location:provinsi:singular'); ?></label>
              <div class="col-sm-4">
                <?php
                  $value = null;
                  if($this->input->get('f-provinsi') != ""){
                    $value = $this->input->get('f-provinsi');
                  }
                ?>
                <select name="f-provinsi" class="form-control" id="provinsi">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                    <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($value == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-kota"><?php echo lang('location:kota:singular'); ?></label>
                <div class="col-sm-4">
                  <select name="f-kota" id="kota" class="form-control">
                    <?php 
                      if($this->input->get('f-provinsi') != '') {
                        $value = null;
                        if($this->input->get('f-kota') != ""){
                          $value = $this->input->get('f-kota');
                        }
                    ?>
                      <option value=""><?php echo lang('global:select-pick') ?></option>
                    <?php 
                        foreach ($kota['entries'] as $kota_entry){ ?>
                          <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                    <?php 
                        } 
                      }else{
                    ?>
                        <option value=""><?php echo lang('global:select-none') ?></option>
                    <?php
                      }
                    ?>
                  </select>

                  <script type="text/javascript">
                    $('#provinsi').change(function() {
                      var id_provinsi = $(this).val();
                      $("#kota").html('<option value=""><?php echo  lang("laporan:ajax_load_data") ?></option>');
                      
                      $.ajax({
                        url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                        dataType: 'json',
                        success: function(data){
                          if(data.length > 0){
                            $('#kota').html('<option value="">-- Pilih --</option>');
                          }else{
                            $('#kota').html('<option value="">-- Tidak ada --</option>');
                          }
                          $.each(data, function(i, object){
                            $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                          });
                        }
                      });
                    });
                  </script>
                </div>
            </div>
            <?php if ($id_groups == '9') { ?>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" ><?php echo lang('laporan:tipe_pendamping'); ?></label>&nbsp;
              <?php
                $value = null;
                if($this->input->get('f-tipe_unit') != ""){
                  $value = $this->input->get('f-tipe_unit');
                }
              ?>
              <div class="col-sm-2">
                <select name="f-tipe_unit" class="form-control">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <option value="4" <?php echo ($value == 4) ? 'selected' : ''; ?>>Gapoktan 2017</option>
                  <option value="5" <?php echo ($value == 5) ? 'selected' : ''; ?>>TTI 2017</option>
                </select>
              </div>
            </div>
            <?php } ?>
            <div class="form-group">
              <div class="col-sm-2"></div>
              <div class="col-sm-4">
                <button href="<?php echo current_url() . '#'; ?>" class="btn btn-success" type="submit">
                  <i class="icon-ok"></i>
                  Filter
                </button>

                <a href="<?php echo site_url('laporan/pendamping/index'); ?>" class="btn btn-default">
                  <i class="icon-remove"></i>
                  Clear
                </a>
              </div>
            </div>
          <?php echo form_close() ?>
        </div>
      </div>
    </div>
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-table font-red"></i>
            <span class="caption-subject font-red bold uppercase"> Daftar <?php echo $nama_pendamping ?></span>
          </div>
          <div class="actions">
            <?php if ($pendamping['total'] > 0){ ?>
              <a class="btn btn-circle btn-icon-only btn-default" target="blank" href="<?php echo base_url() ?>laporan/pendamping/download/1/<?php echo $id_group ?>?<?php echo $_SERVER['QUERY_STRING'] ?>">
                <i class="fa fa-print"></i> 
              </a>
              <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url() ?>laporan/pendamping/download/0/<?php echo $id_group ?>?<?php echo $_SERVER['QUERY_STRING'] ?>">
                <i class="fa fa-download"></i> 
              </a>
            <?php } ?>
          </div>
        </div>
        <div class="portlet-body">
          <?php if ($pendamping['total'] > 0){ ?>
            <center>
              <h3 style="line-height:1.6em;">
                Pelaksana Program Pengembangan Usaha Pangan Masyarakat (PUPM) <br> melalui kegiatan Toko Tani Indonesia (TTI) <?php echo $get_location ?>
              </h3>
            </center>
            <p class="pull-right"><?php echo lang('laporan:showing').' '.count($pendamping['entries']).' '.lang('laporan:of').' '.$pendamping['total'] ?></p>
            <div class="row" style="clear: both">
              <div class="col-sm-12" style="overflow-y: auto;">
                <?php 
                $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
                if($cur_page != 0){
                  $item_per_page = $pagination_config['per_page'];
                  $no = $cur_page + 1;
                }else{
                  $no = 1;
                }
                ?>
                
                <?php 
                if($id_groups != 9) {
                  foreach ($pendamping['entries'] as $pendamping_entry): 

                    $rows = ($pendamping_entry['total_gap'] + $pendamping_entry['total_tti']) - 1;
                    ?>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th colspan="10">
                            <center>
                              Provinsi <?php echo $pendamping_entry['provinsi'] ?><br>
                              <?php echo $pendamping_entry['kota'] ?>
                            </center>
                          </th>
                        </tr>
                        <tr>
                          <th colspan="2">Pendamping</th>
                          <th colspan="4">Gapoktan</th>
                          <th colspan="4">TTI</th>
                        </tr>
                        <tr>
                          <th>Nama</th>
                          <th>No HP</th>
                          <th>Nama</th>
                          <th>Ketua</th>
                          <th>Alamat</th>
                          <th>No HP</th>
                          <th>Nama</th>
                          <th>Pemilik</th>
                          <th>Alamat</th>
                          <th>No HP</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php 
                          foreach ($pendamping_entry['gapoktan'] as $key2 => $gap) { 
                            $rows2 = count($gap['tti']);
                            $telp = substr($gap['telp'], 0, -4);
                            $hp_ketua = substr($gap['hp_ketua'], 0, -4);
                            ?>
                            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['display_name'] ?></td>
                            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $telp ?>xxxx</td>
                            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['unit_name'] ?></td>
                            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['nama_ketua'] ?></td>
                            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['unit_description'] ?></td>
                            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $hp_ketua; ?>xxxx</td>
                            <?php
                            if(count($gap['tti']) > 0){
                              foreach ($gap['tti'] as $key3 => $tti) { 
                                if($key3 > 0){ ?>
                                  <tr>
                                  <?php
                                }

                                // $hp_ketua = substr($tti['hp_ketua'], 0, -4);
                                $hp_ketua = $tti['hp_ketua'];
                                ?>
                                <td><?php echo $tti['unit_name']; ?></td>
                                <td><?php echo $tti['nama_ketua']; ?></td>
                                <td><?php echo $tti['unit_description']; ?></td>
                                <!-- <td><?php echo $hp_ketua; ?>xxxx</td> -->
                                <td><?php echo $hp_ketua; ?></td>
                                <?php
                                if($key3 > 0 || count($gap['tti']) == 1){ ?>
                                  </tr>
                                  <?php
                                }
                              }
                            }else{ ?>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              </tr>
                              <?php
                            }
                            ?>
                          <?php
                            if($key2 > 0){ ?>
                              </tr>
                              <?php
                            } 
                          } 
                        ?>
                        </tr>
                      </tbody>
                    </table>
                  <?php endforeach; 
                }else{ ?>

                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tipe</th>
                        <th>Provinsi</th>
                        <th>Kota</th>
                        <th>Nama Pendamping</th>
                        <th>No HP Pendamping</th>
                        <th>Nama Unit</th>
                        <th>Ketua</th>
                        <th>Alamat</th>
                        <th>No HP Ketua</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
                        if($cur_page != 0){
                        $item_per_page = $pagination_config['per_page'];
                        $no = (($cur_page -1) * $item_per_page) + 1;
                        }else{
                        $no = 1;
                        }
                        ?>
                    <?php 
                      foreach ($pendamping['entries'] as $pendamping_entry) {
                        $telp = substr($pendamping_entry['telp'], 0, -4);
                        $hp_ketua = substr($pendamping_entry['hp_ketua'], 0, -4);
                      ?>
                      <tr>
                          <td><?php echo $no; $no++; ?></td>
                          <td><?php echo $pendamping_entry['type_name'] ?></td>
                          <td><?php echo $pendamping_entry['provinsi'] ?></td>
                          <td><?php echo $pendamping_entry['kota'] ?></td>
                          <td><?php echo $pendamping_entry['display_name'] ?></td>
                          <td><?php echo $telp ?>xxxx</td>
                          <td><?php echo $pendamping_entry['unit_name'] ?></td>
                          <td><?php echo $pendamping_entry['nama_ketua'] ?></td>
                          <td><?php echo $pendamping_entry['unit_description'] ?></td>
                          <td><?php echo $pendamping_entry['hp_ketua'] ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <?php 
                }
                ?>
              </div>
            </div>
            <?php echo $pendamping['pagination']; ?>
          <?php }else{ ?>
            <?php echo lang('laporan:pendamping:no_entry'); ?>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>