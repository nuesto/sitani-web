<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Page Print</title>
	<style type="text/css" media="print">
    @page 
    {
      /*size: landscape;    auto is the current printer page size */
      /*margin: 0mm;  /* this affects the margin in the printer settings */
      size: auto;   /* auto is the initial value */ 
      margin:7mm;
    }
    body 
    {
   	  margin:0px;
    }
    button{
      display:none;
    }
	</style>

	<style type="text/css">
		body{
      margin: 0px; 
			line-height: 1.4;
    	font-family: calibri;
      padding:25px;
      padding-top: 0px;
		}
		table{
			font-size: 13px;
		}
    th{
      background-color: #CCC;
    }
    td, th{
      padding:3px;
    }
    .table, .table > td, .table > th{
      border:1px solid #ccc;
    }
    .table > thead > th{
      background-color: #CCC;
    }
	</style>
  <script type="text/javascript" src="<?php echo base_url() ?>addons/default/themes/sitoni/assets/global/plugins/jquery.min.js"></script>
</head>
<body onload="printme()">
  <div class="content">
    <center>
      <button onclick="printme()" style="margin-top:20px;">Print</button>
      <?php echo $html ?>
    </center>
  </div>

	<script type="text/javascript"> 
	function printme() { 
	  window.print(); 
	  // document.body.onmousemove = doneyet; 
	} 
	function doneyet() { 
	  document.body.onmousemove = ""; 
	  window.close();
	} 
	</script> 
</body>
</html>