<?php 
$row_id = NULL;
if($mode == 'edit'){
	$row_id = $entry_id;
}
?>
<form id="form_absen" method="post" action="<?php echo base_url(); ?>admin/laporan/absen/edit/<?php echo $row_id ?>">
	<div class="modal fade bs-example-modal-lg" id="myModal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><?php echo lang('laporan:absen:'.$mode); ?></h4>
				</div>
				<div class="modal-body">
					<?php
						$h = $fields['tanggal']; 
						$h_1 = date("Y-m-d", strtotime("-1 day", strtotime($h)));
						$h1 = date("Y-m-d", strtotime("+1 day", strtotime($h)));
						$h2 = date("Y-m-d", strtotime("+2 day", strtotime($h)));
						$h3 = date("Y-m-d", strtotime("+3 day", strtotime($h)));
						$h4 = date("Y-m-d", strtotime("+4 day", strtotime($h)));
					?>
					<div id="form-result">
						
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right"><?php echo lang('laporan:absen:singular'); ?></label>
							<div class="col-sm-5">
								<input type="text" readonly value="<?php echo date_idr($fields['tanggal'], 'd F Y', NULL) ?>" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right"><?php echo lang('laporan:waktu_buka'); ?></label>
							<div class="col-sm-5">
								<select name="tgl_buka" id="tgl_buka2" class="form-control">
									<option value="<?php echo $h_1 ?>" <?php echo ($h_1 == $fields['tgl_buka']) ? 'selected' : ''; ?>><?php echo date_idr($h_1, 'd F Y', NULL) ?></option>
									<option value="<?php echo $h ?>" <?php echo ($h == $fields['tgl_buka']) ? 'selected' : ''; ?>><?php echo date_idr($h, 'd F Y', NULL) ?></option>
									<option value="<?php echo $h1 ?>" <?php echo ($h1 == $fields['tgl_buka']) ? 'selected' : ''; ?>><?php echo date_idr($h1, 'd F Y', NULL) ?></option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right"><?php echo lang('laporan:jam_buka'); ?></label>
							<div class="col-sm-5">
								<div class="bootstrap-timepicker">
									<input name="jam_buka" id="timepicker1" class="jam_buka2" type="text" value="<?php echo $fields['jam_buka'] ?>" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right"><?php echo lang('laporan:waktu_tutup'); ?></label>
							<div class="col-sm-5">
								<select name="tgl_tutup" id="tgl_tutup2" class="form-control">
									<option value="<?php echo $h ?>" <?php echo ($h == $fields['tgl_tutup']) ? 'selected' : ''; ?>><?php echo date_idr($h, 'd F Y', NULL) ?></option>
									<option value="<?php echo $h1 ?>" <?php echo ($h1 == $fields['tgl_tutup']) ? 'selected' : ''; ?>><?php echo date_idr($h1, 'd F Y', NULL) ?></option>
									<option value="<?php echo $h2 ?>" <?php echo ($h2 == $fields['tgl_tutup']) ? 'selected' : ''; ?>><?php echo date_idr($h2, 'd F Y', NULL) ?></option>
									<option value="<?php echo $h3 ?>" <?php echo ($h3 == $fields['tgl_tutup']) ? 'selected' : ''; ?>><?php echo date_idr($h3, 'd F Y', NULL) ?></option>
									<option value="<?php echo $h4 ?>" <?php echo ($h4 == $fields['tgl_tutup']) ? 'selected' : ''; ?>><?php echo date_idr($h4, 'd F Y', NULL) ?></option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label no-padding-right"><?php echo lang('laporan:jam_tutup'); ?></label>
							<div class="col-sm-5">
								<div class="bootstrap-timepicker">
									<input name="jam_tutup" id="timepicker2" class="jam_tutup2" type="text" value="<?php echo $fields['jam_tutup'] ?>"/>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="submit"><?php echo lang('buttons:save'); ?></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	$('#timepicker1, #timepicker2').timepicker({
		minuteStep: 1,
		showSeconds: true,
		showMeridian: false,
		defaultTime: false
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	$('#form_absen').submit(function(e){
		e.preventDefault();
		$(".modal-footer .btn").toggleClass('disabled');
		$(".modal-header .close").attr('disabled','disabled');

		$('#form-result').html('<h3 style="margin-top: 0; margin-bottom: 15px; text-align: center;"><i class="icon-spinner icon-spin orange bigger-125"></i></h3>');
		var jam_buka = $(".jam_buka2").val();
		var jam_tutup = $(".jam_tutup2").val();

		var tgl_buka = $("#tgl_buka2").val();
		var tgl_tutup = $("#tgl_tutup2").val();

		var waktu_buka = tgl_buka+' '+jam_buka;	
		var waktu_tutup = tgl_tutup+' '+jam_tutup;	
		waktu_buka = Date.parse(waktu_buka)/1000;
		waktu_tutup = Date.parse(waktu_tutup)/1000;

		var msg = '';
		if(waktu_tutup <  waktu_buka){
			msg += '<?php echo lang('laporan:absen:time_failure'); ?>';
		}

		if(msg != ''){
			$('#form-result').html('<div class="alert alert-danger">'+msg+'</div>');
			$(".modal-footer .btn").toggleClass('disabled',false);
  		$(".modal-header .close").removeAttr('disabled');
		}else{
			var form = $(this).attr('action');
			var serialData = $(this).serialize();
			$.ajax({
				type: "POST",
				url: form,
				data: serialData,
				success: function(data){
					$('#form-result').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>'+data+'</div>');
			      $(".modal-footer .btn").toggleClass('disabled',false);
		    		$(".modal-header .close").removeAttr('disabled');
		    		setTimeout(function() { $('#myModal').modal('hide'); },1000);
			      $('#myModal').on('hidden.bs.modal', function (e) {
					  	location.reload();
						})		
		    },
			});
		}

	});

</script>