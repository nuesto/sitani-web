<style>
  .loading_unit{
    margin-left: 5px; display: inline-block; padding-top: 4px;
  }
</style>

<div class="page-header">
	<h1><?php echo lang('laporan:daftar_laporan'); ?> (Keuangan)</h1>
  <?php if($this->input->get('f-tipe_laporan')) { ?>
  	<?php if($firstDayOfWeek != NULL) { ?>
      <?php if ($laporan_data['total'] > 0){ ?>
      	<div class="btn-group content-toolbar">
          <a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/data_keuangan/daftar_laporan?page=download&<?php echo $_SERVER['QUERY_STRING'] ?>">
            <i class="fa fa-download"></i>  
            <span class="no-text-shadow"><?php echo lang('laporan:download') ?></span>
          </a>  
        </div>

        <div class="btn-group content-toolbar">
          <a class="btn btn-yellow btn-sm" target="_blank" href="<?php echo base_url() ?>admin/laporan/data_keuangan/daftar_laporan?page=print&<?php echo $_SERVER['QUERY_STRING'] ?>">
            <i class="fa fa-print"></i>  
            <span class="no-text-shadow"><?php echo lang('laporan:print') ?></span>
          </a>  
        </div>
      <?php } ?>
    <?php } ?>
  <?php } ?>
</div>

<?php 
  $link = base_url().'admin/laporan/data_keuangan/daftar_laporan/';
?>

<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get')) ?>
  <div class="form-group">
    <label><?php echo lang('laporan:tipe_laporan'); ?></label>&nbsp;
    <?php
      $value = null;
      if($this->input->get('f-tipe_laporan') != ""){
        $value = $this->input->get('f-tipe_laporan');
      }
    ?>
    <select name="f-tipe_laporan" onchange="this.form.submit()">
      <option value=""><?php echo lang('global:select-pick') ?></option>
      <?php 
        foreach ($tipes as $key => $tipe) { 
          if(in_array($tipe['id'], $type_ids) || group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_prov_laporan')){ ?>
            <option value="<?php echo $tipe['id'] ?>" <?php echo ($value == $tipe['id']) ? 'selected' : ''; ?>><?php echo $tipe['nama_laporan'] ?></option>
            <?php
          }
        } 
      ?>
    </select>
  </div>
  <hr>
<?php echo form_close(); ?>

<?php 
if($this->input->get('f-tipe_laporan')) {  ?>

  <?php echo form_open($link, array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>

  	<input type="hidden" id="tipe_laporan" name="f-tipe_laporan" value="<?php echo $f_tipe_laporan ?>">

    <?php 
    if(group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_prov_laporan')) { ?>

      <!--- show location untuk user yg bisa manambahkan semua laporan -->
        <div class="form-group">
          <label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?></label>

          <div class="col-sm-6">
            <?php
              
              $val_prov = $id_provinsi;
              if(!group_has_role('laporan','view_all_laporan')){ ?>
                <input type="text" value="<?php echo $nama_provinsi; ?>" readonly>
                <input type="hidden"  id="provinsi" value="<?php echo $id_provinsi; ?>" readonly>
                <?php
              }else{
                if($this->input->get('f-provinsi') != NULL){
                  $val_prov = $this->input->get('f-provinsi');
                }
                ?>
                <select name="f-provinsi" id="provinsi" class="col-xs-10 col-sm-5">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                    <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                  <?php } ?>
                </select>
                <?php
              }
            ?>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label no-padding-right" for="kota"><?php echo lang('location:kota:singular'); ?></label>

          <div class="col-sm-6">
            <?php 
              $val_kota = $id_kota;
              if($this->input->get('f-kota') != NULL){
                $val_kota = $this->input->get('f-kota');
              }
            ?>
            <select name="f-kota" id="kota" class="col-xs-10 col-sm-5">
              <?php 
                if(count($kota['entries']) > 0) { ?>
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php 
                  foreach ($kota['entries'] as $kota_entry){ ?>
                    <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($val_kota == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                    <?php 
                  } 
                }else{ ?>
                  <option value=""><?php echo lang('global:select-none') ?></option>
                  <?php
                }
              ?>
            </select>
            <span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>

            <script type="text/javascript">
              $(document).ready(function(){
                <?php if($val_prov == NULL) { ?>
                  $("#provinsi").change();
                <?php } ?>

                <?php if($val_kota == NULL) { ?>
                  $("#kota").change();
                <?php } ?>
              });

              $('#provinsi').change(function() {
                $("#id_unit").html('<option value=""><?php echo  lang("global:select-none") ?></option>');
                var id_provinsi = $(this).val();
                $("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $.ajax({
                  url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                  dataType: 'json',
                  success: function(data){
                    if(data.length > 0){
                      $('#kota').html('<option value="">-- Pilih --</option>');
                    }else{
                      $('#kota').html('<option value="">-- Tidak ada --</option>');
                    }
                    $.each(data, function(i, object){
                      $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                    });
                    $("#kota").change();
                    $('.loading-kota').html('');
                  }
                });
              });

              // khusu sitoni
              $('#kota').change(function() {
                var kota = ($(this).val() == '') ? 0 : $(this).val();
                var tipe_laporan = '<?php echo $this->input->get('f-tipe_laporan') ?>';
                tipe_laporan = (tipe_laporan == 2) ? 1 : tipe_laporan; 
                $("#<?php echo $org_name ?>").html('<option value="-1"><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading<?php echo $org_name ?>').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $("#<?php echo $org_name ?>").load("<?php echo site_url('admin/laporan/data_keuangan/ajax_get_unit_by_kota') ?>" + '/' + kota + '/' + tipe_laporan, function(data) {
                    $("#<?php echo $org_name ?>").change();
                  $('.loading<?php echo $org_name ?>').html('');
                });
              });
            </script>
          </div>
        </div>
      <?php
    }

    // Load Organization--------
     
    $n = 0;
    $addedLevel = array();

    if(isset($organization['id_organization_unit']) && $organization['id_organization_unit'] != -1) { ?>
      <input type="hidden" name="id_organization_unit" value="<?php echo $organization['id_organization_unit'] ?>">
      <?php
    }

    foreach ($types as $key => $type) {
        if (in_array($type['level'], $addedLevel)) {
            continue;
        }
        $addedLevel[] = $type['level'];
        $n++;
        $types2[] = $type;
        if($type['slug'] == $node_type_slug){
            break;
        }
    }
    $n2 = $n;
    
    foreach ($types2 as $i => $type) {

      $parent_field_name = isset($field_name) ? $field_name : null;
      $field_name = $i == $n - 1 ? 'id_organization_unit' : ('id_organization_unit_' . $type['level']);
      ?>
      <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="<?php echo $field_name; ?>"><?php echo $type['name']; ?></label>
        <div class="col-sm-10">
          <?php
          if(isset($organization[$field_name])){
            $value = $organization[$field_name];
          } else {
            $value = $this->input->get($field_name);
          }
          
          $organization[$field_name] = $value != NULL ? $value : -1; 
          $parent_lvl = $type['level'] - 1; 
          
          if(isset($organization['organization_name_'.$type['level']]) && !$is_tti17) { ?>
            <div class="entry-value col-sm-5" id="unit-predefined-text"><?php echo $organization['organization_name_'.$type['level']]; ?></div>
            <?php 
          } else { 
            //if(isset($organization['id_organization_unit_'.$parent_lvl]) && user_units($this->current_user->id) == $organization['id_organization_unit_'.$parent_lvl] || $field_name == 'id_organization_unit_0' || $n2 == 1){ ?>

              <select id="<?php echo $field_name; ?>" name="<?php echo $field_name; ?>" class="col-xs-10 col-sm-5">
                <option value="-1"><?php echo (count($child_organization) > 0) ? lang('global:select-pick') : lang('global:select-none'); ?></option>
                <?php        
                foreach ($child_organization as $child) { 
                  echo '<option value="' . $child['id'] . '"> ' . $child['unit_name'] . '</option>';
                }
                ?>  
              </select>

              <script>
                $( document ).ready(function() {
                  $("#<?php echo $field_name; ?>").val(<?php echo $organization[$field_name]; ?>).change();
                });
              </script>

              <?php 
          }

          if ($parent_field_name) { ?>
            <script type="text/javascript">
              $('#<?php echo $parent_field_name; ?>').change(function() {
                var parent_id = $(this).val();
                $("#<?php echo $field_name; ?>").html('<option><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading<?php echo $field_name ?>').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $("#<?php echo $field_name; ?>").load("<?php echo site_url('laporan/data_keuangan/ajax_unit_dropdown') ?>" + '/' + parent_id, function(data) {
                  if(data != '<option value="-1">-- Tidak ada --</option>'){
                      $(this).val(<?php echo $organization[$field_name]; ?>).change();
                  }
                  $('.loading<?php echo $field_name ?>').html('');
                });
              });
            </script>
            <?php 
          } 
          ?>
          <span class="loading_unit loading<?php echo $field_name ?>"></span>
        </div>
      </div>
      <?php
    }
    ?>

    <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:periode'); ?></label>
      <div class="col-sm-10">
        <?php
          $value = $tahun;
          if($this->input->get('f-tahun') != ""){
            $value = $this->input->get('f-tahun');
          }
        ?>
        <select name="f-tahun" id="f-tahun">
          <!-- <option value="">-- <?php echo lang('laporan:tahun') ?> --</option> -->
          <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
            <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
          <?php } ?>
        </select>
        
        &nbsp;
        <?php
          $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
          $value = $bulan;
          if($this->input->get('f-bulan') != ""){
            $value = $this->input->get('f-bulan');
          }
        ?>
        <select name="f-bulan" id="f-bulan">
          <!-- <option value="">-- <?php echo lang('laporan:bulan') ?> --</option> -->
          <?php foreach ($arr_month as $key => $month) { ?>
            <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="kota"><?php echo lang('laporan:channel'); ?></label>
      <div class="col-sm-2">
        <?php 
          $value = NULL;
          if($this->input->get('f-channel') != NULL){
            $value = $this->input->get('f-channel');
          }
        ?>
        <select name="f-channel" class="form-control">
          <option value=""><?php echo lang('global:select-pick') ?></option>
          <?php 
            $arr_ch = array('web'=>'Web','sms'=>'SMS','android'=>'Android'); 
            foreach ($arr_ch as $key => $ch) { ?>
              <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $ch ?></option>
              <?php
            }
          ?>
        </select>
      </div>
    </div>

  	<div class="form-group">
      <div class="col-sm-2"></div>
      <div class="col-sm-10">
        <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
          <i class="icon-ok"></i>
          <?php echo lang('buttons:filter'); ?>
        </button>

        <a href="<?php echo site_url('admin/laporan/data_keuangan/daftar_laporan?f-tipe_laporan='.$this->input->get('f-tipe_laporan')); ?>" class="btn btn-xs">
          <i class="icon-remove"></i>
          <?php echo lang('buttons:clear'); ?>
        </a>
      </div>
    </div>

  <?php echo form_close() ?>

  <?php if($firstDayOfWeek != NULL) { ?>
    <?php if ($laporan_data['total'] > 0): ?>
    	
    	<p class="pull-right"><?php echo lang('laporan:showing').' '.count($laporan_data['entries']).' '.lang('laporan:of').' '.$laporan_data['total'] ?></p>
    	<div class="table-responsive">
    	<table class="table table-striped table-bordered table-hover">
    		<thead>
    			<tr>
    				<th>No</th>
    				<th><?php echo lang('laporan:pengirim'); ?></th>
    				<th><?php echo lang('location:provinsi:singular'); ?></th>
            <th><?php echo lang('location:kota:singular'); ?></th>
    				<th><?php echo lang('laporan:nama_unit') ?></th>
            <th><?php echo lang('laporan:channel'); ?></th>
            <th><?php echo lang('laporan:bulan'); ?></th>
    				<th><?php echo lang('laporan:tanggal'); ?></th>
            <th>Action</th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php 
    			$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
    			if($cur_page != 0){
    				$item_per_page = $pagination_config['per_page'];
    				$no = $cur_page + 1;
    			}else{
    				$no = 1;
    			}

          $arr_enum = array('Produsen'=>1,'Pedagang Grosir'=>2, 'Pedagang Eceran'=>3);
    			?>
    			
    			<?php foreach ($laporan_data['entries'] as $laporan_data_entry): ?>
    			<tr>
    				<td><?php echo $no; $no++; ?></td>
    				<td><?php echo $laporan_data_entry['display_name']; ?></td>
    				<td><?php echo $laporan_data_entry['provinsi']; ?></td>
            <td><?php echo $laporan_data_entry['kota']; ?></td>
            <td><?php echo $laporan_data_entry['unit_name']; ?></td>
            <td><?php echo $laporan_data_entry['channel']; ?></td>
    				<td><?php echo $laporan_data_entry['bulan']; ?></td>
    				<td><?php echo date_idr($laporan_data_entry['created_on'], 'd F Y', null); ?></td>
            <td>
              <?php
              $tgl_or_id_absen = $laporan_data_entry['id_absen']; 
              if(group_has_role('laporan', 'edit_all_laporan') || group_has_role('laporan','edit_own_prov_laporan')){
                echo anchor('admin/laporan/data_keuangan/edit/'.$laporan_data_entry['id_unit'].'/'.$laporan_data_entry['id_user'].'/'.$tgl_or_id_absen.'/'.$laporan_data_entry['id_laporan'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
              }elseif(group_has_role('laporan', 'edit_own_laporan') && !group_has_role('laporan', 'edit_own_unit_laporan')){
                if($laporan_data_entry['id_user'] == $this->current_user->id){
                  if($tglOrIdAbsen == $tgl_or_id_absen){
                    echo anchor('admin/laporan/data_keuangan/edit/'.$laporan_data_entry['id_unit'].'/'.$laporan_data_entry['id_user'].'/'.$tgl_or_id_absen.'/'.$laporan_data_entry['id_laporan'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
                  }
                }
              }

              if(group_has_role('laporan', 'edit_own_unit_laporan')){
                if(user_cek_entries($this->current_user->id, $laporan_data_entry['id_unit']) == TRUE) {
                  if($tglOrIdAbsen == $tgl_or_id_absen){  
                    echo anchor('admin/laporan/data_keuangan/edit/'.$laporan_data_entry['id_unit'].'/'.$laporan_data_entry['id_user'].'/'.$tgl_or_id_absen.'/'.$laporan_data_entry['id_laporan'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
                  }
                }
              }
              echo ' '.anchor('admin/laporan/data_keuangan/delete/'.$laporan_data_entry['id_unit'].'/'.$laporan_data_entry['id_user'].'/'.$tgl_or_id_absen.'/'.$laporan_data_entry['id_laporan'].$uri, lang('global:delete'), 'class="btn btn-xs btn-info danger confirm"');
              ?>

            </td>
    			</tr>
    			<?php endforeach; ?>
    		</tbody>
    	</table>
    	</div>
    	<?php echo $laporan_data['pagination']; ?>
    	
    <?php else: ?>
      <div class="well"><?php echo lang('laporan:data:no_entry'); ?></div>
    <?php endif;?>
  <?php }else{ ?>
  	<div class="well"><?php echo lang('laporan:data:no_entry'); ?></div>
  <?php } ?>

<?php } ?>