<div class="page-header">
	<h1><?php echo lang('laporan:metadata:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tipe"><?php echo lang('laporan:tipe_laporan'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = ($this->input->get('f-tipe_laporan')) ? $this->input->get('f-tipe_laporan') : NULL;
				if($this->input->post('id_laporan_tipe') != NULL){
					$value = $this->input->post('id_laporan_tipe');
				}elseif($mode == 'edit'){
					$value = $fields['id_laporan_tipe'];
				}

				$val_tipe_laporan = $value;
			?>
			<select name="id_laporan_tipe"  class="col-xs-10 col-sm-2" id="tipe_laporan" >
    		<option value=""><?php echo lang('global:select-pick') ?></option>
    		<?php foreach ($tipes as $key => $tipe) { 
    			if($tipe['nama_laporan'] == 'Gapoktan 2016'){
    				$tipe['nama_laporan'] = 'Gapoktan';
    			} 
    			if($tipe['nama_laporan'] == 'TTI 2016'){
    				$tipe['nama_laporan'] = 'TTI';
    			}
    			?>
    			<option value="<?php echo $tipe['id'] ?>" <?php echo ($value == $tipe['id']) ? 'selected' : ''; ?>><?php echo $tipe['nama_laporan'] ?></option>
    		<?php } ?>
    	</select>
		</div>
	</div>

	<div class="form-group" id="content-komoditas" <?php echo($val_tipe_laporan != 1 && $val_tipe_laporan != 2) ? 'style="display:none;"' : ''; ?> >
		<label class="col-sm-2 control-label no-padding-right" for="tipe"><?php echo lang('laporan:komoditas'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = ($this->input->get('f-komoditas')) ? $this->input->get('f-komoditas') : NULL;
				if($this->input->post('id_laporan_komoditas') != NULL){
					$value = $this->input->post('id_laporan_komoditas');
				}elseif($mode == 'edit'){
					$value = $fields['id_laporan_komoditas'];
				}
			?>
			<select name="id_laporan_komoditas"  class="col-xs-10 col-sm-2" id="" >
    		<option value=""><?php echo lang('global:select-pick') ?></option>
    		<?php foreach ($komoditas as $key => $komoditas_entry) { ?>
    			<option value="<?php echo $komoditas_entry['id'] ?>" <?php echo ($value == $komoditas_entry['id']) ? 'selected' : ''; ?>><?php echo $komoditas_entry['nama_komoditas'] ?></option>
    		<?php } ?>
    	</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_tipe_field"><?php echo lang('laporan:tipe_field'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = ($this->input->get('f-tipe_laporan_field')) ? $this->input->get('f-tipe_laporan_field') : NULL;;
				if($this->input->post('id_laporan_tipe_field') != NULL){
					$value = $this->input->post('id_laporan_tipe_field');
				}elseif($mode == 'edit'){
					$value = $fields['id_laporan_tipe_field'];
				}
			?>
			<select name="id_laporan_tipe_field"  class="col-xs-10 col-sm-2" id="" >
    		<option value=""><?php echo lang('global:select-pick') ?></option>
    		<?php foreach ($tipe_field as $key => $field) { ?>
    			<option value="<?php echo $field['id'] ?>" <?php echo ($value == $field['id']) ? 'selected' : ''; ?>><?php echo $field['nama_tipe_field'] ?></option>
    		<?php } ?>
    	</select>

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="field"><?php echo lang('laporan:field'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = ($this->input->get('f-field')) ? $this->input->get('f-field') : NULL;;
				if($this->input->post('field') != NULL){
					$value = $this->input->post('field');
				}elseif($mode == 'edit'){
					$value = $fields['field'];
				}
			?>
			<input name="field" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="field" />

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="slug"><?php echo lang('laporan:slug'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('slug') != NULL){
					$value = $this->input->post('slug');
				}elseif($mode == 'edit'){
					$value = $fields['slug'];
				}
			?>
			<input name="slug" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="slug"/>
			<script type="text/javascript">
        $('#field').slugify({slug: '#slug', type: '-'});
    	</script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama"><?php echo lang('laporan:nama'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = ($this->input->get('f-nama')) ? $this->input->get('f-nama') : NULL;;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="nama" />

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="satuan"><?php echo lang('laporan:satuan'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('satuan') != NULL){
					$value = $this->input->post('satuan');
				}elseif($mode == 'edit'){
					$value = $fields['satuan'];
				}
			?>
			<input name="satuan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="" />

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nilai_minimal_home"><?php echo lang('laporan:nilai_minimal_home'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nilai_minimal_home') != NULL){
					$value = $this->input->post('nilai_minimal_home');
				}elseif($mode == 'edit'){
					$value = $fields['nilai_minimal_home'];
				}
			?>
			<input name="nilai_minimal_home" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="nilai_minimal_home" onkeypress="return isNumberKey(event)">

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nilai_maksimal_home"><?php echo lang('laporan:nilai_maksimal_home'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nilai_maksimal_home') != NULL){
					$value = $this->input->post('nilai_maksimal_home');
				}elseif($mode == 'edit'){
					$value = $fields['nilai_maksimal_home'];
				}
			?>
			<input name="nilai_maksimal_home" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="nilai_maksimal_home" onkeypress="return isNumberKey(event)">

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nilai_minimal"><?php echo lang('laporan:nilai_minimal'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nilai_minimal') != NULL){
					$value = $this->input->post('nilai_minimal');
				}elseif($mode == 'edit'){
					$value = $fields['nilai_minimal'];
				}
			?>
			<input name="nilai_minimal" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="nilai_minimal" onkeypress="return isNumberKey(event)">

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nilai_maksimal"><?php echo lang('laporan:nilai_maksimal'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nilai_maksimal') != NULL){
					$value = $this->input->post('nilai_maksimal');
				}elseif($mode == 'edit'){
					$value = $fields['nilai_maksimal'];
				}
			?>
			<input name="nilai_maksimal" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="nilai_maksimal" onkeypress="return isNumberKey(event)"/>

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nilai_maksimal"><?php echo lang('laporan:urutan'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = $total;
				if($this->input->post('urutan') != NULL){
					$value = $this->input->post('urutan');
				}elseif($mode == 'edit'){
					$value = $fields['urutan'];
				}
			?>
			<select name="urutan" id="urutan" >
				<?php for($i=0;$i<$total;$i++) { ?>
					<option <?php echo ($i == $value) ? 'selected' : ''; ?>><?php echo $i ?></option>
				<?php } ?>
    	</select>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">
	$("#tipe_laporan").on('change', function(event){
		event.preventDefault();
		var val = $(this).val();
		if(val != 1 && val != 2){
			$("#content-komoditas").hide();
		}else{
			$("#content-komoditas").show();
		}
	})

	$('#slug').keyup(function (e) {
    var allowedChars = /^[a-z\d -]+$/i;
    var str = String.fromCharCode(e.charCode || e.which);

    var forbiddenChars = /[^a-z\d -]/gi;
    if (forbiddenChars.test(this.value)) {
        this.value = this.value.replace(forbiddenChars, '');
    }

    if (allowedChars.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
	});

	function cek_nilai(){
		var nilai_min = $("#nilai_minimal").val();
		var nilai_max = $("#nilai_maksimal").val();
		if(nilai_min!="" && nilai_max!=""){
			if(nilai_min > nilai_max){
				bootbox.alert("Nilai Minimal harus lebih kecil dari Nilai Maksimal", function(result) {});
				$("#nilai_minimal, #nilai_maksimal").val("");
			}
		}
	}

	function isNumberKey(evt)
  {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

</script>