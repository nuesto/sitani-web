<style>
  .loading_unit{
    margin-left: 5px; display: inline-block; padding-top: 4px;
  }
</style>
<div class="page-header">
  <h1><?php echo lang('laporan:data:plural'); ?> (Kinerja Pendamping)</h1>

  <?php 
  if($this->input->get('f-tipe_laporan')) {
    $show_btn_print = false;
    if($this->input->get('f-is_sandingan')){
      if($data['total'] > 0 && $data2['total'] > 0){
        $show_btn_print = true;
      }
    }else{
      if($data['total'] > 0){
        $show_btn_print = true;
      }
    }
    ?>

    <div class="btn-group content-toolbar">
      <a class="btn btn-yellow btn-sm" target="blank" href="<?php echo base_url() ?>admin/laporan/data_kinerja/sandingkan_laporan?<?php echo $_SERVER['QUERY_STRING'] ?>">
        <i class="fa fa-list-alt"></i>
        <span class="no-text-shadow"><?php echo lang('laporan:sandingkan_laporan') ?></span>
      </a>
    </div>
    <?php
    if ($show_btn_print){ ?>
      <div class="btn-group content-toolbar">
        <a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/data_kinerja/index?page=download&tipe_rekap=keuangan&<?php echo $_SERVER['QUERY_STRING'] ?>">
          <i class="fa fa-download"></i>
          <span class="no-text-shadow"><?php echo lang('laporan:download') ?></span>
        </a>
      </div>
      <div class="btn-group content-toolbar">
        <a class="btn btn-yellow btn-sm" target="blank" href="<?php echo base_url() ?>admin/laporan/data_kinerja/index?page=print&tipe_rekap=keuangan&<?php echo $_SERVER['QUERY_STRING'] ?>">
          <i class="fa fa-print"></i>
          <span class="no-text-shadow"><?php echo lang('laporan:print') ?></span>
        </a>
      </div>
      <?php 
    } 
    
    if(group_has_role('laporan', 'create_laporan') || group_has_role('laporan', 'create_own_laporan') || group_has_role('laporan','create_own_prov_laporan')){ ?>
      <div class="btn-group content-toolbar">
        <a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/data_kinerja/create<?php echo $uri ?>">
          <i class="icon-plus"></i> 
          <span class="no-text-shadow"><?php echo lang('laporan:data:new'); ?></span>
        </a>
      </div>
      <?php 
    }
  } 
  ?>
</div>

<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get')) ?>
  <div class="form-group">
    <label><?php echo lang('laporan:tipe_laporan'); ?></label>&nbsp;
    <?php
      $value = null;
      if($this->input->get('f-tipe_laporan') != ""){
        $value = $this->input->get('f-tipe_laporan');
      }
    ?>
    <select name="f-tipe_laporan" onchange="this.form.submit()">
      <option value=""><?php echo lang('global:select-pick') ?></option>
      <?php 
        foreach ($tipes as $key => $tipe) { 
          if(in_array($tipe['id'], $type_ids) || group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_prov_laporan')){ ?>
            <option value="<?php echo $tipe['id'] ?>" <?php echo ($value == $tipe['id']) ? 'selected' : ''; ?>><?php echo $tipe['nama_laporan'] ?></option>
            <?php
          }
        } 
      ?>
    </select>
  </div>
  <hr>
<?php echo form_close(); ?>

<?php 
if($this->input->get('f-tipe_laporan')) {  ?>

  <?php echo form_open(base_url().'admin/laporan/data_kinerja/index', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>

    <input type="hidden" id="tipe_laporan" name="f-tipe_laporan" value="<?php echo $f_tipe_laporan ?>">

    <?php 
    if(group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_prov_laporan')) { ?>

      <!--- show location untuk user yg bisa manambahkan semua laporan -->
        <div class="form-group">
          <label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?></label>

          <div class="col-sm-6">
            <?php 
               $val_prov = $id_provinsi;
              if(!group_has_role('laporan','view_all_laporan')){ ?>
                <input type="text" value="<?php echo $nama_provinsi; ?>" readonly>
                <input type="hidden"  id="provinsi" value="<?php echo $id_provinsi; ?>" readonly>
                <?php
              }else{
                if($this->input->get('f-provinsi') != NULL){
                  $val_prov = $this->input->get('f-provinsi');
                }
                ?>
                <select name="f-provinsi" id="provinsi" class="col-xs-10 col-sm-5">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                    <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                  <?php } ?>
                </select>
                <?php
              }
            ?>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label no-padding-right" for="kota"><?php echo lang('location:kota:singular'); ?></label>

          <div class="col-sm-6">
            <?php 
              $val_kota = $id_kota;
              if($this->input->get('f-kota') != NULL){
                $val_kota = $this->input->get('f-kota');
              }
            ?>
            <select name="f-kota" id="kota" class="col-xs-10 col-sm-5">
              <?php 
                if(count($kota['entries']) > 0) { ?>
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php 
                  foreach ($kota['entries'] as $kota_entry){ ?>
                    <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($val_kota == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                    <?php 
                  } 
                }else{ ?>
                  <option value=""><?php echo lang('global:select-none') ?></option>
                  <?php
                }
              ?>
            </select>
            <span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>

            <script type="text/javascript">
              $(document).ready(function(){
                <?php if($val_prov == NULL) { ?>
                  $("#provinsi").change();
                <?php } ?>

                <?php if($val_kota == NULL) { ?>
                  $("#kota").change();
                <?php } ?>
              });

              $('#provinsi').change(function() {
                $("#id_unit").html('<option value=""><?php echo  lang("global:select-none") ?></option>');
                var id_provinsi = $(this).val();
                $("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $.ajax({
                  url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                  dataType: 'json',
                  success: function(data){
                    if(data.length > 0){
                      $('#kota').html('<option value="">-- Pilih --</option>');
                    }else{
                      $('#kota').html('<option value="">-- Tidak ada --</option>');
                    }
                    $.each(data, function(i, object){
                      $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                    });
                    $("#kota").change();
                    $('.loading-kota').html('');
                  }
                });
              });

              // khusu sitoni
              $('#kota').change(function() {
                var kota = ($(this).val() == '') ? 0 : $(this).val();
                var tipe_laporan = '<?php echo $this->input->get('f-tipe_laporan') ?>';
                tipe_laporan = (tipe_laporan == 2) ? 1 : tipe_laporan; 
                //tipe_laporan = (tipe_laporan == 7) ? 6 : tipe_laporan;
                $("#<?php echo $org_name ?>").html('<option value="-1"><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading<?php echo $org_name ?>').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $("#<?php echo $org_name ?>").load("<?php echo site_url('admin/laporan/data_kinerja/ajax_get_unit_by_kota') ?>" + '/' + kota + '/' + tipe_laporan, function(data) {
                    $("#<?php echo $org_name ?>").change();
                  $('.loading<?php echo $org_name ?>').html('');
                });
              });
            </script>
          </div>
        </div>
      <?php
    }

    // Load Organization--------
     
    $n = 0;
    $addedLevel = array();

    if(isset($organization['id_organization_unit']) && $organization['id_organization_unit'] != -1) { ?>
      <input type="hidden" name="id_organization_unit" value="<?php echo $organization['id_organization_unit'] ?>">
      <?php
    }

    foreach ($types as $key => $type) {
        if (in_array($type['level'], $addedLevel)) {
            continue;
        }
        $addedLevel[] = $type['level'];
        $n++;
        $types2[] = $type;
        if($type['slug'] == $node_type_slug){
            break;
        }
    }
    $n2 = $n;
    
    foreach ($types2 as $i => $type) {

      $parent_field_name = isset($field_name) ? $field_name : null;
      $field_name = $i == $n - 1 ? 'id_organization_unit' : ('id_organization_unit_' . $type['level']);
      ?>
      <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="<?php echo $field_name; ?>"><?php echo $type['name']; ?></label>
        <div class="col-sm-10">
          <?php
          if(isset($organization[$field_name])){
            $value = $organization[$field_name];
          } else {
            $value = $this->input->get($field_name);
          }
          
          $organization[$field_name] = $value != NULL ? $value : -1; 
          $parent_lvl = $type['level'] - 1; 
          
          if(isset($organization['organization_name_'.$type['level']]) && !$is_tti17) { ?>
            <div class="entry-value col-sm-5" id="unit-predefined-text"><?php echo $organization['organization_name_'.$type['level']]; ?></div>
            <?php 
          } else { 
            // if(isset($organization['id_organization_unit_'.$parent_lvl]) && user_units($this->current_user->id) == $organization['id_organization_unit_'.$parent_lvl] || $field_name == 'id_organization_unit_0' || $n2 == 1){ ?>

              <select id="<?php echo $field_name; ?>" name="<?php echo $field_name; ?>" class="col-xs-10 col-sm-5">
                <option value="-1"><?php echo (count($child_organization) > 0) ? lang('global:select-pick') : lang('global:select-none'); ?></option>
                <?php        
                foreach ($child_organization as $child) { 
                  echo '<option value="' . $child['id'] . '"> ' . $child['unit_name'] . '</option>';
                }
                ?>  
              </select>

              <script>
                $( document ).ready(function() {
                  $("#<?php echo $field_name; ?>").val(<?php echo $organization[$field_name]; ?>).change();
                });
              </script>

              <?php 
          }

          if ($parent_field_name) { ?>
            <script type="text/javascript">
              $('#<?php echo $parent_field_name; ?>').change(function() {
                var parent_id = $(this).val();
                $("#<?php echo $field_name; ?>").html('<option><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading<?php echo $field_name ?>').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $("#<?php echo $field_name; ?>").load("<?php echo site_url('laporan/data/ajax_unit_dropdown') ?>" + '/' + parent_id, function(data) {
                  if(data != '<option value="-1">-- Tidak ada --</option>'){
                      $(this).val(<?php echo $organization[$field_name]; ?>).change();
                  }
                  $('.loading<?php echo $field_name ?>').html('');
                });
              });
            </script>
            <?php 
          } 
          ?>
          <span class="loading_unit loading<?php echo $field_name ?>"></span>
        </div>
      </div>
      <?php
    }
    ?> 
    <!-- <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="value"><?php echo lang('laporan:komoditas') ?></label> 
      <div class="col-sm-10">
        <?php
          $value = null;
          if($this->input->get('f-komoditas') != ""){
            $value = $this->input->get('f-komoditas');
          }
        ?>
        <select name="f-komoditas" class="col-xs-10 col-sm-5">
          <?php foreach ($komoditas_related as $key => $komoditas_entry) { ?>
            <option value="<?php echo $komoditas_entry['id'] ?>" <?php echo ($value == $komoditas_entry['id']) ? 'selected' : ''; ?>><?php echo $komoditas_entry['nama_komoditas'] ?></option>
          <?php } ?>
        </select>
      </div>
    </div> -->
    <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:periode'); ?></label>
      <div class="col-sm-10">
        <?php
          $value = $tahun;
          if($this->input->get('f-tahun') != ""){
            $value = $this->input->get('f-tahun');
          }
        ?>
        <select name="f-tahun" id="f-tahun">
          <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
            <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
          <?php } ?>
        </select>
        
        &nbsp;
        <?php
          $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
          $value = $bulan;
          if(isset($_GET['f-bulan'])){
            $value = $this->input->get('f-bulan');
          }
        ?>
        <select name="f-bulan" id="f-bulan">
          <option value=""><?php echo lang('global:select-pick') ?></option>
          <?php foreach ($arr_month as $key => $month) { ?>
            <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
          <?php } ?>
        </select>

        &nbsp;
        <?php
          $value = $minggu_ke;
          if(isset($_GET['f-minggu_ke'])){
            $value = $this->input->get('f-minggu_ke');
          }
        ?>

        <select name="f-minggu_ke" id="f-minggu_ke">
          <option value=""><?php echo lang('global:select-pick') ?></option>
          <?php
            $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
            for ($i=1;$i<=$count_of_week;$i++) { ?>
              <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
          <?php } ?>
        </select>

        <script type="text/javascript">
          $('#f-tahun, #f-bulan').change(function() {
            var thn = $('#f-tahun').val();
            var bln = $('#f-bulan').val();
            $("#f-minggu_ke").html('<option value="">...</option>');
            $("#f-minggu_ke").load("<?php echo site_url('laporan/data/ajax_get_minggu_ke') ?>" + '/' + thn + '/' + bln, function(data) {
              $('.loading-minggu_ke').html('');
              $("#f-minggu_ke").change();
            });
          });
        </script>

        <!-- &nbsp;
        <?php
          $value = NULL;
          if($this->input->get('f-hari') != ""){
            $value = $this->input->get('f-hari');
          }
        ?>
        <select name="f-hari" id="f-hari">
          <?php if(count($days) > 0) { ?>
            <option value="">- Hari -</option>
            <?php
              foreach($days as $day){ ?>
                <option value="<?php echo $day ?>" <?php echo ($value == $day) ? 'selected' : ''; ?>><?php echo date_idr($day, 'l, d F Y', null) ?></option>
                <?php 
              }
            }else{ ?>
              <option value="">-- Tidak ada --</option>
            <?php
            } 
          ?>
        </select>

        <script type="text/javascript">
          $('#f-minggu_ke').change(function() {
            var thn = $('#f-tahun').val();
            var bln = $('#f-bulan').val();
            var minggu_ke = $('#f-minggu_ke').val();
            var tipe_laporan = $('#tipe_laporan').val();
            $("#f-hari").html('<option value="">...</option>');
            $('.loading-hari').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
            $("#f-hari").load("<?php echo site_url('laporan/data/ajax_get_hari') ?>" + '/' + thn + '/' + bln + '/' +minggu_ke+ '/' +tipe_laporan, function(data) {
              $('.loading-hari').html('');
            });
          });
        </script>
        <span class="loading-hari" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span> -->
      </div>
    </div>

    <!-- Sandingkan dengan periode -->
    <div class="form-group" <?php echo ($this->input->get('f-is_sandingan')) ? '' : 'style="display:none;"'; ?> id="content-sandingan">
      <label class="col-sm-2 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:sanding_periode'); ?></label>
      <div class="col-sm-10">
        <?php
          $value = $tahun2;
          if($this->input->get('f-tahun2') != ""){
            $value = $this->input->get('f-tahun2');
          }
        ?>
        <select name="f-tahun2" id="f-tahun2">
          <!-- <option value="">-- <?php echo lang('laporan:tahun') ?> --</option> -->
          <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
            <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
          <?php } ?>
        </select>
        
        &nbsp;
        <?php
          $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
          $value = $bulan2;
          if(isset($_GET['f-bulan'])){
            $value = $this->input->get('f-bulan2');
          }
        ?>
        <select name="f-bulan2" id="f-bulan2">
          <option value=""><?php echo lang('global:select-pick') ?></option>
          <?php foreach ($arr_month as $key => $month) { ?>
            <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
          <?php } ?>
        </select>

        &nbsp;
        <?php
          $value = $minggu_ke2;
          if(isset($_GET['f-minggu_ke2'])){
            $value = $this->input->get('f-minggu_ke2');
          }
        ?>

        <select name="f-minggu_ke2" id="f-minggu_ke2">
          <option value=""><?php echo lang('global:select-pick') ?></option>
          <?php
            $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
            for ($i=1;$i<=$count_of_week2;$i++) { ?>
              <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
          <?php } ?>
        </select>

        <script type="text/javascript">
          $('#f-tahun2, #f-bulan2').change(function() {
            var thn2 = $('#f-tahun2').val();
            var bln2 = $('#f-bulan2').val();
            $("#f-minggu_ke2").html('<option value="">...</option>');
            $("#f-minggu_ke2").load("<?php echo site_url('laporan/data/ajax_get_minggu_ke') ?>" + '/' + thn2 + '/' + bln2, function(data) {
              $('.loading-minggu_ke2').html('');
              $("#f-minggu_ke2").change();
            });
          });
        </script>

        <!-- &nbsp;
        <?php
          $value = NULL;
          if($this->input->get('f-hari2') != ""){
            $value = $this->input->get('f-hari2');
          }
        ?>
        <select name="f-hari2" id="f-hari2">
          <?php 
          if(count($days2) > 0) { ?>
            <option value="">- Hari -</option>
            <?php
            foreach($days2 as $day){ ?>
              <option value="<?php echo $day ?>" <?php echo ($value == $day) ? 'selected' : ''; ?>><?php echo date_idr($day, 'l, d F Y', null) ?></option>
              <?php 
            }
          }else{ ?>
            <option value="">-- Tidak ada --</option>
          <?php
          } 
          ?>
        </select>

        <script type="text/javascript">
          $('#f-minggu_ke2').change(function() {
            var thn = $('#f-tahun2').val();
            var bln = $('#f-bulan2').val();
            var minggu_ke = $('#f-minggu_ke2').val();
            var tipe_laporan = $('#tipe_laporan').val();
            $("#f-hari2").html('<option value="">...</option>');
            $('.loading-hari2').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
            $("#f-hari2").load("<?php echo site_url('laporan/data/ajax_get_hari') ?>" + '/' + thn + '/' + bln + '/' +minggu_ke+ '/' +tipe_laporan, function(data) {
              $('.loading-hari2').html('');
            });
          });
        </script>
        <span class="loading-hari2" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span> -->
      </div>
    </div>


    <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="f-is_sandingan"></label>
      <div class="col-sm-10">
        <div class="checkbox" style="padding-top: 0px;">
          <label>
            <input name="f-is_sandingan" <?php echo ($this->input->get('f-is_sandingan')) ? 'checked' : ''; ?> class="ace" id="is_sandingan" type="checkbox">
            <span class="lbl"> Sandingkan antar periode</span>
          </label>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $('#is_sandingan').click(function() {
        $("#content-sandingan").toggle(this.checked);
        // if($(this).is(':checked')){
        //   $("#provinsi, #kota").prop('required','required');
        // }else{
        //   $("#provinsi, #kota").removeAttr('required');
        // }
      })
    </script>
    <div class="form-group">
      <div class="col-sm-2"></div>
      <div class="col-sm-10">
        <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
          <i class="icon-ok"></i>
          <?php echo lang('buttons:filter'); ?>
        </button>

        <a href="<?php echo site_url('admin/laporan/data_kinerja/index?f-tipe_laporan='.$f_tipe_laporan); ?>" class="btn btn-xs">
          <i class="icon-remove"></i>
          <?php echo lang('buttons:clear'); ?>
        </a>
      </div>
    </div>

  <?php echo form_close() ?>

  <?php if ($data['total'] > 0): ?>
    <?php if($this->input->get('f-is_sandingan')) { ?>
    <br>
    <br>
    <p class="pull-left"><?php echo $text_periode1 ?></p>
    <?php } ?>
    <p class="pull-right"><?php echo lang('laporan:showing').' '.count($data['entries']).' '.lang('laporan:of').' '.$data['total'] ?></p>
    
    <div class="table-responsive" style="width: 100%; overflow: auto;">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Provinsi</th>
            <th rowspan="2">Kota</th>
            <th rowspan="2">Nama Unit</th>
            <th rowspan="2">Petugas</th>
            <?php foreach ($komoditas as $key => $metadata) { ?>
              <th rowspan="2"><?php echo $metadata['nama'] ?></th>
            <?php } ?>

            <th colspan="<?php echo count($metadata_harga); ?>" style="text-align:center;">Rata-Rata Satu Tahun Per Pendamping di Kota/Kab</th>
            <?php if(count($metadata_volume) > 0) { ?> 
              <th colspan="<?php echo count($metadata_volume); ?>" style="text-align:center;">Total Satu Tahun Per Pendamping di Kota/Kab</th>
            <?php } ?>
          </tr>
          <tr>
          <?php 
            foreach ($metadata_harga as $field) { 
              $explode = explode('_',$field);
              $nama = $explode[0]; ?>
              
              <th><?php echo $nama ?></th>
              <?php 
            } 
          ?>

          <?php 
            foreach ($metadata_volume as $field) { 
              $explode = explode('_',$field);
              $nama = $explode[0]; ?>
              
              <th><?php echo $nama ?></th>
              <?php 
            } 
          ?>
        </tr>
        </thead>
        <tbody>
          <?php 
          $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
          if($cur_page != 0){
            $no = $cur_page + 1;
          }else{
            $no = 1;
          }
          ?>
          
          <?php
          foreach ($data['entries'] as $key => $entries) { ?>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $entries['provinsi'] ?></td>
              <td><?php echo $entries['kota'] ?></td>
              <td><?php echo $entries['unit_name'] ?></td>
              <td><?php echo $entries['display_name'] ?></td>
              <?php 
                foreach ($komoditas as $key => $metadata) { 
                  $key = $key+1;
                  if(isset($entries[$metadata['field']])){
                    $entries_metadata = number_format($entries[$metadata['field']],0,',','.');
                  }else{
                    $entries_metadata = 0;
                  }
                  ?>
                  <td style="text-align:right;"><?php echo $entries_metadata ?></td>
                  <?php 
                } 
              ?>

              <?php 
              foreach ($metadata_harga as $field) { 
                if(isset($entries['avgthnan_'.$field])){
                  $rata2tahunan = number_format($entries['avgthnan_'.$field],0,',','.'); 
                }else{
                  $rata2tahunan = 0;
                }

                ?>
                  
                <td style="text-align:right;"><?php echo ($rata2tahunan > 0) ? $rata2tahunan : '-'; ?></td>
                <?php 
              } 
              ?>

              <?php 
                foreach ($metadata_volume as $field) { 
                  if(isset($entries['sumthnan_'.$field])){
                    $totaltahunan = number_format($entries['sumthnan_'.$field],0,',','.'); 
                  }else{
                    $totaltahunan = 0;
                  }

                  ?>
                  <td style="text-align:right;"><?php echo ($totaltahunan > 0) ? $totaltahunan : '-'; ?></td>
                  <?php 
                } 
              ?>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <?php echo $data['pagination']; ?>
    <div style="clear:both;" ></div>
    <br>
  <?php else: ?>
    <div class="well"><?php echo lang('laporan:data:no_entry'); ?></div>
  <?php endif;?>

  <!--- sandingan periode -->

  <?php if($this->input->get('f-is_sandingan')) { ?>
    <?php if ($data2['total'] > 0): ?>
      
      <p class="pull-left"><?php echo $text_periode2 ?></p>
      <p class="pull-right"><?php echo lang('laporan:showing').' '.count($data2['entries']).' '.lang('laporan:of').' '.$data2['total'] ?></p>
      
      <div class="table-responsive" style="width: 100%; overflow: auto;">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th rowspan="2">No</th>
              <th rowspan="2">Provinsi</th>
              <th rowspan="2">Kota</th>
              <th rowspan="2">Nama Unit</th>
              <th rowspan="2">Petugas</th>
              <?php foreach ($komoditas as $key => $metadata) { ?>
                <th rowspan="2"><?php echo $metadata['nama'] ?></th>
              <?php } ?>

              <th colspan="<?php echo count($metadata_harga); ?>" style="text-align:center;">Rata-Rata Satu Tahun Per Pendamping di Kota/Kab</th>
              <?php if(count($metadata_volume) > 0) { ?> 
                <th colspan="<?php echo count($metadata_volume); ?>" style="text-align:center;">Total  Satu Tahun Per Pendamping di Kota/Kab</th>
              <?php } ?>
            </tr>
            <tr>
            <?php 
              foreach ($metadata_harga as $field) { 
                $explode = explode('_',$field);
                $nama = $explode[0]; ?>
                
                <th><?php echo $nama ?></th>
                <?php 
              } 
            ?>

            <?php 
              foreach ($metadata_volume as $field) { 
                $explode = explode('_',$field);
                $nama = $explode[0]; ?>
                
                <th><?php echo $nama ?></th>
                <?php 
              } 
            ?>
          </tr>
          </thead>
          <tbody>
            <?php 
            $cur_page = (int) $this->uri->segment($pagination_config2['uri_segment']);
            if($cur_page != 0){
              $no = $cur_page + 1;
            }else{
              $no = 1;
            }
            ?>
            
            <?php
            foreach ($data2['entries'] as $key => $entries) { ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $entries['provinsi'] ?></td>
                <td><?php echo $entries['kota'] ?></td>
                <td><?php echo $entries['unit_name'] ?></td>
                <td><?php echo $entries['display_name'] ?></td>
                <?php 
                  foreach ($komoditas as $key => $metadata) { 
                    $key = $key+1;?>
                    <td style="text-align:right;"><?php echo number_format($entries[$metadata['field']],0,',','.'); ?></td>
                    <?php 
                  } 
                ?>

                <?php 
                foreach ($metadata_harga as $field) { 
                  $rata2tahunan = number_format($entries['avgthnan_'.$field],0,',','.'); ?>
                  <td style="text-align:right;"><?php echo ($rata2tahunan > 0) ? $rata2tahunan : '-'; ?></td>
                  <?php 
                } 
                ?>

                <?php 
                  foreach ($metadata_volume as $field) { 
                    $totaltahunan = number_format($entries['sumthnan_'.$field],0,',','.'); ?>
                    <td style="text-align:right;"><?php echo ($totaltahunan > 0) ? $totaltahunan : '-'; ?></td>
                    <?php 
                  } 
                ?>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <?php echo $data2['pagination']; ?>
      
    <?php else: ?>
      <div class="well"><?php echo lang('laporan:data:no_entry'); ?></div>
    <?php endif;?>

  <?php } ?>

<?php } ?>


