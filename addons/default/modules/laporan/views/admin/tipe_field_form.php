<div class="page-header">
	<h1><?php echo lang('laporan:tipe_field:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_tipe_field"><?php echo lang('laporan:nama_tipe_field'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama_tipe_field') != NULL){
					$value = $this->input->post('nama_tipe_field');
				}elseif($mode == 'edit'){
					$value = $fields['nama_tipe_field'];
				}
			?>
			<input name="nama_tipe_field" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="nama_tipe_field" />

		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>
