<div class="page-header">
  <h1><?php echo lang('laporan:sandingkan_laporan'); ?> (Kinerja Pendamping)</h1>
  <!-- <div class="btn-group content-toolbar">
    <a class="btn btn-yellow btn-sm" target="blank" href="<?php echo base_url() ?>admin/laporan/data_kinerja/index?<?php echo $_SERVER['QUERY_STRING'] ?>">
      <i class="fa fa-list-alt"></i>
      <span class="no-text-shadow"><?php echo lang('global:back') ?></span>
    </a>
  </div> -->
</div>

<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get')) ?>
  <div class="form-group">
    <label><?php echo lang('laporan:tipe_laporan'); ?></label>&nbsp;
    <?php
      $value = null;
      if($this->input->get('f-tipe_laporan') != ""){
        $value = $this->input->get('f-tipe_laporan');
      }
    ?>
    <select name="f-tipe_laporan" onchange="this.form.submit()">
      <option value=""><?php echo lang('global:select-pick') ?></option>
      <?php 
        foreach ($tipes as $key => $tipe) { 
          if(in_array($tipe['id'], $type_ids) || group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_prov_laporan')){ ?>
            <option value="<?php echo $tipe['id'] ?>" <?php echo ($value == $tipe['id']) ? 'selected' : ''; ?>><?php echo $tipe['nama_laporan'] ?></option>
            <?php
          }
        } 
      ?>
    </select>
  </div>
  <hr>
<?php echo form_close(); ?>

<?php 
if($this->input->get('f-tipe_laporan')) {  ?>

  <?php echo form_open(base_url().'admin/laporan/data_kinerja/sandingkan_laporan', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>

    <input type="hidden" id="tipe_laporan" name="f-tipe_laporan" value="<?php echo $f_tipe_laporan ?>">

    <?php 
    if(group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_prov_laporan')) { ?>

      <!--- show location untuk user yg bisa manambahkan semua laporan -->
        <div class="form-group">
          <label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?></label>

          <div class="col-sm-6">
            <?php 
               $val_prov = $id_provinsi;
              if(!group_has_role('laporan','view_all_laporan')){ ?>
                <input type="text" value="<?php echo $nama_provinsi; ?>" readonly>
                <input type="hidden"  id="provinsi" value="<?php echo $id_provinsi; ?>" readonly>
                <?php
              }else{
                if($this->input->get('f-provinsi') != NULL){
                  $val_prov = $this->input->get('f-provinsi');
                }
                ?>
                <select name="f-provinsi" id="provinsi" class="col-xs-10 col-sm-5">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                    <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                  <?php } ?>
                </select>
                <?php
              }
            ?>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label no-padding-right" for="kota"><?php echo lang('location:kota:singular'); ?></label>

          <div class="col-sm-6">
            <?php 
              $val_kota = $id_kota;
              if($this->input->get('f-kota') != NULL){
                $val_kota = $this->input->get('f-kota');
              }
            ?>
            <select name="f-kota" id="kota" class="col-xs-10 col-sm-5">
              <?php 
                if(count($kota['entries']) > 0) { ?>
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php 
                  foreach ($kota['entries'] as $kota_entry){ ?>
                    <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($val_kota == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                    <?php 
                  } 
                }else{ ?>
                  <option value=""><?php echo lang('global:select-none') ?></option>
                  <?php
                }
              ?>
            </select>
            <span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>

            <script type="text/javascript">
              $(document).ready(function(){
                <?php if($val_prov == NULL) { ?>
                  $("#provinsi").change();
                <?php } ?>

                <?php if($val_kota == NULL) { ?>
                  $("#kota").change();
                <?php } ?>
              });

              $('#provinsi').change(function() {
                $("#id_unit").html('<option value=""><?php echo  lang("global:select-none") ?></option>');
                var id_provinsi = $(this).val();
                $("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $.ajax({
                  url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                  dataType: 'json',
                  success: function(data){
                    if(data.length > 0){
                      $('#kota').html('<option value="">-- Pilih --</option>');
                    }else{
                      $('#kota').html('<option value="">-- Tidak ada --</option>');
                    }
                    $.each(data, function(i, object){
                      $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                    });
                    $("#kota").change();
                    $('.loading-kota').html('');
                  }
                });
              });

              // khusu sitoni
              $('#kota').change(function() {
                var kota = ($(this).val() == '') ? 0 : $(this).val();
                var tipe_laporan = '<?php echo $this->input->get('f-tipe_laporan') ?>';
                tipe_laporan = (tipe_laporan == 2) ? 1 : tipe_laporan; 
                //tipe_laporan = (tipe_laporan == 7) ? 6 : tipe_laporan;
                $("#<?php echo $org_name ?>").html('<option value="-1"><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading<?php echo $org_name ?>').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $("#<?php echo $org_name ?>").load("<?php echo site_url('admin/laporan/data_kinerja/ajax_get_unit_by_kota') ?>" + '/' + kota + '/' + tipe_laporan, function(data) {
                    $("#<?php echo $org_name ?>").change();
                  $('.loading<?php echo $org_name ?>').html('');
                });
              });
            </script>
          </div>
        </div>
      <?php
    }

    // Load Organization--------
     
    $n = 0;
    $addedLevel = array();

    if(isset($organization['id_organization_unit']) && $organization['id_organization_unit'] != -1) { ?>
      <input type="hidden" name="id_organization_unit" value="<?php echo $organization['id_organization_unit'] ?>">
      <?php
    }

    foreach ($types as $key => $type) {
        if (in_array($type['level'], $addedLevel)) {
            continue;
        }
        $addedLevel[] = $type['level'];
        $n++;
        $types2[] = $type;
        if($type['slug'] == $node_type_slug){
            break;
        }
    }
    $n2 = $n;
    
    foreach ($types2 as $i => $type) {

      $parent_field_name = isset($field_name) ? $field_name : null;
      $field_name = $i == $n - 1 ? 'id_organization_unit' : ('id_organization_unit_' . $type['level']);
      ?>
      <div class="form-group">
        <label class="col-sm-2 control-label no-padding-right" for="<?php echo $field_name; ?>"><?php echo $type['name']; ?></label>
        <div class="col-sm-10">
          <?php
          if(isset($organization[$field_name])){
            $value = $organization[$field_name];
          } else {
            $value = $this->input->get($field_name);
          }
          
          $organization[$field_name] = $value != NULL ? $value : -1; 
          $parent_lvl = $type['level'] - 1; 
          
          if(isset($organization['organization_name_'.$type['level']]) && !$is_tti17) { ?>
            <div class="entry-value col-sm-5" id="unit-predefined-text"><?php echo $organization['organization_name_'.$type['level']]; ?></div>
            <?php 
          } else { 
            // if(isset($organization['id_organization_unit_'.$parent_lvl]) && user_units($this->current_user->id) == $organization['id_organization_unit_'.$parent_lvl] || $field_name == 'id_organization_unit_0' || $n2 == 1){ ?>

              <select id="<?php echo $field_name; ?>" name="<?php echo $field_name; ?>" class="col-xs-10 col-sm-5">
                <option value="-1"><?php echo (count($child_organization) > 0) ? lang('global:select-pick') : lang('global:select-none'); ?></option>
                <?php        
                foreach ($child_organization as $child) { 
                  echo '<option value="' . $child['id'] . '"> ' . $child['unit_name'] . '</option>';
                }
                ?>  
              </select>

              <script>
                $( document ).ready(function() {
                  $("#<?php echo $field_name; ?>").val(<?php echo $organization[$field_name]; ?>).change();
                });
              </script>

              <?php 
          }

          if ($parent_field_name) { ?>
            <script type="text/javascript">
              $('#<?php echo $parent_field_name; ?>').change(function() {
                var parent_id = $(this).val();
                $("#<?php echo $field_name; ?>").html('<option><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading<?php echo $field_name ?>').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $("#<?php echo $field_name; ?>").load("<?php echo site_url('laporan/data/ajax_unit_dropdown') ?>" + '/' + parent_id, function(data) {
                  if(data != '<option value="-1">-- Tidak ada --</option>'){
                      $(this).val(<?php echo $organization[$field_name]; ?>).change();
                  }
                  $('.loading<?php echo $field_name ?>').html('');
                });
              });
            </script>
            <?php 
          } 
          ?>
          <span class="loading_unit loading<?php echo $field_name ?>"></span>
        </div>
      </div>
      <?php
    }
    ?> 
    <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="value"><?php echo lang('laporan:komoditas') ?></label> 
      <div class="col-sm-10">
        <?php
          $value = null;
          if($this->input->get('f-komoditas') != ""){
            $value = $this->input->get('f-komoditas');
          }
        ?>
        <select name="f-komoditas" class="col-xs-10 col-sm-5">
          <?php foreach ($komoditas_related as $key => $komoditas_entry) { ?>
            <option value="<?php echo $komoditas_entry['id'] ?>" <?php echo ($value == $komoditas_entry['id']) ? 'selected' : ''; ?>><?php echo $komoditas_entry['nama_komoditas'] ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div id="konten-periode">
      <?php 

      for ($i_periode=1; $i_periode <= $jml_periode; $i_periode++) { ?>
        <div class="form-group periode" id="periode<?php echo $i_periode ?>">
          <label class="col-sm-2 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:periode') .' '.$i_periode; ?></label>
          <div class="col-sm-10">
            <?php
              $val_tahun = date('Y');
              if($this->input->get('f-tahun_'.$i_periode) != ""){
                $val_tahun = $this->input->get('f-tahun_'.$i_periode);
              }
            ?>
            <select class="f-tahun" name="f-tahun_<?php echo $i_periode ?>">
              <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
                <option value="<?php echo $i ?>" <?php echo ($val_tahun == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
              <?php } ?>
            </select>&nbsp;

            <?php
              $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
              $val_bulan = NULL;
              if(isset($_GET['f-bulan_'.$i_periode])){
                $val_bulan = $this->input->get('f-bulan_'.$i_periode);
              }
            ?>
            <select class="f-bulan" name="f-bulan_<?php echo $i_periode ?>">
              <option value="">Bulan</option>
              <?php foreach ($arr_month as $key => $month) { ?>
                <option value="<?php echo $key ?>" <?php echo ($val_bulan == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
              <?php } ?>
            </select>&nbsp;
            <?php
              $val_minggu = NULL;
              if(isset($_GET['f-minggu_ke_'.$i_periode])){
                $val_minggu = $this->input->get('f-minggu_ke_'.$i_periode);
              }
            ?>
            <select class="f-minggu_ke" name="f-minggu_ke_<?php echo $i_periode ?>" class="f-minggu_ke" id="f-minggu_ke1">
              <option value="">Minggu</option>
              <?php
                $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
                for ($i=1;$i<=5;$i++) { ?>
                  <option value="<?php echo $i ?>" <?php echo ($val_minggu == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
              <?php } ?>
            </select>
            &nbsp; 
            s/d
            &nbsp; 
            <?php
              $val_tahun = date('Y');
              if($this->input->get('f-tahun_sd_'.$i_periode) != ""){
                $val_tahun = $this->input->get('f-tahun_sd_'.$i_periode);
              }
            ?>
            <select class="f-tahun_sd" name="f-tahun_sd_<?php echo $i_periode ?>">
              <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
                <option value="<?php echo $i ?>" <?php echo ($val_tahun == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
              <?php } ?>
            </select>&nbsp;

            <?php
              $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
              $val_bulan = NULL;
              if(isset($_GET['f-bulan_sd_'.$i_periode])){
                $val_bulan = $this->input->get('f-bulan_sd_'.$i_periode);
              }
            ?>
            <select class="f-bulan_sd" name="f-bulan_sd_<?php echo $i_periode ?>">
              <option value="">Bulan</option>
              <?php foreach ($arr_month as $key => $month) { ?>
                <option value="<?php echo $key ?>" <?php echo ($val_bulan == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
              <?php } ?>
            </select>&nbsp;
            <?php
              $val_minggu = NULL;
              if(isset($_GET['f-minggu_ke_sd_'.$i_periode])){
                $val_minggu = $this->input->get('f-minggu_ke_sd_'.$i_periode);
              }
            ?>
            <select class="f-minggu_ke_sd" name="f-minggu_ke_sd_<?php echo $i_periode ?>" class="f-minggu_ke_sd" id="f-minggu_ke_sd1">
              <option value="">Minggu</option>
              <?php
                $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
                for ($i=1;$i<=5;$i++) { ?>
                  <option value="<?php echo $i ?>" <?php echo ($val_minggu == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
              <?php } ?>
            </select>
            &nbsp; 
            <?php if($i_periode > 1){ ?>
              <button type="button" class="btn btn-danger btn-xs" onclick="remove_periode(<?php echo $i_periode ?>)"><i class="fa fa-close"></i></button>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="value"></label> 
      <div class="col-sm-10">
        <input type="hidden" name="f-jml_periode" id="f-jml_periode" value="<?php echo $jml_periode ?>">
        <button class="btn btn-primary btn-xs" type="button" id="btn-add-periode"><i class="fa fa-plus"></i> Tambah Periode</button>
      </div>
    </div>

    <script type="text/javascript">

      $('#btn-add-periode').click(function(){
        var content = '<div class="form-group periode" id="periode"><label class="col-sm-2 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:periode'); ?><span id="lbl-periode"></span></label><div class="col-sm-10">';

        var tahun = '<select class="f-tahun" name="f-tahun">';
        <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
          tahun += '<option value="<?php echo $i ?>" <?php echo ($val_tahun == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>';
        <?php } ?>
        tahun += '</select> &nbsp;';
        content += tahun;

        var bulan = '<select class="f-bulan" name="f-bulan"><option value="">Bulan</option>';
        <?php foreach ($arr_month as $key => $month) { ?>
          bulan += '<option value="<?php echo $key ?>" ><?php echo $month; ?></option>';
        <?php } ?>
        bulan += '</select> &nbsp;';
        content += bulan;

        var minggu_ke = '<select class="f-minggu_ke" name="f-minggu_ke" class="f-minggu_ke" id="f-minggu_ke1"><option value="">Minggu</option>';
        <?php
          $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
          for ($i=1;$i<=5;$i++) { ?>
            minggu_ke += '<option value="<?php echo $i ?>"><?php echo $romawi[$i] ?></option>';
        <?php } ?>
        minggu_ke += '</select> &nbsp; s/d &nbsp; ';
        content += minggu_ke;

        var tahun_sd = '<select class="f-tahun_sd" name="f-tahun_sd">';
        <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
          tahun_sd += '<option value="<?php echo $i ?>" <?php echo ($val_tahun == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>';
        <?php } ?>
        tahun_sd += '</select> &nbsp;';
        content += tahun_sd;

        var bulan_sd = '<select class="f-bulan_sd" name="f-bulan_sd"><option value="">Bulan</option>';
        <?php foreach ($arr_month as $key => $month) { ?>
          bulan_sd += '<option value="<?php echo $key ?>" ><?php echo $month; ?></option>';
        <?php } ?>
        bulan_sd += '</select> &nbsp;';
        content += bulan_sd;

        var minggu_ke_sd = '<select class="f-minggu_ke_sd" name="f-minggu_ke_sd" class="f-minggu_ke_sd" id="f-minggu_ke_sd1"><option value="">Minggu</option>';
        <?php
          $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
          for ($i=1;$i<=5;$i++) { ?>
            minggu_ke_sd += '<option value="<?php echo $i ?>"><?php echo $romawi[$i] ?></option>';
        <?php } ?>
        minggu_ke_sd += '</select> &nbsp;';
        content += minggu_ke_sd;

        content += ' <button type="button" class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button></div></div>';

        $("#konten-periode").append(content);

        setnumber();
      })

      function setnumber(){
        no = 0;
        $(".periode").each(function(){

          no++;
          $(this).attr('id','periode'+no);
          $(this).find("#lbl-periode").html(' '+no);
          $(this).find("select[class='f-tahun']").attr('id','f-tahun_'+no);
          $(this).find("select[class='f-tahun']").attr('name','f-tahun_'+no);
          $(this).find("select[class='f-bulan']").attr('id','f-bulan_'+no);
          $(this).find("select[class='f-bulan']").attr('name','f-bulan_'+no);
          $(this).find("select[class='f-minggu_ke']").attr('id','f-minggu_ke_'+no);
          $(this).find("select[class='f-minggu_ke']").attr('name','f-minggu_ke_'+no);

          $(this).find("select[class='f-tahun_sd']").attr('id','f-tahun_sd_'+no);
          $(this).find("select[class='f-tahun_sd']").attr('name','f-tahun_sd_'+no);
          $(this).find("select[class='f-bulan_sd']").attr('id','f-bulan_sd_'+no);
          $(this).find("select[class='f-bulan_sd']").attr('name','f-bulan_sd_'+no);
          $(this).find("select[class='f-minggu_ke_sd']").attr('id','f-minggu_ke_sd_'+no);
          $(this).find("select[class='f-minggu_ke_sd']").attr('name','f-minggu_ke_sd_'+no);

          $(this).find("button").attr('onclick','remove_periode('+no+')');
          $(this).find("button").attr('data-periode',no);
        });
        $("#f-jml_periode").val(no);
      }

      function remove_periode(no){
        $("#periode"+no).remove();
        setnumber();
      }
    </script>
    <hr>
    <div class="form-group">
      <div class="col-sm-2"></div>
      <div class="col-sm-10">
        <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
          <i class="icon-ok"></i>
          <?php echo lang('buttons:filter'); ?>
        </button>

        <a href="<?php echo site_url('admin/laporan/data_kinerja/index?f-tipe_laporan='.$f_tipe_laporan); ?>" class="btn btn-xs">
          <i class="icon-remove"></i>
          <?php echo lang('buttons:clear'); ?>
        </a>
      </div>
    </div>

  <?php echo form_close() ?>

  <div class="btn-group content-toolbar">
    <a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/data_kinerja/sandingkan_laporan?page=download&<?php echo $_SERVER['QUERY_STRING'] ?>">
      <i class="fa fa-download"></i>
      <span class="no-text-shadow"><?php echo lang('laporan:download') ?></span>
    </a>
  </div>
  <div class="btn-group content-toolbar">
    <a class="btn btn-yellow btn-sm" target="blank" href="<?php echo base_url() ?>admin/laporan/data_kinerja/sandingkan_laporan?page=print&<?php echo $_SERVER['QUERY_STRING'] ?>">
      <i class="fa fa-print"></i>
      <span class="no-text-shadow"><?php echo lang('laporan:print') ?></span>
    </a>
  </div>
  <br>
  <div class="table-responsive" style="width: 100%; overflow: auto;">
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>Metadata</th>
          <?php for ($i=1; $i <= $jml_periode; $i++) { ?> 
            <th><?php echo $text['periode'.$i] ?> s/d <?php echo $text_sd['periode'.$i] ?></th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($komoditas as $key => $metadata) { ?>
          <tr>
            <td><?php echo $metadata['nama'] ?></td>
            <?php for ($i=1; $i <= $jml_periode; $i++) { ?> 
              <td><?php echo $metadata_col[$metadata['id']][$i]; ?></td>
            <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>

<?php } ?>