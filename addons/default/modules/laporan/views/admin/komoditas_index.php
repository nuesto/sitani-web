<div class="page-header">
	<h1><?php echo lang('laporan:komoditas:plural'); ?></h1>
	
	<div class="btn-group content-toolbar">
		<?php if(group_has_role('laporan', 'manage_komoditas')){ ?>
			<a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/komoditas/create<?php echo $uri ?>">
				<i class="icon-plus"></i>	
				<span class="no-text-shadow"><?php echo lang('laporan:komoditas:new') ?></span>
			</a>
		<?php } ?>
	</div>
</div>

<?php if ($komoditas['total'] > 0): ?>
	
	<p class="pull-right"><?php echo lang('laporan:showing').' '.count($komoditas['entries']).' '.lang('laporan:of').' '.$komoditas['total'] ?></p>
	<div class="table-responsive">
	<table class="table table-bordered" id="table-1">
		<thead>
			<tr class="nodrag nodrop">
				<th width="50">No</th>
				<th><?php echo lang('laporan:nama'); ?></th>
				<th><?php echo lang('laporan:slug'); ?></th>
				<th><?php echo lang('laporan:kode_komoditas'); ?></th>
				<th><?php echo lang('laporan:deskripsi'); ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$no = 1;
			?>
			
			<?php foreach ($komoditas['entries'] as $komoditas_entry): ?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $komoditas_entry['nama_komoditas']; ?></td>
				<td><?php echo $komoditas_entry['slug']; ?></td>
				<td><?php echo $komoditas_entry['kode_komoditas']; ?></td>
				<td><?php echo $komoditas_entry['deskripsi']; ?></td>
				<td>
				<?php 
				if(group_has_role('laporan', 'manage_komoditas')){
					echo anchor('admin/laporan/komoditas/edit/' . $komoditas_entry['id'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
				}
				?>
				<?php 
				if(group_has_role('laporan', 'manage_komoditas')){
					echo anchor('admin/laporan/komoditas/delete/' . $komoditas_entry['id'].$uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
				}
				?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	</div>
<?php else: ?>
	<div class="well"><?php echo lang('laporan:komoditas:no_entry'); ?></div>
<?php endif;?>