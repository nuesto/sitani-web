<div class="page-header">
	<h1><?php echo lang('laporan:data:edit'); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().'?'.$_SERVER['QUERY_STRING']); ?>
	<div class="form-horizontal">
		<input type="hidden" value="<?php echo $id_unit ?>" name="id_unit">
		<input type="hidden" value="<?php echo $id_user ?>" name="id_user">
		<?php if($this->input->get('f-tipe_laporan') <=2) { ?>
			<input type="hidden" value="<?php echo $id_absen ?>" name="id_absen">
		<?php }else{ ?>
			<input type="hidden" value="<?php echo $tanggal ?>" name="tanggal">
		<?php	} ?>
		<input type="hidden" value="<?php echo $id_laporan_tipe ?>" name="id_laporan_tipe">
		<input type="hidden" value="<?php echo $id_laporan_komoditas ?>" name="id_laporan_komoditas">
	<?php foreach ($metadata as $key => $data) { ?>
		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="value"><?php echo $data['nama'] ?></label>

			<div class="col-sm-9">
				<?php 
					$value = NULL;
					if($this->input->post($data['field']) != NULL){
						$value = $this->input->post($data['field']);
					}else{
						$value = $data['value'];
					}
				?>
				<input name="<?php echo $data['field'] ?>" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-2" id="" />
				
				<span class="help-inline col-xs-12 col-sm-8">
					<span class="middle"><?php echo $data['satuan'] ?> <?php if($data['nilai_minimal'] !=""){?>, minimal: <?php echo $data['nilai_minimal'] ?><?php } if($data['nilai_maksimal'] != "") { ?>, maksimal: <?php echo $data['nilai_maksimal'] ?><?php } ?> </span>
				</span>
			</div>
		</div>
	<?php } ?>
	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
			<?php if(!$this->session->userdata('id_enumerator')) { ?>
				<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
			<?php } ?>
		</div>
	</div>
</div>
<?php echo form_close();?>