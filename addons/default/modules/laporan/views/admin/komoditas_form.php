<div class="page-header">
	<h1><?php echo lang('laporan:komoditas:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_komoditas"><?php echo lang('laporan:nama_komoditas'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = ($this->input->get('f-nama_komoditas')) ? $this->input->get('f-nama_komoditas') : NULL;;
				if($this->input->post('nama_komoditas') != NULL){
					$value = $this->input->post('nama_komoditas');
				}elseif($mode == 'edit'){
					$value = $fields['nama_komoditas'];
				}
			?>
			<input name="nama_komoditas" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="nama_komoditas" />

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="slug"><?php echo lang('laporan:slug'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('slug') != NULL){
					$value = $this->input->post('slug');
				}elseif($mode == 'edit'){
					$value = $fields['slug'];
				}
			?>
			<input name="slug" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="slug"/>
			<script type="text/javascript">
        $('#nama_komoditas').slugify({slug: '#slug', type: '-'});
    	</script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="kode_komoditas"><?php echo lang('laporan:kode_komoditas'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = ($this->input->get('f-kode_komoditas')) ? $this->input->get('f-kode_komoditas') : NULL;;
				if($this->input->post('kode_komoditas') != NULL){
					$value = $this->input->post('kode_komoditas');
				}elseif($mode == 'edit'){
					$value = $fields['kode_komoditas'];
				}
			?>
			<input name="kode_komoditas" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="kode_komoditas"/>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="deskripsi"><?php echo lang('laporan:deskripsi'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = ($this->input->get('f-deskripsi')) ? $this->input->get('f-deskripsi') : NULL;;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}
			?>
			<textarea name="deskripsi" class="col-xs-10 col-sm-4"><?php echo $value; ?></textarea>

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nilai_maksimal"><?php echo lang('laporan:urutan'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = $total;
				if($this->input->post('urutan') != NULL){
					$value = $this->input->post('urutan');
				}elseif($mode == 'edit'){
					$value = $fields['urutan'];
				}
			?>
			<select name="urutan" id="urutan" >
				<?php for($i=0;$i<$total;$i++) { ?>
					<option <?php echo ($i == $value) ? 'selected' : ''; ?>><?php echo $i ?></option>
				<?php } ?>
    	</select>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">
	$('#slug').keyup(function (e) {
    var allowedChars = /^[a-z\d -]+$/i;
    var str = String.fromCharCode(e.charCode || e.which);

    var forbiddenChars = /[^a-z\d -]/gi;
    if (forbiddenChars.test(this.value)) {
        this.value = this.value.replace(forbiddenChars, '');
    }

    if (allowedChars.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
	});
</script>