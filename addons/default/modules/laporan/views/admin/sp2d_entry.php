<div class="page-header">
	<h1>
		<span><?php echo lang('laporan:sp2d:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/laporan/sp2d/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('laporan:back') ?>
		</a>

		<?php if(group_has_role('laporan', 'edit_all_sp2d')){ ?>
			<a href="<?php echo site_url('admin/laporan/sp2d/edit/'.$sp2d['id'].$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('laporan', 'edit_own_sp2d')){ ?>
			<?php if($sp2d['created_by'] == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/laporan/sp2d/edit/'.$sp2d['id'].$uri); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('laporan', 'delete_all_sp2d')){ ?>
			<a href="<?php echo site_url('admin/laporan/sp2d/delete/'.$sp2d['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('laporan', 'delete_own_sp2d')){ ?>
			<?php if($sp2d['created_by'] == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/laporan/sp2d/delete/'.$sp2d['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('location:provinsi:singular'); ?></div>
		<div class="entry-value col-sm-8"><?php echo $sp2d['provinsi']; ?></div>
	</div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:nama_sp2d'); ?></div>
		<?php if(isset($sp2d['nama_sp2d'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $sp2d['nama_sp2d']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:deskripsi'); ?></div>
		<?php if(isset($sp2d['deskripsi'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $sp2d['deskripsi']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:file'); ?></div>
		<?php if(isset($sp2d['file'])){ ?>
		<div class="entry-value col-sm-8">
			<a href="<?php echo base_url() ?>files/download/<?php echo $sp2d['file'] ?>" class="btn btn-yellow btn-minier"/><i class="fa fa-download"></i> Download</a>
		</div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:created_by'); ?></div>
		<?php if(isset($sp2d['created_by'])){ ?>
		<div class="entry-value col-sm-8"><?php echo user_displayname($sp2d['created_by'], true); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:created_on'); ?></div>
		<?php if(isset($sp2d['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $sp2d['created_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:updated_by'); ?></div>
		<?php if(isset($sp2d['updated_by'])){ ?>
		<div class="entry-value col-sm-8"><?php echo user_displayname($sp2d['updated_by'], true); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:updated_on'); ?></div>
		<?php if(isset($sp2d['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $sp2d['updated_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:status_saat_ini'); ?></div>
		<div class="entry-value col-sm-8">
			<?php $arr_lbl_status = array(1=>'yellow',2=>'warning',3=>'success',4=>'danger'); ?>
			<span class="label label-<?php echo $arr_lbl_status[$sp2d['status']]; ?>">
				<?php echo lang('laporan:status_'.$sp2d['status']); ?>
			</span>
		</div>
	</div>

	<?php if($sp2d['catatan']!=""){ ?>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('laporan:catatan'); ?></div>
		<div class="entry-value col-sm-8"><?php echo $sp2d['catatan']; ?></div>
	</div>
	<?php } ?>

	<?php if(isset($sp2d['changed_on'])){ ?>
	<div class="row">
		<div class="entry-label col-sm-2">
			<?php 
			if($sp2d['status'] > 1){
				echo lang('laporan:status_'.$sp2d['status']) .' pada';
			}else{
				echo lang('laporan:changed_on');
			}
			?>
		</div>
		<div class="entry-value col-sm-8"><?php echo $sp2d['changed_on']; ?></div>
	</div>
	<?php } ?>
</div>

<?php if($sp2d['status'] < 3) { ?>
	<?php if(group_has_role('laporan','change_sp2d_status')){ ?>
		<br>
		<div class="page-header" id="ubah_status">
			<h1><?php echo lang('laporan:sp2d:change_status'); ?><span class="loading"></span></h1>
		</div>
		<div id="result_ubah_status"></div>


		<form action="<?php echo site_url('admin/laporan/sp2d/ajax_edit/edit/'.$sp2d["id"]); ?>" method="post" id="form_ubah_status">
			<div class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label no-padding-right" for="id_ruangan"><?php echo lang('laporan:ubah_status_menjadi'); ?> *</label>

					<div class="col-sm-10">
						<input type="hidden" name="id" value="<?php echo $sp2d['id']; ?>">
						<?php $arr_status = array(1=>lang('laporan:status_1'), 2=>lang('laporan:status_2'),3=>lang('laporan:status_3'),4=>lang('laporan:status_4')); ?>
						<select name="status" class="col-xs-10 col-sm-5" id="">
							<?php 
							foreach ($arr_status as $key => $status) { 
								//if($key >= $sp2d['status']) {
									?>

									<option value="<?php echo $key ?>" <?php echo ($sp2d['status'] == $key) ? 'selected' : ''; ?>><?php echo $status; ?></option>
									<?php  
								//}
							} 
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label no-padding-right" for="catatan"><?php echo lang('laporan:catatan'); ?></label>

					<div class="col-sm-10">
						<textarea name="catatan" class="col-xs-10 col-sm-8" rows="5" id="" /></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label no-padding-right"></label>

					<div class="col-sm-10">
						<button id="submit_status" class="btn btn-primary btn-sm"><span><?php echo lang('laporan:sp2d:change_status'); ?></span></button>
					</div>
				</div>

			</div>
		</form>
	<?php } ?>
<?php } ?>

	<script>
	$('#form_ubah_status').submit(function(e){
		e.preventDefault();
		var form = $(this).attr('action');
		var date = '<?php echo date("Y-m-d H:i:s") ?>';
		var serialData = $(this).serialize()+'&changed_on='+date;
		$('#submit_status').toggleClass('disabled');
		$('#ubah_status .loading').html(' <i class="icon-spinner icon-spin orange bigger-100"></i>');
		$.ajax({
			type: "POST",
			url: form,
			data: serialData,
			success: function(data){
				$('#result_ubah_status').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>' + data + '</div>');
		    	$('#ubah_status .loading').html('');
		    	console.log(data);
		    	// window.hash='ubah_status';
		    	setTimeout("location.reload()",1000);
		    },
		    error: function(data){
		    	$('#ubah_status .loading').html('');
	        	$('#result_ubah_status').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>'+data.responseText+'</div>');
		    },
		    complete: function(){
		    	$('#submit_status').toggleClass('disabled',false);

		    }
		});


	});
	</script>
