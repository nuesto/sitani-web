<?php foreach ($metadata as $key => $data) { ?>
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="value"><?php echo $data['nama'] ?></label>

		<div class="col-sm-9">
			<?php 
				$value = NULL;
				if($this->input->post($data['field']) != NULL){
					$value = $this->input->post($data['field']);
				}elseif($mode == 'edit'){
					$value = $fields[$data['field']];
				}
			?>
			<input name="<?php echo $data['field'] ?>" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-2" id="" />

			<span class="help-inline col-xs-12 col-sm-8">
				<span class="middle"><?php echo $data['satuan'] ?> <?php if($data['nilai_minimal'] !=""){?>, minimal: <?php echo $data['nilai_minimal'] ?><?php } if($data['nilai_maksimal'] != "") { ?>, maksimal: <?php echo $data['nilai_maksimal'] ?><?php } ?> </span>
			</span>
		</div>
	</div>
<?php } ?>