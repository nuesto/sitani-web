<div class="page-header">
	<h1><?php echo lang('laporan:pendamping') ?></h1>

  <div class="btn-group content-toolbar">
    <a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/pendamping/import/<?php echo $uri ?>">
      <i class="icon-plus"></i> 
      <span class="no-text-shadow"><?php echo lang('laporan:pendamping:import') ?></span>
    </a>
  </div>
	<?php
  if($this->input->get('f-groups')){
    $link_download = ($this->input->get('f-groups') == 7 || $this->input->get('f-groups') == 10) ? 'download' : 'download_harian'; 
    if ($pendamping['total'] > 0){ ?>
  		<div class="btn-group content-toolbar">
  			<a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/pendamping/<?php echo $link_download ?>/0?<?php echo $_SERVER['QUERY_STRING'] ?>">
  				<i class="fa fa-download"></i>	
  				<span class="no-text-shadow"><?php echo lang('global:download') ?></span>
  			</a>
  		</div>
  		<div class="btn-group content-toolbar">
  			<a class="btn btn-yellow btn-sm" target="blank" href="<?php echo base_url() ?>admin/laporan/pendamping/<?php echo $link_download ?>/1?<?php echo $_SERVER['QUERY_STRING'] ?>">
  				<i class="fa fa-print"></i>	
  				<span class="no-text-shadow"><?php echo lang('global:print') ?></span>
  			</a>
  		</div>	

  	<?php if(group_has_role('users', 'manage_users') || group_has_role('users', 'manage_unit_users')){ ?>
  		<div class="btn-group content-toolbar">
  			<a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/users/create?f_group=<?php echo $id_groups; ?>">
  				<i class="icon-plus"></i>	
  				<span class="no-text-shadow"><?php echo lang('laporan:pendamping:new'); ?></span>
  			</a>
  		</div>
  	<?php } 
    }
  }
  ?>
</div>
<br>

<?php
$url = base_url()."admin/laporan/pendamping/index/";
echo form_open($url, array('class' => 'form-inline', 'method' => 'get')) ?>
  <div class="form-group">
    <label><?php echo lang('laporan:pendamping'); ?></label>&nbsp;
    <?php
      $value = null;
      if($this->input->get('f-groups') != ""){
        $value = $this->input->get('f-groups');
      }

      $id_groups = $value;
    ?>
    <select name="f-groups" onchange="this.form.submit('')">
      <option value=""><?php echo lang('global:select-pick') ?></option>
      <?php 
      if ($id_groups == '7'){
        echo "<option selected value='7'>Pendamping 2016</option>";
      } else {
        echo "<option value='7'>Pendamping 2016</option>";
      }
      if ($id_groups == '9'){
        echo "<option selected value='9'>Pendamping 2017</option>";
      } else {
        echo "<option value='9'>Pendamping 2017</option>";
      }
      if ($id_groups == '10'){
        echo "<option selected value='10'>Pendamping 2018</option>";
      } else {
        echo "<option value='10'>Pendamping 2018</option>";
      }
      ?>
    </select>
  </div>
  <hr>
<?php echo form_close(); ?>


<?php if($id_groups == '7' || $id_groups == '10'){ ?>

<?php echo form_open(base_url().'admin/laporan/pendamping/index/', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
	<!-- 
	<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" ><?php echo lang('laporan:tipe_laporan'); ?></label>&nbsp;
    <?php
      $value = null;
      if($this->input->get('f-tipe_laporan') != ""){
        $value = $this->input->get('f-tipe_laporan');
      }
    ?>
    <div class="col-sm-6">
	    <select name="f-tipe_laporan">
	      <option value=""><?php echo lang('global:select-pick') ?></option>
	      <?php foreach ($tipes as $key => $tipe) { ?>
	        <option value="<?php echo $tipe['id'] ?>" <?php echo ($value == $tipe['id']) ? 'selected' : ''; ?>><?php echo $tipe['nama_laporan'] ?></option>
	      <?php } ?>
	    </select>
	  </div>
  </div> -->
  <input type="hidden" name="f-groups" value="<?php echo $this->input->get('f-groups') ?>">
	<div class="form-group">
	  <label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?></label>

	  <div class="col-sm-6">
	    <?php 
	      $val_prov = $id_provinsi;
	      if($this->input->get('f-provinsi') != NULL){
	        $val_prov = $this->input->get('f-provinsi');
	      }
	    ?>
	    <select name="f-provinsi" id="provinsi" class="col-xs-10 col-sm-5">
	      <option value=""><?php echo lang('global:select-pick') ?></option>
	      <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
	        <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
	      <?php } ?>
	    </select>
	  </div>
	</div>

	<div class="form-group">
	  <label class="col-sm-2 control-label no-padding-right" for="kota"><?php echo lang('location:kota:singular'); ?></label>

	  <div class="col-sm-6">
	    <?php 
	      $val_kota = $id_kota;
	      if($this->input->get('f-kota') != NULL){
	        $val_kota = $this->input->get('f-kota');
	      }
	    ?>
	    <select name="f-kota" id="kota" class="col-xs-10 col-sm-5">
	      <?php 
	        if(count($kota['entries']) > 0) { ?>
	          <option value=""><?php echo lang('global:select-pick') ?></option>
	          <?php 
	          foreach ($kota['entries'] as $kota_entry){ ?>
	            <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($val_kota == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
	            <?php 
	          } 
	        }else{ ?>
	          <option value=""><?php echo lang('global:select-none') ?></option>
	          <?php
	        }
	      ?>
	    </select>
	    <span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>

	    <script type="text/javascript">
	      $('#provinsi').change(function() {
	        var id_provinsi = $(this).val();
	        $("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
	        $('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
	        $.ajax({
            url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
            dataType: 'json',
            success: function(data){
              if(data.length > 0){
                $('#kota').html('<option value="">-- Pilih --</option>');
              }else{
                $('#kota').html('<option value="">-- Tidak ada --</option>');
              }
              $.each(data, function(i, object){
                $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
              });

              $('.loading-kota').html('');
            }
          });
	      });
	    </script>
	  </div>
	</div>

	<div class="form-group">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
      <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
        <i class="icon-ok"></i>
        <?php echo lang('buttons:filter'); ?>
      </button>

      <a href="<?php echo site_url('admin/laporan/pendamping/index'); ?>" class="btn btn-xs">
        <i class="icon-remove"></i>
        <?php echo lang('buttons:clear'); ?>
      </a>
    </div>
  </div>
<?php echo form_close() ?>

<?php if ($pendamping['total'] > 0): ?>
	
	<p class="pull-right"><?php echo lang('laporan:showing').' '.count($pendamping['entries']).' '.lang('laporan:of').' '.$pendamping['total'] ?></p>
	<div class="table-responsive">
	<?php 

  foreach ($pendamping['entries'] as $pendamping_entry): 

    $rows = ($pendamping_entry['total_gap'] + $pendamping_entry['total_tti']) - 1;
    ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th colspan="10">
            <center>
              Provinsi <?php echo $pendamping_entry['provinsi'] ?><br>
              <?php echo $pendamping_entry['kota'] ?>
            </center>
          </th>
        </tr>
        <tr>
          <th colspan="2">Pendamping</th>
          <th colspan="4">Gapoktan</th>
          <th colspan="4">TTI</th>
        </tr>
        <tr>
          <th>Nama</th>
          <th>No HP</th>
          <th>Nama</th>
          <th>Ketua</th>
          <th>Alamat</th>
          <th>No HP</th>
          <th>Nama</th>
          <th>Pemilik</th>
          <th>Alamat</th>
          <th>No HP</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          
          <?php 
          foreach ($pendamping_entry['gapoktan'] as $key2 => $gap) { 
            $rows2 = count($gap['tti']);
            ?>
            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['display_name'] ?></td>
            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['telp'] ?></td>
            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['unit_name'] ?></td>
            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['nama_ketua'] ?></td>
            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['unit_description'] ?></td>
            <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['hp_ketua'] ?></td>
            <?php
            if(count($gap['tti']) > 0){
              foreach ($gap['tti'] as $key3 => $tti) { 
                if($key3 > 0){ ?>
                  <tr>
                  <?php
                }
                ?>
                <td><?php echo $tti['unit_name']; ?></td>
                <td><?php echo $tti['nama_ketua']; ?></td>
                <td><?php echo $tti['unit_description']; ?></td>
                <td><?php echo $tti['hp_ketua']; ?></td>
                <?php
                if($key3 > 0 || count($gap['tti']) == 1){ ?>
                  </tr>
                  <?php
                }
              }
            }else{ ?>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              </tr>
              <?php
            }
            ?>
          <?php
            if($key2 > 0){ ?>
              </tr>
              <?php
            } 
          } 
        ?>
        </tr>
      </tbody>
    </table>
  <?php endforeach; ?>
  </div>
	
	<?php echo $pendamping['pagination']; ?>
	
<?php else: ?>
	<div class="well"><?php echo lang('laporan:pendamping:no_entry'); ?></div>
<?php endif;?>

<?php } else if ($id_groups == '9'){ ?>

<?php echo form_open(base_url().'admin/laporan/pendamping/index/', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
  <input type="hidden" name="f-groups" value="9">
  <div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?></label>

    <div class="col-sm-6">
      <?php 

        $val_prov = $id_provinsi;
        if($this->input->get('f-provinsi') != NULL){
          $val_prov = $this->input->get('f-provinsi');
        }
      ?>
      <select name="f-provinsi" id="provinsi" class="col-xs-10 col-sm-5">
        <option value=""><?php echo lang('global:select-pick') ?></option>
        <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
          <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
        <?php } ?>
      </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="kota"><?php echo lang('location:kota:singular'); ?></label>

    <div class="col-sm-6">
      <?php 
        $val_kota = $id_kota;
        if($this->input->get('f-kota') != NULL){
          $val_kota = $this->input->get('f-kota');
        }
      ?>
      <select name="f-kota" id="kota" class="col-xs-10 col-sm-5">
        <?php 
          if(count($kota['entries']) > 0) { ?>
            <option value=""><?php echo lang('global:select-pick') ?></option>
            <?php 
            foreach ($kota['entries'] as $kota_entry){ ?>
              <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($val_kota == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
              <?php 
            } 
          }else{ ?>
            <option value=""><?php echo lang('global:select-none') ?></option>
            <?php
          }
        ?>
      </select>
      <span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>

      <script type="text/javascript">
        $('#provinsi').change(function() {
          var id_provinsi = $(this).val();
          $("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
          $('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
          $.ajax({
            url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
            dataType: 'json',
            success: function(data){
              if(data.length > 0){
                $('#kota').html('<option value="">-- Pilih --</option>');
              }else{
                $('#kota').html('<option value="">-- Tidak ada --</option>');
              }
              $.each(data, function(i, object){
                $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
              });

              $('.loading-kota').html('');
            }
          });
        });
      </script>
    </div>
  </div>


  <div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="provinsi">Tipe Unit</label>

    <div class="col-sm-6">
      <?php
        $value = null;
        if($this->input->get('f-tipe_unit') != ""){
          $value = $this->input->get('f-tipe_unit');
        }
      ?>
      <select name="f-tipe_unit"  class="col-xs-10 col-sm-5" id="tipe_unit">
        <option value=""><?php echo lang('global:select-pick') ?></option>
        <option value="4" <?php echo ($value == 4) ? 'selected' : ''; ?>>Gapoktan 2017</option>
        <option value="5" <?php echo ($value == 5) ? 'selected' : ''; ?>>TTI 2017</option>
      </select>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
      <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
        <i class="icon-ok"></i>
        <?php echo lang('buttons:filter'); ?>
      </button>

      <a href="<?php echo site_url('admin/laporan/pendamping/index'); ?>" class="btn btn-xs">
        <i class="icon-remove"></i>
        <?php echo lang('buttons:clear'); ?>
      </a>
    </div>
  </div>
<?php echo form_close() ?>

  <?php if ($pendamping['total'] > 0): ?>
  
  <p class="pull-right"><?php echo lang('laporan:showing').' '.count($pendamping['entries']).' '.lang('laporan:of').' '.$pendamping['total'] ?></p>
    <?php
    // print_r($pendamping['entries']);
    // die();
    ?>
    <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Tipe</th>
          <th>Provinsi</th>
          <th>Kota</th>
          <th>Nama Pendamping</th>
          <th>No HP Pendamping</th>
          <th>Nama Unit</th>
          <th>Ketua</th>
          <th>Alamat</th>
          <th>No HP Ketua</th>
        </tr>
      </thead>
      <tbody>
      <?php 
          $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
          if($cur_page != 0){
          $item_per_page = $pagination_config['per_page'];
          $no = (($cur_page -1) * $item_per_page) + 1;
          }else{
          $no = 1;
          }
          ?>
      <?php 
        foreach ($pendamping['entries'] as $pendamping_entry) {
        ?>
        <tr>
            <td><?php echo $no; $no++; ?></td>
            <td><?php echo $pendamping_entry['type_name'] ?></td>
            <td><?php echo $pendamping_entry['provinsi'] ?></td>
            <td><?php echo $pendamping_entry['kota'] ?></td>
            <td><?php echo $pendamping_entry['display_name'] ?></td>
            <td><?php echo $pendamping_entry['telp'] ?></td>
            <td><?php echo $pendamping_entry['unit_name'] ?></td>
            <td><?php echo $pendamping_entry['nama_ketua'] ?></td>
            <td><?php echo $pendamping_entry['unit_description'] ?></td>
            <td><?php echo $pendamping_entry['hp_ketua'] ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    </div>
  <?php echo $pendamping['pagination']; ?>
  
<?php else: ?>
  <div class="well"><?php echo lang('laporan:pendamping:no_entry'); ?></div>
<?php endif;?>

<?php } ?>