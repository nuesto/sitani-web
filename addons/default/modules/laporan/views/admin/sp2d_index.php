<div class="page-header">
	<h1><?php echo lang('laporan:sp2d:plural'); ?></h1>
	
	<div class="btn-group content-toolbar">
		<?php if(group_has_role('laporan', 'create_sp2d') || group_has_role('laporan', 'create_own_prov_sp2d')){ ?>
			<a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/sp2d/create<?php echo $uri ?>">
				<i class="icon-plus"></i>	
				<span class="no-text-shadow"><?php echo lang('laporan:sp2d:new') ?></span>
			</a>
		<?php } ?>
	</div>
</div>

<?php echo form_open('', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>

	<?php if(group_has_role('laporan','view_all_laporan') || group_has_role('laporan','view_own_prov_laporan')) { ?>
		<div class="form-group">
		  <label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?></label>

		  <div class="col-sm-6">
		    <?php 
		      if(!group_has_role('laporan','view_all_sp2d')){ ?>
		        <input type="text" value="<?php echo $nama_provinsi; ?>" readonly>
		        <input type="hidden"  id="provinsi" value="<?php echo $id_provinsi; ?>" readonly>
		        <?php
		      }else{
		        $val_prov = $id_provinsi;
		        if($this->input->get('f-provinsi') != NULL){
		          $val_prov = $this->input->get('f-provinsi');
		        }
		        ?>
		        <select name="f-provinsi" id="provinsi" class="col-xs-10 col-sm-5">
		          <option value=""><?php echo lang('global:select-pick') ?></option>
		          <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
		            <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
		          <?php } ?>
		        </select>
		        <?php
		      }
		    ?>
		  </div>
		</div>
	<?php } ?>

	<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="f-nama_sp2d"><?php echo lang('laporan:nama_sp2d'); ?></label>
    <div class="col-sm-10">
    	<?php
    		$value = null;
    		if($this->input->get('f-nama_sp2d') != ""){
    			$value = $this->input->get('f-nama_sp2d');
    		}
    	?>
    	<input type="text" name="f-nama_sp2d" value="<?php echo $value ?>" style="width:250px;">
    </div>
	</div>
	
	<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="f-status"><?php echo lang('laporan:status'); ?></label>
    <div class="col-sm-10">
    	<?php
    		$value = null;
    		if($this->input->get('f-status') != ""){
    			$value = $this->input->get('f-status');
    		}
    	?>
    	<?php $arr_status = array(1=>lang('laporan:status_1'), 2=>lang('laporan:status_2'),3=>lang('laporan:status_3'),4=>lang('laporan:status_4')); ?>
			<select name="f-status">
				<option value=""><?php echo lang('global:select-pick') ?></option>
				<?php 
				foreach ($arr_status as $key => $status) {
					?>

					<option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $status; ?></option>
					<?php
				} 
				?>
			</select>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
      <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
        <i class="icon-ok"></i>
        <?php echo lang('buttons:filter'); ?>
      </button>

      <a href="<?php echo site_url('admin/laporan/sp2d/index'); ?>" class="btn btn-xs">
        <i class="icon-remove"></i>
        <?php echo lang('buttons:clear'); ?>
      </a>
    </div>
  </div>
<?php echo form_close() ?>

<?php if ($sp2d['total'] > 0): ?>
	
	<p class="pull-right"><?php echo lang('laporan:showing').' '.count($sp2d['entries']).' '.lang('laporan:of').' '.$sp2d['total'] ?></p>
	
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="50">No</th>
				<th><?php echo lang('location:provinsi:singular'); ?></th>
				<th><?php echo lang('laporan:nama_sp2d'); ?></th>
				<th><?php echo lang('laporan:created_by'); ?></th>
				<th><?php echo lang('laporan:created_on'); ?></th>
				<th><?php echo lang('laporan:status'); ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
      $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
      if($cur_page != 0){
        $item_per_page = $pagination_config['per_page'];
        $no = $cur_page + 1;
      }else{
        $no = 1;
      }
      ?>
			
			<?php $arr_lbl_status = array(1=>'yellow',2=>'warning',3=>'success',4=>'danger'); ?>
			<?php foreach ($sp2d['entries'] as $sp2d_entry): ?>
			<tr>
				<td><?php echo $no++ ?></td>
				<td><?php echo $sp2d_entry['provinsi']; ?></td>
				<td><?php echo $sp2d_entry['nama_sp2d']; ?></td>
				<td><?php echo user_displayname($sp2d_entry['created_by'], true); ?></td>
				<td><?php echo $sp2d_entry['created_on']; ?></td>
				<td>
					<span class="label label-<?php echo $arr_lbl_status[$sp2d_entry['status']]; ?>">
						<?php echo lang('laporan:status_'.$sp2d_entry['status']); ?>
					</span>
				</td>
				<td>
				<?php 
				if(group_has_role('laporan', 'view_all_sp2d')){
					echo anchor('admin/laporan/sp2d/view/' . $sp2d_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-success view"');
				}elseif(group_has_role('laporan','view_own_sp2d')){
					if($this->curent_user->id == $sp2d_entry['created_by']){
						echo anchor('admin/laporan/sp2d/view/' . $sp2d_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-success view"');
					}
				}elseif(group_has_role('laporan','view_own_prov_sp2d')){
					if(user_provinsi($this->current_user->id) == $sp2d_entry['id_provinsi']){
						echo anchor('admin/laporan/sp2d/view/' . $sp2d_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-success view"');
					}
				}
				?>
				<?php 
				if(group_has_role('laporan', 'edit_all_sp2d')){
					echo anchor('admin/laporan/sp2d/edit/' . $sp2d_entry['id'] . $uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
				}elseif(group_has_role('laporan','edit_own_sp2d')){
					if($this->curent_user->id == $sp2d_entry['created_by']){
						if($sp2d_entry['status'] == 1){
							echo anchor('admin/laporan/sp2d/edit/' . $sp2d_entry['id'] . $uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
						}
					}
				}elseif(group_has_role('laporan','edit_own_prov_sp2d')){
					if(user_provinsi($this->current_user->id) == $sp2d_entry['id_provinsi']){
						if($sp2d_entry['status'] == 1){
							echo anchor('admin/laporan/sp2d/edit/' . $sp2d_entry['id'] . $uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
						}
					}
				}
				?>
				<?php 
				if(group_has_role('laporan', 'delete_all_sp2d')){
					echo anchor('admin/laporan/sp2d/delete/' . $sp2d_entry['id'] . $uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
				}elseif(group_has_role('laporan','delete_own_sp2d')){
					if($this->curent_user->id == $sp2d_entry['created_by']){
						if($sp2d_entry['status'] == 1){
							echo anchor('admin/laporan/sp2d/delete/' . $sp2d_entry['id'] . $uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
						}
					}
				}elseif(group_has_role('laporan','delete_own_prov_sp2d')){
					if(user_provinsi($this->current_user->id) == $sp2d_entry['id_provinsi']){
						if($sp2d_entry['status'] == 1){
							echo anchor('admin/laporan/sp2d/delete/' . $sp2d_entry['id'] . $uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
						}
					}
				}
				?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
  <?php echo $sp2d['pagination']; ?>
	
<?php else: ?>
	<div class="well"><?php echo lang('laporan:sp2d:no_entry'); ?></div>
<?php endif;?>