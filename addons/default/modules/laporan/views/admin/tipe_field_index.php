<div class="page-header">
	<h1><?php echo lang('laporan:tipe_field:plural'); ?></h1>
	
	<div class="btn-group content-toolbar">
		<?php if(group_has_role('laporan', 'manage_tipe_field')){ ?>
			<a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/laporan/tipe_field/create">
				<i class="icon-plus"></i>	
				<span class="no-text-shadow"><?php echo lang('laporan:tipe_field:new') ?></span>
			</a>
		<?php } ?>
	</div>
</div>
<?php if ($tipe_field['total'] > 0): ?>
	
	<p class="pull-right"><?php echo lang('laporan:showing').' '.count($tipe_field['entries']).' '.lang('laporan:of').' '.$tipe_field['total'] ?></p>
	
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="50">No</th>
				<th><?php echo lang('laporan:nama_tipe_field'); ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$no = 1;
			?>
			
			<?php foreach ($tipe_field['entries'] as $tipe_field_entry): ?>
			<tr>
				<td><?php echo $no++ ?></td>
				<td><?php echo $tipe_field_entry['nama_tipe_field']; ?></td>
				<td>
				<?php 
				if(group_has_role('laporan', 'manage_tipe_field')){
					echo anchor('admin/laporan/tipe_field/edit/' . $tipe_field_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
				}
				?>
				<?php 
				if(group_has_role('laporan', 'manage_tipe_field')){
					echo anchor('admin/laporan/tipe_field/delete/' . $tipe_field_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
				}
				?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
<?php else: ?>
	<div class="well"><?php echo lang('laporan:tipe_field:no_entry'); ?></div>
<?php endif;?>