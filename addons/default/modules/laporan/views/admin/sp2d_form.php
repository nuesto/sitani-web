<div class="page-header">
	<h1><?php echo lang('laporan:sp2d:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string() . $uri); ?>

<div class="form-horizontal">
	<?php if(group_has_role('laporan','create_sp2d') || group_has_role('laporan','create_own_prov_sp2d')) { ?>
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?><span style="color:red"> *</span></label>

			<div class="col-sm-6">
				<?php
          if(!group_has_role('laporan','create_sp2d')){ ?>
            <input type="text" value="<?php echo $nama_provinsi; ?>" readonly>
            <input type="hidden" name="id_provinsi" id="id_provinsi" value="<?php echo $id_provinsi; ?>" readonly>
            <?php
          }else{
						$val_prov = $id_provinsi;
						if($this->input->post('id_provinsi') != NULL){
							$val_prov = $this->input->post('id_provinsi');
						}elseif($mode == 'edit'){
							$val_prov = $fields['id_provinsi'];
						}
						?>
						<select name="id_provinsi" id="id_provinsi" class="col-xs-10 col-sm-5">
			    		<option value=""><?php echo lang('global:select-pick') ?></option>
			    		<?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
			    			<option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
			    		<?php } ?>
			    	</select>
			    	<?php
			    }
			  ?>
			</div>
		</div>
	<?php } ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_sp2d"><?php echo lang('laporan:nama_sp2d'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = $this->input->get('f-nama_sp2d');
				if($this->input->post('nama_sp2d') != NULL){
					$value = $this->input->post('nama_sp2d');
				}elseif($mode == 'edit'){
					$value = $fields['nama_sp2d'];
				}
			?>
			<input name="nama_sp2d" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-4" id="nama_sp2d" />

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="deskripsi"><?php echo lang('laporan:deskripsi'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}
			?>
			<textarea name="deskripsi" class="col-xs-10 col-sm-4"><?php echo $value ?></textarea>

		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="file"><?php echo lang('laporan:file'); ?><span style="color:red"> *</span></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('file') != NULL){
					$value = $this->input->post('file');
				}elseif($mode == 'edit'){
					$value = $fields['file'];
				}
			?>
			
			<input type="file" id="id-input-file-2" name="file">
			<span class="help-block">File yang diperbolehkan: jpg, jpeg, pdf. Maksimal ukuran file 1 Mb.</span>

			<?php if($mode == 'edit') { ?>
				<input type="hidden" name="old_file" value="<?php echo $value ?>">
			<?php } ?>

		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">
	jQuery(function($) {

		$('#id-input-file-2').ace_file_input({
			no_file:'No File ...',
			btn_choose:'Choose',
			btn_change:'Change',
			droppable:false,
			onchange:null,
			thumbnail:false //| true | large
			//whitelist:'gif|png|jpg|jpeg'
			//blacklist:'exe|php'
			//onchange:''
			//
		});
	});

</script>
