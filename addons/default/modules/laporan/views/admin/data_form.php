<div class="page-header">
	<h1><?php echo $title; ?></h1>
</div>

<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get')) ?>
	<div class="form-group">
		<label><?php echo lang('laporan:tipe_laporan'); ?></label>&nbsp;
		<?php
  		$value = null;
  		if($this->input->get('f-tipe_laporan') != ""){
  			$value = $this->input->get('f-tipe_laporan');
  		}

  		$id_tipe_laporan = $value;
  	?>
		<select name="f-tipe_laporan" onchange="this.form.submit()">
      <option value=""><?php echo lang('global:select-pick') ?></option>
      <?php 
        foreach ($tipes as $key => $tipe) { 
          if(in_array($tipe['id'], $type_ids) || group_has_role('laporan','create_laporan') || group_has_role('laporan','create_own_prov_laporan')){ ?>
            <option value="<?php echo $tipe['id'] ?>" <?php echo ($value == $tipe['id']) ? 'selected' : ''; ?>><?php echo $tipe['nama_laporan'] ?></option>
            <?php
          }
        } 
      ?>
    </select>
  </div>
  <hr>
<?php echo form_close(); ?>

<?php 
if($this->input->get('f-tipe_laporan')) { 

	if(count($absen) > 0 || $report_time_picker) { ?>
		<?php echo form_open_multipart(uri_string().'?'.$_SERVER['QUERY_STRING']); ?>
		<div class="form-horizontal">

			<!--- hidden id_user jika login sebagai pendamping -->
			<input type="hidden" name="id_user" value="<?php echo $this->current_user->id; ?>">

			<?php 
			if(group_has_role('laporan','create_laporan') || group_has_role('laporan','create_own_prov_laporan')) { ?>

				<!--- show location untuk user yg bisa manambahkan semua laporan -->
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?></label>

					<div class="col-sm-6">
						<?php
							// $val_prov = null;
								$val_prov = $id_provinsi;
              if(!group_has_role('laporan','view_all_laporan')){ ?>
                <input type="text" value="<?php echo $nama_provinsi; ?>" readonly>
                <input type="hidden" name="provinsi" id="provinsi" value="<?php echo $id_provinsi; ?>" readonly>
                <?php
              }else{
								if($this->input->post('provinsi') != NULL){
									$val_prov = $this->input->post('provinsi');
								}elseif($mode == 'edit'){
									$val_prov = $fields['id_provinsi'];
								}
								?>
								<select name="provinsi" id="provinsi" class="col-xs-10 col-sm-5">
					    		<option value=""><?php echo lang('global:select-pick') ?></option>
					    		<?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
					    			<option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
					    		<?php } ?>
					    	</select>
					    	<?php
					    }
					  ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="kota"><?php echo lang('location:kota:singular'); ?></label>

					<div class="col-sm-6">
						<?php 
							$val_kota = $id_kota;
							if($this->input->post('kota') != NULL){
								$val_kota = $this->input->post('kota');
							}elseif($mode == 'edit'){
								$val_kota = $fields['id_kota'];
							}
						?>
						<select name="kota" id="kota" class="col-xs-10 col-sm-5">
			    		<?php 
			    			if(count($kota['entries']) > 0) { ?>
			    				<option value=""><?php echo lang('global:select-pick') ?></option>
			    				<?php 
			      			foreach ($kota['entries'] as $kota_entry){ ?>
			      				<option value="<?php echo $kota_entry['id'] ?>" <?php echo ($val_kota == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
			      				<?php 
			      			} 
			      		}else{ ?>
			      			<option value=""><?php echo lang('global:select-none') ?></option>
			      			<?php
			      		}
			      	?>
			    	</select>
			    	<span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>

			    	<script type="text/javascript">
              $(document).ready(function(){
                <?php if($val_prov == NULL) { ?>
                  $("#provinsi").change();
                <?php } ?>

                <?php if($val_kota == NULL) { ?>
                  $("#kota").change();
                <?php } ?>
              });

              $('#provinsi').change(function() {
                $("#id_unit").html('<option value=""><?php echo  lang("global:select-none") ?></option>');
                var id_provinsi = $(this).val();
                $("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $.ajax({
                  url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                  dataType: 'json',
                  success: function(data){
                    if(data.length > 0){
                      $('#kota').html('<option value="">-- Pilih --</option>');
                    }else{
                      $('#kota').html('<option value="">-- Tidak ada --</option>');
                    }
                    $.each(data, function(i, object){
                      $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                    });

                    $("#kota").change();
                  	$('.loading-kota').html('');
                  }
                });
              });

              // khusu sitoni
              $('#kota').change(function() {
                var kota = ($(this).val() == '') ? 0 : $(this).val();
                var tipe_laporan = '<?php echo $this->input->get('f-tipe_laporan') ?>';
                tipe_laporan = (tipe_laporan == 2) ? 1 : tipe_laporan; 
                // tipe_laporan = (tipe_laporan == 7) ? 6 : tipe_laporan;
                $("#<?php echo $org_name ?>").html('<option value="-1"><?php echo  lang("global:ajax_load_data") ?></option>');
                $('.loading<?php echo $org_name ?>').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
                $("#<?php echo $org_name ?>").load("<?php echo site_url('admin/laporan/data/ajax_get_unit_by_kota') ?>" + '/' + kota + '/' + tipe_laporan, function(data) {
                  $("#<?php echo $org_name ?>").change();
                  $('.loading<?php echo $org_name ?>').html('');
                });
              });
            </script>
					</div>
				</div>
				<?php 
			}

			// Load Organization--------
     
	    $n = 0;
	    $addedLevel = array();

	    if(isset($organization['id_organization_unit']) && $organization['id_organization_unit'] != -1) { ?>
	      <input type="hidden" name="id_organization_unit" value="<?php echo $organization['id_organization_unit'] ?>">
	      <?php
	    }

	    foreach ($types as $key => $type) {
	        if (in_array($type['level'], $addedLevel)) {
	            continue;
	        }
	        $addedLevel[] = $type['level'];
	        $n++;
	        $types2[] = $type;
	        if($type['slug'] == $node_type_slug){
	            break;
	        }
	    }
	    $n2 = $n;
	    // dump($types);
	    // dump($types2);
	    foreach ($types2 as $i => $type) {

	      $parent_field_name = isset($field_name) ? $field_name : null;
	      $field_name = $i == $n - 1 ? 'id_organization_unit' : ('id_organization_unit_' . $type['level']);
	      ?>
	      <div class="form-group">
	        <label class="col-sm-3 control-label no-padding-right" for="<?php echo $field_name; ?>"><?php echo $type['name']; ?></label>
	        <div class="col-sm-9">
	          <?php
	          if(isset($organization[$field_name])){
	            $value = $organization[$field_name];
	          } else if($this->input->post($field_name)) {
	          	$value = $this->input->post($field_name);
	          }else{
	            $value = $this->input->get($field_name);
	          }
	          
	          $organization[$field_name] = $value != NULL ? $value : -1; 
	          $parent_lvl = $type['level'] - 1; 
	          
	          if(isset($organization['organization_name_'.$type['level']]) && !$is_tti17) { ?>
	            <div class="entry-value col-sm-5" id="unit-predefined-text"><?php echo $organization['organization_name_'.$type['level']]; ?></div>
	            <?php 
	          } else { 
	            // if(isset($organization['id_organization_unit_'.$parent_lvl]) && user_units($this->current_user->id) == $organization['id_organization_unit_'.$parent_lvl] || $field_name == 'id_organization_unit_0' || $n2 == 1){ ?>

	              <select id="<?php echo $field_name; ?>" name="<?php echo $field_name; ?>" class="col-xs-10 col-sm-5">
	              	<?php 
	              	if(count($child_organization) > 0) { ?>
		                <option value="-1"><?php echo lang('global:select-pick'); ?></option>
		                <?php        
		                foreach ($child_organization as $child) { 
		                  echo '<option value="' . $child['id'] . '"> ' . $child['unit_name'] . '</option>';
		                }
		              }else{
		              	echo '<option value="-1">'.lang('global:select-none').'</option>';
		              }
	                ?>  
	              </select>

	              <script>
	                $( document ).ready(function() {
	                  $("#<?php echo $field_name; ?>").val(<?php echo $organization[$field_name]; ?>).change();
	                });
	              </script>

	              <?php 
	            
	          }

	          if ($parent_field_name) { ?>
	            <script type="text/javascript">
	              $('#<?php echo $parent_field_name; ?>').change(function() {
	                var parent_id = $(this).val();
	                $("#<?php echo $field_name; ?>").html('<option><?php echo  lang("global:ajax_load_data") ?></option>');
	                $('.loading<?php echo $field_name ?>').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
	                $("#<?php echo $field_name; ?>").load("<?php echo site_url('laporan/data/ajax_unit_dropdown') ?>" + '/' + parent_id, function(data) {
	                  if(data != '<option value="-1">-- Tidak ada --</option>'){
	                      $(this).val(<?php echo $organization[$field_name]; ?>).change();
	                  }
	                  $('.loading<?php echo $field_name ?>').html('');
	                });
	              });
	            </script>
	            <?php 
	          } 
	          ?>
	          <span class="loading_unit loading<?php echo $field_name ?>"></span>
	        </div>
	      </div>
	      <?php
	    }
    ?> 

			<!--- Set ID Absen-->
    	<?php if(!$report_time_picker) { ?>
				<input type="hidden" value="<?php echo $absen['id'] ?>" name="id_absen">
    	<?php } ?>

			<!--- Set ID Tipe Laporan-->
			<input type="hidden" value="<?php echo $this->input->get('f-tipe_laporan') ?>" id="tipe_laporan" name="tipe_laporan">


			<?php if($report_time_picker) { ?>

		    <div class="form-group">
		      <label class="col-sm-3 control-label no-padding-right" for="periode"><?php echo lang('laporan:periode'); ?></label>
		      <div class="col-sm-9">
		        <?php
		          $value = $tahun;
		          if($this->input->post('tahun') != ""){
		            $value = $this->input->post('tahun');
		          }
		        ?>
		        <select name="tahun" id="tahun">
		          <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
		            <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
		          <?php } ?>
		        </select>
		        
		        &nbsp;
		        <?php
		          $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
		          $value = $bulan;
		          if(isset($_POST['bulan'])){
		            $value = $this->input->post('bulan');
		          }
		        ?>
		        <select name="bulan" id="bulan">
		          <option value=""><?php echo lang('global:select-pick') ?></option>
		          <?php foreach ($arr_month as $key => $month) { ?>
		            <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
		          <?php } ?>
		        </select>

		        &nbsp;
		        <?php
		          $value = $minggu_ke;
		          if(isset($_POST['minggu_ke'])){
		            $value = $this->input->post('minggu_ke');
		          }
		        ?>

		        <select name="minggu_ke" id="minggu_ke">
		          <option value=""><?php echo lang('global:select-pick') ?></option>
		          <?php
		            $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
		            for ($i=1;$i<=$count_of_week;$i++) { ?>
		              <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
		          <?php } ?>
		        </select>

		        <script type="text/javascript">
		          $('#tahun, #bulan').change(function() {
		            var thn = $('#tahun').val();
		            var bln = $('#bulan').val();
		            $("#minggu_ke").html('<option value="">...</option>');
		            $("#minggu_ke").load("<?php echo site_url('laporan/data/ajax_get_minggu_ke') ?>" + '/' + thn + '/' + bln, function(data) {
		              $('.loading-minggu_ke').html('');
		              $("#minggu_ke").change();
		            });
		          });
		        </script>

		        &nbsp;
		        <?php
		          $value = NULL;
		          if($this->input->post('hari') != ""){
		            $value = $this->input->post('hari');
		          }
		        ?>
		        <select name="hari" id="hari">
		          <?php if(count($days) > 0) { ?>
		            <option value="">- Hari -</option>
		            <?php
		              foreach($days as $day){ ?>
		                <option value="<?php echo $day ?>" <?php echo ($value == $day) ? 'selected' : ''; ?>><?php echo date_idr($day, 'l, d F Y', null) ?></option>
		                <?php 
		              }
		            }else{ ?>
		              <option value="">-- Tidak ada --</option>
		            <?php
		            } 
		          ?>
		        </select>

		        <script type="text/javascript">
		          $('#minggu_ke').change(function() {
		            var thn = $('#tahun').val();
		            var bln = $('#bulan').val();
		            var minggu_ke = $('#minggu_ke').val();
		            var tipe_laporan = $('#tipe_laporan').val();
		            $("#hari").html('<option value="">...</option>');
		            $('.loading-hari').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
		            $("#hari").load("<?php echo site_url('laporan/data/ajax_get_hari') ?>" + '/' + thn + '/' + bln + '/' +minggu_ke+ '/' +tipe_laporan, function(data) {
		              $('.loading-hari').html('');
		            });
		          });
		        </script>
		        <span class="loading-hari" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>
		      </div>
		    </div>
			<?php } ?>
			<!--- Metadata -->
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="value"><?php echo lang('laporan:komoditas') ?></label>

				<!--- komoditas -->
				<div class="col-sm-9">
					<?php 
						$value = ($this->input->get('f-komoditas')) ? $this->input->get('f-komoditas') : NULL;
						if($this->input->post('id_laporan_komoditas') != NULL){
							$value = $this->input->post('id_laporan_komoditas');
						}elseif($mode == 'edit'){
							$value = $fields['id_laporan_komoditas'];
						}
					?>
					<select name="id_laporan_komoditas"  class="col-xs-10 col-sm-2" id="komoditas" >
		    		<?php foreach ($komoditas as $key => $komoditas_entry) { ?>
		    			<option value="<?php echo $komoditas_entry['id'] ?>" <?php echo ($value == $komoditas_entry['id']) ? 'selected' : ''; ?>><?php echo $komoditas_entry['nama_komoditas'] ?></option>
		    		<?php } ?>
		    	</select>
		    	<span class="loading-komoditas" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>
				</div>
			</div>

			<script>
				$("#komoditas").change(function(){
					var id_komoditas = $(this).val();
					var id_tipe_laporan = "<?php echo $tipe_laporan ?>";
					$("#content_metadata").html('');
					$('.loading-komoditas').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
					$("#content_metadata").load("<?php echo site_url('admin/laporan/data/ajax_get_metadata_by_komoditas') ?>" + '/' + id_tipe_laporan+ '/' + id_komoditas, function(data) {
						$('.loading-komoditas').html('');
          });
				})
			</script>

			<!--- Metadata -->
			<div id="content_metadata">
				<?php foreach ($metadata as $key => $data) { ?>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" for="value"><?php echo $data['nama'] ?></label>

						<div class="col-sm-9">
							<?php 
								$value = NULL;
								if($this->input->post($data['field']) != NULL){
									$value = $this->input->post($data['field']);
								}elseif($mode == 'edit'){
									$value = $fields[$data['field']];
								}
							?>
							<input name="<?php echo $data['field'] ?>" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-2" id="" />

							<span class="help-inline col-xs-12 col-sm-8">
								<span class="middle"><?php echo $data['satuan'] ?> <?php if($data['nilai_minimal'] !=""){?>, minimal: <?php echo $data['nilai_minimal'] ?><?php } if($data['nilai_maksimal'] != "") { ?>, maksimal: <?php echo $data['nilai_maksimal'] ?><?php } ?> </span>
							</span>
						</div>
					</div>
				<?php } ?>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
					<?php if(!$this->session->userdata('id_enumerator')) { ?>
						<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
					<?php } ?>
				</div>
			</div>
		</div>	
		<?php echo form_close();?>

		<?php 
	}else{ ?>

		<style>
			td{
		    pointer-events: none !important;
	    	cursor: default;
			}
		</style>
		<div class="alert alert-danger">
			<?php echo  lang('laporan:data:not_available') ?>
		</div>
		<h4>Tanggal Input Harga:</h4>
		<div id="full-year" style="padding-top:8px;">
		</div>
		<script type="text/javascript">
			$('#full-year').multiDatesPicker({
				<?php if($addDates != '') { ?>
				addDates: [<?php echo $addDates ?>],
				<?php } ?>
				minDate: 0,
				numberOfMonths: [1,4],
				// defaultDate: '1/1/2016'
				defaultDate: '<?php echo date('m/d/Y') ?>'
			});
		</script>

		<?php 
	}
} 
?>
