<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo lang('laporan:sp2d:singular') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-search font-red"></i>
            <span class="caption-subject font-red bold uppercase"> Filter
            </span>
          </div>
        </div>
        <div class="portlet-body">
          <?php echo form_open(base_url().'laporan/sp2d/index', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-provinsi"><?php echo lang('location:provinsi:singular'); ?></label>
              <div class="col-sm-4">
                <?php
                  $value = null;
                  if($this->input->get('f-provinsi') != ""){
                    $value = $this->input->get('f-provinsi');
                  }
                ?>
                <select name="f-provinsi" class="form-control" id="provinsi">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                    <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($value == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-2 control-label no-padding-right"><?php echo lang('laporan:nama_sp2d'); ?></div>
              <div class="col-sm-4">
                <?php
                  $value = null;
                  if($this->input->get('f-nama_sp2d') != ""){
                    $value = $this->input->get('f-nama_sp2d');
                  }
                ?>
                <input type="text" class="form-control" name="f-nama_sp2d" value="<?php echo $value ?>">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2"></div>
              <div class="col-sm-4">
                <button href="<?php echo current_url() . '#'; ?>" class="btn btn-success" type="submit">
                  <i class="icon-ok"></i>
                  Filter
                </button>

                <a href="<?php echo site_url('laporan/sp2d/index'); ?>" class="btn btn-default">
                  <i class="icon-remove"></i>
                  Clear
                </a>
              </div>
            </div>
          <?php echo form_close() ?>
        </div>
      </div>
    </div>
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-table font-red"></i>
            <span class="caption-subject font-red bold uppercase"> <?php echo lang('laporan:sp2d:plural') ?></span>
          </div>
        </div>
        <div class="portlet-body">
          <?php if ($sp2d['total'] > 0): ?>
  
            <p class="pull-right"><?php echo lang('laporan:showing').' '.count($sp2d['entries']).' '.lang('laporan:of').' '.$sp2d['total'] ?></p>
            <div style="overflow-y: auto;clear: both">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="50">No</th>
                  <th><?php echo lang('location:provinsi:singular'); ?></th>
                  <th><?php echo lang('laporan:nama_sp2d'); ?></th>
                  <th><?php echo lang('laporan:created_by'); ?></th>
                  <th><?php echo lang('laporan:created_on'); ?></th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
                if($cur_page != 0){
                  $item_per_page = $pagination_config['per_page'];
                  $no = $cur_page + 1;
                }else{
                  $no = 1;
                }
                ?>
                
                <?php $arr_lbl_status = array(1=>'yellow',2=>'warning',3=>'success',4=>'danger'); ?>
                <?php foreach ($sp2d['entries'] as $sp2d_entry): ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $sp2d_entry['provinsi']; ?></td>
                  <td><?php echo $sp2d_entry['nama_sp2d']; ?></td>
                  <td><?php echo $sp2d_entry['display_name'] ?></td>
                  <td><?php echo $sp2d_entry['created_on']; ?></td>
                  <td><a href="<?php echo base_url() ?>files/download/<?php echo $sp2d_entry['file'] ?>" class="btn red-sunglo btn-xs"/><i class="fa fa-download"></i> Download</a></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            </div>
            <?php echo $sp2d['pagination']; ?>
            
          <?php else: ?>
            <div class="well"><?php echo lang('laporan:sp2d:no_entry'); ?></div>
          <?php endif;?>
        </div>
      </div>
    </div>
  </div>
</div>