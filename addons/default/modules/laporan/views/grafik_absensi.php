<style type="text/css">
  .filter{
    font-weight: 500; 
    margin-bottom: 25px; 
    font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif !important;
  }
</style>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo lang('laporan:grafik_absensi') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-table font-red"></i>
            <span class="caption-subject font-red bold uppercase"> <?php echo lang('laporan:grafik_absensi') ?></span>
          </div>
        </div>
        <div class="portlet-body">
          <div class="row">
            <div class="col-sm-6" style="border-right: 1px solid #eee; height: 400px;">
              <center><h4 class="filter" style="margin-top:20px;"> Filter Absen </h4></center>
              <?php echo form_open(base_url().'laporan/absen/grafik_absensi', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
                <div class="form-group">
                  <label class="col-sm-3 control-label no-padding-right" for="f-provinsi"><?php echo lang('location:provinsi:singular'); ?></label>
                  <div class="col-sm-8">
                    <?php
                      $value = null;
                      if($this->input->get('f-provinsi') != ""){
                        $value = $this->input->get('f-provinsi');
                      }
                    ?>
                    <select name="f-provinsi" class="form-control" id="provinsi">
                      <option value=""><?php echo lang('global:select-pick') ?></option>
                      <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                        <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($value == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label no-padding-right" for="f-kota"><?php echo lang('location:kota:singular'); ?></label>
                    <div class="col-sm-8">
                      <select name="f-kota" id="kota" class="form-control">
                        <?php 
                          if($this->input->get('f-provinsi') != '') {
                            $value = null;
                            if($this->input->get('f-kota') != ""){
                              $value = $this->input->get('f-kota');
                            }
                        ?>
                          <option value=""><?php echo lang('global:select-pick') ?></option>
                        <?php 
                            foreach ($kota['entries'] as $kota_entry){ ?>
                              <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                        <?php 
                            } 
                          }else{
                        ?>
                            <option value=""><?php echo lang('global:select-none') ?></option>
                        <?php
                          }
                        ?>
                      </select>

                      <script type="text/javascript">
                        $('#provinsi').change(function() {
                          var id_provinsi = $(this).val();
                          $("#kota").html('<option value=""><?php echo  lang("laporan:ajax_load_data") ?></option>');
                          
                          $.ajax({
                            url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                            dataType: 'json',
                            success: function(data){
                              if(data.length > 0){
                                $('#kota').html('<option value="">-- Pilih --</option>');
                              }else{
                                $('#kota').html('<option value="">-- Tidak ada --</option>');
                              }
                              $.each(data, function(i, object){
                                $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                              });
                            }
                          });
                        });
                      </script>
                    </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:periode'); ?></label>
                  <div class="col-sm-8">
                    <div class="row">
                      <div class="col-sm-3" style="padding-right: 0px;">    
                        <?php
                          $value = $thn;
                          if($this->input->get('f-thn') != ""){
                            $value = $this->input->get('f-thn');
                          }
                        ?>
                        <select name="f-thn" id="f-thn" class="form-control">
                          <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
                            <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-sm-5" style="padding-right: 0px;">
                        <?php
                          $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
                          $value = $bln;
                          if($this->input->get('f-bln') != ""){
                            $value = $this->input->get('f-bln');
                          }
                        ?>
                        <select name="f-bln" id="f-bln" class="form-control">
                          <?php 
                            foreach ($arr_month as $key => $month) { ?>
                              <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
                              <?php 
                            } 
                          ?>
                        </select>
                      </div>
                      <div class="col-sm-3" style="padding-right: 0px;">
                        <?php
                          $value = $minggu_ke;
                          if($this->input->get('f-minggu_ke') != ""){
                            $value = $this->input->get('f-minggu_ke');
                          }
                        ?>
                        <select name="f-minggu_ke" id="f-minggu_ke" class="form-control">
                          <?php
                            $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
                            for ($i=1;$i<=$end_week;$i++) { ?>
                              <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
                              <?php 
                            } 
                          ?>
                        </select>
                        <script type="text/javascript">
                          $('#f-thn, #f-bln').change(function() {
                            var thn = $('#f-thn').val();
                            var bln = $('#f-bln').val();
                            $("#f-minggu_ke").html('<option value="">...</option>');
                            $("#f-minggu_ke").load("<?php echo site_url('laporan/data/ajax_get_minggu_ke2') ?>" + '/' + thn + '/' + bln, function(data) {
                            });
                          });
                        </script>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-8">
                    <button href="<?php echo current_url() . '#'; ?>" class="btn btn-success" type="submit">
                      <i class="icon-ok"></i>
                      Filter
                    </button>

                    <a href="<?php echo site_url('laporan/absen/grafik_absensi'); ?>" class="btn btn-default">
                      <i class="icon-remove"></i>
                      Clear
                    </a>
                  </div>
                </div>
              <?php echo form_close() ?>
            </div>

            <div class="col-sm-6">
              <?php if(count($pendamping) > 0) { ?>
              <div id="container"></div>
              <?php }else{  ?>
                <div style="text-align:center; padding-top:100px;">Tidak ada data pendamping</div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url() ?>addons/default/modules/laporan/js/highcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>addons/default/modules/laporan/js/exporting.js" type="text/javascript"></script>
<script  type="text/javascript">
  $(function () {
    $('#container').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            text: 'Grafik Absensi Pelaporan <?php echo $get_location .''.$get_periode ?>'
        },

        xAxis: {
            categories: [<?php echo $type_names ?>]
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Jumlah Unit'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },

        series: [{
            name: 'Belum dilapor',
            data: [<?php echo $belum_lapor ?>],
            stack: 'gap',
            color: '#f86d6d'
        }, {
            name: 'Telah dilapor',
            data: [<?php echo $melapor ?>],
            stack: 'gap',
            color: '#a0ff7a'
        }]
    });
});
</script>