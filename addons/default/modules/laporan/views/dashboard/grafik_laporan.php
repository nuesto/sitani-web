<div id="statistik<?php echo $id ?>" <?php echo($id_komoditas == NULL ) ? 'style="min-height:600px"' : ''; ?>>
</div>
<script type="text/javascript">

  $(function () {
    $('#statistik<?php echo $id ?>').highcharts({
      title: {
        text: 'Perkembangan PUPM Melalui Kegiatan TTI',
        x: -20 //center
      },
      xAxis: {
        categories: [<?php echo $categories ?>],
        crosshair: true
      },
      yAxis: [{
        title: {
          text: 'Harga',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
        labels: {
          format: '{value} Rp',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        }
      },{
        title: {
          text: 'Volume',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
        labels: {
          format: '{value} Kg',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
        opposite: true
      }],
      tooltip: {
        shared: true
      },
      series: <?php echo $json_series ?>
    });
  });
</script>