<ul class="feeds">
    <?php foreach ($report as $key => $data) { ?>
    <li>
        <div class="col1">
            <div class="cont">
                <div class="cont-col1">
                    <?php 
                        if($data['channel'] == 'sms'){
                            $lbl = 'success';
                            $fa = 'fa-envelope-o';
                        }elseif($data['channel'] == 'web'){
                            $lbl = 'danger';
                            $fa = 'fa-desktop';
                        }elseif($data['channel'] == 'android'){
                            $lbl = 'info';
                            $fa = 'fa-android';
                        }
                    ?>
                    <div class="label label-sm label-<?php echo $lbl ?>">
                        <i class="fa <?php echo $fa ?>"></i>
                    </div>
                </div>
                <div class="cont-col2">
                    <div class="desc"> <?php echo $data['pendamping'] ?> telah melapor harga untuk <?php echo $data['nama_laporan'] ?> <?php echo $data['unit_name'] ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col2">
            <div class="date"> <?php echo humanTiming(strtotime($data['created_on'])); ?> </div>
        </div>
    </li>
    <?php } ?>
</ul>