<div class="tabbable-custom ">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="#tab_1" data-toggle="tab" aria-expanded="true">Gapoktan</a>
        </li>
    </ul>
    <div class="tab-content" style="overflow: auto;">
        <div class="tab-pane active" id="tab_1">
            <?php 
            if(count($data) > 0){ ?>
                <table class="table table-hover table-light">
                    <thead>
                        <tr class="uppercase">
                            <th>Komoditas</th>
                            <th style="text-align: right;">Terendah</th>
                            <th style="text-align: right;">Tertinggi</th>
                            <th style="text-align: right;">Rata-rata</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $key2 => $value) { ?>
                            <tr>
                                <td class="font-blue-sharp"><?php echo $value['nama'] ?></td>
                                <td style="text-align: right;">
                                    <div class="hasTooltip" style="cursor:pointer;">
                                        <?php echo number_format($value['min_val'],0,',','.') ?>
                                    </div>
                                </td>
                                <td style="text-align: right;">
                                    <div class="hasTooltip" style="cursor:pointer;">
                                        <?php echo number_format($value['max_val'],0,',','.') ?>
                                    </div>
                                </td>
                                <td>
                                    <a href="<?php echo base_url() ?>laporan/data/harga/1">
                                        <?php echo number_format($value['avg_val'],0,',','.') ?>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php 
            }else{ ?>
                <div class="well" style="text-align: left; margin-top:10px;">Tidak ada data</div>
                <?php 
            }
            ?>
        </div>
    </div>
</div>