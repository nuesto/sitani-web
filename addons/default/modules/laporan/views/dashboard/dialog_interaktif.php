<?php if(count($dialog) > 0) { ?>
    <?php foreach ($dialog as $key => $value) { ?>
        <div class="general-item-list">
            <div class="item">
                <div class="item-head">
                    <div class="item-details">
                        <span class="item-name primary-link"><?php echo $value['nama'] ?></span>
                    </div>
                    <span class="item-status" style="margin-top: -15px;"><small><?php echo date_idr($value['created_on'], 'd F Y', null) ?></small></span>
                </div>
                <div class="item-body"><?php echo $value['isi'] ?></div>
            </div>
        </div>
    <?php } ?>
<?php }else{ ?>
    <div class="well margin-top-10">Tidak ada data</div>
<?php } ?>

<form id="form_dialog" method="post" action="<?php echo base_url(); ?>laporan/dialog/ajax_add">
    <div class="modal fade" id="form-dialog" tabindex="-1" role="form-dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Form Dialog</h4>
                </div>
                <div class="modal-body">
                    <div id="form_dialog_result" style="line-height: normal;"></div>
                      <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
                      </div>
                      <div class="form-group">
                        <label for="email">Email Anda</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                      </div>
                      <div class="form-group">
                        <label for="kontak">Nomor Kontak</label>
                        <input type="text" class="form-control" id="kontak" name="kontak" placeholder="Kontak" onkeypress="return isNumberKey(event)" maxlength="13" required>
                        <!-- <input type="text" class="form-control" id="kontak" name="kontak" placeholder="Kontak"> -->
                      </div>
                      <div class="form-group">
                        <label for="isi">Isi Dialog</label>
                        <textarea name="isi" class="form-control" required></textarea>
                      </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn red-sunglo"><i class="fa fa-check"></i> Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</form>

<script type="text/javascript">

    $('#form-dialog').on('hidden.bs.modal', function (e) {
        $("#form_dialog_result").html('');
        $(this).find("input,textarea").val('').end();
    });

    $('#form_dialog').submit(function(e){
        e.preventDefault();
        $(".modal-footer .btn").toggleClass('disabled');
        $(".modal-header .close").attr('disabled','disabled');
        var form = $(this).attr('action');
        var serialData = $(this).serialize();
        $('#form_dialog_result').html("<center><i class='fa fa-spinner fa-pulse fa-2x'></i><center>");
        $.ajax({
            type: "POST",
            url: form,
            data: serialData,
            success: function(data){
                $('#form_dialog_result').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>' + data + '</div>');
                $(".modal-footer .btn").toggleClass('disabled',false);
                $(".modal-header .close").removeAttr('disabled');
                setTimeout(function() { $('#form-dialog').modal('hide'); },3000);
                $('#form-dialog').on('hidden.bs.modal', function (e) {
                    $("#form_dialog_result").html('');
                    $(this).find("input,textarea").val('').end();
                });      
            },
            error: function(data){
                $('#form_dialog_result').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>'+data.responseText+'</div>');
                $(".modal-footer .btn").toggleClass('disabled',false);
                $(".modal-header .close").removeAttr('disabled');
            }
        });
    }); 

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
        return true;
    }
</script>