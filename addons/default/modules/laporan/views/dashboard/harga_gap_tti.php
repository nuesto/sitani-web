<div class="tabbable-custom ">
    <ul class="nav nav-tabs ">
        <?php foreach ($tipe_laporan as $key => $tipe) { ?>
            <li class="<?php echo($key==1) ? 'active' : ''; ?>">
                <a href="#tab_<?php echo $key ?>" data-toggle="tab" aria-expanded="true"><?php echo $tipe ?></a>
            </li>
        <?php } ?>
    </ul>
    <div class="tab-content" style="overflow: auto;">
        <?PHP foreach ($tipe_laporan as $key => $tipe) { ?>
            <div class="tab-pane <?php echo ($key==1) ? 'active' : ''; ?>" id="tab_<?php echo $key ?>">
                <?php 
                if(isset($data[$key])) { 
                    if(count($data[$key]) > 0){ ?>
                        <table class="table table-hover table-light">
                            <thead>
                                <tr class="uppercase">
                                    <th>Komoditas</th>
                                    <th style="text-align: right;">Terendah</th>
                                    <th style="text-align: right;">Tertinggi</th>
                                    <th style="text-align: right;">Rata-rata</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($data[$key] as $key2 => $value) { ?>
                                    <tr>
                                        <td class="font-blue-sharp"><?php echo $value['nama'] ?></td>
                                        <td style="text-align: right;">
                                            <div class="hasTooltip" style="cursor:pointer;">
                                                <?php echo number_format($value['min_val'],0,',','.') ?>
                                            </div>
                                        </td>
                                        <td style="text-align: right;">
                                            <div class="hasTooltip" style="cursor:pointer;">
                                                <?php echo number_format($value['max_val'],0,',','.') ?>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() ?>laporan/data/harga/1">
                                                <?php echo number_format($value['avg_val'],0,',','.') ?>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php 
                    }else{ ?>
                        <div class="well" style="text-align: left; margin-top:10px;">Tidak ada data</div>
                        <?php 
                    }
                }else{ ?>
                    <div class="well" style="text-align: left; margin-top:10px;">Tidak ada data</div> 
                    <?php
                } 
                ?>
            </div>
        <?php } ?>
    </div>
</div>