<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>Laporan <?php echo lang('laporan:kinerja_pendamping') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-search font-red"></i>
            <span class="caption-subject font-red bold uppercase"> Filter
            </span>
          </div>
        </div>
        <div class="portlet-body">
          
          <?php
          $url = base_url()."laporan/data/kinerja_pendamping";
          echo form_open($url, array('class' => 'form-horizontal', 'method' => 'get')) ?>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-provinsi"><?php echo lang('laporan:pendamping'); ?></label>
              <div class="col-sm-4">
                <?php
                  $value = null;
                  if($this->input->get('f-groups') != ""){
                    $value = $this->input->get('f-groups');
                  }

                  $id_groups = $value;
                ?>
                <select name="f-groups" onchange="this.form.submit('')" class="form-control">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php 
                  if ($id_groups == '7'){
                    echo "<option selected value='7'>Pendamping 2016</option>";
                  } else {
                    echo "<option value='7'>Pendamping 2016</option>";
                  }
                  if ($id_groups == '9'){
                    echo "<option selected value='9'>Pendamping 2017</option>";
                  } else {
                    echo "<option value='9'>Pendamping 2017</option>";
                  }
                  if ($id_groups == '10'){
                    echo "<option selected value='10'>Pendamping 2018</option>";
                  } else {
                    echo "<option value='10'>Pendamping 2018</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
          <?php echo form_close(); ?>
          <?php if($this->input->get('f-groups')) { ?>
            <?php echo form_open(base_url().'laporan/data/kinerja_pendamping', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
              <input type="hidden" value="<?php echo $id_groups ?>" name="f-groups">
              <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="f-provinsi"><?php echo lang('location:provinsi:singular'); ?></label>
                <div class="col-sm-4">
                  <?php
                    $value = null;
                    if($this->input->get('f-provinsi') != ""){
                      $value = $this->input->get('f-provinsi');
                    }
                  ?>
                  <select name="f-provinsi" class="form-control" id="provinsi">
                    <option value=""><?php echo lang('global:select-pick') ?></option>
                    <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                      <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($value == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="f-kota"><?php echo lang('location:kota:singular'); ?></label>
                  <div class="col-sm-4">
                    <select name="f-kota" id="kota" class="form-control">
                      <?php 
                        if($this->input->get('f-provinsi') != '') {
                          $value = null;
                          if($this->input->get('f-kota') != ""){
                            $value = $this->input->get('f-kota');
                          }
                      ?>
                        <option value=""><?php echo lang('global:select-pick') ?></option>
                      <?php 
                          foreach ($kota['entries'] as $kota_entry){ ?>
                            <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                      <?php 
                          } 
                        }else{
                      ?>
                          <option value=""><?php echo lang('global:select-none') ?></option>
                      <?php
                        }
                      ?>
                    </select>

                    <script type="text/javascript">
                      $('#provinsi').change(function() {
                        var id_provinsi = $(this).val();
                        $("#kota").html('<option value=""><?php echo  lang("laporan:ajax_load_data") ?></option>');
                        
                        $.ajax({
                          url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                          dataType: 'json',
                          success: function(data){
                            if(data.length > 0){
                              $('#kota').html('<option value="">-- Pilih --</option>');
                            }else{
                              $('#kota').html('<option value="">-- Tidak ada --</option>');
                            }
                            $.each(data, function(i, object){
                              $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                            });
                          }
                        });
                      });
                    </script>
                  </div>
              </div>
              <?php if ($id_groups == '9') { ?>
                <div class="form-group">
                  <label class="col-sm-2 control-label no-padding-right" ><?php echo lang('laporan:tipe_pendamping'); ?></label>&nbsp;
                  <?php
                    $value = null;
                    if($this->input->get('f-tipe_unit') != ""){
                      $value = $this->input->get('f-tipe_unit');
                    }
                  ?>
                  <div class="col-sm-2">
                    <select name="f-tipe_unit" class="form-control">
                      <option value=""><?php echo lang('global:select-pick') ?></option>
                      <option value="4" <?php echo ($value == 4) ? 'selected' : ''; ?>>Gapoktan 2017</option>
                      <option value="5" <?php echo ($value == 5) ? 'selected' : ''; ?>>TTI 2017</option>
                    </select>
                  </div>
                </div>
              <?php } ?>
              <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:periode'); ?></label>
                <div class="col-sm-4">
                  <div class="row">
                    <div class="col-sm-3" style="padding-right: 0px;">    
                      <?php
                        $value = $tahun;
                        if($this->input->get('f-tahun') != ""){
                          $value = $this->input->get('f-tahun');
                        }
                      ?>
                      <select name="f-tahun" id="f-tahun" class="form-control">
                        <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
                          <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-sm-5" style="padding-right: 0px;">
                      <?php
                        $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
                        $value = $bulan;
                        if($this->input->get('f-bulan') != ""){
                          $value = $this->input->get('f-bulan');
                        }
                      ?>
                      <select name="f-bulan" id="f-bulan" class="form-control">
                        <?php 
                          foreach ($arr_month as $key => $month) { ?>
                            <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
                            <?php 
                          } 
                        ?>
                      </select>
                    </div>
                    <div class="col-sm-3" style="padding-right: 0px;">
                      <?php
                        $value = $minggu_ke;
                        if($this->input->get('f-minggu_ke') != ""){
                          $value = $this->input->get('f-minggu_ke');
                        }
                      ?>
                      <select name="f-minggu_ke" id="f-minggu_ke" class="form-control">
                        <?php
                          $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
                          for ($i=1;$i<=$count_of_week;$i++) { ?>
                            <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
                            <?php 
                          } 
                        ?>
                      </select>
                      <script type="text/javascript">
                        $('#f-tahun, #f-bulan').change(function() {
                          var tahun = $('#f-tahun').val();
                          var bulan = $('#f-bulan').val();
                          $("#f-minggu_ke").html('<option value="">...</option>');
                          $("#f-minggu_ke").load("<?php echo site_url('laporan/data/ajax_get_minggu_ke2') ?>" + '/' + tahun + '/' + bulan, function(data) {
                          });
                        });
                      </script>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-4">
                  <button href="<?php echo current_url() . '#'; ?>" class="btn btn-success" type="submit">
                    <i class="icon-ok"></i>
                    Filter
                  </button>

                  <a href="<?php echo site_url('laporan/data/kinerja_pendamping'); ?>" class="btn btn-default">
                    <i class="icon-remove"></i>
                    Clear
                  </a>
                </div>
              </div>
            <?php echo form_close() ?>
          <?php } ?>
        </div>
      </div>
    </div>

    <?php if($this->input->get('f-groups')) { ?>
      <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-table font-red"></i>
            <span class="caption-subject font-red bold uppercase">Laporan Kinerja <small>( <?php if($firstDayOfWeek == NULL){ echo lang('laporan:absen:no_absen'); }else{ echo date_idr($weeks['first_day_of_week'], 'd F Y', null); ?> - <?php echo date_idr($weeks['last_day_of_week'], 'd F Y', null); }?> )</small></span>
          </div>
        </div>
        <div class="portlet-body" style="overflow-y: auto;">
          <p class="pull-right"><?php echo lang('laporan:showing').' '.count($pendamping['entries']).' '.lang('laporan:of').' '.$pendamping['total'] ?></p>
          <?php if ($pendamping['total'] > 0){ ?>
            <?php 
            $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
            if($cur_page != 0){
              $item_per_page = $pagination_config['per_page'];
              $no = $cur_page + 1;
            }else{
              $no = 1;
            }
            ?>
            
            <?php 
            $jml_metadata_gap  = count($metadata_gap);
            $jml_metadata_tti  = count($metadata_tti);
            $cols_all = $jml_metadata_gap+$jml_metadata_tti+5; ?>

            <?php if($id_groups == '7' || $id_groups == '10'){
              foreach ($pendamping['entries'] as $pendamping_entry): 

                $rows = ($pendamping_entry['total_gap'] + $pendamping_entry['total_tti']) - 1;
                ?>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th colspan="<?php echo $cols_all ?>">
                        <center>
                          Provinsi <?php echo $pendamping_entry['provinsi'] ?><br>
                          <?php echo $pendamping_entry['kota'] ?>
                        </center>
                      </th>
                    </tr>
                    <tr>
                      <th>Pendamping</th>
                      <th>Gapoktan</th>
                      <th colspan="<?php echo $jml_metadata_gap+1; ?>">Metadata</th>
                      <th>TTI</th>
                      <th colspan="<?php echo $jml_metadata_tti+1; ?>">Metadata</th>
                    </tr>
                    <tr>
                      <th>Nama</th>
                      <th>Nama</th>
                      <?php foreach ($metadata_gap as $key => $metadata) { ?>
                        <th><?php echo $metadata['field'] ?></th>
                      <?php } ?>
                      <th>Grafik</th>
                      <th>Nama</th>
                      <?php foreach ($metadata_tti as $key => $metadata) { ?>
                        <th><?php echo $metadata['field'] ?></th>
                      <?php } ?>
                      <th>Grafik</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php 
                      foreach ($pendamping_entry['gapoktan'] as $key2 => $gap) { 
                        $rows2 = count($gap['tti']);
                        $telp = substr($gap['telp'], 0, -4);
                        $hp_ketua = substr($gap['hp_ketua'], 0, -4);
                        ?>
                        <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['display_name'] ?></td>
                        <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><?php echo $gap['unit_name'] ?></td>
                        <?php
                          $arr_metadata_gap = $pendamping_entry['metadata_gapoktan'];
                          foreach ($metadata_gap as $keym => $metadata) {
                            if(isset($arr_metadata_gap[$metadata['id']])){ ?>
                              <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>>
                                <?php echo number_format($arr_metadata_gap[$metadata['id']]['total'], 0); ?>  
                              </td>
                              <?php
                            }else{ ?>
                              <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>>-</td>
                              <?php
                            } 
                          }
                        ?>
                        <td <?php echo ($rows2 > 1) ? 'rowspan="'.$rows2.'"' : ''; ?>><button class="btn red-sunglo btn-xs" onclick="show_graph('<?php echo $gap['unit_type'] ?>', '<?php echo $gap['id_unit'] ?>')"><i class="fa fa-line-chart"></i></button></td>
                        <?php
                        if(count($gap['tti']) > 0){
                          foreach ($gap['tti'] as $key3 => $tti) { 
                            if($key3 > 0){ ?>
                              <tr>
                              <?php
                            }

                            $hp_ketua = substr($tti['hp_ketua'], 0, -4);
                            ?>
                            <td><?php echo $tti['unit_name']; ?></td>
                            <?php
                              $arr_metadata_tti = $tti['metadata_tti'];
                              foreach ($metadata_tti as $keym => $metadata) {
                                if(isset($arr_metadata_tti[$metadata['id']])){ ?>
                                  <td>
                                    <?php echo number_format($arr_metadata_tti[$metadata['id']]['total'], 0); ?>  
                                  </td>
                                  <?php
                                }else{ ?>
                                  <td>-</td>
                                  <?php
                                } 
                              }
                            ?>
                            <td><button class="btn red-sunglo btn-xs" onclick="show_graph(2,'<?php echo $tti['id_unit'] ?>')"><i class="fa fa-line-chart"></i></button></td>
                            <?php
                            if($key3 > 0 || count($gap['tti']) == 1){ ?>
                              </tr>
                              <?php
                            }
                          }
                        }else{ ?>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          </tr>
                          <?php
                        }
                        ?>
                      <?php
                        if($key2 > 0){ ?>
                          </tr>
                          <?php
                        } 
                      } 
                    ?>
                    </tr>
                  </tbody>
                </table>
              <?php endforeach; ?>
              <?php echo $pendamping['pagination']; ?>
            
            <?php }else{ ?>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tipe</th>
                    <th>Provinsi</th>
                    <th>Kota</th>
                    <th>Nama Pendamping</th>
                    <th>No HP Pendamping</th>
                    <th>Nama Unit</th>
                    <th>Ketua</th>
                    <th>Alamat</th>
                    <th>No HP Ketua</th>
                    <th>Grafik</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                    $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
                    if($cur_page != 0){
                    $item_per_page = $pagination_config['per_page'];
                    $no = (($cur_page -1) * $item_per_page) + 1;
                    }else{
                    $no = 1;
                    }
                    ?>
                <?php 
                  foreach ($pendamping['entries'] as $pendamping_entry) {
                  ?>
                  <tr>
                      <td><?php echo $no; $no++; ?></td>
                      <td><?php echo $pendamping_entry['type_name'] ?></td>
                      <td><?php echo $pendamping_entry['provinsi'] ?></td>
                      <td><?php echo $pendamping_entry['kota'] ?></td>
                      <td><?php echo $pendamping_entry['display_name'] ?></td>
                      <td><?php echo $pendamping_entry['telp'] ?></td>
                      <td><?php echo $pendamping_entry['unit_name'] ?></td>
                      <td><?php echo $pendamping_entry['nama_ketua'] ?></td>
                      <td><?php echo $pendamping_entry['unit_description'] ?></td>
                      <td><?php echo $pendamping_entry['hp_ketua'] ?></td>
                      <td><button class="btn red-sunglo btn-xs" onclick="show_graph('<?php echo $pendamping_entry['unit_type'] ?>', '<?php echo $pendamping_entry['id_unit'] ?>')"><i class="fa fa-line-chart"></i></button></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              <?php echo $pendamping['pagination']; ?>
            <?php } ?>
          <?php }else{ ?>
              <?php echo lang('laporan:pendamping:no_entry'); ?>
          <?php } ?>
        </div>
      </div>
      </div>
    <?php } ?>
  </div>
</div>

<div id="content-modal">

</div>

<script src="<?php echo base_url() ?>addons/default/modules/laporan/js/highcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>addons/default/modules/laporan/js/exporting.js" type="text/javascript"></script>
<script>
  function show_graph(id_tipe, id_unit){
    var minggu_ke = '<?php echo $minggu_ke ?>';
    var tahun_bulan = '<?php echo $thn_bln ?>';
    $("#content-modal").load("<?php echo site_url('laporan/data/grafik_laporan') ?>" + '/' + id_tipe + '/1/' + id_unit + '/' + minggu_ke + '/' + tahun_bulan, function() {
      // $.isLoading( "hide" );
      $('#myModal').modal({
          backdrop: 'static',
          keyboard: false
      });
      $("#myModal").modal('show');
      $('#myModal').on('hidden.bs.modal', function (e) {
        $("#content-modal").html("");
      });
    });
  }
</script>