<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo lang('laporan:input_sms') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="row">
      <div class="col-lg-12 col-md-12">
        <label>Pilih Komoditas :</label>
                  <?php echo form_open('', array('method' => 'get')) ?>
                  <div class="form-group">
                    <?php
                      $value = null;
                        if($this->input->get('f-komoditas') != ""){
                          $value = $this->input->get('f-komoditas');
                        }
                    ?>

                    <select name="f-komoditas" class="form-control" onchange="this.form.submit()">
                    <?php foreach ($data_komoditas as $komoditas_entry) {
                    ?>
                      <option value='<?php echo $komoditas_entry['id'];?>' <?php echo ($value == $komoditas_entry['id']) ? 'selected' : ''; ?>><?php echo $komoditas_entry['nama_komoditas']; ?></option>
                    <?php } ?>
                    </select>
                  </div>
                  <?php echo form_close();?>
        <hr>
      </div>

        <?php foreach ($metadatas as $kode_sms => $metadata) { ?>
          <div class="col-md-6 col-sm-6">
            <div class="portlet light portlet-fit ">
              <div class="portlet-title">
                <span class="caption-subject bold font-red uppercase"> Format SMS <?php echo $laporan[$kode_sms] ?></span>
              </div>
              <div class="portlet-body">
                <?php
                  $format_tgl = date('dmy');
                  $cthhrg = 2000;
                  $cthstok = 23;
                  $jml_metadata = count($metadata);
                  $arr_cth = array();
                  $arr_field = array();
                  foreach ($metadata as $key => $metadata_entry){
                    $nilai_min = ($metadata_entry['nilai_minimal'] != "") ? $metadata_entry['nilai_minimal'] : 0;
                    $nilai_max = ($metadata_entry['nilai_maksimal'] != "") ? $metadata_entry['nilai_maksimal'] : 1000000;

                    $tipe_field = $metadata_entry['nama_tipe_field'];

                    if($tipe_field == 'Harga'){
                      $cthhrg = $cthhrg + 700;
                      $contoh = $cthhrg;
                    }else{
                      $cthstok = $cthstok + 8;
                      $contoh = $cthstok;
                    }

                    $arr_cth[] = ($key == 7) ? $contoh.'<br>' : $contoh;
                    $arr_field[] = $metadata_entry['field'];

                  }

                  $default_code = $kode_sms;
                  $kode_sms_default = $kode_sms;

                  if($kode_sms == 'TTI'){
                    $kode_sms.= ' <span style="color: #e7505a;">&lt;strip&gt;</span> Kode TTI';
                    $kode_sms_default.='-A';
                  } else if($kode_sms == 'TTIC'){
                    $kode_sms.= ' <span style="color: #e7505a;">&lt;strip&gt;</span> Kode TTIC';
                    $kode_sms_default.='-A';
                  }

                  $format = implode('<span style="color: #337ab7;"> &lt;pagar&gt;</span>',$arr_field);
                  $format = $kode_sms.'<span style="color: #e7505a;"> &lt;spasi&gt; </span> '.$kode_komoditas['kode_komoditas'].' <span style="color: #e7505a;"> &lt;spasi&gt; </span>Tanggal<span style="color: #337ab7;"> &lt;pagar&gt; </span>'.$format;
                  $contoh_format = implode('<span style="color: #337ab7;">#</span>',$arr_cth);
                  $contoh_format = $kode_sms_default.' '.$kode_komoditas['kode_komoditas'].' '.$format_tgl.'<span style="color: #337ab7;">#</span>'.$contoh_format;
                ?>

                Ketik:<br>
                PUPM <?php echo $format ?><br><br>

                Contoh:<br>
                <div style="word-wrap: break-word;">PUPM <?php echo $contoh_format ?></div><br>

                Kirim ke: <span style="color: #337ab7;"><?php echo $nohp; ?></span>
                <hr>

                <?php if($default_code == 'TTI') { ?>
                  <h5>Cek Kode <?php echo $default_code; ?> :</h5>
                  <div class="input-group" style="width:60%">
                    <input type="text" class="form-control" id="telp" placeholder="Masukkan No HP">
                    <span class="input-group-btn">
                        <button class="btn red" type="button" id="btn-src" onclick="get_kode_tti_by_telp()"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                  <div id="daftar_tti">

                  </div>
                  <hr>
                <?php } ?>

                <?php if($default_code == 'TTIC') { ?>
                  <h5>Cek Kode Gapoktan :</h5>
                  <div class="input-group" style="width:60%">
                    <input type="text" class="form-control" id="telpon" placeholder="Masukkan No HP">
                    <span class="input-group-btn">
                        <button class="btn red" type="button" id="btn-src2" onclick="get_kode_ttic_by_telp()"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                  <div id="daftar_ttic">

                  </div>
                  <hr>
                <?php } ?>

                <h4>Keterangan:</h4>
                <table class="table table-striped table-borderd table-hover">
                  <tbody>
                    <tr>
                      <td>Tanggal</td>
                      <td> : </td>
                      <td>Format hhbbtt (h-hari b-bulan t-tahun), 6 digit angka, <br>contoh: <b><?php echo $format_tgl; ?></b> (<?php echo date_idr(date('Y-m-d'), 'd F Y', NULL) ?>)</td>
                    </tr>
                    <tr>
                    <?php 
                      $cthhrg = 2000;
                      $cthstok = 23;
                      $jml_metadata = count($metadata);
                      foreach ($metadata as $metadata_entry){
                        $nilai_min = ($metadata_entry['nilai_minimal'] != "") ? $metadata_entry['nilai_minimal'] : 0;
                        $nilai_max = ($metadata_entry['nilai_maksimal'] != "") ? $metadata_entry['nilai_maksimal'] : 1000000;

                        $tipe_field = $metadata_entry['nama_tipe_field'];

                        if($tipe_field == 'Harga'){
                          $cthhrg = $cthhrg + 700;
                          $contoh = $cthhrg;
                        }else{
                          $cthstok = $cthstok + 8;
                          $contoh = $cthstok;
                        }
                    ?>
                      <tr>
                        <td><?php echo $metadata_entry['field']; ?></td>
                        <td>:</td>
                        <td>
                          <b><?php echo $metadata_entry['nama'] ?></b>, dalam 
                          <?php echo $metadata_entry['satuan'] ?>, bilangan bulat, 
                          <br>nilai min <?php echo $nilai_min ?>, 
                          nilai maks <?php echo $nilai_max ?>, 
                          <br>contoh: <?php echo $contoh; ?>
                        </td>
                      </tr>
                    <?php
                      } 
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

<script>
  function get_kode_tti_by_telp(){
    var telp = $("#telp").val();
    if(telp != ""){
      $('#daftar_tti').html('');
      $('#btn-src').html('<i class="fa fa-spinner fa-spin"></i>');
      $.ajax({
        url: "<?php echo site_url('laporan/data/ajax_get_kode_tti_by_telp/') ?>" + '/' + telp,
        dataType: 'json',
        success: function(data){
          $('#daftar_tti').append('<br>');
          if(data.length > 0){
            console.log(data);
            var table = '<table class="table table-striped table-bordered table-hover"><thead><tr>';
            table += '<th>Provinsi</th>';
            table += '<th>Kota</td>';
            table += '<th>Nama TTI</th>';
            table += '<th>Kode TTI</th></thead><tbody>';
             $.each(data, function(i, object){
              table += '<tr><td>'+object['provinsi']+'</td>';
              table += '<td>'+object['kota']+'</td>';
              table += '<td>'+object['unit_name']+'</td>';
              table += '<td>'+object['kode_tti']+'</td></tr>';
            });
            table += '</tbody></table>';
            $('#daftar_tti').append(table);
          }else{
            $('#daftar_tti').append('Data tidak ditemukan.');
          }
          $('#btn-src').html('<i class="fa fa-search"></i>');
        }
      });
    }else{
      $('#daftar_tti').html('Masukkan no hp terlebih dahulu');
    }
  }

  function get_kode_ttic_by_telp(){
    var telp = $("#telpon").val();
    if(telp != ""){
      $('#daftar_ttic').html('');
      $('#btn-src2').html('<i class="fa fa-spinner fa-spin"></i>');
      $.ajax({
        url: "<?php echo site_url('laporan/data/ajax_get_kode_ttic_by_telp/') ?>" + '/' + telp,
        dataType: 'json',
        success: function(data){
          $('#daftar_ttic').append('<br>');
          if(data.length > 0){
            console.log(data);
            var table = '<table class="table table-striped table-bordered table-hover"><thead><tr>';
            table += '<th>Provinsi</th>';
            table += '<th>Kota</td>';
            table += '<th>Nama TTIC</th>';
            table += '<th>Kode TTIC</th></thead><tbody>';
             $.each(data, function(i, object){
              table += '<tr><td>'+object['provinsi']+'</td>';
              table += '<td>'+object['kota']+'</td>';
              table += '<td>'+object['unit_name']+'</td>';
              table += '<td>'+object['kode_ttic']+'</td></tr>';
            });
            table += '</tbody></table>';
            $('#daftar_ttic').append(table);
          }else{
            $('#daftar_ttic').append('Data tidak ditemukan.');
          }
          $('#btn-src2').html('<i class="fa fa-search"></i>');
        }
      });
    }else{
      $('#daftar_ttic').html('Masukkan no hp terlebih dahulu');
    }
  }
</script>