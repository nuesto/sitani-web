<style type="text/css">
.portlet-body>.img{
  max-width: 100%;
}
</style>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo lang('laporan:input_android') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
      <div class="col-sm-3">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase">1. Buka Aplikasi Play Store<br><br></span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/1.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase">2. Cari dan install Aplikasi "SITANI KEMENTAN"</span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/2.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase">3. Klik "Input Laporan"<br><br></span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/3.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase">4. Login<br><br></span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/4.png" class="img">
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-sm-3">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase">5. Pilih laporan yang akan diinput</span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/5.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase">6. Masukan Nilai pada Form input Gapoktan</span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/6.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase">7. Form input jika tipe laporannya TTI</span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/7.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase">8. Pilih TTI<br><br></span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/8.png" class="img">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>