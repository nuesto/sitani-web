<style>
  /** entry details **/
.entry-detail-row {
  position: relative;
}
.entry-detail-row:first-child .entry-detail-name {
  border-top: none;
}
.entry-detail-row:first-child .entry-detail-value {
  border-top: none;
}
.entry-detail-value {
  padding: 6px 4px 6px 6px;
  margin-left: 200px;
  border-top: 1px dotted #D5E4F1;
}
.entry-detail-value > span + span:before {
  /* for a list of values (such as location city & country) put a comma between them */

  display: inline;
  content: ",";
  margin-left: 1px;
  margin-right: 3px;
  color: #666;
  border-bottom: 1px solid #FFF;
}
.entry-detail-value > span + span.editable-container:before {
  display: none;
}
.entry-detail-name {
  position: absolute;
  width: 190px;
  text-align: right;
  padding: 6px 10px 6px 0;
  left: 0;
  top: 0;
  bottom: 0;
  font-weight: normal;
  color: #667E99;
  background-color: transparent;
  border-top: 1px dotted #D5E4F1;
}
@media only screen and (max-width: 480px) {
  .entry-detail-name {
    width: 80px;
  }
  .entry-detail-value {
    margin-left: 90px;
  }
}

</style>

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo lang('laporan:sumberdata:view') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-body">
          <div class="detail_body">
            <div class="entry-detail-row">
              <div class="entry-detail-name">Penerima Kegiatan PUPM</div>
              <?php if($units->unit_name){ ?>
              <div class="entry-detail-value"><?php echo $units->unit_name; ?></div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div>

            <div class="entry-detail-row">
              <div class="entry-detail-name"><?php echo lang('organization:nama_ketua'); ?></div>
              <?php if($units->nama_ketua){ ?>
              <div class="entry-detail-value"><?php echo $units->nama_ketua; ?></div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div>

            <div class="entry-detail-row">
              <div class="entry-detail-name">HP Ketua</div>
              <?php if($units->hp_ketua){ 
                $nohp = substr($units->hp_ketua, 0, -4);
              ?>
              <div class="entry-detail-value"><?php echo $nohp; ?>xxxx</div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div>
              
            <div class="entry-detail-row">
              <div class="entry-detail-name"><?php echo lang('organization:alamat'); ?></div>
              <?php if($units->unit_description){ ?>
              <div class="entry-detail-value"><?php echo $units->unit_description; ?></div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div>

            <div class="entry-detail-row">
              <div class="entry-detail-name"><?php echo lang('organization:unit_type'); ?></div>
              <?php if($units->unit_type){ ?>
              <div class="entry-detail-value"><?php echo $units->unit_type->type_name; ?></div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div>

            <div class="entry-detail-row">
              <div class="entry-detail-name">Provinsi</div>
              <?php if($units->provinsi){ ?>
              <div class="entry-detail-value"><?php echo $units->provinsi; ?></div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div>

            <div class="entry-detail-row">
              <div class="entry-detail-name">Kota</div>
              <?php if($units->kota){ ?>
              <div class="entry-detail-value"><?php echo $units->kota; ?></div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div>

            <div class="entry-detail-row">
              <div class="entry-detail-name">Pendamping</div>
              <?php if($units->nama_pendamping){ ?>
              <div class="entry-detail-value"><?php echo $units->nama_pendamping; ?></div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div>

            <!-- <div class="entry-detail-row">
              <div class="entry-detail-name">HP Pendamping</div>
              <?php if($units->no_pendamping){ 
                $no2 = substr($units->no_pendamping, 0, -4);
              ?>
              <div class="entry-detail-value"><?php echo $units->no_pendamping; ?></div>
              <?php }else{ ?>
              <div class="entry-detail-value">-</div>
              <?php } ?>
            </div> -->

            <?php if($units->unit_type->id == 2) { ?>
              <div class="entry-detail-row">
                <div class="entry-detail-name"><?php echo lang('organization:unit_parents'); ?></div>
                <?php if($units->unit_parents){ ?>
                <?php
                $parent_str_arr = array();
                foreach($units->unit_parents as $unit_parent){
                  $parent_str_arr[] = '<a href="'.site_url('laporan/sumberdata/view/'.$unit_parent['id']).'">'.$unit_parent['unit_name'].'</a>';
                }
                ?>
                <div class="entry-detail-value"><?php echo implode(', ', $parent_str_arr); ?></div>
                <?php }else{ ?>
                <div class="entry-detail-value">-</div>
                <?php } ?>
              </div>
            <?php } ?>

            <div class="entry-detail-row">
              <div class="entry-detail-name"></div>
              <div class="entry-detail-value">
              <br>
              <a href="<?php echo base_url() ?>laporan/sumberdata/index" class="btn btn-danger btn-sm">Kembali</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>