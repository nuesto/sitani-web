<style type="text/css">
.portlet-body>.img{
  max-width: 100%;
}
</style>
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo lang('laporan:input_web') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
				<div class="portlet-title">
					<span class="caption-subject bold font-red uppercase"> 1. Klik Tombol Login pada ujung atas halaman website</span>
				</div>
				<div class="portlet-body text-center">
          <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/login.png" class="img">
				</div>
			</div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase"> 2. Cekis Tombol Login</span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/login_ceklis.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase"> 3. Masukkan No HP dan Tekan Tombol "Masuk"</span>
            </div>
            <div class="portlet-body text-center" style="height:276px;">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/login_entry.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase"> 4. Klik Menu "Laporan"</span>
            </div>
            <div class="portlet-body text-center" style="height:291px;">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/pick_laporan.png" class="img">
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase"> 5. Pilih "Input Laporan"</span>
            </div>
            <div class="portlet-body text-center" style="height:240px;">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/pick_input.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase"> 6. Pilih Tipe Laporan</span>
            </div>
            <div class="portlet-body text-center" style="height:239px;">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/pick_tipe.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit ">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase"> 7. Input Laporan Gapoktan</span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/pick_gap.png" class="img">
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase"> 8a. Masukkan nilai pada form input gapoktan</span>
            </div>
            <div class="portlet-body text-center">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/input_laporan.png" class="img">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="page-content-inner">
          <div class="portlet light portlet-fit">
            <div class="portlet-title">
              <span class="caption-subject bold font-red uppercase"> 8b. Masukkan nilai pada form input tti</span>
            </div>
            <div class="portlet-body text-center" style="height: 494px;">
              <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/pick_child_tti.png" class="img">
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>