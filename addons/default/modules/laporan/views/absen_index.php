<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>Absensi Pelaporan - <?php echo ucfirst($nama_laporan) ?> <small>( <?php if($firstDayOfWeek != NULL) { echo date_idr($weeks['last_day_of_week'], 'F, Y', null); }else{ echo lang('laporan:absen:no_absen'); } ?> )</small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
				<div class="portlet-title">
					<div class="caption">
            <i class="fa fa-search font-red"></i>
            <span class="caption-subject font-red bold uppercase"> Filter
            </span>
          </div>
				</div>
				<div class="portlet-body">
          <?php echo form_open(base_url().'laporan/absen/index/'.$id, array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-provinsi"><?php echo lang('location:provinsi:singular'); ?></label>
              <div class="col-sm-4">
                <?php
                  $value = null;
                  if($this->input->get('f-provinsi') != ""){
                    $value = $this->input->get('f-provinsi');
                  }
                ?>
                <select name="f-provinsi" class="form-control" id="provinsi">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                    <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($value == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-kota"><?php echo lang('location:kota:singular'); ?></label>
                <div class="col-sm-4">
                  <select name="f-kota" id="kota" class="form-control">
                    <?php 
                      if($this->input->get('f-provinsi') != '') {
                        $value = null;
                        if($this->input->get('f-kota') != ""){
                          $value = $this->input->get('f-kota');
                        }
                    ?>
                      <option value=""><?php echo lang('global:select-pick') ?></option>
                    <?php 
                        foreach ($kota['entries'] as $kota_entry){ ?>
                          <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                    <?php 
                        } 
                      }else{
                    ?>
                        <option value=""><?php echo lang('global:select-none') ?></option>
                    <?php
                      }
                    ?>
                  </select>

                  <script type="text/javascript">
                    $('#provinsi').change(function() {
                      var id_provinsi = $(this).val();
                      $("#kota").html('<option value=""><?php echo  lang("laporan:ajax_load_data") ?></option>');
                      $.ajax({
                        url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                        dataType: 'json',
                        success: function(data){
                          if(data.length > 0){
                            $('#kota').html('<option value="">-- Pilih --</option>');
                          }else{
                            $('#kota').html('<option value="">-- Tidak ada --</option>');
                          }
                          $.each(data, function(i, object){
                            $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                          });
                        }
                      });
                    });
                  </script>
                </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-periode"><?php echo lang('laporan:periode'); ?></label>
              <div class="col-sm-4">
                <div class="row">
                  <div class="col-sm-3" style="padding-right: 0px;">    
                    <?php
                      $value = $tahun;
                      if($this->input->get('f-tahun') != ""){
                        $value = $this->input->get('f-tahun');
                      }
                    ?>
                    <select name="f-tahun" id="f-tahun" class="form-control">
                      <?php for($i=$min_year;$i<=$max_year;$i++){ ?>
                        <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $i ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-5" style="padding-right: 0px;">
                    <?php
                      $arr_month = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
                      $value = $bulan;
                      if($this->input->get('f-bulan') != ""){
                        $value = $this->input->get('f-bulan');
                      }
                    ?>
                    <select name="f-bulan" id="f-bulan" class="form-control">
                      <?php 
                        foreach ($arr_month as $key => $month) { ?>
                          <option value="<?php echo $key ?>" <?php echo ($value == $key) ? 'selected' : ''; ?>><?php echo $month; ?></option>
                          <?php 
                        } 
                      ?>
                    </select>
                  </div>

                  <?php if ($id > 2){ ?> 
                    <div class="col-sm-3" style="padding-right: 0px;">
                      <?php
                        $value = $minggu_ke;
                        if($this->input->get('f-minggu_ke') != ""){
                          $value = $this->input->get('f-minggu_ke');
                        }
                      ?>
                      <select name="f-minggu_ke" id="f-minggu_ke" class="form-control">
                        <?php
                          $romawi = array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V');
                          for ($i=1;$i<=$count_of_week;$i++) { ?>
                            <option value="<?php echo $i ?>" <?php echo ($value == $i) ? 'selected' : ''; ?>><?php echo $romawi[$i] ?></option>
                            <?php
                          }
                        ?>
                      </select>
                      <script type="text/javascript">
                        $('#f-tahun, #f-bulan').change(function() {
                          var tahun = $('#f-tahun').val();
                          var bulan = $('#f-bulan').val();
                          $("#f-minggu_ke").html('<option value="">...</option>');
                          $("#f-minggu_ke").load("<?php echo site_url('laporan/data/ajax_get_minggu_ke2') ?>" + '/' + tahun + '/' + bulan, function(data) {
                          });
                        });
                      </script>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2"></div>
              <div class="col-sm-4">
                <button href="<?php echo current_url() . '#'; ?>" class="btn btn-success" type="submit">
                  <i class="icon-ok"></i>
                  Filter
                </button>

                <a href="<?php echo site_url('laporan/absen/index/'.$id); ?>" class="btn btn-default">
                  <i class="icon-remove"></i>
                  Clear
                </a>
              </div>
            </div>
          <?php echo form_close() ?>
        </div>
      </div>
    </div>

    <?php if($firstDayOfWeek != NULL) { ?>
      <div class="page-content-inner">
        <div class="portlet light portlet-fit ">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-table font-red"></i>
              <span class="caption-subject font-red bold uppercase"> Tabel Absensi Pelaporan - <?php echo $nama_laporan ?>
              </span>
            </div>
            <div class="actions">
              <a class="btn btn-circle btn-icon-only btn-default" target="blank" href="<?php echo base_url() ?>laporan/absen/index/<?php echo $id ?>?page=print&<?php echo $_SERVER['QUERY_STRING'] ?>">
                <i class="fa fa-print"></i> 
              </a>
              <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url() ?>laporan/absen/index/<?php echo $id ?>?page=download&<?php echo $_SERVER['QUERY_STRING'] ?>">
                <i class="fa fa-download"></i> 
              </a>
            </div>
          </div>
          <div class="portlet-body" style="overflow-y: auto;">
            <?php if(count($absensi) > 0 && count($jadwal) > 0) { ?>
              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Provinsi</th>
                    <th rowspan="2">Kota/Kabupaten</th>
                    <th rowspan="2">Nama</th>
                    <?php foreach ($jadwal as $key => $absen) { ?>
                      <th style="text-align:center;" <?php echo (count($absen['count_day']) > 1) ? 'colspan="'.count($absen['count_day']).'"' : ''; ?>>
                        M<?php echo $absen['minggu_ke'] ?><br>
                        <span style="font-size:12px; font-weight: normal;">(<?php echo date_idr($absen['tanggal_awal'],'d/m',null); ?> - <?php echo date_idr($absen['tanggal_akhir'],'d/m',null); ?>)</span>
                      </th>
                    <?php } ?> 
                    <th rowspan="2" width="30">%</th>
                  </tr>
                  <tr>
                    <?php
                      $jml_hari = 0; 
                      foreach ($jadwal as $absen) {
                        $i=1;
                        foreach ($absen['count_day'] as $key => $tanggal) {
                          # code...
                          $jml_hari++;
                        ?>
                          <th style="text-align:center;">
                            H<?php echo $i; ?><br>
                            <span style="font-size: 12px; font-weight: normal">(<?php echo date_idr($tanggal, 'd/m', null) ?>)</span>
                          </th>
                        <?php     
                          $i++;
                        } 
                      }
                    ?>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
                  if($cur_page != 0){
                    $item_per_page = $pagination_config['per_page'];
                    $no = $cur_page + 1;
                  }else{
                    $no = 1;
                  }
                  foreach ($absensi as $key => $absen) { ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $absen['provinsi'] ?></td>
                      <td><?php echo $absen['kota'] ?></td>
                      <td><?php echo $absen['display_name'] ?></td>

                      <?php 
                        $jml_hadir = 0;
                        foreach ($minggu as $week) {
                          if(count($absen['days_on_week'.$week]) > 0){
                            foreach ($absen['days_on_week'.$week] as $key => $val) { 
                              if($val == 1) { 
                                $jml_hadir++;
                                ?>
                                <td style="text-align: center;"><?php echo img('addons/default/themes/sitoni/img/check.png', ''); ?></td>
                                <?php
                              }else{ ?>
                                <td style="text-align: center;"><?php echo img('addons/default/themes/sitoni/img/wrong.png', ''); ?></td>
                                <?php       
                              }
                            }
                          }else{
                             ?>
                            <td style="text-align: center;"><?php echo img('addons/default/themes/sitoni/img/wrong.png', ''); ?></td>
                            <?php
                          }
                          
                        }
                      ?>
                      <td><?php echo round(($jml_hadir/$jml_hari)*100); ?>%</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
              <?php echo $pagination; ?>
            <?php }else{ ?>
              <?php echo lang('laporan:data:no_entry'); ?>
            <?php } ?>
          </div>
        </div>
      </div>
    <?php }else{ ?>
      <div class="portlet light portlet-fit ">
        <div class="portlet-body font-red">
          <?php echo lang('laporan:absen:no_absen'); ?>
        </div>
      </div>
    <?php } ?>
  </div>
</div>