<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="form-dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title caption-subject font-red bold">Grafik Kinerja <?php echo $nama_laporan ?> "<?php echo $unit_name ?>"</h4>
          </div>
          <div class="modal-body" id="content-modal">
            <center>
              <div id="statistik<?php echo $id ?>">
              </div>
            </center>
          </div>
      </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">

  $(function () {
    $('#statistik<?php echo $id ?>').highcharts({
      title: {
        text: 'Perkembangan PUPM Melalui Kegiatan TTI',
        x: -20 //center
      },
      xAxis: {
        categories: [<?php echo $categories ?>],
        crosshair: true
      },
      yAxis: [{
        title: {
          text: 'Harga',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
        labels: {
          format: '{value} Rp',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        }
      },{
        title: {
          text: 'Volume',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
        labels: {
          format: '{value} Kg',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
        opposite: true
      }],
      tooltip: {
        shared: true
      },
      series: <?php echo $json_series ?>
    });
  });
</script>