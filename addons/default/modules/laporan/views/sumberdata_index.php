<!-- BEGIN PAGE HEAD-->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1><?php echo lang('laporan:sumberdata') ?></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE BREADCRUMBS -->
    {{ theme:partial name='breadcrumbs' }}
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-search font-red"></i>
            <span class="caption-subject font-red bold uppercase"> Filter
            </span>
          </div>
        </div>
        <div class="portlet-body">
          <?php echo form_open(base_url().'laporan/sumberdata/index', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-provinsi"><?php echo lang('location:provinsi:singular'); ?></label>
              <div class="col-sm-4">
                <?php
                  $value = null;
                  if($this->input->get('f-provinsi') != ""){
                    $value = $this->input->get('f-provinsi');
                  }
                ?>
                <select name="f-provinsi" class="form-control" id="provinsi">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
                    <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($value == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" for="f-kota"><?php echo lang('location:kota:singular'); ?></label>
                <div class="col-sm-4">
                  <select name="f-kota" id="kota" class="form-control">
                    <?php 
                      if($this->input->get('f-provinsi') != '') {
                        $value = null;
                        if($this->input->get('f-kota') != ""){
                          $value = $this->input->get('f-kota');
                        }
                    ?>
                      <option value=""><?php echo lang('global:select-pick') ?></option>
                    <?php 
                        foreach ($kota['entries'] as $kota_entry){ ?>
                          <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
                    <?php 
                        } 
                      }else{
                    ?>
                        <option value=""><?php echo lang('global:select-none') ?></option>
                    <?php
                      }
                    ?>
                  </select>

                  <script type="text/javascript">
                    $('#provinsi').change(function() {
                      var id_provinsi = $(this).val();
                      $("#kota").html('<option value=""><?php echo  lang("laporan:ajax_load_data") ?></option>');
                      $.ajax({
                        url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
                        dataType: 'json',
                        success: function(data){
                          if(data.length > 0){
                            $('#kota').html('<option value="">-- Pilih --</option>');
                          }else{
                            $('#kota').html('<option value="">-- Tidak ada --</option>');
                          }
                          $.each(data, function(i, object){
                            $('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
                          });
                        }
                      });
                    });
                  </script>
                </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label no-padding-right" ><?php echo lang('laporan:tipe'); ?></label>&nbsp;
              <?php
                $value = null;
                if($this->input->get('f-tipe_laporan') != ""){
                  $value = $this->input->get('f-tipe_laporan');
                }
              ?>
              <div class="col-sm-2">
                <select name="f-tipe_laporan" class="form-control">
                  <option value=""><?php echo lang('global:select-pick') ?></option>
                  <?php foreach ($tipes as $key => $tipe) { ?>
                    <option value="<?php echo $tipe['id'] ?>" <?php echo ($value == $tipe['id']) ? 'selected' : ''; ?>><?php echo $tipe['nama_laporan'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-2"></div>
              <div class="col-sm-4">
                <button href="<?php echo current_url() . '#'; ?>" class="btn btn-success" type="submit">
                  <i class="icon-ok"></i>
                  Filter
                </button>

                <a href="<?php echo site_url('laporan/sumberdata/index'); ?>" class="btn btn-default">
                  <i class="icon-remove"></i>
                  Clear
                </a>
              </div>
            </div>
          <?php echo form_close() ?>
        </div>
      </div>
    </div>
    <div class="page-content-inner">
      <div class="portlet light portlet-fit ">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-table font-red"></i>
            <span class="caption-subject font-red bold uppercase"> <?php echo lang('laporan:sumberdata:plural') ?></span>
          </div>
          <div class="actions">
            <?php if ($sumberdata['total'] > 0){ ?>
              <a class="btn btn-circle btn-icon-only btn-default" target="blank" href="<?php echo base_url() ?>laporan/sumberdata/download/1?<?php echo $_SERVER['QUERY_STRING'] ?>">
                <i class="fa fa-print"></i> 
              </a>
              <a class="btn btn-circle btn-icon-only btn-default" href="<?php echo base_url() ?>laporan/sumberdata/download?<?php echo $_SERVER['QUERY_STRING'] ?>">
                <i class="fa fa-download"></i> 
              </a>
            <?php } ?>
          </div>
        </div>
        <div class="portlet-body">
          <?php if ($sumberdata['total'] > 0){ ?>
            <p class="pull-right"><?php echo lang('laporan:showing').' '.count($sumberdata['entries']).' '.lang('laporan:of').' '.$sumberdata['total'] ?></p>
            <div style="overflow-y: auto; clear: both">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th><?php echo lang('location:provinsi:singular'); ?></th>
                  <th><?php echo lang('location:kota:singular'); ?></th>
                  <th><?php echo lang('laporan:tipe'); ?></th>
                  <th><?php echo lang('laporan:gap_or_tti'); ?></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
                if($cur_page != 0){
                  $item_per_page = $pagination_config['per_page'];
                  $no = $cur_page + 1;
                }else{
                  $no = 1;
                }
                ?>
                
                <?php foreach ($sumberdata['entries'] as $sumberdata_entry): ?>
                <tr>
                  <td><?php echo $no; $no++; ?></td>
                  <td><?php echo $sumberdata_entry['provinsi']; ?></td>
                  <td><?php echo $sumberdata_entry['kota']; ?></td>
                  <td><?php echo $sumberdata_entry['type_name'] ?></td>
                  <td><?php echo $sumberdata_entry['unit_name']; ?></td>
                  <td><a href="<?php echo base_url() ?>laporan/sumberdata/view/<?php echo $sumberdata_entry['unit_id'] ?>" class="btn btn-danger btn-sm">Tinjau</a></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            </div>
            <?php echo $sumberdata['pagination']; ?>
          <?php }else{ ?>
            <?php echo lang('laporan:sumberdata:no_entry'); ?>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>