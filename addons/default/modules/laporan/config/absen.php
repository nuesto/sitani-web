<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['report_time_picker'] = true;
$config['start_report_time_picker'] = '2018-01-01';
$config['end_report_time_picker'] = date('Y-m-d');