<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['laporan/admin/data(:any)'] = 'admin_data$1';
$route['laporan/admin/data_keuangan(:any)'] = 'admin_data_keuangan$1';
$route['laporan/admin/data_kinerja(:any)'] = 'admin_data_kinerja$1';
$route['laporan/admin/tipe_field(:any)'] = 'admin_tipe_field$1';
$route['laporan/admin/pendamping(:any)'] = 'admin_pendamping$1';
$route['laporan/admin/metadata(:any)'] = 'admin_metadata$1';
$route['laporan/admin/komoditas(:any)'] = 'admin_komoditas$1';
$route['laporan/admin/absen(:any)'] = 'admin_absen$1';
$route['laporan/admin/sms(:any)'] = 'admin_sms$1';
$route['laporan/admin/dialog(:any)'] = 'admin_dialog$1';
$route['laporan/admin/sp2d(:any)'] = 'admin_sp2d$1';

$route['laporan/panduan(:any)'] = 'laporan_panduan$1';
$route['laporan/data(:any)'] = 'laporan_data$1';
$route['laporan/absen(:any)'] = 'laporan_absen$1';
$route['laporan/pendamping(:any)'] = 'laporan_pendamping$1';
$route['laporan/sumberdata(:any)'] = 'laporan_sumberdata$1';
$route['laporan/dialog(:any)'] = 'laporan_dialog$1';
$route['laporan/sms(:any)'] = 'laporan_sms$1';
$route['laporan/sp2d(:any)'] = 'laporan_sp2d$1';
// $route['laporan/metadata(:any)'] = 'laporan_metadata$1';
// $route['laporan/admin/get_sms(:any)'] = 'admin_get_sms$1';
