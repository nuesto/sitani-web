<?php
  if(!function_exists('getWeeksByDate')){

    function getWeeksByDate($date, $rollover, $type_date=NULL){
      $cut = substr($date, 0, 8);
      $daylen = 86400;

      $timestamp = strtotime($date);
      $first = strtotime($cut . "00");
      $elapsed = ($timestamp - $first) / $daylen;

      $start_date_of_this_month =  date('Y-m-01', strtotime($date));
      $day = date('j', strtotime($start_date_of_this_month));
      $day_of_week = date('w', strtotime($start_date_of_this_month));
      $next_week = date("j", strtotime('next sunday', strtotime($start_date_of_this_month)));
      if($type_date == 'last'){
        if($day <= $next_week && $day_of_week >= 3){
          $weeks = 0;
        }else{
          $weeks = 1;
        }
      }elseif($type_date == 'next'){
        $weeks = 1;
      }else{
        if(($day <= $next_week && $day_of_week > 3) || $day_of_week == 0){
          $weeks = 0;
        }else{
          $weeks = 1;
        }
      }
      for ($i = 1; $i <= $elapsed; $i++)
      {
        $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
        $daytimestamp = strtotime($dayfind);

        $day = strtolower(date("l", $daytimestamp));
        if($day == strtolower($rollover)){
          $weeks++;
        }
      }


      return $weeks;
    }
  }

  if(!function_exists('getWeeks')){
    function getWeeks($date, $rollover)
    {
      // $date = '2017-07-30';
      // $date = '2016-11-27';
      // dump($date);
      // $date = '2018-09-02';
      $rollover = 'sunday';
      $weeks = getWeeksByDate($date, $rollover);
      $day = date('j', strtotime($date));
      $day_of_week = date('w', strtotime($date));
      $month = date('n', strtotime($date));
      $year = date('Y', strtotime($date));
      $start_date_of_this_month =  date('Y-m-01', strtotime($date));
      $start_day_of_week =  date('w', strtotime($start_date_of_this_month));
      $next_week = date("j", strtotime('next sunday', strtotime($start_date_of_this_month)));
      $count_of_week_on_this_month = get_count_of_week($date);
      if($weeks >= $count_of_week_on_this_month && $day_of_week < 3){
        // Next Month
        $next_month = ($month == 12) ? 1 : $month+1;
        $next_year = ($month == 12) ? $year+1 : $year;
        $next_year_month = $next_year.'-'.$next_month;
        $first_date_of_next_month = date("Y-m-01", strtotime($next_year_month));
        $day_of_week_next_month = date('w', strtotime($first_date_of_next_month));
        $mont_of_next_day = date('n', strtotime('+1 days', strtotime($date)));
        $weeks_next_month = getWeeksByDate($first_date_of_next_month, $rollover,'next');
        // dump($day_of_week_next_month, $day_of_week);
        // dump($count_of_week_on_this_month);
        // dump($weeks);
        // dump($mont_of_next_day, $month);

        $last_date_of_this_month = date("Y-m-t", strtotime($date));
        $day_of_last_day_this_month = date('j', strtotime($last_date_of_this_month));
        $jarak = $day_of_last_day_this_month - $day;
        // dump($jarak, $day_of_last_day_this_month, $day, $last_date_of_this_month);

        if(($day_of_week_next_month <= 3 && $mont_of_next_day > $month) || $jarak <3){
          // if($day_of_week_next_month)
          $weeks = $weeks_next_month;
          $month = $next_month;
          $year = $next_year;
        }

        // cek next month of next week;
        // dump($first_date_of_next_month);
        // die();
      }else{
        if(($day >= 1 && $day < $next_week && $start_day_of_week >= 4)){
          // Last Month
          $last_month = ($month == 1) ? 12 : $month-1;
          $last_year = ($month == 1) ? $year-1 : $year;
          $last_year_month = $last_year.'-'.$last_month;
          $last_date_of_last_month = date("Y-m-t", strtotime($last_year_month));
          // $weeks_last_month = getWeeksByDate($last_date_of_last_month, $rollover, 'last');
          $count_week_of_last_month = get_count_of_week($last_date_of_last_month);
          $weeks = $count_week_of_last_month;
          $month = $last_month;
          $year = $last_year;
        }
      }

      $time = strtotime($date);
      $hari = date('l',$time);
      $sunday_or_monday = 'monday';
      if($hari == 'Sunday'){
        $sunday_or_monday = 'sunday';
      }
      // $sunday_of_week = date('Y-m-d', strtotime('monday this week', $time));
      $sunday_of_week = date('Y-m-d', strtotime($sunday_or_monday.' this week', $time));
      // dump($monday_of_week);
      if($hari != 'Sunday'){
        $sunday_of_week = date('Y-m-d', strtotime('-1 days', strtotime($sunday_of_week)));
      }
      // $sunday_of_week = date('Y-m-d', strtotime('-1 days', strtotime($monday_of_week)));
      $first_day_of_week = $sunday_of_week;
      $last_day_of_week = date('Y-m-d', strtotime('+6 days', strtotime($first_day_of_week)));
      // dump($date, $sunday_of_week, $last_day_of_week);

      $bln1 = strtotime(date('Y-m', strtotime($first_day_of_week)));
      $bln2 = strtotime(date('Y-m', strtotime($last_day_of_week)));


      $arr_weeks['week'] = $weeks;
      $arr_weeks['first_day_of_week'] = $first_day_of_week;
      $arr_weeks['last_day_of_week'] = $last_day_of_week;
      $arr_weeks['bulan'] = $month;
      $arr_weeks['tahun'] = $year;
      // dump($arr_weeks);
      // die();
      return $arr_weeks;
    }
  }

  if(!function_exists('get_count_of_week')){
    function get_count_of_week($date){

      $timestamp = strtotime($date);
      $date = date('Y-m-t', $timestamp);
      $cut = substr($date, 0, 8);
      $daylen = 86400;

      $timestamp = strtotime($date);
      $first = strtotime($cut . "00");
      $elapsed = ($timestamp - $first) / $daylen;

      $start_date_of_this_month =  date('Y-m-01', strtotime($date));
      $day = date('j', strtotime($start_date_of_this_month));
      $day_of_week = date('w', strtotime($start_date_of_this_month));
      $next_week = date("j", strtotime('next sunday', strtotime($start_date_of_this_month)));
      if(($day <= $next_week && $day_of_week > 3) || $day_of_week == 0){
        $weeks = 0;
      }else{
        $weeks = 1;
      }

      for ($i = 1; $i <= $elapsed; $i++)
      {
        $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
        $daytimestamp = strtotime($dayfind);
        $day = strtolower(date("l", $daytimestamp));
        if($day == strtolower('sunday')){
          $weeks ++;
        }
      }

      $day = date('j', strtotime($dayfind));
      $day_of_week = date('w', strtotime($dayfind));
      $next_week = date("j", strtotime('next sunday', strtotime($dayfind)));
      if($day_of_week < 3){
        $weeks = $weeks-1;
      }

      return $weeks;
    }
  }
  if(!function_exists('cek_first_week_of_month')){
    function cek_first_week_of_month($date, $rollover){
      $cut = substr($date, 0, 8);
      $daylen = 86400;

      $timestamp = strtotime($date);
      $first = strtotime($cut . "00");
      $elapsed = ($timestamp - $first) / $daylen;

      $weeks = 0;

      for ($i = 1; $i <= $elapsed; $i++)
      {
        $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
        $daytimestamp = strtotime($dayfind);

        $day = strtolower(date("l", $daytimestamp));
        if($day == strtolower($rollover)){
          $weeks ++;
        }
      }

      return $weeks;
    }
  }

  if(!function_exists('date_idr')){
    function date_idr ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
      date_default_timezone_set("Asia/Jakarta"); 
      if (trim ($timestamp) == '')
      {
              $timestamp = time ();
      }
      elseif (!ctype_digit ($timestamp))
      {
          $timestamp = strtotime ($timestamp);
      }
      # remove S (st,nd,rd,th) there are no such things in indonesia :p
      $date_format = preg_replace ("/S/", "", $date_format);
      $pattern = array (
          '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
          '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
          '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
          '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
          '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
          '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
          '/April/','/June/','/July/','/August/','/September/','/October/',
          '/November/','/December/',
      );
      $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
          'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
          'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
          'Januari','Februari','Maret','April','Juni','Juli','Agustus','September',
          'Oktober','November','Desember',
      );
      $date = date ($date_format, $timestamp);
      $date = preg_replace ($pattern, $replace, $date);
      $date = "{$date}{$suffix}";
      return $date;
    }
  }

  if(!function_exists('get_date_by_wmy')){
    function get_date_by_wmy($week, $thn_bln){
      $ex = explode('-',$thn_bln);
      $year = $ex[0];
      $month = $ex[1];
      $beg = (int) date('W', strtotime("first monday of $year-$month"));
      $end = (int) date('W', strtotime("last  monday of $year-$month"));
      $w = 1;
      $get_weeks = 0;
      for($i=$beg; $i<=$end;$i++) {
        if($w == $week){
          $get_weeks = $i;
          break;
        }
        $w++;
      }
      $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $get_weeks * 7 * 24 * 60 * 60 );
      $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
      $date = date( 'Y-m-d', $timestamp_for_monday );
      
      return $date;
    }
  }

  if(!function_exists('get_interval')){
    // Mengambil minggu, bulan tahun berdasarkan tanggal
    function get_interval($date, $start, $end){
      $date = strtotime($date);

      for($i=$start;$i<=$end;$i++){
        $interval = date('Y-m-d', strtotime("-".$i." day", $date));
        $arr_days[$i] = getWeeks($interval, 'monday');
        // dump($interval, $arr_days[$i]);
        $i = $i+6;
      }
      return $arr_days;
    }
  }

  if(!function_exists('humanTiming')){
    function humanTiming ($time)
    {

      $time = time() - $time; // to get the time since that moment
      $time = ($time<1)? 1 : $time;
      $tokens = array (
          31536000 => 'year',
          2592000 => 'month',
          604800 => 'week',
          86400 => 'day',
          3600 => 'hour',
          60 => 'min',
          1 => 'sec'
      );

      foreach ($tokens as $unit => $text) {
          if ($time < $unit) continue;
          $numberOfUnits = floor($time / $unit);
          return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
      }

    }
  }

  if(!function_exists('get_first_komoditas')){
    function get_first_komoditas($index){
      ci()->load->model('komoditas_m');
      $komoditas = ci()->komoditas_m->get_first_komoditas();
      return $komoditas[$index];
    }
  }
?>