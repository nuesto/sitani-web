<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Laporan extends Module
{
  public $version = '1.7';

  public function info()
  {
    $info = array();
		$info['name'] = array(
			'en' => 'Laporan',
			'id' => 'Laporan',
		);
		$info['description'] = array(
			'en' => 'Modul yang me-manage laporan',
			'id' => 'Modul yang me-manage laporan',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'Laporan';
		$info['roles'] = array(
			'access_laporan_backend',
			'create_laporan',
			'create_own_laporan',
			'create_own_unit_laporan', 
			'create_own_prov_laporan', 
			'view_all_laporan', 
			'view_own_laporan', 
			'view_own_unit_laporan', 
			'view_own_prov_laporan', 
			'edit_all_laporan', 
			'edit_own_laporan', 
			'edit_own_unit_laporan',
			'edit_own_prov_laporan',

			'access_metadata_backend', 
			'manage_metadata',

			'access_komoditas_backend', 
			'manage_komoditas',


			'access_tipe_field_backend', 
			'manage_tipe_field',

			'access_absen_backend',
			'manage_absen',

			'access_sms_backend', 
			'send_sms',
			'view_all_sms',
			'view_own_sms',
			'view_own_prov_sms',
			'delete_all_sms',

			'access_dialog_backend', 
			'manage_dialog',

			'access_sp2d_backend',
			'view_all_sp2d',
			'view_own_sp2d',
			'view_own_prov_sp2d',
			'create_sp2d',
			'create_own_prov_sp2d',
			'edit_all_sp2d',
			'edit_own_sp2d',
			'edit_own_prov_sp2d',
			'delete_all_sp2d',
			'delete_own_sp2d',
			'delete_own_prov_sp2d',
			'change_sp2d_status',
			'api_pendamping','api_absen','api_laporan','api_sp2d','api_dialog',
		);
		
		if(group_has_role('laporan', 'access_laporan_backend')){
			$info['sections']['laporan']['name'] = 'laporan:laporan:plural';
			$info['sections']['laporan']['uri'] = 'admin/laporan/laporan/index';
			
			if(group_has_role('laporan', 'create_laporan')){
				$info['sections']['laporan']['shortcuts']['create'] = array(
					'name' => 'laporan:laporan:new',
					'uri' => 'admin/laporan/laporan/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('laporan', 'access_metadata_backend')){
			$info['sections']['metadata']['name'] = 'laporan:metadata:plural';
			$info['sections']['metadata']['uri'] = 'admin/laporan/metadata/index';
			
			if(group_has_role('laporan', 'manage_metadata')){
				$info['sections']['metadata']['shortcuts']['create'] = array(
					'name' => 'laporan:metadata:new',
					'uri' => 'admin/laporan/metadata/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('laporan', 'access_komoditas_backend')){
			$info['sections']['komoditas']['name'] = 'laporan:komoditas:plural';
			$info['sections']['komoditas']['uri'] = 'admin/laporan/komoditas/index';
			
			if(group_has_role('laporan', 'manage_komoditas')){
				$info['sections']['komoditas']['shortcuts']['create'] = array(
					'name' => 'laporan:komoditas:new',
					'uri' => 'admin/laporan/komoditas/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('laporan', 'access_dialog_backend')){
			$info['sections']['dialog']['name'] = 'laporan:dialog:plural';
			$info['sections']['dialog']['uri'] = 'admin/laporan/dialog/index';
			
			if(group_has_role('laporan', 'manage_dialog')){
				$info['sections']['dialog']['shortcuts']['create'] = array(
					'name' => 'laporan:dialog:new',
					'uri' => 'admin/laporan/dialog/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('laporan', 'access_tipe_backend')){
			$info['sections']['tipe']['name'] = 'laporan:tipe:plural';
			$info['sections']['tipe']['uri'] = 'admin/laporan/tipe/index';
			
			if(group_has_role('laporan', 'manage_tipe')){
				$info['sections']['tipe']['shortcuts']['create'] = array(
					'name' => 'laporan:tipe:new',
					'uri' => 'admin/laporan/tipe/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
	}
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items, &$menu_order){
		unset($menu_items['lang:cp:nav_Laporan']);
		
		if(group_has_role('laporan','access_laporan_backend')){
			if(group_has_role('laporan', 'create_laporan') || group_has_role('laporan', 'create_own_laporan') || group_has_role('laporan', 'create_own_unit_laporan') || group_has_role('laporan', 'create_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:data:new']['lang:laporan:data:komoditas']['urls'] = array('admin/laporan/data/create');
			}
			if(group_has_role('laporan', 'create_laporan') || group_has_role('laporan', 'create_own_laporan') || group_has_role('laporan', 'create_own_unit_laporan') || group_has_role('laporan', 'create_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:data:new']['lang:laporan:data:keuangan']['urls'] = array('admin/laporan/data_keuangan/create');
			}
			if(group_has_role('laporan', 'create_laporan') || group_has_role('laporan', 'create_own_laporan') || group_has_role('laporan', 'create_own_unit_laporan') || group_has_role('laporan', 'create_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:data:new']['lang:laporan:data:kinerja']['urls'] = array('admin/laporan/data_kinerja/create');
			}

			// rekapitulasi data
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:data:plural']['lang:laporan:data:komoditas']['urls'] = array('admin/laporan/data/index');
			}
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:data:plural']['lang:laporan:data:keuangan']['urls'] = array('admin/laporan/data_keuangan/index');
			}
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:data:plural']['lang:laporan:data:kinerja']['urls'] = array('admin/laporan/data_kinerja/index');
			}


			// sandingkan periode
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:sandingkan_laporan']['lang:laporan:data:komoditas']['urls'] = array('admin/laporan/data/sandingkan_laporan');
			}
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:sandingkan_laporan']['lang:laporan:data:keuangan']['urls'] = array('admin/laporan/data_keuangan/sandingkan_laporan');
			}
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:sandingkan_laporan']['lang:laporan:data:kinerja']['urls'] = array('admin/laporan/data_kinerja/sandingkan_laporan');
			}
		}

		if(group_has_role('laporan','access_tipe_field_backend')){
			if(group_has_role('laporan', 'manage_tipe_field')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:tipe_field:plural']['urls'] = array('admin/laporan/tipe_field/index','admin/laporan/tipe_field/create','admin/laporan/tipe_field/edit%1');
			}
		}


		if(group_has_role('laporan','access_komoditas_backend')){
			if(group_has_role('laporan', 'manage_komoditas')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:komoditas:plural']['urls'] = array('admin/laporan/komoditas/index','admin/laporan/komoditas/create','admin/laporan/komoditas/edit%1');
			}
		}
		
		if(group_has_role('laporan','access_metadata_backend')){
			if(group_has_role('laporan', 'manage_metadata')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:metadata:plural']['urls'] = array('admin/laporan/metadata/index','admin/laporan/metadata/create','admin/laporan/metadata/edit%1');
			}
		}

		if(group_has_role('laporan','access_absen_backend')){
			if(group_has_role('laporan', 'manage_absen')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:absen:singular']['urls'] = array('admin/laporan/absen/index');
			}
		}

		// Daftar Laporan
		if(group_has_role('laporan','access_laporan_backend')){
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:daftar_laporan']['lang:laporan:data:komoditas']['urls'] = array('admin/laporan/data/daftar_laporan','admin/laporan/data/edit%4', 'admin/laporan/data/edit%5');
			}
		}
		if(group_has_role('laporan','access_laporan_backend')){
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:daftar_laporan']['lang:laporan:data:keuangan']['urls'] = array('admin/laporan/data_keuangan/daftar_laporan','admin/laporan/data_keuangan/edit%4', 'admin/laporan/data_keuangan/edit%5');
			}
		}
		if(group_has_role('laporan','access_laporan_backend')){
			if(group_has_role('laporan', 'view_all_laporan') || group_has_role('laporan', 'view_own_laporan') || group_has_role('laporan','view_own_unit_laporan') || group_has_role('laporan','view_own_prov_laporan')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:daftar_laporan']['lang:laporan:data:kinerja']['urls'] = array('admin/laporan/data_kinerja/daftar_laporan','admin/laporan/data_kinerja/edit%4', 'admin/laporan/data_kinerja/edit%5');
			}
		}

		if(group_has_role('laporan','access_sms_backend')){
			if(group_has_role('laporan', 'view_all_sms') || group_has_role('laporan', 'view_own_sms') || group_has_role('laporan','view_own_unit_sms') || group_has_role('laporan','view_own_prov_sms')) {
				$menu_items['lang:cp:nav_Laporan']['lang:laporan:daftar_sms']['urls'] = array('admin/laporan/sms/index');
			}
		}

		if(group_has_role('users', 'manage_users') OR group_has_role('users', 'manage_own_unit_users')) {
			$menu_items["lang:cp:nav_Pendamping"]["lang:laporan:pendamping"]['urls'] = array(
				'admin/laporan/pendamping/index');
		}

		if(group_has_role('laporan','access_dialog_backend')){
			if(group_has_role('laporan', 'manage_dialog')) {
				$menu_items['lang:cp:nav_Dialog']['lang:laporan:dialog:singular']['urls'] = array('admin/laporan/dialog/index');
			}
		}

		if(group_has_role('laporan','access_sp2d_backend')){
			if(group_has_role('laporan', 'view_all_sp2d') || group_has_role('laporan','view_own_sp2d') || group_has_role('laporan','view_own_prov_sp2d')) {
				$menu_items['lang:cp:nav_Sp2d']['lang:laporan:sp2d:singular']['urls'] = array('admin/laporan/sp2d/index','admin/laporan/sp2d/create','admin/laporan/sp2d/edit%1','admin/laporan/sp2d/view%1');
			}
		}

		
	}

  /**
   * Install
   *
   * This function will set up our streams
	 *
   */
  public function install()
  {
    $this->load->dbforge();

		// data
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'value' => array(
				'type' => 'INT',
			),
			'channel' => array(
				'type' => 'VARCHAR',
				'constraint' => '11',
			),
			'id_metadata' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'id_sms' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'id_absen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'id_user' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'id_unit' => array(
				'type' => 'INT',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('laporan_data', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_laporan_data(id)");
		$this->db->query("CREATE INDEX id_metadata ON default_laporan_data(id_metadata)");
		$this->db->query("CREATE INDEX id_sms ON default_laporan_data(id_sms)");
		$this->db->query("CREATE INDEX id_absen ON default_laporan_data(id_absen)");
		$this->db->query("CREATE INDEX id_user ON default_laporan_data(id_user)");
		$this->db->query("CREATE INDEX id_unit ON default_laporan_data(id_unit)");


		// komoditas
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'kode_komoditas' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'urutan' => array(
				'type' => 'INT',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('laporan_komoditas', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_laporan_komoditas(id)");

		// metadata
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'field' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
			'satuan' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'id_laporan_tipe' => array(
				'type' => 'INT',
			),
			'nilai_minimal_home' => array(
				'type' => 'INT',
			),
			'nilai_maksimal_home' => array(
				'type' => 'INT',
			),
			'nilai_minimal' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'nilai_maksimal' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'urutan' => array(
				'type' => 'INT',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('laporan_metadata', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_laporan_metadata(id)");


		// absen
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'tanggal' => array(
				'type' => 'DATE',
			),
			'bulan' => array(
				'type' => 'INT',
			),
			'tahun' => array(
				'type' => 'INT',
			),
			'minggu_ke' => array(
				'type' => 'INT',
			),
			'tanggal_awal' => array(
				'type' => 'DATE',
			),
			'tanggal_akhir' => array(
				'type' => 'DATE',
			),
			'waktu_buka' => array(
				'type' => 'DATETIME',
			),
			'waktu_tutup' => array(
				'type' => 'DATETIME',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('laporan_absen', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_laporan_absen(id)");


		// sms
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'kode' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'no_hp' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'isi' => array(
				'type' => 'TEXT',
			),
			'tanggal' => array(
				'type' => 'DATETIME',
			),
			'status_error' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'keterangan_parsing' => array(
				'type' => 'TEXT',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('laporan_sms', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_laporan_sms(id)");
		$this->db->query("CREATE INDEX no_hp ON default_laporan_sms(no_hp)");


		// dialog
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'kontak' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
			'isi' => array(
				'type' => 'TEXT',
			),
			'id_sms' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'status' => array(
				'type' => 'INT',
				'constraint' => 1,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('laporan_dialog', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_laporan_dialog(id)");

		// tipe
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama_laporan' => array(
				'type' => 'VARCHAR',
				'constraint' => '45',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('laporan_tipe', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_laporan_tipe(id)");

		// stop installation in this version
		// please upgrade
		$this->version = '1.0';

		return true;
  }

  /**
   * Uninstall
   *
   * Uninstall our module - this should tear down
   * all information associated with it.
   */
  public function uninstall()
  {
		$this->load->dbforge();
	    $this->dbforge->drop_table('laporan_data');
	    $this->dbforge->drop_table('laporan_metadata');
	    $this->dbforge->drop_table('laporan_absen');
	    $this->dbforge->drop_table('laporan_sms');
	    $this->dbforge->drop_table('laporan_dialog');
	    $this->dbforge->drop_table('laporan_tipe');
	    $this->dbforge->drop_table('laporan_tipe_field');

	    return true;
  }

  public function upgrade($old_version)
  {
  	$this->load->dbforge();

		switch($old_version){

			case '1.0':
				// tipe field
				$fields = array(
					'id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'nama_tipe_field' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
					),
				);
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('laporan_tipe_field', TRUE);
				$this->db->query("CREATE INDEX author_index ON default_laporan_tipe_field(id)");

				$fields = array(
					'id_laporan_tipe_field' => array(
						'type' => 'INT',
					),
				);
				$this->dbforge->add_column('laporan_metadata', $fields, 'field');

				// stop upgrade in this version
				// please upgrade again
				$this->version = '1.1';
				break;

			case '1.1':

				$fields = array(
					'id_laporan_tipe' => array(
						'type' => 'INT',
					),
				);
				$this->dbforge->add_column('laporan_absen', $fields, 'id');

				// stop upgrade in this version
				// please upgrade again
				$this->version = '1.2';
				break;

			case '1.2' :

				// Set Foregin Key On default_laporan_data
				// FK SMS
				$this->db->query("ALTER TABLE `default_laporan_data` ADD CONSTRAINT `fk_data_sms` FOREIGN KEY (`id_sms`) REFERENCES `default_laporan_sms`(`id`)");

				// Set Id absen Unsigned
				$this->db->query("ALTER TABLE `default_laporan_data` CHANGE `id_unit` `id_unit` INT(11) UNSIGNED NOT NULL");
				// Delete unit not existing
				$this->db->query("DELETE FROM default_laporan_data WHERE id_unit NOT IN (SELECT id FROM default_organization_units)");
				// FK Unit
				$this->db->query("ALTER TABLE `default_laporan_data` ADD CONSTRAINT `fk_data_unit` FOREIGN KEY (`id_unit`) REFERENCES `default_organization_units`(`id`)");

				// Delete absen not existing
				$this->db->query("DELETE FROM default_laporan_data WHERE id_absen NOT IN (SELECT id FROM default_laporan_absen)");
				// FK Absen
				$this->db->query("ALTER TABLE `default_laporan_data` ADD CONSTRAINT `fk_data_absen` FOREIGN KEY (`id_absen`) REFERENCES `default_laporan_absen`(`id`)");

				// Delete metadata not existing
				$this->db->query("DELETE FROM default_laporan_data WHERE id_metadata NOT IN (SELECT id FROM default_laporan_metadata)");
				// FK Metadata
				$this->db->query("ALTER TABLE `default_laporan_data` ADD CONSTRAINT `fk_data_metadata` FOREIGN KEY (`id_metadata`) REFERENCES `default_laporan_metadata`(`id`)");

				// Delete users not existing
				$this->db->query("DELETE FROM default_laporan_data WHERE id_user NOT IN (SELECT user_id FROM default_profiles)");
				// FK User
				$this->db->query("ALTER TABLE `default_laporan_data` ADD CONSTRAINT `fk_data_user` FOREIGN KEY (`id_user`) REFERENCES `default_profiles`(`user_id`)");

				// Set Foregin Key On default_laporan_metadata
				// Set id_laporan_tipe_field unsigned
				$this->db->query("ALTER TABLE `default_laporan_metadata` CHANGE `id_laporan_tipe_field` `id_laporan_tipe_field` INT(11) UNSIGNED NOT NULL");
				// create index
				$this->db->query("CREATE INDEX tipe_field_index ON default_laporan_metadata(id_laporan_tipe_field)");
				// FK Tipe FIeld
				$this->db->query("ALTER TABLE `default_laporan_metadata` ADD CONSTRAINT `fk_metadata_tipe_field` FOREIGN KEY (`id_laporan_tipe_field`) REFERENCES `default_laporan_tipe_field`(`id`)");

				// Set id_laporan_tipe unsigned
				$this->db->query("ALTER TABLE `default_laporan_metadata` CHANGE `id_laporan_tipe` `id_laporan_tipe` INT(11) UNSIGNED NOT NULL");
				// create index
				$this->db->query("CREATE INDEX tipe_index ON default_laporan_metadata(id_laporan_tipe)");
				// Set id_laporan_tipe unsigned
				$this->db->query("ALTER TABLE `default_laporan_tipe` CHANGE `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;");
				// FK Tipe FIeld
				$this->db->query("ALTER TABLE `default_laporan_metadata` ADD CONSTRAINT `fk_metadata_tipe` FOREIGN KEY (`id_laporan_tipe`) REFERENCES `default_laporan_tipe`(`id`)");

				// Set Foreign Key On default_laporan_dialog
				// create index
				$this->db->query("CREATE INDEX sms_index ON default_laporan_dialog(id_sms)");
				// Set id_sms unsigned
				$this->db->query("ALTER TABLE `default_laporan_dialog` CHANGE `id_sms` `id_sms` INT(11) UNSIGNED NULL DEFAULT NULL;");
				// FK id sms
				$this->db->query("ALTER TABLE `default_laporan_dialog` ADD CONSTRAINT `fk_dialog_sms` FOREIGN KEY (`id_sms`) REFERENCES `default_laporan_sms`(`id`)");

				// Set Foreign Key On default_laporan_absen
				// create index
				$this->db->query("CREATE INDEX laporan_tipe_index ON default_laporan_absen(id_laporan_tipe)");
				// Set id_laporan_tipe unsigned
				$this->db->query("ALTER TABLE `default_laporan_absen` CHANGE `id_laporan_tipe` `id_laporan_tipe` INT(11) UNSIGNED NOT NULL;");
				// FK id laporan_tipe
				$this->db->query("ALTER TABLE `default_laporan_absen` ADD CONSTRAINT `fk_laporan_tipe` FOREIGN KEY (`id_laporan_tipe`) REFERENCES `default_laporan_tipe`(`id`)");

				$this->version = '1.3';
				break;

			case '1.3' :
				// laporan sp2d
				$fields = array(
					'id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'nama_sp2d' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
					),
					'deskripsi' => array(
						'type' => 'TEXT',
						'null' => TRUE
					),
					'file' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
					),
					'id_provinsi' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
					),
					'status' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
					),
					'catatan' => array(
						'type' => 'TEXT',
						'null' => TRUE
					),
					'created_on' => array(
						'type' => 'DATETIME',
					),
					'created_by' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
					),
					'updated_on' => array(
						'type' => 'DATETIME',
						'null' => TRUE,
					),
					'updated_by' => array(
						'type' => 'INT',
						'constraint' => 11,
						'null' => TRUE,
						'unsigned' => TRUE,
					),
					'changed_on' => array(
						'type' => 'DATETIME',
						'null' => TRUE,
					),
					'changed_by' => array(
						'type' => 'INT',
						'constraint' => 11,
						'null' => TRUE,
						'unsigned' => TRUE,
					)
				);

				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('laporan_sp2d', TRUE);
				$this->db->query("CREATE INDEX id_provinsi ON default_laporan_sp2d(id_provinsi)");
				$this->db->query("CREATE INDEX created_by ON default_laporan_sp2d(created_by)");
				$this->db->query("CREATE INDEX updated_by ON default_laporan_sp2d(updated_by)");
				$this->db->query("CREATE INDEX changed_by ON default_laporan_sp2d(changed_by)");

				$this->db->query("ALTER TABLE `default_laporan_sp2d` ADD CONSTRAINT `fk_sp2d_id_provinsi` FOREIGN KEY (`id_provinsi`) REFERENCES `default_location_provinsi`(`id`)");
				$this->db->query("ALTER TABLE `default_laporan_sp2d` ADD CONSTRAINT `fk_sp2d_created_by` FOREIGN KEY (`created_by`) REFERENCES `default_profiles`(`user_id`)");
				$this->db->query("ALTER TABLE `default_laporan_sp2d` ADD CONSTRAINT `fk_sp2d_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `default_profiles`(`user_id`)");
				$this->db->query("ALTER TABLE `default_laporan_sp2d` ADD CONSTRAINT `fk_sp2d_changed_by` FOREIGN KEY (`changed_by`) REFERENCES `default_profiles`(`user_id`)");

				$this->version = '1.4';

				break;

			case '1.4' :
				// laporan komoditas
				$fields = array(
					'id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
					),
					'nama_komoditas' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
					),
					'slug' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
					),
					'deskripsi' => array(
						'type' => 'text',
						'null' => TRUE,
					),
					'urutan' => array(
						'type' => 'INT',
					),
				);
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('laporan_komoditas', TRUE);
				$this->db->query("CREATE INDEX author_index ON default_laporan_komoditas(id)");

				$this->db->query("INSERT INTO default_laporan_komoditas (nama_komoditas, slug, urutan)VALUES ('Beras','beras','1')");

				$fields = array(
					'id_laporan_komoditas' => array(
						'type' => 'INT',
						'unsigned' => TRUE,
					),
				);
				$this->dbforge->add_column('laporan_metadata', $fields, 'field');
				$this->db->query("CREATE INDEX id_laporan_komoditas ON default_laporan_metadata(id_laporan_komoditas)");
				$this->db->query("UPDATE default_laporan_metadata set id_laporan_komoditas = 1");

				$this->db->query("ALTER TABLE `default_laporan_metadata` ADD CONSTRAINT `fk_metadata_komoditas` FOREIGN KEY (`id_laporan_komoditas`) REFERENCES `default_laporan_komoditas`(`id`)");

				$this->version = '1.5';

				break;
				
			case '1.5':
				$this->db->query("ALTER TABLE `default_laporan_data` CHANGE `id_absen` `id_absen` INT(11) UNSIGNED NULL");
				$fields = array(
					'minggu_ke' => array(
						'type' => 'INT',
						'default' => NULL,
						'null' => TRUE
					),
					'id_group' => array(
						'type' => 'INT',
						'default' => 7,
					),
					'is_ttic' => array(
						'type' => 'INT',
						'constraint' => 1,
						'default' => 0,
					),
				);

				$this->dbforge->add_column('laporan_data', $fields);
				$this->db->query("CREATE INDEX id_group ON default_laporan_data(id_group)");
				$this->db->query("ALTER TABLE `default_laporan_data` ADD CONSTRAINT `fk_data_group` FOREIGN KEY (`id_group`) REFERENCES `default_groups`(`id`)");

				$this->db->query("INSERT INTO `default_laporan_tipe` (`id`, `nama_laporan`, `kode_sms`) VALUES (3, 'TTIC', 'TTIC'), (4, 'Gapoktan 2017', 'GAP17'), (5, 'TTI 2017', 'TTI17')");

				$this->db->query("INSERT INTO `default_organization_types` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `type_name`, `type_description`, `type_slug`, `type_level`, `available_groups`) VALUES (3, '2017-03-06 03:15:46', '2017-03-09 03:14:41', 1, NULL, 'TTIC', '', 'ttic', 0, '[9]'), (4, '2017-03-06 03:16:36', '2017-03-09 03:14:44', 1, NULL, 'Gapoktan 2017', '', 'gapoktan-2017', 1, '[9]'), (5, '2017-03-06 03:16:53', '2017-03-09 03:14:48', 1, NULL, 'TTI 2017', '', 'tti-2017', 0, '[9]')");

				$this->version = '1.6';
				break;

			case '1.6':
			
				$fields = array(
					'kode_komoditas' => array(
						'type' => 'VARCHAR',
						'constraint' => '45'
					)
				);

				$this->dbforge->add_column('laporan_komoditas', $fields);

				$this->db->query("UPDATE `default_groups` SET `name` = 'p16', `description` = 'Pendamping Mingguan', `sort_order` = '4' WHERE `default_groups`.`id` = 7");
				$this->db->query("UPDATE `default_groups` SET `sort_order` = '3' WHERE `default_groups`.`id` = 8");
				$this->db->query("INSERT INTO `default_groups` (`id`, `name`, `description`, `sort_order`, `force_fill_profile`, `enable_personel_link`, `force_create_personel_link`) VALUES (9, 'p17', 'Pendamping Harian', 5, 0, 0, 0)");

				$this->db->query("INSERT INTO `default_permissions` (`id`, `group_id`, `module`, `roles`) VALUES (470, 9, 'users', '{\"view_own_account\":\"1\",\"edit_own_account\":\"1\",\"edit_own_email\":\"1\",\"edit_own_username\":\"1\",\"edit_own_password\":\"1\",\"view_own_profile\":\"1\",\"edit_own_profile\":\"1\",\"edit_own_picture\":\"1\"}'), (471, 9, 'organization', '{\"access_units_backend\":\"1\",\"view_own_units\":\"1\",\"view_own_memberships\":\"1\",\"delete_own_memberships\":\"1\",\"create_own_units_memberships\":\"1\"}'), (472, 9, 'laporan', '{\"access_laporan_backend\":\"1\",\"create_own_laporan\":\"1\",\"view_own_laporan\":\"1\",\"edit_own_laporan\":\"1\",\"access_sms_backend\":\"1\",\"send_sms\":\"1\",\"view_own_sms\":\"1\",\"api_laporan\":\"1\"}')");

				$this->version = '1.7';
				break;
		}

		return true;
  }

  public function help()
  {
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
  }

}