<style>
  .loading_content{
    color: #a7b2c0;
    text-align: center;
    margin: 10px;
  }
  .img-center{
    margin-left: auto;
    margin-right: auto;
  }
</style>

<div class="page-content">
  <div class="container">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
      <br>
      <div class="row">
        <div class="col-md-5 col-sm-5">
          <iframe width="100%" height="220" src="https://www.youtube.com/embed/86UXqkxwv3o?autoplay=1&loop=1&playlist=86UXqkxwv3o" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="col-md-7 col-sm-7">
          <img src="<?php echo base_url() ?>addons/default/themes/sitoni/img/TONI.jpg" style="width:100%; height:219px;">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
          <div class="dashboard-stat blue">
            <div class="visual">
              <i class="fa fa-group fa-icon-medium"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo number_format($total_pendamping) ?> </div>
              <div class="desc"> Total Pendamping 2016</div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/pendamping/index"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-home"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo number_format($total_gapoktan) ?> </div>
              <div class="desc"> Total Gapoktan 2016</div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/sumberdata/index?f-tipe_laporan=1"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="dashboard-stat green">
            <div class="visual">
              <i class="fa fa-building-o fa-icon-medium"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo number_format($total_tti) ?> </div>
              <div class="desc"> Total TTI 2016</div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/sumberdata/index?f-tipe_laporan=2"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
          <div class="dashboard-stat yellow">
            <div class="visual">
              <i class="fa fa-group fa-icon-medium"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo (isset($total_pendamping_2017)) ? number_format($total_pendamping_2017) : 0; ?> </div>
              <div class="desc"> Total Pendamping 2017 </div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/pendamping/pendamping_harian"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="dashboard-stat purple">
            <div class="visual">
                <i class="fa fa-home"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo (isset($total_gapoktan_2017)) ? number_format($total_gapoktan_2017) : 0; ?> </div>
              <div class="desc"> Total Gapoktan 2017 </div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/sumberdata/index?f-tipe_laporan=4"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="dashboard-stat yellow">
            <div class="visual">
              <i class="fa fa-building-o fa-icon-medium"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo (isset($total_tti_2017)) ? number_format($total_tti_2017) : 0; ?> </div>
              <div class="desc"> Total TTI 2017 </div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/sumberdata/index?f-tipe_laporan=5"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
          <div class="dashboard-stat yellow" style="background: cadetblue;">
            <div class="visual">
              <i class="fa fa-group fa-icon-medium"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo (isset($total_pendamping_2018)) ? number_format($total_pendamping_2018) : 0; ?> </div>
              <div class="desc"> Total Pendamping 2018 </div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/pendamping/pendamping_harian"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="dashboard-stat yellow" style="background: cadetblue;">
            <div class="visual">
                <i class="fa fa-home"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo (isset($total_gapoktan_2018)) ? number_format($total_gapoktan_2018) : 0; ?> </div>
              <div class="desc"> Total Gapoktan 2018 </div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/sumberdata/index?f-tipe_laporan=4"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="dashboard-stat yellow" style="background: cadetblue;">
            <div class="visual">
              <i class="fa fa-building-o fa-icon-medium"></i>
            </div>
            <div class="details">
              <div class="number"> <?php echo (isset($total_tti_2018)) ? number_format($total_tti_2018) : 0; ?> </div>
              <div class="desc"> Total TTI 2018 </div>
            </div>
            <a class="more" href="<?php echo base_url() ?>laporan/sumberdata/index?f-tipe_laporan=5"> Selengkapnya
              <i class="m-icon-swapright m-icon-white"></i>
            </a>
          </div>
        </div>
      </div>

      <!-- BEGIN ROW -->
      <div class="row">
        <div class="col-md-7 col-sm-7">
          <!-- 10 laporan terakhir -->
          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption caption-md">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark uppercase bold">10 Laporan Terakhir</span>
              </div>
            </div>
            <div class="portlet-body">
              <div class="scroller" style="height: 200px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2" id="ten_last_report">
                <div class="loading_content">
                  <i class='fa fa-spinner fa-pulse fa-2x'></i>
                </div>
              </div>
            </div>
          </div>

          <!-- Grafik GAPOKTAN -->
          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption caption-md">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark uppercase bold">Grafik Laporan Gapoktan</span>
              </div>
              <div class="actions">
                <a class="btn btn-transparent red-sunglo  btn-sm" href="<?php echo base_url() ?>laporan/data/harga/1">Selengkapnya  <i class="fa fa-share"></i></a>
              </div>
            </div>
            <div class="portlet-body form-inline" style="margin-bottom: 10px;">
              <div class="col-12 form-group">
                <label for="gap" class="font-dark">Laporan</label>
                <select id="id_tipe_laporan_gap" class="form-control input-sm change_tipe_gap">
                  <option value="6">Gapoktan 2018</option>
                  <option value="4">Gapoktan 2017</option>
                  <option value="1">Gapoktan 2016</option>
                </select>

                <label for="gap" class="font-dark">Komoditas</label>
                <select id="id_komoditas_gap"  class="form-control input-sm change_komoditas_gap">
                  <?php foreach ($komoditas_gap as $key){ ?>
                    <option value="<?php echo $key['id'] ?>" ><?php echo $key['nama_komoditas'] ?></option>
                  <?php } ?>
                  <option value="">Semua Komoditas</option>
                </select>
              </div>
            </div>
            <div class="portlet-body" id="laporan_gapoktan">
              <div class="loading_content">
                <i class='fa fa-spinner fa-pulse fa-2x'></i>
              </div>
            </div>
          </div>

          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption caption-md">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark uppercase bold">Grafik Laporan TTI </span>
              </div>
              <div class="actions">
                <a class="btn btn-transparent red-sunglo  btn-sm" href="<?php echo base_url() ?>laporan/data/harga/2">Selengkapnya  <i class="fa fa-share"></i></a>
              </div>
            </div>
            <div class="portlet-body form-inline" style="margin-bottom: 10px;">
              <div class="col-12 form-group">
                <label for="tti" class="font-dark">Laporan</label>
                <select id="id_tipe_laporan_tti" class="form-control input-sm change_tipe_tti">
                  <option value="7">TTI 2018</option>
                  <option value="5">TTI 2017</option>
                  <option value="2">TTI 2016</option>
                </select>

                <label for="tti" class="font-dark">Komoditas</label>
                <select id="id_komoditas_tti"  class="form-control input-sm change_komoditas_tti">
                  <?php foreach ($komoditas_tti as $key){ ?>
                    <option value="<?php echo $key['id'] ?>" ><?php echo $key['nama_komoditas'] ?></option>
                  <?php } ?>
                  <option value="">Semua Komoditas</option>
                </select>
              </div>
            </div>
            <div class="portlet-body" id="laporan_tti">
              <div class="loading_content">
                <i class='fa fa-spinner fa-pulse fa-2x'></i>
              </div>
            </div>
          </div>

          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption caption-md">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark uppercase bold">Grafik Laporan TTIC </span>
              </div>
              <div class="actions">
                <a class="btn btn-transparent red-sunglo  btn-sm" href="<?php echo base_url() ?>laporan/data/harga/3">Selengkapnya  <i class="fa fa-share"></i></a>
              </div>
            </div>
            <div class="portlet-body form-inline" style="margin-bottom: 10px;">
              <div class="col-12 form-group">
              <label for="ttic" class="font-dark">Komoditas</label>
                <select id="ttic" onchange="change_laporan_ttic(this.value);" class="form-control input-sm">
                  <?php foreach ($komoditas_ttic as $key){ ?>
                    <option value="<?php echo $key['id'] ?>" ><?php echo $key['nama_komoditas'] ?></option>
                  <?php } ?>
                  <option value="">Semua Komoditas</option>
                </select>
              </div>
            </div>
            <div class="portlet-body" id="laporan_ttic">
              <div class="loading_content">
                <i class='fa fa-spinner fa-pulse fa-2x'></i>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-5 col-sm-5">

          <!-- Pengumuman -->
          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption caption-md">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark uppercase bold">Pengumuman</span>
              </div>
            </div>
            <div class="portlet-body" id="pengumuman">
              <div>
                {{ files:listing folder="6" }}
                  <img src="{{ url:site }}files/thumb/{{ id }}/300" alt="{{ description }}" class="img-responsive img-center" />
                {{ /files:listing }}
              </div>
            </div>
          </div>


          <!-- Harga tertinggi/terendah Gapoktan -->
          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption caption-md">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark uppercase bold">Harga Gapoktan/TTI Minggu ini</span>
              </div>
            </div>
            <div class="portlet-body" style="margin-bottom:20px;">
              <div class="row">
                <div class="col-sm-6">
                  <label for="gap" class="font-dark">Laporan</label>
                  <select id="tipe_laporan_hrg" class="form-control input-sm change_tipe_hrg">
                    <option value="10">Pendamping 2018</option>
                    <option value="9">Pendamping 2017</option>
                    <option value="7">Pendamping 2016</option>
                  </select>
                </div>
                <div class="col-sm-6">
                  <label for="max" class="font-dark">Komoditas</label>
                  <select class="form-control input-sm change_komoditas_hrg" id="id_komoditas_hrg">
                    <option value="">Semua Komoditas</option>
                    <?php foreach ($komoditas as $key){ ?>
                      <option value="<?php echo $key['id'] ?>" ><?php echo $key['nama_komoditas'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="portlet-body" id="harga_gap_tti">
              <div class="loading_content">
                <i class='fa fa-spinner fa-pulse fa-2x'></i>
              </div>
              
            </div>
          </div>

          <!-- Harga tertinggi/terendah TTIC -->
          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption caption-md">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark uppercase bold">Harga Pangan Harian Tingkat TTIC</span>
              </div>
            </div>
            <div class="portlet-body form-inline" style="margin-bottom:20px;">
              <div class="col-12 form-group">
                <label for="max" class="font-dark">Komoditas</label>
                <select class="form-control input-sm" onchange="change_harga_ttic(this.value)">
                  <option value="">Semua Komoditas</option>
                  <?php foreach ($komoditas as $key){ ?>
                    <option value="<?php echo $key['id'] ?>" ><?php echo $key['nama_komoditas'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="portlet-body" id="harga_ttic">
              <div class="loading_content">
                <i class='fa fa-spinner fa-pulse fa-2x'></i>
              </div>
              
            </div>
          </div>

          <div class="portlet light">
            <div class="portlet-title">
              <div class="caption caption-md">
                <i class="icon-bar-chart font-dark hide"></i>
                <span class="caption-subject font-dark uppercase bold">Dialog Interaktif</span>
              </div>

              <div class="actions">
                <a class="btn btn-sm red-sunglo " data-toggle="modal" href="#form-dialog"><i class="fa fa-commenting-o"></i> Isi Dialog </a>
              </div>
            </div>
            <div class="portlet-body">
              <div class="scroller" style="height: 313px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2" id="dialog_interaktif"> 
                <div class="loading_content">
                  <i class='fa fa-spinner fa-pulse fa-2x'></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END ROW -->
    </div>
  </div>
</div>

<script src="<?php echo base_url() ?>addons/default/modules/laporan/js/highcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>addons/default/modules/laporan/js/exporting.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var id_komoditas = <?php echo $komoditas_first['id']; ?>;
    console.log(id_komoditas);
    change_laporan_gapoktan(6, id_komoditas);
    change_laporan_tti(7, id_komoditas);
    change_laporan_ttic(id_komoditas);
    change_harga_gap_tti(10);
    change_harga_ttic();
    load_dialog();
    load_ten_last_report();
  });

   $(".change_tipe_hrg, .change_komoditas_hrg").change(function(){
    var tipe_laporan = $("#tipe_laporan_hrg").val();
    var id_komoditas = $("#id_komoditas_hrg").val();
    change_harga_gap_tti(tipe_laporan, id_komoditas);
  });

  function change_harga_gap_tti(tipe_laporan, id_komoditas='null'){
    $("#harga_gap_tti").html("<div class='loading_content'><i class='fa fa-spinner fa-pulse fa-2x'></i></div>");
    $("#harga_gap_tti").load("<?php echo site_url('laporan/data/harga_gap_tti/') ?>" + '/' + tipe_laporan + '/' + id_komoditas, function() {});
  }


  function change_harga_ttic(id_komoditas='null'){
    $("#harga_ttic").html("<div class='loading_content'><i class='fa fa-spinner fa-pulse fa-2x'></i></div>");
    $("#harga_ttic").load("<?php echo site_url('laporan/data/harga_ttic/') ?>" + '/' + id_komoditas, function() {});
  }

  $(".change_tipe_gap, .change_komoditas_gap").change(function(){
    var id_tipe_laporan = $("#id_tipe_laporan_gap").val();
    var id_komoditas = $("#id_komoditas_gap").val();
    change_laporan_gapoktan(id_tipe_laporan, id_komoditas);
  });

  function change_laporan_gapoktan(id_tipe_laporan, id_komoditas){
    $("#laporan_gapoktan").html("<div class='loading_content'><i class='fa fa-spinner fa-pulse fa-2x'></i></div>");
    console.log('tes');
    $("#laporan_gapoktan").load("<?php echo site_url('laporan/data/grafik_laporan/') ?>" + '/'+id_tipe_laporan+'/' + id_komoditas, function() {});
  }

  $(".change_tipe_tti, .change_komoditas_tti").change(function(){
    var id_tipe_laporan = $("#id_tipe_laporan_tti").val();
    var id_komoditas = $("#id_komoditas_tti").val();
    change_laporan_tti(id_tipe_laporan, id_komoditas);
  });

  function change_laporan_tti(id_tipe_laporan, id_komoditas){
    $("#laporan_tti").html("<div class='loading_content'><i class='fa fa-spinner fa-pulse fa-2x'></i></div>");
    $("#laporan_tti").load("<?php echo site_url('laporan/data/grafik_laporan/') ?>" + '/'+id_tipe_laporan+'/' + id_komoditas, function() {});
  }

  function change_laporan_ttic(id_komoditas){
    $("#laporan_ttic").html("<div class='loading_content'><i class='fa fa-spinner fa-pulse fa-2x'></i></div>");
    $("#laporan_ttic").load("<?php echo site_url('laporan/data/grafik_laporan/') ?>" + '/3/' + id_komoditas, function() {});
  }

  function load_dialog(){
    $("#dialog_interaktif").load("<?php echo site_url('laporan/data/dialog_interaktif/') ?>", function() {});
  }

  function load_ten_last_report(){
    $("#ten_last_report").load("<?php echo site_url('laporan/data/ten_last_report/') ?>", function() {});
    var waktu = setTimeout("load_ten_last_report()",3000);
  }
</script>