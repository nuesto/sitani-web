<div class="c-comment-list">
    <?php if ($comments): ?>
        <?php foreach ($comments as $item): ?>
            <div class="media">
                <div class="media-body">
                    <h4 class="media-heading">
                        <a href="#"><?php echo $item->user_name; ?></a> on
                        <span class="c-date"><?php echo format_date($item->created_on); ?></span>
                    </h4>
                        <?php if (Settings::get('comment_markdown') AND $item->parsed > ''): ?>
                            <?php echo $item->parsed; ?>
                        <?php else: ?>
                            <?php echo nl2br($item->comment); ?>
                        <?php endif; ?>
                </div>
            </div>

        <?php endforeach; ?>

    <?php else: ?>
        <p><?php echo lang('comments:no_comments'); ?></p>
    <?php endif; ?>
</div>