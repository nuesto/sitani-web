<div class="blog-content-2">
    {{ post }}
    <div class="blog-single-content bordered blog-container" style="overflow-y: auto">
        <div class="blog-single-head">
            <h1 class="blog-single-head-title">{{ title }}</h1>
            <div class="blog-single-head-date">
                <i class="icon-calendar font-blue"></i>
                <a href="javascript:;">{{ helper:date format="d" timestamp=created_on }}{{ helper:date format="M" timestamp=created_on }}</a>
            </div>
        </div>
        {{if image:img}}
            <div class="blog-single-img">
                {{image:img}}
            </div>
        {{endif}}
        <div class="blog-single-desc">
            {{ body }}
        </div>

        <div class="blog-comments" style="display:block; margin-top:25px;">
            <h3 class="sbold blog-comments-title">{{ comments:count_string entry_id=post:id entry_key="blog:post" [module="blog"] }}</h3>
            <?php
            if (Settings::get('enable_comments')){
                echo $this->comments->display() ;
                if ($form_display){echo $this->comments->form();}
                else{echo sprintf(lang('blog:disabled_after'), strtolower(lang('global:duration:'.str_replace(' ', '-', $post[0]['comments_enabled']))));}
            }
             ?>
        </div>
    </div>

    {{/post}}
</div>
