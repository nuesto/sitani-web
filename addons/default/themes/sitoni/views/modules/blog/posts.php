<div class="blog-content-1">
    {{ if posts }}

        {{ posts }}
            <div class="blog-post-lg bordered blog-container">
                {{if image:img}}
                    <div class="blog-img-thumb">
                        <a href="javascript:;">
                           {{image:img}}
                        </a>
                    </div>
                {{endif}}
                <div class="blog-post-content">
                    <h2 class="blog-title blog-post-title">
                        <a href="javascript:;">{{ title }}</a>
                    </h2>
                    <p class="blog-post-desc">{{ preview }}</p>
                    <div class="blog-post-foot">
                        <ul class="blog-post-tags">
                            <li class="uppercase">
                                <a href="{{ url }}">{{ helper:lang line="blog:read_more_label" }}</a>
                            </li>
                        </ul>
                        <div class="blog-post-meta">
                            <i class="icon-calendar font-blue"></i>
                            <a href="javascript:;">{{ helper:date format="d" timestamp=created_on }} {{ helper:date format="M" timestamp=created_on }}</a>
                        </div>
                        <div class="blog-post-meta">
                            <i class="icon-bubble font-blue"></i>
                            <a href="{{url}}#comments"> {{ commented }}</a>
                        </div>
                    </div>
                </div>
            </div>
            
        {{ /posts }}
        
        {{ pagination }}

    {{ else }}
        
        {{ helper:lang line="blog:currently_no_posts" }}

    {{ endif }}
</div>