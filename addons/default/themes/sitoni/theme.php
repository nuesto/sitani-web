<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Sitoni extends Theme {

  public $name = 'Sitoni';
  public $author = 'teguhrizki';
  public $author_website = 'teguh.nuestodev.com';
  public $website = '';
  public $description = 'HTML Template for Sistem Informasi Toko Tani';
  public $version = '1.0.0';
  public $options = array(
    'color' => array(
      'title'         => 'color',
      'title'         => 'Color of the theme',
      'description'   => 'Please choose one of the predefined colors. to use your own color consult to documentation',
      'default'       => 'red',
      'type'          => 'select',
      'options'       => 'red=Red|green=Green',
      'is_required'   => TRUE
    ),
    'show_breadcrumbs' => array(
      'title'         => 'Show Breadcrumbs',
      'description'   => 'Would you like to display breadcrumbs?',
      'default'       => 'yes',
      'type'          => 'radio',
      'options'       => 'yes=Yes|no=No',
      'is_required'   => TRUE
    ),
  );
}

