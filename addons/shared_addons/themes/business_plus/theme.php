<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Business_plus extends Theme {

    public $name = 'Business Plus';
    public $author = 'Aditya Satrya';
    public $author_website = '';
    public $website = '';
    public $description = 'Responsive HTML 5 template';
    public $version = '1.0.0';
    public $options = array(
		'color' => array(
            'title'         => 'color',
            'title'         => 'Color of the theme',
            'description'   => 'Please choose one of the predefined colors. to use your own color consult to documentation',
            'default'       => 'blue',
            'type'          => 'select',
            'options'       => 'blue=Blue|green=Green',
            'is_required'   => TRUE
        )
	);
}

