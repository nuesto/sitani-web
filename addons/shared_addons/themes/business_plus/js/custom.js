jQuery(document).ready(function() {
  

/* ----------------------------------------------------------
  Vars
  ----------------------------------------------------------- */ 
  var Speed = 300;
  var EaseMethod = 'easeInOutCirc';
  var sliderAnimation = 'slide';
  var defaultColor = '#05acd6';




   /* -------------------------------------------------------------- 
     Fix for footer copyrights link , you can delete this 
     if you want .. 
    -------------------------------------------------------------- */
  jQuery('footer .copyrights p a').each(function(){
    if(jQuery(this).text() ==  'DesignPrefix.com')
    {
      jQuery(this).attr('href','http://designprefix.com');
    }
  });




   /* -------------------------------------------------------------- 
     tooltip
    -------------------------------------------------------------- */
    jQuery('.social .content a').each(function()
    {
       var a = jQuery(this);
       var title = '';
       if(a.attr('title') !== '' && a.attr('title') !== undefined)
       {
          title = a.attr('title');
       }
       else
       {
          title = a.attr('class');
       }


       // append the tooltip
       a.append('<span class="title" style="display: none;">'+title+'</span>');

       // show on hover
       a.hover(function()
       {
          jQuery(this).find('span.title').fadeIn(Speed, EaseMethod);
       },function()
       {  
          jQuery(this).find('span.title').fadeOut(Speed , EaseMethod);
       })

    });
  


/* -------------------------------------------------------------- 
  Placeholders for ie
  -------------------------------------------------------------- */
  /**
   * this will replace default input value with the placeholder
   * because ie does not support placeholder attribute
   */
  if(jQuery.browser.msie && parseInt(jQuery.browser.version , 10) < 10)
  {
      var inputs = jQuery('input[type=text] , textarea , input[type=email]');
      // loop through fields
      inputs.each(function() {

        var i = jQuery(this);
        
        var placeholder = jQuery(this).attr('placeholder');
        
        // check if placeholder attr is there
        if(placeholder != '' && placeholder !== 'undefined' && placeholder !== 'false')
        {
            // check if there's no value
            if(i.val() == '')
            {
              i.val(placeholder);
            }
        }

        // add focus and blur effects in placeholder are used 
        if(placeholder === i.val())
        {
            // remove the value when focus
            i.focus(function() {
              if(i.val() === placeholder)
              {
                i.val('');
              }
            });

            // add the value when blur
            i.blur(function() {
              if(i.val() == '')
              {
                i.val(placeholder);
              }
            });
        }
      });
  }
  
 /* -------------------------------------------------------------- 
   tinyscroll 
  -------------------------------------------------------------- */
  /**
   * Fix Width
   */
  jQuery('.tinyscroll .overview').each(function() {

    var g = 0;
    jQuery(this).find('.single-post').each(function()  {
      g += jQuery(this).width() + 20;
    });

    jQuery(this).css({width: g + 'px'});
  });

  jQuery('.tinyscroll').tinyscrollbar({ axis: 'x' });




 /* -------------------------------------------------------------- 
   Switch Index Page Style
  -------------------------------------------------------------- */
  jQuery('.left-side').each(function () {

    var container = jQuery(this).parent().parent().parent();
    var click = jQuery(this).find('span');
    container.find('.header .switcher span.grid').addClass('active');

    // hide classic blog
    container.find('.classic-blog').hide(0);

    click.click(function () {

      // toggle class
      var t = jQuery(this);
      click.removeClass('active');
      t.addClass('active');

      // show hidden div
       if(t.hasClass('grid'))
      {
          container.find('.classic-blog').hide(0);
          container.find('.grid-blog').show(0);
      }
      else if(t.hasClass('classic'))
      {
        container.find('.classic-blog').show(0);
        container.find('.grid-blog').hide(0);
      }
      


    });
    });

 


 /* -------------------------------------------------------------- 
   Responsive Menu
  -------------------------------------------------------------- */
  jQuery('section.navigation-section ul li.first').click(function() {
    var ul = jQuery(this).parent('ul');
    if(ul.hasClass('opened'))
    {
      ul.find('li:not(.first)').slideUp(Speed , EaseMethod);
      ul.removeClass('opened');
    }
    else
    {
      ul.find('li').slideDown(Speed , EaseMethod);
      ul.addClass('opened');
    }

    return false;
  });

  jQuery(window).resize(function() {
    if(jQuery(window).width() > 767)
    {
      jQuery('section.navigation-section ul li').css({display: 'block'});
      jQuery('section.navigation-section ul li.first').css({display: 'none'});
    }
    else
    {
      jQuery('section.navigation-section ul li').css({display: 'none'});
      jQuery('section.navigation-section ul li.first').css({display: 'block'});
    }
  })

 /* -------------------------------------------------------------- 
   Slider 
  -------------------------------------------------------------- */
  jQuery('#slider').flexslider({
    animation : sliderAnimation ,
    easing : EaseMethod ,
    animationSpeed : Speed ,
    controlNav: false,
    directionNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#sliderid"

  });


  jQuery('.slider.default-slider').flexslider({
    animation: 'sliderAnimation',
    easing: EaseMethod,
    animationSpeed: Speed,
    controlNav: true,
    directionNav: false,
    touch: true,
    keyboard: true
  });


  jQuery('#sliderid').flexslider({

    animation: sliderAnimation,
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 57,
    itemMargin: 5,
    asNavFor: '#slider'

  });


  // video slider
  jQuery('.widget-content .carousel').carousel({
    direction: 'vertical'
  });
  jQuery('.widget-content .carousel input').val('');

 /* -------------------------------------------------------------- 
   tabs widget
  -------------------------------------------------------------- */

  jQuery('.widget .tabs').each(function(){

    var links = jQuery(this).find('.links');
    var tabs = jQuery(this).find('.posts-tab');

    /**
     * Hide All Tabs
     */
    tabs.find('.post-tab').hide(0);

    // Show first tab
    tabs.find('.post-tab').first().show();

    // click event
    links.find('a').live('click' , function() {

      var hash = jQuery(this).attr('href');

      var hex = '#05acd6';

      if(jQuery.cookie && jQuery.cookie('b_theme_color_hex') != ''){
        hex = jQuery.cookie('b_theme_color_hex');
      }


      // remove class
      links.find('a').removeClass('active').css('border-top-color' , '#e4e4e4');

      // add class active to current anchor
      jQuery(this).addClass('active').css('border-top-color' , hex);

      // hide tabs
      tabs.find('.post-tab').fadeOut(0);

      // show tab
      tabs.find(hash).fadeIn(Speed);


      return false;
    });

  });


 /* -------------------------------------------------------------- 
   Top Menu
  -------------------------------------------------------------- */
  // store height value
  var topH = jQuery('.top-section .sixteen').height();

  // Hide top menu
  jQuery('.top-section .sixteen').height('2px').css({padding: '0px'}).addClass('hidden');

  // add class to toggle button
  jQuery('.top-section a.toggle').addClass('toggleDown');

  // click action 
  jQuery('.top-section a.toggle').live('click' , function() {

    var c = jQuery(this).parent().find('.sixteen');
    if(c.hasClass('hidden')) 
    {
      c.stop().animate({height: topH , padding: "15px 0px"} , {duration: Speed}).removeClass('hidden');
      jQuery(this).removeClass('toggleDown');
    }
    else
    {
      c.stop().animate({height: "2px" , padding: "0px"} , {duration: Speed}).addClass('hidden');
      jQuery(this).addClass('toggleDown');
    }

  });

 /* -------------------------------------------------------------- 
   prettyphoto
  -------------------------------------------------------------- */
  jQuery('.zoom').prettyPhoto({theme: 'dark_rounded'});




 /* -------------------------------------------------------------- 
   badged opacity
  -------------------------------------------------------------- */
  jQuery('.widget .social-badges div a').css({position: 'relative'}).hover(function() {
    jQuery(this).stop().animate({opacity: 0.5} , {duration: Speed});
  },
  function(){
    jQuery(this).stop().animate({opacity: 1.0} , {duration: Speed});
  });


   /* -------------------------------------------------------------- 
     subscribe form
    -------------------------------------------------------------- */
    jQuery('.subscribe-form .signup').css({display: "block"}).live('click' , function() {
      var t = jQuery(this);
      if(t.hasClass('faded'))
      {
        t.fadeOut(0).removeClass('faded');
        t.parent().find('input').fadeIn(Speed);
      }
      else
      {
        t.fadeOut(0).addClass('faded');
        t.parent().find('input').fadeIn(Speed);
      }

      return false;
    });




     /* -------------------------------------------------------------- 
       Hover On Post Effect
      -------------------------------------------------------------- */
      jQuery('.posts .single-post').each(function() {
          var post = jQuery(this);
          var postFormat = jQuery(this).find('.post-format').css({zIndex : 1});
          var w = postFormat.width();
          var h = postFormat.height();
          if(jQuery.cookie('b_theme_color_hex') != '')
          {
            colorhex = jQuery.cookie('b_theme_color_hex');
          }
          else
          {
            colorhex = defaultColor;
          }
          if(post.find('.post-format').length > 0)
          {   
              post.find('div.image').css({background: colorhex , overflow: 'hidden'});
              // hover
              post.find('> div.image').hover(function()
              {
                postFormat.stop().animate({width: '100%' , height: '100%'} , {duration: Speed}).css({backgroundPosition: '55% 45%'});
              } , function()
              {
                postFormat.stop().animate({width: w , height: h} , {duration: Speed}).queue(function(item) {
                  jQuery(this).css({backgroundPosition: '6px 6px'});
                });
              });
          }
      });



       /* Quick Accoridon */
      jQuery('.accordion').each(function() {
      var acc = $(this);
      // show first item content
      if(acc.hasClass('opened')) {
       jQuery(this).find('.item:first').find('.item-content').slideDown();
      }
      
      // when click
      jQuery(this).find('.head').click(function() {
          if(!jQuery(this).hasClass('head-active')){
        jQuery(acc).find('.head').removeClass('head-active').parent().find('.item-content').slideUp(Speed , EaseMethod);
        jQuery(this).parent().find('.item-content').slideDown(Speed , EaseMethod);
        jQuery(this).addClass('head-active');
          }
          return false; 
      });
        }); // End Accrodion



      /**
       * flickr feed
       */
      jQuery('.flickr-widget').html('').jflickrfeed({
        limit: 8,
        qstrings: {
          id: '44802888@N04'
        },
        itemTemplate: 
        '<div class="photo"><a href="{{image_b}}"><img src="{{image_s}}" alt="{{title}}" /></a></div>'
      });



    /**
     * Custom Twitter Feed
     */
    var twitterID = 'twitter', 
    twitterLimit = 4 ,
    jTwitter = 'https://api.twitter.com/1/statuses/user_timeline/'+twitterID+'.json?count='+twitterLimit+'&callback=?';

    // get tweets
    jQuery.getJSON(jTwitter , function(data) {

        var tweets = '';

        // loop and get tweets
        jQuery.each(data , function() {

          // tweet html
          var tweet = [
            '<div class="single-tweet">' ,
            '<p><span class="name"><a style="color: #fff; text-decoration: none;" href="http://twitter.com/'+this.user.name+'">@'+this.user.name+'</span></a> '+this.text ,
            ' <br /> <span class="date">'+jQuery.timeago(this.created_at)+'</span><p>' ,
            '</div>'].join("\n");

          tweets = tweets + tweet;

        });


        // append tweets to dom
        jQuery('.tweets').html(tweets);
    });

});
  