<div class="container">

<div class="left-side eleven columns">
  
	<!-- recentposts slider -->
	<div class="slider default-slider recent-posts section">
	<ul class="slides">	  
		{{ blog:posts limit="5" }}
		<li>
			<div class="image"><img src="{{image:thumb}}/700/350/fit" /></div>
			<div class="desc">
				<h3><a href="{{ url:site uri="blog/category/{{category:slug}}" }}">{{category:title}}</a></h3>
				<div class="p">
					<h2><a href="{{url}}">{{title}}</a></h2>
				</div>
			</div>
		</li>
		{{ /blog:posts }}
	  
	</ul>
	</div>
	<!-- end recent posts slider -->
  
	<!-- [145px cols] -->
	<div class="box145 style-default section">

	<!-- header -->
	<div class="header">
	  <h3><a href="#">Pilihan Editor</a></h3>
	  <span></span> </div>
	<!-- end header -->

	<!-- posts -->
	<div class="posts">
		{{ blog:posts limit="4" }}
		<div class="single-post">
			
			<div class="image">
				<a href="{{url}}" class="link post-format"></a>
				<img src="{{image:thumb}}/150/150/fit" />
			</div>
		
			<div class="content">
				<h4><a href="{{url}}">{{title}}</a></h4>
			</div>
		
		</div>
		{{ /blog:posts }}
	</div>
	<!-- end posts -->

	</div>
	<!-- end box145 -->
  
</div>

<!-- rightside [sidebar] -->
<div class="right-side five columns">
	<!-- widgets -->
	<div class="widgets">
	{{ widgets:area slug="sidebar" }}
	</div>
	<!-- end widgets -->
</div>
<!-- end right side -->

</div>

<div class="container">
	<!-- box 310 column -->
	<div class="box310 style-featured section">

	<div class="single-column">
		<div class="header">
			<h3><a href='{{ url:site uri="blog/category/politik" }}'>Politik</a></h3>
			<span></span>
		</div>
		
		{{ blog:posts category="politik" limit="3" }}
		
		{{if {helper:count identifier="count_politik"} == 1}}
		<div class="single-post featured">
			<div class="image">
				<img src="{{image:thumb}}/310/150/fit" />
			</div>
			<div class="meta">
				<div class="date">
					<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
				</div>
			</div>
			<h4><a href="{{url}}">{{title}}</a></h4>
		</div>
		
		<div class="style-grid">
		
		{{else}}
		
			<div class="grid-post">
				<div class="image"><img src="{{image:thumb}}/57/57/fit" /></div>
				<div class="meta">
					<div class="date">
						<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
					</div>
					<h5>
						<a href="{{url}}">{{title}}</a>
					</h5>
				</div>
			</div>
		
		{{endif}}
		
		{{ /blog:posts }}

		</div>
	  
		<!-- more -->
		<a href='{{ url:site uri="blog/category/politik" }}' class="view-more">Selengkapnya</a> 
	</div>

	<div class="single-column">
		<div class="header">
			<h3><a href='{{ url:site uri="blog/category/ekonomi" }}'>Ekonomi</a></h3>
			<span></span>
		</div>
		
		{{ blog:posts category="ekonomi" limit="3" }}
		
		{{if {helper:count identifier="count_ekonomi"} == 1}}
		<div class="single-post featured">
			<div class="image">
				<img src="{{image:thumb}}/310/150/fit" />
			</div>
			<div class="meta">
				<div class="date">
					<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
				</div>
			</div>
			<h4><a href="{{url}}">{{title}}</a></h4>
		</div>
		
		<div class="style-grid">
		
		{{else}}
		
			<div class="grid-post">
				<div class="image"><img src="{{image:thumb}}/57/57/fit" /></div>
				<div class="meta">
					<div class="date">
						<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
					</div>
					<h5>
						<a href="{{url}}">{{title}}</a>
					</h5>
				</div>
			</div>
		
		{{endif}}
		
		{{ /blog:posts }}

		</div>
	  
		<!-- more -->
		<a href='{{ url:site uri="blog/category/ekonomi" }}' class="view-more">Selengkapnya</a> 
	</div>
	
	<div class="single-column">
		<div class="header">
			<h3><a href='{{ url:site uri="blog/category/hukum" }}'>Hukum</a></h3>
			<span></span>
		</div>
		
		{{ blog:posts category="hukum" limit="3" }}
		
		{{if {helper:count identifier="count_hukum"} == 1}}
		<div class="single-post featured">
			<div class="image">
				<img src="{{image:thumb}}/310/150/fit" />
			</div>
			<div class="meta">
				<div class="date">
					<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
				</div>
			</div>
			<h4><a href="{{url}}">{{title}}</a></h4>
		</div>
		
		<div class="style-grid">
		
		{{else}}
		
			<div class="grid-post">
				<div class="image"><img src="{{image:thumb}}/57/57/fit" /></div>
				<div class="meta">
					<div class="date">
						<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
					</div>
					<h5>
						<a href="{{url}}">{{title}}</a>
					</h5>
				</div>
			</div>
		
		{{endif}}
		
		{{ /blog:posts }}

		</div>
	  
		<!-- more -->
		<a href='{{ url:site uri="blog/category/hukum}}" }}' class="view-more">Selengkapnya</a> 
	</div>

	</div>
	<!-- end box 310 -->
  
	<!-- box 310 column -->
	<div class="box310 style-featured section">

	<div class="single-column">
		<div class="header">
			<h3><a href='{{ url:site uri="blog/category/nasional" }}'>Nasional</a></h3>
			<span></span>
		</div>
		
		{{ blog:posts category="nasional" limit="3" }}
		
		{{if {helper:count identifier="count_nasional"} == 1}}
		<div class="single-post featured">
			<div class="image">
				<img src="{{image:thumb}}/310/150/fit" />
			</div>
			<div class="meta">
				<div class="date">
					<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
				</div>
			</div>
			<h4><a href="{{url}}">{{title}}</a></h4>
		</div>
		
		<div class="style-grid">
		
		{{else}}
		
			<div class="grid-post">
				<div class="image"><img src="{{image:thumb}}/57/57/fit" /></div>
				<div class="meta">
					<div class="date">
						<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
					</div>
					<h5>
						<a href="{{url}}">{{title}}</a>
					</h5>
				</div>
			</div>
		
		{{endif}}
		
		{{ /blog:posts }}

		</div>
	  
		<!-- more -->
		<a href='{{ url:site uri="blog/category/nasional" }}' class="view-more">Selengkapnya</a> 
	</div>
	
	<div class="single-column">
		<div class="header">
			<h3><a href='{{ url:site uri="blog/category/internasional" }}'>Internasional</a></h3>
			<span></span>
		</div>
		
		{{ blog:posts category="internasional" limit="3" }}
		
		{{if {helper:count identifier="count_internasional"} == 1}}
		<div class="single-post featured">
			<div class="image">
				<img src="{{image:thumb}}/310/150/fit" />
			</div>
			<div class="meta">
				<div class="date">
					<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
				</div>
			</div>
			<h4><a href="{{url}}">{{title}}</a></h4>
		</div>
		
		<div class="style-grid">
		
		{{else}}
		
			<div class="grid-post">
				<div class="image"><img src="{{image:thumb}}/57/57/fit" /></div>
				<div class="meta">
					<div class="date">
						<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
					</div>
					<h5>
						<a href="{{url}}">{{title}}</a>
					</h5>
				</div>
			</div>
		
		{{endif}}
		
		{{ /blog:posts }}

		</div>
	  
		<!-- more -->
		<a href='{{ url:site uri="blog/category/internasional" }}' class="view-more">Selengkapnya</a> 
	</div>
	
	<div class="single-column">
		<div class="header">
			<h3><a href='{{ url:site uri="blog/category/seni-budaya" }}'>Seni Budaya</a></h3>
			<span></span>
		</div>
		
		{{ blog:posts category="seni-budaya" limit="3" }}
		
		{{if {helper:count identifier="count_seni-budaya"} == 1}}
		<div class="single-post featured">
			<div class="image">
				<img src="{{image:thumb}}/310/150/fit" />
			</div>
			<div class="meta">
				<div class="date">
					<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
				</div>
			</div>
			<h4><a href="{{url}}">{{title}}</a></h4>
		</div>
		
		<div class="style-grid">
		
		{{else}}
		
			<div class="grid-post">
				<div class="image"><img src="{{image:thumb}}/57/57/fit" /></div>
				<div class="meta">
					<div class="date">
						<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
					</div>
					<h5>
						<a href="{{url}}">{{title}}</a>
					</h5>
				</div>
			</div>
		
		{{endif}}
		
		{{ /blog:posts }}

		</div>
	  
		<!-- more -->
		<a href='{{ url:site uri="blog/category/seni-budaya" }}' class="view-more">Selengkapnya</a> 
	</div>
	
	</div>
	<!-- end box 310 -->
	
</div>

