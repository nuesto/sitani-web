<div class="widget <?php echo $widget->slug ?>">
	<?php if ($widget->options['show_title']): ?>
		<div class="header">
			<h3><?php echo $widget->instance_title ?></h3>
			<span></span>
		</div>
	<?php endif ?>

	<div class="widget-content">
		<?php echo $widget->body ?>
	</div>
	
	<div class="divider"></div>
</div>