<div class="page-single">

	{{ post }}

	<!-- main image -->
	<div class="post-image">
		<img alt="" src="{{image:thumb}}/640/310/fit" />
	</div>
	<!-- end main image -->
	
	<!-- post meta -->
	<div class="post-meta">
		<h3>{{title}}</h3>
		
		<!-- date -->
		<div class="date">
			<p>{{ helper:date format="j F Y, H:i" timestamp={created} }} dalam 
			<a class="tag" hrfe='{{ url:site uri="blog/category/{{category:slug}}" }}'>{{category:title}}</a>
			</p>
		</div>
		<!-- end date -->
	</div>
	<!-- end post meta -->
	
	<!-- post content -->
	<div class="post-content section">
		<p>{{body}}</p>
	</div>
	<!-- end post content -->
	
	{{ /post }}
	
	<!-- [145px cols] -->
	<div class="box145 style-default section">
		<!-- header -->
		<div class="header">
			<h3>Berita Terkait</h3>
			<span></span>
		</div>
		<!-- end header -->
		
		<!-- posts -->
		<div class="posts">
			
			<?php foreach($post[0]['related_posts']['entries'] as $related_post){ ?>
			
			<div class="single-post">
				<div class="image" style="overflow: hidden;">
					<img alt="" src="<?php echo $related_post['image']['thumb'] ?>/145/125/fit">
				</div>

				<div class="content">
					<h4><a href="<?php echo $related_post['url'] ?>"><?php echo $related_post['title'] ?></a></h4>
				</div>
			</div>
			
			<?php } //foreach ?>
			
		</div>
		<!-- end posts -->
	</div>
	<!-- end box145 -->
	
	<?php if (Settings::get('enable_comments')): ?>

	<div class="comment-container section">
		<div class="header">
			<h3><?php echo lang('comments:title') ?></h3>
			<span></span>
		</div>
		
		<div class="comments">
			<?php echo $this->comments->display() ?>
		</div>
		
	</div>
	
	<?php if ($form_display): ?>
	<div class="comments-form section">
		<div class="header">
			<h3><?php echo lang('comments:your_comment') ?></h3>
			<span></span>
		</div>
		<div class="form">
			<?php echo $this->comments->form() ?>
		</div>
	</div>
	<?php else: ?>
	
	<?php echo sprintf(lang('blog:disabled_after'), strtolower(lang('global:duration:'.str_replace(' ', '-', $post[0]['comments_enabled'])))) ?>
	
	<?php endif ?>

	<?php endif ?>
	
</div>