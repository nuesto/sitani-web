<?php if(isset($category['title'])){ ?>
	
<div class="section">
	<div class="header">
		<h3><?php echo $category['title']; ?></h3>
		<span></span>
	</div>
</div>

<?php } ?>

<div class="classic-blog">

{{ if posts }}
	
	<div class="posts">
	
	{{ posts }}
	
		<div class="single-post">
			<div class="image">
				<a href="{{url}}"><img alt="" src="{{image:thumb}}/200/150/fit"></a>
			</div>
			
			<!-- meta -->
			<div class="meta">
				<h3><a href="{{url}}">{{title}}</a></h3>
				<div class="date">
					<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
					{{ if keywords }}
					<p> | <span class="tag">
						{{ keywords }}
							{{ keyword }}
						{{ /keywords }}
					</span> </p>
					{{ endif }}
				</div>
				<!-- end date -->
				
				<!-- content -->
				<div class="content">
					{{preview}}
					<a href="{{url}}" class="readmore">Selengkapnya -&gt; </a> .. </p>
				</div>
				<!-- end content -->
			</div>
			<!-- end meta -->
		</div>

	{{ /posts }}
	
	</div>

	{{ pagination }}

{{ else }}
	
	{{ helper:lang line="blog:currently_no_posts" }}

{{ endif }}

</div>