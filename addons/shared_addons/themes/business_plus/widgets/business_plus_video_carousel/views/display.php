<!-- carousel -->
<div class="carousel">
  <ul class="slides">
	<li>
	  <div class="post">
		<!-- video -->
		<div class="video">
		  <iframe src="http://player.vimeo.com/video/19251347?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=4cb3ad" width="304" height="170" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
		</div>
		<!-- end video -->
		<!-- meta -->
		<div class="meta">
		  <h5><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h5>
		  <!-- date -->
		  <div class="date">
			<p>By <a href="#">Admin</a> , 29 , Aug 2012</p>
			<span class="fav"> <a href="#" class="comments-link">28</a> <a href="#" class="like">50</a> </span> </div>
		  <!-- end date -->
		</div>
		<!-- end meta -->
	  </div>
	  <!-- end post -->
	</li>
	<li>
	  <div class="post">
		<!-- video -->
		<div class="video">
		  <iframe src="http://player.vimeo.com/video/49458056?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=4cb3ad" width="302" height="170" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
		</div>
		<!-- end video -->
		<!-- meta -->
		<div class="meta">
		  <h5><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h5>
		  <!-- date -->
		  <div class="date">
			<p>By <a href="#">Admin</a> , 29 , Aug 2012</p>
			<span class="fav"> <a href="#" class="comments-link">28</a> <a href="#" class="like">50</a> </span> </div>
		  <!-- end date -->
		</div>
		<!-- end meta -->
	  </div>
	  <!-- end post -->
	</li>
  </ul>
</div>