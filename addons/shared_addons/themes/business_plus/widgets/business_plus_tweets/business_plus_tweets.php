<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Display tweets
 * 
 * @author  	Aditya Satrya
 */
class Widget_Business_plus_tweets extends Widgets
{


	/**
	 * The widget title
	 *
	 * @var array
	 */
	public $title = '[Business Plus] Tweets';

	/**
	 * The translations for the widget description
	 *
	 * @var array
	 */
	public $description = array(
		'en' => 'Display tweets',
		'id' => 'Menampilkan tweet',
	);
	
	/**
	 * The author of the widget
	 *
	 * @var string
	 */
	public $author = 'Aditya Satrya';
	
	/**
	 * The website of the widget
	 *
	 * @var string
	 */
	public $website = '';

	/**
	 * The version of the widget
	 *
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * The fields for customizing the options of the widget.
	 *
	 * @var array 
	 */
	public $fields = array(
		array(),
	);

	/**
	 * The main function of the widget.
	 *
	 * @param array $options The options for displaying an HTML widget.
	 * @return array 
	 */
	public function run($options)
	{
		// Store the feed items
		return array('output' => '');
	}

}