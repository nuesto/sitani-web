<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Show Statistics of top contents
 * 
 * @author  	Aditya Satrya
 */
class Widget_Business_plus_content_tabs extends Widgets
{


	/**
	 * The widget title
	 *
	 * @var array
	 */
	public $title = '[Business Plus] Content Tabs';

	/**
	 * The translations for the widget description
	 *
	 * @var array
	 */
	public $description = array(
		'en' => 'Show most popular, newest contents',
		'id' => 'Menampilkan konten terpopuler dan terbaru',
	);
	
	/**
	 * The author of the widget
	 *
	 * @var string
	 */
	public $author = 'Aditya Satrya';
	
	/**
	 * The website of the widget
	 *
	 * @var string
	 */
	public $website = '';

	/**
	 * The version of the widget
	 *
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * The fields for customizing the options of the widget.
	 *
	 * @var array 
	 */
	public $fields = array(
		array(),
	);

	/**
	 * The main function of the widget.
	 *
	 * @param array $options The options for displaying an HTML widget.
	 * @return array 
	 */
	public function run($options)
	{
		// Store the feed items
		return array('output' => '');
	}

}