<div class="tabs">
	<!-- links -->
	<div class="links">
		<a href="#recent-posts-tab" class="active">Terbaru</a>
		<a href="#popular-posts-tab">Populer</a>
		<a href="#comments-posts-tab" class="comments-tab-link"></a>
	</div>
	<!-- end links -->
  
	<!-- posts tab -->
	<div class="posts-tab">
	
		<div id="recent-posts-tab" class="post-tab">
			{{ blog:posts limit="12" }}
			<div class="single-post">
				<div class="date">
					<p>{{ helper:date format="H:i" timestamp={created} }}</p>
				</div>
				<div class="meta">
					<a href="{{url}}">{{title}}</a>
				</div>
			</div>
			{{ /blog:posts }}
		</div>
		
		
		<div id="popular-posts-tab" class="post-tab">
			{{ blog:posts limit="6" }}
			<div class="single-post">
				<div class="image"><img src="{{image:thumb}}/57/57/fit" /></div>
				<div class="meta">
					<div class="date">
						<p>{{ helper:date format="j F Y, H:i" timestamp={created} }}</p>
					</div>
					<h4><a href="{{url}}">{{title}}</a></h4>
				</div>
			</div>
			{{ /blog:posts }}
		</div>
		
		<div id="comments-posts-tab" class="post-tab">
			{{ comments:display entry_key="blog:posts" [module="blog"] }}
		</div>
		
	</div>
	<!-- end posts tab -->
</div>