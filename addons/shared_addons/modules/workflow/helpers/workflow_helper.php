<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( ! function_exists('get_group_name'))
{
	function get_group_name($id = '')
	{
		$CI =& get_instance();
		$CI->load->model('groups/group_m');
		$result =  $CI->group_m->get_group_by_id($id);
		return $result->description;
	}

	function get_target_status($status_now, $rule_type, $value)
	{
		$CI =& get_instance();
		$CI->load->model('workflow/rule_m');
		$CI->load->model('workflow/state_m');
		$status =  $CI->rule_m->get_rule_by(array('id_domain_state'=>$status_now));
		foreach ($status as $stat) {
			$grant_roles = json_decode($stat['grant_roles'],true);
			if($rule_type=='by_user') {
				if(in_array($value, $grant_roles['grant_by_user'])) {
					$result[] = $stat['id_target_state'];
				}
			} elseif($rule_type=='by_group') {
				if(in_array($value, $grant_roles['grant_by_group'])) {
					$result[] = $stat['id_target_state'];
				}
			} elseif($rule_type=='by_organization') {
				if(in_array($value, $grant_roles['constraint_by_organization'])) {
					$result[] = $stat['id_target_state'];
				}
			} elseif($rule_type=='by_event') {
				if(in_array($value, $grant_roles['automatically_on_event'])) {
					$result[] = $stat['id_target_state'];
				}
			}

		}

		// get target state
		$target_state = NULL;

		if(isset($result) AND count($result)>0) {
			$target_state = $CI->state_m->get_state_by('s.id IN ('.implode(",", $result).')');
		}

		return $target_state;
	}
}