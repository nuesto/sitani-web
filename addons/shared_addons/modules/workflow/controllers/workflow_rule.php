<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Modul untuk mengelola workflow
 *
 */
class Workflow_rule extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'rule';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('workflow');
		
		$this->load->model('rule_m');
    }

    /**
	 * List all rule
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'view_all_rule') AND ! group_has_role('workflow', 'view_own_rule')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'workflow/rule/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->rule_m->count_all_rule();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['rule']['entries'] = $this->rule_m->get_rule($pagination_config);
		$data['rule']['total'] = count($data['rule']['entries']);
		$data['rule']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('workflow:rule:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('workflow:rule:plural'))
			->build('rule_index', $data);
    }
	
	/**
     * Display one rule
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'view_all_rule') AND ! group_has_role('workflow', 'view_own_rule')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['rule'] = $this->rule_m->get_rule_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('workflow', 'view_all_rule')){
			if($data['rule']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:rule:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:rule:plural'), '/workflow/rule/index')
			->set_breadcrumb(lang('workflow:rule:view'))
			->build('rule_entry', $data);
    }
	
	/**
     * Create a new rule entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'create_rule')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_rule('new')){	
				$this->session->set_flashdata('success', lang('workflow:rule:submit_success'));				
				redirect('workflow/rule/index');
			}else{
				$data['messages']['error'] = lang('workflow:rule:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'workflow/rule/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:rule:new'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:rule:plural'), '/workflow/rule/index')
			->set_breadcrumb(lang('workflow:rule:new'))
			->build('rule_form', $data);
    }
	
	/**
     * Edit a rule entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the rule to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'edit_all_rule') AND ! group_has_role('workflow', 'edit_own_rule')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'edit_all_rule')){
			$entry = $this->rule_m->get_rule_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_rule('edit', $id)){	
				$this->session->set_flashdata('success', lang('workflow:rule:submit_success'));				
				redirect('workflow/rule/index');
			}else{
				$data['messages']['error'] = lang('workflow:rule:submit_failure');
			}
		}
		
		$data['fields'] = $this->rule_m->get_rule_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'workflow/rule/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
        $this->template->title(lang('workflow:rule:edit'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:rule:plural'), '/workflow/rule/index')
			->set_breadcrumb(lang('workflow:rule:view'), '/workflow/rule/view/'.$id)
			->set_breadcrumb(lang('workflow:rule:edit'))
			->build('rule_form', $data);
    }
	
	/**
     * Delete a rule entry
     * 
     * @param   int [$id] The id of rule to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'delete_all_rule') AND ! group_has_role('workflow', 'delete_own_rule')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'delete_all_rule')){
			$entry = $this->rule_m->get_rule_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->rule_m->delete_rule_by_id($id);
		$this->session->set_flashdata('error', lang('workflow:rule:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('workflow/rule/index');
    }
	
	/**
     * Insert or update rule entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_rule($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('workflow:rule:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->rule_m->insert_rule($values);
			}
			else
			{
				$result = $this->rule_m->update_rule($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}