<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Modul untuk mengelola workflow
 *
 */
class Admin_state extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'state';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'access_workflow_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('workflow');		
		$this->load->model('state_m');
		$this->load->model('workflow_m');
    }

    /**
	 * List all state
     *
     * @return	void
     */
    public function index($id_workflow = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['state']['entries'] = $this->state_m->get_state($id_workflow);
		$data['state']['total'] = count($data['state']['entries']);
		$data['workflow'] = $this->workflow_m->get_workflow_by_id($id_workflow);

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		$this->template
            ->append_css('module::workflow.css')
            ->append_js('module::jquery.tablednd.0.7.min.js')
            ->append_js('module::prettify.js');
		
        $this->template->title(lang('workflow:state:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:workflow:singular'), 'admin/workflow/index')
			->set_breadcrumb($data['workflow']['name'], 'admin/workflow/state/index/'.$id_workflow)
			->set_breadcrumb(lang('workflow:state:plural'))
			->build('admin/state_index', $data);
    }
	
	/**
     * Display one state
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['state'] = $this->state_m->get_state_by_id($id);
		
		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('workflow:state:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:state:plural'), '/admin/workflow/state/index')
			->set_breadcrumb(lang('workflow:state:view'))
			->build('admin/state_entry', $data);
    }
	
	/**
     * Create a new state entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($id_workflow = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_state('new')){	
				$this->session->set_flashdata('success', lang('workflow:state:submit_success'));				
				redirect('admin/workflow/state/index/'.$id_workflow);
			}else{
				$data['messages']['error'] = lang('workflow:state:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/workflow/state/index/'.$id_workflow;
		$data['workflow'] = $this->workflow_m->get_workflow_by_id($id_workflow);
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:state:new'))
        	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:workflow:singular'), '/admin/workflow/index')
			->set_breadcrumb($data['workflow']['name'], 'admin/workflow/state/index/'.$id_workflow)
			->set_breadcrumb(lang('workflow:state:new'))
			->build('admin/state_form', $data);
    }
	
	/**
     * Edit a state entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the state to the be deleted.
     * @return	void
     */
    public function edit($id_workflow = 0, $id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_state('edit', $id)){	
				$this->session->set_flashdata('success', lang('workflow:state:submit_success'));				
				redirect('admin/workflow/state/index/'.$id_workflow);
			}else{
				$data['messages']['error'] = lang('workflow:state:submit_failure');
			}
		}
		
		$data['fields'] = $this->state_m->get_state_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/workflow/state/index/'.$id_workflow;
		$data['entry_id'] = $id;
		$data['workflow'] = $this->workflow_m->get_workflow_by_id($id_workflow);
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:state:edit'))
        	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:workflow:singular'), '/admin/workflow/index')
			->set_breadcrumb($data['workflow']['name'], 'admin/workflow/state/index/'.$id_workflow)
			->set_breadcrumb(lang('workflow:state:edit'))
			->build('admin/state_form', $data);
    }
	
	/**
     * Delete a state entry
     * 
     * @param   int [$id] The id of state to be deleted
     * @return  void
     */
    public function delete($id_workflow = 0, $id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
		$this->load->model('rule_m');

		$this->rule_m->delete_rule_by_id($id);
        $this->state_m->delete_state_by_id($id);

        $this->session->set_flashdata('error', lang('workflow:state:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/workflow/state/index/'.$id_workflow);
    }
	
	/**
     * Insert or update state entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_state($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('name', lang('workflow:name'), 'required');
		$this->form_validation->set_rules('slug', lang('workflow:slug'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->state_m->insert_state($values);
				
			}
			else
			{
				$result = $this->state_m->update_state($values, $row_id);
			}
		}
		
		return $result;
	}

	public function change_ordering_count(){

        $result = $_REQUEST["table-1"];
        $i = 0;
        foreach($result as $id) {
            $this->state_m->update_ordering_count($id,$i-1);
            $i++;
        }   
    }

	// --------------------------------------------------------------------------

}