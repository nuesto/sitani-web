<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Modul untuk mengelola workflow
 *
 */
class Admin_rule extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'rule';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'access_workflow_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('workflow');
        $this->load->helper('workflow');		
		$this->load->model('rule_m');
		$this->load->model('state_m');
		$this->load->model('workflow_m');
        $this->load->model('groups/group_m');
       
    }

    /**
	 * List all rule
     *
     * @return	void
     */
    public function index($id_workflow = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
        // Data
        // -------------------------------------

        $data['groups'] = $this->group_m->get_all();
        // dump($data['groups']);
        // die();
        $data['workflow_states'] = $this->state_m->get_state($id_workflow);
        $data['workflow_rules'] = $this->rule_m->get_rule_matrix($id_workflow);
		$data['workflow'] = $this->workflow_m->get_workflow_by_id($id_workflow);

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('workflow:rule:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:workflow:singular'), 'admin/workflow/index')
			->set_breadcrumb($data['workflow']['name'], 'admin/workflow/state/index/'.$id_workflow)
			->set_breadcrumb(lang('workflow:rule:plural'))
			->build('admin/rule_index', $data);
    }
	
	/**
     * Display one rule
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['rule'] = $this->rule_m->get_rule_by_id($id);
		
		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('workflow:rule:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:rule:plural'), '/admin/workflow/rule/index')
			->set_breadcrumb(lang('workflow:rule:view'))
			->build('admin/rule_entry', $data);
    }
	
	/**
     * Create a new rule entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_rule('new')){	
				$this->session->set_flashdata('success', lang('workflow:rule:submit_success'));				
				redirect('admin/workflow/rule/index');
			}else{
				$data['messages']['error'] = lang('workflow:rule:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/workflow/rule/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:rule:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:rule:plural'), '/admin/workflow/rule/index')
			->set_breadcrumb(lang('workflow:rule:new'))
			->build('admin/rule_form', $data);
    }
	
	/**
     * Edit a rule entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the rule to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_rule('edit', $id)){	
				$this->session->set_flashdata('success', lang('workflow:rule:submit_success'));				
				redirect('admin/workflow/rule/index');
			}else{
				$data['messages']['error'] = lang('workflow:rule:submit_failure');
			}
		}
		
		$data['fields'] = $this->rule_m->get_rule_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/workflow/rule/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:rule:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:rule:plural'), '/admin/workflow/rule/index')
			->set_breadcrumb(lang('workflow:rule:view'), '/admin/workflow/rule/view/'.$id)
			->set_breadcrumb(lang('workflow:rule:edit'))
			->build('admin/rule_form', $data);
    }
	
	/**
     * Delete a rule entry
     * 
     * @param   int [$id] The id of rule to be deleted
     * @return  void
     */
    public function delete($id_workflow = 0, $did = 0, $tid = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->rule_m->delete_rule_by_id(NULL,$did, $tid);
        $this->session->set_flashdata('error', lang('workflow:rule:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/workflow/rule/index/'.$id_workflow);
    }
	
	 /**
     * Insert or update Workflow transition entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
     * @param   int [$did] The domain id
     * @param   int [$tid] The target id
     * @return	boolean
     */
    private function _update_rule($method, $did = null, $tid = null) {
        // -------------------------------------
        // Load everything we need
        // -------------------------------------

        $this->load->helper(array('form', 'url'));

        // -------------------------------------
        // Set Values
        // -------------------------------------

        $values = $this->input->post();

        // -------------------------------------
        // Validation
        // -------------------------------------
        // Set validation rules
        $this->form_validation->set_rules('grant_roles', lang('workflow:grant_roles'), 'required');

        // Set Error Delimns
        $this->form_validation->set_error_delimiters('<div>', '</div>');

        $result = false;

        if ($this->form_validation->run() === true) {
            if ($method == 'new') {
                if (empty($values['id_domain_state'])) {
                    $values['id_domain_state'] = $did;
                }
                if (empty($values['id_target_state'])) {
                    $values['id_target_state'] = $tid;
                }
                $result = $this->rule_m->insert_rule($values);
            } else {
                $result = $this->rule_m->update_rule($values, $did, $tid);
            }
        } else {
            if($this->input->is_ajax_request()){
                header('Content-Type: application/json',TRUE,406);
                die (validation_errors());
            }

        }

        return $result;
    }

    /**
     * Edit a Workflow transition entry via ajax.
     * Add new if workflow transition does not exist.
     *
     * @param   int [$did] The domain id of Workflow transition to be edited
     * @param   int [$tid] The target id of Workflow transition to be edited
     * @return	void
     */
    public function ajax_edit($did = 0, $tid = 0, $is_new = 1) {
        // -------------------------------------
        // Check permission
        // -------------------------------------

       	if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }

        // -------------------------------------
        // Process POST input
        // -------------------------------------

        if ($_POST) {
            if ($this->_update_rule($is_new ? 'new' : 'edit', $did, $tid)) {
                die('' . lang('workflow:rule:submit_success'));
            } else {
                die('' . lang('workflow:rule:submit_failure'));
            }
        }
    }

	// --------------------------------------------------------------------------

}