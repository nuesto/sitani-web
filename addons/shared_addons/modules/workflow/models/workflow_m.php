<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Workflow model
 *
 * @author Aditya Satrya
 */
class Workflow_m extends MY_Model {
	
	public function get_workflow($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_workflow_workflow');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_workflow_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_workflow_workflow');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_workflow_by($params=NULL)
	{
		$this->db->select('*');

		if($params) {
			if(is_array($params)) {
				foreach ($params as $key => $value) {
					$this->db->where($key, $value);
				}
			} else {
				$this->db->where($params,NULL,FALSE);
			}
		}
		$query = $this->db->get('default_workflow_workflow');
		$result = $query->result_array();
		
		return $result;
	}
	
	public function count_all_workflow()
	{
		return $this->db->count_all('workflow_workflow');
	}
	
	public function delete_workflow_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_workflow_workflow');
	}
	
	public function insert_workflow($values)
	{
		
		return $this->db->insert('default_workflow_workflow', $values);
	}
	
	public function update_workflow($values, $row_id)
	{
		$this->db->where('id', $row_id);
		return $this->db->update('default_workflow_workflow', $values); 
	}
	
}