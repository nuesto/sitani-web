<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Workflow extends Module
{
    public $version = '1.1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Workflow',
			'id' => 'Workflow',
		);
		$info['description'] = array(
			'en' => 'Modul untuk mengelola workflow',
			'id' => 'Modul untuk mengelola workflow',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'Workflow';
		$info['roles'] = array(
			'access_workflow_backend',
			'manage_workflow',
		);

		if(group_has_role('workflow', 'access_workflow_backend')){
			$info['sections']['workflow']['name'] = 'workflow:workflow:plural';
			$info['sections']['workflow']['uri'] = 'admin/workflow/workflow/index';

			if(group_has_role('workflow', 'manage_workflow')){
				$info['sections']['workflow']['shortcuts']['create'] = array(
					'name' => 'workflow:workflow:new',
					'uri' => 'admin/workflow/workflow/create',
					'class' => 'add'
				);
			}
		}

		return $info;
    }

	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items, &$menu_order){
		unset($menu_items['lang:cp:nav_Workflow']);
		$menu_items['lang:cp:nav_structure']['lang:workflow:workflow:singular'] = 'admin/workflow/workflow/index';
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

        // workflow
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'description' => array(
				'type' => 'TEXT',
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('workflow_workflow', TRUE);


		// state
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_workflow' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'is_initial' => array(
				'type' => 'TINYINT',
				'default' => 0,
			),
			'is_final' => array(
				'type' => 'TINYINT',
				'default' => 0,
			),
			'is_content_editable' => array(
				'type' => 'TINYINT',
				'default' => 0,
			),
			'is_content_deletable' => array(
				'type' => 'TINYINT',
				'default' => 0,
			),
			'severity' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('workflow_state', TRUE);

		// rule
		$fields = array(
			'id_domain_state' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'id_target_state' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'grant_roles' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id_domain_state', TRUE);
		$this->dbforge->add_key('id_target_state', TRUE);
		$this->dbforge->create_table('workflow_rule', TRUE);
		$this->db->query("CREATE INDEX domain_state_index ON default_workflow_rule(id_domain_state)");
		$this->db->query("CREATE INDEX target_state_index ON default_workflow_rule(id_target_state)");

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('workflow_workflow');
        $this->dbforge->drop_table('workflow_state');
        $this->dbforge->drop_table('workflow_rule');
        return true;
    }

    public function upgrade($old_version)
    {
        switch ($old_version) {
			case '1.0':
                // convert all tables to InnoDB engine
                $this->db->query("ALTER TABLE `default_workflow_workflow` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_workflow_state` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_workflow_rule` ENGINE=InnoDB");

        }

        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}
