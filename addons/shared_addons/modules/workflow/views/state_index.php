<div class="page-header">	<h1>		<span><?php echo lang('workflow:state:plural'); ?></span>	</h1>		<?php if(group_has_role('workflow', 'create_state')){ ?>	<div class="btn-group content-toolbar">		<a class="btn btn-default btn-sm" href="<?php echo site_url('workflow/state/create'); ?>">			<i class="icon-plus"></i>			<span class="no-text-shadow"><?php echo lang('workflow:state:new'); ?></span>		</a>	</div>	<?php } ?></div><fieldset>	<legend><?php echo lang('global:filters') ?></legend>		<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>		<div class="form-group">
			<label><?php echo lang('workflow:id_workflow'); ?>:&nbsp;</label>
			<input type="text" name="f-id_workflow" value="<?php echo $this->input->get('f-id_workflow'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('workflow:name'); ?>:&nbsp;</label>
			<input type="text" name="f-name" value="<?php echo $this->input->get('f-name'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('workflow:slug'); ?>:&nbsp;</label>
			<input type="text" name="f-slug" value="<?php echo $this->input->get('f-slug'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('workflow:is_initial'); ?>:&nbsp;</label>
			<input type="text" name="f-is_initial" value="<?php echo $this->input->get('f-is_initial'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('workflow:is_final'); ?>:&nbsp;</label>
			<input type="text" name="f-is_final" value="<?php echo $this->input->get('f-is_final'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('workflow:is_content_editable'); ?>:&nbsp;</label>
			<input type="text" name="f-is_content_editable" value="<?php echo $this->input->get('f-is_content_editable'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('workflow:is_content_deletable'); ?>:&nbsp;</label>
			<input type="text" name="f-is_content_deletable" value="<?php echo $this->input->get('f-is_content_deletable'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('workflow:severity'); ?>:&nbsp;</label>
			<input type="text" name="f-severity" value="<?php echo $this->input->get('f-severity'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('workflow:ordering_count'); ?>:&nbsp;</label>
			<input type="text" name="f-ordering_count" value="<?php echo $this->input->get('f-ordering_count'); ?>">
		</div>
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">			<i class="icon-ok"></i>			<?php echo lang('buttons:submit'); ?>		</button>				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-danger btn-xs" type="reset">			<i class="icon-remove"></i>			<?php echo lang('buttons:clear'); ?>		</button>	<?php echo form_close() ?></fieldset><hr /><?php if ($state['total'] > 0): ?>		<p class="pull-right"><?php echo lang('workflow:showing').' '.count($state['entries']).' '.lang('workflow:of').' '.$state['total'] ?></p>		<table class="table table-striped table-bordered table-hover">		<thead>			<tr>				<th>No</th>				<th><?php echo lang('workflow:id_workflow'); ?></th>				<th><?php echo lang('workflow:name'); ?></th>				<th><?php echo lang('workflow:slug'); ?></th>				<th><?php echo lang('workflow:is_initial'); ?></th>				<th><?php echo lang('workflow:is_final'); ?></th>				<th><?php echo lang('workflow:is_content_editable'); ?></th>				<th><?php echo lang('workflow:is_content_deletable'); ?></th>				<th><?php echo lang('workflow:severity'); ?></th>				<th><?php echo lang('workflow:ordering_count'); ?></th>				<th><?php echo lang('workflow:created'); ?></th>				<th><?php echo lang('workflow:updated'); ?></th>				<th><?php echo lang('workflow:created_by'); ?></th>				<th></th>			</tr>		</thead>		<tbody>			<?php 			$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);			if($cur_page != 0){				$item_per_page = $pagination_config['per_page'];				$no = (($cur_page -1) * $item_per_page) + 1;			}else{				$no = 1;			}			?>						<?php foreach ($state['entries'] as $state_entry): ?>			<tr>				<td><?php echo $no; $no++; ?></td>				<td><?php echo $state_entry['id_workflow']; ?></td>				<td><?php echo $state_entry['name']; ?></td>				<td><?php echo $state_entry['slug']; ?></td>				<td><?php echo $state_entry['is_initial']; ?></td>				<td><?php echo $state_entry['is_final']; ?></td>				<td><?php echo $state_entry['is_content_editable']; ?></td>				<td><?php echo $state_entry['is_content_deletable']; ?></td>				<td><?php echo $state_entry['severity']; ?></td>				<td><?php echo $state_entry['ordering_count']; ?></td>							<?php if($state_entry['created']){ ?>				<td><?php echo format_date($state_entry['created'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<?php if($state_entry['updated']){ ?>				<td><?php echo format_date($state_entry['updated'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<td><?php echo user_displayname($state_entry['created_by'], true); ?></td>				<td class="actions">				<?php 				if(group_has_role('workflow', 'view_all_state')){					echo anchor('workflow/state/view/' . $state_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');				}elseif(group_has_role('workflow', 'view_own_state')){					if($state_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('workflow/state/view/' . $state_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');					}				}				?>				<?php 				if(group_has_role('workflow', 'edit_all_state')){					echo anchor('workflow/state/edit/' . $state_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');				}elseif(group_has_role('workflow', 'edit_own_state')){					if($state_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('workflow/state/edit/' . $state_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');					}				}				?>				<?php 				if(group_has_role('workflow', 'delete_all_state')){					echo anchor('workflow/state/delete/' . $state_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));				}elseif(group_has_role('workflow', 'delete_own_state')){					if($state_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('workflow/state/delete/' . $state_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));					}				}				?>				</td>			</tr>			<?php endforeach; ?>		</tbody>	</table>		<?php echo $state['pagination']; ?>	<?php else: ?>	<div class="well"><?php echo lang('workflow:state:no_entry'); ?></div><?php endif;?>