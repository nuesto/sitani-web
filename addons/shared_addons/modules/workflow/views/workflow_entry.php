<div class="page-header">
	<h1>
		<span><?php echo lang('workflow:workflow:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('workflow/workflow/index'); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('workflow:back') ?>
		</a>

		<?php if(group_has_role('workflow', 'edit_all_workflow')){ ?>
			<a href="<?php echo site_url('workflow/workflow/edit/'.$workflow['id']); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('workflow', 'edit_own_workflow')){ ?>
			<?php if($workflow->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('workflow/workflow/edit/'.$workflow['id']); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('workflow', 'delete_all_workflow')){ ?>
			<a href="<?php echo site_url('workflow/workflow/delete/'.$workflow['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('workflow', 'delete_own_workflow')){ ?>
			<?php if($workflow->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('workflow/workflow/delete/'.$workflow['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $workflow['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:name'); ?></div>
		<?php if(isset($workflow['name'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $workflow['name']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:slug'); ?></div>
		<?php if(isset($workflow['slug'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $workflow['slug']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:description'); ?></div>
		<?php if(isset($workflow['description'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $workflow['description']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:ordering_count'); ?></div>
		<?php if(isset($workflow['ordering_count'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $workflow['ordering_count']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:created'); ?></div>
		<?php if(isset($workflow['created'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($workflow['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:updated'); ?></div>
		<?php if(isset($workflow['updated'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($workflow['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($workflow['created_by'], true); ?></div>
	</div>
</div>