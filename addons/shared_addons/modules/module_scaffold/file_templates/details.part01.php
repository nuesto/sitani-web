<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_%module_class_name% extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => '%module_name%',
			'id' => '%module_name%',
		);
		$info['description'] = array(
			'en' => '%module_description%',
			'id' => '%module_description%',
		);
		$info['frontend'] = %module_has_frontend%;
		$info['backend'] = %module_has_backend%;
		$info['menu'] = '%module_menu%';
		$info['roles'] = array(
