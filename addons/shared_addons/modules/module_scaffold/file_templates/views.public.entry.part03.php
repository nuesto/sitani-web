	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('%module_slug%:created'); ?></div>
		<?php if(isset($%section_slug%['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($%section_slug%['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('%module_slug%:updated'); ?></div>
		<?php if(isset($%section_slug%['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($%section_slug%['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('%module_slug%:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($%section_slug%['created_by'], true); ?></div>
	</div>
</div>