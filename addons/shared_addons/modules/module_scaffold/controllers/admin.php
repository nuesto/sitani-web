<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * New Module Scaffold
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/module_scaffold/generator/index');
    }

}