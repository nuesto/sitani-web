<?php

$lang['cp:nav_piwik_analytics'] = 'Piwik Analytics';

/*****************************************************
 * Date Interval
 *****************************************************/
$lang['piwik_analytics:choose_interval'] = 'Choose Interval';
$lang['piwik_analytics:today'] = 'Today';
$lang['piwik_analytics:yesterday'] = 'Yesterday';
$lang['piwik_analytics:last7'] = 'Last 7 Days';
$lang['piwik_analytics:last30'] = 'Last 30 Days';