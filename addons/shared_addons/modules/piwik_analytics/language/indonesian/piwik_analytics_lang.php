<?php

$lang['cp:nav_piwik_analytics'] = 'Piwik Analytics';

/*****************************************************
 * Date Interval
 *****************************************************/
$lang['piwik_analytics:choose_interval'] = 'Pilih Rentang Waktu';
$lang['piwik_analytics:today'] = 'Hari ini';
$lang['piwik_analytics:yesterday'] = 'Kemarin';
$lang['piwik_analytics:last7'] = '7 Hari terakhir';
$lang['piwik_analytics:last30'] = '30 Hari terakhir';