<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Organization extends Module
{
    public $version = '1.5.3';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Organization',
			'id' => 'Organization',
		);
		$info['description'] = array(
			'en' => 'Module to manage organization',
			'id' => 'Module to manage organization',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'organization';
		$info['roles'] = array(
			'access_units_backend', 'view_all_units', 'view_own_units', 'view_provinsi_units', 'edit_all_units', 'edit_own_units', 'edit_provinsi_units', 'delete_all_units', 'delete_own_units', 'delete_provinsi_units', 'create_units', 'create_provinsi_units',
			'access_types_backend', 'view_all_types', 'view_own_types', 'edit_all_types', 'edit_own_types', 'delete_all_types', 'delete_own_types', 'create_types',
			'view_all_memberships', 'view_own_memberships', 'delete_all_memberships', 'delete_own_memberships', 'create_all_units_memberships', 'create_own_units_memberships',
            'api_organization'
		);

		if(group_has_role('organization', 'access_units_backend')){
			$info['sections']['units']['name'] = 'organization:units:plural';
			$info['sections']['units']['uri'] = 'admin/organization/units/index';

			if(group_has_role('organization', 'create_units') OR group_has_role('organization', 'create_provinsi_units')){
				$info['sections']['units']['shortcuts']['create'] = array(
					'name' => 'organization:units:new',
					'uri' => 'admin/organization/units/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('organization', 'access_types_backend')){
			$info['sections']['types']['name'] = 'organization:types:plural';
			$info['sections']['types']['uri'] = 'admin/organization/types/index';

			if(group_has_role('organization', 'create_types')){
				$info['sections']['types']['shortcuts']['create'] = array(
					'name' => 'organization:types:new',
					'uri' => 'admin/organization/types/create',
					'class' => 'add'
				);
			}
		}

		return $info;
    }

    /**
     * Admin menu
     *
     * If a module has an admin_menu function, then
     * we simply run that and allow it to manipulate the
     * menu array
     */
    public function admin_menu(&$menu_items, &$menu_order){
        $menu_items['lang:cp:nav_organization'] = array();
        if(group_has_role('organization', 'access_units_backend')){
            $menu_items['lang:cp:nav_organization']['lang:organization:units:plural']['urls'] = array('admin/organization/units/index','admin/organization/units/create','admin/organization/units/view%1','admin/organization/units/edit%1','admin/organization/memberships/index%1','admin/organization/units/tanpa_pendamping','admin/organization/units/import','admin/organization/units/view%2','admin/organization/memberships/index%2','admin/organization/units/edit%2','admin/organization/units/edit%3','admin/organization/units/view%2','admin/organization/units/tanpa_pendamping%1','admin/organization/units/view%3','admin/organization/memberships/index%2');
        }
        if(group_has_role('organization', 'access_types_backend')){
            $menu_items['lang:cp:nav_organization']['lang:organization:types:plural']['urls'] = array('admin/organization/types/index','admin/organization/types/create','admin/organization/types/edit%1');
        }
    }

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

        // Table organization_types

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'type_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'type_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'type_description' => array(
                'type' => 'LONGTEXT',
            ),
            'type_level' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'available_groups' => array(
                'type' => 'TEXT',
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_types', TRUE);

        // Table organization_units

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'unit_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'unit_abbrevation' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'unit_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'unit_description' => array(
                'type' => 'LONGTEXT',
            ),
            'unit_type' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'unit_sort_order' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_units', TRUE);
        $this->db->query("CREATE INDEX types_index ON default_organization_units(unit_type)");

        // Table organization_units_units

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'row_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'units_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_units_units', TRUE);

        // Table organization_titles

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'title_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'title_description' => array(
                'type' => 'LONGTEXT',
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_titles', TRUE);

        // Table organization_memberships

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'membership_unit' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'membership_user' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'membership_title' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'membership_is_head' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_memberships', TRUE);

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
        $this->load->dbforge();

        $this->dbforge->drop_table('organization_memberships');
        $this->dbforge->drop_table('organization_titles');
        $this->dbforge->drop_table('organization_units_units');
        $this->dbforge->drop_table('organization_units');
        $this->dbforge->drop_table('organization_types');

        // $this->load->driver('Streams');

        // // For this teardown we are using the simple remove_namespace
        // // utility in the Streams API Utilties driver.
        // $this->streams->utilities->remove_namespace('organization');

        return true;
    }

    public function upgrade($old_version)
    {
      switch($old_version){

        case '1.0':

            // add fields

            $fields = array(
                'type_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 256,
                ),
                'type_level' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                ),
            );

            $this->dbforge->add_column('organization_types', $fields);

            $fields = array(
                'unit_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 256,
                ),
            );

            $this->dbforge->add_column('organization_units', $fields);

            // delete fields

            $this->dbforge->drop_column('organization_units', 'unit_level');

        case '1.1':

            // add fields

            $fields = array(
                    'available_groups' => array(
                    'type' => 'TEXT',
                ),
            );

            $this->dbforge->add_column('organization_types', $fields);

        case '1.2':
            // convert all tables to InnoDB engine
            $this->db->query("ALTER TABLE `default_organization_units` ENGINE=InnoDB");
            $this->db->query("ALTER TABLE `default_organization_units_units` ENGINE=InnoDB");
            $this->db->query("ALTER TABLE `default_organization_types` ENGINE=InnoDB");
            $this->db->query("ALTER TABLE `default_organization_titles` ENGINE=InnoDB");
            $this->db->query("ALTER TABLE `default_organization_memberships` ENGINE=InnoDB");

            // stop upgrade in this version
            // please upgrade again
            $this->version = '1.3.0';
            break;

        case '1.3.0':
            $fields = array(
                'memberships_reports_type' => array(
                    'type' => 'INT',
                    'null' => TRUE,
                    ),
            );

            $this->dbforge->add_column('organization_memberships', $fields, 'membership_user');

            $fields = array(
                'unit_head' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 10,
                    'null' => TRUE,
                    ),
                'unit_code' => array(
                    'type' => 'INT',
                    'constraint' => 10,
                    'null' => TRUE,
                    ),
                'id_kota' => array(
                    'type' => 'INT',
                    'null' => TRUE,
                    ),
            );

            $this->dbforge->add_column('organization_units', $fields);

            // stop upgrade in this version
            // please upgrade again
            $this->version = '1.3.1';

        case '1.3.1':
            // change column id data type
            $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units')." CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL");

            // stop at this version
            // to continue please upgrade again
            $this->version = '1.4.0';
            break;

        case '1.4.0':
            // add column nama dan nomor hp ketua
            $fields = array(
                'nama_ketua' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 500,
                    'null' => TRUE,
                    ),
                'hp_ketua' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 30,
                    'null' => TRUE,
                    ),
            );

            $this->dbforge->add_column('organization_units', $fields);

            // stop at this version
            // to continue please upgrade again
            $this->version = '1.4.1';
            break;

        case '1.4.1':
            // change column id data type
            $this->db->query("SET foreign_key_checks = 0");
            $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units')." CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT");
            $this->db->query("SET foreign_key_checks = 1");

            // stop at this version
            // to continue please upgrade again
            $this->version = '1.5.0';
            break;

        case '1.5.0':
            // add column nama dan nomor hp ketua
            $fields = array(
                'kode_tti' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 25,
                    'null' => TRUE,
                    ),
            );

            $this->dbforge->add_column('organization_units', $fields);
            break;

        case '1.5.2':
            // add foreign key on default_organization_units
            $this->db->query("CREATE INDEX unit_type_index ON default_organization_units(unit_type)");
            $this->db->query("ALTER TABLE `default_organization_units` ADD CONSTRAINT `fk_units_unit_type` FOREIGN KEY (`unit_type`) REFERENCES `default_organization_types`(`id`)");

            $this->db->query("CREATE INDEX id_kota_index ON default_organization_units(id_kota)");
            $this->db->query("ALTER TABLE `default_organization_units` CHANGE `id_kota` `id_kota` INT(11) UNSIGNED NULL DEFAULT NULL");
            $this->db->query("ALTER TABLE `default_organization_units` ADD CONSTRAINT `fk_units_id_kota` FOREIGN KEY (`id_kota`) REFERENCES `default_location_kota`(`id`)");

            // add foreign key on default_organization_units_units
            $this->db->query("CREATE INDEX row_id_index ON default_organization_units_units(row_id)");
            $this->db->query("ALTER TABLE `default_organization_units_units` CHANGE `row_id` `row_id` INT(11) UNSIGNED NOT NULL, CHANGE `units_id` `units_id` INT(11) UNSIGNED NOT NULL");
            $this->db->query("ALTER TABLE `default_organization_units_units` ADD CONSTRAINT `fk_row_id` FOREIGN KEY (`row_id`) REFERENCES `default_organization_units`(`id`)");

            $this->db->query("CREATE INDEX units_id_index ON default_organization_units_units(units_id)");
            $this->db->query("DELETE FROM default_organization_units_units WHERE units_id NOT IN (SELECT id FROM default_organization_units)");
            $this->db->query("ALTER TABLE `default_organization_units_units` ADD CONSTRAINT `fk_units_id` FOREIGN KEY (`units_id`) REFERENCES `default_organization_units`(`id`)");

            // add foreign key on default_organization_memberships
            $this->db->query("ALTER TABLE `default_organization_memberships` CHANGE `membership_user` `membership_user` INT(11) UNSIGNED NULL DEFAULT NULL");
            $this->db->query("ALTER TABLE `default_organization_memberships` ADD CONSTRAINT `fk_membership_user` FOREIGN KEY (`membership_user`) REFERENCES `default_profiles`(`user_id`)");

            $this->db->query("CREATE INDEX membership_unit_index ON default_organization_memberships(membership_unit)");
            $this->db->query("ALTER TABLE `default_organization_memberships` CHANGE `membership_unit` `membership_unit` INT(11) UNSIGNED NULL DEFAULT NULL");
            $this->db->query("ALTER TABLE `default_organization_memberships` ADD CONSTRAINT `fk_membership_unit` FOREIGN KEY (`membership_unit`) REFERENCES `default_organization_units`(`id`)");

            break;

	    }

		return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}
