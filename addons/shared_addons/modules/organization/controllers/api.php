<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* API Organization
*
* Module API
*
*/
class Api extends API2_Controller
{
	public $metod = 'get';

	// This controller simply redirect to main section controller
	public function __construct(){
		parent::__construct();
		$this->lang->load('organization');
        $this->lang->load('location/location');
        $this->load->library('Organization');

		$this->load->model('units_m');
		$this->load->model('types_m');
		$this->load->model('location/kota_m');
		$this->load->model('location/provinsi_m');

		// $this->authorize_api_access('organization');
	}

	// public function index(){
	//   $data['organization'] = $this->units_m->get_units();
	//   $data['service'] = $this->service_m->get_service();
	//   $data['custom_field'] = $this->custom_field_m->get_custom_field();
	//   echo json_encode($data);
	// }

	public function get_all($type){
		// Get types
		$data['types'] = $this->types_m->get_all_types();

		// Get units
		$data['filter_type'] = $type;

		$filters = NULL;
		if($this->input->get('id_provinsi')) {
			$filters['default_location_provinsi.id'] = $this->input->get('id_provinsi');
		}

		if($this->input->get('id_kota')) {
			$filters['default_location_kota.id'] = $this->input->get('id_kota');
		}

		if($data['filter_type'] == NULL){
	    	$root_units = $this->units_m->get_units_by_level(0);

	    	$units = $this->organization->build_serial_tree(array(), $root_units);
		}else{
			$units = $this->units_m->get_units_by_type($data['filter_type'],$filters);
		}

		$fields = array( "id", "unit_name", "nama_ketua", "unit_type", "unit_slug", "unit_description", "hp_ketua", "type_name", "type_description", "type_slug", "type_level", "id_provinsi", "provinsi", "id_kota", "kota");
		$cleaned_units = array();

		foreach ($units as $key => $unit) {
			foreach ($unit as $unit_key => $value) {
				if(in_array($unit_key, $fields)) {
					$cleaned_units[$key][$unit_key] = $value;
				}
			}
		}

		$result = $cleaned_units;
		$status = "200";

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>strtoupper($type)." data not found");
			$status = "400";
		}
		_output($result);
	}

	public function get_all_by_parent($id_parent){
		// Get types
		$data['types'] = $this->types_m->get_all_types();

		// Get units

		$filters = NULL;
		if($this->input->get('id_provinsi')) {
			$_GET['f-provinsi'] = $this->input->get('id_provinsi');
			unset($_GET['id_provinsi']);
		}

		if($this->input->get('id_kota')) {
			$_GET['f-kota'] = $this->input->get('id_kota');
			unset($_GET['id_kota']);
		}

		$units = $this->units_m->get_units_by_parent($id_parent);

		$fields = array( "id", "unit_name", "nama_ketua", "unit_type", "unit_slug", "unit_description", "hp_ketua", "type_name", "type_description", "type_slug", "type_level", "id_provinsi", "provinsi", "id_kota", "kota");
		$cleaned_units = array();

		foreach ($units as $key => $unit) {
			foreach ($unit as $unit_key => $value) {
				if(in_array($unit_key, $fields)) {
					$cleaned_units[$key][$unit_key] = $value;
				}
			}
		}

		$result = $cleaned_units;
		$status = "200";

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>"TTI data not found");
			$status = "400";
		}
		_output($result);
	}

	public function get_by_id($id, $type = NULL){
		$status = '200';
		// Get our entry. We are simply specifying
    	$data['units'] =  $this->units_m->get_units_by_id($id);
		$data['units']->unit_type = (object) $this->types_m->get_type_by_id($data['units']->unit_type);


		// Get multiple relationship

		$unit_parents = $this->units_m->get_parent_units_by_id($data['units']->id);
		foreach ($unit_parents as $key => $value) {
			$unit_parents[$key]['unit_type'] = (object) $this->types_m->get_type_by_id($value['unit_type']);
		}

		$data['units']->unit_parents = $unit_parents;

		$this->load->model('organization/memberships_m');
		$pendampings = $this->memberships_m->get_membership_by_unit($id);
		$data['pendampings'] = $pendampings;
		$nos = '';
		$namas = '';
		if(count($pendampings) > 0){
			foreach ($pendampings as $key => $pendamping) {
				$nohp = substr($pendamping['user_telp'], 0, -4).'xxxx';
				$nama_pendamping[] = $pendamping['user_display_name'] .' ('.$nohp.')';
				// $nama_pendamping[] = $pendamping['user_display_name'];
				$no_pendamping[] = $nohp;
			}
			$nos = implode(', ',$no_pendamping);
			$namas = implode(', ', $nama_pendamping);
		}

		$data['units']->nama_pendamping = $namas;
		$data['units']->no_pendamping = $nos;

		$fields = array( "id", "unit_name", "nama_ketua", "unit_type", "unit_slug", "unit_address", "hp_ketua", "type_name", "type_description", "type_slug", "type_level", "id_provinsi", "provinsi", "id_kota", "kota");
		$units = NULL;
		foreach ($data['units'] as $key => $value) {
			if(in_array($key, $fields)) {
				$units[$key] = $value;
			}
		}

		$result = $units;

		if($type != strtolower($units['unit_type']->type_name)) {
			$result = array('status'=>'error', 'message'=>ucwords($type).' tidak ditemukan');
	    	$status = '200';
	    }

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>strtoupper($type)." data not found");
			$status = "400";
		}

		_output($result,$status);
	}

	public function get_units_by_member($id){
		$status = '200';
		// Get our entry. We are simply specifying
    	$result =  $this->memberships_m->get_units_by_member($id);

		if(empty($result)) {
			$result = array("status"=>"error","messages"=>"Organization data not found");
			$status = "200";
		}

		_output($result,$status);
	}

	public function jumlah($type){
		// Get types
		$data['types'] = $this->types_m->get_all_types();

		// Get units
		$data['filter_type'] = $type;

		$filters = NULL;
		if($this->input->get('id_provinsi')) {
			$filters['default_location_provinsi.id'] = $this->input->get('id_provinsi');
		}

		if($this->input->get('id_kota')) {
			$filters['default_location_kota.id'] = $this->input->get('id_kota');
		}

		if($data['filter_type'] == NULL){
	    	$root_units = $this->units_m->get_units_by_level(0);

	    	$units = $this->organization->build_serial_tree(array(), $root_units);
		}else{
			$units = $this->units_m->get_units_by_type($data['filter_type'],$filters);
		}

		$fields = array( "id", "unit_name", "nama_ketua", "unit_type", "unit_slug", "unit_description", "hp_ketua", "type_name", "type_description", "type_slug", "type_level", "id_provinsi", "provinsi", "id_kota", "kota");
		$cleaned_units = array();

		foreach ($units as $key => $unit) {
			foreach ($unit as $unit_key => $value) {
				if(in_array($unit_key, $fields)) {
					$cleaned_units[$key][$unit_key] = $value;
				}
			}
		}

		$result = count($cleaned_units);
		$status = "200";

		_output($result);
	}
}
