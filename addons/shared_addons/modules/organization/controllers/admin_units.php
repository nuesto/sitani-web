<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Organization Module
 *
 * Module to manage organization
 *
 */
class Admin_units extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'units';

	private $units_arr = array();
	private $sorted_units_arr = array();

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('organization', 'access_units_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('organization');
        $this->lang->load('location/location');
        $this->load->library('Organization');

		$this->load->model('units_m');
		$this->load->model('types_m');
		$this->load->model('location/kota_m');
		$this->load->model('location/provinsi_m');
    }

    /**
	 * List all Units
     *
     * @return	void
     */
    public function index()
    {
		// Check permission
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_provinsi_units') AND ! group_has_role('organization', 'view_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		if(group_has_role('organization','view_provinsi_units')) {
			$_GET['f-provinsi'] = $this->current_user->id_provinsi;
		}
		$data['view'] = 'index';
		// Get types
		$data['types'] = $this->types_m->get_all_types();
		$data['types_all'] = $this->types_m->get_type_all();

		// Get units
		$data['filter_type'] = $this->input->get('type');

		$surffix = '';
	    if($_SERVER['QUERY_STRING']){
	      	$surffix = '?'.$_SERVER['QUERY_STRING'];
	    }

    	// if($data['filter_type'] == NULL){
	    	$units = $this->units_m->get_units_by_level(0);
	    	// $units = $this->organization->build_serial_tree(array(), $root_units);
		// }else{
			// $units = $this->units_m->get_units_by_type($data['filter_type']);
		// }
		$data['units']['total'] = count($units);
		$pagination_config['base_url'] = base_url(). 'admin/organization/units/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($units);
		// $pagination_config['per_page'] = 2;
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		// if($data['filter_type'] == NULL){
	    	$root_units = $this->units_m->get_units_by_level(0, null, null, $pagination_config);
	    	// dump($root_units);
	    	$units = $this->organization->build_serial_tree(array(), $root_units);
		// }else{
			// $units = $this->units_m->get_units_by_type($data['filter_type']);
		// }
		$data['units']['entries'] = $units;
		$data['units']['pagination'] = $this->pagination->create_links(null,1);
		// -------------------------------------
		// Restric users
		// -------------------------------------
		// Eliminate not owned unit
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_provinsi_units')){
			$temp_arr = array();
			foreach($data['units']['entries'] as $unit){
				if($this->organization->is_member($this->current_user->id, $unit['id'])){
					$temp_arr[] = $unit;
				}
			}
			$data['units']['entries'] = $temp_arr;
			$data['units']['total'] = count($temp_arr);
		}

		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;

		$filter_kota = null;
	  	if($this->input->get('f-provinsi') != '') {
	  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
	  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
	  	}
		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		$data['uri'] = $this->get_query_string(5);

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->append_css('module::treetable/jquery.treetable.css');
        $this->template->append_css('module::organization.css');
        $this->template->append_js('module::treetable/jquery.treetable.js');

        $this->template->title(lang('organization:units:plural'));
		$this->template->build('admin/units_index', $data);
    }

    public function tanpa_pendamping()
    {
        // Check permission
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_provinsi_units') AND ! group_has_role('organization', 'view_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		if(group_has_role('organization','view_provinsi_units')) {
			$_GET['f-provinsi'] = $this->current_user->id_provinsi;
		}

		// Get types
		$data['types'] = $this->types_m->get_all_types();
		$data['types_all'] = $this->types_m->get_type_all();

		// Get units
		$data['filter_type'] = $this->input->get('type');

		$surffix = '';
    if($_SERVER['QUERY_STRING']){
      $surffix = '?'.$_SERVER['QUERY_STRING'];
    }

		$units = $this->units_m->get_units_without_pendamping();
		$data['units']['total'] = count($units);
		$pagination_config['base_url'] = base_url(). 'admin/organization/units/tanpa_pendamping';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($units);
		// $pagination_config['per_page'] = 2;
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

		$units = $this->units_m->get_units_without_pendamping(null, null, $pagination_config);
		$data['units']['entries'] = $units;
		$data['units']['pagination'] = $this->pagination->create_links(null,1);;

		// -------------------------------------
		// Restric users
		// -------------------------------------
		// Eliminate not owned unit
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_provinsi_units')){
			$temp_arr = array();
			foreach($data['units']['entries'] as $unit){
				if($this->organization->is_member($this->current_user->id, $unit['id'])){
					$temp_arr[] = $unit;
				}
			}
			$data['units']['entries'] = $temp_arr;
			$data['units']['total'] = count($temp_arr);
		}

		$data['view'] = 'tanpa_pendamping';

		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;

		$filter_kota = null;
	  	if($this->input->get('f-provinsi') != '') {
	  		$filter_kota['id_provinsi'] = $this->input->get('f-provinsi');
	  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL,$filter_kota);
	  	}
		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		$data['uri'] = $this->get_query_string(5);

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->append_css('module::treetable/jquery.treetable.css');
        $this->template->append_css('module::organization.css');
        $this->template->append_js('module::treetable/jquery.treetable.js');

        $this->template->title(lang('organization:units:plural'));
		$this->template->build('admin/units_index', $data);
    }

	/**
     * Display one Units
     *
     * @return  void
     */
    public function view($id = 0, $view = 'index')
    {
        // Check permission
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_provinsi_units') AND ! group_has_role('organization', 'view_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// Get our entry. We are simply specifying
        $data['units'] =  $this->units_m->get_units_by_id($id);
		$data['units']->unit_type = (object) $this->types_m->get_type_by_id($data['units']->unit_type);

		// Check view all/own permission
		if(! group_has_role('organization', 'view_all_units')){
			if(! group_has_role('organization', 'view_provinsi_units')){
				if(! $this->organization->is_member($this->current_user->id, $id)){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		// Get multiple relationship

		$unit_parents = $this->units_m->get_parent_units_by_id($data['units']->id);
		foreach ($unit_parents as $key => $value) {
			$unit_parents[$key]['unit_type'] = (object) $this->types_m->get_type_by_id($value['unit_type']);
		}

		$data['view'] = $view;
		$data['units']->unit_parents = $unit_parents;

		// // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:units:view'));
        $this->template->build('admin/units_entry', $data);
    }

	/**
     * Create a new Units entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// Check permission
		if(! group_has_role('organization', 'create_units') AND !group_has_role('organization', 'create_provinsi_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// -------------------------------------
		// Process POST input
		// -------------------------------------

		$data['content_unit_parents'] = '';

		if($_POST){
			if($this->_update_units('new')){
				$this->session->set_flashdata('success', lang('organization:units:submit_success'));

				redirect('admin/organization/units/index'.$this->get_query_string(5));
			}else{
				if(isset($_POST['unit_parents']) && isset($_POST['unit_type'])){
					$unit_parent = $_POST['unit_parents'];
					$unit_type = $_POST['unit_type'];
					$provinsi = (isset($_POST['id_provinsi'])) ? $_POST['id_provinsi'] : NULL;
					$kota = (isset($_POST['id_kota'])) ? $_POST['id_kota'] : NULL;
					if($unit_type == 2){
						$data['content_unit_parents'] = $this->set_unit_parents($unit_type, $unit_parent, $provinsi, $kota);
					}else{
						$data['content_unit_parents'] = $this->set_unit_parents($unit_type, $unit_parent);
					}
				}
				$data['messages']['error'] = lang('organization:units:submit_failure');
			}
		}

		// Load location
		// Location
		$data['mode'] = 'new';
		if(group_has_role('organization', 'create_provinsi_units')) {
			$_GET['f-provinsi'] = $this->current_user->id_provinsi;
		}
		$data['id_provinsi'] = ($this->input->get('f-provinsi')) ? $this->input->get('f-provinsi') : NULL;
		$data['id_kota'] = ($this->input->get('f-kota')) ? $this->input->get('f-kota') : NULL;
		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();


		$filter_kota = NULL;
	  	if($data['id_provinsi'] != NULL) {
	  		$filter_kota['id_provinsi'] = $data['id_provinsi'];
	  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);
	  	}else{
	  		if($this->input->post('id_provinsi') != ''){
	  			$data['id_provinsi'] = $this->input->post('id_provinsi');
		  		$filter_kota['id_provinsi'] = $data['id_provinsi'];
		  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);
	  		}
	  	}

		// Load everything we need
		$data['unit_type'] = $this->types_m->get_all_types();
		$data['mode'] = 'new'; //'new', 'edit'
		$data['uri'] = $this->get_query_string(5);
		$data['return'] = 'admin/organization/units/index'.$data['uri'];

		// Build the form page.
		$this->template->append_js('jquery/jquery.slugify.js');
        $this->template->title(lang('organization:units:new'));
        $this->template->build('admin/units_form', $data);
    }

    public function set_unit_parents($type_id, $id_unit, $provinsi = null, $kota = null){
    	$this->config->load('organization');
		$allow_multi_parent = $this->config->item('allow_multi_parent');
		$content = '';
		$type = $this->types_m->get_type_by_id($type_id);
		if(isset($type->type_level) AND $type->type_level > 0){
			$parent_level = $type->type_level - 1;
			$type_id = $type_id - 1;
			if($provinsi == null && $kota == null){
				$units = $this->units_m->get_units_by_type('ttic');
			}else{
				$units = $this->units_m->get_units_by_level($parent_level, $provinsi, $kota, null, $type_id, false);
			}

			if(count($units) > 0) {
				if ($allow_multi_parent) {
					foreach($units as $unit){
						$content .= '<input type="checkbox" name="unit_parents[]" id="unit_parents" value="'.$unit['id'].'" /> '.$unit['unit_name'].'<br />';
					}
				} else {
					$content .= '<select name="unit_parents" id="unit_parents">';
					foreach($units as $unit){
						$selected = ($unit['id']==$id_unit) ? 'selected="selected"' : '';
						$content .= '<option value="'.$unit['id'].'" '.$selected.'>'.$unit['unit_name'] . ($kota == NULL ? ' - '.$unit['kota'] : '').'</option>';
					}
					$content .= '</select>';
				}
			}else{
				$content .= lang('organization:no_entry_unit_parents');
			}
		}else{
			$content .= '<input name="unit_parents" value="" type="hidden" />';
			$content .= '<p id="unit_parents" class="form-control-static" />-</p>';
		}
		return $content;
    }

	/**
     * Edit a Units entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Units to the be deleted.
     * @return	void
     */
    public function edit($id = 0, $view = 'index')
    {
        // Check permission
		if(! group_has_role('organization', 'edit_all_units') AND ! group_has_role('organization', 'edit_provinsi_units') AND ! group_has_role('organization', 'edit_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'edit_all_units')){
			if(! group_has_role('organization', 'edit_provinsi_units')) {
				if(! $this->organization->is_member($this->current_user->id, $id)){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		$data['uri'] = $this->get_query_string(5);
		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			if($this->_update_units('edit', $id)){
				$this->session->set_flashdata('success', lang('organization:units:submit_success'));

				redirect('admin/organization/units/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('organization:units:submit_failure');
			}
		}


		// Load everything we need

		$data['fields'] = $this->units_m->get_units_by_id($id);
		$data['unit_type'] = $this->types_m->get_all_types();
		$data['mode'] = 'edit'; //'new', 'edit'
		$data['uri'] = $this->get_query_string(6);
		if($view == 'index'){
			$data['return'] = 'admin/organization/units/index'.$data['uri'];
		}else{
			$data['return'] = 'admin/organization/units'.$data['uri'];
		}

		// Location
		$data['mode'] = 'edit';
		$data['provinsi']['entries'] = $this->provinsi_m->get_provinsi();

		$data['id_provinsi'] = $data['fields']->id_provinsi;
		$data['id_kota'] = $data['fields']->id_kota;

		$data['content_unit_parents'] = '';
		if($data['fields']->unit_type == 2 || $data['fields']->unit_type == 4){
			$unit_parent = $this->units_m->get_parent_units_by_id($id);
			$unit_parent = $unit_parent[0]['id'];
			$unit_type = $data['fields']->unit_type;
			$provinsi = NULL;
			$kota = $data['fields']->id_kota;
			if($unit_type == 2){
				$data['content_unit_parents'] = $this->set_unit_parents($unit_type, $unit_parent, $provinsi, $kota);
			}else{
				$data['content_unit_parents'] = $this->set_unit_parents($unit_type, $unit_parent);
			}
		}

		$filter_kota = NULL;
	  	if($data['id_provinsi'] != NULL) {
	  		$filter_kota['id_provinsi'] = $data['id_provinsi'];
	  		$data['kota']['entries'] = $this->kota_m->get_kota(NULL, $filter_kota);
	  	}

		// Build the form page.
		$this->template->append_js('jquery/jquery.slugify.js');
        $this->template->title(lang('organization:units:edit'));
        $this->template->build('admin/units_form', $data);
    }

	/**
     * Delete a Units entry
     *
     * @param   int [$id] The id of Units to be deleted
     * @return  void
     */
    public function delete($id = 0, $view = 'index')
    {
		// Check permission
		if(! group_has_role('organization', 'delete_all_units') AND ! group_has_role('organization', 'delete_provinsi_units') AND ! group_has_role('organization', 'delete_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'delete_all_units')){
			if(! group_has_role('organization', 'delete_provinsi_units')){
				if(! $this->organization->is_member($this->current_user->id, $id)){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		$children = $this->units_m->get_units_by_parent($id);
		$is_used = $this->units_m->is_used($id);

		if($is_used){
			if(count($children) > 0){
				$this->session->set_flashdata('error', lang('organization:units:delete_fail_has_child'));
			}else{
				$this->units_m->delete_units_by_id($id);
				$this->session->set_flashdata('success', lang('organization:units:deleted'));
			}
		}else{
			$this->session->set_flashdata('error', lang('organization:units:is_used'));
		}

		redirect('admin/organization/units/'.$view);
    }

	public function ajax_unit_parents_checkbox($type_id = 0, $provinsi = null, $kota = null){
		$this->config->load('organization');
		$allow_multi_parent = $this->config->item('allow_multi_parent');
		$type = $this->types_m->get_type_by_id($type_id);
		if(isset($type->type_level) AND $type->type_level > 0){
			$parent_level = $type->type_level - 1;
			$type_id = $type_id - 1;
			if($provinsi == null && $kota == null){
				$units = $this->units_m->get_units_by_type('ttic');
			}else{
				$units = $this->units_m->get_units_by_level($parent_level, $provinsi, $kota, null, $type_id, false);
			}

			if(count($units) > 0) {
				if ($allow_multi_parent) {
					foreach($units as $unit){
						echo '<input type="checkbox" name="unit_parents[]" id="unit_parents" value="'.$unit['id'].'" /> '.$unit['unit_name'].'<br />';
					}
				} else {
					echo '<select name="unit_parents" id="unit_parents">';
					foreach($units as $unit){
						echo '<option value="'.$unit['id'].'">'.$unit['unit_name'] . ($kota == NULL ? ' - '.$unit['kota'] : '').'</option>';
					}
					echo '</select>';
				}
			}else{
				echo lang('organization:no_entry_unit_parents');
			}
		}else{
			echo '<input name="unit_parents" value="" type="hidden" />';
			echo '<p id="unit_parents" class="form-control-static" />-</p>';
		}
	}

	/**
     * Insert or update Units entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_units($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper(array('form', 'url'));

 		// -------------------------------------
		// Set Values
		// -------------------------------------

		$values = $this->input->post();
		unset($values['id_provinsi']);

		// -------------------------------------
		// Validation
		// -------------------------------------

		// Set validation rules
		$this->form_validation->set_rules('unit_name', lang('organization:unit_name'), 'required|max_length[100]');
		$this->form_validation->set_rules('unit_slug', lang('organization:unit_slug'), 'required|max_length[100]');
		$this->form_validation->set_rules('unit_type', lang('organization:unit_type'), 'required');
		$this->form_validation->set_rules('id_kota', lang('location:kota:singular'), 'required');
		$this->form_validation->set_rules('nama_ketua', lang('organization:nama_ketua'), 'max_length[50]');
		$this->form_validation->set_rules('hp_ketua', lang('organization:hp_ketua'), 'numeric|max_length[13]');

		if($values['unit_type'] == 2 || $values['unit_type'] == 4){
			if(!isset($values['unit_parents'])) {
				$this->form_validation->set_rules('unit_parents', lang('organization:unit_parents'), 'required');
			}
		}

		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');

		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->units_m->insert_units($values);

			}
			else
			{
				$result = $this->units_m->update_units($values, $row_id);
			}
		}

		return $result;
	}

	public function get_query_string($page){
		// -------------------------------------
		// Get query string
		// -------------------------------------

		$page = $this->uri->segment($page);
		$uri = '';
	    if($page){
	        $uri = '/'.$page;
	    }
	    if($_SERVER['QUERY_STRING']){
	        $uri = '?'.$_SERVER['QUERY_STRING'];
	    }
	    if($_SERVER['QUERY_STRING'] && $page){
	        $uri = '/'.$page.'?'.$_SERVER['QUERY_STRING'];
	    }

	    return $uri;
	}

	public function import(){
		$uri = $this->get_query_string(5);
		if($_POST){
			if($this->_import_units()){
				$this->session->set_flashdata('success', lang('organization:units:submit_success'));
				redirect('admin/organization/units/index/'.$uri);
			}else{
				$data['messages']['error'] = lang('organization:units:submit_failure');
			}
		}

		$data['uri'] = $uri;
		$data['return'] = 'admin/organization/units/index/'. $uri;

		$this->template->title(lang('organization:units:import'))
			->build('admin/units_import', $data);
	}

	private function _import_units()
	{
		global $input_units;
		$this->load->library('form_validation');

		$this->form_validation->set_rules('upload', 'units', 'callback__upload_csv_units');

    $result = false;

		if ($this->form_validation->run() === true)
		{
			foreach ($input_units as $data) {
				$values['unit_name'] = $data['unit_name'];
				$values['unit_description'] = $data['description'];
				$values['unit_type'] = $data['unit_type'];
				$values['id_kota'] = $data['id_kota'];
				$values['nama_ketua'] = $data['nama_ketua'];
				$values['hp_ketua'] = $data['hp_ketua'];
				// if($data['unit_type'] == 2 || $data['unit_type'] == 4){
				if($data['unit_parents'] != NULL){
					$values['unit_parents'] = $data['unit_parents'];
				}
				// }
				$result = $this->units_m->insert_units($values);
			}

			$result = true;
		}

		return $result;
	}

	public function _upload_csv_units()
	{
		global $input_units;
		$config['upload_path'] = 'uploads/temp/';
    $config['allowed_types'] = 'csv';
    $config['file_name'] = 'data.csv';
    $config['overwrite'] = true;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('file'))
    {
	    $filename = $config['upload_path'] . $config['file_name'];
	    $withHeader = true;
	    $csv = fopen($filename, FOPEN_READ);
	    $i = 0;
	    $errors = array();

	    while (($row = fgetcsv($csv,0,';')))
	    {
        $i++;
        if ($withHeader) {
            $withHeader = false;
            continue;
        }

        if($row[0] == "" && $row[1]=="" && $row[2]=="" && $row[3]=="" && $row[4]=="" && $row[5]=="" && $row[6]=="" && $row[7]=="" && $row[8]==""){
        	continue;
        }

        // Cek Provinsi
        $id_provinsi = NULL;
      	if($row[0] == "")
      	{
          $errors[] = "Baris {$i}: <b>Provinsi</b> belum diisi.";
        }else{
	        $this->db->where('nama', $row[0]);
	        $query = $this->db->get('default_location_provinsi');
	        if($query->num_rows() > 0){
	        	$provinsi = $query->row_array();
	        	$id_provinsi = $provinsi['id'];
	        }else{
	        	$errors[] = "Baris {$i}: Provinsi <b>".$row[0]."</b> tidak terdaftar";
	        }
	      }

        // Cek Kota
        $id_kota = NULL;
        if($row[1] == "")
      	{
          $errors[] = "Baris {$i}: <b>Kota/Kabupaten</b> belum diisi.";
        }else{
	        $this->db->where('nama', $row[1]);
	        $this->db->where('id_provinsi', $id_provinsi);
	        $query = $this->db->get('default_location_kota');
	        if($query->num_rows() > 0){
	        	$kota = $query->row_array();
	        	$id_kota = $kota['id'];
	        }else{
	        	$errors[] = "Baris {$i}: Kota <b>".$row[1]."</b> pada provinsi <b>".$row[0]."</b> tidak terdaftar";
	        }
	      }

	      // Cek Tipe
	      $unit_type = NULL;
        if($row[2] == "")
      	{
          $errors[] = "Baris {$i}: <b>Tipe</b> belum diisi.";
        }else{
	        $arr_enum = array('Gapoktan','TTI','Gapoktan17','TTI17','Gapoktan18','TTI18');
	        if(!in_array($row[2], $arr_enum)){
	        	$errors[] = "Baris {$i}: Tipe <b>".$row[2]."</b> tidak terdafatar.";
	        }else{
	        	$arr_type = array('Gapoktan'=>1,'TTI'=>2, 'Gapoktan17'=>4,'TTI17'=>5, 'Gapoktan18'=>6,'TTI18'=>7);
	        	$unit_type = $arr_type[$row[2]];
	        }
	      }

	      // Cek Nama Gapoktan/TTI
        if($row[3] == "")
      	{
    			$errors[] = "Baris {$i}: <b>Nama Gapoktan/TTI</b> tidak boleh kosong";
        }

	      // Cek No Telp
	      if($row[6] != ""){
	        $row[6] = str_replace(" ", "", $row[6]);
        	$row[6] = str_replace("+62","0", $row[6]);
	        if(substr($row[6], 0,1) == 8){
	        	$row[6] = '0'.$row[6];
	        }
	       //  if(strlen($row[6]) < 8 || strlen($row[6]) > 15){
	      	// 	$errors[] = "Baris {$i}: No HP tidak boleh kurang dari 8 digit dan tidak boleh lebih dari 15 digit";
	      	// }
	      }

	      $id_kota_parent = NULL;
	      if($row[2] == 'TTI' || $row[2] == 'Gapoktan17' || $row[2] == 'TTI18'){
	        if($row[8] != ""){
		        $this->db->where('nama', $row[8]);
		        $query = $this->db->get('default_location_kota');
		        if($query->num_rows() > 0){
		        	$kota = $query->row_array();
		        	$id_kota_parent = $kota['id'];
		        }else{
		        	$errors[] = "Baris {$i}: Kota Unit Parent <b>".$row[8]."</b> tidak terdaftar";
		        }
		      }
		    }

	      $id_parent = NULL;
	      $nama_kota = $row[1];
	      if($row[2] == 'TTI' || $row[2] == 'Gapoktan17' || $row[2] == 'TTI18'){
	      	if($row[7] == ""){
	      		$errors[] = "Baris {$i}: <b>Satuan Induk ".$row[2]."</b> tidak boleh kosong";
	      	}else{
	      		$id_kota2 = $id_kota;
	      		if($id_kota_parent != NULL){
	      			$id_kota2 = $id_kota_parent;
	      			$nama_kota = $row[8];
	      		}
	      		$unit_alert = 'Gapoktan';
	      		if($row[2] == 'TTI'){
	      			$type_unit = 1;
	      		}
	      		if($row[2] == 'Gapoktan17'){
	      			$type_unit = 3;
	      			$unit_alert = 'TTIC';
	      		}
	      		if($row[2] == 'TTI18'){
	      			$type_unit = 6;
	      		}
	      		$this->db->where('unit_name', $row[7]);
	        	$this->db->where('id_kota', $id_kota2);
	        	$this->db->where('unit_type', $type_unit);
		        $query = $this->db->get('default_organization_units');
		        if($query->num_rows() > 0){
		        	$unit = $query->row_array();
		        	$id_parent = $unit['id'];
		        }else{
		        	$errors[] = "Baris {$i}: ".$unit_alert." <b>'".$row[7]."'</b> tidak terdaftar di kota <b>".$nama_kota."</b>";
		        }
	      	}
	      }

      	$input_units[] = array(
        	'id_provinsi' => $id_provinsi,
        	'id_kota' => $id_kota,
        	'unit_type' => $unit_type,
        	'unit_name' => $row[3],
        	'description' => $row[4],
        	'nama_ketua' => $row[5],
        	'hp_ketua' => $row[6],
        	'unit_parents' => $id_parent
        );
      }

      fclose($csv);
      unlink($filename);

      if (count($errors)) {
          $this->form_validation->set_message('_upload_csv_units', 'Terjadi kesalahan data pada berkas.<br />' . implode('<br />', $errors));
          return false;
      } else {
          return true;
      }
	  }
	  else
	  {
		  $this->form_validation->set_message('_upload_csv_units', $this->upload->display_errors('', ''));
	    return false;
	  }
	}

	public function csv_example()
	{
		$this->load->helper('download');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=contoh_data_import_units.csv");
		header("Pragma: no-cache");
		header("Expires: 0");

    $data = 'Provinsi,Kabupaten/Kota,Tipe,Nama,Alamat,Nama Ketua,No HP Ketua,Satuan Induk (Parent Unit),Kota Satuan Induk (Jika kota gapoktan berbeda dengan kota TTI)
Jawa Tengah,Kab. Tegal,Gapoktan,Gapoktan Tegal,Jl. Asri Raya,Gani Mahmud,8792384993,,
Jawa Barat,Kota Bandung,TTI,Toko Resti,Desa Pugaan Kec. Pugaan,Haris Mulyo,87273848349,Gap Resti,Kab. Bandung
';
    $name = 'contoh_data_import_units.csv';

    force_download($name, $data);
	}
}
