<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Types model
 *
 * @author Aditya Satrya
 */
class Types_m extends MY_Model {
	
	/**
	 * Get all organization types
	 * @return array
	 */
	public function get_all_types($pagination_config = NULL, $tipe = NULL)
	{
		$this->db->select('organization_types.id AS id');
		$this->db->select('organization_types.type_name AS name');
		$this->db->select('organization_types.type_description AS description');
		$this->db->select('organization_types.type_slug AS slug');
		$this->db->select('organization_types.type_level AS level');
		$this->db->select('organization_types.available_groups AS available_groups');

		if($this->input->get('f-tipe_laporan')){
			

			if($this->input->get('f-tipe_laporan') == 1 || $this->input->get('f-tipe_laporan') == 2){
				// jika pendamping mingguna maka mengambil tipe tti
				$this->db->where('id <=', $this->input->get('f-tipe_laporan'));
			}

			//jika pendamping harian maka ttic mengambil tipe gapoktan saja
			if($this->input->get('f-tipe_laporan') == 3){
				$this->db->where_in('id', array(3,4));
			}
			if($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 4 || $this->input->get('f-tipe_laporan') == 7){
				$this->db->where('id', $this->input->get('f-tipe_laporan'));
			}

			if($this->input->get('f-tipe_laporan') == 6){
				$this->db->where('id', 6);
			}
			// if($this->input->get('f-tipe_laporan') == 7){
			// 	$this->db->where_in('id', array(6,7));
			// }
		}

		if($tipe != NULL){
			$this->db->where('id <=', $tipe);
		}
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;

		$this->db->limit($pagination_config['per_page'], $start);

		$res = $this->db->get('organization_types')->result_array();

		return $res;
	}
	public function get_all_types_front($pagination_config = NULL, $tipe = NULL)
	{
		$this->db->select('organization_types.id AS id');
		$this->db->select('organization_types.type_name AS name');
		$this->db->select('organization_types.type_description AS description');
		$this->db->select('organization_types.type_slug AS slug');
		$this->db->select('organization_types.type_level AS level');
		$this->db->select('organization_types.available_groups AS available_groups');

		if($tipe){
			

			if($tipe == 1 || $tipe == 2){
				// jika pendamping mingguna maka mengambil tipe tti
				$this->db->where('id <=', $tipe);
			}

			//jika pendamping harian maka ttic mengambil tipe gapoktan saja
			if($tipe == 3){
				$this->db->where_in('id', array(3,4));
			}
			if($tipe == 5 || $tipe == 4 || $tipe == 7){
				$this->db->where('id', $tipe);
			}

			if($tipe == 6){
				$this->db->where('id', 6);
			}
			// if($this->input->get('f-tipe_laporan') == 7){
			// 	$this->db->where_in('id', array(6,7));
			// }
		}
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;

		$this->db->limit($pagination_config['per_page'], $start);

		$res = $this->db->get('organization_types')->result_array();

		return $res;
	}

	public function get_type_by_id($type_id)
	{
		$this->db->where('id', $type_id);
		$res = $this->db->get('organization_types')->row();
		
		if(count($res) >= 1){
			return $res;
		}else{
			return NULL;
		}
	}

	public function get_type_all()
	{
		$res = $this->db->get('organization_types')->result_array();
		return $res;
	}

	public function count_all_types() {
		return $this->db->count_all('organization_types');
	}

	public function insert_types($values) {

		$values['created'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('organization_types', $values);
	}

	public function update_types($values, $row_id = 0) {
		unset($values['row_edit_id']);

		$values['updated'] = date("Y-m-d H:i:s");

		$this->db->where('id', $row_id);
		return $this->db->update('organization_types', $values);
	}

	public function delete_types_by_id($row_id) {
		$this->db->where('unit_type', $row_id);
		$this->db->update('organization_units', array('unit_type'=>NULL));

		$this->db->where('id', $row_id);
		return $this->db->delete('organization_types');
	}
	
	public function get_type_by_slug($slug)
	{
		$this->db->where('type_slug', $slug);
		$res = $this->db->get('organization_types')->row();
		
		if(count($res) < 1){
			if($this->input->get('f-tipe_laporan')){
				//jika pendamping harian maka ttic mengambil tipe gapoktan saja
				if($this->input->get('f-tipe_laporan') == 3){
					$this->db->where_in('id', array(3,4));
				}else if($this->input->get('f-tipe_laporan') == 5 || $this->input->get('f-tipe_laporan') == 4){
					$this->db->where('id', $this->input->get('f-tipe_laporan'));
				}else{
					// jika pendamping mingguna maka mengambil tipe tti
					$this->db->where('id <=', $this->input->get('f-tipe_laporan'));
				}
			}
			$this->db->order_by('id','desc');
			$this->db->limit(1);
			$res = $this->db->get('organization_types')->row();
		}
		return $res;
	}
}