<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Units model
 *
 * @author Aditya Satrya
 */
class Units_m extends MY_Model {

	public function get_units($params = NULL)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.nama_ketua AS nama_ketua');
		$this->db->select('organization_units.hp_ketua AS hp_ketua');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_units_units.units_id AS parent_id');
		$this->db->select('parent_units.unit_name AS parent_name');
		$this->db->select('parent_units.unit_abbrevation AS parent_abbrevation');
		$this->db->select('parent_units.unit_slug AS parent_slug');

		$this->db->select('location_kota.id AS id_kota');
		$this->db->select('location_kota.nama AS kota');
		$this->db->select('location_provinsi.id AS id_provinsi');
		$this->db->select('location_provinsi.nama AS provinsi');

		if($this->input->get('f-provinsi') != ''){

			
			$this->db->where('location_kota.id_provinsi', $this->input->get('f-provinsi'));
		}
		if($this->input->get('f-kota') != ''){
			$this->db->where('location_kota.id', $this->input->get('f-kota'));
		}

		if($this->input->get('f-type') != ''){
			$this->db->where('organization_types.id', $this->input->get('f-type'));
		}

		if(isset($params)) {
			$this->db->like($params);
		}

		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$this->db->join('organization_units_units', 'organization_units.id = organization_units_units.row_id', 'left');
		$this->db->join('organization_units parent_units', 'parent_units.id = organization_units_units.units_id', 'left');
		$this->db->join('location_kota', 'location_kota.id = organization_units.id_kota', 'left');
		$this->db->join('location_provinsi', 'location_provinsi.id = location_kota.id_provinsi', 'left');

		$this->db->order_by('parent_name', 'asc');
		$this->db->order_by('unit_name', 'asc');

		$res = $this->db->get('organization_units')->result_array();
		return $res;
	}

	public function get_units_by_type($type_slug, $filters = NULL)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.nama_ketua AS nama_ketua');
		$this->db->select('organization_units.hp_ketua AS hp_ketua');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');
		$this->db->select('organization_units_units.units_id AS parent_id');
		$this->db->select('parent_units.unit_name AS parent_name');
		$this->db->select('parent_units.unit_abbrevation AS parent_abbrevation');
		$this->db->select('parent_units.unit_slug AS parent_slug');
		$this->db->select('default_location_provinsi.id as id_provinsi, default_location_provinsi.nama as provinsi, default_location_kota.id as id_kota, default_location_kota.nama as kota');

		if(is_string($type_slug)){
			$this->db->where('organization_types.type_slug', $type_slug);
		}else{
			$this->db->where('organization_types.id', $type_slug);
			
		}

		// filters
		if(isset($filters) AND is_array($filters)) {
			foreach ($filters as $key => $value) {
				$this->db->where($key, $value,false);
			}
		}

		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$this->db->join('organization_units_units', 'organization_units.id = organization_units_units.row_id', 'left');
		$this->db->join('organization_units parent_units', 'parent_units.id = organization_units_units.units_id', 'left');

		$this->db->join('default_location_kota', 'default_location_kota.id = organization_units.id_kota');
		$this->db->join('default_location_provinsi', 'default_location_provinsi.id = default_location_kota.id_provinsi');

		$this->db->order_by('parent_name', 'asc');
		$this->db->order_by('unit_name', 'asc');

		$res = $this->db->get('organization_units')->result_array();
		return $res;
	}

	public function get_units_by_level($level, $provinsi=null, $kota = null, $pagination_config = null, $type = null, $table = true)
	{
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.nama_ketua AS nama_ketua');
		$this->db->select('organization_units.hp_ketua AS hp_ketua');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_units.kode_tti AS kode_tti');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');

		$this->db->select('location_kota.id AS id_kota');
		$this->db->select('location_kota.nama AS kota');
		$this->db->select('location_provinsi.id AS id_provinsi');
		$this->db->select('location_provinsi.nama AS provinsi');

		if($this->input->get('f-provinsi') != ''){
			$this->db->where('location_kota.id_provinsi', $this->input->get('f-provinsi'));
		}
		if($provinsi != NULL){
			$this->db->where('location_kota.id_provinsi', $provinsi);
		}

		if($this->input->get('f-kota') != ''){
			$this->db->where('location_kota.id', $this->input->get('f-kota'));
		}else{
			if($kota != NULL){
				$this->db->where('location_kota.id', $kota);
			}
		}

		if($this->input->get('f-type') != ''){
			if ($this->input->get('f-type') == '2' or $this->input->get('f-type') == '4'){
				if($table){
					$level = 1;
				}else{
					$level = 0;	
				}
			}
			// $this->db->where('organization_types.id', $this->input->get('f-type'));
			if($table){
				$this->db->where('organization_types.id', $this->input->get('f-type'));
			}
		}
		if($this->input->get('f-tipe_laporan') > 2){
			if ($this->input->get('f-tipe_laporan') == '4'){
				$level = 1;
			}
			if($type == NULL){
				$this->db->where('organization_types.id', $this->input->get('f-tipe_laporan'));
			}
		}
		if($type != NULL){
			$this->db->where('organization_types.id', $type);
		}

		$this->db->where('organization_types.type_level', $level);
		$this->db->or_where('organization_types.type_level', NULL);
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');

		$this->db->join('location_kota', 'location_kota.id = organization_units.id_kota', 'left');
		$this->db->join('location_provinsi', 'location_provinsi.id = location_kota.id_provinsi', 'left');

		$res = $this->db->get('organization_units')->result_array();
		// dump($this->db->last_query());
		return $res;
	}

	public function get_units_without_pendamping($provinsi=null, $kota = null, $pagination_config = null , $type = null)
	{
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.nama_ketua AS nama_ketua');
		$this->db->select('organization_units.hp_ketua AS hp_ketua');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_units.kode_tti AS kode_tti');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');

		$this->db->select('location_kota.id AS id_kota');
		$this->db->select('location_kota.nama AS kota');
		$this->db->select('location_provinsi.id AS id_provinsi');
		$this->db->select('location_provinsi.nama AS provinsi');

		if($this->input->get('f-provinsi') != ''){
			$this->db->where('location_kota.id_provinsi', $this->input->get('f-provinsi'));
		}
		if($provinsi != NULL){
			$this->db->where('location_kota.id_provinsi', $provinsi);
		}

		if($this->input->get('f-kota') != ''){
			$this->db->where('location_kota.id', $this->input->get('f-kota'));
		}
		if($kota != NULL){
			$this->db->where('location_kota.id', $kota);
		}

		if($this->input->get('f-type') != ''){
			$this->db->where('organization_types.id', $this->input->get('f-type'));
		}
		if($type != NULL){
			$this->db->where('organization_types.id', $type);
		}

		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$this->db->join('organization_memberships', 'organization_units.id=organization_memberships.membership_unit','left');
		$this->db->where('membership_user is null',null,false);
		$this->db->join('location_kota', 'location_kota.id = organization_units.id_kota', 'left');
		$this->db->join('location_provinsi', 'location_provinsi.id = location_kota.id_provinsi', 'left');

		$res = $this->db->get('organization_units')->result_array();

		return $res;
	}

	public function get_units_by_parent($parent_id, $type_level = 0, $group = NULL)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.nama_ketua AS nama_ketua');
		$this->db->select('organization_units.hp_ketua AS hp_ketua');
		$this->db->select('organization_units.kode_tti AS kode_tti');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');
		$this->db->select('organization_units_units.units_id AS parent_id');

		$this->db->select('location_kota.id AS id_kota');
		$this->db->select('location_kota.nama AS kota');
		$this->db->select('location_provinsi.id AS id_provinsi');
		$this->db->select('location_provinsi.nama AS provinsi');

		$this->db->where('organization_units_units.units_id', $parent_id);

		$this->db->join('organization_units', 'organization_units.id = organization_units_units.row_id', 'left');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');

		$this->db->join('location_kota', 'location_kota.id = organization_units.id_kota', 'left');
		$this->db->join('location_provinsi', 'location_provinsi.id = location_kota.id_provinsi', 'left');

		if(!group_has_role('laporan','create_own_unit_laporan') && !group_has_role('laporan','create_laporan') && !group_has_role('laporan','create_own_prov_laporan') && isset($this->current_user->id)){
			if($group == NULL && $group == 7){
				$this->db->join('organization_memberships m','m.membership_unit = organization_units.id','left');
				$this->db->where('m.membership_user', $this->current_user->id);
			}
		}

		$res = $this->db->get('organization_units_units')->result_array();

		return $res;
	}

	public function get_units_by_ids($where)
	{
		$this->db->select('id,unit_name');
		if ($where!='') $this->db->where($where);
		$this->db->join('users', 'organization_units.created_by=users.id');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$query = $this->db->get('organization_units');
		$result = $query->result_array();

        return $result;
	}

	public function get_units_by_id($id)
	{
		$this->db->select('organization_units.*, users.username as created_by_username, users.id as created_by_user_id, users.email as created_by_email');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');
		$this->db->select('location_kota.id_provinsi');
		$this->db->select('location_kota.nama AS kota');
		$this->db->select('location_provinsi.nama AS provinsi');

		if ($id!='') $this->db->where('organization_units.id', $id);
		$this->db->join('users', 'organization_units.created_by=users.id');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$this->db->join('location_kota', 'organization_units.id_kota = location_kota.id', 'left');
		$this->db->join('location_provinsi', 'location_kota.id_provinsi = location_provinsi.id', 'left');
		$query = $this->db->get('organization_units');

		$result = $query->row();

        return $result;
	}

	public function get_parent_units_by_id($id)
	{
		$this->db->select('organization_units.*, users.username as created_by_username, users.id as created_by_user_id, users.email as created_by_email');
		if ($id!='') $this->db->where('organization_units_units.row_id', $id);
		$this->db->join('users', 'organization_units.created_by=users.id');
		$this->db->join('organization_units_units', 'organization_units_units.units_id=organization_units.id', 'LEFT');
		$query = $this->db->get('organization_units');

		$result = $query->result_array();

        return $result;
	}

	public function insert_units($values) {
		$unit_parents = array();
		if(isset($values['unit_parents'])) {
			$unit_parents = $values['unit_parents'];
			unset($values['unit_parents']);
		}

		$values['created'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;

		$this->db->insert('organization_units', $values);
		$id_unit = $this->db->insert_id();

		if(count($unit_parents)>0 && is_array($unit_parents)) {
			if(is_array($unit_parents)) {
				foreach ($unit_parents as $p) {
					if($p!='') {
						$this->db->insert('organization_units_units', array('row_id' => $id_unit, 'units_id'=>$p));
					}
				}
			} else {
				$this->db->insert('organization_units_units', array('row_id' => $id_unit, 'units_id'=>$unit_parents));
			}
		}elseif($unit_parents != '' && !is_array($unit_parents)){
			$this->db->insert('organization_units_units', array('row_id' => $id_unit, 'units_id'=>$unit_parents ));
		}

		return true;
	}

	public function update_units($values, $row_id) {
		$unit_parents = array();
		if(isset($values['unit_parents']) && $values['unit_parents'] != '') {
			$unit_parents = $values['unit_parents'];
		}

		unset($values['row_edit_id']);
		unset($values['unit_parents']);

		$values['updated'] = date("Y-m-d H:i:s");

		$this->db->where('id', $row_id);
		$this->db->update('organization_units', $values);

		if(count($unit_parents)>0) {
			$this->db->where('row_id', $row_id);
			$this->db->delete('organization_units_units');

			if(is_array($unit_parents)) {
				foreach ($unit_parents as $p) {
					$this->db->insert('organization_units_units', array('row_id' => $row_id, 'units_id'=>$p));
				}
			} else {
				$this->db->insert('organization_units_units', array('row_id' => $row_id, 'units_id'=>$unit_parents));
			}
		}

		return true;
	}

	public function delete_units_by_id($id)
	{
		$this->db->where('membership_unit', $id);
		$this->db->delete('organization_memberships');

		$this->db->where('row_id', $id);
		$this->db->delete('organization_units_units');

		$this->db->where('id', $id);
		$this->db->delete('organization_units');
	}

	public function get_units_by_kota($id_kota, $unit_type = null, $row_id = NULL){
		$this->db->where('id_kota', $id_kota);
		if($unit_type == NULL){
			$this->db->select('u.*, t.available_groups');
			$this->db->from('organization_units u');
			$this->db->join('organization_types t', 't.id = u.unit_type');
			$this->db->where('u.unit_type !=',2);
			$this->order_by('u.id, u.unit_type','ASC');
			$query = $this->db->get();
			$result = $query->result_array();
			foreach ($result as $key => $data) {
				$result[$key]['childs'] = $this->get_units_child_by_parent($id_kota, $data['id']);
			}
			return $result;
		}else{

			$this->db->where('unit_type', $unit_type);
			$query = $this->db->get('organization_units');
			$result = $query->result_array();
		}
		return $result;
	}

	public function get_units_child_by_parent($id_kota, $parent_id){
		$this->db->select('u.*, t.available_groups');
		$this->db->from('organization_units u');
		$this->db->join('organization_units_units u2','u2.row_id = u.id');
		$this->db->join('organization_types t', 't.id = u.unit_type');
		$this->db->where('u2.units_id',$parent_id);
		$this->db->where('u.id_kota', $id_kota);
		$this->db->order_by('u.id, u.unit_type','ASC');
		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	public function get_unit_by_child($child_id)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_units_units.units_id AS parent_id');

		$this->db->where('organization_units_units.row_id', $child_id);

		$this->db->join('organization_units', 'organization_units.id = organization_units_units.units_id', 'left');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');

		$res = $this->db->get('organization_units_units')->row_array();
		return $res;
	}

	public function get_child($units_id)
	{
		$this->db->select('row_id');
		if(is_array($units_id)){
			$this->db->where_in('units_id',$units_id);
		}else{
			$this->db->where('units_id',$units_id);
		}
		if(!group_has_role('laporan','view_own_unit_laporan') && !group_has_role('laporan','view_all_laporan')){
			$this->db->join('organization_memberships','organization_memberships.membership_unit = organization_units_units.row_id');
			$this->db->where('organization_memberships.membership_user', $this->current_user->id);
		}
		$query = $this->db->get('organization_units_units');
		$hasil = $query->result_array();
		$where = '';
		if(count($hasil) > 0){
			foreach($hasil as $data){
				$where .= ",".$data['row_id'];
				$hasil_sementara = $this->get_child($data['row_id']);
				if(isset($hasil_sementara)){
					$where .= $hasil_sementara;
				}
			}
			return $where;
		}

	}

	public function get_sumberdata($pagination_config = NULL)
	{
		$this->db->select('t.id, t.type_name, lp.id as id_provinsi, lp.nama as provinsi, k.id as id_kota, k.nama as kota, u.unit_name, u.id as unit_id, u.kode_tti');
		$this->db->from('default_organization_units u');
		$this->db->join('default_organization_types t', 't.id = u.unit_type');
		$this->db->join('default_location_kota k', 'k.id = u.id_kota');
		$this->db->join('default_location_provinsi lp', 'lp.id = k.id_provinsi');

		if($this->input->get('f-provinsi') != ''){
			$this->db->where('lp.id', $this->input->get('f-provinsi'));
		}
		if($this->input->get('f-kota') != ''){
			$this->db->where('k.id', $this->input->get('f-kota'));
		}
		if($this->input->get('f-tipe_laporan') != ''){
			$this->db->where('t.id', $this->input->get('f-tipe_laporan'));
		}


		$this->db->order_by('lp.id, k.id, t.id', 'ASC');
		// $this->db->group_by('t.id, k.id');
		if($pagination_config){
			$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
			$this->db->limit($pagination_config['per_page'], $start);
		}
		$this->db->order_by('lp.nama,u.unit_name');
		$query = $this->db->get();

		$result = $query->result_array();

    return $result;
	}

	public function is_used($id){
		$this->db->where('id_unit', $id);
		$query = $this->db->get('default_laporan_data');

		if($query->num_rows() > 0){
			return false;
		}
		return true;
	}
}
