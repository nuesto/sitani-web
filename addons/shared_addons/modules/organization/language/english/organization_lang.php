<?php

$lang['cp:nav_organization'] = 'Organization';

/*****************************************************
 * STREAMS: units
 *****************************************************/
$lang['organization:units:singular'] = 'Unit';
$lang['organization:units:plural'] = 'Units';
$lang['organization:units:no_entry'] = 'There are currently no Units';
$lang['organization:units:new'] = 'Add Unit';
$lang['organization:units:edit'] = 'Edit Unit';
$lang['organization:units:view'] = 'Unit Detail';
$lang['organization:units:submit_success'] = 'Unit submitted successfully.';
$lang['organization:units:submit_failure'] = 'There was a problem submitting your Units.';
$lang['organization:units:deleted'] = 'The Unit was deleted.';
$lang['organization:units:delete_fail_has_child'] = 'This unit can\'t be deleted because has child.';
$lang['organization:units:select'] = '-- Select Unit --';

/*****************************************************
 * STREAMS: types
 *****************************************************/
$lang['organization:types:singular'] = 'Type';
$lang['organization:types:plural'] = 'Types';
$lang['organization:types:no_entry'] = 'There are currently no Type';
$lang['organization:types:new'] = 'Add Type';
$lang['organization:types:edit'] = 'Edit Type';
$lang['organization:types:view'] = 'Type Detail';
$lang['organization:types:submit_success'] = 'Type submitted successfully.';
$lang['organization:types:submit_failure'] = 'There was a problem submitting your Type.';
$lang['organization:types:deleted'] = 'The Type was deleted.';
$lang['organization:types:deleted_fail'] = 'There was an error deletting your Type.';

/*****************************************************
 * STREAMS: memberships
 *****************************************************/
$lang['organization:memberships:singular'] = 'Membership';
$lang['organization:memberships:plural'] = 'Memberships';
$lang['organization:memberships:no_entry'] = 'There are currently no member';
$lang['organization:memberships:new'] = 'Add member';
$lang['organization:memberships:edit'] = 'Edit Membership';
$lang['organization:memberships:view'] = 'Membership Detail';
$lang['organization:memberships:submit_success'] = 'Member added successfully.';
$lang['organization:memberships:submit_failure'] = 'There was a problem submitting new member.';
$lang['organization:memberships:deleted'] = 'The Membership was deleted.';


/*****************************************************
 * FIELDS
 *****************************************************/
$lang['organization:unit_name'] = 'Name';
$lang['organization:unit_abbrevation'] = 'Abbrevation';
$lang['organization:unit_slug'] = 'Slug';
$lang['organization:unit_description'] = 'Description';
$lang['organization:unit_type'] = 'Type';
$lang['organization:unit_sort_order'] = 'Sort Order';
$lang['organization:unit_parents'] = 'Parents';
$lang['organization:choose_type'] = 'Choose Unit Type';
$lang['organization:type_name'] = 'Name';
$lang['organization:type_slug'] = 'Slug';
$lang['organization:type_level'] = 'Level';
$lang['organization:type_description'] = 'Description';
$lang['organization:available_groups'] = 'Available Groups';
$lang['organization:membership_unit'] = 'Unit';
$lang['organization:membership_user'] = 'User';
$lang['organization:title_name'] = 'Name';
$lang['organization:title_description'] = 'Description';

/*****************************************************
 * DEFAULT FIELDS
 *****************************************************/
$lang['organization:created'] = 'Created';
$lang['organization:updated'] = 'Updated';
$lang['organization:created_by'] = 'Created by';

/*****************************************************
 * STREAMS (GENERAL)
 *****************************************************/
$lang['organization:streams'] = 'Streams';
$lang['organization:view_options'] = 'View Options';
$lang['organization:field_assignments'] = 'Field Assignments';
$lang['organization:new_assignment'] = 'New Field Assignment';
$lang['organization:edit_assignment'] = 'Edit Field Assignment';

/*****************************************************
 * GLOBAL
 *****************************************************/
$lang['organization:back'] = 'Back';
$lang['organization:confirm_delete'] = 'Are you sure?';
$lang['organization:detail'] = 'Detail';
$lang['organization:all_hieararchy'] = 'All Hierarchy';

$lang['organization:membership_user_already_registered'] = 'User is already become member in this Unit.';
$lang['organization:count_member'] = '# Members';
$lang['organization:you'] = '+you';
$lang['organization:loading'] = 'Loading...';