<div class="page-header">
	<h1><?php echo lang('organization:import') ?></h1>
</div>
<br>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="well">
	<b>Agar import pengguna dapat berjalan lancar dan mudah, pastikan dokumen csv anda memenuhi kriteria berikut.		<br>1. Fields dibatasi  ','
		<br>2. Line dibatasi 
		<hr>
		Pastikan dokumen csv anda memiliki hanya(jika hanya) field berikut		
		<br>1. Provinsi	(wajib diisi)	
		<br>2. Kota/Kabupaten	(wajib diisi)		
		<br>3. Nama	Gapoktan / TTI(wajib diisi)		
		<br>4. Alamat		
		<br>5. Nama Ketua	
		<br>6. No HP Ketua		
		<br>7. Satuan Induk (Jika TTI, maka wajib diisi dengan nama gapoktan yang sudah terdaftar disistem)
		<br>8. Kota Satuan Induk (Jika TTI dan jika kota gapoktan tidak sama dengan kota TTI maka wajib diisi)
	</b>
	<br><br>
	<a href="<?php echo base_url() ?>admin/organization/units/csv_example" class="btn btn-sm btn-primary"><i class="icon-download-alt"></i> <span>Download Contoh CSV</span></a>
</div>

<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="provinsi">Pilih File</label>

		<div class="col-sm-6">
			<input type="file" name="file" class="col-xs-10 col-sm-5">
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="submit-import" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>