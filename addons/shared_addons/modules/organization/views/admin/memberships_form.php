<div class="page-header">
	<h1><?php echo lang('organization:memberships:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['membership_unit']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['membership_unit']['input_title']);?> <?php echo $fields['membership_unit']['required'];?></label>

		<div class="col-sm-10">
			<?php if($mode == 'edit'){ ?>

			<span class="form-entry"><?php echo $fields['membership_unit']['entry_value']->unit_name; ?></span>
			<input type="hidden" name="<?php echo $fields['membership_unit']['input_slug'];?>" value="<?php echo $fields['membership_unit']['value'];?>" />

			<?php }else{ ?>

			<span class="form-entry"><?php echo $unit->unit_name; ?></span>
			<input type="hidden" name="<?php echo $fields['membership_unit']['input_slug'];?>" value="<?php echo $unit->id; ?>" />

			<?php } ?>
			
			<?php if( $fields['membership_unit']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['membership_unit']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['membership_user']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['membership_user']['input_title']);?> <?php echo $fields['membership_user']['required'];?></label>

		<div class="col-sm-10">
			<?php if($mode == 'edit'){ ?>

			<span class="form-entry"><?php echo user_displayname($fields['membership_user']['value'], true); ?></span>
			<input type="hidden" name="<?php echo $fields['membership_user']['input_slug']; ?>" value="<?php echo $fields['membership_user']['value']; ?>" />

			<?php }else{ ?>

			<?php echo $fields['membership_user']['input']; ?>
			
			<?php } ?>
			
			<?php if( $fields['membership_user']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['membership_user']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>
<input type="hidden" value="<?php echo $return;?>" name="return" />

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>