<div class="page-header">
	<h1><?php echo lang('organization:units:plural'); ?></h1>


	<div class="btn-group content-toolbar">
		<a class="btn btn-yellow btn-sm" href="<?php echo base_url() ?>admin/organization/units/import">
			<i class="icon-plus"></i>
			<span class="no-text-shadow"><?php echo lang('organization:import') ?></span>
		</a>
	</div>
	<?php file_partial('shortcuts'); ?>
</div>

<div class="tabbable tab-link">
	<ul class="nav nav-tabs">
		<li class="<?php if($view == 'index'){echo 'active';} ?>">
			<a href="<?php echo site_url('admin/organization/units/index'); ?>"><?php echo lang('organization:all_hieararchy'); ?></a>
		</li>
		<li class="<?php if($view == 'tanpa_pendamping'){echo 'active';} ?>">
			<a href="<?php echo site_url('admin/organization/units/tanpa_pendamping'); ?>">Tanpa Pendamping</a>
		</li>
	</ul>
</div>

<?php echo form_open(base_url().'admin/organization/units/'.$view,array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
  <div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="f-provinsi"><?php echo lang('location:provinsi:singular'); ?></label>
    <div class="col-sm-10">
      <?php
        $value = null;
        if($this->input->get('f-provinsi') != ""){
          $value = $this->input->get('f-provinsi');
        }
        $disabled = false;
        if(!group_has_role('organization','view_all_units') AND group_has_role('organization','view_provinsi_units')) {
        	$disabled = true;
        }
      ?>
      <select name="f-provinsi" id="provinsi" <?php echo ($disabled) ? 'disabled="disabled"' : ''; ?>>
        <option value=""><?php echo lang('global:select-pick') ?></option>
        <?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
          <option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($value == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="f-kota"><?php echo lang('location:kota:singular'); ?></label>
    <div class="col-sm-10">
      <select name="f-kota" id="kota">
        <?php
          if($this->input->get('f-provinsi') != '') {
            $value = null;
            if($this->input->get('f-kota') != ""){
              $value = $this->input->get('f-kota');
            }
        ?>
          <option value=""><?php echo lang('global:select-pick') ?></option>
        <?php
            foreach ($kota['entries'] as $kota_entry){ ?>
              <option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
        <?php
            }
          }else{
        ?>
            <option value=""><?php echo lang('global:select-none') ?></option>
        <?php
          }
        ?>
      </select>

      <script type="text/javascript">
        $('#provinsi').change(function() {
          var id_provinsi = $(this).val();
          $("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
          $('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');
          $.ajax({
						url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
						dataType: 'json',
						success: function(data){
              if(data.length > 0){
                $('#kota').html('<option value="">-- Pilih --</option>');
              }else{
                $('#kota').html('<option value="">-- Tidak ada --</option>');
              }
							$.each(data, function(i, object){
								$('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
							});

							$('.loading-kota').html('');
						}
					});
        });
      </script>
      <span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="f-type"><?php echo lang('organization:types:singular'); ?></label>
    <div class="col-sm-10">

    	<?php
    		$value = null;
    		if($this->input->get('f-type') != ""){
    			$value = $this->input->get('f-type');
    		}
    	?>
    	<select name="f-type" style="width:300px;">
    		<option value=""><?php echo lang('global:select-pick') ?></option>
    		<?php foreach ($types_all as $types_entry) { ?>
    			<option value="<?php echo $types_entry['id'] ?>" <?php echo ($value == $types_entry['id']) ? 'selected' : ''; ?>><?php echo $types_entry['type_name'] ?></option>
    		<?php } ?>
    	</select>
    </div>
	</div>

  <div class="form-group">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
      <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
        <i class="icon-ok"></i>
        <?php echo lang('buttons:filter'); ?>
      </button>
      <?php 
        if ($view == 'tanpa_pendamping'){
        	$url_clear = site_url("admin/organization/units/tanpa_pendamping");
        } else {
        	$url_clear = site_url("admin/organization/units/index");
        }
      ?>
      <a href="<?php echo $url_clear;?>" class="btn btn-xs">
        <i class="icon-remove"></i>
        <?php echo lang('buttons:clear'); ?>
      </a>
    </div>
  </div>

<?php echo form_close() ?>
<hr>
<?php if (count($units['entries']) > 0): ?>

<p class="pull-right"><?php echo lang('organization:showing').' '.count($units['entries']).' '.lang('organization:of').' '.$units['total'] ?></p>
<div class="table-responsive">
<table id="units_list" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>

			<th><?php echo lang('organization:unit_name'); ?></th>

			<?php if($filter_type == NULL){ ?>
				<th><?php echo lang('organization:unit_type'); ?></th>
			<?php }else{ ?>
				<th><?php echo lang('organization:unit_parents'); ?></th>
			<?php } ?>

			<th><?php echo lang('location:provinsi:singular') ?></th>
			<th><?php echo lang('location:kota:singular') ?></th>
			<th><?php echo lang('organization:nama_ketua') ?></th>
			<th><?php echo lang('organization:hp_ketua') ?></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php $count = 1; ?>
		<?php foreach ($units['entries'] as $units_entry): ?>
		<tr data-tt-id="<?php echo $units_entry['id']; ?>" <?php echo (isset($units_entry['parent_id'])) ? 'data-tt-parent-id="'.$units_entry['parent_id'].'"' : ''; ?>>

			<td>
			<?php
			if($units_entry['unit_abbrevation'] != NULL AND $units_entry['unit_abbrevation'] != ''){
				$units_entry['unit_name'] .= ' ('.$units_entry['unit_abbrevation'].')';
			}
			echo $units_entry['unit_name'];
			?>
			</td>

			<?php if($filter_type == NULL){ ?>
				<td><?php echo $units_entry['type_name']; ?></td>
			<?php }else{ ?>
				<td><?php echo $units_entry['parent_name']; ?></td>
			<?php } ?>

			<?php $members = $this->organization->get_membership_by_unit($units_entry['id']); ?>
			<td><?php echo $units_entry['provinsi']; ?>
			<td><?php echo $units_entry['kota']; ?>
			<td><?php echo $units_entry['nama_ketua']; ?></td>
			<td><?php echo $units_entry['hp_ketua']; ?></td>
			<td class="actions">

			<?php
			if(group_has_role('organization', 'view_all_units')){
				echo anchor('admin/organization/units/view/' . $units_entry['id'].'/'.$view. $uri, lang('global:view'), 'class="btn btn-xs btn-info view"');
			}elseif(group_has_role('organization', 'view_provinsi_units')){
				if($this->current_user->id_provinsi==$units_entry['id_provinsi'] and $units_entry['type_slug']=='tti'){
					echo anchor('admin/organization/units/view/' . $units_entry['id'].'/'.$view. $uri, lang('global:view'), 'class="btn btn-xs btn-info view"');
				}
			}elseif(group_has_role('organization', 'view_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/view/' . $units_entry['id'].'/'.$view. $uri, lang('global:view'), 'class="btn btn-xs btn-info view"');
				}
			}
			?>
			<?php
			if(group_has_role('organization', 'edit_all_units')){
				echo anchor('admin/organization/units/edit/' . $units_entry['id'].'/'.$view. $uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
			}elseif(group_has_role('organization', 'edit_provinsi_units')){
				if($this->current_user->id_provinsi==$units_entry['id_provinsi'] and $units_entry['type_slug']=='tti'){
					echo anchor('admin/organization/units/edit/' . $units_entry['id'].'/'.$view. $uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
				}
			}elseif(group_has_role('organization', 'edit_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/edit/' . $units_entry['id'].'/'.$view. $uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
				}
			}
			?>
			<?php
			// if(group_has_role('organization', 'view_all_memberships')){
			// 	echo anchor('admin/organization/memberships/index/' . $units_entry['id'], lang('organization:memberships:singular').' <span class="badge badge-yellow">'.$members['total'].'</span>', 'class="btn btn-xs btn-info view"');
			// }elseif(group_has_role('organization', 'view_own_memberships')){
			// 	if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
			// 		echo anchor('admin/organization/memberships/index/' . $units_entry['id'], lang('organization:memberships:singular').' <span class="badge badge-yellow">'.$members['total'].'</span>', 'class="btn btn-xs btn-info view"');
			// 	}
			// }
			?>
			<?php
			if(group_has_role('organization', 'delete_all_units')){
				echo anchor('admin/organization/units/delete/' . $units_entry['id'].'/'.$view. $uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
			}elseif(group_has_role('organization', 'delete_provinsi_units')){
				if($this->current_user->id_provinsi==$units_entry['id_provinsi'] and $units_entry['type_slug']=='tti'){
					echo anchor('admin/organization/units/delete/' . $units_entry['id'].'/'.$view. $uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
				}
			}elseif(group_has_role('organization', 'delete_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/delete/' . $units_entry['id'].'/'.$view. $uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
				}
			}
			?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>
<?php echo $units['pagination']; ?>

<script type="text/javascript">
$('#units_list').treetable(
	{expandable : true});
</script>

<?php else: ?>
<div class="well"><?php echo lang('organization:units:no_entry'); ?></div>
<?php endif;?>
