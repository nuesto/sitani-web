	<div class="page-header">
	<h1><?php echo lang('organization:units:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri, 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_name"><?php echo lang('organization:unit_name'); ?> *</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('unit_name') != NULL){
					$value = $this->input->post('unit_name');
				}elseif($mode == 'edit'){
					$value = $fields->unit_name;
				}
			?>
			<input name="unit_name" type="text" value="<?php echo $value; ?>" class="form-control" id="unit_name" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_abbrevation"><?php echo lang('organization:unit_abbrevation'); ?> </label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('unit_abbrevation') != NULL){
					$value = $this->input->post('unit_abbrevation');
				}elseif($mode == 'edit'){
					$value = $fields->unit_abbrevation;
				}
			?>
			<input name="unit_abbrevation" type="text" value="<?php echo $value; ?>" class="form-control" id="unit_abbrevation" />
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_slug"><?php echo lang('organization:unit_slug');?> *</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('unit_slug') != NULL){
					$value = $this->input->post('unit_slug');
				}elseif($mode == 'edit'){
					$value = $fields->unit_slug;
				}
			?>
			<input name="unit_slug" type="text" value="<?php echo $value; ?>" class="form-control" id="unit_slug" />
			<script type="text/javascript">$('#unit_name').slugify({slug:'#unit_slug', type:'-'});</script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-12 col-sm-2 control-label no-padding-right" for="nama_ketua"><?php echo lang('organization:nama_ketua'); ?> </label>

		<div class="col-xs-12 col-sm-6">
			<?php 
				$value = NULL;
				if($this->input->post('nama_ketua') != NULL){
					$value = $this->input->post('nama_ketua');
				}elseif($mode == 'edit'){
					$value = $fields->nama_ketua;
				}
			?>
			<input type="text" name="nama_ketua" class="form-control" id="nama_ketua" value="<?php echo $value; ?>" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-12 col-sm-2 control-label no-padding-right" for="hp_ketua"><?php echo lang('organization:hp_ketua'); ?> </label>

		<div class="col-xs-12 col-sm-4">
			<?php 
				$value = NULL;
				if($this->input->post('hp_ketua') != NULL){
					$value = $this->input->post('hp_ketua');
				}elseif($mode == 'edit'){
					$value = $fields->hp_ketua;
				}
			?>
			<input type="text" name="hp_ketua" class="form-control" id="hp_ketua" value="<?php echo $value; ?>" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_description"><?php echo lang('organization:alamat'); ?> </label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('unit_description') != NULL){
					$value = $this->input->post('unit_description');
				}elseif($mode == 'edit'){
					$value = $fields->unit_description;
				}
			?>
			<textarea name="unit_description" class="form-control" id="unit_description" row="10" cols="40"><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_type"><?php echo lang('organization:unit_type'); ?> *</label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('unit_type') != NULL){
					$value = $this->input->post('unit_type');
				}elseif($mode == 'edit'){
					$value = $fields->unit_type;
				}
				$val_unit_type = $value;

				// create provinsi only can add tti
				if(group_has_role('organization','create_provinsi_units')) {
					$new_unit_type = array();
					foreach ($unit_type as $kt => $kv) {
						if($kv['slug']=='tti') {
							$new_unit_type[] = $kv;
						}
						unset($unit_type[$kt]);
					}
					$unit_type = $new_unit_type;
				}
			?>
			
			<select name="unit_type" class="form-control" id="unit_type">
				<option value="">-- <?php echo lang('organization:choose_type'); ?> --</option>
			<?php foreach ($unit_type as $type) { ?>
				<option value="<?php echo $type['id']; ?>" <?php if($type['id'] == $value) echo 'selected'; ?>><?php echo $type['name']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group" style="display:none;">
		<label class="col-sm-2 control-label no-padding-right" for="unit_sort_order"><?php echo lang('organization:unit_sort_order');?> </label>

		<div class="col-sm-1">
			<select class="form-control" id="unit_sort_order" name="unit_sort_order">
				<?php for($i = -20; $i <= 20; $i++){ ?>
				<option <?php if($i == 0){echo 'selected';} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div id="location">
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('location:provinsi:singular'); ?> <span>*</span></label>

			<div class="col-sm-6">
				<?php 
					$val_prov = $id_provinsi;
					if($this->input->post('id_provinsi') != NULL){
						$val_prov = $this->input->post('id_provinsi');
					}elseif($mode == 'edit'){
						$val_prov = $fields->id_provinsi;
					}
				?>

				<?php if(group_has_role('organization','create_units')) { ?>
				<select id="provinsi" name="id_provinsi" class="col-xs-10 col-sm-5">
					<option value=""><?php echo lang('global:select-pick') ?></option>
					<?php foreach ($provinsi['entries'] as $provinsi_entry){ ?>
					<option value="<?php echo $provinsi_entry['id'] ?>" <?php echo ($val_prov == $provinsi_entry['id']) ? 'selected' : ''; ?>><?php echo $provinsi_entry['nama'] ?></option>
					<?php } ?>
				</select>
				<?php } else {
					foreach ($provinsi['entries'] as $pk => $pv) {
						if($pv['id']==$val_prov) {
							echo form_input(array('value'=>$pv['nama'],'disabled'=>'disabled'));
							echo form_input(array('name'=>'id_provinsi','value'=>$pv['id'],'id'=>'provinsi','type'=>'hidden'));
						}
					}
				} ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="kota"><?php echo lang('location:kota:singular'); ?> <span>*</span></label>

			<div class="col-sm-6">
				<?php
					$value = $id_kota;
					if($this->input->post('id_kota') != NULL){
						$value = $this->input->post('id_kota');
					}elseif($mode == 'edit'){
						$value = $fields->id_kota;
					}
				?>
				<select name="id_kota" id="kota" class="col-xs-10 col-sm-5">
	    		<?php 
	    			if($val_prov != NULL) {
	    		?>
	    			<option value=""><?php echo lang('global:select-pick') ?></option>
	    		<?php 
	      			foreach ($kota['entries'] as $kota_entry){ ?>
	      				<option value="<?php echo $kota_entry['id'] ?>" <?php echo ($value == $kota_entry['id']) ? 'selected' : ''; ?>><?php echo $kota_entry['nama'] ?></option>
	      	<?php 
	      			} 
	      		}else{
	      	?>
	      			<option value=""><?php echo lang('global:select-none') ?></option>
	      	<?php
	      		}
	      	?>
	    	</select>
	    	<span class="loading-kota" style="margin-left: 5px; display: inline-block; padding-top: 4px;"></span>

	    	<script type="text/javascript">
				$('#provinsi').change(function() {
					var id_provinsi = $(this).val();
					var type_id = $('#unit_type').val();
					$("#kota").html('<option value=""><?php echo  lang("global:ajax_load_data") ?></option>');
					$('.loading-kota').html(' <i class="icon-spinner icon-spin orange bigger-150"></i>');

					$.ajax({
						url: "<?php echo site_url('location/kelurahan/ajax_get_kota_by_id_provinsi') ?>" + '/' + id_provinsi,
						dataType: 'json',
						success: function(data){
							$('#kota').html('<option value="">--</option>');
							$.each(data, function(i, object){
								$('#kota').append('<option value="' + object['id'] + '">' + object['nama'] + '</option>');
							});

							$('.loading-kota').html('');
							if(type_id == 2){
								$('#unit_type').change();
							}
						}
					});
				});

				$('#kota').change(function() {
					var type_id = $('#unit_type').val();
					if(type_id == 2){
						$('#unit_type').change();
					}
				});
	    	</script>
			</div>
		</div>
	</div>
	<div class="form-group" id="unit_parent" <?php echo ($val_unit_type != 2 && $val_unit_type != 4 && $val_unit_type != 7) ? 'style="display:none"' : ''; ?>>
		<label class="col-sm-2 control-label no-padding-right" for="unit_parents"><?php echo lang('organization:unit_parents'); ?>*</span></label>

		<?php if($val_unit_type != 2 && $val_unit_type != 4 && $val_unit_type != 7){ ?>
			<div id="unit_parents_container" class="col-sm-10">
				<p id="unit-parents" class="form-control-static"><?php echo lang('organization:loading'); ?></p>
				<img id="unit-loader" src="<?php echo base_url("addons/shared_addons/modules/organization/img/ajax-loader.gif"); ?>">
			</div>
		<?php }else{ ?>
			<div id="unit_parents_container" class="col-sm-10">
				<p id="unit-parents" class="form-control-static"><?php echo $content_unit_parents; ?></p>
				<img id="unit-loader" src="<?php echo base_url("addons/shared_addons/modules/organization/img/ajax-loader.gif"); ?>">
			</div>
		<?php } ?>
	</div>

	<script>
		$(document).ready(function(){
			$('#unit-loader').hide();
			// make get request
			var type_id = $('#unit_type').val();

			<?php if($val_unit_type != 2 && $val_unit_type != 4 && $val_unit_type != 7) { ?>
				$("#unit-parents").load("<?php echo site_url('admin/organization/units/ajax_unit_parents_checkbox') ?>" + '/' + type_id, function(){
					//$("#unit_parents").html('Loading..');
				});
			<?php } ?>
		});
		
		$('#unit_type').change(function(){
			$("#unit-parents").html('');

			var type_id = $('#unit_type').val();
			var provinsi = $('#provinsi').val();
			var kota = $('#kota').val();
			if(type_id == 2 || type_id == 4){
				$("#unit_parent").show();
				if((provinsi != '' && (type_id ==2)) || type_id == 4){
					$("#unit-parents").hide();
					$('#unit-loader').show();
					var link = "<?php echo site_url('admin/organization/units/ajax_unit_parents_checkbox') ?>" + '/' + type_id +'/'+provinsi+'/'+kota;
					if(type_id == 4){
						link = "<?php echo site_url('admin/organization/units/ajax_unit_parents_checkbox') ?>" + '/' + type_id;
					}
					console.log(link);
					$("#unit-parents").load(link, function(){
						//$("#unit_parents").html('Loading..');
						$('#unit-loader').hide();
						$("#unit-parents").show();
					});
				}
			}else{
				$("#unit_parent").hide();
			}
		});
	</script>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $fields->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>