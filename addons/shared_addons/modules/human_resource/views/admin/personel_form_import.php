<div class="page-header">
	<h1><?php echo lang('user:import_title') ?></h1>
</div>
<?php echo form_open_multipart(uri_string().'?'.$_SERVER['QUERY_STRING'], 'class="form-horizontal" autocomplete="off"') ?>
<div class="well">
		<b>
		Agar import personel dapat berjalan lancar dan mudah, pastikan dokumen csv anda memenuhi kriteria berikut.
		<br>1. Fields dibatasi  ','
		<br>2. Line dibatasi '\n'
		<hr>
		Pastikan dokumen csv anda memiliki hanya(jika hanya) field berikut
		<br>1. <?php echo lang('human_resource:nama_lengkap'); ?> (Required)
		<br>2. <?php echo lang('human_resource:jenis_kelamin'); ?> (Required)
		<br>3. <?php echo lang('human_resource:nomor_induk'); ?>
		<br>4. <?php echo lang('human_resource:tanggal_lahir'); ?>
		<br>5. <?php echo lang('human_resource:id_status_pekerja'); ?>
		<br>6. <?php echo lang('human_resource:id_level'); ?>
		<br>7. <?php echo lang('human_resource:tahun_mulai_bekerja'); ?>
		<br>8. <?php echo lang('human_resource:tahun_mulai_profesional'); ?>
		<br>9. <?php echo lang('human_resource:id_metode_rekrutmen'); ?>
		<br>10. <?php echo lang('human_resource:id_supervisor'); ?>
		<br>11. <?php echo lang('human_resource:handphone'); ?>
		<br>12. <?php echo lang('human_resource:email'); ?>
		<br>13. <?php echo lang('human_resource:id_organization_unit'); ?>
		<br>14. <?php echo lang('human_resource:id_user'); ?>
		</b>
		<br>
		
</div>
<div class="form-group">
	<div class="col-sm-10">
		<a href="<?php echo site_url().'admin/human_resource/personel/download_csv_sample'; ?>" class="btn btn-sm btn-info"><i class="icon-cloud-download"></i> Download Contoh Csv</a>
	</div>
</div>
<fieldset>
	
	<div class="form-group">
		<label class="col-md-2 control-label no-padding-right"><?php echo lang('user:select_file') ?> <span>*</span></label>
		<div class="col-sm-10">
			<input type="file" name="file">
			<!--<?php echo form_upload('file','', 'name="file"') ?>-->
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-2 control-label no-padding-right"><?php echo lang('user:set_header') ?> </label>
		<div class="col-sm-10">
			<?php echo form_checkbox('header', 'yes', TRUE); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
		<div class="col-md-offset-2 col-md-9">
			<button class="btn btn-sm btn-success" value="submit" name="btnAction" type="submit">
				<span><?php echo lang('human_resource:personel:import'); ?></span>
			</button>
		</div>
	</div>
</fieldset>
	
<?php echo form_close() ?>