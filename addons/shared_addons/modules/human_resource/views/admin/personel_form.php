<style type="text/css">
	.row {
		margin-left: 0;
		margin-right: 0;
	}

	.alamat .row {
		margin-top: 10px;
	}
</style>
<div class="page-header">
	<h1><?php $title = lang('human_resource:personel:new');
        if($forced=='forced') {
			$title = 'Lengkapi data profil';
		}
		echo $title; ?></h1>
</div>

<?php echo form_open_multipart(uri_string(),array('id'=>'form-personel')); ?>

<div class="form-horizontal">
	<h3>Account</h3>
	<hr>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_user"><?php echo lang('human_resource:id_user'); ?></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				$disabled = NULL;
				if($this->input->post('id_user') != NULL){
					$value = $this->input->post('id_user');
				}elseif($mode == 'edit'){
					$value = $fields['id_user'];
				}

				if((!group_has_role('human_resource','create_personel') AND group_has_role('human_resource','create_own_user_personel'))
				OR (!group_has_role('human_resource','edit_all_personel') AND !group_has_role('human_resource','edit_own_personel') AND group_has_role('human_resource','edit_own_user_personel'))) {
					$value = $this->current_user->id;
					$disabled = 'disabled';
				}
				
				if($this->uri->segment(5)=='forced') {
					$value = $this->current_user->id;
					$disabled = 'disabled';
				}
			?>
			<!-- <input type="hidden" id="id_user" class="col-sm-12 select-ajax" data-source="<?php echo base_url(); ?>admin/human_resource/personel/get_user" name="id_user" style="padding-left:0;padding-right:0;"> -->
			
			<select name="id_user" <?php echo $disabled; ?> class="col-sm-12 select-ajax" id="select_user" data-default="<?php echo $value; ?>" data-source="<?php echo base_url().'admin/human_resource/personel/get_user/'.$value; ?>">
				
			</select>

			<?php if(group_has_role('users','create_users')) { ?>
			<?php echo anchor('admin/users/create', 'Buat User Baru', array('target' => '_blank', 'id' => 'buat_user')); ?> | 
			<a href="#" id="reload_user">Reload User List</a>
			<?php } ?>
		</div>
	</div>

	<h3>Data Personel</h3>
	<hr>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_lengkap"><?php echo lang('human_resource:nama_lengkap'); ?> <span style="color:red">*</span></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				if($this->input->post('nama_lengkap') != NULL){
					$value = $this->input->post('nama_lengkap');
				}elseif($mode == 'edit'){
					$value = $fields['nama_lengkap'];
				}
			?>
			<input name="nama_lengkap" type="text" value="<?php echo $value; ?>" class="form-control" id="nama_lengkap" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="jenis_kelamin"><?php echo lang('human_resource:jenis_kelamin'); ?> <span style="color:red">*</span></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('jenis_kelamin') != NULL){
					$value = $this->input->post('jenis_kelamin');
				}elseif($mode == 'edit'){
					$value = $fields['jenis_kelamin'];
				}
			?>
			
			<select name="jenis_kelamin" class="form-control" id="">
				<option value=""><?php echo lang('human_resource:choose'); ?></option>
				<option <?php echo (strtoupper($value)=='L') ? 'selected' : ''; ?> value="L"><?php echo lang('human_resource:jenis_kelamin_laki'); ?></option>
				<option <?php echo (strtoupper($value)=='P') ? 'selected' : ''; ?> value="P"><?php echo lang('human_resource:jenis_kelamin_perempuan'); ?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tanggal_lahir"><?php echo lang('human_resource:tanggal_lahir'); ?></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('tanggal_lahir') != NULL){
					$value = format_date($this->input->post('tanggal_lahir'), 'Y-m-d');
				}elseif($mode == 'edit'){
					$value = format_date($fields['tanggal_lahir'],'Y-m-d');
				}
			?>
			<div class="input-group">
				<input name="tanggal_lahir" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>" class="form-control date-picker" id="" />
				<span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
			</div>
		</div>
	</div>

	<h3>Alamat</h3>
	<hr>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="alamat_kantor"><?php echo lang('human_resource:alamat_kantor'); ?></label>

		<div class="col-sm-5 alamat">
			<div class="row">
				<?php 
					$value = NULL;
					if($this->input->post('address_office') != NULL){
						$value = $this->input->post('address_office');
					}elseif($mode == 'edit'){
						$value = $fields['address_office'];
					}
				?>
				<input name="address_office" type="text" value="<?php echo $value; ?>" class="form-control" id="" />
			</div>
			<div class="row">
				<?php
				$value = NULL;
				if($this->input->post('provinsi_office') != NULL){
					$value = $this->input->post('provinsi_office');
				}elseif($mode == 'edit'){
					$value = $personel_kelurahan_office['id_provinsi'];
				}
				?>
				<select name="provinsi_office" id="provinsi_kantor" class="col-sm-6" data-value="<?php echo $value; ?>">
					<option value=""><?php echo lang('location:choose_provinsi'); ?></option>
					<?php foreach ($provinsi_office as $prov) { ?>
						<option <?php echo ($prov['id']==$value) ? 'selected' : ''; ?> value="<?php echo $prov['id']; ?>"><?php echo $prov['nama']; ?></option>
					<?php }?>
				</select>

				<?php
				$value = NULL;
				if($this->input->post('kota_office') != NULL){
					$value = $this->input->post('kota_office');
				}elseif($mode == 'edit'){
					$value = $personel_kelurahan_office['id_kota'];
				}
				?>
				<select name="kota_office" id="kota_kantor" class="col-sm-6" data-value="<?php echo $value; ?>">
					<option value=""><?php echo lang('location:choose_kota'); ?></option>
					<?php foreach ($kota_office as $kota) { ?>
						<option <?php echo ($kota['id']==$value) ? 'selected' : ''; ?> value="<?php echo $kota['id']; ?>"><?php echo $kota['nama']; ?></option>
					<?php }?>
				</select>
			</div>
			<div class="row">
				<?php
				$value = NULL;
				if($this->input->post('kecamatan_office') != NULL){
					$value = $this->input->post('kecamatan_office');
				}elseif($mode == 'edit'){
					$value = $personel_kelurahan_office['id_kecamatan'];
				}
				?>
				<select name="kecamatan_office" id="kecamatan_kantor" class="col-sm-6" data-value="<?php echo $value; ?>">
					<option value=""><?php echo lang('location:choose_kecamatan'); ?></option>
					<?php foreach ($kecamatan_office as $kec) { ?>
						<option <?php echo ($kec['id']==$value) ? 'selected' : ''; ?> value="<?php echo $kec['id']; ?>"><?php echo $kec['nama']; ?></option>
					<?php }?>
				</select>

				<?php
				$value = NULL;
				if($this->input->post('kelurahan_office') != NULL){
					$value = $this->input->post('kelurahan_office');
				}elseif($mode == 'edit'){
					$value = $personel_kelurahan_office['id'];
				}
				?>
				<select name="id_location_kelurahan_office" id="kelurahan_kantor" class="col-sm-6" data-value="<?php echo $value; ?>">
					<option value=""><?php echo lang('location:choose_kelurahan'); ?></option>
					<?php foreach ($kelurahan_office as $kel) { ?>
						<option <?php echo ($kel['id']==$value) ? 'selected' : ''; ?> value="<?php echo $kel['id']; ?>"><?php echo $kel['nama']; ?></option>
					<?php }?>
				</select>
			</div>
			<div class="row">
				<?php 
					$value = NULL;
					if($this->input->post('postal_code_office') != NULL){
						$value = $this->input->post('postal_code_office');
					}elseif($mode == 'edit'){
						$value = $fields['postal_code_office'];
					}
				?>
				<input name="postal_code_office" type="text" value="<?php echo $value; ?>" class="col-sm-4" placeholder="Kode Pos" id="" />
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="alamat_kantor"><?php echo lang('human_resource:alamat_rumah'); ?></label>

		<div class="col-sm-5 alamat">
			<div class="row">
				<?php 
					$value = NULL;
					if($this->input->post('address_home') != NULL){
						$value = $this->input->post('address_home');
					}elseif($mode == 'edit'){
						$value = $fields['address_home'];
					}
				?>
				<input name="address_home" type="text" value="<?php echo $value; ?>" class="form-control" id="" />
			</div>
			<div class="row">
				<?php
				$value = NULL;
				if($this->input->post('provinsi_home') != NULL){
					$value = $this->input->post('provinsi_home');
				}elseif($mode == 'edit'){
					$value = $personel_kelurahan_home['id_provinsi'];
				}
				?>
				<select name="provinsi_home" id="provinsi_rumah" class="col-sm-6" data-value="<?php echo $value; ?>">
					<option value=""><?php echo lang('location:choose_provinsi'); ?></option>
					<?php foreach ($provinsi_home as $prov) { ?>
						<option <?php echo ($prov['id']==$value) ? 'selected' : ''; ?> value="<?php echo $prov['id']; ?>"><?php echo $prov['nama']; ?></option>
					<?php }?>
				</select>

				<?php
				$value = NULL;
				if($this->input->post('kota_home') != NULL){
					$value = $this->input->post('kota_home');
				}elseif($mode == 'edit'){
					$value = $personel_kelurahan_home['id_kota'];
				}
				?>
				<select name="kota_home" id="kota_rumah" class="col-sm-6" data-value="<?php echo $value; ?>">
					<option value=""><?php echo lang('location:choose_kota'); ?></option>
					<?php foreach ($kota_home as $kota) { ?>
						<option <?php echo ($kota['id']==$value) ? 'selected' : ''; ?> value="<?php echo $kota['id']; ?>"><?php echo $kota['nama']; ?></option>
					<?php }?>
				</select>
			</div>
			<div class="row">
				<?php
				$value = NULL;
				if($this->input->post('kecamatan_home') != NULL){
					$value = $this->input->post('kecamatan_home');
				}elseif($mode == 'edit'){
					$value = $personel_kelurahan_home['id_kecamatan'];
				}
				?>
				<select name="kecamatan_home" id="kecamatan_rumah" class="col-sm-6" data-value="<?php echo $value; ?>">
					<option value=""><?php echo lang('location:choose_kecamatan'); ?></option>
					<?php foreach ($kecamatan_home as $kec) { ?>
						<option <?php echo ($kec['id']==$value) ? 'selected' : ''; ?> value="<?php echo $kec['id']; ?>"><?php echo $kec['nama']; ?></option>
					<?php }?>
				</select>

				<?php
				$value = NULL;
				if($this->input->post('kelurahan_home') != NULL){
					$value = $this->input->post('kelurahan_home');
				}elseif($mode == 'edit'){
					$value = $personel_kelurahan_home['id'];
				}
				?>
				<select name="id_location_kelurahan_home" id="kelurahan_rumah" class="col-sm-6" data-value="<?php echo $value; ?>">
					<option value=""><?php echo lang('location:choose_kelurahan'); ?></option>
					<?php foreach ($kelurahan_home as $kel) { ?>
						<option <?php echo ($kel['id']==$value) ? 'selected' : ''; ?> value="<?php echo $kel['id']; ?>"><?php echo $kel['nama']; ?></option>
					<?php }?>
				</select>
			</div>
			<div class="row">
				<?php 
					$value = NULL;
					if($this->input->post('postal_code_home') != NULL){
						$value = $this->input->post('postal_code_home');
					}elseif($mode == 'edit'){
						$value = $fields['postal_code_home'];
					}
				?>
				<input name="postal_code_home" type="text" value="<?php echo $value; ?>" class="col-sm-4" placeholder="Kode Pos" id="" />
			</div>
		</div>
	</div>

	<h3>Kontak</h3>
	<hr>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="telpon_kantor"><?php echo lang('human_resource:telpon_kantor'); ?></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				if($this->input->post('phone_office') != NULL){
					$value = $this->input->post('phone_office');
				}elseif($mode == 'edit'){
					$value = $fields['phone_office'];
				}
			?>
			<input name="phone_office" type="text" value="<?php echo $value; ?>" class="form-control" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="telpon_rumah"><?php echo lang('human_resource:telpon_rumah'); ?></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				if($this->input->post('phone_home') != NULL){
					$value = $this->input->post('phone_home');
				}elseif($mode == 'edit'){
					$value = $fields['phone_home'];
				}
			?>
			<input name="phone_home" type="text" value="<?php echo $value; ?>" class="form-control" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="handphone"><?php echo lang('human_resource:handphone'); ?></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				if($this->input->post('handphone') != NULL){
					$value = $this->input->post('handphone');
				}elseif($mode == 'edit'){
					$value = $fields['handphone'];
				}
			?>
			<input name="handphone" type="text" value="<?php echo $value; ?>" class="form-control" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="email"><?php echo lang('human_resource:email'); ?></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				if($this->input->post('email') != NULL){
					$value = $this->input->post('email');
				}elseif($mode == 'edit'){
					$value = $fields['email'];
				}
			?>
			<input name="email" type="text" value="<?php echo $value; ?>" class="form-control" id="email" />
		</div>
	</div>

	<h3>Kepegawaian</h3>
	<hr>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nomor_induk"><?php echo lang('human_resource:nomor_induk'); ?></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('nomor_induk') != NULL){
					$value = $this->input->post('nomor_induk');
				}elseif($mode == 'edit'){
					$value = $fields['nomor_induk'];
				}
			?>
			<input name="nomor_induk" type="text" value="<?php echo $value; ?>" class="form-control" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_status_pekerja"><?php echo lang('human_resource:id_status_pekerja'); ?></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('id_status_pekerja') != NULL){
					$value = $this->input->post('id_status_pekerja');
				}elseif($mode == 'edit'){
					$value = $fields['id_status_pekerja'];
				}
			?>
			
			<select name="id_status_pekerja" class="form-control" id="">
				<?php
				echo '<option value="">'.lang('human_resource:choose').'</option>';
				if(count($status_pekerja)>0) {
					foreach ($status_pekerja as $sp) {
						$selected = '';
						if($sp['id']==$value) {
							$selected = 'selected';
						}
						echo '<option value="'.$sp['id'].'" '.$selected.'>'.$sp['nama_status'].'</option>';
					}
				} else {
					echo '<option value="">'.lang('human_resource:status_pekerja:no_entry').'</option>';
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_level"><?php echo lang('human_resource:id_level'); ?></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('id_level') != NULL){
					$value = $this->input->post('id_level');
				}elseif($mode == 'edit'){
					$value = $fields['id_level'];
				}
			?>
			
			<select name="id_level" class="form-control" id="">
				<?php
				echo '<option value="">'.lang('human_resource:choose').'</option>';
				if(count($level)>0) {
					foreach ($level as $lv) {
						$selected = '';
						if($lv['id']==$value) {
							$selected = 'selected';
						}
						echo '<option value="'.$lv['id'].'" '.$selected.'>'.$lv['nama_level'].'</option>';
					}
				} else {
					echo '<option value="">'.lang('human_resource:level:no_entry').'</option>';
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tahun_mulai_bekerja"><?php echo lang('human_resource:tahun_mulai_bekerja'); ?></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('tahun_mulai_bekerja') != NULL){
					$value = $this->input->post('tahun_mulai_bekerja');
				}elseif($mode == 'edit'){
					$value = $fields['tahun_mulai_bekerja'];
				}
			?>
			
			<select name="tahun_mulai_bekerja" class="form-control" id="">
				<option value=""><?php echo lang('human_resource:choose'); ?></option>
				<?php for($tahun = date('Y'); $tahun >= date('Y')-25; $tahun--) {
					$selected = '';
					if($tahun==$value) {
						$selected = 'selected';
					}
					echo '<option value="'.$tahun.'" '.$selected.'>'.$tahun.'</option>';
				} ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tahun_mulai_profesional"><?php echo lang('human_resource:tahun_mulai_profesional'); ?></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('tahun_mulai_profesional') != NULL){
					$value = $this->input->post('tahun_mulai_profesional');
				}elseif($mode == 'edit'){
					$value = $fields['tahun_mulai_profesional'];
				}
			?>
			
			<select name="tahun_mulai_profesional" class="form-control" id="">
				<option value=""><?php echo lang('human_resource:choose'); ?></option>
				<?php for($tahun = date('Y'); $tahun >= date('Y')-25; $tahun--) {
					$selected = '';
					if($tahun==$value) {
						$selected = 'selected';
					}
					echo '<option value="'.$tahun.'" '.$selected.'>'.$tahun.'</option>';
				} ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_metode_rekrutmen"><?php echo lang('human_resource:id_metode_rekrutmen'); ?></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('id_metode_rekrutmen') != NULL){
					$value = $this->input->post('id_metode_rekrutmen');
				}elseif($mode == 'edit'){
					$value = $fields['id_metode_rekrutmen'];
				}
			?>
			
			<select name="id_metode_rekrutmen" class="form-control" id="">
				<?php
				echo '<option value="">'.lang('human_resource:choose').'</option>';
				if(count($metode_rekrutmen)>0) {
					foreach ($metode_rekrutmen as $md) {
						$selected = '';
						if($md['id']==$value) {
							$selected = 'selected';
						}
						echo '<option value="'.$md['id'].'" '.$selected.'>'.$md['nama_metode'].'</option>';
					}
				} else {
					echo '<option value="">'.lang('human_resource:metode_rekrutmen:no_entry').'</option>';
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_supervisor"><?php echo lang('human_resource:id_supervisor'); ?></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				$id = (isset($entry_id)) ? $entry_id : NULL;
				if($this->input->post('id_supervisor') != NULL){
					$value = $this->input->post('id_supervisor');
				}elseif($mode == 'edit'){
					$value = $fields['id_supervisor'];
				}
			?>
			
			<select name="id_supervisor" class="col-sm-12 select-ajax" data-default="<?php echo $value; ?>" data-source="<?php echo base_url().'admin/human_resource/personel/get_supervisor/'.$id; ?>">
				
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_organization_unit"><?php echo lang('human_resource:id_organization_unit'); ?></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				if($this->input->post('id_organization_unit') != NULL){
					$value = $this->input->post('id_organization_unit');
				}elseif($mode == 'edit'){
					$value = $fields['id_organization_unit'];
				}
			?>
			
			<select id="id_organization_unit" name="id_organization_unit" class="col-sm-12 select-ajax" data-default="<?php echo $value; ?>" data-source="<?php echo base_url(); ?>admin/human_resource/personel/get_organization_unit">
				
			</select>
		</div>
	</div>

	<h3>Jabatan</h3>
	<hr>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_organization_unit"><?php echo lang('human_resource:jabatan'); ?></label>

		<div class="col-sm-10">
			<?php
			for ($i=1; $i <=12 ; $i++) { 
				$bulan[$i] = date("F",strtotime(date("Y")."-".$i."-01"));
			}

			?>
			<table class="table table-bordered" id="tabel_jabatan">
				<thead>
					<tr>
						<th>Mulai</th>
						<th>Selesai</th>
						<th>Posisi</th>
						<th>Deskripsi</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($mode == 'edit' AND count($jabatan)>0) {
						foreach ($jabatan as $jab) {
					?>
					<tr>
						<td>
							<select name="bulan_mulai[]" class="bln_mulai">
								<?php 
								foreach ($bulan as $key => $bln) {
									$selected = '';
									if($jab['bulan_mulai']==$key) {
										$selected = 'selected';
									}
									
									echo '<option value="'.$key.'" '.$selected.'>'.$bln.'</option>';
								}?>
							</select>
							<select name="tahun_mulai[]" class="thn_mulai">
								<?php for ($thn=date('Y'); $thn >=date('Y')-25 ; $thn--) { 
									$selected = '';
									if($jab['tahun_mulai']==$thn) {
										$selected = 'selected';
									}

									echo '<option value="'.$thn.'" '.$selected.'>'.$thn.'</option>';
								}

								?>
							</select>
						</td>
						<td>
							<select name="bulan_selesai[]" class="bln_selesai">
								<?php foreach ($bulan as $key => $bln) {
									$selected = '';
									if($jab['bulan_selesai']==$key) {
										$selected = 'selected';
									}

									echo '<option value="'.$key.'" '.$selected.'>'.$bln.'</option>';
								}?>
							</select>
							<select name="tahun_selesai[]" class="thn_selesai">
								<?php for ($thn=date('Y'); $thn >=date('Y')-25 ; $thn--) { 
									$selected = '';
									if($jab['tahun_selesai']==$thn) {
										$selected = 'selected';
									}

									echo '<option value="'.$thn.'" '.$selected.'>'.$thn.'</option>';
								}

								?>
							</select>
						</td>
						<td><input type="text" name="posisi[]" class="posisi" value="<?php echo (isset($jab['posisi'])) ? $jab['posisi'] : ''; ?>"></td>
						<td><input type="text" name="deskripsi_jabatan[]" class="deskripsi_jabatan" value="<?php echo (isset($jab['deskripsi'])) ? $jab['deskripsi'] : ''; ?>"></td>
						<td><a href="#tabel_jabatan"><i class="fa fa-times fa-2x hapus_jabatan"></i></a></td>
					</tr>
					<?php
						}
					} else {
					?>
					<tr>
						<td>
							<select name="bulan_mulai[]" class="bln_mulai">
								<?php foreach ($bulan as $key => $bln) {
									echo '<option value="'.$key.'">'.$bln.'</option>';
								}?>
							</select>
							<select name="tahun_mulai[]" class="thn_mulai">
								<?php for ($thn=date('Y'); $thn >=date('Y')-25 ; $thn--) { 
									echo '<option value="'.$thn.'">'.$thn.'</option>';
								}

								?>
							</select>
						</td>
						<td>
							<select name="bulan_selesai[]" class="bln_selesai">
								<?php foreach ($bulan as $key => $bln) {
									echo '<option value="'.$key.'">'.$bln.'</option>';
								}?>
							</select>
							<select name="tahun_selesai[]" class="thn_selesai">
								<?php for ($thn=date('Y'); $thn >=date('Y')-25 ; $thn--) { 
									echo '<option value="'.$thn.'">'.$thn.'</option>';
								}

								?>
							</select>
						</td>
						<td><input type="text" name="posisi[]" class="posisi"></td>
						<td><input type="text" name="deskripsi_jabatan[]" class="deskripsi_jabatan"></td>
						<td><a href="#tabel_jabatan"><i class="fa fa-times fa-2x hapus_jabatan"></i></a></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<span id="tambah_jabatan" class="btn btn-sm btn-yellow">Tambah Jabatan</span>
		</div>
	</div>

	<?php if(group_has_role('human_resource','set_complete_personel')) { ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="is_complete">Is Complete?</label>

		<div class="col-sm-5">
			<?php 
				$value = $this->config->item('default_is_complete');
				if($this->input->post('is_complete') != NULL){
					$value = $this->input->post('is_complete');
				}elseif($mode == 'edit'){
					$value = $fields['is_complete'];
				}
			?>
			
			<label>
				<input name="is_complete" class="ace ace-switch ace-switch-5" value="1" <?php echo ($value=='1') ? 'checked' : ''; ?> type="checkbox">
				<span class="lbl"></span>
			</label>
		</div>
	</div>
	<?php } ?>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<!-- Script -->
<script>
	// ----------------------------------------
	// Datepicker
	// ----------------------------------------
	$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	// ----------------------------------------
	// Datepicker
	// ----------------------------------------
	$(document).ready(function() {
		$('.select-ajax').each(function() {
			var $select = $(this);
			
			$.getJSON($(this).data('source'), function(result){
				var group = '';
		        $.each(result, function(i, val){
		        	if(val.group != undefined) {
		        		if(val.group!=group) {
			        		$select.append('<optgroup label="'+val.group+'"></optgroup>');
			        		group = val.group;
			        	}
						
						if(val.group==group || i==0) {
							// console.log(group);	        		
			        		$select.find('optgroup[label="'+group+'"]').append('<option value="'+val.id+'">'+val.text+'</option>');
			        	}
		        	} else {
			        	$select.append('<option value="'+val.id+'">'+val.text+'</option>');
			        }
		        });

		        if(result.length!=0) {
		        	$select.val($select.data('default')).trigger("change");
	        	}
		    });
		
			
			$(this).select2({
				allowClear : true,
				placeholder : '- Pilih Salah Satu-',
			});
		});

		$('#reload_user').click(function(e) {
			e.preventDefault();

			var $select = $('#select_user');
			$select.html("<option>- Sedang Mengambil Data -</option>");

			$.getJSON($select.data('source'), function(result){
				$select.empty();
				var group = '';
		        $.each(result, function(i, val){
		        	if(val.group != undefined) {
		        		if(val.group!=group) {
			        		$select.append('<optgroup label="'+val.group+'"></optgroup>');
			        		group = val.group;
			        	}
						
						if(val.group==group || i==0) {
							// console.log(group);	        		
			        		$select.find('optgroup[label="'+group+'"]').append('<option value="'+val.id+'">'+val.text+'</option>');
			        	}
		        	} else {
			        	$select.append('<option value="'+val.id+'">'+val.text+'</option>');
			        }
		        });

		        if(result.length!=0) {
		        	if($select.data('default')!='') {
	        			$select.val($select.data('default')).trigger("change");
	        		}
	        	}
		    });
			
			$select.select2({
				allowClear : true,
				placeholder : '- Pilih Salah Satu-',
			});
		});

		$('#provinsi_kantor,#provinsi_rumah').change(function() {
			var id_provinsi = $(this).val();
			// check whether its home or office
			var alamat = $(this).attr('id').substr(9);

			set_kota(id_provinsi,alamat);
		});

		$('#kota_kantor,#kota_rumah').change(function() {
			var id_kota = $(this).val();
			// check whether its home or office
			var alamat = $(this).attr('id').substr(5);

			set_kecamatan(id_kota,alamat);
		});

		$('#kecamatan_kantor,#kecamatan_rumah').change(function() {
			var id_kecamatan = $(this).val();
			// check whether its home or office
			var alamat = $(this).attr('id').substr(10);

			set_kelurahan(id_kecamatan,alamat);
		});

		$('#tambah_jabatan').click(function() {
			var tr = $('#tabel_jabatan tbody tr:first').html();
			$('#tabel_jabatan tbody').append('<tr>'+tr+'</tr>');
		});

		$('#tabel_jabatan').on('click','.hapus_jabatan', function() {
			if($('#tabel_jabatan tbody tr').length>1) {
				$(this).closest('tr').remove();
			}
		});

		$('#select_user').change(function() {
			var id = $(this).val();
			if(id) {
				$.getJSON(BASE_URL+'admin/human_resource/personel/get_user_account/'+id, function(result) {
					$('#nama_lengkap').val(result.display_name);
					$('#nama_lengkap').attr('readonly',true);
					$('#email').val(result.email);
					$('#email').attr('readonly',true);
					$('#id_organization_unit').val(result.organization).trigger("change");
					$('#id_organization_unit').attr('disabled',true);
					// $('#id_organization_unit').after('<input id="hidden_organization" name="id_organization_unit" type="hidden" value="'+result.organization+'">');
				});
			} else {
				$('#nama_lengkap').val('');
				$('#nama_lengkap').removeAttr('readonly');
				$('#email').val('');
				$('#email').removeAttr('readonly');
				$('#id_organization_unit').val('').trigger("change");
				$('#id_organization_unit').removeAttr('disabled');
				$('#hidden_organization').remove();
			}
		});

		$('#form-personel').on('submit',function(e) {
			$('#id_organization_unit').removeAttr('disabled');
			$('#select_user').removeAttr('disabled');
		});

		function set_kota(id_provinsi,type) {
			$('#kota_'+type+' option:first').html('-- Sedang Mengambil Data --');
			$.getJSON(BASE_URL+'admin/human_resource/personel/get_kota_by_provinsi/'+id_provinsi, function(result) {
				var choose = '-- Pilih Kota --';
				if(result.length==0) {
					choose = '-- Tidak Ada Kota --'
				}
				$('#kota_'+type).html('<option value="">'+choose+'</option>');
				$.each(result, function(key,value) {
					$('#kota_'+type).append('<option value="'+value.id+'">'+value.nama+'</option>');
				});

				if($('#kota_'+type).data('value')!='') {
					$('#kota_'+type).val($('#kota_'+type).data('value'));
				}
				$('#kota_'+type).trigger('change');
			});
		}

		function set_kecamatan(id_kota, type) {
			$('#kecamatan_'+type+' option:first').html('-- Sedang Mengambil Data --');
			$.getJSON(BASE_URL+'admin/human_resource/personel/get_kecamatan_by_kota/'+id_kota, function(result) {
				var choose = '-- Pilih Kecamatan --';
				if(result.length==0) {
					choose = '-- Tidak Ada Kecamatan --'
				}
				$('#kecamatan_'+type).html('<option value="">'+choose+'</option>');
				$.each(result, function(key,value) {
					$('#kecamatan_'+type).append('<option value="'+value.id+'">'+value.nama+'</option>');
				});

				if($('#kecamatan_'+type).data('value')!='') {
					$('#kecamatan_'+type).val($('#kecamatan_'+type).data('value'));
				}
				$('#kecamatan_'+type).trigger('change');
			});
		}

		function set_kelurahan(id_kecamatan, type) {
			$('#kelurahan_'+type+' option:first').html('-- Sedang Mengambil Data --');
			$.getJSON(BASE_URL+'admin/human_resource/personel/get_kelurahan_by_kecamatan/'+id_kecamatan, function(result) {
				var choose = '-- Pilih Kelurahan --';
				if(result.length==0) {
					choose = '-- Tidak Ada Kelurahan --'
				}
				$('#kelurahan_'+type).html('<option value="">'+choose+'</option>');
				$.each(result, function(key,value) {
					$('#kelurahan_'+type).append('<option value="'+value.id+'">'+value.nama+'</option>');
				});

				if($('#kelurahan_'+type).data('value')!='') {
					$('#kelurahan_'+type).val($('#kelurahan_'+type).data('value'));
				}
				$('#kelurahan_'+type).trigger('change');
			});
		}

		$('#provinsi_kantor').trigger('change');
		$('#provinsi_rumah').trigger('change');
	});
</script>