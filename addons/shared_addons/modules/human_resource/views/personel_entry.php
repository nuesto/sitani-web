<div class="page-header">
	<h1>
		<span><?php echo lang('human_resource:personel:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('human_resource/personel/index'); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('human_resource:back') ?>
		</a>

		<?php if(group_has_role('human_resource', 'edit_all_personel')){ ?>
			<a href="<?php echo site_url('human_resource/personel/edit/'.$personel['id']); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'edit_own_personel')){ ?>
			<?php if($personel->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('human_resource/personel/edit/'.$personel['id']); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('human_resource', 'delete_all_personel')){ ?>
			<a href="<?php echo site_url('human_resource/personel/delete/'.$personel['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'delete_own_personel')){ ?>
			<?php if($personel->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('human_resource/personel/delete/'.$personel['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $personel['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:nama_lengkap'); ?></div>
		<?php if(isset($personel['nama_lengkap'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['nama_lengkap']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:jenis_kelamin'); ?></div>
		<?php if(isset($personel['jenis_kelamin'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['jenis_kelamin']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:nomor_induk'); ?></div>
		<?php if(isset($personel['nomor_induk'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['nomor_induk']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:tanggal_lahir'); ?></div>
		<?php if(isset($personel['tanggal_lahir'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['tanggal_lahir']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_status_pekerja'); ?></div>
		<?php if(isset($personel['id_status_pekerja'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['id_status_pekerja']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_level'); ?></div>
		<?php if(isset($personel['id_level'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['id_level']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:tahun_mulai_bekerja'); ?></div>
		<?php if(isset($personel['tahun_mulai_bekerja'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['tahun_mulai_bekerja']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:tahun_mulai_profesional'); ?></div>
		<?php if(isset($personel['tahun_mulai_profesional'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['tahun_mulai_profesional']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_metode_rekrutmen'); ?></div>
		<?php if(isset($personel['id_metode_rekrutmen'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['id_metode_rekrutmen']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_supervisor'); ?></div>
		<?php if(isset($personel['id_supervisor'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['id_supervisor']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:handphone'); ?></div>
		<?php if(isset($personel['handphone'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['handphone']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:email'); ?></div>
		<?php if(isset($personel['email'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['email']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_organization_unit'); ?></div>
		<?php if(isset($personel['id_organization_unit'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['id_organization_unit']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_user'); ?></div>
		<?php if(isset($personel['id_user'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $personel['id_user']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:created'); ?></div>
		<?php if(isset($personel['created'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($personel['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:updated'); ?></div>
		<?php if(isset($personel['updated'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($personel['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($personel['created_by'], true); ?></div>
	</div>
</div>