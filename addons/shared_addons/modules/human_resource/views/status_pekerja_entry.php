<div class="page-header">
	<h1>
		<span><?php echo lang('human_resource:status_pekerja:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('human_resource/status_pekerja/index'); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('human_resource:back') ?>
		</a>

		<?php if(group_has_role('human_resource', 'edit_all_status_pekerja')){ ?>
			<a href="<?php echo site_url('human_resource/status_pekerja/edit/'.$status_pekerja['id']); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'edit_own_status_pekerja')){ ?>
			<?php if($status_pekerja->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('human_resource/status_pekerja/edit/'.$status_pekerja['id']); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('human_resource', 'delete_all_status_pekerja')){ ?>
			<a href="<?php echo site_url('human_resource/status_pekerja/delete/'.$status_pekerja['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'delete_own_status_pekerja')){ ?>
			<?php if($status_pekerja->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('human_resource/status_pekerja/delete/'.$status_pekerja['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $status_pekerja['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:nama_status'); ?></div>
		<?php if(isset($status_pekerja['nama_status'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $status_pekerja['nama_status']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:ordering_count'); ?></div>
		<?php if(isset($status_pekerja['ordering_count'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $status_pekerja['ordering_count']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:created'); ?></div>
		<?php if(isset($status_pekerja['created'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($status_pekerja['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:updated'); ?></div>
		<?php if(isset($status_pekerja['updated'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($status_pekerja['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($status_pekerja['created_by'], true); ?></div>
	</div>
</div>