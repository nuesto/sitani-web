<div class="page-header">	<h1>		<span><?php echo lang('human_resource:personel:plural'); ?></span>	</h1>		<?php if(group_has_role('human_resource', 'create_personel')){ ?>	<div class="btn-group content-toolbar">		<a class="btn btn-default btn-sm" href="<?php echo site_url('human_resource/personel/create'); ?>">			<i class="icon-plus"></i>			<span class="no-text-shadow"><?php echo lang('human_resource:personel:new'); ?></span>		</a>	</div>	<?php } ?></div><fieldset>	<legend><?php echo lang('global:filters') ?></legend>		<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>		<div class="form-group">
			<label><?php echo lang('human_resource:nama_lengkap'); ?>:&nbsp;</label>
			<input type="text" name="f-nama_lengkap" value="<?php echo $this->input->get('f-nama_lengkap'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:jenis_kelamin'); ?>:&nbsp;</label>
			<input type="text" name="f-jenis_kelamin" value="<?php echo $this->input->get('f-jenis_kelamin'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:nomor_induk'); ?>:&nbsp;</label>
			<input type="text" name="f-nomor_induk" value="<?php echo $this->input->get('f-nomor_induk'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:tanggal_lahir'); ?>:&nbsp;</label>
			<input type="text" name="f-tanggal_lahir" value="<?php echo $this->input->get('f-tanggal_lahir'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:id_status_pekerja'); ?>:&nbsp;</label>
			<input type="text" name="f-id_status_pekerja" value="<?php echo $this->input->get('f-id_status_pekerja'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:id_level'); ?>:&nbsp;</label>
			<input type="text" name="f-id_level" value="<?php echo $this->input->get('f-id_level'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:tahun_mulai_bekerja'); ?>:&nbsp;</label>
			<input type="text" name="f-tahun_mulai_bekerja" value="<?php echo $this->input->get('f-tahun_mulai_bekerja'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:tahun_mulai_profesional'); ?>:&nbsp;</label>
			<input type="text" name="f-tahun_mulai_profesional" value="<?php echo $this->input->get('f-tahun_mulai_profesional'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:id_metode_rekrutmen'); ?>:&nbsp;</label>
			<input type="text" name="f-id_metode_rekrutmen" value="<?php echo $this->input->get('f-id_metode_rekrutmen'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:id_supervisor'); ?>:&nbsp;</label>
			<input type="text" name="f-id_supervisor" value="<?php echo $this->input->get('f-id_supervisor'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:handphone'); ?>:&nbsp;</label>
			<input type="text" name="f-handphone" value="<?php echo $this->input->get('f-handphone'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:email'); ?>:&nbsp;</label>
			<input type="text" name="f-email" value="<?php echo $this->input->get('f-email'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:id_organization_unit'); ?>:&nbsp;</label>
			<input type="text" name="f-id_organization_unit" value="<?php echo $this->input->get('f-id_organization_unit'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('human_resource:id_user'); ?>:&nbsp;</label>
			<input type="text" name="f-id_user" value="<?php echo $this->input->get('f-id_user'); ?>">
		</div>
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">			<i class="icon-ok"></i>			<?php echo lang('buttons:submit'); ?>		</button>				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-danger btn-xs" type="reset">			<i class="icon-remove"></i>			<?php echo lang('buttons:clear'); ?>		</button>	<?php echo form_close() ?></fieldset><hr /><?php if ($personel['total'] > 0): ?>		<p class="pull-right"><?php echo lang('human_resource:showing').' '.count($personel['entries']).' '.lang('human_resource:of').' '.$personel['total'] ?></p>		<table class="table table-striped table-bordered table-hover">		<thead>			<tr>				<th>No</th>				<th><?php echo lang('human_resource:nama_lengkap'); ?></th>				<th><?php echo lang('human_resource:jenis_kelamin'); ?></th>				<th><?php echo lang('human_resource:nomor_induk'); ?></th>				<th><?php echo lang('human_resource:tanggal_lahir'); ?></th>				<th><?php echo lang('human_resource:id_status_pekerja'); ?></th>				<th><?php echo lang('human_resource:id_level'); ?></th>				<th><?php echo lang('human_resource:tahun_mulai_bekerja'); ?></th>				<th><?php echo lang('human_resource:tahun_mulai_profesional'); ?></th>				<th><?php echo lang('human_resource:id_metode_rekrutmen'); ?></th>				<th><?php echo lang('human_resource:id_supervisor'); ?></th>				<th><?php echo lang('human_resource:handphone'); ?></th>				<th><?php echo lang('human_resource:email'); ?></th>				<th><?php echo lang('human_resource:id_organization_unit'); ?></th>				<th><?php echo lang('human_resource:id_user'); ?></th>				<th><?php echo lang('human_resource:created'); ?></th>				<th><?php echo lang('human_resource:updated'); ?></th>				<th><?php echo lang('human_resource:created_by'); ?></th>				<th></th>			</tr>		</thead>		<tbody>			<?php 			$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);			if($cur_page != 0){				$item_per_page = $pagination_config['per_page'];				$no = (($cur_page -1) * $item_per_page) + 1;			}else{				$no = 1;			}			?>						<?php foreach ($personel['entries'] as $personel_entry): ?>			<tr>				<td><?php echo $no; $no++; ?></td>				<td><?php echo $personel_entry['nama_lengkap']; ?></td>				<td><?php echo $personel_entry['jenis_kelamin']; ?></td>				<td><?php echo $personel_entry['nomor_induk']; ?></td>				<td><?php echo $personel_entry['tanggal_lahir']; ?></td>				<td><?php echo $personel_entry['id_status_pekerja']; ?></td>				<td><?php echo $personel_entry['id_level']; ?></td>				<td><?php echo $personel_entry['tahun_mulai_bekerja']; ?></td>				<td><?php echo $personel_entry['tahun_mulai_profesional']; ?></td>				<td><?php echo $personel_entry['id_metode_rekrutmen']; ?></td>				<td><?php echo $personel_entry['id_supervisor']; ?></td>				<td><?php echo $personel_entry['handphone']; ?></td>				<td><?php echo $personel_entry['email']; ?></td>				<td><?php echo $personel_entry['id_organization_unit']; ?></td>				<td><?php echo $personel_entry['id_user']; ?></td>							<?php if($personel_entry['created']){ ?>				<td><?php echo format_date($personel_entry['created'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<?php if($personel_entry['updated']){ ?>				<td><?php echo format_date($personel_entry['updated'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<td><?php echo user_displayname($personel_entry['created_by'], true); ?></td>				<td class="actions">				<?php 				if(group_has_role('human_resource', 'view_all_personel')){					echo anchor('human_resource/personel/view/' . $personel_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');				}elseif(group_has_role('human_resource', 'view_own_personel')){					if($personel_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('human_resource/personel/view/' . $personel_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');					}				}				?>				<?php 				if(group_has_role('human_resource', 'edit_all_personel')){					echo anchor('human_resource/personel/edit/' . $personel_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');				}elseif(group_has_role('human_resource', 'edit_own_personel')){					if($personel_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('human_resource/personel/edit/' . $personel_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');					}				}				?>				<?php 				if(group_has_role('human_resource', 'delete_all_personel')){					echo anchor('human_resource/personel/delete/' . $personel_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));				}elseif(group_has_role('human_resource', 'delete_own_personel')){					if($personel_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('human_resource/personel/delete/' . $personel_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));					}				}				?>				</td>			</tr>			<?php endforeach; ?>		</tbody>	</table>		<?php echo $personel['pagination']; ?>	<?php else: ?>	<div class="well"><?php echo lang('human_resource:personel:no_entry'); ?></div><?php endif;?>