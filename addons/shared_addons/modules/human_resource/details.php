<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Human_resource extends Module
{
    public $version = '1.3.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Human Resource',
			'id' => 'Human Resource',
		);
		$info['description'] = array(
			'en' => 'Manage human resource',
			'id' => 'Manage human resource',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		// $info['menu'] = 'Human Resource';
		$info['roles'] = array(
			'access_personel_backend',
			'view_all_personel',
			'view_own_unit_personel', //new
			'view_own_personel',
			'edit_all_personel',
			'edit_own_unit_personel', //new
			'edit_own_personel',
			'delete_all_personel',
			'delete_own_unit_personel', //new
			'delete_own_personel',
			'create_personel',
			'access_status_pekerja_backend',
			'view_all_status_pekerja',
			'edit_all_status_pekerja',
			'delete_all_status_pekerja',
			'create_status_pekerja',
			'access_level_backend',
			'view_all_level',
			'edit_all_level',
			'delete_all_level',
			'create_level',
			'access_metode_rekrutmen_backend',
			'view_all_metode_rekrutmen',
			'edit_all_metode_rekrutmen',
			'delete_all_metode_rekrutmen',
			'create_metode_rekrutmen',
			'create_own_user_personel',
			'view_own_user_personel',
			'edit_own_user_personel',
			'set_complete_personel'
		);

		if(group_has_role('human_resource', 'access_personel_backend')){
			$info['sections']['personel']['name'] = 'human_resource:personel:plural';
			$info['sections']['personel']['uri'] = 'admin/human_resource/personel/index';

			if(group_has_role('human_resource', 'create_personel')){
				$info['sections']['personel']['shortcuts']['create'] = array(
					'name' => 'human_resource:personel:new',
					'uri' => 'admin/human_resource/personel/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('human_resource', 'access_status_pekerja_backend')){
			$info['sections']['status_pekerja']['name'] = 'human_resource:status_pekerja:plural';
			$info['sections']['status_pekerja']['uri'] = 'admin/human_resource/status_pekerja/index';

			if(group_has_role('human_resource', 'create_status_pekerja')){
				$info['sections']['status_pekerja']['shortcuts']['create'] = array(
					'name' => 'human_resource:status_pekerja:new',
					'uri' => 'admin/human_resource/status_pekerja/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('human_resource', 'access_level_backend')){
			$info['sections']['level']['name'] = 'human_resource:level:plural';
			$info['sections']['level']['uri'] = 'admin/human_resource/level/index';

			if(group_has_role('human_resource', 'create_level')){
				$info['sections']['level']['shortcuts']['create'] = array(
					'name' => 'human_resource:level:new',
					'uri' => 'admin/human_resource/level/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('human_resource', 'access_metode_rekrutmen_backend')){
			$info['sections']['metode_rekrutmen']['name'] = 'human_resource:metode_rekrutmen:plural';
			$info['sections']['metode_rekrutmen']['uri'] = 'admin/human_resource/metode_rekrutmen/index';

			if(group_has_role('human_resource', 'create_metode_rekrutmen')){
				$info['sections']['metode_rekrutmen']['shortcuts']['create'] = array(
					'name' => 'human_resource:metode_rekrutmen:new',
					'uri' => 'admin/human_resource/metode_rekrutmen/create',
					'class' => 'add'
				);
			}
		}

		return $info;
    }

	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items, &$menu_order){
		unset($menu_items['lang:cp:nav_0']);
		if(group_has_role('human_resource','view_all_personel') OR group_has_role('human_resource', 'view_own_unit_personel') OR group_has_role('human_resource', 'view_own_personel') OR group_has_role('human_resource', 'view_own_user_personel') OR group_has_role('human_resource', 'create_personel') OR group_has_role('human_resource', 'create_own_user_personel')) {
			$menu_items['lang:cp:nav_Human_Resource']['lang:human_resource:personel:employee']['urls'] = array('admin/human_resource/personel/index',
				'admin/human_resource/personel/create',
				'admin/human_resource/personel/create%1',
				'admin/human_resource/personel/view%1',
				'admin/human_resource/personel/edit%1',
				'admin/human_resource/personel/edit%2',
				'admin/human_resource/personel/personel_history%1',
				'admin/human_resource/personel/import_personel');
		}

		if(group_has_role('human_resource','view_all_status_pekerja')) {
			$menu_items['lang:cp:nav_Human_Resource']['lang:human_resource:status_pekerja:plural']['urls'] = array('admin/human_resource/status_pekerja/index',
				'admin/human_resource/status_pekerja/create',
				'admin/human_resource/status_pekerja/view%1',
				'admin/human_resource/status_pekerja/edit%1');
		}

		if(group_has_role('human_resource','view_all_level')) {
			$menu_items['lang:cp:nav_Human_Resource']['lang:human_resource:level:plural']['urls'] = array('admin/human_resource/level/index',
				'admin/human_resource/level/create',
				'admin/human_resource/level/view%1',
				'admin/human_resource/level/edit%1');
		}

		if(group_has_role('human_resource','view_all_metode_rekrutmen')) {
			$menu_items['lang:cp:nav_Human_Resource']['lang:human_resource:metode_rekrutmen:plural']['urls'] = array('admin/human_resource/metode_rekrutmen/index',
				'admin/human_resource/metode_rekrutmen/create',
				'admin/human_resource/metode_rekrutmen/view%1',
				'admin/human_resource/metode_rekrutmen/edit%1');
		}
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// status_pekerja
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama_status' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'default' => '',
				'null' => FALSE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('human_resource_status_pekerja', TRUE);


		// level
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama_level' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'default' => '',
				'null' => FALSE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => FALSE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('human_resource_level', TRUE);


		// metode_rekrutmen
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama_metode' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'default' => '',
				'null' => FALSE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('human_resource_metode_rekrutmen', TRUE);

		// personel
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama_lengkap' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'default' => '',
				'null' => FALSE,
			),
			'jenis_kelamin' => array(
				'type' => 'CHAR',
				'constraint' => '1',
				'default' => '',
				'null' => FALSE,
			),
			'nomor_induk' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'default' => '',
				'null' => TRUE,
			),
			'tanggal_lahir' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'id_status_pekerja' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'id_level' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'tahun_mulai_bekerja' => array(
				'type' => 'SMALLINT',
				'null' => TRUE,
			),
			'tahun_mulai_profesional' => array(
				'type' => 'SMALLINT',
				'null' => TRUE,
			),
			'id_metode_rekrutmen' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'id_supervisor' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'handphone' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'default' => '',
				'null' => TRUE,
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'id_organization_unit' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'id_user' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'address_home' => array(
				'type' => 'TEXT',
				'null' => TRUE
				),
			'id_location_kelurahan_home' => array(
				'type' => 'INT',
				'null' => TRUE
				),
			'postal_code_home' => array(
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => TRUE
				),
			'address_office' => array(
				'type' => 'TEXT',
				'null' => TRUE
				),
			'id_location_kelurahan_office' => array(
				'type' => 'INT',
				'null' => TRUE
				),
			'postal_code_office' => array(
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => TRUE
				),
			'phone_home' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE
				),
			'phone_office' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE
				),
			'is_complete' => array(
						'type' => 'TINYINT',
						'null' => TRUE
						),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('human_resource_personel', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_human_resource_personel(created_by)");


		// personel_history
		$fields = array(
			'id_personel' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'default' => '',
				'null' => FALSE,
			),
			'description' => array(
				'type' => 'TEXT',
				'null' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
				'null' => FALSE,
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => FALSE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->create_table('human_resource_personel_history', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_human_resource_personel_history(created_by)");

		// jabatan
		$fields = array(
			'id_personel' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'bulan_mulai' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'tahun_mulai' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'bulan_selesai' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'tahun_selesai' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'posisi' => array(
				'type' => 'VARCHAR',
				'constraint' => 200,
				'null' => FALSE,
			),
			'deskripsi' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->create_table('human_resource_jabatan', TRUE);

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('human_resource_personel');
        $this->dbforge->drop_table('human_resource_personel_history');
        $this->dbforge->drop_table('human_resource_status_pekerja');
        $this->dbforge->drop_table('human_resource_level');
        $this->dbforge->drop_table('human_resource_metode_rekrutmen');
        $this->dbforge->drop_table('human_resource_jabatan');

        return true;
    }

    public function upgrade($old_version)
    {
    	switch ($old_version) {
    		case '1.0':
                // add new fields to human_resource_personel table
    			$personel_new_field = array(
					'address_home' => array(
						'type' => 'TEXT',
						'null' => TRUE
						),
					'id_location_kelurahan_home' => array(
						'type' => 'INT',
						'null' => TRUE
						),
					'postal_code_home' => array(
						'type' => 'VARCHAR',
						'constraint' => 10,
						'null' => TRUE
						),
					'address_office' => array(
						'type' => 'TEXT',
						'null' => TRUE
						),
					'id_location_kelurahan_office' => array(
						'type' => 'INT',
						'null' => TRUE
						),
					'postal_code_office' => array(
						'type' => 'VARCHAR',
						'constraint' => 10,
						'null' => TRUE
						),
					'phone_home' => array(
						'type' => 'VARCHAR',
						'constraint' => 50,
						'null' => TRUE
						),
					'phone_office' => array(
						'type' => 'VARCHAR',
						'constraint' => 50,
						'null' => TRUE
						),
    				);

    			$this->dbforge->add_column('human_resource_personel',$personel_new_field);

				// jabatan
				$fields = array(
					'id_personel' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
					),
					'bulan_mulai' => array(
						'type' => 'INT',
						'null' => TRUE,
					),
					'tahun_mulai' => array(
						'type' => 'INT',
						'null' => TRUE,
					),
					'bulan_selesai' => array(
						'type' => 'INT',
						'null' => TRUE,
					),
					'tahun_selesai' => array(
						'type' => 'INT',
						'null' => TRUE,
					),
					'posisi' => array(
						'type' => 'VARCHAR',
						'constraint' => 200,
						'null' => FALSE,
					),
					'deskripsi' => array(
						'type' => 'TEXT',
						'null' => TRUE,
					),
				);
				$this->dbforge->add_field($fields);
				$this->dbforge->create_table('human_resource_jabatan', TRUE);

            case '1.1':
                // add new field to human_resource_personel table
                $personel_new_field = array(
					'is_complete' => array(
						'type' => 'TINYINT',
						'null' => TRUE
						),
    				);

    			$this->dbforge->add_column('human_resource_personel',$personel_new_field);

            case '1.2':
                // convert all tables to InnoDB engine
                $this->db->query("ALTER TABLE `default_human_resource_personel` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_human_resource_personel_history` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_human_resource_status_pekerja` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_human_resource_level` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_human_resource_metode_rekrutmen` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_human_resource_jabatan` ENGINE=InnoDB");
                
    	}
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}
