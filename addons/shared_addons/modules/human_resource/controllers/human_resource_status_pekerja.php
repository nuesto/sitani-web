<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Human_resource_status_pekerja extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'status_pekerja';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('human_resource');
		
		$this->load->model('status_pekerja_m');
    }

    /**
	 * List all status_pekerja
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_status_pekerja') AND ! group_has_role('human_resource', 'view_own_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['status_pekerja']['entries'] = $this->status_pekerja_m->get_status_pekerja();
		$data['status_pekerja']['total'] = count($data['status_pekerja']['entries']);

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('human_resource:status_pekerja:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('human_resource:status_pekerja:plural'))
			->build('status_pekerja_index', $data);
    }
	
	/**
     * Display one status_pekerja
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_status_pekerja') AND ! group_has_role('human_resource', 'view_own_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['status_pekerja'] = $this->status_pekerja_m->get_status_pekerja_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('human_resource', 'view_all_status_pekerja')){
			if($data['status_pekerja']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:status_pekerja:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:status_pekerja:plural'), '/human_resource/status_pekerja/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:view'))
			->build('status_pekerja_entry', $data);
    }
	
	/**
     * Create a new status_pekerja entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'create_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_status_pekerja('new')){	
				$this->session->set_flashdata('success', lang('human_resource:status_pekerja:submit_success'));				
				redirect('human_resource/status_pekerja/index');
			}else{
				$data['messages']['error'] = lang('human_resource:status_pekerja:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'human_resource/status_pekerja/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:status_pekerja:new'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:status_pekerja:plural'), '/human_resource/status_pekerja/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:new'))
			->build('status_pekerja_form', $data);
    }
	
	/**
     * Edit a status_pekerja entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the status_pekerja to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'edit_all_status_pekerja') AND ! group_has_role('human_resource', 'edit_own_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('human_resource', 'edit_all_status_pekerja')){
			$entry = $this->status_pekerja_m->get_status_pekerja_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_status_pekerja('edit', $id)){	
				$this->session->set_flashdata('success', lang('human_resource:status_pekerja:submit_success'));				
				redirect('human_resource/status_pekerja/index');
			}else{
				$data['messages']['error'] = lang('human_resource:status_pekerja:submit_failure');
			}
		}
		
		$data['fields'] = $this->status_pekerja_m->get_status_pekerja_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'human_resource/status_pekerja/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
        $this->template->title(lang('human_resource:status_pekerja:edit'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:status_pekerja:plural'), '/human_resource/status_pekerja/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:view'), '/human_resource/status_pekerja/view/'.$id)
			->set_breadcrumb(lang('human_resource:status_pekerja:edit'))
			->build('status_pekerja_form', $data);
    }
	
	/**
     * Delete a status_pekerja entry
     * 
     * @param   int [$id] The id of status_pekerja to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'delete_all_status_pekerja') AND ! group_has_role('human_resource', 'delete_own_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('human_resource', 'delete_all_status_pekerja')){
			$entry = $this->status_pekerja_m->get_status_pekerja_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->status_pekerja_m->delete_status_pekerja_by_id($id);
		$this->session->set_flashdata('error', lang('human_resource:status_pekerja:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('human_resource/status_pekerja/index');
    }
	
	/**
     * Insert or update status_pekerja entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_status_pekerja($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('human_resource:status_pekerja:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->status_pekerja_m->insert_status_pekerja($values);
			}
			else
			{
				$result = $this->status_pekerja_m->update_status_pekerja($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}