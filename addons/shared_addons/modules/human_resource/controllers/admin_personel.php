<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Admin_personel extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'personel';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'access_personel_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('human_resource');		
		$this->load->model('personel_m');
		$this->load->model('status_pekerja_m');
		$this->load->model('level_m');
		$this->load->model('metode_rekrutmen_m');
		$this->load->library('organization/Organization');
		$this->load->helper('human_resource');

		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
		$this->load->model('location/kecamatan_m');
		$this->load->model('location/kelurahan_m');
		$this->load->language('location/location');

		$this->config->load('human_resource/personel');
    }

    /**
	 * List all personel
     *
     * @return	void
     */
    public function index($download = '')
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_personel') AND ! group_has_role('human_resource', 'view_own_unit_personel') AND ! group_has_role('human_resource', 'view_own_personel') AND ! group_has_role('human_resource', 'view_own_user_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		if(! group_has_role('human_resource', 'view_all_personel') AND ! group_has_role('human_resource', 'view_own_unit_personel') AND ! group_has_role('human_resource', 'view_own_personel') AND group_has_role('human_resource', 'view_own_user_personel')){
			$personel_entry = $this->personel_m->get_personel_by_account($this->current_user->id);
			redirect('admin/human_resource/personel/view/'.$personel_entry['id']);
		}

		// Filter
		$filter = NULL;
		$where_arr = array();
		$data['filter_value'] = array();
		$index_filter = 0;
		
		$select_filters = $this->input->get('f-field');
		
		if($this->input->get('f-nama_lengkap')) {
			$filter .= '(';
			foreach ($this->input->get('f-nama_lengkap') as $key => $f) {
				$filter .= 'default_human_resource_personel.nama_lengkap LIKE "%'.$f.'%"';

				if($key+1<count($this->input->get('f-nama_lengkap'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['nama_lengkap'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-jenis_kelamin')) {
			$filter .= '(';
			foreach ($this->input->get('f-jenis_kelamin') as $key => $f) {
				$filter .= 'default_human_resource_personel.jenis_kelamin LIKE "%'.$f.'%"';

				if($key+1<count($this->input->get('f-jenis_kelamin'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['jenis_kelamin'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-tanggal_lahir')) {
			$key_for_form = array_keys($select_filters,"tanggal_lahir");

			$filter .= '(';
			foreach ($this->input->get('f-tanggal_lahir') as $key => $f) {
				$filter .= 'default_human_resource_personel.tanggal_lahir LIKE "%'.$f.'%"';

				if($key+1<count($this->input->get('f-tanggal_lahir'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['tanggal_lahir'][$key_for_form[$key]] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-nomor_induk')) {
			$filter .= '(';
			foreach ($this->input->get('f-nomor_induk') as $key => $f) {
				$filter .= 'default_human_resource_personel.nomor_induk LIKE "%'.$f.'%"';

				if($key+1<count($this->input->get('f-nomor_induk'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['nomor_induk'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-id_status_pekerja')) {
			$filter .= '(';
			foreach ($this->input->get('f-id_status_pekerja') as $key => $f) {
				$filter .= 'default_human_resource_personel.id_status_pekerja = "'.$f.'"';

				if($key+1<count($this->input->get('f-id_status_pekerja'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['id_status_pekerja'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-id_level')) {
			$filter .= '(';
			foreach ($this->input->get('f-id_level') as $key => $f) {
				$filter .= 'default_human_resource_personel.id_level = "'.$f.'"';

				if($key+1<count($this->input->get('f-id_level'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['id_level'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-tahun_mulai_bekerja')) {
			$filter .= '(';
			foreach ($this->input->get('f-tahun_mulai_bekerja') as $key => $f) {
				$filter .= 'default_human_resource_personel.tahun_mulai_bekerja = "'.$f.'"';

				if($key+1<count($this->input->get('f-tahun_mulai_bekerja'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['tahun_mulai_bekerja'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-tahun_mulai_profesional')) {
			$filter .= '(';
			foreach ($this->input->get('f-tahun_mulai_profesional') as $key => $f) {
				$filter .= 'default_human_resource_personel.tahun_mulai_profesional = "'.$f.'"';

				if($key+1<count($this->input->get('f-tahun_mulai_profesional'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['tahun_mulai_profesional'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-id_metode_rekrutmen')) {
			$filter .= '(';
			foreach ($this->input->get('f-id_metode_rekrutmen') as $key => $f) {
				$filter .= 'default_human_resource_personel.id_metode_rekrutmen = "'.$f.'"';

				if($key+1<count($this->input->get('f-id_metode_rekrutmen'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['id_metode_rekrutmen'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-id_supervisor')) {
			$filter .= '(';
			foreach ($this->input->get('f-id_supervisor') as $key => $f) {
				$filter .= 'default_human_resource_personel.id_supervisor = "'.$f.'"';

				if($key+1<count($this->input->get('f-id_supervisor'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['id_supervisor'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-level_supervisor')) {
			$filter .= '(';
			foreach ($this->input->get('f-level_supervisor') as $key => $f) {
				$filter .= 'supervisor.id_level = "'.$f.'"';

				if($key+1<count($this->input->get('f-level_supervisor'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['level_supervisor'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-handphone')) {
			$filter .= '(';
			foreach ($this->input->get('f-handphone') as $key => $f) {
				$filter .= 'default_human_resource_personel.handphone LIKE "%'.$f.'%"';

				if($key+1<count($this->input->get('f-handphone'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['handphone'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-email')) {
			$filter .= '(';
			foreach ($this->input->get('f-email') as $key => $f) {
				$filter .= 'default_human_resource_personel.email LIKE "%'.$f.'%"';

				if($key+1<count($this->input->get('f-email'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['email'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-unit_name')) {
			$filter .= '(';
			foreach ($this->input->get('f-unit_name') as $key => $f) {
				$filter .= 'default_human_resource_personel.id_organization_unit = "'.$f.'"';

				if($key+1<count($this->input->get('f-unit_name'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['unit_name'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-created_on')) {
			$filter .= '(';
			foreach ($this->input->get('f-created_on') as $key => $f) {
				$filter .= 'default_human_resource_personel.created_on = "'.$f.'"';

				if($key+1<count($this->input->get('f-created_on'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['created_on'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-updated_on')) {
			$filter .= '(';
			foreach ($this->input->get('f-updated_on') as $key => $f) {
				$filter .= 'default_human_resource_personel.updated_on = "'.$f.'"';

				if($key+1<count($this->input->get('f-updated_on'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['updated_on'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		if($this->input->get('f-jabatan')) {
			$filter .= '(';
			foreach ($this->input->get('f-jabatan') as $key => $f) {
				$filter .= 'default_human_resource_jabatan.posisi LIKE "%'.$f.'%"';

				if($key+1<count($this->input->get('f-jabatan'))) {
					$filter .= ' OR ';
				}
				
				// set value to display in form
				$data['filter_value']['jabatan'][$index_filter++] = $f;
			}
			$filter .= ') AND ';
		}

		$filter = rtrim($filter, ' AND ');

		// Column to display
		$data['column'] = array('nama_lengkap','nama_status','nama_level','handphone','supervisor_nama_lengkap','supervisor_level','supervisor_handphone');
		if($this->input->get('display_column')) {
			$data['column'] = $this->input->get('display_column');
		}

		// Sorting Column
		$sort = NULL;
		if($this->input->get('s-field')) {
			foreach ($this->input->get('s-field') as $key => $s) {
				$sort .= $s.' '.$this->input->get('s-type')[$key].', ';
			}
		}
		$sort = rtrim($sort, ', ');

		// -------------------------------------
		// Get entries
		// -------------------------------------

		if(! group_has_role('human_resource', 'view_all_personel')) {
			if(group_has_role('human_resource', 'view_own_unit_personel')){
				$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);

				$unit_current_user = $memberships_current_user['entries'];

				if(count($unit_current_user)>0) {
					$filter .= ($filter!='') ? ' AND ' : '';
					$filter .= '(';
					foreach ($unit_current_user as $key => $value) {
						$filter .= 'default_human_resource_personel.id_organization_unit='.$value['membership_unit']['id'];

						if($key+1<count($unit_current_user)) {
							$filter .= ' OR ';
						}
					}

					$filter .= ')';
				}
				
			}
			if (group_has_role('human_resource', 'view_own_personel')){
				$filter .= ($filter!='') ? ' AND ' : '';
				$filter .= 'default_human_resource_personel.created_by='.$this->current_user->id;
			}

			if (group_has_role('human_resource', 'view_own_user_personel')){
				$filter .= ($filter!='') ? ' OR ' : '';
				$filter .= 'default_human_resource_personel.id_user='.$this->current_user->id;
			}
		}

		// -------------------------------------
		// Pagination
		// -------------------------------------
		$pagination_config = NULL;

		if($download=='') {

			$pagination_config['base_url'] = base_url(). 'admin/human_resource/personel/index';
			$pagination_config['uri_segment'] = 5;
			$pagination_config['total_rows'] = $this->personel_m->count_all_personel($filter);
			$pagination_config['per_page'] = Settings::get('records_per_page');
			$this->pagination->initialize($pagination_config);
			$data['pagination_config'] = $pagination_config;
			$data['personel']['pagination'] = $this->pagination->create_links();
		}
		
        $data['personel']['entries'] = $this->personel_m->get_personel($pagination_config, NULL, $filter, $sort);
        $data['personel']['total'] = count($data['personel']['entries']);

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$this->template->append_css('datepicker.css');
        $this->template->append_js('date-time/bootstrap-datepicker.js');
        
        if($download != '') {
        	generate_table($data['column'], $data['personel']['entries'], true);
        } else {
        $this->template->title(lang('human_resource:personel:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('human_resource:personel:plural'))
			->build('admin/personel_index', $data);
		}
    }
	
	/**
     * Display one personel
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_personel') AND ! group_has_role('human_resource', 'view_own_unit_personel') AND ! group_has_role('human_resource', 'view_own_personel') AND ! group_has_role('human_resource', 'view_own_user_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['personel'] = $this->personel_m->get_personel_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		if(!isset($data['personel'])) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		if(! group_has_role('human_resource', 'view_all_personel')){
			$skip = false;
			if(group_has_role('human_resource', 'view_own_unit_personel')){
				if(!_check_permission_own_unit($data['personel']['id'])) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (group_has_role('human_resource', 'view_own_personel') AND group_has_role('human_resource', 'view_own_user_personel')){
				if($data['personel']['created_by'] != $this->current_user->id AND $data['personel']['id_user'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (group_has_role('human_resource', 'view_own_personel') AND !group_has_role('human_resource', 'view_own_user_personel')){
				if($data['personel']['created_by'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (!group_has_role('human_resource', 'view_own_personel') AND group_has_role('human_resource', 'view_own_user_personel')){
				if($data['personel']['id_user'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

        // -------------------------------------
		// Get entries personel history
		// -------------------------------------

		$filter = array();
		if($this->input->get('type')) {
			$filter = array('type'=>$this->input->get('type'));
		}

		$pagination_config['base_url'] = base_url(). 'admin/human_resource/personel/view/'.$id;
		if (count($_GET) > 0) $pagination_config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$pagination_config['first_url'] = $pagination_config['base_url'].'?'.http_build_query($_GET);

		$pagination_config['uri_segment'] = 6;
		$pagination_config['total_rows'] = $this->personel_m->count_all_personel_history($id, $filter);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        $data['personel_history']['entries'] = $this->personel_m->get_personel_history($id, $pagination_config, $filter);
        $data['personel_history']['total'] = count($data['personel_history']['entries']);
		$data['personel_history']['pagination'] = $this->pagination->create_links();
		$data['personel_history']['type'] = $this->personel_m->get_personel_history_type();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:personel:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/admin/human_resource/personel/index')
			->set_breadcrumb($data['personel']['nama_lengkap'])
			->build('admin/personel_entry', $data);
    }

    /**
     * Display one personel
     *
     * @return  void
     */
    public function personel_history($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_personel') AND ! group_has_role('human_resource', 'view_own_unit_personel') AND ! group_has_role('human_resource', 'view_own_personel') AND ! group_has_role('human_resource', 'view_own_user_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['personel'] = $this->personel_m->get_personel_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_personel')){
			$skip = false;
			if(group_has_role('human_resource', 'view_own_unit_personel')){
				if(!_check_permission_own_unit($data['personel']['id'])) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (group_has_role('human_resource', 'view_own_personel') AND group_has_role('human_resource', 'view_own_user_personel')){
				if($data['personel']['created_by'] != $this->current_user->id AND $data['personel']['id_user'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (group_has_role('human_resource', 'view_own_personel') AND !group_has_role('human_resource', 'view_own_user_personel')){
				if($data['personel']['created_by'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (!group_has_role('human_resource', 'view_own_personel') AND group_has_role('human_resource', 'view_own_user_personel')){
				if($data['personel']['id_user'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

        // -------------------------------------
		// Get entries personel history
		// -------------------------------------

		$filter = array();
		if($this->input->get('type')) {
			$filter = array('type'=>$this->input->get('type'));
		}

		$pagination_config['base_url'] = base_url(). 'admin/human_resource/personel/view/'.$id;
		if (count($_GET) > 0) $pagination_config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$pagination_config['first_url'] = $pagination_config['base_url'].'?'.http_build_query($_GET);

		$pagination_config['uri_segment'] = 6;
		$pagination_config['total_rows'] = $this->personel_m->count_all_personel_history($id, $filter);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        $data['personel_history']['entries'] = $this->personel_m->get_personel_history($id, $pagination_config, $filter);
        $data['personel_history']['total'] = count($data['personel_history']['entries']);
		$data['personel_history']['pagination'] = $this->pagination->create_links();
		$data['personel_history']['type'] = $this->personel_m->get_personel_history_type();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:personel:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/admin/human_resource/personel/index')
			->set_breadcrumb($data['personel']['nama_lengkap'])
			->build('admin/personel_history', $data);
    }
	
	/**
     * Create a new personel entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($forced = '')
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'create_personel') AND ! group_has_role('human_resource', 'create_own_user_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_personel('new')){	
				$this->session->set_flashdata('success', lang('human_resource:personel:submit_success'));				
				redirect('admin/human_resource/personel/index');
			}else{
				$data['messages']['error'] = lang('human_resource:personel:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/human_resource/personel/index';

		// -------------------------------------
		// Get required data for form.
		// -------------------------------------
		$data['status_pekerja'] = $this->status_pekerja_m->get_status_pekerja();
		$data['level'] = $this->level_m->get_level();
		$data['metode_rekrutmen'] = $this->metode_rekrutmen_m->get_metode_rekrutmen();

		$data['provinsi_office'] = $this->provinsi_m->get_provinsi();
		$data['kota_office'] = array();
		$data['kecamatan_office'] = array();
		$data['kelurahan_office'] = array();

		$data['provinsi_home'] = $this->provinsi_m->get_provinsi();
		$data['kota_home'] = array();
		$data['kecamatan_home'] = array();
		$data['kelurahan_home'] = array();
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------

		$this->template->append_css('datepicker.css');
        $this->template->append_js('date-time/bootstrap-datepicker.js');
        $this->template->append_css('select2.css');
        $this->template->append_js('select2.full.min.js');

        $title = lang('human_resource:personel:new');
        if($forced=='forced') {
			$title = 'Lengkapi data profil';
		}
		$this->template->set('forced',$forced);
		
        $this->template->title($title)
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/admin/human_resource/personel/index')
			->set_breadcrumb($title)
			->build('admin/personel_form', $data);
    }

    /**
     * Import a new personel entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function import_personel()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'create_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		$row_skipped = 0;
		$row_inserted = 0;
		$error_index = 0;
		$insert = 0;
		$row = 0;
		$err_message = '';
		$temp = array();
		$temp_nama = array();
		$temp_user = array();
		if($this->input->post()!=null){
			$name = $_FILES['file']['name'];
			
			if($name!=null){
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$skip_status = $this->input->post('header');

					// get required data for validation
					$status_pekerja_db = array();
					$level_db = array();
					$metode_rekrutmen_db = array();
					$supervisor_db = array();
					$organization_unit_db = array();
					$user_db = array();

					$status_pekerja = get_status_pekerja();
					foreach(json_decode($status_pekerja) as $sp) {
						$status_pekerja_db[$sp->id] = $sp->nama_status;
					}
					
					$level = get_level();
					foreach(json_decode($level) as $lv) {
						$level_db[$lv->id] = $lv->nama_level;
					}
					
					$metode_rekrutmen = get_metode_rekrutmen();
					foreach(json_decode($metode_rekrutmen) as $mr) {
						$metode_rekrutmen_db[$mr->id] = $mr->nama_metode;
					}
					
					$supervisor = get_supervisor();
					foreach(json_decode($supervisor) as $sv) {
						$supervisor_db[$sv->id] = $sv->text;
					}
					
					$this->load->model('organization/units_m');
					$organization_units = $this->units_m->get_units();
					
					foreach($organization_units as $ou) {
						$organization_unit_db[$ou['id']] = $ou['unit_name'];
					}

					$users = $this->personel_m->get_personel_account(NULL,'',true);
					foreach($users as $u) {
						$user_db[$u['user_id']] = $u['display_name'];
					}

					// check duplicate data
				    $personel = $this->personel_m->get_personel();
				    $exist_nama = array();
				    $exist_user = array();
				    foreach ($personel as $key => $value) {
				    	$exist_nama[] = array(
				    		'nama_lengkap' => $value['nama_lengkap'],
				    		'jenis_kelamin' => $value['jenis_kelamin']
				    		);
				    	if(isset($value['id_user'])) {
					    	$exist_user[] = $value['id_user'];
					    }
				    }

				    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				    	$row++;
				    	
				        if($skip_status == 'yes'){
				        	$skip_status = 'no';
				        }else{
					    	// check number of column in scv
					        if (count($data) != 14) {
					        	if(count($data)>14) {
					        		$stat = lang('human_resource:more_column');
					        	} else {
					        		$stat = lang('human_resource:less_column');
					        	}
			        			$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:column_number').' '.$stat;
			        			$error_index++;
			        			continue;
			        		}

			        		// personel fields
					        $values['nama_lengkap'] = $data[0];
					        $values['jenis_kelamin'] = $data[1];
					        $values['nomor_induk'] = $data[2];
					        $values['tanggal_lahir'] = ($data[3]!='') ? $data[3] : NULL;
					        
					        $id_status_pekerja = array_search(strtolower($data[4]), array_map('strtolower', $status_pekerja_db));
					        $values['id_status_pekerja'] = ($id_status_pekerja != '') ? $id_status_pekerja : NULL;

					        $id_level = array_search(strtolower($data[5]), array_map('strtolower', $level_db));
					        $values['id_level'] = ($id_level != '') ? $id_level : NULL;
					        
					        $values['tahun_mulai_bekerja'] = ($data[6]=='NULL' || $data[6]=='') ? NULL : $data[6];
					        $values['tahun_mulai_profesional'] = ($data[7]=='NULL' || $data[7]=='') ? NULL : $data[7];
					        
					        $id_metode_rekrutmen = array_search(strtolower($data[8]), array_map('strtolower', $metode_rekrutmen_db));
					        $values['id_metode_rekrutmen'] = ($id_metode_rekrutmen != '') ? $id_metode_rekrutmen : NULL;

					        $id_supervisor = array_search(strtolower($data[9]), array_map('strtolower', $supervisor_db));
					        $values['id_supervisor'] = ($id_supervisor != '') ? $id_supervisor : NULL;

					        $values['handphone'] = $data[10];
					        $values['email'] = $data[11];

					        $id_organization_unit = array_search(strtolower($data[12]), array_map('strtolower', $organization_unit_db));
					        $values['id_organization_unit'] = ($id_organization_unit != '') ? $id_organization_unit : NULL;

					        $id_user = array_search(strtolower($data[13]), array_map('strtolower', $user_db));
					        $values['id_user'] = ($id_user != '') ? $id_user : NULL;

				        	foreach ($values as $key => $value) {
				        		// check required column
				        		$required_column = array('nama_lengkap', 'jenis_kelamin');

				        		if (in_array($key, $required_column) AND $value=='') {
				        			$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' harus diisi';
				        			$error_index++;
				        		}
				        		
				        		// check jenis_kelamin
				        		if($key=='jenis_kelamin' AND !preg_match("/^[lL|pP]$/", $value)) {
				        			$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' hanya boleh diisi "L" atau "P"';
				        			$error_index++;
				        		}

				        		// check tanggal_lahir
				        		if($key=='tanggal_lahir' AND $values['tanggal_lahir'] != '' AND !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $value)) {
				        			$error[$error_index] = lang('human_resource:row').' '.$row.' Format tanggal lahir seharusnya yyyy-mm-dd';
				        			$error_index++;
				        		}

				        		// check email format
				        		if($key=='email' AND ($value!=NULL OR $value!='') AND !filter_var($value, FILTER_VALIDATE_EMAIL)) {
				        			$error[$error_index] = lang('human_resource:row').' '.$row.' Format email salah';
				        			$error_index++;
				        		}

				        		// check email format
				        		if($key=='handphone' AND ($value!=NULL OR $value!='') AND strlen($value)>14) {
				        			$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' Panjang Karakter tidak boleh lebih dari 14';
				        			$error_index++;
				        		}

					        	// check whether the value is in the database
					        	if($key=='id_status_pekerja' AND ($data[4]!='NULL' AND $data[4] != '') AND !array_key_exists($value, $status_pekerja_db)) {
					        		$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' dengan nama "'.$data[4].'" tidak terdaftar di database';
					        			$error_index++;
					        	}

					        	if($key=='id_level' AND ($data[5]!='NULL' AND $data[5] != '') AND !array_key_exists($value, $level_db)) {
						        	$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' dengan nama "'.$data[5].'" tidak terdaftar di database';
					        			$error_index++;
					        	}

					        	if($key=='id_metode_rekrutmen' AND ($data[8]!='NULL' AND $data[8] != '') AND !array_key_exists($value, $metode_rekrutmen_db)) {
					        		$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' dengan nama "'.$data[8].'" tidak terdaftar di database';
					        			$error_index++;
					        	}


					        	if($key=='id_supervisor' AND ($data[9]!='NULL' AND $data[9] != '') AND !array_key_exists($value, $supervisor_db)) {
					        		$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' dengan nama "'.$data[9].'" tidak terdaftar di database';
					        			$error_index++;
					        	}
					        	
					        	if($key=='id_organization_unit' AND ($data[12]!='NULL' AND $data[12] != '') AND !array_key_exists($value, $organization_unit_db)) {
					        		$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' dengan nama "'.$data[12].'" tidak terdaftar di database';
					        			$error_index++;
					        	}
					        	
					        	if($key=='id_user' AND ($data[13]!='NULL' AND $data[13] != '') AND !array_key_exists($value, $user_db)) {
					        		$error[$error_index] = lang('human_resource:row').' '.$row.' '.lang('human_resource:'.$key).' dengan nama "'.$data[13].'" tidak terdaftar di database';
					        			$error_index++;
					        	}

				        	}
				        	
				        	$cek_value_nama = array('nama_lengkap'=>$values['nama_lengkap'],'jenis_kelamin'=>$values['jenis_kelamin']);
				        	$cek_value_user = $values['id_user'];

				        	// check with data in csv file
				        	if((count($temp_nama) > 0 AND in_array($cek_value_nama, $temp_nama)) OR (count($temp_nama) > 0 AND in_array($cek_value_user, $temp_user))) {
				        		$ket = '';
				        		if(count($temp_nama) > 0 AND in_array($cek_value_nama, $temp_nama)) {
					        		$pos_data = array_search($cek_value_nama, $temp_nama)+1;
					        		$ket .= '"Nama Lengkap dan Jenis Kelamin" sama';
					        	}

					        	if(count($temp_user) > 0 AND in_array($cek_value_user, $temp_user)) {
					        		$pos_data = array_search($cek_value_user, $temp_user)+1;
					        		$ket .= '"Display Name" sama';
					        	}

				        		if($this->input->post('header') == 'yes') {
				        			$pos_data++;
				        		}

				        		$error[$error_index] = lang('human_resource:row').' '.$row.' Data sudah ada di baris '.$pos_data.', '.$ket;
				        		$error_index++;
				        	}

				        	// check with data in database
				        	if((count($exist_nama) > 0 AND in_array($cek_value_nama, $exist_nama)) OR (count($exist_user) > 0 AND in_array($cek_value_user, $exist_user))) {
				        		$ket = '';
				        		if(count($exist_nama) > 0 AND in_array($cek_value_nama, $exist_nama)) {
					        		$pos_data = array_search($cek_value_nama, $exist_nama)+1;
					        		$ket .= '"Nama Lengkap dan Jenis Kelamin" sudah digunakan';
					        	}

					        	if(count($exist_user) > 0 AND in_array($cek_value_user, $exist_user)) {
					        		$pos_data = array_search($cek_value_user, $exist_user)+1;

					        		$ket .= ($ket!='') ? ', ' : '';
					        		$ket .= '"Display Name" sudah digunakan';
					        	}

				        		if($this->input->post('header') == 'yes') {
				        			$pos_data++;
				        		}

				        		$error[$error_index] = lang('human_resource:row').' '.$row.' Personel sudah ada di database'.', '.$ket;
				        		$error_index++;
				        	}
				        	
				        	$temp_nama[] = array($values['nama_lengkap'],$values['jenis_kelamin']);
				        	if(isset($values['id_user'])) {
					        	$temp_user[] = $values['id_user'];
					        }

				        	if($error_index==0) {
				        		$temp[$insert] = $values;
					        	$insert++;	
				        	}
				        }
				    }
				    
				    // $error_index=1;
	            	
	            	if($error_index != 0){
	            		for ($i = 0; $i < count($error); $i++) {
		            		$err_message .= $error[$i]."<br>"; 
		            	}
		            	
				    	fclose($handle);

				    	$data['messages']['error'] = $err_message;
	            	}else{
	            		for ($i=0; $i < count($temp); $i++) {
	            			$this->db->trans_start();
							$retval = $this->personel_m->insert_personel($temp[$i]);

							if($retval) {
								$row_inserted++;
							}
	            			$this->db->trans_complete();
	            		}
	            		if($row_inserted > 0){
		            		if($row_inserted == 1){
								$this->session->set_flashdata('success', $row_inserted.' '.lang('human_resource:scc1_import'));
		            		}else{
		            			$this->session->set_flashdata('success', $row_inserted.' '.lang('human_resource:scc2_import'));
		            		}
		            	}
		            	fclose($handle);
		            	
		            	redirect('admin/human_resource/personel/index');
	            	}
				}
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/human_resource/personel/index';
	
		// -------------------------------------
		// Build the form page.
		// -------------------------------------

		$this->template->title(lang('human_resource:personel:import'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:import'))
			->build('admin/personel_form_import', $data);
    }

    public function download_csv_sample() {
    	role_or_die('human_resource', 'create_personel');

		$this->load->helper('download');
		force_download("personel_example.csv", file_get_contents(base_url().'uploads/personel_import_example.csv'));
    }
	
	/**
     * Edit a personel entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the personel to the be deleted.
     * @return	void
     */
    public function edit($id = 0,$forced = '')
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'edit_all_personel') AND ! group_has_role('human_resource', 'edit_own_unit_personel') AND ! group_has_role('human_resource', 'edit_own_personel') AND ! group_has_role('human_resource', 'edit_own_user_personel')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('human_resource', 'edit_all_personel')){
			$entry = $this->personel_m->get_personel_by_id($id);
			$created_by_user_id = $entry['created_by'];
			$skip = false;
			if(group_has_role('human_resource', 'edit_own_unit_personel')){
				if(!_check_permission_own_unit($entry['id'])) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (group_has_role('human_resource', 'edit_own_personel') AND group_has_role('human_resource', 'edit_own_user_personel')){
				if($entry['created_by'] != $this->current_user->id AND $entry['id_user'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (group_has_role('human_resource', 'edit_own_personel') AND !group_has_role('human_resource', 'edit_own_user_personel')){
				if($entry['created_by'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif (!group_has_role('human_resource', 'edit_own_personel') AND group_has_role('human_resource', 'edit_own_user_personel')){
				if($entry['id_user'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
			
		}
		
		$data['fields'] = $this->personel_m->get_personel_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/human_resource/personel/index';
		$data['entry_id'] = $id;

		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_personel('edit', $id, $data['fields'])){	
				$this->session->set_flashdata('success', lang('human_resource:personel:submit_success'));
				redirect('admin/human_resource/personel/index');
			}else{
				$data['messages']['error'] = lang('human_resource:personel:submit_failure');
			}
		}
		

		// -------------------------------------
		// Get required data for form.
		// -------------------------------------
		$data['status_pekerja'] = $this->status_pekerja_m->get_status_pekerja();
		$data['level'] = $this->level_m->get_level();
		$data['metode_rekrutmen'] = $this->metode_rekrutmen_m->get_metode_rekrutmen();

		$data['personel_kelurahan_office'] = $this->kelurahan_m->get_kelurahan_by_id($data['fields']['id_location_kelurahan_office']);
		$data['personel_kelurahan_home'] = $this->kelurahan_m->get_kelurahan_by_id($data['fields']['id_location_kelurahan_home']);

		$data['provinsi_office'] = $this->provinsi_m->get_provinsi();
		$data['kota_office'] = $this->kota_m->get_kota_by_provinsi($data['personel_kelurahan_office']['id_provinsi']);
		$data['kecamatan_office'] = $this->kecamatan_m->get_kecamatan_by_kota($data['personel_kelurahan_office']['id_kota']);
		$data['kelurahan_office'] = $this->kelurahan_m->get_kelurahan_by_kecamatan($data['personel_kelurahan_office']['id_kecamatan']);

		$data['provinsi_home'] = $this->provinsi_m->get_provinsi();
		$data['kota_home'] = $this->kota_m->get_kota_by_provinsi($data['personel_kelurahan_home']['id_provinsi']);
		$data['kecamatan_home'] = $this->kecamatan_m->get_kecamatan_by_kota($data['personel_kelurahan_home']['id_kota']);
		$data['kelurahan_home'] = $this->kelurahan_m->get_kelurahan_by_kecamatan($data['personel_kelurahan_home']['id_kecamatan']);

		$data['jabatan'] = $this->personel_m->get_jabatan_by_personel($id);
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------

		$this->template->append_css('datepicker.css');
        $this->template->append_js('date-time/bootstrap-datepicker.js');
        $this->template->append_css('select2.css');
        $this->template->append_js('select2.full.min.js');

        $title = lang('human_resource:personel:edit');
        if($forced=='forced') {
			$title = 'Lengkapi data profil';
		}
		$this->template->set('forced',$forced);
		
        $this->template->title($title)
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:view'), '/admin/human_resource/personel/view/'.$id)
			->set_breadcrumb($title)
			->build('admin/personel_form', $data);
    }
	
	/**
     * Delete a personel entry
     * 
     * @param   int [$id] The id of personel to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'delete_all_personel') AND ! group_has_role('human_resource', 'delete_own_unit_personel') AND ! group_has_role('human_resource', 'delete_own_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('human_resource', 'delete_all_personel')){
			$entry = $this->personel_m->get_personel_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if(group_has_role('human_resource', 'delete_own_unit_personel')){
				if(!_check_permission_own_unit($entry['id'])) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->personel_m->delete_personel_by_id($id);
        $this->session->set_flashdata('error', lang('human_resource:personel:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/human_resource/personel/index');
    }
	
	/**
     * Insert or update personel entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_personel($method, $row_id = null, $old_data = NULL)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// Check value
		if(!$this->input->post('tanggal_lahir')) {
			$values['tanggal_lahir'] = NULL;
		}

		if(!$this->input->post('id_status_pekerja')) {
			$values['id_status_pekerja'] = NULL;
		}

		if(!$this->input->post('id_location_kelurahan_office')) {
			$values['id_location_kelurahan_office'] = NULL;
		}

		if(!$this->input->post('id_location_kelurahan_home')) {
			$values['id_location_kelurahan_home'] = NULL;
		}

		if(!$this->input->post('id_level')) {
			$values['id_level'] = NULL;
		}

		if(!$this->input->post('id_metode_rekrutmen')) {
			$values['id_metode_rekrutmen'] = NULL;
		}

		if(!$this->input->post('tahun_mulai_bekerja')) {
			$values['tahun_mulai_bekerja'] = NULL;
		}

		if(!$this->input->post('tahun_mulai_profesional')) {
			$values['tahun_mulai_profesional'] = NULL;
		}

		if(!$this->input->post('id_supervisor')) {
			$values['id_supervisor'] =  NULL;
		}

		if(!$this->input->post('id_organization_unit')) {
			$values['id_organization_unit'] =  NULL;
		}

		if(!$this->input->post('id_user')) {
			$values['id_user'] =  NULL;
		}

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama_lengkap', lang('human_resource:nama_lengkap'), 'required');
		$this->form_validation->set_rules('jenis_kelamin', lang('human_resource:jenis_kelamin'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			for($n=0; $n<count($values['bulan_mulai']); $n++) {
				if($values['posisi'][$n]!='') {
					$values['jabatan'][] = array(
						'bulan_mulai' => $values['bulan_mulai'][$n],
						'tahun_mulai' => $values['tahun_mulai'][$n],
						'bulan_selesai' => $values['bulan_selesai'][$n],
						'tahun_selesai' => $values['tahun_selesai'][$n],
						'posisi' => $values['posisi'][$n],
						'deskripsi' => $values['deskripsi_jabatan'][$n],
						);
				}
			}

			unset($values['bulan_mulai']);
			unset($values['tahun_mulai']);
			unset($values['bulan_selesai']);
			unset($values['tahun_selesai']);
			unset($values['posisi']);
			unset($values['deskripsi_jabatan']);

			if ($method == 'new')
			{
				if(!group_has_role('human_resource','set_complete_personel')) {
					$values['is_complete'] = $this->config->item('default_is_complete');
				}

				$result = $this->personel_m->insert_personel($values);
			}
			else
			{
				if(!group_has_role('human_resource','set_complete_personel')) {
					$values['is_complete'] = 1;
				}

				$history = array();

				$old_jabatan = $this->personel_m->get_jabatan_by_personel($row_id);
				
				if(isset($values['jabatan']) AND count($old_jabatan) != count($values['jabatan'])) {
					if(count($old_jabatan)>count($values['jabatan'])) {
						$ket = 'pengurangan';
					} else if(count($old_jabatan)<count($values['jabatan'])) {
						$ket = 'penambahan';
					}
					$description = 'Ada '.$ket.' Jabatan Personel';
					
					$history[] = array('type' => 'update-data-kepegawaian-jabatan', 'description' => $description);
				} else if (isset($values['jabatan']) AND count($old_jabatan)==count($values['jabatan'])) {
					for ($i=0; $i < count($old_jabatan); $i++) { 
						if(isset($values['jabatan'][$i]['bulan_mulai']) and $values['jabatan'][$i]['bulan_mulai']!=$old_jabatan[$i]['bulan_mulai']) {
							$description = 'Ada perubahan pada Jabatan Personel';
							
							$history[] = array('type' => 'update-data-kepegawaian-jabatan', 'description' => $description);
						}
					}
				}

				if($old_data['nama_lengkap'] != $values['nama_lengkap'] OR $old_data['jenis_kelamin'] != $values['jenis_kelamin'] OR format_date($old_data['tanggal_lahir'],'d-m-Y') != format_date($values['tanggal_lahir'],'d-m-Y')) {
					$description = '<ul>';
					$count_diff = 0;
					if($old_data['nama_lengkap'] != $values['nama_lengkap']) {
						$count_diff++;

						$old = ($old_data['nama_lengkap']!='') ? $old_data['nama_lengkap'] : '""';
						$new = ($values['nama_lengkap']!='') ? $values['nama_lengkap'] : '""';
						$description .= '<li>Nama lengkap berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if($old_data['jenis_kelamin'] != $values['jenis_kelamin']) {
						$count_diff++;
						$jenis_kelamin = array('L'=>'Laki-laki', 'P'=>'Perempuan');

						$old = ($jenis_kelamin[$old_data['jenis_kelamin']]) ? $jenis_kelamin[$old_data['jenis_kelamin']] : '""';
						$new = ($jenis_kelamin[$values['jenis_kelamin']]) ? $jenis_kelamin[$values['jenis_kelamin']] : '""';
						$description .= '<li>Jenis kelamin berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if(format_date($old_data['tanggal_lahir'],'d-m-Y') != format_date($values['tanggal_lahir'],'d-m-Y')) {
						$count_diff++;
						$old = ($old_data['tanggal_lahir']!='') ? format_date($old_data['tanggal_lahir'],'d-m-Y') : '""';
						$new = ($values['tanggal_lahir']!='') ? format_date($values['tanggal_lahir'], 'd-m-Y') : '""';
						$description .= '<li>Tanggal lahir berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					$description .= '</ul>';

					if($count_diff==1) {
						$description = substr($description,8,strlen($description)-10);
					}

					$history[] = array('type' => 'update-data-personal', 'description' => $description);
				}

				if($old_data['address_office'] != $values['address_office'] OR $old_data['postal_code_office'] != $values['postal_code_office'] OR $old_data['id_location_kelurahan_office'] != $values['id_location_kelurahan_office'] OR 
					$old_data['address_home'] != $values['address_home'] OR $old_data['postal_code_home'] != $values['postal_code_home'] OR $old_data['id_location_kelurahan_home'] != $values['id_location_kelurahan_home']) {
					$description = '<ul>';
					$count_diff = 0;

					//================ Office ========================//
					if($old_data['address_office'] != $values['address_office']) {
						$count_diff++;

						$old = ($old_data['address_office']!='') ? $old_data['address_office'] : '""';
						$new = ($values['address_office']!='') ? $values['address_office'] : '""';
						$description .= '<li>Alamat Kantor berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if($old_data['id_location_kelurahan_office'] != $values['id_location_kelurahan_office']) {
						$count_diff++;

						$old = ($old_data['id_location_kelurahan_office']!='') ? $this->kelurahan_m->get_kelurahan_by_id($old_data['id_location_kelurahan_office'])['nama'] : '""';
						$new = ($values['id_location_kelurahan_office']!='') ? $this->kelurahan_m->get_kelurahan_by_id($values['id_location_kelurahan_office'])['nama'] : '""';
						$description .= '<li>Kelurahan Kantor berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if($old_data['postal_code_office'] != $values['postal_code_office']) {
						$count_diff++;

						$old = ($old_data['postal_code_office']!='') ? $old_data['postal_code_office'] : '""';
						$new = ($values['postal_code_office']!='') ? $values['postal_code_office'] : '""';
						$description .= '<li>Kode POS Kantor berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					//================== Home ===========================//
					if($old_data['address_home'] != $values['address_home']) {
						$count_diff++;

						$old = ($old_data['address_home']!='') ? $old_data['address_home'] : '""';
						$new = ($values['address_home']!='') ? $values['address_home'] : '""';
						$description .= '<li>Alamat Rumah berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if($old_data['id_location_kelurahan_home'] != $values['id_location_kelurahan_home']) {
						$count_diff++;

						$old = ($old_data['id_location_kelurahan_home']!='') ? $this->kelurahan_m->get_kelurahan_by_id($old_data['id_location_kelurahan_home'])['nama'] : '""';
						$new = ($values['id_location_kelurahan_home']!='') ? $this->kelurahan_m->get_kelurahan_by_id($values['id_location_kelurahan_home'])['nama'] : '""';
						$description .= '<li>Kelurahan Rumah berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if($old_data['postal_code_home'] != $values['postal_code_home']) {
						$count_diff++;

						$old = ($old_data['postal_code_home']!='') ? $old_data['postal_code_home'] : '""';
						$new = ($values['postal_code_home']!='') ? $values['postal_code_home'] : '""';
						$description .= '<li>Kode POS Rumah berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					$description .= '</ul>';

					if($count_diff==1) {
						$description = substr($description,8,strlen($description)-10);
					}
					
					$history[] = array('type' => 'update-data-address', 'description' => $description);
				}

				if($old_data['phone_office'] != $values['phone_office'] OR $old_data['phone_home'] != $values['phone_home'] OR $old_data['handphone'] != $values['handphone'] OR $old_data['email'] != $values['email']) {
					$description = '<ul>';
					$count_diff = 0;
					
					if($old_data['phone_office'] != $values['phone_office']) {
						$count_diff++;

						$old = ($old_data['phone_office']!='') ? $old_data['phone_office'] : '""';
						$new = ($values['phone_office']!='') ? $values['phone_office'] : '""';
						$description .= '<li>Nomor telpon kantor berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if($old_data['phone_home'] != $values['phone_home']) {
						$count_diff++;

						$old = ($old_data['phone_home']!='') ? $old_data['phone_home'] : '""';
						$new = ($values['phone_home']!='') ? $values['phone_home'] : '""';
						$description .= '<li>Nomor telpon rumah berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if($old_data['handphone'] != $values['handphone']) {
						$count_diff++;

						$old = ($old_data['handphone']!='') ? $old_data['handphone'] : '""';
						$new = ($values['handphone']!='') ? $values['handphone'] : '""';
						$description .= '<li>Nomor handphone berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					if($old_data['email'] != $values['email']) {
						$count_diff++;

						$old = ($old_data['email']!='') ? $old_data['email'] : '""';
						$new = ($values['email']!='') ? $values['email'] : '""';
						$description .= '<li>Email berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong></li>';
					}

					$description .= '</ul>';

					if($count_diff==1) {
						$description = substr($description,8,strlen($description)-10);
					}
					
					$history[] = array('type' => 'update-data-kontak', 'description' => $description);
				}

				if($old_data['nomor_induk'] != $values['nomor_induk']) {
					$old_ni = ($old_data['nomor_induk'] != '') ? $old_data['nomor_induk'] : '""';
					$new_ni = ($values['nomor_induk'] != '') ? $values['nomor_induk'] : '""';
					$description = 'Nomor induk berubah dari <strong>'.$old_ni.'</strong> menjadi <strong>'.$new_ni.'</strong>';
					
					$history[] = array('type' => 'update-data-kepegawaian-nomor-induk', 'description' => $description);
				}

				if($old_data['id_status_pekerja'] != $values['id_status_pekerja']) {
					$old_status = $this->status_pekerja_m->get_status_pekerja_by_id($old_data['id_status_pekerja']);
					$new_status = $this->status_pekerja_m->get_status_pekerja_by_id($values['id_status_pekerja']);

					$old = ($old_status['nama_status']!='') ? $old_status['nama_status'] : '""';
					$new = ($new_status['nama_status']!='') ? $new_status['nama_status'] : '""';
					$description = 'Status pekerja berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong>';
					
					$history[] = array('type' => 'update-data-kepegawaian-status-pekerja', 'description' => $description);
				}

				if($old_data['id_level'] != $values['id_level']) {
					$old_level = $this->level_m->get_level_by_id($old_data['id_level']);
					$new_level = $this->level_m->get_level_by_id($values['id_level']);

					$old = ($old_level['nama_level']!='') ? $old_level['nama_level'] : '""';
					$new = ($new_level['nama_level']!='') ? $new_level['nama_level'] : '""';
					$description = 'Level berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong>';
					
					$history[] = array('type' => 'update-data-kepegawaian-level', 'description' => $description);
				}

				if($old_data['tahun_mulai_bekerja'] != $values['tahun_mulai_bekerja']) {
					$old_tahun = ($old_data['tahun_mulai_bekerja']!='') ? $old_data['tahun_mulai_bekerja'] : '""';
					$new_tahun = ($values['tahun_mulai_bekerja']!='') ? $values['tahun_mulai_bekerja'] : '""';

					$description = 'Tahun mulai bekerja berubah dari <strong>'.$old_tahun.'</strong> menjadi <strong>'.$new_tahun.'</strong>';
					
					$history[] = array('type' => 'update-data-kepegawaian-tahun-mulai-bekerja', 'description' => $description);
				}

				if($old_data['tahun_mulai_profesional'] != $values['tahun_mulai_profesional']) {
					$old_tahun = ($old_data['tahun_mulai_profesional']!='') ? $old_data['tahun_mulai_profesional'] : '""';
					$new_tahun = ($values['tahun_mulai_profesional']!='') ? $values['tahun_mulai_profesional'] : '""';

					$description = 'Tahun mulai profesional berubah dari <strong>'.$old_tahun.'</strong> menjadi <strong>'.$new_tahun.'</strong>';
					
					$history[] = array('type' => 'update-data-kepegawaian-tahun-mulai-profesional', 'description' => $description);
				}

				if($old_data['id_metode_rekrutmen'] != $values['id_metode_rekrutmen']) {
					$old_metode = $this->metode_rekrutmen_m->get_metode_rekrutmen_by_id($old_data['id_metode_rekrutmen']);
					$new_metode = $this->metode_rekrutmen_m->get_metode_rekrutmen_by_id($values['id_metode_rekrutmen']);

					$old = ($old_metode['nama_metode']!='') ? $old_metode['nama_metode'] : '""';
					$new = ($new_metode['nama_metode']!='') ? $new_metode['nama_metode'] : '""';
					$description = 'Metode rekrutmen berubah dari <strong>'.$old.'</strong> menjadi <strong>'.$new.'</strong>';
					
					$history[] = array('type' => 'update-data-kepegawaian-metode-rekrutmen', 'description' => $description);
				}

				if($old_data['id_supervisor'] != $values['id_supervisor']) {
					$old_supervisor = $this->personel_m->get_personel_by_id($old_data['id_supervisor']);
					$new_supervisor = $this->personel_m->get_personel_by_id($values['id_supervisor']);

					$old_supervisor = ($old_supervisor !='') ? $old_supervisor : array('nama_lengkap'=>'""');
					$new_supervisor = ($new_supervisor !='') ? $new_supervisor : array('nama_lengkap'=>'""');

					$description = 'Supervisor berubah dari <strong>'.$old_supervisor['nama_lengkap'].'</strong> menjadi <strong>'.$new_supervisor['nama_lengkap'].'</strong>.';
					$history[] = array('type' => 'update-data-kepegawaian-supervisor', 'description' => $description);
				}

				if($old_data['id_organization_unit'] != $values['id_organization_unit']) {
					$old_unit = $this->personel_m->get_organization_unit_by_id($old_data['id_organization_unit']);
					$new_unit = $this->personel_m->get_organization_unit_by_id($values['id_organization_unit']);

					$old_unit = ($old_unit !='') ? $old_unit : array('unit_name'=>'""');
					$new_unit = ($new_unit !='') ? $new_unit : array('unit_name'=>'""');

					$description = 'Unit berubah dari <strong>'.$old_unit['unit_name'].'</strong> menjadi <strong>'.$new_unit['unit_name'].'</strong>';
					
					$history[] = array('type' => 'update-data-kepegawaian-organization-unit', 'description' => $description);
				}

				if($old_data['id_user'] != $values['id_user']) {
					$old_user = $this->personel_m->get_user_by_id($old_data['id_user']);
					$new_user = $this->personel_m->get_user_by_id($values['id_user']);

					$old_user = ($old_user !='') ? $old_user : array('display_name'=>'""');
					$new_user = ($new_user !='') ? $new_user : array('display_name'=>'""');

					$description = 'Akun berubah dari <strong>'.$old_user['display_name'].'</strong> menjadi <strong>'.$new_user['display_name'].'</strong>';
					
					$history[] = array('type' => 'update-data-kepegawaian-account', 'description' => $description);
				}

				$result = $this->personel_m->update_personel($values, $row_id, $history);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function get_supervisor($id = 0) {
		$data_json = get_supervisor($id);
		echo $data_json;
	}

	public function get_organization_unit() {
		$data_json = get_organization_unit();
		echo $data_json;
	}

	public function get_user($exception_id = '') {
		$data_json = get_user($exception_id);
		echo $data_json;
	}

	public function get_status_pekerja() {
		$data_json = get_status_pekerja();
		echo $data_json;
	}

	public function get_level() {
		$data_json = get_level();
		echo $data_json;
	}

	public function get_metode_rekrutmen() {
		$data_json = get_metode_rekrutmen();
		echo $data_json;
	}

	public function get_tahun_mulai_bekerja() {
		$data_json = get_tahun_mulai_bekerja();
		echo $data_json;
	}

	public function get_tahun_mulai_profesional() {
		$data_json = get_tahun_mulai_profesional();
		echo $data_json;
	}

	public function get_kota_by_provinsi($id_provinsi='')
	{
		$kota = $this->kota_m->get_kota_by_provinsi($id_provinsi);

		echo json_encode($kota);
	}

	public function get_kecamatan_by_kota($id_kota='')
	{
		$kecamatan = $this->kecamatan_m->get_kecamatan_by_kota($id_kota);

		echo json_encode($kecamatan);
	}

	public function get_kelurahan_by_kecamatan($id_kecamatan='')
	{
		$kelurahan = $this->kelurahan_m->get_kelurahan_by_kecamatan($id_kecamatan);

		echo json_encode($kelurahan);
	}

	public function get_user_account($id) {
		$this->load->model('users/user_m');

		$user = $this->user_m->get(array('id'=>$id));
		$memberships_user = $this->organization->get_membership_by_user($id);
		
		if(count($memberships_user['entries'])>0) {
			$organization = $memberships_user['entries'][0]['membership_unit']['id'];
		} else {
			$organization = 0;
		}
		$data = array(
			'display_name'=>$user->display_name,
			'email'=>$user->email,
			'organization'=>$organization,
			);
		
		echo json_encode($data);
	}
}