<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Admin_status_pekerja extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'status_pekerja';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'access_status_pekerja_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('human_resource');		
		$this->load->model('status_pekerja_m');
    }

    /**
	 * List all status_pekerja
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/human_resource/status_pekerja/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $this->status_pekerja_m->count_all_status_pekerja();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['status_pekerja']['entries'] = $this->status_pekerja_m->get_status_pekerja($pagination_config);
		$data['status_pekerja']['total'] = count($data['status_pekerja']['entries']);
		$data['status_pekerja']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:status_pekerja:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:plural'))
			->build('admin/status_pekerja_index', $data);
    }
	
	/**
     * Display one status_pekerja
     *
     * @return  void
     */
    public function view($id = 0)
    {
    	// there is not need to view
    	redirect('admin/human_resource/status_pekerja/index');

        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['status_pekerja'] = $this->status_pekerja_m->get_status_pekerja_by_id($id);
		
		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:status_pekerja:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('human_resource:status_pekerja:plural'), '/admin/human_resource/status_pekerja/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:view'))
			->build('admin/status_pekerja_entry', $data);
    }
	
	/**
     * Create a new status_pekerja entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'create_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_status_pekerja('new')){	
				$this->session->set_flashdata('success', lang('human_resource:status_pekerja:submit_success'));				
				redirect('admin/human_resource/status_pekerja/index');
			}else{
				$data['messages']['error'] = lang('human_resource:status_pekerja:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/human_resource/status_pekerja/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:status_pekerja:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:plural'), '/admin/human_resource/status_pekerja/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:new'))
			->build('admin/status_pekerja_form', $data);
    }
	
	/**
     * Edit a status_pekerja entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the status_pekerja to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'edit_all_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_status_pekerja('edit', $id)){	
				$this->session->set_flashdata('success', lang('human_resource:status_pekerja:submit_success'));				
				redirect('admin/human_resource/status_pekerja/index');
			}else{
				$data['messages']['error'] = lang('human_resource:status_pekerja:submit_failure');
			}
		}
		
		$data['fields'] = $this->status_pekerja_m->get_status_pekerja_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/human_resource/status_pekerja/index';
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:status_pekerja:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:plural'), '/admin/human_resource/status_pekerja/index')
			->set_breadcrumb(lang('human_resource:status_pekerja:view'), '/admin/human_resource/status_pekerja/view/'.$id)
			->set_breadcrumb(lang('human_resource:status_pekerja:edit'))
			->build('admin/status_pekerja_form', $data);
    }
	
	/**
     * Delete a status_pekerja entry
     * 
     * @param   int [$id] The id of status_pekerja to be deleted
     * @return  void
     */
    public function delete($id = 0, $order = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'delete_all_status_pekerja')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->status_pekerja_m->delete_status_pekerja_by_id($id, $order);
        $this->session->set_flashdata('error', lang('human_resource:status_pekerja:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/human_resource/status_pekerja/index');
    }
	
	/**
     * Insert or update status_pekerja entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_status_pekerja($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama_status', lang('human_resource:nama_status'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->status_pekerja_m->insert_status_pekerja($values);
				
			}
			else
			{
				$result = $this->status_pekerja_m->update_status_pekerja($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function move($direction = '', $id = 0, $order = 0) {
		$this->load->helper('human_resource');
		move('status_pekerja', $direction, $id, $order);
	}
}