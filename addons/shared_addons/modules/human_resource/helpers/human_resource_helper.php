<?php
	function move($module = '', $direction = '', $id = 0, $order = 0) {
		ci()->load->model('status_pekerja_m');
		ci()->load->model('level_m');
		ci()->load->model('metode_rekrutmen_m');

		if($id == 0 or $order == 0 or $direction == '') {
			redirect('admin/human_resource/'.$module.'/index');
		}

		if($module=='status_pekerja') {
			$data = ci()->status_pekerja_m->get_status_pekerja();
			$move = ci()->status_pekerja_m->move_status_pekerja($id, $order, $direction);
		}elseif($module=='level') {
			$data = ci()->level_m->get_level();
			$move = ci()->level_m->move_level($id, $order, $direction);
		}elseif($module=='metode_rekrutmen') {
			$data = ci()->metode_rekrutmen_m->get_metode_rekrutmen();
			$move = ci()->metode_rekrutmen_m->move_metode_rekrutmen($id, $order, $direction);
		}else{
			ci()->session->set_flashdata('error', 'Module tidak ditemukan');
			redirect('admin/human_resource/'.$module.'/index');
		}

		if($direction == 'up' AND $order == 1) {
			ci()->session->set_flashdata('error', 'Terjadi kesalahan saat memindah ke atas');
			redirect('admin/human_resource/'.$module.'/index');
		}

		if($direction == 'down' AND $order == count($data)) {
			ci()->session->set_flashdata('error', 'Terjadi kesalahan saat memindah ke atas');
			redirect('admin/human_resource/'.$module.'/index');
		}


		if ($move === FALSE) {
			ci()->session->set_flashdata('error', 'Terjadi kesalahan saat memindah status pekerja');
		} else {
			ci()->session->set_flashdata('success', 'Berhasil memindah status pekerja');
		}

		// echo ci()->db->last_query();

		redirect('admin/human_resource/'.$module.'/index');
	}

	function generate_table($column, $entries, $for_download = false) {

		if($for_download) {
			$html ="
			<table width=\"1200\" border=\"0\" cellpadding=\"5\">
				<tr><td></td></tr>
				<tr>
					<td colspan=\"".(count($column)+1)."\" align=\"center\">
				<span style=\"font: bold 20px Open Sans; display: block;\">Data Personel Human Resource</span>
					</td>
				</tr>
				<tr><td></td></tr>
			</table>";

			$html .= '<table class="table table-striped table-bordered table-hover" border="1">';
		} else {
			$html = '<table class="table table-striped table-bordered table-hover">';
		}

		$html .= '<thead>
			<tr>
				<th>No</th>';
				
				if(in_array('id', $column)) $html .= '<th>ID</th>';
				if(in_array('nama_lengkap', $column)) $html .= '<th>'.lang('human_resource:nama_lengkap').'</th>';
				if(in_array('jenis_kelamin', $column)) $html .= '<th>'.lang('human_resource:jenis_kelamin').'</th>';
				if(in_array('tanggal_lahir', $column)) $html .= '<th>'.lang('human_resource:tanggal_lahir').'</th>';
				if(in_array('nomor_induk', $column)) $html .= '<th>'.lang('human_resource:nomor_induk').'</th>';
				if(in_array('jabatan', $column)) $html .= '<th>'.lang('human_resource:jabatan').'</th>';
				if(in_array('nama_status', $column)) $html .= '<th>'.lang('human_resource:id_status_pekerja').'</th>';
				if(in_array('nama_level', $column)) $html .= '<th>'.lang('human_resource:id_level').'</th>';
				if(in_array('tahun_mulai_bekerja', $column)) $html .= '<th>'.lang('human_resource:tahun_mulai_bekerja').'</th>';
				if(in_array('tahun_mulai_profesional', $column)) $html .= '<th>'.lang('human_resource:tahun_mulai_profesional').'</th>';
				if(in_array('nama_metode', $column)) $html .= '<th>'.lang('human_resource:id_metode_rekrutmen').'</th>';
				if(in_array('handphone', $column)) $html .= '<th>'.lang('human_resource:handphone').'</th>';
				if(in_array('email', $column)) $html .= '<th>'.lang('human_resource:email').'</th>';
				if(in_array('unit_name', $column)) $html .= '<th>'.lang('human_resource:id_organization_unit').'</th>';
				if(in_array('id_user', $column)) $html .= '<th>'.lang('human_resource:id_user').'</th>';
				if(in_array('supervisor_nama_lengkap', $column)) $html .= '<th>'.lang('human_resource:id_supervisor').'</th>';
				if(in_array('supervisor_level', $column)) $html .= '<th>'.lang('human_resource:id_level').' Supervisor</th>';
				if(in_array('supervisor_handphone', $column)) $html .= '<th>'.lang('human_resource:handphone').' Supervisor</th>';
				if(in_array('supervisor_email', $column)) $html .= '<th>'.lang('human_resource:email').' Supervisor</th>';
				if(in_array('address_office', $column)) $html .= '<th>'.lang('human_resource:alamat_kantor').'</th>';
				if(in_array('address_home', $column)) $html .= '<th>'.lang('human_resource:alamat_rumah').'</th>';
				if(in_array('phone_office', $column)) $html .= '<th>'.lang('human_resource:telpon_kantor').'</th>';
				if(in_array('phone_home', $column)) $html .= '<th>'.lang('human_resource:telpon_rumah').'</th>';
				if(in_array('created_on', $column)) $html .= '<th>'.lang('human_resource:created_on').'</th>';
				if(in_array('created_by', $column)) $html .= '<th>'.lang('human_resource:created_by').'</th>';
				if(in_array('updated_on', $column)) $html .= '<th>'.lang('human_resource:updated_on').'</th>';

				if(!$for_download) {
					if (group_has_role('human_resource', 'view_all_personel') OR group_has_role('human_resource', 'view_own_personel') OR group_has_role('human_resource', 'view_own_unit_personel')
					OR group_has_role('human_resource', 'edit_all_personel') OR group_has_role('human_resource', 'edit_own_personel') OR group_has_role('human_resource', 'edit_own_unit_personel')
					OR group_has_role('human_resource', 'delete_all_personel') OR group_has_role('human_resource', 'delete_own_personel') OR group_has_role('human_resource', 'delete_own_unit_personel')) { 
						$html .= '<th style="width:180px"></th>';
					}
				}
			$html .= '</tr>
		</thead>
		<tbody>';
			 
			$no = 1;
			
			foreach ($entries as $personel_entry): 
			$html .= '<tr>
				<td>'.$no.'</td>';
				$no++;

				if(in_array('id', $column)) $html .= '<td>'.$personel_entry['id'].'</td>';
				if(in_array('nama_lengkap', $column)) $html .= '<td>'.$personel_entry['nama_lengkap'].'</td>';
				if(in_array('jenis_kelamin', $column)) { $html .= '<td>'; $html .= ($personel_entry['jenis_kelamin']!='') ? $personel_entry['jenis_kelamin'] : '-'; $html .= '</td>'; }
				if(in_array('tanggal_lahir', $column)) { $html .= '<td>'; $html .= ($personel_entry['tanggal_lahir']!='') ? format_date($personel_entry['tanggal_lahir'],'d-m-Y') : '-'; $html .= '</td>'; }
				if(in_array('nomor_induk', $column)) { $html .= '<td>'; $html .= ($personel_entry['nomor_induk']!='') ? $personel_entry['nomor_induk'] : '-'; $html .= '</td>'; }
				if(in_array('jabatan', $column)) { $html .= '<td>'; $html .= ($personel_entry['jabatan']!='') ? $personel_entry['jabatan'] : '-'; $html .= '</td>'; }
				if(in_array('nama_status', $column)) { $html .= '<td>'; $html .= ($personel_entry['id_status_pekerja']!='') ? $personel_entry['nama_status'] : '-'; $html .= '</td>'; }
				if(in_array('nama_level', $column)) { $html .= '<td>'; $html .= ($personel_entry['id_level']!='') ? $personel_entry['nama_level'] : '-'; $html .= '</td>'; }
				if(in_array('tahun_mulai_bekerja', $column)) { $html .= '<td>'; $html .= ($personel_entry['tahun_mulai_bekerja']!='') ? $personel_entry['tahun_mulai_bekerja'] : '-'; $html .= '</td>'; }
				if(in_array('tahun_mulai_profesional', $column)) { $html .= '<td>'; $html .= ($personel_entry['tahun_mulai_profesional']!='') ? $personel_entry['tahun_mulai_profesional'] : '-'; $html .= '</td>'; }
				if(in_array('nama_metode', $column)) { $html .= '<td>'; $html .= ($personel_entry['nama_metode']!='') ? $personel_entry['nama_metode'] : '-'; $html .= '</td>'; }
				if(in_array('handphone', $column)) { $html .= '<td>'; $html .= ($personel_entry['handphone']!='') ? $personel_entry['handphone'].'&nbsp;' : '-'; $html .= '</td>'; }
				if(in_array('email', $column)) { $html .= '<td>'; $html .= ($personel_entry['email']!='') ? $personel_entry['email'] : '-'; $html .= '</td>'; }
				if(in_array('unit_name', $column)) { $html .= '<td>'; $html .= ($personel_entry['unit_name']!='') ? anchor('admin/organization/units/view/'.$personel_entry['id_organization_unit'], $personel_entry['unit_name']) : '-'; $html .= '</td>'; }
				if(in_array('id_user', $column)) { $html .= '<td>'; $html .= ($personel_entry['id_user']!='') ? user_displayname($personel_entry['id_user']) : '-'; $html .= '</td>'; }
				if(in_array('supervisor_nama_lengkap', $column)) { $html .= '<td>'; $html .= ($personel_entry['supervisor_nama_lengkap']!='') ? $personel_entry['supervisor_nama_lengkap'] : '-'; $html .= '</td>'; }
				if(in_array('supervisor_level', $column)) { $html .= '<td>'; $html .= ($personel_entry['supervisor_level']!='') ? $personel_entry['supervisor_level'] : '-'; $html .= '</td>'; }
				if(in_array('supervisor_handphone', $column)) { $html .= '<td>'; $html .= ($personel_entry['supervisor_handphone']!='') ? $personel_entry['supervisor_handphone'] : '-'; $html .= '</td>'; }
				if(in_array('supervisor_email', $column)) { $html .= '<td>'; $html .= ($personel_entry['supervisor_email']!='') ? $personel_entry['supervisor_email'] : '-'; $html .= '</td>'; }
				if(in_array('address_office', $column)) { $html .= '<td>'; $html .= ($personel_entry['address_office']!='') ? $personel_entry['address_office'] : '-'; $html .= '</td>'; }
				if(in_array('address_home', $column)) { $html .= '<td>'; $html .= ($personel_entry['address_home']!='') ? $personel_entry['address_home'] : '-'; $html .= '</td>'; }
				if(in_array('phone_office', $column)) { $html .= '<td>'; $html .= ($personel_entry['phone_office']!='') ? $personel_entry['phone_office'] : '-'; $html .= '</td>'; }
				if(in_array('phone_home', $column)) { $html .= '<td>'; $html .= ($personel_entry['phone_home']!='') ? $personel_entry['phone_home'] : '-'; $html .= '</td>'; }
				if(in_array('created_on', $column)) { $html .= '<td>'; $html .= ($personel_entry['created_on']!='') ? $personel_entry['created_on'] : '-'; $html .= '</td>'; }
				if(in_array('created_by', $column)) { $html .= '<td>'; $html .= ($personel_entry['created_by']!='') ? user_displayname($personel_entry['created_by']) : '-'; $html .= '</td>'; }
				if(in_array('updated_on', $column)) { $html .= '<td>'; $html .= ($personel_entry['updated_on']!='') ? $personel_entry['updated_on'] : '-'; $html .= '</td>'; }

				if(!$for_download) {
					$html .= '<td class="actions">';
					
					if(group_has_role('human_resource', 'view_all_personel')){
						$html .= anchor('admin/human_resource/personel/view/' . $personel_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');
					}elseif(group_has_role('human_resource', 'view_own_unit_personel')){
						if(_check_permission_own_unit($personel_entry['id'])) {
							$html .= anchor('admin/human_resource/personel/view/' . $personel_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');
						}
					}elseif(group_has_role('human_resource', 'view_own_personel') AND $personel_entry['created_by'] == ci()->current_user->id){
						$html .= anchor('admin/human_resource/personel/view/' . $personel_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');
					}elseif(group_has_role('human_resource', 'view_own_user_personel') AND $personel_entry['id_user'] == ci()->current_user->id) {
						$html .= anchor('admin/human_resource/personel/view/' . $personel_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');
					}
					
					if(group_has_role('human_resource', 'edit_all_personel')){
						$html .= anchor('admin/human_resource/personel/edit/' . $personel_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
					}elseif(group_has_role('human_resource', 'edit_own_unit_personel')){
						if(_check_permission_own_unit($personel_entry['id'])) {
							$html .= anchor('admin/human_resource/personel/edit/' . $personel_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
						}
					}elseif(group_has_role('human_resource', 'edit_own_personel') AND $personel_entry['created_by'] == ci()->current_user->id){
						$html .= anchor('admin/human_resource/personel/edit/' . $personel_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
					}elseif(group_has_role('human_resource', 'edit_own_user_personel') AND $personel_entry['id_user'] == ci()->current_user->id) {
						$html .= anchor('admin/human_resource/personel/edit/' . $personel_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
					}

					if(group_has_role('human_resource', 'delete_all_personel')){
						$html .= anchor('admin/human_resource/personel/delete/' . $personel_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
					}elseif(group_has_role('human_resource', 'delete_own_unit_personel')){
						if(_check_permission_own_unit($personel_entry['id'])) {
							$html .= anchor('admin/human_resource/personel/delete/' . $personel_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
						}
					}elseif(group_has_role('human_resource', 'delete_own_personel')){
						if($personel_entry['created_by'] == ci()->current_user->id){
							$html .= anchor('admin/human_resource/personel/delete/' . $personel_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
						}
					}
					
					$html .= '</td>';
				}
			$html .= '</tr>';
			endforeach;
		$html .= '</tbody>
		</table>';

		if($for_download) {
			$simp = '';
			$simp .= "
			<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
			xmlns=\"http://www.w3.org/TR/REC-html40\">
			 
			<head>
			    <meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
			    <meta name=ProgId content=Excel.Sheet>
			    <meta name=Generator content=\"Microsoft Excel 11\">
			    <title>Data Personel</title>
			</head>
			<body>";

			$simp .= $html;
			$simp .= "
				</body>
				</html>";

			header("Content-Disposition: attachment; filename=\"Data Personel.xls\"");
			header("Content-Type: application/vnd.ms-excel");
			header('Cache-Control: max-age=0');

			echo $simp;
		} else {
			echo $html;
		}
	}

	function _check_permission_own_unit($id_user_personel = 0) {
		$memberships_current_user = ci()->organization->get_membership_by_user(ci()->current_user->id);
		$memberships_personel = ci()->personel_m->get_personel_by_id($id_user_personel);
		
		$unit_personel = $memberships_personel['id_organization_unit'];
		
		if(count($memberships_current_user['entries'])!=0) {
			if (count($memberships_current_user['entries']) < 1) {
				return false;
			}

			$unit_current_user = $memberships_current_user['entries'];

			foreach ($unit_current_user as $key => $value) {
				$unit_lists[] = $value['membership_unit']['id'];
			}

			if (!in_array($unit_personel, $unit_lists)) {
				return false;
			}
		}

		return true;
	}

	function get_supervisor($id = 0) {
		ci()->load->model('personel_m');
		if(ci()->input->get('params')) {
			$params['nama_lengkap'] = ci()->input->get('params');
		} else {
			$params = NULL;
		}

		$skip_personel = NULL;
		if($id != 0 OR $id != "") {
			$skip_personel = array($id);
		}

		$status_pekerja = ci()->personel_m->get_personel(NULL, $params, NULL, 'default_human_resource_level.ordering_count asc, default_human_resource_personel.nama_lengkap asc', $skip_personel);
		$data_json = array();
		foreach ($status_pekerja as $value) {
			$data_json[] = array('id'=>$value['id'], 'text'=>$value['nama_lengkap'], 'group'=>$value['nama_level']);
		}
		return json_encode($data_json);
	}

	function get_organization_unit() {

		if(ci()->input->get('params')) {
			$params['organization_units.unit_name'] = ci()->input->get('params');
		} else {
			$params = NULL;
		}

		ci()->load->model('organization/units_m');
		$units = ci()->units_m->get_units($params);

		foreach ($units as $k => $v) {
			$pos_unit = myfunction($units, 'id', $v['id']);
			$pos_parent = myfunction($units, 'id', $v['parent_id']);
		
			if($v['parent_id']!='') {
				$child[$pos_parent][] = $v;
			} else {
				$parent[] = $v;
			}
		}

		foreach ($parent as $key => $value) {
			$pos = array_search($value, $parent);
			if(array_key_exists($key, $child)) {
				array_splice($parent, $pos+1, 0, $child[$key]);
			}
		}

		$data_json = array();
		foreach ($parent as $value) {
			$text = '';
			for ($i=0; $i < $value['type_level']; $i++) { 
				$text .= '&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			$data_json[] = array('id'=>$value['id'], 'text'=>$text.$value['unit_name']);
		}
		return json_encode($data_json);
	}

	function myfunction($units, $field, $value)
	{
	   foreach($units as $key => $unit)
	   {
	      if ($unit[$field] === $value )
	         return $key;
	   }
	   return false;
	}

	function get_user($exception_id = '') {
		ci()->load->model('personel_m');
		if(ci()->input->get('params')) {
			$params['display_name'] = ci()->input->get('params');
		} else {
			$params = NULL;
		}

		if(!group_has_role('human_resource','create_personel') AND group_has_role('human_resource','create_own_user_personel')) {
			$params['custom'] = 'user_id = '.ci()->current_user->id;
		}

		$users = ci()->personel_m->get_personel_account($params, $exception_id);
		
		$data_json = array();
		foreach ($users as $value) {
			$data_json[] = array('id'=>$value['user_id'], 'text'=>$value['display_name'], 'group'=>$value['group_description']);
		}
		return json_encode($data_json);
	}

	function get_status_pekerja() {
		ci()->load->model('status_pekerja_m');
		$status_pekerja = ci()->status_pekerja_m->get_status_pekerja();
		
		return json_encode($status_pekerja);
	}

	function get_level() {
		ci()->load->model('level_m');
		$level = ci()->level_m->get_level();
		
		return json_encode($level);
	}

	function get_metode_rekrutmen() {
		ci()->load->model('metode_rekrutmen_m');
		$metode_rekrutmen = ci()->metode_rekrutmen_m->get_metode_rekrutmen();
		
		return json_encode($metode_rekrutmen);
	}

	function get_tahun_mulai_bekerja() {
		ci()->load->model('personel_m');
		$tahun = ci()->personel_m->get_tahun_mulai_bekerja();
		
		return json_encode($tahun);
	}

	function get_tahun_mulai_profesional() {
		ci()->load->model('personel_m');
		$tahun = ci()->personel_m->get_tahun_mulai_profesional();
		
		return json_encode($tahun);
	}
?>