<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Status_pekerja model
 *
 * @author Aditya Satrya
 */
class Status_pekerja_m extends MY_Model {
	
	public function get_status_pekerja($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$this->db->order_by('ordering_count');
		$query = $this->db->get('default_human_resource_status_pekerja');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_status_pekerja_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_human_resource_status_pekerja');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_status_pekerja()
	{
		return $this->db->count_all('human_resource_status_pekerja');
	}
	
	public function delete_status_pekerja_by_id($id, $order)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('default_human_resource_status_pekerja');

		if($res) {
			$this->db->query('update default_human_resource_status_pekerja set ordering_count = ordering_count-1 where ordering_count>'.$order);
		}
	}
	
	public function insert_status_pekerja($values)
	{
		$this->db->select_max('ordering_count');
		$max = $this->db->get('default_human_resource_status_pekerja')->row();
		
		$values['ordering_count'] = $max->ordering_count+1;
		return $this->db->insert('default_human_resource_status_pekerja', $values);
	}
	
	public function update_status_pekerja($values, $row_id)
	{
		$this->db->where('id', $row_id);
		return $this->db->update('default_human_resource_status_pekerja', $values); 
	}

	public function move_status_pekerja($id, $order, $direction) {
		if($direction=='up') {
			$new_order = $order--;
		} elseif ($direction=='down') {
			$new_order = $order++;
		}

		$this->db->where('ordering_count', $order);
		$this->db->update('default_human_resource_status_pekerja', array('ordering_count'=>$new_order));

		$this->db->where('id', $id);
		$this->db->update('default_human_resource_status_pekerja', array('ordering_count'=>$order));

		return true;
	}
	
}