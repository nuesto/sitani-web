<div class="page-header">
	<h1>
		<span><?php echo lang('gallery:album:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('gallery/album/index'); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('gallery:back') ?>
		</a>

		<?php if(group_has_role('gallery', 'edit_all_album')){ ?>
			<a href="<?php echo site_url('gallery/album/edit/'.$album['id']); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('gallery', 'edit_own_album')){ ?>
			<?php if($album->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('gallery/album/edit/'.$album['id']); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('gallery', 'delete_all_album')){ ?>
			<a href="<?php echo site_url('gallery/album/delete/'.$album['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('gallery', 'delete_own_album')){ ?>
			<?php if($album->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('gallery/album/delete/'.$album['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name">ID</div>
		<div class="entry-detail-value"><?php echo $album['id']; ?></div>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:nama'); ?></div>
		<?php if(isset($album['nama'])){ ?>
		<div class="entry-detail-value"><?php echo $album['nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:slug'); ?></div>
		<?php if(isset($album['slug'])){ ?>
		<div class="entry-detail-value"><?php echo $album['slug']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:image'); ?></div>
		<?php if(isset($album['image'])){ ?>
		<div class="entry-detail-value"><?php echo $album['image']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:created'); ?></div>
		<?php if(isset($album['created'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($album['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:updated'); ?></div>
		<?php if(isset($album['updated'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($album['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:created_by'); ?></div>
		<div class="entry-detail-value"><?php echo user_displayname($album['created_by'], true); ?></div>
	</div>
</div>