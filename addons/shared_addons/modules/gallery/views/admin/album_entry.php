<style type="text/css">
	.box {
		display: block;
		width: 300px;
		height: 250px;
		margin: 5px;
		float: left;
	}
</style>
<div class="page-header">
	<h1><?php echo lang('gallery:nama'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/gallery/album/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('gallery:back') ?>
		</a>

		<?php if(group_has_role('gallery', 'view_all_image')){ 
			echo anchor('admin/gallery/image/index/' . $album['id'] . $uri, lang('global:view').' '.lang('gallery:image').' ('.$album['count_image'].')', 'class="btn btn-sm btn-yellow view"');
		}elseif(group_has_role('gallery', 'view_own_image')){ ?>
			<?php if($album->created_by_user_id == $this->current_user->id){ 
				echo anchor('admin/gallery/image/index/' . $album['id'], lang('global:view').' '.lang('gallery:image').' ('.$album['count_image'].')', 'class="btn btn-sm btn-yellow view"');
			} ?>
		<?php } ?>

		<?php if(group_has_role('gallery', 'edit_all_album')){ ?>
			<a href="<?php echo site_url('admin/gallery/album/edit/'.$album['id'] . $uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('gallery', 'edit_own_album')){ ?>
			<?php if($album->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/gallery/album/edit/'.$album->id); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('gallery', 'delete_all_album')){ ?>
			<a href="<?php echo site_url('admin/gallery/album/delete/'.$album['id'] . $uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('gallery', 'delete_own_album')){ ?>
			<?php if($album->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/gallery/album/delete/'.$album['id'] . $uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:nama'); ?></div>
		<?php if(isset($album['nama'])){ ?>
		<div class="entry-detail-value"><?php echo $album['nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:slug'); ?></div>
		<?php if(isset($album['slug'])){ ?>
		<div class="entry-detail-value"><?php echo $album['slug']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:image'); ?></div>
		<?php if(isset($album['image'])){ ?>
		<div class="entry-detail-value"><img src="<?php echo base_url(); ?>files/large/<?php echo $album['image']; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;"/></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:created'); ?></div>
		<?php if(isset($album['created'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($album['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:updated'); ?></div>
		<?php if(isset($album['updated'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($album['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:created_by'); ?></div>
		<div class="entry-detail-value"><?php echo user_displayname($album['created_by'], true); ?></div>
	</div>
</div>