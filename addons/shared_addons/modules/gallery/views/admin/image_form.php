<div class="page-header">
	<h1><?php echo lang('gallery:image:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">
<?php
$supported_lang = get_supported_lang();
foreach ($supported_lang as $sl) {
	$tmp_lang = explode("=", $sl);
	$lang_list[$tmp_lang[0]] = $tmp_lang[1];
}
$public_lang = explode(",", $this->settings->get('site_public_lang'));
array_unshift($public_lang, $this->settings->get('site_lang'));
$public_lang = array_unique($public_lang);
foreach ($public_lang as $pl) {
	$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
}

echo form_hidden('available_lang',implode(",", $public_lang));
?>
<?php
foreach ($available_lang as $key => $al) { 
	$lang_code = '';
	if(count($available_lang)>1) {
		echo '<h2>'.$al['name'].'</h2>';
		$lang_code = '_'.$al['code'];
	}
	?>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="title"><?php echo lang('gallery:title'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('title'.$lang_code) != NULL){
					$value = $this->input->post('title'.$lang_code);
				}elseif($mode == 'edit'){
					$value = (isset($fields['title'.$lang_code])) ? $fields['title'.$lang_code] : '';
				}
			?>
			<input name="title<?php echo $lang_code; ?>" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>
<?php } ?>

<?php
if(count($available_lang)>1) {
	echo '<h2>General</h2>';
}
?>
	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="image"><?php echo lang('gallery:image'); ?></label>

		<div class="col-xs-4">
			<?php 
				$value = NULL;
				if($this->input->post('image') != NULL){
					$value = $this->input->post('image');
				}elseif($mode == 'edit'){
					$value = $fields['image'];
				}
			?>
			
			<?php if(!empty($value)){ ?>
			<img src="<?php echo base_url(); ?>files/large/<?php echo $value; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;" />
			<?php } ?>
			<input type="hidden" name="id_image" value="<?php echo $value; ?>">
			<input name="image" id="foto" type="file" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="id_album"><?php echo lang('gallery:id_album'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = ($id_album!=0) ? $id_album : NULL;
				
				if($this->input->post('id_album') != NULL){
					$value = $this->input->post('id_album');
				}elseif($mode == 'edit'){
					$value = $fields['id_album'];
				}
			?>
			
			<select name="id_album" class="col-xs-10 col-sm-5" id="">
				<option value="">-- <?php echo lang('gallery:album:choose'); ?> --</option>
				<?php
				foreach ($album as $key => $a) {
					$selected = ($a['id']==$value) ? 'selected' : '';
					echo '<option value="'.$a['id'].'" '.$selected.'>'.$a['nama'].'</option>';
				}
				?>
			</select>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">
$(document).ready(function() {
	$('#foto').ace_file_input({
		no_file:'No File ...',
		btn_choose:'Choose',
		btn_change:'Change',
		droppable:true,
		onchange:null,
		thumbnail:'large', //| true | large
		whitelist:'gif|png|jpg|jpeg',
		blacklist:'exe|php|html|js|css'
		//onchange:''
		//
	});
});
</script>