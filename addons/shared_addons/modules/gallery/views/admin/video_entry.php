<div class="page-header">
	<h1><?php echo $video['title']; ?></h1>

	<div class="btn-group content-toolbar">

		<a href="<?php echo site_url('admin/gallery/video/index/'.$video['id_album'].$uri); ?>" class="btn btn-sm btn-yellow">
			
		<span class="visible-xs"><i class="icon-arrow-left"></i></span>

			<span class="hidden-xs">
				<i class="icon-arrow-left"></i>
				<?php echo lang('gallery:back') ?>
			</span>
		</a>

		<?php if(group_has_role('gallery', 'edit_all_video')){ ?>
			<a href="<?php echo site_url('admin/gallery/video/edit/'.$video['id'].'/'.$video['id_album'].$uri); ?>" class="btn btn-sm btn-yellow">

				<span class="visible-xs"><i class="icon-edit"></i></span>

				<span class="hidden-xs">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</span>
			</a>
		<?php }elseif(group_has_role('gallery', 'edit_own_video')){ ?>
			<?php if($video->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/gallery/video/edit/'.$video->id.'/'.$video['id_album'].$uri); ?>" class="btn btn-sm btn-yellow">
					
				<span class="visible-xs"><i class="icon-edit"></i></span>

					<span class="hidden-xs">
						<i class="icon-edit"></i>
						<?php echo lang('global:edit') ?>
					</span>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('gallery', 'delete_all_video')){ ?>
			<a href="<?php echo site_url('admin/gallery/video/delete/'.$video['id'].'/'.$video['id_album'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				
				<span class="visible-xs"><i class="icon-trash"></i></span>

				<span class="hidden-xs">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</span>
			</a>
		<?php }elseif(group_has_role('gallery', 'delete_own_video')){ ?>
			<?php if($video->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/gallery/video/delete/'.$video['id'].'/'.$video['id_album'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					
					<span class="visible-xs"><i class="icon-trash"></i></span>

					<span class="hidden-xs">
						<i class="icon-trash"></i>
						<?php echo lang('global:delete') ?>
					</span>
				</a>
			<?php } ?>
		<?php } ?>

	</div>
</div>

<div>
	<div class="row theater">
		<?php if(isset($video['video_url'])){ ?>
			<?php $retval = json_decode($video['video_url'], true); ?>
		<div class="theater-screen">
			<div class="videowrapper">
				<div id="player"></div>
			</div>
		</div>

		<?php }else{ ?>

		<div class="col-xs-12 col-sm-8 data-detail-value"><?php dump($video); ?></div>

		<?php } ?>

		<script>
		  // This code loads the IFrame Player API code asynchronously.
		  var tag = document.createElement('script');

		  tag.src = "https://www.youtube.com/iframe_api";
		  var firstScriptTag = document.getElementsByTagName('script')[0];
		  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		  // This function creates an <iframe> (and YouTube player)
		  // after the API code downloads.
		  var player;
		  function onYouTubeIframeAPIReady() {
			player = new YT.Player('player', {
			  width: '100%',
			  videoId: '<?php echo $retval['video_id'] ?>',
			  playerVars: {
				  'rel': 0,
				  'modestbranding': 0,
				  'showinfo': 0
			  },
			  events: {}
			});
		  }
		</script>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-8">
			<h4 class="smaller">
				<i class="icon-edit bigger-110"></i>
				<?php echo lang('gallery:description'); ?>
			</h4>

			<?php
				if(isset($video['description'])){
					echo nl2br($video['description']);
				}else{
					echo '-';
				}
			?>
		</div>

		<div class="col-xs-12 col-sm-4">
			<h4 id="info-header" class="smaller">
				<i class="icon-info bigger-110"></i>
				<?php echo lang('gallery:info'); ?>
			</h4>

			<div class="data-detail first">
				<div class="col-xs-12 col-sm-4 data-detail-label"><?php echo lang('gallery:album:singular'); ?></div>
				<?php if(isset($video['album_nama'])){ ?>
				<div class="col-xs-12 col-sm-8 data-detail-value"><?php echo '<a href="'.site_url('admin/gallery/video/index/'.$video['id_album']).'">'.$video['album_nama'].'</a>'; ?></div>
				<?php }else{ ?>
				<div class="col-xs-12 col-sm-8 data-detail-value">-</div>
				<?php } ?>
			</div>

			<div class="data-detail">
				<div class="col-xs-12 col-sm-4 data-detail-label"><?php echo lang('gallery:created'); ?></div>
				<?php if(isset($video['created'])){ ?>
				<div class="col-xs-12 col-sm-8 data-detail-value"><?php echo format_date($video['created'], 'd-m-Y G:i'); ?></div>
				<?php }else{ ?>
				<div class="col-xs-12 col-sm-8 data-detail-value">-</div>
				<?php } ?>
			</div>

			<div class="data-detail">
				<div class="col-xs-12 col-sm-4 data-detail-label"><?php echo lang('gallery:updated'); ?></div>
				<?php if(isset($video['updated'])){ ?>
				<div class="col-xs-12 col-sm-8 data-detail-value"><?php echo format_date($video['updated'], 'd-m-Y G:i'); ?></div>
				<?php }else{ ?>
				<div class="col-xs-12 col-sm-8 data-detail-value">-</div>
				<?php } ?>
			</div>

			<div class="data-detail">
				<div class="col-xs-12 col-sm-4 data-detail-label"><?php echo lang('gallery:created_by'); ?></div>
				<div class="col-xs-12 col-sm-8 data-detail-value"><?php echo user_displayname($video['created_by'], true); ?></div>
			</div>	
		</div>
	</div>
</div>
