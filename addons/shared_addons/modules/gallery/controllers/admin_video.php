<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gallery Module
 *
 * Album & video
 *
 */
class Admin_video extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'video';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'access_video_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('gallery');		
		$this->load->model('album_m');
		$this->load->model('video_m');
    }

    /**
	 * List all video
     *
     * @return	void
     */
    public function index($id_album = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_video') AND ! group_has_role('gallery', 'view_own_video')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// show the only album on this site
		$data['album'] = $this->album_m->get_album();
		if(count($data['album'])==1) {
			$id_album = $data['album'][0]['id'];
		}

		// -------------------------------------
		// Pagination
		// -------------------------------------

		$params['id_album'] = $id_album;
		if($this->input->get('f-id_album')) {
			$params['id_album'] = $this->input->get('f-id_album');
		}

		$pagination_config['base_url'] = base_url(). 'admin/gallery/video/index/'.$params['id_album'];
		$pagination_config['uri_segment'] = 6;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->video_m->count_all_video($params);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(6);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		$data['id_album'] = $params['id_album'];
		
        $data['video']['entries'] = $this->video_m->get_video($pagination_config, $params);
		$data['video']['total'] = $pagination_config['total_rows'];
		$data['video']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		if(count($data['video']['entries'])==0 AND ($this->uri->segment(6) != NULL AND $this->uri->segment(6)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(6), '', $data['uri']);
			if($this->uri->segment(6) != '' AND $this->uri->segment(6)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(6)-$pagination_config['per_page'];
				redirect('admin/gallery/video/index/'.$params['id_album'].$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/gallery/video/index/'.$params['id_album'].$data['uri']);
				}
			}
		}

		$album_entry = $this->album_m->get_album_by_id($params['id_album']);

		Asset::js('jquery.colorbox-min.js');
		Asset::css('colorbox.css');
		
        $this->template->title(lang('gallery:video:plural'))
			->set_breadcrumb('Home', '/admin');
		if(group_has_role('gallery','access_album_backend')) {
			$this->template->set_breadcrumb(lang('gallery:album:plural'), '/admin/album');
		} else {
			$this->template->set_breadcrumb(lang('gallery:album:plural'), '');
		}
		if (isset($album_entry)) {
			$this->template->set_breadcrumb($album_entry['nama']);
		}
		$this->template->set_breadcrumb(lang('gallery:video:plural'))
			->append_css('module::gallery.css')
			->build('admin/video_index', $data);
    }
	
	/**
     * Display one video
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_video') AND ! group_has_role('gallery', 'view_own_video')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['video'] = $this->video_m->get_video_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_video')){
			if($data['video']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}


		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('gallery:video:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('gallery', 'view_all_album%') OR group_has_role('gallery', 'view_own_album')){
			$this->template->set_breadcrumb($data['video']['album_nama'], '/admin/gallery/video/index/'.$data['video']['id_album']);
			$this->template->set_breadcrumb(lang('gallery:video:plural'), '/admin/gallery/video/index/'.$data['video']['id_album']);
		}else{
			$this->template->set_breadcrumb($data['video']['album_nama']);
			$this->template->set_breadcrumb(lang('gallery:video:plural'));
		}

		$this->template->set_breadcrumb($data['video']['title'])
			->append_css('module::gallery.css')
			->build('admin/video_entry', $data);
    }
	
	/**
     * Create a new video entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($id_album = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'create_video')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// if($id_album==0) {
		// 	$this->session->set_flashdata('error', lang('cp:access_denied'));
		// 	redirect('admin/gallery/album/index');	
		// }

		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_video('new')){	
				$this->session->set_flashdata('success', lang('gallery:video:submit_success'));
				$id_album = $this->input->post('id_album');
				$data['uri'] = str_replace("f-id_album=".$this->input->get('f-id_album'), "f-id_album=".$id_album, $data['uri']);
				redirect('admin/gallery/video/index/'.$id_album.$data['uri']);
			}else{
				$data['messages']['error'] = lang('gallery:video:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/gallery/video/index/'.$id_album;

		$data['id_album'] = $id_album;
		$data['album'] = $this->album_m->get_album();
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:video:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('gallery:video:plural'), '/admin/gallery/video/index')
			->set_breadcrumb(lang('gallery:video:new'))
			->append_css('module::gallery.css')
			->build('admin/video_form', $data);
    }
	
	/**
     * Edit a video entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the video to the be deleted.
     * @return	void
     */
    public function edit($id = 0, $id_album = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'edit_all_video') AND ! group_has_role('gallery', 'edit_own_video')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('gallery', 'edit_all_video')){
			$entry = $this->video_m->get_video_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		$data['uri'] = get_query_string(7);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_video('edit', $id)){	
				$this->session->set_flashdata('success', lang('gallery:video:submit_success'));
				$id_album = $this->input->post('id_album');
				$data['uri'] = str_replace("f-id_album=".$this->input->get('f-id_album'), "f-id_album=".$id_album, $data['uri']);
				redirect('admin/gallery/video/index/'.$id_album.$data['uri']);
			}else{
				$data['messages']['error'] = lang('gallery:video:submit_failure');
			}
		}
		
		$data['fields'] = $this->video_m->get_video_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/gallery/video/index/'.$id_album.$data['uri'];
		$data['entry_id'] = $id;

		$translation = $this->video_m->get_translation($id);
		foreach ($translation as $trans) {
			$tmp_translation = json_decode($trans->translation,true);
			$data['fields'] = array_merge($data['fields'],$tmp_translation);
		}

		$id_album = $data['fields']['id_album'];

		$data['id_album'] = $id_album;
		$data['album'] = $this->album_m->get_album();
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:video:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('gallery', 'view_all_album') OR group_has_role('gallery', 'view_own_album')){
			$this->template->set_breadcrumb(lang('gallery:video:plural'), '/admin/gallery/video/index/'.$id_album)
			->set_breadcrumb(lang('gallery:video:view'), '/admin/gallery/video/view/'.$id.$data['uri']);
		}else{
			$this->template->set_breadcrumb(lang('gallery:video:plural'))
			->set_breadcrumb(lang('gallery:video:view'));
		}

		$this->template->set_breadcrumb(lang('gallery:video:edit'))
			->append_css('module::gallery.css')
			->build('admin/video_form', $data);
    }
	
	/**
     * Delete a video entry
     * 
     * @param   int [$id] The id of video to be deleted
     * @return  void
     */
    public function delete($id = 0, $id_album = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'delete_all_video') AND ! group_has_role('gallery', 'delete_own_video')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('gallery', 'delete_all_video')){
			$entry = $this->video_m->get_video_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->video_m->delete_video_by_id($id);
        $this->session->set_flashdata('error', lang('gallery:video:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        $data['uri'] = get_query_string(7);
        redirect('admin/gallery/video/index/'.$id_album.$data['uri']);
    }
	
	/**
     * Insert or update video entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_video($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		// language
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_unshift($public_lang, $this->settings->get('site_lang'));
		$public_lang = array_unique($public_lang);
		foreach ($public_lang as $pl) {
			$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
		}
		if(count($available_lang) > 1) {
			// for additional lang
			foreach ($available_lang as $al) {
				$this->form_validation->set_rules('title_'.$al['code'], lang('gallery:title').' '.$al['name'], 'required|xss_clean|min_length[5]|max_length[250]');
				$this->form_validation->set_rules('description_'.$al['code'], lang('gallery:description').' '.$al['name'], 'xss_clean');
			}
		} else {
			$this->form_validation->set_rules('title', lang('gallery:title'), 'required|xss_clean|min_length[5]|max_length[250]');
			$this->form_validation->set_rules('description', lang('gallery:description'), 'xss_clean');
		}

		$this->form_validation->set_rules('id_album', lang('gallery:id_album'), 'required|xss_clean');
		$this->form_validation->set_rules('video_url', lang('gallery:video_url'), 'required');		
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{

			$values['video_url'] = $values['video_url_url'];
			unset($values['video_url_url']);
			
			if ($method == 'new')
			{
				$result = $this->video_m->insert_video($values);
				
			}
			else
			{
				$result = $this->video_m->update_video($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}