<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gallery Module
 *
 * Album & Image
 *
 */
class Admin_album extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'album';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'access_album_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('gallery');		
		$this->load->model('album_m');
		$this->load->model('image_m');
    }

    /**
	 * List all album
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_album') AND ! group_has_role('gallery', 'view_own_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/gallery/album/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->album_m->count_all_album();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['album']['entries'] = $this->album_m->get_album($pagination_config);
		$data['album']['total'] = $pagination_config['total_rows'];
		$data['album']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['album']['entries'])==0 AND ($this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/gallery/album/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/gallery/album/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('gallery:album:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('gallery:album:plural'))
			->build('admin/album_index', $data);
    }
	
	/**
     * Display one album
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_album') AND ! group_has_role('gallery', 'view_own_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['album'] = $this->album_m->get_album_by_id($id);

        // -------------------------------------
		// Get our entry of images.
		// -------------------------------------
        $data['image']['entries'] = $this->image_m->get_image_by_album($id);
		$data['image']['total'] = count($data['image']['entries']);
		$data['image']['pagination'] = '';
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_album')){
			if($data['album']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('gallery:album:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('gallery', 'view_all_album%') OR group_has_role('gallery', 'view_own_album')){
			$this->template->set_breadcrumb(lang('gallery:album:plural'), '/admin/gallery/album/index');
		}

		$this->template->set_breadcrumb(lang('gallery:album:view'))
			->build('admin/album_entry', $data);
    }
	
	/**
     * Create a new album entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'create_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_album('new')){	
				$this->session->set_flashdata('success', lang('gallery:album:submit_success'));				
				redirect('admin/gallery/album/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('gallery:album:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/gallery/album/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:album:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('gallery', 'view_all_album%') OR group_has_role('gallery', 'view_own_album')){
			$this->template->set_breadcrumb(lang('gallery:album:plural'), '/admin/gallery/album/index');
		}

		$this->template->set_breadcrumb(lang('gallery:album:new'))
			->build('admin/album_form', $data);
    }
	
	/**
     * Edit a album entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the album to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'edit_all_album') AND ! group_has_role('gallery', 'edit_own_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('gallery', 'edit_all_album')){
			$entry = $this->album_m->get_album_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_album('edit', $id)){	
				$this->session->set_flashdata('success', lang('gallery:album:submit_success'));				
				redirect('admin/gallery/album/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('gallery:album:submit_failure');
			}
		}
		
		$data['fields'] = $this->album_m->get_album_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/gallery/album/index/'.$data['uri'];
		$data['entry_id'] = $id;

		$translation = $this->album_m->get_translation($id);
		foreach ($translation as $trans) {
			$tmp_translation = json_decode($trans->translation,true);
			$data['fields'] = array_merge($data['fields'],$tmp_translation);
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:album:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('gallery', 'view_all_album') OR group_has_role('gallery', 'view_own_album')){
			$this->template->set_breadcrumb(lang('gallery:album:plural'), '/admin/gallery/album/index')
			->set_breadcrumb(lang('gallery:album:view'), '/admin/gallery/album/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('gallery:album:plural'))
			->set_breadcrumb(lang('gallery:album:view'));
		}

		$this->template->set_breadcrumb(lang('gallery:album:edit'))
			->build('admin/album_form', $data);
    }
	
	/**
     * Delete a album entry
     * 
     * @param   int [$id] The id of album to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'delete_all_album') AND ! group_has_role('gallery', 'delete_own_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('gallery', 'delete_all_album')){
			$entry = $this->album_m->get_album_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->album_m->delete_album_by_id($id);
        $this->session->set_flashdata('error', lang('gallery:album:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/gallery/album/index'.$data['uri']);
    }
	
	/**
     * Insert or update album entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_album($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		// language
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_unshift($public_lang, $this->settings->get('site_lang'));
		$public_lang = array_unique($public_lang);
		foreach ($public_lang as $pl) {
			$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
		}
		if(count($available_lang) > 1) {
			// for additional lang
			foreach ($available_lang as $al) {
				$this->form_validation->set_rules('nama_'.$al['code'], lang('gallery:nama').' '.$al['name'], 'required|xss_clean|min_length[5]|max_length[50]');
				$this->form_validation->set_rules('slug_'.$al['code'], lang('gallery:slug').' '.$al['name'], 'required|xss_clean|alpha_dash|max_length[50]');
			}
		} else {
			$this->form_validation->set_rules('nama', lang('gallery:nama'), 'required|xss_clean|min_length[5]|max_length[50]');
			$this->form_validation->set_rules('slug', lang('gallery:slug'), 'required|xss_clean|max_length[50]');
		}
		$this->form_validation->set_rules('image','Image','callback_check_image');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			$values['image'] = $_POST['id_image'];
			if ($method == 'new')
			{
				$result = $this->album_m->insert_album($values);				
			}
			else
			{
				$result = $this->album_m->update_album($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function check_image($image) {
		$this->load->library('files/files');
		$id_folder = Files::get_id_by_name('Gallery Album');
		$image_id = null;
		if($_FILES['image']['name']!=''){
			$allowed_type = array('bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'png', 'tiff', 'tif', 'BMP', 'GIF', 'JPEG', 'JPG', 'PNG');
			$data = Files::upload($id_folder, $_FILES['image']['name'], 'image',false,false,false,implode("|", $allowed_type));

			if($data['status']===false) {
				$this->form_validation->set_message('check_image','<strong>Image</strong> '.strip_tags($data['message']));
				return false;
			} else {
				$image_id = $data['data']['id'];
				$_POST['id_image'] = $image_id;
				return true;
			}
		} else {
			if($this->input->post('id_image')=='') {
				$this->form_validation->set_message('check_image','<strong>Image</strong> '.lang('gallery:required'));
				return false;
			}
		}
	}

}