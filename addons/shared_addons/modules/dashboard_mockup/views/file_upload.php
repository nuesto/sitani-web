<!-- start website traffic widget -->
<div class="widget-body">
	<div id="file-upload-chart" style="height: 250px;"></div>
</div>
<div class="widget-foot no-background">
  <div class="row row-merge">
		<div class="col-xs-3 text-center border-right">
			<h4 class="no-margin">1232</h4>
			<small class="text-muted">Visitors</small>
		</div>
		<div class="col-xs-3 text-center border-right">
			<h4 class="no-margin">5421</h4>
			<small class="text-muted">Orders</small>
		</div>
		<div class="col-xs-3 text-center border-right">
			<h4 class="no-margin">3021</h4>
			<small class="text-muted">Tickets</small>
		</div>
		<div class="col-xs-3 text-center">
			<h4 class="no-margin">7098</h4>
			<small class="text-muted">Customers</small>
		</div>
	</div>
</div>
<!-- end website traffic widget -->

<script type="text/javascript">
	//Morris Chart
	var donutChart = Morris.Donut({
	  element: 'file-upload-chart',
	  data: [
		{label: "Download Sales", value: 1236},
		{label: "In-Store Sales", value: 3091},
		{label: "Mail-Order Sales", value: 2781}
	  ],
	  colors: ['#f3ce85','#65CEA7' ,'#FC8675']
	});
</script>