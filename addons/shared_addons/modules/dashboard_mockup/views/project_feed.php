<!-- start browser widget -->
<div class="widget-body no-padding">
	<ul class="feed">
		<li class="clearfix">
			<div class="img">
				<?php echo Asset::img('module::avatar.png', 'Avatar') ?>
			</div>
			<div class="title">
				<a href="#">Robert Downey Jr.</a> took photo with Instagram.
			</div>
			<div class="post-time">Today 5:22 pm</div>
			<div class="time-ago"><i class="fa fa-clock-o"></i> 5 min.</div>
		</li>
		<li class="clearfix">
			<div class="img">
				<?php echo Asset::img('module::avatar.png', 'Avatar') ?>
			</div>
			<div class="title">
				<a href="#">Adriana Lima</a> checked in at Las Vegas Oscars
			</div>
			<div class="post-time">Yesterday 11:38 am</div>
			<div class="photos clearfix">
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
			</div>
			<div class="time-ago"><i class="fa fa-clock-o"></i> 9 hours.</div>
		</li>
		<li class="clearfix">
			<div class="img">
				<?php echo Asset::img('module::avatar.png', 'Avatar') ?>
			</div>
			<div class="title">
				<a href="#">Emma Watson</a> commented on Scarlett Johansson's video.
			</div>
			<div class="post-time">Today 11:59 pm</div>
				<div class="time-ago"><i class="fa fa-clock-o"></i> 28 min.</div>
			</li>
		<li class="clearfix">
			<div class="img">
				<?php echo Asset::img('module::avatar.png', 'Avatar') ?>
			</div>
			<div class="title">
				<a href="#">Ryan Gosling</a> likes Ryan Gosling's link on his own Timeline.
			</div>
			<div class="post-time">Yesterday 9:43 pm</div>
			<div class="photos clearfix">
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
			</div>
			<div class="time-ago"><i class="fa fa-clock-o"></i> 5 hours.</div>
		</li>
		<li class="clearfix">
			<div class="img"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></div>
			<div class="title">
				<a href="#">Mila Kunis</a> invited you to his birthday party at her mansion.
			</div>
			<div class="post-time">Yesterday 7:50 am</div>
			<div class="time-ago"><i class="fa fa-clock-o"></i> 9 hours.</div>
		</li>
		<li class="clearfix">
			<div class="img"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></div>
			<div class="title">
				<a href="#">Emma Watson</a> commented on Scarlett Johansson's video.
			</div>
			<div class="post-time">Today 11:59 pm</div>
			<div class="time-ago"><i class="fa fa-clock-o"></i> 28 min.</div>
		</li>
		<li class="clearfix">
			<div class="img"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></div>
			<div class="title">
				<a href="#">Adriana Lima</a> checked in at Las Vegas Oscars
			</div>
			<div class="post-time">Yesterday 11:38 am</div>
			<div class="photos clearfix">
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
				<div class="item">
					<a href="#"><?php echo Asset::img('module::avatar.png', 'Avatar') ?></a>
				</div>
			</div>
			<div class="time-ago"><i class="fa fa-clock-o"></i> 9 hours.</div>
		</li>
	</ul>
</div>
<!-- end browser widget -->