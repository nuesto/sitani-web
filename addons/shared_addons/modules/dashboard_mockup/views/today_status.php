<!-- start today status widget -->
<div class="widget-body">
	<!-- Visitors, pageview, bounce rate, etc., Sparklines plugin used -->
	<ul class="current-status">
	  <li>
			<span id="status1" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9">12,23,3,12,5,3,12,11,23,3,12,5,4,12</span>
			<span class="bold">Visits : 2000</span>
			<i class="fa fa-arrow-up green"></i>
	  </li>
	  <li>
			<span id="status2" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9">12,23,3,12,5,3,12,11,23,3,12,5,4,12</span>
			<span class="bold">Unique Visitors : 1,345</span>
			<i class="fa fa-arrow-down red"></i>
	  </li>
	  <li>
			<span id="status3" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9">12,23,3,12,5,3,12,11,23,3,12,5,4,12</span>
			<span class="bold">Pageviews : 2000</span>
			<i class="fa fa-arrow-up green"></i>
	  </li>
	  <li>
			<span id="status4" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9">12,23,3,12,5,3,12,11,23,3,12,5,4,12</span>
			<span class="bold">Pages / Visit : 2000</span>
			<i class="fa fa-arrow-down red"></i>
	  </li>
	  <li>
			<span id="status5" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9">12,23,3,12,5,3,12,11,23,3,12,5,4,12</span>
			<span class="bold">Avg. Visit Duration : 2000</span>
			<i class="fa fa-arrow-down red"></i>
	  </li>
	  <li>
			<span id="status6" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9">12,23,3,12,5,3,12,11,23,3,12,5,4,12</span>
			<span class="bold">Bounce Rate : 2000</span>
			<i class="fa fa-arrow-up green"></i>
	  </li>
	  <li>
			<span id="status7" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9">12,23,3,12,5,3,12,11,23,3,12,5,4,12</span>
			<span class="bold">% New Visits : 2000</span>
			<i class="fa fa-arrow-up green"></i>
	  </li>
	</ul>
</div>
<!-- end today status widget -->

<script type="text/javascript">
	$widget
		.find('.sparkinline')
		.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
</script>