<!-- start last order widget -->
<div class="widget-body">
  <div class="table-responsive clearfix">
		<table class="table table-hover no-margin">
			<thead>
				<tr>
					<th><a href="#"><span>Order ID</span></a></th>
					<th><a href="#" class="desc"><span>Date</span></a></th>
					<th><a href="#" class="asc"><span>Customer</span></a></th>
					<th class="text-center"><span>Status</span></th>
					<th class="text-right"><span>Price</span></th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="#">#8002</a></td>
					<td>2013/08/08</td>
					<td><a href="#">Robert De Niro</a></td>
					<td class="text-center"><span class="label label-success">Completed</span></td>
					<td class="text-right">$ 825.50</td>
					<td class="text-center" style="width: 15%;">
						<a href="#" class="table-link">
							<span class="fa-stack">
								<i class="fa fa-square fa-stack-2x"></i>
								<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</td>
				</tr>
				<tr>
					<td><a href="#">#5832</a></td>
					<td>2013/08/08</td>
					<td><a href="#">John Wayne</a></td>
					<td class="text-center"><span class="label label-warning">On hold</span></td>
					<td class="text-right">$ 923.93</td>
					<td class="text-center" style="width: 15%;">
						<a href="#" class="table-link">
							<span class="fa-stack">
								<i class="fa fa-square fa-stack-2x"></i>
								<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</td>
				</tr>
				<tr>
					<td><a href="#">#2547</a></td>
					<td>2013/08/08</td>
					<td><a href="#">Anthony Hopkins</a></td>
					<td class="text-center"><span class="label label-info">Pending</span></td>
					<td class="text-right">$ 1.625.50</td>
					<td class="text-center" style="width: 15%;">
						<a href="#" class="table-link">
							<span class="fa-stack">
								<i class="fa fa-square fa-stack-2x"></i>
								<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</td>
				</tr>
				<tr>
					<td><a href="#">#9274</a></td>
					<td>2013/08/08</td>
					<td><a href="#">Charles Chaplin</a></td>
					<td class="text-center"><span class="label label-danger">Cancelled</span></td>
					<td class="text-right">$ 35.34</td>
					<td class="text-center" style="width: 15%;">
						<a href="#" class="table-link">
							<span class="fa-stack">
								<i class="fa fa-square fa-stack-2x"></i>
								<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</td>
				</tr>
				<tr>
					<td><a href="#">#8463</a></td>
					<td>2013/08/08</td>
					<td><a href="#">Gary Cooper</a></td>
					<td class="text-center"><span class="label label-success">Completed</span></td>
					<td class="text-right">$ 34.199.99</td>
					<td class="text-center" style="width: 15%;">
						<a href="#" class="table-link">
							<span class="fa-stack">
								<i class="fa fa-square fa-stack-2x"></i>
								<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
							</span>
						</a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="widget-foot clearfix">
	<a href="#" class="btn btn-primary pull-right">
		<i class="fa fa-eye fa-lg"></i> View all orders
	</a>
</div>
<!-- end last order widget -->