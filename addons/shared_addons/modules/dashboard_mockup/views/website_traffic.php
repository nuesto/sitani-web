<!-- start website traffic widget -->
<div class="widget-body">
	<div id="website-traffic-graphic" style="height:250px"></div>
</div>
<div class="widget-foot no-background">
  <div class="row row-merge">
		<div class="col-xs-3 text-center border-right">
			<h4 class="no-margin">1232</h4>
			<small class="text-muted">Visitors</small>
		</div>
		<div class="col-xs-3 text-center border-right">
			<h4 class="no-margin">5421</h4>
			<small class="text-muted">Orders</small>
		</div>
		<div class="col-xs-3 text-center border-right">
			<h4 class="no-margin">3021</h4>
			<small class="text-muted">Tickets</small>
		</div>
		<div class="col-xs-3 text-center">
			<h4 class="no-margin">7098</h4>
			<small class="text-muted">Customers</small>
		</div>
	</div>
</div>
<!-- end website traffic widget -->

<script type="text/javascript">
	var $placeholder = $('#website-traffic-graphic');

	//Flot Chart
	//Website traffic chart
	var init = { data: [[0, 5], [1, 8], [2, 5], [3, 8], [4, 7], [5,9], [6, 8], [7, 8], [8, 10], [9, 12], [10, 10]],
			 label: "Visitor"
		},
		options = {
			series: {
				lines: {
					show: true,
					fill: true,
					fillColor: 'rgba(121,206,167,0.2)'
				},
				points: {
					show: true,
					radius: '4.5'
				}
			},
			grid: {
				hoverable: true,
				clickable: true
			},
			colors: ["#37b494"]
		},
		plot;

	plot = $.plot($placeholder, [init], options);

	$("<div id='tooltip'></div>").css({
		position: "absolute",
		display: "none",
		border: "1px solid #222",
		padding: "4px",
		color: "#fff",
		"border-radius": "4px",
		"background-color": "rgb(0,0,0)",
		opacity: 0.90
	}).appendTo("body");

	$placeholder.bind("plothover", function (event, pos, item) {

		var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
		$("#hoverdata").text(str);

		if (item) {
			var x = item.datapoint[0],
				y = item.datapoint[1];

				$("#tooltip").html("Visitor : " + y)
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(200);
		} else {
			$("#tooltip").hide();
		}
	});

	$placeholder.bind("plotclick", function (event, pos, item) {
		if (item) {
			$("#clickdata").text(" - click point " + item.dataIndex + " in " + item.series.label);
			plot.highlight(item.series, item.datapoint);
		}
	});

	var animate = function () {
	   $placeholder.animate( {tabIndex: 0}, {
		   duration: 3000,
		   step: function ( now, fx ) {

				 var r = $.map( init.data, function ( o ) {
					  return [[ o[0], o[1] * fx.pos ]];
				});

				 plot.setData( [{ data: r }] );
			 plot.draw();
			}
		});
	}

	animate();
</script>