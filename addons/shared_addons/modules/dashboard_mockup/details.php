<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Dashboard_mockup extends Module
{
	public $version = '1.1';

	public function info()
	{
		$info = array();
		$info['name'] = array(
			'en' => 'Dashboard Mockup',
			'id' => 'Dashboard Mockup',
		);
		$info['description'] = array(
			'en' => 'Module dashboard prototype.',
			'id' => 'Modul prototipe dasbor.',
		);
		$info['frontend'] = true;
		$info['backend'] = false;
		$info['roles'] = array();

		return $info;
	}

	/**
	 * Install
	 *
	 * This function will set up our streams
	 *
	 */
	public function install()
	{
		$this->load->library('widgets/widgets');

		$available_widgets = $this->widgets->list_available_widgets();

		// create area dashboard stats
		$area_dashboard_stats = array(
			'title'	=> 'Dashboard Stats',
			'slug'	=> 'dashboard-stats'
		);

		if ($id = $this->widgets->add_area($area_dashboard_stats))
		{
			$dashboard_stats = $this->widgets->get_area('dashboard-stats');

			// get widget stats-box
			$statbox = $this->widgets->get_widget('statbox');
			
			// create instance
			
			$instance = array(
					array(
						'title' => 'Messages',
						'src_url' => 'dashboard_mockup/stats',
						'target_url' => 'admin/blog',
						'color' => 'bg-info',
						'icon_class' => 'fa fa-envelope',
						'widget_area_id' => $dashboard_stats->id,
						'widget_id' => $statbox->id,
					),
					array(
						'title' => 'Orders',
						'src_url' => 'dashboard_mockup/stats',
						'target_url' => 'admin/blog',
						'color' => 'bg-success',
						'icon_class' => 'fa fa-money',
						'widget_area_id' => $dashboard_stats->id,
						'widget_id' => $statbox->id,
					),
					array(
						'title' => 'Visitors/Day',
						'src_url' => 'dashboard_mockup/stats',
						'target_url' => 'admin/blog',
						'color' => 'bg-danger',
						'show_graphic' => '1',
						'graphic_options' => 'sparkHeight="56px" sparkBarWidth="6" sparkBarColor="#ffffff"',
						'widget_area_id' => $dashboard_stats->id,
						'widget_id' => $statbox->id,
					),
					array(
						'title' => 'Total Profit',
						'src_url' => 'dashboard_mockup/stats',
						'target_url' => 'admin/blog',
						'color' => 'bg-warning',
						'show_graphic' => '1',
						'graphic_options' => 'sparkHeight="56px" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="7" sparkLineColor="#ffffff" sparkFillColor="#ffffff"',
						'widget_area_id' => $dashboard_stats->id,
						'widget_id' => $statbox->id,
					),
				);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
			
			$status		= $result;
		}
		else
		{
			$status		= false;
		}

		// create area dashboard left
		$area_dashboard_left = array(
			'title'	=> 'Dashboard Left',
			'slug'	=> 'dashboard-left'
		);

		if ($id = $this->widgets->add_area($area_dashboard_left))
		{
			$dashboard_left = $this->widgets->get_area('dashboard-left');

			// get widget dashboard
			$dashboard = $this->widgets->get_widget('dashboard');
			
			// create instance
			
			$instance = array(
					array(
						'title' => 'Website Traffic',
						'view_uri' => 'dashboard_mockup/view/website_traffic',
						'color_scheme' => 'warning',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_left->id,
						'widget_id' => $dashboard->id,
					),
					array(
						'title' => 'Last Order',
						'view_uri' => 'dashboard_mockup/view/last_order',
						'color_scheme' => 'info',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_left->id,
						'widget_id' => $dashboard->id,
					),
				);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
			
			$status		= $result;
		}
		else
		{
			$status		= false;
		}

		// create area dashboard right
		$area_dashboard_right = array(
			'title'	=> 'Dashboard Right',
			'slug'	=> 'dashboard-right'
		);

		if ($id = $this->widgets->add_area($area_dashboard_right))
		{
			$dashboard_right = $this->widgets->get_area('dashboard-right');

			// get widget dashboard
			$dashboard = $this->widgets->get_widget('dashboard');
			
			// create instance
			
			$instance = array(
					array(
						'title' => 'Today Status',
						'view_uri' => 'dashboard_mockup/view/today_status',
						'color_scheme' => 'danger',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_right->id,
						'widget_id' => $dashboard->id,
					),
					array(
						'title' => 'Browser Access',
						'view_uri' => 'dashboard_mockup/view/browser_access',
						'color_scheme' => 'success',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_right->id,
						'widget_id' => $dashboard->id,
					),
					array(
						'title' => 'File Upload',
						'view_uri' => 'dashboard_mockup/view/file_upload',
						'color_scheme' => 'info',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_right->id,
						'widget_id' => $dashboard->id,
					),
				);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
			
			$status		= $result;
		}
		else
		{
			$status		= false;
		}

		// create area dashboard middle left
		$area_dashboard_middle_left = array(
			'title'	=> 'Dashboard Middle Left',
			'slug'	=> 'dashboard-middle-left'
		);

		if ($id = $this->widgets->add_area($area_dashboard_middle_left))
		{
			$dashboard_middle_left = $this->widgets->get_area('dashboard-middle-left');

			// get widget dashboard
			$dashboard = $this->widgets->get_widget('dashboard');
			
			// create instance
			
			$instance = array(
					array(
						'title' => 'Visitor Location',
						'view_uri' => 'dashboard_mockup/view/visitor_location',
						'color_scheme' => 'warning',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_middle_left->id,
						'widget_id' => $dashboard->id,
					),
				);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
			
			$status		= $result;
		}
		else
		{
			$status		= false;
		}

		// create area dashboard middle right
		$area_dashboard_middle_right = array(
			'title'	=> 'Dashboard Middle Right',
			'slug'	=> 'dashboard-middle-right'
		);

		if ($id = $this->widgets->add_area($area_dashboard_middle_right))
		{
			$dashboard_middle_right = $this->widgets->get_area('dashboard-middle-right');

			// get widget dashboard
			$dashboard = $this->widgets->get_widget('dashboard');
			
			// create instance
			
			$instance = array(
					array(
						'title' => 'Project Feed',
						'view_uri' => 'dashboard_mockup/view/project_feed',
						'color_scheme' => 'danger',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_middle_right->id,
						'widget_id' => $dashboard->id,
					),
				);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
			
			$status		= $result;
		}
		else
		{
			$status		= false;
		}

		// create area dashboard bottom left
		$area_dashboard_bottom_left = array(
			'title'	=> 'Dashboard Bottom Left',
			'slug'	=> 'dashboard-bottom-left'
		);

		if ($id = $this->widgets->add_area($area_dashboard_bottom_left))
		{
			$dashboard_bottom_left = $this->widgets->get_area('dashboard-bottom-left');

			// get widget dashboard
			$dashboard = $this->widgets->get_widget('dashboard');
			
			// create instance
			
			$instance = array(
					array(
						'title' => 'Curve Chart',
						'view_uri' => 'dashboard_mockup/view/curve_chart',
						'color_scheme' => 'success',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_bottom_left->id,
						'widget_id' => $dashboard->id,
					),
				);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
			
			$status		= $result;
		}
		else
		{
			$status		= false;
		}

		// create area dashboard bottom right
		$area_dashboard_bottom_right = array(
			'title'	=> 'Dashboard Bottom Right',
			'slug'	=> 'dashboard-bottom-right'
		);

		if ($id = $this->widgets->add_area($area_dashboard_bottom_right))
		{
			$dashboard_bottom_right = $this->widgets->get_area('dashboard-bottom-right');

			// get widget dashboard
			$dashboard = $this->widgets->get_widget('dashboard');
			
			// create instance
			
			$instance = array(
					array(
						'title' => 'Quick Post',
						'view_uri' => 'dashboard_mockup/view/quick_post',
						'color_scheme' => 'info',
						'show_title' => '1',
						'has_container' => '1',
						'widget_area_id' => $dashboard_bottom_right->id,
						'widget_id' => $dashboard->id,
					),
				);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
			
			$status		= $result;
		}
		else
		{
			$status		= false;
		}

		return $status;
	}

	/**
	 * Uninstall
	 *
	 * Uninstall our module - this should tear down
	 * all information associated with it.
	 */
	public function uninstall()
	{
		$this->load->library('widgets/widgets');
		$status = true;
		// remove area dashboard stats
		$dashboard_stats = $this->widgets->get_area('dashboard-stats');

		if(isset($dashboard_stats)) {
			$status = ($this->widgets->delete_area($dashboard_stats->id)) ? true : false;
		}

		// remove area dashboard left
		$dashboard_left = $this->widgets->get_area('dashboard-left');

		if(isset($dashboard_left)) {
			$status = ($this->widgets->delete_area($dashboard_left->id)) ? true : false;
		}

		// remove area dashboard right
		$dashboard_right = $this->widgets->get_area('dashboard-right');

		if(isset($dashboard_right)) {
			$status = ($this->widgets->delete_area($dashboard_right->id)) ? true : false;
		}

		// remove area dashboard middle left
		$dashboard_middle_left = $this->widgets->get_area('dashboard-middle-left');

		if(isset($dashboard_middle_left)) {
			$status = ($this->widgets->delete_area($dashboard_middle_left->id)) ? true : false;
		}

		// remove area dashboard middle right
		$dashboard_middle_right = $this->widgets->get_area('dashboard-middle-right');

		if(isset($dashboard_middle_right)) {
			$status = ($this->widgets->delete_area($dashboard_middle_right->id)) ? true : false;
		}

		// remove area dashboard bottom left
		$dashboard_bottom_left = $this->widgets->get_area('dashboard-bottom-left');

		if(isset($dashboard_bottom_left)) {
			$status = ($this->widgets->delete_area($dashboard_bottom_left->id)) ? true : false;
		}

		// remove area dashboard bottom right
		$dashboard_bottom_right = $this->widgets->get_area('dashboard-bottom-right');

		if(isset($dashboard_bottom_right)) {
			$status = ($this->widgets->delete_area($dashboard_bottom_right->id)) ? true : false;
		}

		$status = ($this->widgets->delete_widget('statbox')) ? true : false;
		$status = ($this->widgets->delete_widget('dashboard')) ? true : false;

		return $status;
	}

	public function upgrade($old_version)
	{
		switch($old_version){

	    case '1.0': 
	    	$this->load->library('widgets/widgets');

	    	$available_widgets = $this->widgets->list_available_widgets();

			// create area dashboard stats
			$area_dashboard_stats = array(
				'title'	=> 'Dashboard Stats',
				'slug'	=> 'dashboard-stats'
			);

			if ($id = $this->widgets->add_area($area_dashboard_stats))
			{
				$dashboard_stats = $this->widgets->get_area('dashboard-stats');

				// get widget stats-box
				$statbox = $this->widgets->get_widget('statbox');
				
				// create instance
				
				$instance = array(
						array(
							'title' => 'Messages',
							'src_url' => 'dashboard_mockup/stats',
							'target_url' => 'admin/blog',
							'color' => 'bg-info',
							'icon_class' => 'fa fa-envelope',
							'widget_area_id' => $dashboard_stats->id,
							'widget_id' => $statbox->id,
						),
						array(
							'title' => 'Orders',
							'src_url' => 'dashboard_mockup/stats',
							'target_url' => 'admin/blog',
							'color' => 'bg-success',
							'icon_class' => 'fa fa-money',
							'widget_area_id' => $dashboard_stats->id,
							'widget_id' => $statbox->id,
						),
						array(
							'title' => 'Visitors/Day',
							'src_url' => 'dashboard_mockup/stats',
							'target_url' => 'admin/blog',
							'color' => 'bg-danger',
							'show_graphic' => '1',
							'graphic_options' => 'sparkHeight="56px" sparkBarWidth="6" sparkBarColor="#ffffff"',
							'widget_area_id' => $dashboard_stats->id,
							'widget_id' => $statbox->id,
						),
						array(
							'title' => 'Total Profit',
							'src_url' => 'dashboard_mockup/stats',
							'target_url' => 'admin/blog',
							'color' => 'bg-warning',
							'show_graphic' => '1',
							'graphic_options' => 'sparkHeight="56px" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="7" sparkLineColor="#ffffff" sparkFillColor="#ffffff"',
							'widget_area_id' => $dashboard_stats->id,
							'widget_id' => $statbox->id,
						),
					);

				foreach ($instance as $itc) {
					$title = $itc['title'];
					$widget_id = $itc['widget_id'];
					$widget_area_id = $itc['widget_area_id'];

					$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
				}
				
				$status		= $result;
			}
			else
			{
				$status		= false;
			}

			// create area dashboard left
			$area_dashboard_left = array(
				'title'	=> 'Dashboard Left',
				'slug'	=> 'dashboard-left'
			);

			if ($id = $this->widgets->add_area($area_dashboard_left))
			{
				$dashboard_left = $this->widgets->get_area('dashboard-left');

				// get widget dashboard
				$dashboard = $this->widgets->get_widget('dashboard');
				
				// create instance
				
				$instance = array(
						array(
							'title' => 'Website Traffic',
							'view_uri' => 'dashboard_mockup/view/website_traffic',
							'color_scheme' => 'warning',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_left->id,
							'widget_id' => $dashboard->id,
						),
						array(
							'title' => 'Last Order',
							'view_uri' => 'dashboard_mockup/view/last_order',
							'color_scheme' => 'info',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_left->id,
							'widget_id' => $dashboard->id,
						),
					);

				foreach ($instance as $itc) {
					$title = $itc['title'];
					$widget_id = $itc['widget_id'];
					$widget_area_id = $itc['widget_area_id'];

					$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
				}
				
				$status		= $result;
			}
			else
			{
				$status		= false;
			}

			// create area dashboard right
			$area_dashboard_right = array(
				'title'	=> 'Dashboard Right',
				'slug'	=> 'dashboard-right'
			);

			if ($id = $this->widgets->add_area($area_dashboard_right))
			{
				$dashboard_right = $this->widgets->get_area('dashboard-right');

				// get widget dashboard
				$dashboard = $this->widgets->get_widget('dashboard');
				
				// create instance
				
				$instance = array(
						array(
							'title' => 'Today Status',
							'view_uri' => 'dashboard_mockup/view/today_status',
							'color_scheme' => 'danger',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_right->id,
							'widget_id' => $dashboard->id,
						),
						array(
							'title' => 'Browser Access',
							'view_uri' => 'dashboard_mockup/view/browser_access',
							'color_scheme' => 'success',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_right->id,
							'widget_id' => $dashboard->id,
						),
						array(
							'title' => 'File Upload',
							'view_uri' => 'dashboard_mockup/view/file_upload',
							'color_scheme' => 'info',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_right->id,
							'widget_id' => $dashboard->id,
						),
					);

				foreach ($instance as $itc) {
					$title = $itc['title'];
					$widget_id = $itc['widget_id'];
					$widget_area_id = $itc['widget_area_id'];

					$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
				}
				
				$status		= $result;
			}
			else
			{
				$status		= false;
			}

			// create area dashboard middle left
			$area_dashboard_middle_left = array(
				'title'	=> 'Dashboard Middle Left',
				'slug'	=> 'dashboard-middle-left'
			);

			if ($id = $this->widgets->add_area($area_dashboard_middle_left))
			{
				$dashboard_middle_left = $this->widgets->get_area('dashboard-middle-left');

				// get widget dashboard
				$dashboard = $this->widgets->get_widget('dashboard');
				
				// create instance
				
				$instance = array(
						array(
							'title' => 'Visitor Location',
							'view_uri' => 'dashboard_mockup/view/visitor_location',
							'color_scheme' => 'warning',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_middle_left->id,
							'widget_id' => $dashboard->id,
						),
					);

				foreach ($instance as $itc) {
					$title = $itc['title'];
					$widget_id = $itc['widget_id'];
					$widget_area_id = $itc['widget_area_id'];

					$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
				}
				
				$status		= $result;
			}
			else
			{
				$status		= false;
			}

			// create area dashboard middle right
			$area_dashboard_middle_right = array(
				'title'	=> 'Dashboard Middle Right',
				'slug'	=> 'dashboard-middle-right'
			);

			if ($id = $this->widgets->add_area($area_dashboard_middle_right))
			{
				$dashboard_middle_right = $this->widgets->get_area('dashboard-middle-right');

				// get widget dashboard
				$dashboard = $this->widgets->get_widget('dashboard');
				
				// create instance
				
				$instance = array(
						array(
							'title' => 'Project Feed',
							'view_uri' => 'dashboard_mockup/view/project_feed',
							'color_scheme' => 'danger',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_middle_right->id,
							'widget_id' => $dashboard->id,
						),
					);

				foreach ($instance as $itc) {
					$title = $itc['title'];
					$widget_id = $itc['widget_id'];
					$widget_area_id = $itc['widget_area_id'];

					$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
				}
				
				$status		= $result;
			}
			else
			{
				$status		= false;
			}

			// create area dashboard bottom left
			$area_dashboard_bottom_left = array(
				'title'	=> 'Dashboard Bottom Left',
				'slug'	=> 'dashboard-bottom-left'
			);

			if ($id = $this->widgets->add_area($area_dashboard_bottom_left))
			{
				$dashboard_bottom_left = $this->widgets->get_area('dashboard-bottom-left');

				// get widget dashboard
				$dashboard = $this->widgets->get_widget('dashboard');
				
				// create instance
				
				$instance = array(
						array(
							'title' => 'Curve Chart',
							'view_uri' => 'dashboard_mockup/view/curve_chart',
							'color_scheme' => 'success',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_bottom_left->id,
							'widget_id' => $dashboard->id,
						),
					);

				foreach ($instance as $itc) {
					$title = $itc['title'];
					$widget_id = $itc['widget_id'];
					$widget_area_id = $itc['widget_area_id'];

					$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
				}
				
				$status		= $result;
			}
			else
			{
				$status		= false;
			}

			// create area dashboard bottom right
			$area_dashboard_bottom_right = array(
				'title'	=> 'Dashboard Bottom Right',
				'slug'	=> 'dashboard-bottom-right'
			);

			if ($id = $this->widgets->add_area($area_dashboard_bottom_right))
			{
				$dashboard_bottom_right = $this->widgets->get_area('dashboard-bottom-right');

				// get widget dashboard
				$dashboard = $this->widgets->get_widget('dashboard');
				
				// create instance
				
				$instance = array(
						array(
							'title' => 'Quick Post',
							'view_uri' => 'dashboard_mockup/view/quick_post',
							'color_scheme' => 'info',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $dashboard_bottom_right->id,
							'widget_id' => $dashboard->id,
						),
					);

				foreach ($instance as $itc) {
					$title = $itc['title'];
					$widget_id = $itc['widget_id'];
					$widget_area_id = $itc['widget_area_id'];

					$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
				}
				
				$status		= $result;
			}
			else
			{
				$status		= false;
			}

	    }
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}