<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Grid List Field Type
 *
 * @package		Addons\Field Types
 * @author		Aditya Satrya (asatrya)
 */
class Field_grid_list
{
	public $field_type_slug    = 'grid_list';
	public $db_col_type        = 'text';
	public $version            = '1.0.0';
	public $custom_parameters  = array('columns',);

	// --------------------------------------------------------------------------

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	// --------------------------------------------------------------------------

	/**
	 * Output form input
	 *
	 * @param	array
	 * @param	array
	 * @return	string
	 */
	public function form_output($data)
	{
		if(is_array($data['value'])){
			$output = $this->_transpose_array($data['value']);
		}else{
			$output = unserialize($data['value']);
		}

		if (is_null($output) OR (!is_array($data['value']) AND strlen($data['value']) == 0)) {
			$str = '<table class="table table-bordered grid_list_field" id="'.$data['form_slug'].'">';

			$str .= '<tr>';
			if(isset($data['custom']['columns']) AND is_array($data['custom']['columns'])){
				foreach($data['custom']['columns'] as $columns){
					$str .= '<th>'.$columns['label'].'</th>';
				}
			}
			$str .= '<th>';
			$str .= '</th>';
			$str .= '</tr>';

			$str .= '<tr>';
			if(isset($data['custom']['columns']) AND is_array($data['custom']['columns'])){
				foreach($data['custom']['columns'] as $columns){
					
					$form_slug = $data['form_slug'].'['.$columns['slug'].'][]';

					$param = array(
						'form_slug' => $form_slug,
						'value' => '',
					);
					if(isset($columns['custom'])){
						$param['custom'] = $columns['custom'];
					}
					$field = NULL;
					if($columns['type'] == 'choice'){
						$field = new stdClass();
						$field->is_required = FALSE;
					}
					$str .= '<td>'.$this->CI->type->types->{$columns['type']}->form_output($param, NULL, $field).'</td>';
				}
			}
			$str .= '<td>';
			$str .= '<div class="btn btn-sm btn-success add">+</div>&nbsp;<div class="btn btn-sm btn-danger remove">-</div>';
			$str .= '</td>';
			$str .= '<tr>';

			$str .= '</table>';

			return $str;

		} else {

			$str = '<table class="table table-bordered grid_list_field" id="'.$data['form_slug'].'">';

			$str .= '<tr>';
			if(isset($data['custom']['columns']) AND is_array($data['custom']['columns'])){
				foreach($data['custom']['columns'] as $columns){
					$str .= '<th>'.$columns['label'].'</th>';
				}
			}
			$str .= '<th>';
			$str .= '</th>';
			$str .= '</tr>';
			
			if(is_array ($output) AND isset($data['custom']['columns']) AND is_array($data['custom']['columns'])){

				$columns = $data['custom']['columns'];

				foreach ($output as $line) {
					if (is_array($line)) {
						$str .= '<tr>';
						$col_count = 0;


						foreach($columns as $column){

							$type = $data['custom']['columns'][$col_count]['type'];

							$str .= '<td>';
							
							$form_slug = $data['form_slug'].'['.$column['slug'].'][]';

							$value = '';
							if(isset($line[$column['slug']])){
								$value = $line[$column['slug']];
							}

							$param = array(
								'form_slug' => $form_slug,
								'value' => $value,
							);
							if(isset($data['custom']['columns'][$col_count]['custom'])){
								$param['custom'] = $data['custom']['columns'][$col_count]['custom'];
							}
							$field = NULL;
							if($type == 'choice'){
								$field = new stdClass();
								$field->is_required = FALSE;
							}
							$str .= $this->CI->type->types->{$data['custom']['columns'][$col_count]['type']}->form_output($param, NULL, $field);
							$str .= '</td>';
							$col_count++;
						}

						$str .= '<td>';
						$str .= '<div class="btn gray add">+</div>&nbsp;<div class="btn gray remove">-</div>';
						$str .= '</td>';
						$str .= '</tr>';
					}
				}
			}
			return $str.'</table>';
		}
	}

	public function event($field)
	{
		$this->CI->type->add_js('grid_list', 'grid_list.js');
		$this->CI->type->add_css('grid_list', 'grid_list.css');
	}

	public function pre_save($input, $field)
	{
		$input = $this->_pre_save_file($input, $field);
		
		return serialize($this->_transpose_array($input));
	}

	private function _pre_save_file($input, $field)
	{
		foreach($field->field_data as $columns){
			foreach($columns as $column){
				if($column['type'] == 'file' OR $column['type'] == 'image'){
					foreach($input[$column['slug']] as $idx => $value){
						$field_slug = $field->field_slug;
						$column_slug = $column['slug'];

						$input['file_'.$column_slug][$idx]['name'] = $_FILES['file_'.$field_slug]['name'][$column_slug][$idx];
						$input['file_'.$column_slug][$idx]['type'] = $_FILES['file_'.$field_slug]['type'][$column_slug][$idx];
						$input['file_'.$column_slug][$idx]['tmp_name'] = $_FILES['file_'.$field_slug]['tmp_name'][$column_slug][$idx];
						$input['file_'.$column_slug][$idx]['error'] = $_FILES['file_'.$field_slug]['error'][$column_slug][$idx];
						$input['file_'.$column_slug][$idx]['size'] = $_FILES['file_'.$field_slug]['size'][$column_slug][$idx];

						$_FILES['file_'.$column_slug.'_'.$idx] = $input['file_'.$column_slug][$idx];

						$input[$column_slug][$idx] = $this->_upload_file($column['type'], $field_slug, $column_slug, $idx, $column_slug.'_'.$idx, $column['custom']);

						unset($input['file_'.$column_slug]);
					}
				}
			}
		}
		return $input;
	}

	private function _upload_file($type, $field_slug, $column_slug, $idx, $file_field_slug, $custom = array())
	{
		// If we do not have a file that is being submitted. If we do not,
		// it could be the case that we already have one, in which case just
		// return the numeric file record value.
		if ( ! isset($_FILES['file_'.$file_field_slug]['name']) or ! $_FILES['file_'.$file_field_slug]['name'])
		{
			$field_slug_post = $this->CI->input->post($field_slug);
			if (isset($field_slug_post[$column_slug][$idx]))
			{
				return $field_slug_post[$column_slug][$idx];
			}
			else
			{
				return null;
			}
		}
		

		$this->CI->load->library('files/files');

		// Resize options
		$resize_width = NULL;
		$resize_height = NULL;
		$keep_ratio = NULL;

		if($type == 'image'){
			$resize_width 	= (isset($custom['resize_width'])) ? $custom['resize_width'] : null;
			$resize_height 	= (isset($custom['resize_height'])) ? $custom['resize_height'] : null;
			$keep_ratio 	= (isset($custom['keep_ratio']) and $custom['keep_ratio'] == 'yes') ? true : false;
		}

		$folder_id = $custom['folder'];

		// If you don't set allowed types, we'll set it to allow all.
		$allowed_types 	= (isset($custom['allowed_types'])) ? $custom['allowed_types'] : '*';

		$return = Files::upload($folder_id, null, 'file_'.$file_field_slug, $resize_width, $resize_height, $keep_ratio, $allowed_types);
		
		if ( ! $return['status'])
		{
			$this->CI->session->set_flashdata('notice', $return['message']);

			return null;
		}
		else
		{
			// Return the ID of the file DB entry
			return $return['data']['id'];
		}
	}

	public function pre_output($input, $data)
	{
		return unserialize($input);
	}

	public function _transpose_array($input_arr)
	{
		$result_arr = array();
		foreach($input_arr as $key => $value_arr){
			foreach($value_arr as $idx => $value){
				$result_arr[$idx][$key] = $value;
			}
		}

		return $result_arr;
	}
}
