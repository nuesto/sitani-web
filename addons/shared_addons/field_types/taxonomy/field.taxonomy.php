<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * PyroStreams Taxonomy Field Type
 *
 * @author		Aditya Satrya
 * @copyright	Copyright (c) 2013, Aditya Satrya
 */
class Field_taxonomy
{
	public $field_type_name 		= 'Taxonomy';
	public $field_type_slug			= 'taxonomy';
	public $alt_process				= true;
	public $db_col_type				= false;
	public $custom_parameters		= array('taxonomy_slug', 'multiselect_override');
	public $version					= '1.0.0';
	public $author					= array('name' => 'Aditya Satrya', 'url' => '');

	/**
	 * Output form input
	 *
	 * @param	array
	 * @return	string
	 */
	public function form_output($data, $entry_id, $field)
	{
		$this->CI->load->model('taxonomy/taxonomies_m');
		$this->CI->load->model('taxonomy/vocabularies_m');
		$this->CI->load->model('taxonomy/entries_m');
		
		$str = '';
		
		/************************************
		 * GET TAXONOMY
		 ************************************/
		
		$taxonomies = $this->CI->taxonomies_m->get_taxonomy_by_slug($data['custom']['taxonomy_slug']);
		
		if($taxonomies['total'] > 0){
			$taxonomy = $taxonomies['entries'][0];
		}else{
			return 'no taxonomy like that';
		}
		
		/************************************
		 * GET VOCABULARIES
		 ************************************/
		
		$root_vocabularies = $this->CI->vocabularies_m->get_vocabularies_by_taxonomy_and_parent($taxonomy['id'], NULL);
		$vocabularies['entries'] = $this->_build_serial_tree($taxonomy['id'], array(), $root_vocabularies);
		$vocabularies['total'] = count($vocabularies['entries']);
		
		if($vocabularies['total'] == 0){
			return 'this taxonomy has no vocabulary';
		}
		
		/************************************
		 * GET ENTRY
		 ************************************/
		
		if($entry_id){
			$entries = $this->CI->entries_m->get_entries($taxonomy['id'], $field->stream_namespace, $field->stream_slug, $entry_id);
		}
		
		$choices = array('' => '----');
		foreach($vocabularies['entries'] as $vocabulary){
			$choices[$vocabulary['id']] = $vocabulary['vocabulary_name'];
		}
		
		/************************************
		 * MULTISELECT?
		 ************************************/
		
		if(isset($data['custom']['multiselect_override'])){
			if($data['custom']['multiselect_override'] == 'yes'){
				$multiselect = TRUE;
			}else{
				$multiselect = FALSE;
			}
		}elseif($taxonomy['taxonomy_multiselect']['key'] == 'yes'){
			$multiselect = TRUE;
		}else{
			$multiselect = FALSE;
		}
		
		if($multiselect){
			$entry_ids = array();
			if(isset($entries) AND is_array($entries)){
				foreach($entries['entries'] as $entry){
					$entry_ids[] = $entry['entry_vocabulary']['id'];
				}
			}
			
			$str .= form_multiselect($data['form_slug'].'[]', $choices, $entry_ids, 'id="'.$data['form_slug'].'" class="'.$data['form_slug'].' form-control"');
		}else{
			$entry_id = '';
			if(isset($entries['entries'][0])){
				$entry_id = $entries['entries'][0]['entry_vocabulary']['id'];
			}
			
			$str .= form_dropdown($data['form_slug'], $choices, $entry_id, 'id="'.$data['form_slug'].'" class="'.$data['form_slug'].' form-control"');
		}

		return $str;
	}
	
	private function _build_serial_tree($taxonomy_id, $serial_tree, $vocabularies, $level = 0)
	{
		foreach($vocabularies['entries'] as $vocabulary){
			$prefix = '';
			for($i = 0; $i < $level; $i++){
				$prefix .= '-- ';
			}
			
			$vocabulary['vocabulary_name'] = $prefix.$vocabulary['vocabulary_name'];
		
			$serial_tree[] = $vocabulary;
			$children = $this->CI->vocabularies_m->get_vocabularies_by_taxonomy_and_parent($taxonomy_id, $vocabulary['id']);
			
			if($children['total'] == 0){
				continue;
			}else{
				$serial_tree = $this->_build_serial_tree($taxonomy_id, $serial_tree, $children, $level+1);
			}
		}
		
		return $serial_tree;
	}
	
	public function pre_save($input, $field, $stream, $id, $form_data)
	{
		$this->CI->load->model('taxonomy/taxonomies_m');
		$this->CI->load->model('taxonomy/vocabularies_m');
		$this->CI->load->model('taxonomy/entries_m');
		
		/************************************
		 * GET TAXONOMY
		 ************************************/
		
		$taxonomies = $this->CI->taxonomies_m->get_taxonomy_by_slug($field->field_data['taxonomy_slug']);
		$taxonomy = $taxonomies['entries'][0];
		
		/************************************
		 * DELETE ENTRY
		 ************************************/
		
		$this->CI->entries_m->delete_entries_by_namespace_slug_id($stream->stream_namespace, $stream->stream_slug, $id);
		
		/************************************
		 * INSERT ENTRY
		 ************************************/
		
		if(is_array($input)){
			foreach($input as $vocabulary_id){
				$this->CI->entries_m->insert_entry($taxonomy['id'], $vocabulary_id, $stream->stream_namespace, $stream->stream_slug, $id);
			}
		}else{
			$this->CI->entries_m->insert_entry($taxonomy['id'], $input, $stream->stream_namespace, $stream->stream_slug, $id);
		}
		
	}
	
	public function alt_pre_output($row_id, $extra, $type, $stream)
	{
		$this->CI->load->model('taxonomy/taxonomies_m');
		$this->CI->load->model('taxonomy/vocabularies_m');
		$this->CI->load->model('taxonomy/entries_m');
		
		/************************************
		 * GET TAXONOMY
		 ************************************/
		
		$taxonomies = $this->CI->taxonomies_m->get_taxonomy_by_slug($extra['taxonomy_slug']);
		if($taxonomies['total'] > 0){
			$taxonomy = $taxonomies['entries'][0];
		}else{
			return 'No taxonomy like this';
		}
		
		/************************************
		 * GET ENTRY
		 ************************************/
		if($row_id){
			$entries = $this->CI->entries_m->get_entries($taxonomy['id'], $stream->stream_namespace, $stream->stream_slug, $row_id);
		}
		
		$vocabularies = array();
		if(isset($entries)){
			foreach($entries['entries'] as $entry){
				$vocabularies[] = $entry['entry_vocabulary']['vocabulary_name'];
			}
		}
		
		return implode(', ', $vocabularies);
	}
	
	public function alt_pre_output_plugin($column_data, $field_data, $column_slug, $stream)
	{
		$this->CI->load->model('taxonomy/taxonomies_m');
		$this->CI->load->model('taxonomy/vocabularies_m');
		$this->CI->load->model('taxonomy/entries_m');
		
		/************************************
		 * GET TAXONOMY
		 ************************************/
		
		$taxonomies = $this->CI->taxonomies_m->get_taxonomy_by_slug($field_data['taxonomy_slug']);
		$taxonomy = $taxonomies['entries'][0];
		
		/************************************
		 * GET ENTRY
		 ************************************/
		
		$entries = $this->CI->entries_m->get_entries($taxonomy['id'], $stream->stream_namespace, $stream->stream_slug, $column_data);
		
		$vocabularies = array();
		foreach($entries['entries'] as $entry){
			$vocabularies[] = $entry['entry_vocabulary'];
		}		
		
		return $vocabularies;
	}
	
	/**
	 * Choose Taxonomy
	 *
	 * @param 	string [$choice]
	 * @return 	string the input form
	 */
	public function param_taxonomy_slug($choice = null)
	{
		$this->CI = get_instance();
		
		$taxonomies = $this->CI->db->get('taxonomy_taxonomies')->result();
		$choices = array();
		foreach($taxonomies as $taxonomy){
			$choices[$taxonomy->taxonomy_slug] = $taxonomy->taxonomy_name;
		}
		
		return form_dropdown('taxonomy_slug', $choices, $choice, 'class="form-control"');
	}
	
	public function param_multiselect_override($choice = null)
	{
		$choices = array(
			'' => '----',
			'yes' => 'Yes',
			'no' => 'No',
		);
		
		return form_dropdown('multiselect_override', $choices, $choice, 'class="form-control"');
	}
}
