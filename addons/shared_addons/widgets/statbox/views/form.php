<div class="form-group">
  <label><?php echo lang('statbox:uri_data_label') ?></label>
   <?php echo form_input('src_url', set_value('src_url', isset($options['src_url']) ? $options['src_url'] : ''), 'class="form-control"') ?>
   <span class="help-block"><?php echo lang('statbox:uri_data_desc') ?></span>
</div>

<div class="form-group">
  <label><?php echo lang('statbox:uri_target_label') ?></label>
  <?php echo form_input('target_url', set_value('target_url', isset($options['target_url']) ? $options['target_url'] : ''), 'class="form-control"') ?>
  <span class="help-block"><?php echo lang('statbox:uri_target_desc') ?></span>
</div>

<div class="form-group">
  <label><?php echo lang('statbox:background_label') ?></label>
  <?php echo form_dropdown('color', $opt_color, isset($options['color']) ? $options['color'] : '', 'class="form-control"') ?>
  <span class="help-block"><?php echo lang('statbox:background_desc') ?></span>
</div>

<div class="form-group">
  <label><?php echo lang('statbox:icon_label') ?></label>
  <?php echo form_input('icon_class', set_value('icon_class', isset($options['icon_class']) ? $options['icon_class'] : ''), 'class="form-control"') ?>
  <span class="help-block"><?php echo lang('statbox:icon_desc') ?></span>
</div>

<div class="form-group">
  <label class="checkbox-inline">
	  <?php echo form_checkbox('show_graphic', true, isset($options['show_graphic']) ? $options['show_graphic'] : false).' '.lang('statbox:graphic_show_label') ?>
	</label>
  <span class="help-block"><?php echo lang('statbox:graphic_show_desc') ?></span>
</div>

<div class="form-group">
  <label><?php echo lang('statbox:graphic_option_label') ?></label>
  <?php echo form_input('graphic_options', set_value('graphic_options', isset($options['graphic_options']) ? $options['graphic_options'] : ''), 'class="form-control"') ?>
  <span class="help-block"><?php echo lang('statbox:graphic_option_desc') ?></span>
</div>