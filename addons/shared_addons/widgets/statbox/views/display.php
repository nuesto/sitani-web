<div class="col-sm-6 col-md-3">
	<div class="statsbox <?php echo $opt['color'] ?>" data-source="<?php echo !empty($opt['src_url']) ? site_url($opt['src_url']) : '' ?>">
	  <a href="<?php echo !empty($opt['target_url']) ? site_url($opt['target_url']) : '#' ?>" class="clearfix">
		  <div class="pull-left">
		  <?php if (isset($opt['show_graphic'])): ?>
		  	<span class="sparkinline" <?php echo $opt['graphic_options'] ?> ></span>
		  <?php else: ?>
		  	<i class="<?php echo !empty($opt['icon_class']) ? $opt['icon_class'] : 'fa fa-cogs' ?>"></i>
		  <?php endif; ?>
		  </div>
		  <div class="datas-text pull-right">
		  	<span>-</span> <?php echo $opt['instance_title'] ?>
		  </div>
	  </a>
	  <div class="stats-refresh">
			<i class="fa fa-refresh"></i>
		</div>
	</div>
</div><!-- /.col -->