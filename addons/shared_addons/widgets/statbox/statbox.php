<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget_Statbox extends Widgets
{
	// The widget title,  this is displayed in the admin interface
	public $title = 'Stats Box';

	//The widget description, this is also displayed in the admin interface.  Keep it brief.
	public $description = 'Display stat box.';

	// The author's name
	public $author = 'davigmacode';

	// The authors website for the widget
	public $website = 'http://nuesto.net/';

	//current version of your widget
	public $version = '1.0';

	/**
	 * $fields array fore storing widget options in the database.
	 * values submited through the widget instance form are serialized and
	 * stored in the database.
	 */
	public $fields = array(
		array(
			'field'   => 'src_url',
			'label'   => 'URL Data',
			'rules'   => 'required|prep_url'
		),
		array(
			'field'   => 'color',
			'label'   => 'Background Color',
			'rules'   => 'required'
		),
		array(
			'field'   => 'target_url',
			'label'   => 'Background Color',
			'rules'   => 'prep_url'
		)
	);

	/**
	 * the $options param is passed by the core Widget class.  If you have
	 * stored options in the database,  you must pass the paramater to access
	 * them.
	 */
	public function run($options)
	{
		// if(empty($options['field_name']))
		// {
		// 	//return an array of data that will be parsed by views/display.php
		// 	return array('output' => '');
		// }

		// Store the feed items
		return array('opt' => $options);
	}

	/**
	 * form() is used to prepare/pass data to the widget admin form
	 * data returned from this method will be available to views/form.php
	 */
	public function form()
	{
		$this->lang->load('statbox');

		$droption = array();

		$droption['opt_color'] = array(
			'bg-danger' => 'Red',
			'bg-info' => 'Blue',
			'bg-success' => 'Green',
			'bg-warning' => 'Yellow',
		);

		return $droption;
	}

	/**
	 * save() is used to alter submited data before their insertion in database
	 */
	// public function save($options)
	// {
	//    if(isset($options['foo']) && !isset($options['bar'])){
	// 	   $options['bar'] = $options['foo'];
	//    }
	//    return $options;
	// }
}