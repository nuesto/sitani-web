<?php defined('BASEPATH') OR exit('No direct script access allowed');

// sidebar
$lang['statbox:uri_data_label']       = 'URI Sumber Data';
$lang['statbox:uri_data_desc']        = 'Alamat uri dari fungsi yang digunakan untuk mengambil data json. Contoh format json: {"items":[7,3,3,10,5,5],"total":52855}';

$lang['statbox:uri_target_label']     = 'URI Target';
$lang['statbox:uri_target_desc']      = 'Alamat uri dari halaman yang akan dituju saat widget diklik.';

$lang['statbox:background_label']     = 'Warna Latar';
$lang['statbox:background_desc']      = 'Pilihan warna latar widget';

$lang['statbox:icon_label']           = 'Class Ikon';
$lang['statbox:icon_desc']            = 'Contoh class : fa fa-envelope. Daftar lengkap class icon dapat dilihat <a href="http://fontawesome.io/icons/" target="_blank">disini</a>';

$lang['statbox:graphic_show_label']   = 'Tampilkan Grafik Item';
$lang['statbox:graphic_show_desc']    = 'Opsi untuk menampilkan grafik pada widget, grafik akan menggantikan ikon';

$lang['statbox:graphic_option_label'] = 'Properti Grafik';
$lang['statbox:graphic_option_desc']  = 'Daftar properti yang digunakan untuk kostumisasi tampilan grafik. Contoh : sparkHeight="56px" sparkBarWidth="6". Daftar selengkapnya dapat dilihat <a href="http://omnipotent.net/jquery.sparkline/#s-docs" target="_blank">disini</a>';
