<?php defined('BASEPATH') OR exit('No direct script access allowed');

// sidebar
$lang['statbox:uri_data_label']       = 'Datasource URI';
$lang['statbox:uri_data_desc']        = 'URI address of a function used to fetch data json. Example format json: {"items":[7,3,3,10,5,5],"total":52855}';

$lang['statbox:uri_target_label']     = 'Target Link URI';
$lang['statbox:uri_target_desc']      = 'URI address of a page when this widget is clicked.';

$lang['statbox:background_label']     = 'Background';
$lang['statbox:background_desc']      = 'Background widget options.';

$lang['statbox:icon_label']           = 'Class Icon';
$lang['statbox:icon_desc']            = 'Example class : fa fa-envelope. <a href="http://fontawesome.io/icons/" target="_blank">full list icon</a>';

$lang['statbox:graphic_show_label']   = 'Show Graphic Item';
$lang['statbox:graphic_show_desc']    = 'Option to show graphic item. graphic will replace icon on widget.';

$lang['statbox:graphic_option_label'] = 'Properti Grafik';
$lang['statbox:graphic_option_desc']  = 'Example : sparkHeight="56px" sparkBarWidth="6". <a href="http://omnipotent.net/jquery.sparkline/#s-docs" target="_blank">full list options</a>';
